//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//============================================================================
// Name        : main.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

#include "Model/Parameter/Parameters.h"
#include "Model/Prior/Priors.h"
#include "Model/Model.h"
#include "Sampler/Samples/Samples.h"
#include "Sampler/IncSamplers.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Likelihood/LikelihoodFactory.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "Utils/XML/ReaderXML.h"
#include "Utils/XML/WriterXML.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.h"


using namespace Sampler::Strategies;
using namespace StatisticalModel;
using namespace StatisticalModel::Likelihood;


template <typename T>
void testLogNormalND(size_t nProposal, size_t nSample, std::string sFileName) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		cout << "testLogNormalND" << endl;
	}

	const size_t nPPB = 4;
	const size_t seed = 1337;

	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);

	LikelihoodInterface::sharedPtr_t lik = LikelihoodFactory::createLogNormalNDim(4, 100);

	Model model(lik);
	Parameters &params = model.getParams();
	Blocks &blocks = model.getBlocks();

	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(lik.get());
	hlp->defineParameters(params);

	hlp->defineBlocks(ParameterBlock::Block::PCA_DOUBLE_VARIABLES, vector<size_t>(1, nPPB), params, blocks);

	Sample sample = hlp->defineInitSample(params);

	BlockSelector::sharedPtr_t bs = BlockSelector::createSingleCyclic(model);
	ModifierStrategy::sharedPtr_t ms = ModifierStrategy::createDefaultModifierStrategy(model);
	AcceptanceStrategy::sharedPtr_t as = AcceptanceStrategy::createDefaultAcceptanceStrategy(ms, model);

	stringstream ssPrefix;
	ssPrefix << sFileName << "IndepNormalND_" << nProposal << "P_" << nPPB << "B_";

	T sampler(sample, model, bs, ms, as);

	sampler.setOutputFile(ssPrefix.str());
	sampler.setWriteBinary(false);
	sampler.setWriteFloat(true);
	sampler.generateNIterations(nSample);

	if(Parallel::mpiMgr().getRank() == 0) {
		cout << "Acceptance ratio : " << as->getAcceptanceRatio() << endl;
		for(uint i=0; i<blocks.size(); ++i){
			cout << model.getBlocks().getBlock(i)->toString() << endl;
			cout << "**************************************" << endl;
		}
		XML::WriterXML writer("ConfigXML/IndepNormals.xml");
		writer.writeXML(nSample, &model, &sampler);
	}
}

template <typename T>
void testCorrelNormalND(size_t nProposal, size_t nSample, std::string sFileName) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		cout << "testCorrelNormalND" << endl;
	}

	const size_t nPPB = 20;
	const size_t seed = 1337;

	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);

	std::string input("Data/Correlated/covmat1.dat");
	LikelihoodInterface::sharedPtr_t lik = LikelihoodFactory::createCorrelatedNormals(100, input);

	Model model(lik);
	Parameters &params = model.getParams();
	Blocks &blocks = model.getBlocks();

	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(lik.get());
	hlp->defineParameters(params);

	hlp->defineBlocks(ParameterBlock::Block::PCA_DOUBLE_VARIABLES, vector<size_t>(1, nPPB), params, blocks);

	Sample sample = hlp->defineInitSample(params);

	BlockSelector::sharedPtr_t bs = BlockSelector::createSingleCyclic(model);
	ModifierStrategy::sharedPtr_t ms = ModifierStrategy::createDefaultModifierStrategy(model);
	AcceptanceStrategy::sharedPtr_t as = AcceptanceStrategy::createDefaultAcceptanceStrategy(ms, model);

	stringstream ssPrefix;
	ssPrefix << sFileName << "CorrelNormalND_" <<nProposal << "P_" << nPPB << "B_";

	//T sampler(sample, model, bs, ms, as);
	T sampler(sample, model, bs, ms, as, ParameterBlock::Config::ConfigFactory::createFastFactory());

	sampler.setOutputFile(ssPrefix.str());
	sampler.setWriteBinary(false);
	sampler.setWriteFloat(true);
	sampler.setThinning(10);
	sampler.generateNIterations(nSample);

	if(Parallel::mpiMgr().getRank() == 0) {
		cout << "Acceptance ratio : " << as->getAcceptanceRatio() << endl;
		for(uint i=0; i<blocks.size(); ++i){
			cout << model.getBlocks().getBlock(i)->toString() << endl;
			cout << "**************************************" << endl;
		}
		XML::WriterXML writer("ConfigXML/CorrelNormals.xml");
		writer.writeXML(nSample, &model, &sampler);
	}
}

template <typename T>
void testBDRateDAG(size_t nProposal, size_t nSample, std::string sFileName) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		cout << "testBDRateDAG" << endl;
	}

	const size_t nThread = 1;
	const size_t nPPB = 8;
	const size_t seed = 1337;

	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);

	std::string input("Data/PyRate/E2_M1.dat");
	LikelihoodInterface::sharedPtr_t lik = LikelihoodFactory::createBDRateDAG(nThread, input, false);

	Model model(lik);
	Parameters &params = model.getParams();
	Blocks &blocks = model.getBlocks();

	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(lik.get());
	hlp->defineParameters(params);
	hlp->defineBlocks(ParameterBlock::Block::PCA_DOUBLE_VARIABLES, vector<size_t>(1, nPPB), params, blocks);

	Sample sample = hlp->defineInitSample(params);

	BlockSelector::sharedPtr_t bs = BlockSelector::createSingleCyclic(model);
	ModifierStrategy::sharedPtr_t ms = ModifierStrategy::createDefaultModifierStrategy(model);
	AcceptanceStrategy::sharedPtr_t as = AcceptanceStrategy::createDefaultAcceptanceStrategy(ms, model);

	stringstream ssPrefix;
	ssPrefix << sFileName << "BDRateDAG" <<nProposal << "P_" << nPPB << "B_";

	T sampler(sample, model, bs, ms, as);

	sampler.setOutputFile(ssPrefix.str());
	sampler.setWriteBinary(false);
	sampler.setWriteFloat(true);
	sampler.setThinning(500);
	sampler.generateNIterations(nSample);

	if(Parallel::mpiMgr().getRank() == 0) {
		cout << "Acceptance ratio : " << as->getAcceptanceRatio() << endl;
		for(uint i=0; i<blocks.size(); ++i){
			cout << model.getBlocks().getBlock(i)->toString() << endl;
			cout << "**************************************" << endl;
		}
		XML::WriterXML writer("ConfigXML/BDRateDAG.xml");
		writer.writeXML(nSample, &model, &sampler);
	}
}


template <typename T>
void testBDRAlphaDAG(size_t nProposal, size_t nSample, std::string sFileName) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		cout << "testBDRAlpha" << endl;
	}

	const size_t nThread = 4;
	const size_t nQuantAlpha = 20;
	const size_t nPPB = 8;
	const size_t seed = 1337;
	const bool usePlant = true;

	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);

	std::string input("Data/PyRate/Big/tbl_gen_all_taxa_macro_1");
	LikelihoodInterface::sharedPtr_t lik = LikelihoodFactory::createBDRAlphaDAG(nThread, nQuantAlpha, input, usePlant);

	Model model(lik);
	Parameters &params = model.getParams();
	Blocks &blocks = model.getBlocks();

	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(lik.get());
	hlp->defineParameters(params);
	hlp->defineBlocks(ParameterBlock::Block::PCA_DOUBLE_VARIABLES, vector<size_t>(1, nPPB), params, blocks);

	Sample sample = hlp->defineInitSample(params);

	BlockSelector::sharedPtr_t bs = BlockSelector::createSingleCyclic(model);
	ModifierStrategy::sharedPtr_t ms = ModifierStrategy::createDefaultModifierStrategy(model);
	AcceptanceStrategy::sharedPtr_t as = AcceptanceStrategy::createDefaultAcceptanceStrategy(ms, model);

	stringstream ssPrefix;
	ssPrefix << sFileName << "BDRAlphaDAG_" <<nProposal << "P_" << nPPB << "B_";

	T sampler(sample, model, bs, ms, as);

	sampler.setOutputFile(ssPrefix.str());
	sampler.setWriteBinary(false);
	sampler.setWriteFloat(true);
	sampler.setThinning(500);
	sampler.generateNIterations(nSample);

	if(Parallel::mpiMgr().getRank() == 0) {
		cout << "Acceptance ratio : " << as->getAcceptanceRatio() << endl;
		for(uint i=0; i<blocks.size(); ++i){
			cout << model.getBlocks().getBlock(i)->toString() << endl;
			cout << "**************************************" << endl;
		}
		XML::WriterXML writer("ConfigXML/BDRAlphaDAG.xml");
		writer.writeXML(nSample, &model, &sampler);
	}
}

int main(int argc, char** argv) {

	// Init the MPI environnement
	Parallel::mpiMgr().init(&argc, &argv);

	XML::ReaderXML readerXML(argv[1]);
	size_t nSample = readerXML.getNIteration();

	double startTime = MPI_Wtime();
	readerXML.getPtrSampler()->generateNIterations(nSample);
	double endTime = MPI_Wtime();

	//readerXML.getPtrSampler()->saveCheckpoint();

	if(Parallel::mcmcMgr().isOutputManager()) {
		std::cout << "Sampling accomplished in " << endTime-startTime << " seconds." << std::endl;
		Sampler::PFAMCMC* tmpPtr = dynamic_cast<Sampler::PFAMCMC*>(readerXML.getPtrSampler().get());
		if(tmpPtr != NULL) {
			std::cout << tmpPtr->getPerfReport() << std::endl;
		}

		for(uint i=0; i<readerXML.getPtrModel()->getBlocks().size(); ++i){
			std::cout << readerXML.getPtrModel()->getBlocks().getBlock(i)->toString() << endl;
			std::cout << "**************************************" << endl;
		}

		StatisticalModel::Likelihood::TreeInference::Base* ptrLik =
				dynamic_cast<StatisticalModel::Likelihood::TreeInference::Base*>(readerXML.getPtrLikXML()->getLikelihoodPtr().get());
		if(ptrLik != NULL) {
			TR::Logger::LogAdapter logAdapter(readerXML.getPtrSampler().get(), ptrLik->getTreeManager());
			logAdapter.adaptLog();
		}

	}

	return 0;
}
