//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DefaultMS.cpp
 *
 *  Created on: 21 jan. 2014
 *      Author: meyerx
 */

#include "DefaultMS.h"

namespace Sampler {
namespace Strategies {

DefaultMS::DefaultMS(const Model &aModel, const uint aNSample) : ModifierStrategy(aModel){
	nSample = aNSample;
}

DefaultMS::~DefaultMS() {
}

void DefaultMS::proposeNextSamples(const ParameterBlock::Block::sharedPtr_t block, const Sample &sample, vector<Sample> &outProposedSamples){

	for(uint iS=0; iS<nSample; ++iS){
		Sample tmpSample(sample);

		block->proposeMove(tmpSample);
		outProposedSamples.push_back(tmpSample);
	}
}

void DefaultMS::setNSample(const uint aNSample){
	nSample	= aNSample;
}

} // namespace Strategies
} // namespace Sampler

