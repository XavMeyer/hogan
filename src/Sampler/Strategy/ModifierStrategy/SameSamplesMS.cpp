//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SameSamplesMS.cpp
 *
 * @date Sep 16, 2015
 * @author meyerx
 * @brief
 */
#include "SameSamplesMS.h"

namespace Sampler {
namespace Strategies {

const size_t SameSamplesMS::SHARED_SEED = 5678;

SameSamplesMS::SameSamplesMS(const Model &aModel) : ModifierStrategy(aModel){

	RNG* rng = Parallel::mpiMgr().getPRNG();
	// Save the current RNG state
	StateRNG stategRNG;
	rng->saveState(stategRNG);

	// Create a shared RNG state for all proc
	rng->setSeed(SHARED_SEED);
	// Save it
	rng->saveState(sharedState);

	// Set the RNG to its previous state
	rng->loadState(stategRNG);

}

SameSamplesMS::~SameSamplesMS() {

}

void SameSamplesMS::proposeNextSamples(const ParameterBlock::Block::sharedPtr_t block, const Sample &sample, vector<Sample> &outProposedSamples){

	RNG* rng = Parallel::mpiMgr().getPRNG();
	// Save the current RNG state
	StateRNG stategRNG;
	rng->saveState(stategRNG);

	// Use the shared RNG
	rng->loadState(sharedState);

	// Generate the samples (one for each Proc in the multi-proposal)

	size_t nProc = Parallel::mcmcMgr().getNProposal();
	for(size_t iS=0; iS<nProc; ++iS){
		Sample tmpSample(sample);
		block->proposeMove(tmpSample);
		outProposedSamples.push_back(tmpSample);
	}

	// Save the state of the shared RNG
	rng->saveState(sharedState);

	// Set the RNG to its previous state
	rng->loadState(stategRNG);
}

} // namespace Strategies
} // namespace Sampler
