//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * DefaultMS.h
 *
 *  Created on: 21 jan. 2014
 *      Author: meyerx
 */

#ifndef DEFAULTMS_H_
#define DEFAULTMS_H_

#include "ModifierStrategy.h"

namespace Sampler {
namespace Strategies {

class DefaultMS: public ModifierStrategy {
public:
	DefaultMS(const Model &aModel, const uint aNSample=1);
	~DefaultMS();

	void proposeNextSamples(const ParameterBlock::Block::sharedPtr_t block, const Sample &sample, vector<Sample> &outProposedSamples);

	void setNSample(const uint aNSample);
private:
	uint nSample;
};

} // namespace Strategies
} // namespace Sampler


#endif /* DEFAULTMS_H_ */
