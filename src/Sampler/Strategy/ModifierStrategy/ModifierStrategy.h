//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ModifierStrategy.h
 *
 *  Created on: 4 oct. 2013
 *      Author: meyerx
 */

#ifndef MODIFIERSTRATEGY_H_
#define MODIFIERSTRATEGY_H_

#include "ParameterBlock/Block.h"
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "Sampler/Samples/Samples.h"
#include "Model/Model.h"
//#include "Parallel/Parallel.h"

#include <boost/shared_ptr.hpp>

namespace Sampler {
namespace Strategies {

using StatisticalModel::Model;

class ModifierStrategy : private Uncopyable {
public:
	typedef boost::shared_ptr<ModifierStrategy> sharedPtr_t;

public:
	ModifierStrategy(const Model &aModel);
	virtual ~ModifierStrategy();

	virtual void proposeNextSamples(const ParameterBlock::Block::sharedPtr_t block, const Sample &sample, vector<Sample> &outProposedSamples) = 0;

	static sharedPtr_t createDefaultModifierStrategy(const Model &aModel);
	static sharedPtr_t createSameSamplesModifierStrategy(const Model &aModel);

protected:
	const Model &model;

};

} // namespace Strategies
} // namespace Sampler


#endif /* MODIFIERSTRATEGY_H_ */
