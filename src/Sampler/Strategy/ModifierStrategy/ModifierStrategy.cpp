//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ModifierStrategy.cpp
 *
 *  Created on: 4 oct. 2013
 *      Author: meyerx
 */

#include "ModifierStrategy.h"
#include "DefaultMS.h"
#include "SameSamplesMS.h"

namespace Sampler {
namespace Strategies {

ModifierStrategy::ModifierStrategy(const Model &aModel) : model(aModel) {
}

ModifierStrategy::~ModifierStrategy() {
}

ModifierStrategy::sharedPtr_t ModifierStrategy::createDefaultModifierStrategy(const Model &aModel) {
	return sharedPtr_t(new DefaultMS(aModel));
}

ModifierStrategy::sharedPtr_t ModifierStrategy::createSameSamplesModifierStrategy(const Model &aModel) {
	return sharedPtr_t(new SameSamplesMS(aModel));
}

} // namespace Strategies
} // namespace Sampler

