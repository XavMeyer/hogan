//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedBS.h
 *
 * @date Dec 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef OPTIMISEDBS_H_
#define OPTIMISEDBS_H_

#include "BlockSelector.h"
#include "Parallel/RNG/RNG.h"

#include <list>

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;

class OptimisedBS: public BlockSelector {
public:
	OptimisedBS(const Model &aModel, const uint aSeed, const size_t aNTimePerBlock);
	~OptimisedBS();

	const Block::sharedPtr_t selectBlock(Sample &sample);
	const vector<int> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

	size_t getSeed() const;

private:
	const size_t N_TIME_PER_BLOCK;

	std::list<size_t> iBlock, nSelected;

	int getNextBlock();

};

} // namespace Strategies
} // namespace Sampler


#endif /* OPTIMISEDBS_H_ */
