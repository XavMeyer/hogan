//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomBS.h
 *
 * @date Jan 9, 2014
 * @author meyerx
 * @brief
 */
#ifndef RANDOMBS_H_
#define RANDOMBS_H_

#include "BlockSelector.h"
#include "Parallel/RNG/RNG.h"

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;

class RandomBS: public BlockSelector {
public:
	RandomBS(const Model &aModel, const uint aSeed);
	~RandomBS();

	const Block::sharedPtr_t selectBlock(Sample &sample);
	const vector<int> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

	size_t getSeed() const;

private:

};

} // namespace Strategies
} // namespace Sampler


#endif /* RANDOMBS_H_ */
