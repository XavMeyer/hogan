//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleBS.cpp
 *
 * @date Jan 29, 2014
 * @author meyerx
 * @brief
 */
#include "SingleBS.h"

namespace Sampler {
namespace Strategies {

SingleBS::SingleBS(const Model &aModel, const int aBId) : BlockSelector(aModel), bId(aBId) {
}

SingleBS::~SingleBS() {

}

const Block::sharedPtr_t SingleBS::selectBlock(Sample &sample) {
	return model.getBlocks().getBlock(bId);
}

const vector<int> SingleBS::selectBlocks(Sample & sample) {
	uint nBlocks = Parallel::mcmcMgr().getNProposal();;
	vector<int> blockIDs(nBlocks, bId);
	return blockIDs;
}

void SingleBS::rewind(const int acceptId) {
	return;
}

} // namespace Strategies
} // namespace Sampler

