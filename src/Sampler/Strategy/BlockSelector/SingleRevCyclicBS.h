//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SingleRevCyclicPS.h
 *
 *  Created on: 24 sept. 2013
 *      Author: meyerx
 */

#ifndef SINGLEREVCYCLICPS_H_
#define SINGLEREVCYCLICPS_H_

#include "BlockSelector.h"

namespace Sampler {
namespace Strategies {

using namespace ParameterBlock;

class SingleRevCyclicBS: public BlockSelector {
public:
	SingleRevCyclicBS(const Model &aModel);
	virtual ~SingleRevCyclicBS();

	const Block::sharedPtr_t selectBlock(Sample &sample);
	const vector<int> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

private:

	enum direction_t {UPWARD, DOWNWARD};
	uint nextBlock;
	direction_t direction;
};

} // namespace Strategies
} // namespace Sampler


#endif /* SINGLEREVCYCLICPS_H_ */
