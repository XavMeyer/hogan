//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParameterSelector.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKSELECTOR_H_
#define BLOCKSELECTOR_H_

#include "Model/Model.h"
#include "ParameterBlock/Block.h"

#include <boost/shared_ptr.hpp>

#include <vector>
using namespace std;

namespace Sampler {
namespace Strategies {

using StatisticalModel::Model;

class BlockSelector : private Uncopyable {
public:
	typedef boost::shared_ptr<BlockSelector> sharedPtr_t;

public:
	BlockSelector(const Model &aModel);
	virtual ~BlockSelector();

	virtual const ParameterBlock::Block::sharedPtr_t selectBlock(Sample &sample) = 0;
	virtual const vector<int> selectBlocks(Sample & sample) = 0;
	virtual void rewind(const int acceptId) = 0;

	RNG* getRNG() const { return m_rng; }

	static sharedPtr_t createSingleCyclic(const Model &aModel);
	static sharedPtr_t createSingleRevCyclic(const Model &aModel);
	static sharedPtr_t createRandom(const Model &aModel, const uint aSeed);
	static sharedPtr_t createOptimised(const Model &aModel, const uint aSeed, const size_t aNTimePerBlock);
	static sharedPtr_t createRandomTreeInference(const Model &aModel, const double aFreqTreeMove, const uint aSeed);
	static sharedPtr_t createOptimisedTreeInference(const Model &aModel, const double aFreqTreeMove, const uint aSeed);
	static sharedPtr_t createSingle(const Model &aModel, const uint aBId);
	static sharedPtr_t createSingleParam(const Model &aModel, const uint aPId, const uint aBId);

protected:
	const Model &model;
	RNG *m_rng;
};

} // namespace Strategies
} // namespace Sampler


#endif /* BLOCKSELECTOR_H_ */
