//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedTIBS.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "OptimisedTIBS.h"

namespace Sampler {
namespace Strategies {

const double OptimisedTIBS::RATE_FREQUENCY = 0.02;

OptimisedTIBS::OptimisedTIBS(const Model &aModel, const double aFreqTreeMove,
							 const uint aSeed) :
		BlockSelector(aModel), MOVE_FREQUENCY(aFreqTreeMove),
		BRANCH_FREQUENCY(1-(RATE_FREQUENCY+MOVE_FREQUENCY)),
		N_TIME_PER_BLOCK(floor(Parallel::mcmcMgr().getNProposal()*1.5)),
		nBlocks(Parallel::mcmcMgr().getNProposal()) {
	m_rng = RNG::createSitmo11RNG(aSeed);
	init();

	iBlock.push_back(getNextBlock());
	nSelected.push_back(0);
}

OptimisedTIBS::~OptimisedTIBS() {
}

void OptimisedTIBS::init() {

	std::vector<double> avgPPB(3, 0.);
	std::vector<int> rateBlocks, branchBlocks, moveBlocks;

	std::vector< std::vector<size_t> > bPool(3);

	for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
		const ParameterBlock::Block::sharedPtr_t block = model.getBlocks().getBlock(iB);
		size_t iParam = block->getPIndices().front();
		int type = model.getParams().getType(iParam);
		avgPPB[type] += block->size();
		bPool[type].push_back(iB);
		if(type == 0) {
			moveBlocks.push_back(iB);
		} else if(type == 1) {
			rateBlocks.push_back(iB);
		} else if(type == 2) {
			branchBlocks.push_back(iB);
		} else {
			std::cerr << "Parameter type (" << type << ") is unknown." << std::endl;
			assert(false);
		}
	}

	double sumBranchPPB = avgPPB[2];

	if(!moveBlocks.empty()) avgPPB[0] /= (double)moveBlocks.size();
	avgPPB[1] /= (double)rateBlocks.size();
	avgPPB[2] /= (double)branchBlocks.size();

	if(!moveBlocks.empty()) moveFreq = MOVE_FREQUENCY;
	rateFreq = RATE_FREQUENCY;
	if(Parallel::mcmcMgr().isActiveMC3()) {
		int myChainMC3 = Parallel::mcmcMgr().getMyChainMC3();
		if(myChainMC3 > 0) rateFreq = RATE_FREQUENCY/2.;
		else rateFreq = RATE_FREQUENCY;
	} else {
		rateFreq = RATE_FREQUENCY;
	}
	//branchFreq = BRANCH_FREQUENCY/pow(avgPPB[2], 0.1); // FIXME Changed
	branchFreq = std::max(BRANCH_FREQUENCY/10., BRANCH_FREQUENCY/pow(avgPPB[2], avgPPB[2]/sumBranchPPB)); // FIXME Changed


	// Rate freq is always equals to RATE_FREQUENCY
	// Then we have to "distribute" the rest
	double sum = branchFreq;
	if(!moveBlocks.empty()) sum += moveFreq;

	branchFreq = (1-rateFreq)*branchFreq/sum;
	if(!moveBlocks.empty()) {
		moveFreq = (1-rateFreq)*moveFreq/sum;
	} else {
		moveFreq = 0;
	}

	bPoolTF.resize(3);
	bPoolTF[0] = moveFreq; // Move
	bPoolTF[1] = rateFreq; // Rate
	bPoolTF[2] = branchFreq; // Branch
	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "**************************************" << std::endl;
		std::cout << "<Block selection frequency> move = " << moveFreq << " - rate = " << rateFreq << " - branch = " << branchFreq << std::endl;
	}

}

int OptimisedTIBS::getNextBlock() {


	// Reform block pools
	std::vector< std::vector<size_t> > bPool(3);
	for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
		const ParameterBlock::Block::sharedPtr_t block = model.getBlocks().getBlock(iB);
		size_t iParam = block->getPIndices().front();
		int type = model.getParams().getType(iParam);
		bPool[type].push_back(iB);
	}

	/*
	double sel = 0;*
	// Count weights
	std::vector<double> weightPerPool(bPool.size(), 0.);
	std::vector< std::vector<double> > weightperBlock(bPool.size());
	for(size_t iP=0; iP<bPool.size(); ++iP) {
		// Get counts
		weightperBlock[iP].resize(bPool[iP].size());
		for(size_t iB=0; iB<bPool[iP].size(); ++iB) {
			weightPerPool[iP] += model.getBlocks().getBlock(bPool[iP][iB])->getNSampled();
			weightperBlock[iP][iB] = model.getBlocks().getBlock(bPool[iP][iB])->getNSampled();
			sel += model.getBlocks().getBlock(bPool[iP][iB])->getNSampled();
		}
	}

	// Select some if needed
	if(sel > 0) {
		// Normalize
		for(size_t iP=0; iP<bPool.size(); ++iP) {
			if(weightPerPool[iP] > 0) {
				for(size_t iB=0; iB<bPool[iP].size(); ++iB) {
					weightperBlock[iP][iB] /= weightPerPool[iP];
				}
			} else { // Pool not yet selected : default value
				double expectedFreq = 1./(double)bPool[iP].size();
				for(size_t iB=0; iB<bPool[iP].size(); ++iB) {
					weightperBlock[iP][iB] = expectedFreq;
				}
			}
			weightPerPool[iP] /= sel;
		}

		// Apply correction
		for(size_t iP=0; iP<bPool.size(); ++iP) {
			double expectedFreq = 1./(double)bPool[iP].size();
			for(size_t iB=0; iB<bPool[iP].size(); ++iB) {
				weightperBlock[iP][iB] = 2.*expectedFreq - weightperBlock[iP][iB];
			}
			weightPerPool[iP] =  std::max(0., 2.*bPoolTF[iP] - weightPerPool[iP]);
		}
	} else { // First iteration
		for(size_t iP=0; iP<bPool.size(); ++iP) {
			double expectedFreq = 1./(double)bPool[iP].size();
			for(size_t iB=0; iB<bPool[iP].size(); ++iB) {
				weightperBlock[iP][iB] = expectedFreq;
			}
			weightPerPool[iP] = bPoolTF[iP];
		}
	}

	// Select the block
	size_t iPool = m_rng->drawFrom(weightPerPool);
	int rndIdx = m_rng->drawFrom(weightperBlock[iPool]);
	int iBlock = bPool[iPool][rndIdx];*/

	// Select the block
	size_t iPool = m_rng->drawFrom(bPoolTF);
	int rndIdx = m_rng->genUniformInt(0, bPool[iPool].size()-1);
	int iBlock = bPool[iPool][rndIdx];

	return iBlock;
}

const ParameterBlock::Block::sharedPtr_t OptimisedTIBS::selectBlock(Sample &sample) {
	if(model.getBlocks().size() < 1) throw out_of_range("Blocks");
	if(model.getBlocks().size() == 1) return model.getBlocks().getBlock(0);

	return model.getBlocks().getBlock(iBlock.front());
}

const vector<int> OptimisedTIBS::selectBlocks(Sample & sample) {
	vector<int> blockIDs;

	// Check that number of block has not changed.
	for(std::list<size_t>::iterator itB=iBlock.begin(); itB != iBlock.end(); ++itB) {
		if(*itB >= model.getBlocks().size()) {
			iBlock.clear();
			nSelected.clear();
			iBlock.push_back(getNextBlock());
			nSelected.push_back(0);
			break;
		}
	}


	if(model.getBlocks().size() == 1) {
		blockIDs.assign(nBlocks, 0);
	} else {
		// Select next blocks
		size_t nBlocks = Parallel::mcmcMgr().getNProposal();
		size_t nCurBlock = std::min(nBlocks, N_TIME_PER_BLOCK-nSelected.front());
		for(size_t i=0; i<nCurBlock; ++i) {
			blockIDs.push_back(iBlock.front());
		}

		if(nCurBlock < nBlocks) {
			if(blockIDs.size() < 2) {
				iBlock.push_back(getNextBlock());
				nSelected.push_back(0);
			}

			for(size_t i=nCurBlock; i<nBlocks; ++i) {
				blockIDs.push_back(iBlock.back());
			}
		}
	}

	return blockIDs;
}

void OptimisedTIBS::rewind(const int acceptId) {
	size_t nKept = 0;
	if(acceptId < 0) {
		nKept = nBlocks;
	} else {
		nKept = 1+acceptId;
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[" << Parallel::mpiMgr().getRank() << "]B nStep = " << acceptId << " | nKept = " << nKept << std::endl;
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
	}*/

	// update selected count
	if(nSelected.front() + nKept >= N_TIME_PER_BLOCK) {
		nKept -= N_TIME_PER_BLOCK-nSelected.front();
		iBlock.pop_front();
		nSelected.pop_front();

		if(iBlock.empty()) {
			iBlock.push_back(getNextBlock());
			nSelected.push_back(0);
		} else {
			nSelected.front() += nKept;
		}
	} else {
		nSelected.front() += nKept;
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
		getchar();
	}*/
}

size_t OptimisedTIBS::getSeed() const {
	return m_rng->getSeed();
}

} /* namespace Strategies */
} /* namespace Sampler */
