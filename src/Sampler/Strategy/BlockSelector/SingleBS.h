//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleBS.h
 *
 * @date Jan 29, 2014
 * @author meyerx
 * @brief
 */
#ifndef SINGLEBS_H_
#define SINGLEBS_H_

#include "BlockSelector.h"

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;

class SingleBS: public BlockSelector {
public:
	SingleBS(const Model &aModel, const int aBId);
	virtual ~SingleBS();

	const Block::sharedPtr_t selectBlock(Sample &sample);
	const vector<int> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

private:
	int bId;
};

} // namespace Strategies
} // namespace Sampler


#endif /* SINGLEBS_H_ */
