//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedBS.cpp
 *
 * @date Dec 13, 2015
 * @author meyerx
 * @brief
 */
#include "OptimisedBS.h"

namespace Sampler {
namespace Strategies {

OptimisedBS::OptimisedBS(const Model &aModel, const uint aSeed, const size_t aNTimePerBlock) :
		BlockSelector(aModel), N_TIME_PER_BLOCK(aNTimePerBlock) {
	m_rng = RNG::createSitmo11RNG(aSeed);

	iBlock.push_back(getNextBlock());
	nSelected.push_back(0);

	assert((int)N_TIME_PER_BLOCK >= Parallel::mcmcMgr().getNProposal());
}

OptimisedBS::~OptimisedBS() {
}


int OptimisedBS::getNextBlock() {
	const vector<double>& expectedFreqs = model.getBlocks().frequencies();
	//const double sumEFreqs = model.getBlocks().frequenciesSum();

	/*double sel = 0;
	std::vector<double> observedFreqs(model.getBlocks().size(), 0.);
	for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
		// Get counts
		observedFreqs[iB] = model.getBlocks().getBlock(iB)->getNSampled();
		sel += model.getBlocks().getBlock(iB)->getNSampled();
	}

	if(sel > 0) {
		// Normalize and apply correction
		for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
			observedFreqs[iB] /= sel;
			// Expected freqs may not sum to 1. thus we normalize it using sumEFreqs
			observedFreqs[iB] = 2.*expectedFreqs[iB]/sumEFreqs - observedFreqs[iB];
		}
	} else {
		// Normalize and apply correction
		for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
			// Expected freqs may not sum to 1. thus we normalize it using sumEFreqs
			observedFreqs[iB] = expectedFreqs[iB];
		}
	}

	// Select the block
	std::vector<int> iBlocks;
	int iBlock = m_rng->drawFrom(observedFreqs);*/
	int iBlock = m_rng->drawFrom(expectedFreqs);

	return iBlock;
}



const Block::sharedPtr_t OptimisedBS::selectBlock(Sample &sample) {
	if(model.getBlocks().size() < 1) throw out_of_range("Blocks");
	if(model.getBlocks().size() == 1) return model.getBlocks().getBlock(0);

	return model.getBlocks().getBlock(getNextBlock());
}

const vector<int> OptimisedBS::selectBlocks(Sample & sample) {
	uint nBlocks = Parallel::mcmcMgr().getNProposal();
	vector<int> blockIDs;

	// Check that number of block has not changed.
	for(std::list<size_t>::iterator itB=iBlock.begin(); itB != iBlock.end(); ++itB) {
		if(*itB >= model.getBlocks().size()) {
			iBlock.clear();
			nSelected.clear();
			iBlock.push_back(getNextBlock());
			nSelected.push_back(0);
			break;
		}
	}

	// Get blocks id
	if(model.getBlocks().size() == 1) {
		blockIDs.assign(nBlocks, 0);
	} else {

		// Select next blocks
		size_t nBlocks = Parallel::mcmcMgr().getNProposal();
		size_t nCurBlock = std::min(nBlocks, N_TIME_PER_BLOCK-nSelected.front());
		for(size_t i=0; i<nCurBlock; ++i) {
			blockIDs.push_back(iBlock.front());
		}

		if(nCurBlock < nBlocks) {
			if(blockIDs.size() < 2) {
				iBlock.push_back(getNextBlock());
				nSelected.push_back(0);
			}

			for(size_t i=nCurBlock; i<nBlocks; ++i) {
				blockIDs.push_back(iBlock.back());
			}
		}

		/*if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[" << Parallel::mpiMgr().getRank() << "]Blocks : ";
			for(size_t i=0; i<nBlocks; ++i) {
				std::cout << blockIDs[i] << "\t";
			}
			std::cout << std::endl;
		}*/

		//blockIDs = getNextBlocks();
	}
	return blockIDs;
}

void OptimisedBS::rewind(const int acceptId) {
	uint nBlocks = Parallel::mcmcMgr().getNProposal();
	size_t nKept = 0;
	if(acceptId < 0) {
		nKept = nBlocks;
	} else {
		nKept = 1+acceptId;
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[" << Parallel::mpiMgr().getRank() << "]B nStep = " << acceptId << " | nKept = " << nKept << std::endl;
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
	}*/

	// update selected count
	if(nSelected.front() + nKept >= N_TIME_PER_BLOCK) {
		nKept -= N_TIME_PER_BLOCK-nSelected.front();
		iBlock.pop_front();
		nSelected.pop_front();

		if(iBlock.empty()) {
			iBlock.push_back(getNextBlock());
			nSelected.push_back(0);
		} else {
			nSelected.front() += nKept;
		}
	} else {
		nSelected.front() += nKept;
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
		getchar();
	}*/
}

size_t OptimisedBS::getSeed() const {
	return m_rng->getSeed();
}

} // namespace Strategies
} // namespace Sampler

