//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedTIBS.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef OPTIMISEDTIBS_H_
#define OPTIMISEDTIBS_H_

#include "BlockSelector.h"

namespace Sampler {
namespace Strategies {

class OptimisedTIBS: public BlockSelector {
public:
	OptimisedTIBS(const Model &aModel, const double aFreqTreeMove,
				  const uint aSeed);
	~OptimisedTIBS();

	const ParameterBlock::Block::sharedPtr_t selectBlock(Sample &sample);
	const vector<int> selectBlocks(Sample & sample);
	void rewind(const int acceptId);

	size_t getSeed() const;

private:

	static const double RATE_FREQUENCY ;
	const double MOVE_FREQUENCY, BRANCH_FREQUENCY;
	const size_t N_TIME_PER_BLOCK;

	double rateFreq, branchFreq, moveFreq;
	size_t nBlocks;

	std::list<size_t> iBlock, nSelected;
	std::vector<double> bPoolTF;

	void init();

	int getNextBlock();
};

} /* namespace Strategies */
} /* namespace Sampler */

#endif /* OPTIMISEDTIBS_H_ */
