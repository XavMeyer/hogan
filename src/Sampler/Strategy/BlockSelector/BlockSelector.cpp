//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParameterSelector.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "SingleCyclicBS.h"
#include "SingleRevCyclicBS.h"
#include "RandomBS.h"
#include "OptimisedBS.h"
#include "RandomTreeInferenceBS.h"
#include "OptimisedTIBS.h"
#include "SingleBS.h"
#include "SingleParamBS.h"
#include "BlockSelector.h"


namespace Sampler {
namespace Strategies {

BlockSelector::BlockSelector(const Model &aModel) : model(aModel) {
	m_rng = NULL;
}

BlockSelector::~BlockSelector() {

}


BlockSelector::sharedPtr_t BlockSelector::createSingleCyclic(const Model &aModel){
	return sharedPtr_t(new SingleCyclicBS(aModel));
}

BlockSelector::sharedPtr_t BlockSelector::createSingleRevCyclic(const Model &aModel){
	return sharedPtr_t(new SingleRevCyclicBS(aModel));
}

BlockSelector::sharedPtr_t BlockSelector::createRandom(const Model &aModel, const uint aSeed){
	return sharedPtr_t(new RandomBS(aModel, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createOptimised(const Model &aModel, const uint aSeed, const size_t aNTimePerBlock){
	return sharedPtr_t(new OptimisedBS(aModel, aSeed, aNTimePerBlock));
}

BlockSelector::sharedPtr_t BlockSelector::createRandomTreeInference(const Model &aModel, const double aFreqTreeMove, const uint aSeed){
	return sharedPtr_t(new RandomTreeInferenceBS(aModel, aFreqTreeMove, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createOptimisedTreeInference(const Model &aModel, const double aFreqTreeMove, const uint aSeed){
	return sharedPtr_t(new OptimisedTIBS(aModel, aFreqTreeMove, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createSingle(const Model &aModel, const uint aBId){
	return sharedPtr_t(new SingleBS(aModel, aBId));
}

BlockSelector::sharedPtr_t BlockSelector::createSingleParam(const Model &aModel, const uint aPId, const uint aBId){
	return sharedPtr_t(new SingleParamBS(aModel, aPId, aBId));
}

} // namespace Strategies
} // namespace Sampler

