//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomBS.cpp
 *
 * @date Jan 9, 2014
 * @author meyerx
 * @brief
 */
#include "RandomBS.h"

namespace Sampler {
namespace Strategies {

RandomBS::RandomBS(const Model &aModel, const uint aSeed) : BlockSelector(aModel) {
	m_rng = RNG::createSitmo11RNG(aSeed);
}

RandomBS::~RandomBS() {
}

const Block::sharedPtr_t RandomBS::selectBlock(Sample &sample) {
	if(model.getBlocks().size() < 1) throw out_of_range("Blocks");
	if(model.getBlocks().size() == 1) return model.getBlocks().getBlock(0);

	uint iBlock = 0;
	double sel = model.getBlocks().frequenciesSum()*m_rng->genUniformDbl();
	const vector<double>& freqs = model.getBlocks().frequencies();

	for(size_t iS=0; iS<freqs.size(); ++iS){
		if(sel >= freqs[iS]){
			sel -= freqs[iS];
		} else {
			iBlock = iS;
			break;
		}
	}

	return model.getBlocks().getBlock(iBlock);
}

const vector<int> RandomBS::selectBlocks(Sample & sample) {
	uint nBlocks = Parallel::mcmcMgr().getNProposal();
	vector<int> blockIDs;

	if(model.getBlocks().size() == 1) {
		blockIDs.assign(nBlocks, 0);
	} else {
		for(uint i=0; i<nBlocks; ++i){
			uint iBlock = 0;
			double sel = model.getBlocks().frequenciesSum()*m_rng->genUniformDbl();
			const vector<double>& freqs = model.getBlocks().frequencies();

			for(size_t iS=0; iS<freqs.size(); ++iS){
				if(sel >= freqs[iS]){
					sel -= freqs[iS];
				} else {
					iBlock = iS;
					break;
				}
			}
			blockIDs.push_back(iBlock);
		}
	}
	return blockIDs;
}

void RandomBS::rewind(const int acceptId) {
	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	uint nSteps = 0;
	if(acceptId >= 0) {
		nSteps = nBlock-(acceptId+1);
		m_rng->rewind(nSteps);
	}
}

size_t RandomBS::getSeed() const {
	return m_rng->getSeed();
}

} // namespace Strategies
} // namespace Sampler

