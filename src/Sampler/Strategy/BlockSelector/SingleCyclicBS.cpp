//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SingleCyclicPS.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "SingleCyclicBS.h"

namespace Sampler {
namespace Strategies {

SingleCyclicBS::SingleCyclicBS(const Model &aModel) : BlockSelector(aModel), nextBlock(0) {
}

SingleCyclicBS::~SingleCyclicBS() {
}

const Block::sharedPtr_t SingleCyclicBS::selectBlock(Sample &sample) {
	if(model.getBlocks().size() < 1) throw out_of_range("Blocks");
	if(model.getBlocks().size() == 1) return model.getBlocks().getBlock(0);

	// check that model.getBlocks().size() has not changed
	if((size_t)nextBlock >= model.getBlocks().size()) {
		nextBlock = model.getBlocks().size()-1;
	}

	int iBlock = nextBlock;
	nextBlock = (nextBlock+1) % model.getBlocks().size();

	return model.getBlocks().getBlock(iBlock);
}

const vector<int> SingleCyclicBS::selectBlocks(Sample & sample) {
	uint nBlocks = Parallel::mcmcMgr().getNProposal();
	vector<int> blockIDs;

	// check that model.getBlocks().size() has not changed
	if((size_t)nextBlock >= model.getBlocks().size()) {
		nextBlock = model.getBlocks().size()-1;
	}

	for(uint i=0; i<nBlocks; ++i){
		int iBlock = nextBlock;
		nextBlock = (nextBlock+1) % model.getBlocks().size();
		blockIDs.push_back(iBlock);
	}

	return blockIDs;
}

void SingleCyclicBS::rewind(const int acceptId) {
	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	uint nSteps = 0;
	if(acceptId >= 0) {
		nSteps = nBlock-(acceptId+1);
		nextBlock = (int)(10*model.getBlocks().size() + nextBlock - nSteps) % (int)model.getBlocks().size();
	}
}

} // namespace Strategies
} // namespace Sampler

