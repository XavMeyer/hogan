//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomTreeInferenceBS.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "RandomTreeInferenceBS.h"

namespace Sampler {
namespace Strategies {

const double RandomTreeInferenceBS::RATE_FREQUENCY = 0.02;

RandomTreeInferenceBS::RandomTreeInferenceBS(const Model &aModel, const double aFreqTreeMove, const uint aSeed) :
		BlockSelector(aModel), MOVE_FREQUENCY(aFreqTreeMove), BRANCH_FREQUENCY(1-(RATE_FREQUENCY+MOVE_FREQUENCY)),
		nBlocks(Parallel::mcmcMgr().getNProposal()), avgPPB(3, 0.), nRand(nBlocks,0), nSeq(nBlocks,0) {
	m_rng = RNG::createSitmo11RNG(aSeed);
	init();
}

RandomTreeInferenceBS::~RandomTreeInferenceBS() {
}

void RandomTreeInferenceBS::init() {

	seqBranch = 0;

	for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
		const ParameterBlock::Block::sharedPtr_t block = model.getBlocks().getBlock(iB);
		size_t iParam = block->getPIndices().front();
		int type = model.getParams().getType(iParam);
		avgPPB[type] += block->size();
		if(type == 0) {
			moveBlocks.push_back(iB);
		} else if(type == 1) {
			rateBlocks.push_back(iB);
		} else if(type == 2) {
			branchBlocks.push_back(iB);
		} else {
			std::cerr << "Parameter type (" << type << ") is unknown." << std::endl;
			assert(false);
		}
	}

	if(!moveBlocks.empty()) avgPPB[0] /= (double)moveBlocks.size();
	avgPPB[1] /= (double)rateBlocks.size();
	avgPPB[2] /= (double)branchBlocks.size();

	//if(!moveBlocks.empty()) moveFreq = MOVE_FREQUENCY/avgPPB[0];
	//rateFreq = RATE_FREQUENCY/avgPPB[1];
	//branchFreq = BRANCH_FREQUENCY/avgPPB[2];
	if(!moveBlocks.empty()) moveFreq = MOVE_FREQUENCY;
	rateFreq = RATE_FREQUENCY;
	branchFreq = BRANCH_FREQUENCY/pow(avgPPB[2], 0.2);


	// Rate freq is always equals to RATE_FREQUENCY
	// Then we have to "distribute" the rest
	double sum = branchFreq;
	if(!moveBlocks.empty()) sum += moveFreq;

	branchFreq = (1-rateFreq)*branchFreq/sum;
	if(!moveBlocks.empty()) {
		moveFreq = (1-rateFreq)*moveFreq/sum;
		//moveFreq /= sum;
	} else {
		moveFreq = 0;
	}

	//std::cout << rateFreq << " :: " << branchFreq << " :: " << moveFreq << std::endl;
}

const ParameterBlock::Block::sharedPtr_t RandomTreeInferenceBS::selectBlock(Sample &sample) {
	if(model.getBlocks().size() < 1) throw out_of_range("Blocks");
	if(model.getBlocks().size() == 1) return model.getBlocks().getBlock(0);

	uint iBlock = 0;
	double selType = m_rng->genUniformDbl();
	if(selType < moveFreq) { // Move
		int rnd = m_rng->genUniformInt(0, moveBlocks.size()-1);
		iBlock = rateBlocks[rnd];
	} else if(selType < (moveFreq+rateFreq)) { // Rate
		int rnd = m_rng->genUniformInt(0, rateBlocks.size()-1);
		iBlock = rateBlocks[rnd];
	} else { // Branch
		iBlock = branchBlocks[seqBranch];
		seqBranch = (seqBranch+1) % branchBlocks.size();
	}

	return model.getBlocks().getBlock(iBlock);
}

const vector<int> RandomTreeInferenceBS::selectBlocks(Sample & sample) {

	// Refresh counts
	moveBlocks.clear();
	rateBlocks.clear();
	branchBlocks.clear();
	for(size_t iB=0; iB<model.getBlocks().size(); ++iB) {
		const ParameterBlock::Block::sharedPtr_t block = model.getBlocks().getBlock(iB);
		size_t iParam = block->getPIndices().front();
		int type = model.getParams().getType(iParam);
		if(type == 0) {
			moveBlocks.push_back(iB);
		} else if(type == 1) {
			rateBlocks.push_back(iB);
		} else if(type == 2) {
			branchBlocks.push_back(iB);
		} else {
			std::cerr << "Parameter type (" << type << ") is unknown." << std::endl;
			assert(false);
		}
	}


	vector<int> blockIDs;
	if(model.getBlocks().size() == 1) {
		blockIDs.assign(nBlocks, 0);
	} else {
		for(uint iB=0; iB<nBlocks; ++iB){
			uint iBlock = 0;
			double selType = m_rng->genUniformDbl();
			if(selType < moveFreq) { // Move
				int rnd = m_rng->genUniformInt(0, moveBlocks.size()-1);
				iBlock = moveBlocks[rnd];
				nRand[iB] = 2;
				nSeq[iB] = 0;
			} else if(selType < (moveFreq+rateFreq)) { // Rate
				int rnd = m_rng->genUniformInt(0, rateBlocks.size()-1);
				iBlock = rateBlocks[rnd];
				nRand[iB] = 2;
				nSeq[iB] = 0;
			} else { // Branch
				iBlock = branchBlocks[seqBranch % branchBlocks.size()];
				seqBranch = (seqBranch+1) % branchBlocks.size();
				nRand[iB] = 1;
				nSeq[iB] = 1;

			}
			blockIDs.push_back(iBlock);
		}
	}

	/*if(Parallel::mcmcMgr().checkRoleProposal(Parallel::MCMCManager::Manager)) {
		double maxTime = 0;
		double sumTime = 0;
		std::cout << "Blocks time : ";
		for(size_t iB=0 ; iB<nBlocks; ++iB) {
			std::cout << "[" << model.getBlocks().getBlock(blockIDs[iB]).getName() << "]" ;
			std::cout << std::scientific << model.getBlocks().getBlock(blockIDs[iB]).getTotalTime() << "\t";
			maxTime = std::max(maxTime, model.getBlocks().getBlock(blockIDs[iB]).getTotalTime());
			sumTime += model.getBlocks().getBlock(blockIDs[iB]).getTotalTime();
		}
		std::cout << std::endl;
		std::cout << "Max time : " << maxTime << "\t meanTime : " << sumTime/nBlocks << std::endl;
	}*/

	return blockIDs;
}

void RandomTreeInferenceBS::rewind(const int acceptId) {

	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	uint nSteps = 0;
	if(acceptId >= 0) {
		nSteps = nBlock-(acceptId+1);

		uint nRewRand=0, nRewSeq=0;
		for(size_t iR=nBlocks-1; iR>=nBlocks-nSteps; --iR){
			nRewRand += nRand[iR];
			nRewSeq += nSeq[iR];
		}

		m_rng->rewind(nRewRand);
		seqBranch = (branchBlocks.size()*nBlocks + seqBranch - nRewSeq) % branchBlocks.size();
	}
}

size_t RandomTreeInferenceBS::getSeed() const {
	return m_rng->getSeed();
}

} /* namespace Strategies */
} /* namespace Sampler */
