//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleParamBS.cpp
 *
 * @date Jan 29, 2014
 * @author meyerx
 * @brief
 */
#include "SingleParamBS.h"

namespace Sampler {
namespace Strategies {

SingleParamBS::SingleParamBS(const Model &aModel, const int aParId, const int aBId) : BlockSelector(aModel), parId(aParId), bId(aBId) {
	block->addParameter(model.getBlocks().getBlock(bId)->getPIndices()[aParId]);
	ParameterBlock::BlockModifier::sharedPtr_t bm = model.getBlocks().getBlock(bId)->getBlockModifier(aParId);
	block->setBlockModifier(0, bm);
}

SingleParamBS::~SingleParamBS() {

}

const Block::sharedPtr_t SingleParamBS::selectBlock(Sample &sample) {
	return block;
}

const vector<int> SingleParamBS::selectBlocks(Sample & sample) {
	vector<int> blockIDs;
	cerr << "Cannot be implemented (SingleParamBS::selectBlocks)" << endl;
	abort();
	return blockIDs;
}

void SingleParamBS::rewind(const int acceptId) {
	return;
}

} // namespace Strategies
} // namespace Sampler

