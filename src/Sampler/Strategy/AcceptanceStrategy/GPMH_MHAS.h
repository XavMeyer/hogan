//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GPMH_MHAS.h
 *
 * @date Sep 16, 2015
 * @author meyerx
 * @brief
 */
#ifndef GPMH_MHAS_H_
#define GPMH_MHAS_H_

#include "AcceptanceStrategy.h"

namespace Sampler {
namespace Strategies {

class GPMH_MHAS: public AcceptanceStrategy {
public:
	GPMH_MHAS(const size_t aN, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	~GPMH_MHAS();

	vector<double> getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const;
	vector<int> acceptanceCheck(const vector<double> &scores) const;
	string getName(char sep=' ') const;

private:

	const size_t N;
	double sumValLog(const double x, const double y) const;

};

} /* namespace Strategies */
} /* namespace Sampler */

#endif /* GPMH_MHAS_H_ */
