//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TI_MHAS.h
 *
 * @date 24 Feb. 2014
 * @author meyerx
 * @brief
 */

#ifndef TI_MHAS_H_
#define TI_MHAS_H_

#include "AcceptanceStrategy.h"

namespace Sampler {
namespace Strategies {

class TI_MHAS : public AcceptanceStrategy {
public:
	TI_MHAS(const double aTao, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	virtual ~TI_MHAS();

	vector<int> acceptanceCheck(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const;
	virtual string getName(char sep=' ') const;

	vector<double> getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const;
	vector<int> acceptanceCheck(const vector<double> &scores) const;

private:
	double tao;

	double getScore(const Sample &prevSample, const Block::sharedPtr_t block, const Sample &sample) const;

};
} // namespace Strategies
} // namespace Sampler


#endif /* TI_MHAS_H_ */
