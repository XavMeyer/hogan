//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseMHAS.h
 *
 * @date 21 feb. 2014
 * @author meyerx
 * @brief Metropolis Rosenbluth Teller
 */

#ifndef BASEMHAS_H_
#define BASEMHAS_H_

#include "AcceptanceStrategy.h"
#include <iostream>

namespace Sampler {
namespace Strategies {

class BaseMHAS : public AcceptanceStrategy {
public:
	static const double LAMBDA;
public:
	BaseMHAS(const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	~BaseMHAS();

	vector<double> getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const;
	vector<int> acceptanceCheck(const vector<double> &scores) const;
	string getName(char sep=' ') const;

private:
	double getScore(const Sample &prevSample, const Block::sharedPtr_t block, const Sample &sample) const;

};

} // namespace Strategies
} // namespace Sampler

#endif /* BASEMHAS_H_ */
