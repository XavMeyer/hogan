//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * AcceptanceStrategy.cpp
 *
 *  Created on: 4 oct. 2013
 *      Author: meyerx
 */

#include "AcceptanceStrategy.h"
#include "BaseMHAS.h"
#include "MC3_MHAS.h"
#include "TI_MHAS.h"
#include "GPMH_MHAS.h"


namespace Sampler {
namespace Strategies {

AcceptanceStrategy::AcceptanceStrategy(const ModifierStrategy::sharedPtr_t &aStrategies, const Model &aModel) : modifierStrategy(aStrategies), model(aModel), nAccept(0), nSample(0), nRound(0){
	invTemp = 1.0;
}

AcceptanceStrategy::~AcceptanceStrategy() {
}


double AcceptanceStrategy::getAcceptanceRatio() const {
	return (double)nAccept/nRound;
}

RatioMH AcceptanceStrategy::getRatioMH(const Sample &prevSample, const double prevQ, const Sample &sample, const double Q) const {
	RatioMH ratioMH(model, prevSample, prevQ, sample, Q);
	return ratioMH;
}

bool AcceptanceStrategy::singleAcceptanceMH(const Sample &prevSample, const Sample &sample, const double ratioQ) const {
	RatioMH ratioMH(model, prevSample, sample, ratioQ);
	return singleAcceptanceMH(ratioMH);
}

bool AcceptanceStrategy::singleAcceptanceMH(const Sample &prevSample, const double prevQ, const Sample &sample, const double Q) const {
	RatioMH ratioMH(model, prevSample, prevQ, sample, Q);
	return singleAcceptanceMH(ratioMH);
}

bool AcceptanceStrategy::singleAcceptanceMH(const RatioMH &ratioMH) const {
	double ratio = ratioMH.getRatio();
	if(model.getLikelihood()->isLog()) {
		if(ratio >= 0.) {
			return true;
		} else {
			double u = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			if(log(u) < ratio) {
				return true;
			}
		}
		return false;
	} else {
		if(ratio >= 1.0) {
			return true;
		} else {
			double u = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			if(u < ratio) {
				return true;
			}
		}
		return false;
	}
}

bool AcceptanceStrategy::singleAcceptanceMH(const double ratio) const {
	if(model.getLikelihood()->isLog()) {
		if(ratio >= 0.) {
			return true;
		} else {
			double u = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			if(log(u) < ratio) {
				return true;
			}
		}
		return false;
	} else {
		if(ratio >= 1.0) {
			return true;
		} else {
			double u = Parallel::mpiMgr().getPRNG()->genUniformDbl();
			if(u < ratio) {
				return true;
			}
		}
		return false;
	}
}

double AcceptanceStrategy::getRatioQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const {
	return block->getMoveProbabilityRatio(fromSample, toSample);
}

double AcceptanceStrategy::getQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const {
	return block->getMoveProbability(fromSample, toSample);
}

double AcceptanceStrategy::getLogQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const {
	return log(block->getMoveProbability(fromSample, toSample));
}

void AcceptanceStrategy::getQS(double &prevQ, double &Q, const Block::sharedPtr_t block, const Sample &prevSample, const Sample &sample) const {
	prevQ = getQ(block, sample, prevSample);
	Q = getQ(block, prevSample, sample);
}

void AcceptanceStrategy::setInvTemperature(double aInvTemp) {
	invTemp = aInvTemp;
}

double AcceptanceStrategy::getInvTemperature() const {
	return invTemp;
}

AcceptanceStrategy::sharedPtr_t AcceptanceStrategy::createDefaultAcceptanceStrategy(const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel){
	return sharedPtr_t(new BaseMHAS(aStrategies, aModel));
}

AcceptanceStrategy::sharedPtr_t AcceptanceStrategy::createBaseStrategy(const ModifierStrategy::sharedPtr_t  aStrategies, const Model &aModel){
	return sharedPtr_t(new BaseMHAS(aStrategies, aModel));
}

AcceptanceStrategy::sharedPtr_t AcceptanceStrategy::createMC3Strategy(const double aLambda, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel){
	return sharedPtr_t(new MC3_MHAS(aLambda, aStrategies, aModel));
}

AcceptanceStrategy::sharedPtr_t AcceptanceStrategy::createTIStrategy(const double aTao, const ModifierStrategy::sharedPtr_t  aStrategies, const Model &aModel){
	return sharedPtr_t(new TI_MHAS(aTao, aStrategies, aModel));
}

AcceptanceStrategy::sharedPtr_t AcceptanceStrategy::createGPMHStrategy(const size_t N, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel) {
	return sharedPtr_t(new GPMH_MHAS(N, aStrategies, aModel));
}


} // namespace Strategies
} // namespace Sampler

