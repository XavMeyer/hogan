//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * AcceptanceStrategy.h
 *
 *  Created on: 4 oct. 2013
 *      Author: meyerx
 */

#ifndef ACCEPTANCESTRATEGY_H_
#define ACCEPTANCESTRATEGY_H_

#include "Sampler/Samples/Samples.h"
#include "Parallel/Parallel.h"
#include "Sampler/Strategy/ModifierStrategy/ModifierStrategy.h"
#include "Utils/RatioMH.h"

#include <boost/shared_ptr.hpp>

namespace Sampler {
namespace Strategies {

using namespace ::ParameterBlock;
using ::StatisticalModel::Model;

class AcceptanceStrategy : private Uncopyable {
public:
	typedef boost::shared_ptr<AcceptanceStrategy> sharedPtr_t;

public:
	AcceptanceStrategy(const ModifierStrategy::sharedPtr_t &aStrategies, const Model &aModel);
	virtual ~AcceptanceStrategy();

	double getAcceptanceRatio() const;

	virtual vector<double> getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const = 0;
	virtual vector<int> acceptanceCheck(const vector<double> &scores) const = 0;
	virtual string getName(char sep=' ') const = 0;

	static sharedPtr_t createDefaultAcceptanceStrategy(const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	static sharedPtr_t createBaseStrategy(const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	static sharedPtr_t createMC3Strategy(const double aLambda, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	static sharedPtr_t createTIStrategy(const double aTao, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);
	static sharedPtr_t createGPMHStrategy(const size_t N, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel);

	void setInvTemperature(double aInvTemp);
	double getInvTemperature() const;

protected:

	const ModifierStrategy::sharedPtr_t &modifierStrategy;
	const Model &model;
	mutable int nAccept, nSample, nRound;
	double invTemp;

	RatioMH getRatioMH(const Sample &prevSample, const double prevQ, const Sample &sample, const double Q) const;
	bool singleAcceptanceMH(const Sample &prevSample, const Sample &sample, const double ratioQ) const;
	bool singleAcceptanceMH(const Sample &prevSample, const double prevQ, const Sample &sample, const double Q) const;
	bool singleAcceptanceMH(const RatioMH &ratioMH) const;
	bool singleAcceptanceMH(const double ratio) const;
	double getRatioQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const;
	double getQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const;
	double getLogQ(const Block::sharedPtr_t block, const Sample &fromSample, const Sample &toSample) const;
	void getQS(double &prevQ, double &Q, const Block::sharedPtr_t block, const Sample &prevSample, const Sample &sample) const;

};

} // namespace Strategies
} // namespace Sampler

#endif /* ACCEPTANCESTRATEGY_H_ */
