//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MC3_MHAS.cpp
 *
 * @date 24 Feb. 2014
 * @author meyerx
 * @brief
 */

#include "MC3_MHAS.h"

namespace Sampler {
namespace Strategies {


MC3_MHAS::MC3_MHAS(const double aLambda, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel) : AcceptanceStrategy(aStrategies, aModel){
	myPower = 1./(1.+ Parallel::mcmcMgr().getRankProposal()*aLambda);
}

MC3_MHAS::~MC3_MHAS() {
}

vector<int> MC3_MHAS::acceptanceCheck(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const {
	vector<int> accepted;

	cout << "This function should not be called : " << endl;
	cout << "vector<int> MC3_MHAS::acceptanceCheck(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const;" << endl;
	abort();
	return accepted;
}

string MC3_MHAS::getName(char sep) const {
	stringstream ss;
	ss << "MC3";
	return ss.str();
}

double MC3_MHAS::getScore(const Sample &prevSample, const Block::sharedPtr_t block, const Sample &sample) const {
	double prevQ, Q, num, denom, ratio;
	getQS(prevQ, Q, block, prevSample, sample);
	if(model.getLikelihood()->isLog()){
		num = myPower*sample.posterior + log(Q);
		denom = myPower*prevSample.posterior + log(prevQ);
		ratio = exp(num - denom);
	} else {
		num = pow(sample.posterior, myPower) * Q;
		denom = pow(prevSample.posterior, myPower) * prevQ;
		ratio = num / denom;
	}
	return std::min(1., ratio);
}

vector<double> MC3_MHAS::getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const {
	vector<double> scores;

	// Process normalizing value for random
	for(size_t iS=0; iS<sVec.size(); ++iS){
		scores.push_back(getScore(prevSample, block, sVec[iS]));
	}

	return scores;
}

vector<int> MC3_MHAS::acceptanceCheck(const vector<double> &scores) const {
	vector<double> scoresMod;
	vector<int> accepted;

	++nRound;
	nSample += scores.size();

	double sel = Parallel::mpiMgr().getPRNG()->genUniformDbl();
	if(sel < scores.front()) {
		accepted.push_back(0);
		++nAccept;
	}

	return accepted;
}
} // namespace Strategies
} // namespace Sampler

