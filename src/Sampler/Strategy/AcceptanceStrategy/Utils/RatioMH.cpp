//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RatioMH.cpp
 *
 * @date 16 oct. 2013
 * @author meyerx
 * @brief
 */

#include "RatioMH.h"

namespace Sampler {
namespace Strategies {


RatioMH::RatioMH() : num(0.), denom(1.), ratio(0.) {
}

RatioMH::RatioMH(const Model &model, const Sample &prevSample, const Sample &sample, const double ratioQ) {
	if(model.getLikelihood()->isLog()){
		num = sample.posterior + log(ratioQ);
		denom = prevSample.posterior;
		ratio = num - denom;
	} else {
		num = sample.posterior*ratioQ;
		denom = prevSample.posterior;
		ratio = num / denom;
	}
}

RatioMH::RatioMH(const Model &model, const Sample &prevSample, const double prevQ, const Sample &sample, const double Q) {
	if(model.getLikelihood()->isLog()){
		num = sample.posterior + log(Q);
		denom = prevSample.posterior + log(prevQ);
		ratio = num - denom;
	} else {
		num = sample.posterior*Q;
		denom = prevSample.posterior*prevQ;
		ratio = num / denom;
	}
}

RatioMH::~RatioMH() {
}

double RatioMH::getNum() const {
	return num;
}

double RatioMH::getDenom() const {
	return denom;
}

double RatioMH::getRatio() const {
	return ratio;
}

double RatioMH::getRawRatio(const Model &model) const {
	if(model.getLikelihood()->isLog()){
		return exp(ratio);
	} else {
		return ratio;
	}
}

void RatioMH::setNum(const Model &model, const Sample &sample, const double Q) {
	if(model.getLikelihood()->isLog()){
		num = sample.posterior + log(Q);
		ratio = num - denom;
	} else {
		num = sample.posterior*Q;
		ratio = num / denom;
	}
}

void RatioMH::setDenom(const Model &model, const Sample &prevSample, const double prevQ) {
	if(model.getLikelihood()->isLog()){
		denom = prevSample.posterior + log(prevQ);
		ratio = num - denom;
	} else {
		denom = prevSample.posterior*prevQ;
		ratio = num / denom;
	}

}

string RatioMH::toString() const {
	stringstream ss;
	ss << "denom=" << denom << " - num=" << num << " - ratio=" << ratio;
	return ss.str();
}

} // namespace Strategies
} // namespace Sampler

