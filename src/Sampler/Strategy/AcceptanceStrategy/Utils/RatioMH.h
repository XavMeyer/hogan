//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RatioMH.h
 *
 * @date 16 oct. 2013
 * @author meyerx
 * @brief
 */

#ifndef RATIOMH_H_
#define RATIOMH_H_

#include "Model/Model.h"

namespace Sampler {
namespace Strategies {

using StatisticalModel::Model;

class RatioMH {
public:
	RatioMH();
	RatioMH(const Model &model, const Sample &prevSample, const Sample &sample, const double ratioQ);
	RatioMH(const Model &model, const Sample &prevSample, const double prevQ, const Sample &sample, const double Q);
	~RatioMH();

	double getNum() const;
	double getDenom() const;
	double getRatio() const;
	double getRawRatio(const Model &model) const;

	void setNum(const Model &model, const Sample &sample, const double Q);
	void setDenom(const Model &model, const Sample &prevSample, const double prevQ);

	string toString() const;

private:
	double num, denom, ratio;
};


} // namespace Strategies
} // namespace Sampler



#endif /* RATIOMH_H_ */
