//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseMHAS.cpp
 *
 * @date 21 feb. 2014
 * @author meyerx
 * @brief
 */

#include "BaseMHAS.h"

namespace Sampler {
namespace Strategies {

const double BaseMHAS::LAMBDA = 0.07;

BaseMHAS::BaseMHAS(const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel) : AcceptanceStrategy(aStrategies, aModel){
	if(Parallel::mcmcMgr().isActiveMC3()) {

		int myChainMC3 = Parallel::mcmcMgr().getMyChainMC3();
		//std::cout << Parallel::mpiMgr().getRank() << " | My chain : " << myChainMC3 << std::endl;
		invTemp = 1./(1.+ myChainMC3*LAMBDA);
		//std::cout << "[" << Parallel::mpiMgr().getRank() << "]MyPower = " << myPower << std::endl;
	} else {
		invTemp = 1.;
	}
}

BaseMHAS::~BaseMHAS() {
}

string BaseMHAS::getName(char sep) const {
	stringstream ss;
	ss << "MRT";
	return ss.str();
}

double BaseMHAS::getScore(const Sample &prevSample, const Block::sharedPtr_t block, const Sample &sample) const {
	double ratioQ = getRatioQ(block, prevSample, sample);

	if(invTemp == 1.0) {
		if(model.getLikelihood()->isLog()){
			return exp(sample.posterior+log(ratioQ)-prevSample.posterior);
		} else {
			return sample.posterior*ratioQ/prevSample.posterior;
		}
	} else {
		if(model.getLikelihood()->isLog()){
			double num = invTemp*sample.posterior + log(ratioQ);
			double denom = invTemp*prevSample.posterior;
			double ratio = exp(num - denom);
			return ratio;
		} else {
			double num = pow(sample.posterior, invTemp) * ratioQ;
			double denom = pow(prevSample.posterior, invTemp);
			double ratio = num / denom;
			return ratio;
		}
	}
}

vector<double> BaseMHAS::getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const {
	vector<double> scores;

	// Process normalizing value for random
	for(size_t iS=0; iS<sVec.size(); ++iS){
		scores.push_back(getScore(prevSample, block, sVec[iS]));
		if(scores.back() < 0 || scores.back() != scores.back() || isnan(scores.back())){
			scores.back() = 0.;
		}
	}

	return scores;
}

vector<int> BaseMHAS::acceptanceCheck(const vector<double> &scores) const {
	vector<int> accepted;

	nSample += scores.size();


	for(size_t iS=0; iS<scores.size(); ++iS){
		++nRound;
		double score = std::min(1.0, scores[iS]);
		double sel = Parallel::mpiMgr().getPRNG()->genUniformDbl();

		if(sel < score) {
			++nAccept;
			accepted.push_back(iS);

			break;
		} else {
			accepted.push_back(-1);
		}
	}

	return accepted;
}

} // namespace Strategies
} // namespace Sampler

