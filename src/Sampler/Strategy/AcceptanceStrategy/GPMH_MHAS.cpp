//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GPMH_MHAS.cpp
 *
 * @date Sep 16, 2015
 * @author meyerx
 * @brief
 */
#include "GPMH_MHAS.h"

namespace Sampler {
namespace Strategies {

GPMH_MHAS::GPMH_MHAS(const size_t aN, const ModifierStrategy::sharedPtr_t aStrategies, const Model &aModel) :
		AcceptanceStrategy(aStrategies, aModel), N(aN) {

}

GPMH_MHAS::~GPMH_MHAS() {
}

vector<double> GPMH_MHAS::getScores(const Sample &prevSample, const Block::sharedPtr_t block, const vector<Sample> &sVec) const {

	vector<double> scores;
	// Compute the probability of K(theta_i, theta_not(i))
	int myI = Parallel::mcmcMgr().getRankProposal();
	const Sample &mySample = sVec[myI];

	double K = 1.;
	K *= block->getMoveProbability(mySample, prevSample);
	for(size_t iS=0; iS<sVec.size(); ++iS) {
		if((int)iS == myI) continue;
		K *= block->getMoveProbability(mySample, sVec[iS]);
	}

	//std::cout << myI << " :: " << K << std::endl;

	scores.push_back(mySample.posterior);
	scores.push_back(K);

	if(Parallel::mcmcMgr().checkRoleProposal(Parallel::MCMCManager::Manager) ||
	   Parallel::mcmcMgr().checkRoleProposal(Parallel::MCMCManager::Sequential)) {
		K = 1.;
		for(size_t iS=0; iS<sVec.size(); ++iS) {
			K *= block->getMoveProbability(prevSample, sVec[iS]);
		}

		scores.push_back(prevSample.posterior);
		scores.push_back(K);

		//std::cout << myI << " :: " << K << std::endl;
	}

	return scores;
}

vector<int> GPMH_MHAS::acceptanceCheck(const vector<double> &scores) const {
	vector<int> accepted;

	double logSum = 0;
	std::vector<double> proba;
	for(size_t iS=0; iS<scores.size()/2; ++iS) {
		double logP = scores[iS*2] + log(scores[iS*2 + 1]);
		proba.push_back(logP);
		if(iS==0) {
			logSum = logP;
		} else {
			logSum = sumValLog(logSum, logP);
		}
	}

	for(size_t iP=0; iP<proba.size(); ++iP) {
		proba[iP] = exp(proba[iP]-logSum);
		//std::cout << "Proba [" << iP << "] : " << proba[iP] << "\t";
	}
	//std::cout << std::endl;

	for(size_t iA=0; iA<N; iA++) {
		int iS = Parallel::mpiMgr().getPRNG()->drawFrom(proba);
		if(iS >= Parallel::mcmcMgr().getNProposal()) {
			iS = -1;
		}
		accepted.push_back(iS);
		//std::cout << iS << "\t";
	}
	//std::cout << std::endl;
	//getchar();
	return accepted;
}

string GPMH_MHAS::getName(char sep) const {
	return "GPMH_MHAS";
}

double GPMH_MHAS::sumValLog(const double x, const double y) const {
	return (x>y ? x+log(1+exp(y-x)) : y+log(1+exp(x-y)));
}

} /* namespace Strategies */
} /* namespace Sampler */
