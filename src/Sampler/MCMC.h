//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MCMC.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef MCMC_H_
#define MCMC_H_

#include "Samples/Samples.h"
#include "Model/Model.h"
#include "Utils/Code/CheckpointFile.h"

#include <vector>
#include <sstream>
#include <fstream>
#include <boost/shared_ptr.hpp>

using namespace std;

namespace Sampler {

using StatisticalModel::Model;

class MCMC : private Uncopyable {
public:
	typedef boost::shared_ptr<MCMC> sharedPtr_t;

public:
	//! Constructor
	MCMC(Model &aModel);
	//! Constructor
	MCMC(Sample &initSample, Model &aModel);
	//! Destructor
	virtual ~MCMC();

	//! Define the output file name and path
	void setOutputFile(const string aFnPrefix="samplesChain_", const string aFnSuffix="");
	//! Generate samples during N iterations
	void generateNIterations(uint aN);
	//! Return the last sample generated
	Sample getLastSample() const;
	//! Set the verbose level (0= no write / 1 = info / 2 = dbg )
	void setVerbose(uint aVerbLvl);
	//! Set the number of samples kept (1 over nThinning)
	void setThinning(uint aThinning);
	//! Set if the log file is wrote in binary or in plain text
	void setWriteBinary(bool aIsBinary);
	//! If binary, set if the number are wrote as float (true) or double (false)
	void setWriteFloat(bool aIsWriteFloat);
	//! Set the output frequency in terms of iteration
	void setOutputFrequency(size_t aOutFrequency);

	//! Print the times taken for each operations (perfomance report)
	virtual void printTimes();

	std::string getOutputFile() const;
	std::string getOutputFullFileName() const;
	size_t getVerbose() const;
	size_t getThinning() const;
	size_t getOutputFrequency() const;
	bool isBinaryWriter() const;
	bool isFloatWriter() const;

protected:

	static const bool DEFAULT_ISBINARY, DEFAULT_ISWRITEFLOAT;
	static const unsigned int DEFAULT_THINNING, STEP_WRITE;

	const Parallel::MCMCManager &mcmcMgr;

	string fnPrefix, fnSuffix;

	size_t iT, cntAccepted, nWSamples;
	size_t serializedSampleSize, serializedSizeMC3;
	size_t nAccStepMC3, nStepMC3;
	uint verbLvl;
	double timeWrite, timeGenSample;

	Model &model;
	SampleVector samples;
	Sample curSample, oldSample;

	char *curState;

	//! Process the next MCMC iteration
	virtual void processNextIteration() = 0;
	//! Process the posterior score given the prior and the likelihood
	double processPosterior(const double aPrior, const double aLikelihood) const;
	//! Check if the prior is possible, if not doesn't process the likelihood
	double isPriorPossible(Sample &sample) const;
	//! Initialise the first sample
	void initSamples(Sample &sample);
	//! return the number of writted samples
	uint getNWSamples() const {return nWSamples;}

	//! Status report
	virtual std::string reportStatus() const = 0;

	//! MC3
	size_t convIterMC3;
	void packInfoMC3(char *dataSend, const bool accepted, const double invTemp, const Sample &sample);
	void unPackInfoMC3(const char *dataRecv, bool &accepted, double &invTemp, Sample &sample);
	void packPartialInfoMC3(char *dataSend, const bool accepted, const double invTemp);
	void unPackPartialInfoMC3(const char *dataRecv, bool &accepted, double &invTemp);
	void writeOutputMC3();

private:
	bool isBinary, isWriter, isWriteFloat;
	size_t thinning,  outputFrequency;

	std::ofstream oFile,  outMC3File;

	//! Default init
	void init(Sample &initSample);

	//! Write the header of the log file
	void writeHeader();
	//! Write the sample in the log file (or binary file depending on isBinary)
	void writeSamples(const uint offset);

	//! Write output information
	void writeOutput();
};

} // namespace Sampler

#endif /* MCMC_H_ */
