//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SampleVector.cpp
 *
 *  Created on: 2 oct. 2013
 *      Author: meyerx
 */

#include "SampleVector.h"

namespace Sampler {

SampleVector::SampleVector() : samples() {
}

SampleVector::SampleVector(int size, Sample defValue) : samples(size, defValue){
}

SampleVector::~SampleVector() {
}

size_t SampleVector::serializedSize() const {
	if(samples.empty()) return 0;

	return serializedSizeBody() + serializedSizeHeader();
}

size_t SampleVector::serializedSizeHeader() const {
	if(samples.empty()) return 0;

	return sizeof(size_t) + samples.front().serializedSizeHeader();
}

size_t SampleVector::serializedSizeBody() const {
	if(samples.empty()) return 0;

	return samples.size()*samples.front().serializedSizeBody();
}

void SampleVector::serialize(char *byteArray) const {
	if(samples.empty()) return;

	serializeHeader(byteArray);
	serializeBody(byteArray+serializedSizeHeader());
}

void SampleVector::serializeHeader(char *byteArray) const {
	if(samples.empty()) return;

	size_t nElem;
	// Serialize the sizes
	nElem = samples.size();
	memcpy(byteArray, &nElem, sizeof(size_t));
	byteArray += sizeof(size_t);
	samples.front().serializeHeader(byteArray);
}

void SampleVector::serializeBody(char *byteArray) const {
	if(samples.empty()) return;

	size_t bodySize = samples.front().serializedSizeBody();
	for(size_t i=0; i<samples.size(); ++i){
		samples[i].serializeBody(byteArray);
		byteArray += bodySize;
	}
}

void SampleVector::initFromSerialized(const char *byteArray) {
	size_t nElem;
	memcpy(&nElem, byteArray, sizeof(size_t));
	byteArray += sizeof(size_t);
	//cout << nElem << endl;
	samples.reserve(nElem);

	// Initialize the first with size
	Sample sample;
	sample.initFromSerialized(byteArray);
	byteArray += sample.serializedSize();
	samples.push_back(sample);

	size_t nInt, nDbl, nMarkers;
	nInt = sample.getIntValues().size();
	nDbl = sample.getDblValues().size();
	nMarkers = sample.getMarkers().size();
	for(size_t i=1; i<nElem; ++i){
		Sample s;
		s.initFromSerialized(nInt, nDbl, nMarkers, byteArray);
		samples.push_back(s);
		byteArray += s.serializedSizeBody();
	}
}

std::vector<Sample> SampleVector::write(size_t &cntAccepted, ostream &oFile, const size_t offset, const char sep, const size_t thinning) const {
	std::vector<Sample> wrtSamples;
	for(size_t i=0; i<samples.size(); ++i){
		if(cntAccepted % thinning == 0) {
			oFile << i+offset << sep;
			samples[i].write(oFile, sep);
			wrtSamples.push_back(samples[i]);
		}
		++cntAccepted;
	}
	return wrtSamples;
}

std::vector<Sample> SampleVector::writeAllButFront(size_t &cntAccepted, ostream &oFile, const size_t offset, const char sep, const size_t thinning) const {
	std::vector<Sample> wrtSamples;
	for(size_t i=1; i<samples.size(); ++i){
		if(cntAccepted % thinning == 0) {
			oFile << i+offset-1 << sep;
			samples[i].write(oFile, sep);
			wrtSamples.push_back(samples[i]);
		}
		++cntAccepted;
		//}
	}
	return wrtSamples;
}

std::vector<Sample> SampleVector::writeBin(size_t &cntAccepted, ostream &oFile, const size_t offset, const char sep, const size_t thinning, const bool writeFloat) const {
	std::vector<Sample> wrtSamples;
	for(size_t i=0; i<samples.size(); ++i){
		size_t tmp = i+offset;
		if(samples[i].hasChanged()){
			if(cntAccepted % thinning == 0) {
				oFile.write(reinterpret_cast<char*>(&tmp), sizeof(size_t));
				if(writeFloat){
					samples[i].writeBinFloat(oFile);
				} else {
					samples[i].writeBin(oFile);
				}
				wrtSamples.push_back(samples[i]);
			}
			++cntAccepted;
		}
	}
	return wrtSamples;
}

std::vector<Sample> SampleVector::writeBinAllButFront(size_t &cntAccepted, ostream &oFile, const size_t offset, const char sep, const size_t thinning, const bool writeFloat) const {
	std::vector<Sample> wrtSamples;
	for(size_t i=1; i<samples.size(); ++i){
		size_t tmp = i+offset-1;
		if(samples[i].hasChanged()){
			if(cntAccepted % thinning == 0) {
				oFile.write(reinterpret_cast<char*>(&tmp), sizeof(size_t));
				if(writeFloat){
					samples[i].writeBinFloat(oFile);
				} else {
					samples[i].writeBin(oFile);
				}
				wrtSamples.push_back(samples[i]);
			}
			++cntAccepted;
		}
	}

	return wrtSamples;
}

string SampleVector::toString() const {
	stringstream ss;
	for(size_t i=0; i<samples.size(); ++i){
		ss << samples[i].toString() << endl;
	}
	return ss.str();
}

} // namespace Sampler


