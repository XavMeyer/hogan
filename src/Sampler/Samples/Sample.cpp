//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sample.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Sample.h"

namespace Sampler {

Sample::Sample() :  changed(true), prior(0.0), likelihood(0.0), posterior(0.0) {
}

Sample::Sample(const Sample &toCopy) {
	copy(toCopy);
}

Sample::~Sample() {
}

Sample& Sample::operator=(const Sample &toCopy) {
	copy(toCopy);
    return *this;
}

bool Sample::isInteger(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return true;
	} else {
		return false;
	}
}

bool Sample::isDouble(int aInd) const {
	return !isInteger(aInd);
}

void Sample::incParameter(int aInd, double aIncr){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] += aIncr;
	} else {
		dblValues[aInd-ivSize] += aIncr;
	}
}

void Sample::multParameter(int aInd, double aMult){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] *= aMult;
	} else {
		dblValues[aInd-ivSize] *= aMult;
	}
}

double Sample::getDoubleParameter(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return intValues[aInd];
	} else {
		return dblValues[aInd-ivSize];
	}
}

void Sample::setDoubleParameter(int aInd, double aVal){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] = aVal;
	} else {
		dblValues[aInd-ivSize] = aVal;
	}
}

long int Sample::getIntParameter(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return intValues[aInd];
	} else {
		return dblValues[aInd-ivSize];
	}
}

void Sample::setIntParameter(int aInd, long int aVal){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] = aVal;
	} else {
		dblValues[aInd-ivSize] = aVal;
	}
}

size_t Sample::getNbParameters() const {
	return intValues.size() + dblValues.size();
}

size_t Sample::serializedSize() const {
	return serializedSizeHeader() + serializedSizeBody();
}

size_t Sample::serializedSizeHeader() const {
	return 3*sizeof(size_t);
}

size_t Sample::serializedSizeBody() const {
	return 3*sizeof(double) + intValues.size()*sizeof(long int) + dblValues.size()*sizeof(double) + markers.size()*sizeof(double);
}

void Sample::serialize(char *byteArray) const {
	serializeHeader(byteArray);
	serializeBody(byteArray+serializedSizeHeader());
}

void Sample::serializeHeader(char *byteArray) const {
	size_t nInt, nDbl, nMarkers;
	// Serialize the sizes
	nInt = intValues.size();
	//cout << "s@" << hex << (long int)byteArray << dec << " => " << nInt << endl;
	memcpy(byteArray, &nInt, sizeof(size_t));
	byteArray += sizeof(size_t);

	nDbl = dblValues.size();
	memcpy(byteArray, &nDbl, sizeof(size_t));
	byteArray += sizeof(size_t);

	nMarkers = markers.size();
	memcpy(byteArray, &nMarkers, sizeof(size_t));
	byteArray += sizeof(size_t);
}

void Sample::serializeBody(char *byteArray) const {
	// Serialize the score
	memcpy(byteArray, &prior, sizeof(double));
	byteArray += sizeof(double);
	memcpy(byteArray, &likelihood, sizeof(double));
	byteArray += sizeof(double);
	memcpy(byteArray, &posterior, sizeof(double));
	byteArray += sizeof(double);
	// Serialize int vector
	memcpy(byteArray, &intValues[0], intValues.size()*sizeof(long int));
	byteArray += intValues.size()*sizeof(long int);

	// Serialize dbl vector
	memcpy(byteArray,  &dblValues[0],  dblValues.size()*sizeof(double));
	byteArray += dblValues.size()*sizeof(double);

	// Serialize marker vector
	memcpy(byteArray,  &markers[0],  markers.size()*sizeof(double));
	byteArray += markers.size()*sizeof(double);
}

void Sample::initFromSerialized(const char *byteArray){
	size_t nInt, nDbl, nMarkers;

	memcpy(&nInt, byteArray, sizeof(size_t));
	//cout << "s@" << hex << (long int)byteArray << dec << " => " << nInt << endl;
	byteArray  += sizeof(size_t);


	memcpy(&nDbl, byteArray, sizeof(size_t));
	//cout << "s@" << hex << (long int)byteArray << dec << " => " << nDbl << endl;
	byteArray  += sizeof(size_t);


	memcpy(&nMarkers, byteArray, sizeof(size_t));
	//cout << "s@" << hex << (long int)byteArray << dec << " => " << nDbl << endl;
	byteArray  += sizeof(size_t);

	initFromSerialized(nInt, nDbl, nMarkers, byteArray);
}


void Sample::initFromSerialized(const size_t nInt, const size_t nDbl, const size_t nMarkers, const char *byteArray){

	// Reserve space in the vectors
	intValues.resize(nInt);
	dblValues.resize(nDbl);
	markers.resize(nMarkers);

	// Set the prior, likelihood, posterior from bytes
	memcpy(&prior, byteArray, sizeof(double));
	byteArray += sizeof(double);
	memcpy(&likelihood, byteArray, sizeof(double));
	byteArray += sizeof(double);
	memcpy(&posterior, byteArray, sizeof(double));
	byteArray += sizeof(double);

	memcpy(&intValues[0], byteArray, nInt*sizeof(long int));
	byteArray += intValues.size()*sizeof(long int);

	memcpy(&dblValues[0], byteArray, nDbl*sizeof(double));
	byteArray += dblValues.size()*sizeof(double);

	memcpy(&markers[0], byteArray, nMarkers*sizeof(double));
	byteArray += markers.size()*sizeof(double);

	// Set the int from bytes
	/*for(size_t i=0; i<nInt; ++i){
		int val;
		memcpy(&val, byteArray, sizeof(int));
		intValues.push_back(val);
		byteArray += sizeof(int);
	}
	//byteArray += intValues.size()*sizeof(int);

	// Set the dbl from bytes
	for(size_t i=0; i<nDbl; ++i){
		double val;
		memcpy(&val, byteArray, sizeof(double));
		dblValues.push_back(val);
		byteArray += sizeof(double);
	}*/
	//memcpy(&(*dblValues.begin()), byteArray, nDbl*sizeof(double));
}

void Sample::write(ostream &oFile, char sep) const {
	oFile << prior << sep;

	// Get rid of minus inf when LOG LIK
	if(likelihood == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max() << sep;
	} else {
		oFile << likelihood << sep;
	}

	// Get rid of minus inf when LOG LIK
	if(posterior == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max();
	} else {
		oFile << posterior;
	}

	for(size_t i=0; i<markers.size(); ++i){
		oFile << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		oFile << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		oFile << sep << dblValues[i];
	}

	oFile << endl;
}

void Sample::writeBin(ostream &oFile) const {
	// Write Prior, Likelihood, Posterior
	oFile.write(reinterpret_cast<const char*>(&prior), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&likelihood), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&posterior), sizeof(double));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&markers[0]), markers.size()*sizeof(double));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&dblValues[0]), dblValues.size()*sizeof(double));
}

void Sample::writeBinFloat(ostream &oFile) const {

	float tmp;
	vector<float> tmpMarker(markers.begin(), markers.end());
	vector<float> tmpDbl(dblValues.begin(), dblValues.end());
	// Write Prior, Likelihood, Posterior
	tmp = static_cast<float>(prior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(likelihood);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(posterior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&tmpMarker[0]), tmpMarker.size()*sizeof(float));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&tmpDbl[0]), tmpDbl.size()*sizeof(float));
}

string Sample::toString(char sep) const {
	stringstream ss;
	ss << prior << sep << likelihood << sep << posterior;

	for(size_t i=0; i<markers.size(); ++i){
		ss << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		ss << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		ss << sep << dblValues[i];
	}
	return ss.str();
}

void Sample::setMarkers(const vector<double> &aMarkers) {
	if(!aMarkers.empty()){
		markers = aMarkers;
	}
}

void Sample::setChanged(bool aChanged) {
	changed = aChanged;
}

bool Sample::hasChanged() const {
	return changed;
}

void Sample::copy(const Sample &toCopy) {
	changed = toCopy.changed;
	prior = toCopy.prior;
	likelihood = toCopy.likelihood;
	posterior = toCopy.posterior;
	intValues = toCopy.intValues;
	dblValues = toCopy.dblValues;
	markers = toCopy.markers;
}

} // namespace Sampler
