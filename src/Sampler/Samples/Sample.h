//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sample.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef SAMPLE_H_
#define SAMPLE_H_

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>

using namespace std;

namespace Sampler {

class Sample {
public:
	Sample();
	Sample(const Sample &toCopy);
   ~Sample();

   Sample& operator=(const Sample& toCopy);

   bool isInteger(int aInd) const;
   bool isDouble(int aInd) const;

   void incParameter(int aInd, double aIncr);
   void multParameter(int aInd, double aMult);
   double getDoubleParameter(int aInd) const;
   void setDoubleParameter(int aInd, double aVal);
   long int getIntParameter(int aInd) const;
   void setIntParameter(int aInd, long int aVal);
   size_t getNbParameters() const;

   void setMarkers(const vector<double> &aMarkers);

   void setChanged(bool aChanged);
   bool hasChanged() const;

   size_t serializedSize() const;
   size_t serializedSizeHeader() const;
   size_t serializedSizeBody() const;
   void serialize(char *byteArray) const;
   void serializeHeader(char *byteArray) const;
   void serializeBody(char *byteArray) const;
   void initFromSerialized(const char *byteArray);
   void initFromSerialized(const size_t nInt, const size_t nDbl, const size_t nMarkers, const char *byteArray);

   inline const vector<long int>& getIntValues() const {return intValues;}
   inline const vector<double>& getDblValues() const {return dblValues;}
   inline const vector<double>& getMarkers() const {return markers;}

   void write(ostream &oFile, char sep='\t') const;
   void writeBin(ostream &oFile) const;
   void writeBinFloat(ostream &oFile) const;

   string toString(char sep='\t') const;

public:
   bool changed;
   double prior, likelihood, posterior;
   vector<long int> intValues;
   vector<double> dblValues;
   vector<double> markers;

private:
   void copy(const Sample &toCopy);

};

} // namespace Sampler

#endif /* SAMPLE_H_ */
