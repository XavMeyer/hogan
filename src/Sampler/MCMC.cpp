//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MCMC.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "MCMC.h"

namespace Sampler {

const bool MCMC::DEFAULT_ISBINARY = false;
const bool MCMC::DEFAULT_ISWRITEFLOAT = false;
const unsigned int MCMC::DEFAULT_THINNING = 1;
const unsigned int MCMC::STEP_WRITE = 200;

MCMC::MCMC(Model &aModel) : mcmcMgr(Parallel::mcmcMgr()), model(aModel), samples() {
	Sample sample(model.getRandomSample());
	init(sample);
}

MCMC::MCMC(Sample &initSample, Model &aModel) : mcmcMgr(Parallel::mcmcMgr()), model(aModel), samples() {
	init(initSample);
}

void MCMC::init(Sample &initSample) {
	iT = 0;

	initSamples(initSample);
	setOutputFile();

	if(model.getLikelihood()->isUpdatable()){
		curState = new char[model.getLikelihood()->stateSize()];
	} else {
		curState = NULL;
	}
	verbLvl = 2;
	thinning = DEFAULT_THINNING;
	isBinary = DEFAULT_ISBINARY;
	isWriteFloat = DEFAULT_ISWRITEFLOAT;

	isWriter = mcmcMgr.isOutputManager();

	timeWrite = timeGenSample = 0.;
	cntAccepted = 0;

	outputFrequency = 1000;

	// MC3
	nAccStepMC3 = 0;
	nStepMC3 = 0;
	convIterMC3 = 0;
}

MCMC::~MCMC() {
	if(curState != NULL) {
		delete [] curState;
	}
}

void MCMC::initSamples(Sample &sample) {
	nWSamples = 0;
	sample.prior = model.processPriorValue(sample);
	sample.likelihood = model.getLikelihood()->processLikelihood(sample);
	sample.setMarkers(model.getLikelihood()->getMarkers());
	sample.posterior = processPosterior(sample.prior, sample.likelihood);
	serializedSampleSize = sample.serializedSize();
	serializedSizeMC3 = serializedSampleSize + sizeof(double) + 1;
	samples.push_back(sample);
}

void MCMC::generateNIterations(uint aN){

	// Checkpoints

	double startTime = MPI_Wtime();

	// Initial write for samples
	if(isWriter){
		writeHeader(); // Init header and output file
		model.getLikelihood()->initCustomizedLog(fnPrefix, fnSuffix, 0);
	}

	size_t start = iT;
	for(iT=start; iT<start+aN; ++iT){
		// Write std output
		writeOutput();

		// Process iteration iT
		processNextIteration();

		// Write samples
		if(isWriter){
			double s = MPI_Wtime();
			if(iT%STEP_WRITE==0) writeSamples(iT);
			timeWrite += MPI_Wtime() - s;
		}
	}

	// Final write for samples
	if(isWriter){
		double s = MPI_Wtime();
		// Insure that everybody is written
		bool changedState = samples.back().hasChanged();
		samples.back().setChanged(true);
		writeSamples(iT);
		samples.back().setChanged(changedState);
		timeWrite += MPI_Wtime() - s;
	}

	timeGenSample = MPI_Wtime() - startTime;
}

Sample MCMC::getLastSample() const {
	return curSample;
}

void MCMC::setVerbose(uint aVerbLvl) {
	verbLvl = aVerbLvl;
}

void MCMC::setThinning(uint aThinning) {
	thinning = aThinning;
}

void MCMC::setWriteBinary(bool aIsBinary) {
	isBinary = aIsBinary;
}

void MCMC::setWriteFloat(bool aIsWriteFloat) {
	isWriteFloat = aIsWriteFloat;
}

void MCMC::setOutputFrequency(const size_t aOutFrequency) {
	outputFrequency = aOutFrequency;
}


void MCMC::setOutputFile(const string aFnPrefix, const string aFnSuffix) {
	fnPrefix = aFnPrefix;
	fnSuffix = aFnSuffix;
}

void MCMC::writeHeader() {
	const char sep = '\t';
	if(!oFile.is_open()){
		stringstream fName;
		if(isBinary){
			fName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << ".hdr";
			oFile.open(fName.str().c_str(), ofstream::out | ofstream::binary);
		} else {
			fName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << ".log";
			oFile.open(fName.str().c_str(), ofstream::out); //tmpFName.c_str()
			oFile.precision(8);
		}
		if(!oFile.is_open()) {
			std::cerr << "[ERROR] void MCMC::writeHeader();" << std::endl;
			std::cerr << " Cannot open file : " << fName.str() << std::endl;
		}
	}

	oFile << "Iteration" << sep << "Prior"<< sep << "Likelihood" << sep << "Posterior" << sep;
	const vector<string> &markerNames = model.getLikelihood()->getMarkerNames();
	for(size_t iN=0; iN<markerNames.size(); ++iN){
		oFile << markerNames[iN] << sep;
	}
	vector<string> names(model.getParams().getNames());
	for(size_t iN=0; iN<names.size(); ++iN){
		oFile << names[iN] << sep;
	}
	oFile << endl;

	if(isBinary){
		oFile << samples.back().getMarkers().size() << sep;
		oFile << samples.back().getIntValues().size() << sep;
		oFile << samples.back().getDblValues().size() << endl;
		oFile.close();
	}
}

void MCMC::writeSamples(const uint offset) {
	// Create output file stream
	const char sep = '\t';

	if(!oFile.is_open()){
		stringstream fName;
		if(isBinary){
			fName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << ".bin";
			oFile.open(fName.str().c_str(), ofstream::trunc | ofstream::binary);
		} else {
			fName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << ".log";
			oFile.open(fName.str().c_str(), ofstream::app);
			oFile.precision(8);
			oFile << scientific;
		}
	}

	std::vector<Sample> wrtSamples; // Keep track of written samples
	if(isBinary) {
		if(offset != 0) {
			wrtSamples = samples.writeBinAllButFront(cntAccepted, oFile, nWSamples, sep, thinning, isWriteFloat);
			nWSamples += samples.size()-1;
		} else {
			wrtSamples = samples.writeBin(cntAccepted, oFile, nWSamples, sep, thinning, isWriteFloat);
			nWSamples += samples.size();
		}
	} else {
		oFile.precision(8);
		oFile << scientific;
		if(offset != 0) {
			wrtSamples = samples.writeAllButFront(cntAccepted, oFile, nWSamples, sep, thinning);
			nWSamples += samples.size()-1;
		} else {
			wrtSamples = samples.write(cntAccepted, oFile, nWSamples, sep, thinning);
			nWSamples += samples.size();
		}
	}

	model.getLikelihood()->writeCustomizedLog(wrtSamples);
	//model.getLikelihood()->customWrite(fnPrefix, fnSuffix, wrtSamples);

	// Copy last sample and insert it
	Sample tmp(samples.back());
	samples.clear();
	samples.push_back(tmp);
}

void MCMC::writeOutput() {
	if(Parallel::mpiMgr().isMainProcessor() && verbLvl > 0){
		if(iT%outputFrequency==0) cout << "Iteration : " << iT << " -- posterior = " << samples.back().posterior << endl;
		if((1+iT)%(100*outputFrequency)==0 && verbLvl > 1) {
			std::cout << reportStatus() << std::endl;;
			std::cout << "Block status : " << std::endl;
			for(uint i=0; i<model.getBlocks().size(); ++i){
				std::cout << model.getBlocks().getBlock(i)->summaryString();
				std::cout << "**************************************" << endl;
			}
		}
	} else if (Parallel::mcmcMgr().isManagerMC3() && verbLvl > 1) {
		writeOutputMC3();
	}
}

double MCMC::processPosterior(const double aPrior, const double aLikelihood) const {
	if(model.getLikelihood()->isLog()){
		return aPrior + aLikelihood;
	} else {
		return aPrior * aLikelihood;
	}
}

double MCMC::isPriorPossible(Sample &sample) const {
	if(model.getLikelihood()->isLog()){
		if(sample.prior == -numeric_limits<double>::infinity()) {
			sample.likelihood = -numeric_limits<double>::infinity();
			sample.posterior = -numeric_limits<double>::infinity();
			return false;
		}
	} else {
		if(sample.prior == 0.){
			sample.likelihood = 0.;
			sample.posterior = 0.;
			return false;
		}
	}

	return true;
}


void MCMC::printTimes(){
	if(Parallel::mpiMgr().isMainProcessor()){
		cout << "Time W :\t" << timeWrite <<  endl;
	}
}

std::string MCMC::getOutputFile() const {
	return fnPrefix;
}

std::string MCMC::getOutputFullFileName() const {
	std::stringstream fName;
	fName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix;
	return fName.str();
}

size_t MCMC::getVerbose() const {
	return verbLvl;
}

size_t MCMC::getThinning() const {
	return thinning;
}

size_t MCMC::getOutputFrequency() const {
	return outputFrequency;
}

bool MCMC::isBinaryWriter() const {
	return isBinary;
}

bool MCMC::isFloatWriter() const {
	return isWriteFloat;
}

void MCMC::packInfoMC3(char *dataSend, const bool accepted, const double invTemp, const Sample &sample) {
	if(accepted) {
		dataSend[serializedSampleSize+sizeof(double)] = 'Y';
	} else {
		dataSend[serializedSampleSize+sizeof(double)] = 'N';
	}

	double *tmp = reinterpret_cast<double *>(&dataSend[serializedSampleSize]);
	*tmp = invTemp;


	sample.serialize(dataSend);
}

void MCMC::unPackInfoMC3(const char *dataRecv, bool &accepted, double &invTemp, Sample &sample){
	if(dataRecv[serializedSampleSize+sizeof(double)] == 'Y') {
		accepted = true;
	} else {
		accepted = false;
	}

	const double *tmp = reinterpret_cast<const double *>(&dataRecv[serializedSampleSize]);
	invTemp = *tmp;

	sample.initFromSerialized(dataRecv);
}

void MCMC::packPartialInfoMC3(char *dataSend, const bool accepted, const double invTemp) {
	if(accepted) {
		dataSend[serializedSampleSize+sizeof(double)] = 'Y';
	} else {
		dataSend[serializedSampleSize+sizeof(double)] = 'N';
	}

	double *tmp = reinterpret_cast<double *>(&dataSend[serializedSampleSize]);
	*tmp = invTemp;
}

void MCMC::unPackPartialInfoMC3(const char *dataRecv, bool &accepted, double &invTemp) {
	if(dataRecv[serializedSampleSize+sizeof(double)] == 'Y') {
		accepted = true;
	} else {
		accepted = false;
	}

	const double *tmp = reinterpret_cast<const double *>(&dataRecv[serializedSampleSize]);
	invTemp = *tmp;
}

void MCMC::writeOutputMC3() {
	if(!outMC3File.is_open()) {
		std::stringstream ssOutMC3FileName;
		ssOutMC3FileName << getOutputFile() << "MC3_" << Parallel::mcmcMgr().getMyChainMC3() << ".txt";
		outMC3File.open(ssOutMC3FileName.str().c_str());
	}

	if(iT%(5*outputFrequency)==0) outMC3File << "Iteration : " << iT << " -- posterior = " << curSample.posterior << endl;
	if((1+iT)%(100*outputFrequency)==0) {
		outMC3File << reportStatus() << std::endl;;
		outMC3File << "Block status : " << std::endl;
		for(uint i=0; i<model.getBlocks().size(); ++i){
			outMC3File << model.getBlocks().getBlock(i)->summaryString();
			outMC3File << "**************************************" << std::endl;
		}
	}
}

} // namespace Sampler


