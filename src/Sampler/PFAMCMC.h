//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PFAMCMC.h
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#ifndef PFAMCMC_H_
#define PFAMCMC_H_

#include "MCMC.h"
#include "Adaptive/IncAdaptiveBlocks.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Rho/BatchEvoRU.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "Strategy/BlockSelector/BlockSelector.h"
#include "Strategy/ModifierStrategy/ModifierStrategy.h"
#include "Strategy/AcceptanceStrategy/AcceptanceStrategy.h"
#include "Utils/Code/SerializationSupport.h"

#include <algorithm>
#include <vector>

namespace Sampler {

using namespace Strategies;
using ParameterBlock::Config::ConfigFactory;

class PFAMCMC : public MCMC {
public:
	/**
	 * Constructor.
	 * @param initSample Initial sample (starting point of the MCMC process)
	 * @param aModel Statistical model that should be inferred.
	 * @param aBS BlockSelector defining the way blocks are selected.
	 * @param aMS ModifierStrategy defining how blocks are modified.
	 * @param aAS Acceptance strategies defining how proposed sampled are selected.
	 */
	PFAMCMC(Sample &initSample, Model &aModel, BlockSelector::sharedPtr_t aBS,
			ModifierStrategy::sharedPtr_t aMS, AcceptanceStrategy::sharedPtr_t aAS,
			ConfigFactory::sharedPtr_t aCfgFac = ConfigFactory::createDefaultFactory());

	//! Destructor
	~PFAMCMC();

	//! Get a vector containing all BlockStats
	vector<Adaptive::AdaptiveBlock::sharedPtr_t> getAdaptiveBlock() { return adaptiveBlocks;};

	//! Print the times
	virtual void printTimes();

	//! Return a formatted string with this run performance
	virtual string getPerfReport() const;

	BlockSelector::sharedPtr_t getBlockSel() const;
	ModifierStrategy::sharedPtr_t getModStrat() const;
	AcceptanceStrategy::sharedPtr_t getAcceptStrat() const;

private:

	bool fullConvergence;
	size_t convIt, convS;

	BlockSelector::sharedPtr_t blockSel;
	ModifierStrategy::sharedPtr_t modStrat;
	AcceptanceStrategy::sharedPtr_t acceptStrat;

	ConfigFactory::sharedPtr_t cfgFac;

	// Define the threshold used for the relaxation of the adaptive phase
	static const uint NB_BLOCK_RELAXATION_TH;

	double timeP1, timeP2, timeI, timeB, timeB2, timeC, timeW, timeTot, timeP1_1, timeP1_2, timeP1_3, timeMC3;

	size_t nAdaptiveBlocks;
	vector<Adaptive::AdaptiveBlock::sharedPtr_t> adaptiveBlocks;

	//! Prepare scores in order to send them
	void prepareScores(const uint nPropSample, const vector<double> &scores, vector<double> &alphas) const;

	//! Execute one MCMC iteration
	void processNextIteration();

	//! Process the score of a sample (non-update case)
	void processSample(vector<Sample> &proposedSamples);
	//! Process the score of a sample (update case)
	void processSample(const vector<size_t> &pInd, vector<Sample> &proposedSamples);

	//! apply the acceptance check given the scores
	int acceptanceCheck(const vector<double> &scores, int &nReject);

	//! Define the accepted sample position (processor, idx)
	void defineSamplePosition(const int acceptId, int &aRank, int &aRow, const int propSize);
	//! Exchange the sample with the other member of the parallel proposal group
	void exchangeSample(const int aRank, const int aRow, const vector<Sample> &propSamples);
	//! Exchange the state with the other member of the parallel proposal group
	void exchangeState(const int aRank, const int aRow);

	//! Initialize the times for the performance report
	void initTimes();
	void initAdaptive();

	//! Check if has converged
	bool hasConverged() const;
	void displayBlockConvergence(const size_t iB, Adaptive::AdaptiveBlock::conv_signal_t convSignals) const;
	bool checkConvergence();

	void updateLocalAlphas(const std::vector<int> &iBlocks);
	std::vector< std::vector<double> > updateLocalAlpha(const std::vector<int> &iBlocks);

	std::vector< std::vector<double> > balancedLocalAlphaUpdate(const std::vector<int> &iBlocks);
	void localAlphaComputation(const size_t iB, const std::vector<size_t> &pInd, std::vector<double> &locAlphas);

	//! Apply the sequential algorithm (no pre-fetching)
	void doSequentialAcceptanceAndUpdate(const vector<int> &iBlocks, const vector<Sample> &propSamples, const vector<double> &scores);
	//! Calls the designed functions for the adaptive phase
	void adaptBlocks(const vector<int> &iBlocks, const int acceptedId, const vector<double> &alphas);

	//! MC3 related
	RNG *mc3_rng;
	BlockStatCfg::sharedPtr_t cfgMC3;
	ParameterBlock::BatchEvoRU *evoMC3;
	bool doMC3();
	void defineExchange(int &coldChain, int &hotChain, double &unifDbl);
	double defineExchangeRatio(const int myChain, const int otherChain, char *dataRecv, Sample &newSample);
	void acceptExchangeManager(Sample &newSample, const int otherChain);
	void acceptExchangeWorker(Sample &newSample);

	//! Status report
	std::string reportStatus() const;
};

} /* namespace Sampler */

#endif /* PFAMCMC_H_ */
