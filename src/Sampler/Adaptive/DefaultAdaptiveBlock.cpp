//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DefaultAdaptiveBlock.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include <Sampler/Adaptive/DefaultAdaptiveBlock.h>

namespace Sampler {
namespace Adaptive {

DefaultAdaptiveBlock::DefaultAdaptiveBlock(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		const CF::ConfigFactory::sharedPtr_t &aCfgFac) : AdaptiveBlock(aBlock, aModel, aCfgFac) {
	init(initSample);
}

DefaultAdaptiveBlock::~DefaultAdaptiveBlock() {
}


void DefaultAdaptiveBlock::init(const Sample &aSample) {
	// Got this from mesures (best alpha in function of nP)
	unsigned int nProp = mcmcMgr.getNProposal();

	state = DEFAULT_ADAPT;

	alpha = AdaptiveBlock::getBestAlpha(nProp, block->size());

	bStat.reset(new ParameterBlock::EvoBS(alpha, block, cfgFac->createConfig(block->size())));
	bStat->initSample(aSample);
}


void DefaultAdaptiveBlock::updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample) {

	/*if(block.getId() == 2 && accepted) { // FIXME
		std::cout << "Block rate : accepted " << std::endl;
	} else if(block.getId() == 2 && !accepted) {
		std::cout << "Block rate : not accepted " << std::endl;
	}*/

	if(!accepted){
		bStat->update(oldSample, 1);
	} else {
		bStat->update(curSample, 1);
	}
}

AdaptiveBlock::conv_signal_t DefaultAdaptiveBlock::updateLambda(const double aAlpha) {
	AdaptiveBlock::conv_signal_t signal = NO_SIGNAL;

	if(bStat->isReady()){
		bStat->updateSD(aAlpha);

		double Sd = bStat->getSd();
		for(uint iP=0; iP<block->getPIndices().size(); ++iP){
			double var = Sd*bStat->getLocSd(iP)*bStat->getSigmaIdx(iP);
			if(var > 0.){
				block->getBlockModifier(iP)->setWindowSize(vector<double>(1,sqrt(var)));
			}
		}
		if(bStat->hasLambdaConverged()) {
			signal = SIGNAL_CONV;
			state = CONVERGED;
		}
	}

	return signal;
}

void DefaultAdaptiveBlock::create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) {
	AdaptiveBlock::createDefault1DMoves(curSample, nDSample, newSamples);
}

void DefaultAdaptiveBlock::updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
												      const std::vector<double> &percConvergedLambda) {

	assert(!percConvergedSigma.empty() && !percConvergedLambda.empty());
	double percSigma = percConvergedSigma.back();
	double percLambda = percConvergedLambda.back();

	double relaxTolSigma = std::max(1.0, pow(percSigma/40.,2));
	double relaxTolLambda = std::max(1.0, pow((percSigma + percLambda)/80.,3));
	bStat->relaxToleranceSigma(relaxTolSigma, 3.*relaxTolSigma);
	bStat->relaxToleranceLambda(relaxTolLambda);

}

bool DefaultAdaptiveBlock::hasSigmaConverged() const {
	return bStat->hasSigmaConverged();
}

bool DefaultAdaptiveBlock::hasLambdaConverged() const {
	return bStat->hasLambdaConverged();
}

bool DefaultAdaptiveBlock::isReadyForLocUpdate() const {
	return bStat->isReadyForLocUpdate();
}

bool DefaultAdaptiveBlock::hasLocLambdaConverged() const {
	return bStat->hasLocLambdaConverged();
}

void DefaultAdaptiveBlock::updateLocSD(std::vector<double> &localAlphas) {
	bStat->updateLocSD(localAlphas);
}

std::string DefaultAdaptiveBlock::getSummaryString() const {
	std::stringstream ss;
	ss << "Default adaptive block" << std::endl;
	ss << bStat->getSummaryString();
	return ss.str();
}

} /* namespace Adaptive */
} /* namespace Sampler */
