//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MixedAdaptiveBlock.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include <Sampler/Adaptive/MixedAdaptiveBlock.h>

namespace Sampler {
namespace Adaptive {

MixedAdaptiveBlock::MixedAdaptiveBlock(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, const CF::ConfigFactory::sharedPtr_t &aCfgFac) :
				AdaptiveBlock(aBlock, aModel, aCfgFac){
	init(initSample);
	cfgFac = aCfgFac;
}

MixedAdaptiveBlock::~MixedAdaptiveBlock() {
}

void MixedAdaptiveBlock::init(const Sample &aSample) {
	// Got this from mesures (best alpha in function of nP)
	unsigned int nProp = mcmcMgr.getNProposal();

	state = PCA_INDEP;

	alpha = AdaptiveBlock::getBestAlpha(nProp, block->size());

	bStat.reset(new ParameterBlock::EvoBS(alpha, block, cfgFac->createConfig(block->size())));
	bStat->initSample(aSample);

	hashSigma = -1.;
}


void MixedAdaptiveBlock::updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample) {
	if(!accepted){
		bStat->update(oldSample, 1);
	} else {
		bStat->update(curSample, 1);
	}
}

AdaptiveBlock::conv_signal_t MixedAdaptiveBlock::updateLambda(const double alpha) {
	using ParameterBlock::BlockModifier;
	conv_signal_t signal = NO_SIGNAL;

	if(bStat->isReady()){
		bStat->updateSD(alpha);

		if(state == PCA_INDEP){
			updateIndepWindows();
			if(bStat->hasSigmaConverged() && bStat->hasLambdaConverged()){
				if(bStat->hasCorrelation() && !bStat->hasDisabledFactors()){
					/*if(Parallel::mpiMgr().isMainProcessor()  && verbLvl == 2) {
						cout << "Correlated block : " << block.getName() << endl;
					}*/
					block->setBlockModifier(BlockModifier::createMVGaussianWindow(block->size(), 1.0, model.getParams()));
					for(size_t iParam=0; iParam<block->size(); ++iParam){
						block->setBlockModifier(iParam, BlockModifier::sharedPtr_t());
					}
					bStat->resetMVCC();
					state = PCA_CORREL;
					signal = SIGNAL_CORREL;
				} else {
					state = PCA_INDEP_P2;
					signal = SIGNAL_INDEP;
				}

			}
		} else if(state == PCA_CORREL) {
			updateCorrelWindows();
			if(bStat->hasSigmaConverged() && bStat->hasLambdaConverged()) {
				state = CONVERGED;
				signal = SIGNAL_CONV;
			}
		} else if(state == PCA_INDEP_P2) {
			updateIndepWindows();
			if(bStat->hasSigmaConverged() && bStat->hasLambdaConverged()) {
				state = CONVERGED;
				signal = SIGNAL_CONV;
			}
		} else {
			if(Parallel::mpiMgr().isMainProcessor()) {
				std::cerr << "[Error] in AdaptiveBlock::conv_signal_t MixedAdaptiveBlock::updateLambda(const double alpha);" << std::endl;
				std::cerr << "Block [" << block->getName() << "is in an incorrect state." << std::endl;
				abort();
			}
		}
	}

	return signal;
}

void MixedAdaptiveBlock::create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) {
	AdaptiveBlock::createDefault1DMoves(curSample, nDSample, newSamples);
}

void MixedAdaptiveBlock::updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
												    const std::vector<double> &percConvergedLambda) {

	assert(percConvergedSigma.size() >= 2 && percConvergedLambda.size() >= 2);
	double pSigmaP1 = percConvergedSigma[0];
	double pSigmaP2 = percConvergedSigma[1];
	double pLambdaP1 = percConvergedLambda[0];
	double pLambdaP2 = percConvergedLambda[1];

	double relaxTolSigmaP1 = 2*std::max(1.0, pow(pSigmaP1/50.,2));
	double relaxTolLambdaP1 = 2*std::max(1.0, pow((pSigmaP1 + pLambdaP1)/100.,3));
	double relaxTolSigmaP2 = std::max(1.0, pow(pSigmaP2/40.,2));
	double relaxTolLambdaP2 = std::max(1.0, pow((pSigmaP2 + pLambdaP2)/80.,3));

	if(bStat != NULL && state == PCA_INDEP){
		bStat->relaxToleranceSigma(relaxTolSigmaP1, 2.5*relaxTolSigmaP1);
		bStat->relaxToleranceLambda(relaxTolLambdaP1);
	} else if (bStat != NULL && state == PCA_CORREL){
		bStat->relaxToleranceSigma(relaxTolSigmaP2, relaxTolSigmaP2);
		bStat->relaxToleranceLambda(relaxTolLambdaP2);
	} else if (bStat != NULL && state == PCA_INDEP_P2){
		bStat->relaxToleranceSigma(relaxTolSigmaP2, 4.*relaxTolSigmaP2);
		bStat->relaxToleranceLambda(relaxTolLambdaP2);
	}
}

bool MixedAdaptiveBlock::hasSigmaConverged() const {
	return bStat->hasSigmaConverged();
}

bool MixedAdaptiveBlock::hasLambdaConverged() const {
	return bStat->hasLambdaConverged();
}

void MixedAdaptiveBlock::updateIndepWindows() {
	double Sd = bStat->getSd();
	for(uint iP=0; iP<block->getPIndices().size(); ++iP){
		double var = Sd*bStat->getLocSd(iP)*bStat->getSigmaIdx(iP);

		if(var > 0.){
			block->getBlockModifier(iP)->setWindowSize(std::vector<double>(1,sqrt(var)));
		}
	}
}

void MixedAdaptiveBlock::updateCorrelWindows() {
	size_t sizeCM = block->size();

	const double *ptrCM = bStat->getSigma();
	vector<double> covMat(ptrCM, ptrCM+sizeCM*sizeCM);

	double Sd = bStat->getSd();

	// Process scaled covariance matrix
	double tmpHS = 0.;
	for(size_t i=0; i<sizeCM; ++i){
		for(size_t j=0; j<sizeCM; ++j){
			covMat[i*sizeCM+j] *= sqrt(Sd*bStat->getLocSd(i))*sqrt(Sd*bStat->getLocSd(j));
			tmpHS += covMat[i*sizeCM+j];
			if(i==j && covMat[i*sizeCM+j] < 0) {
				cout << "NOTGUD" << endl;
				return;
			}
		}
	}

	// Set new windows size
	if(tmpHS != hashSigma){
		hashSigma = tmpHS;
		block->getBlockModifier()->setWindowSize(covMat);
	}
}

bool MixedAdaptiveBlock::isReadyForLocUpdate() const {
	return bStat->isReadyForLocUpdate();
}

bool MixedAdaptiveBlock::hasLocLambdaConverged() const {
	return bStat->hasLocLambdaConverged();
}

void MixedAdaptiveBlock::updateLocSD(std::vector<double> &localAlphas) {
	bStat->updateLocSD(localAlphas);
}

std::string MixedAdaptiveBlock::getSummaryString() const {
	std::stringstream ss;
	ss << "Mixed adaptive block" << std::endl;
	ss << bStat->getSummaryString();
	return ss.str();
}

} /* namespace Adaptive */
} /* namespace Sampler */
