//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FixedProposalBlock.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include <Sampler/Adaptive/FixedProposalBlock.h>

namespace Sampler {
namespace Adaptive {

FixedProposalBlock::FixedProposalBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel) :
	AdaptiveBlock(aBlock, aModel){
	state = NOT_ADAPTIVE;
}

FixedProposalBlock::~FixedProposalBlock() {
}

void FixedProposalBlock::updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample) {
	return;
}

AdaptiveBlock::conv_signal_t FixedProposalBlock::updateLambda(const double alpha) {
	return NO_SIGNAL;
}

void FixedProposalBlock::create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) {
	return;
}

void FixedProposalBlock::updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
		   	   	   	   	   	   	   	   	   	   	    const std::vector<double> &percConvergedLambda) {
	return;
}

bool FixedProposalBlock::hasConverged() const {
	return true;
}

bool FixedProposalBlock::hasSigmaConverged() const {
	return true;
}

bool FixedProposalBlock::hasLambdaConverged() const {
	return true;
}

bool FixedProposalBlock::isReadyForLocUpdate() const {
	return false;
}

bool FixedProposalBlock::hasLocLambdaConverged() const {
	return true;
}

void FixedProposalBlock::updateLocSD(std::vector<double> &localAlphas) {
	return;
}

std::string FixedProposalBlock::getSummaryString() const {
	std::stringstream ss;
	ss << "Fixed proposal block" << std::endl;
	return ss.str();
}


} /* namespace Adaptive */
} /* namespace Sampler */
