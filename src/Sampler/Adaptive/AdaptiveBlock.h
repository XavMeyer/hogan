//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveBlock.h
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEBLOCK_H_
#define ADAPTIVEBLOCK_H_

#include <vector>

#include "Parallel/Parallel.h"
#include "Sampler/Samples/Sample.h"
#include "ParameterBlock/Block.h"
#include "Model/Model.h"
#include "ParameterBlock/BlockStats/BlockStatsInterface.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"

namespace Sampler {
namespace Adaptive {

namespace CF = ParameterBlock::Config;

class AdaptiveBlock {
public:
	enum state_t {STATELESS=-1, NOT_ADAPTIVE=0, DEFAULT_ADAPT=1, PCA_INDEP=3, PCA_CORREL=4, CONVERGED=5, PCA_INDEP_P2=6};
	enum conv_signal_t {NO_SIGNAL=-1, SIGNAL_INDEP=0, SIGNAL_CORREL=1, SIGNAL_CONV=2};
	typedef boost::shared_ptr<AdaptiveBlock> sharedPtr_t;

public:
	AdaptiveBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel);
	AdaptiveBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
			const CF::ConfigFactory::sharedPtr_t &aCfgFac);
	virtual ~AdaptiveBlock();

	//! Return the block
	const ParameterBlock::Block::sharedPtr_t getBlock() const;

	//! Return the adaptive process state and type
	state_t getState() const;
	//! Return the config factory
	CF::ConfigFactory::sharedPtr_t getCfgFac() const;

	//! Update the covariance matrix Sigma
	virtual void updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample) = 0;
	//! Update the global scaling factor
	virtual conv_signal_t updateLambda(const double alpha) = 0;
	//! Create 1D moves required to the update of local alphas
	virtual void create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) = 0;
	//! Check the convergence of the adaptive phase
	virtual void updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
											const std::vector<double> &percConvergedLambda) = 0;
	//! Return true if the adaptive phase has converged
	virtual bool hasSigmaConverged() const = 0;
	virtual bool hasLambdaConverged() const = 0;

	virtual bool isReadyForLocUpdate() const = 0;
	virtual bool hasLocLambdaConverged() const = 0;

	virtual void updateLocSD(std::vector<double> &localAlphas) = 0;

	virtual std::string getSummaryString() const = 0;

	// Factory
	static sharedPtr_t createFixedProposal(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel);
	static sharedPtr_t createDefault(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
			StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
	static sharedPtr_t createMixed(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
			StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);
	static sharedPtr_t createPCA(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
			StatisticalModel::Model &aModel, CF::ConfigFactory::sharedPtr_t aCfgFac);

protected:
	const Parallel::MCMCManager &mcmcMgr;
	const ParameterBlock::Block::sharedPtr_t block;
	StatisticalModel::Model &model;
	state_t state;
	CF::ConfigFactory::sharedPtr_t cfgFac;

	void createDefault1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples);

	double getBestAlpha(size_t nP, size_t nPPB);

};


} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* ADAPTIVEBLOCK_H_ */
