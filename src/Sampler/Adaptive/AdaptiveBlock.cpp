//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveBlock.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include "AdaptiveBlock.h"
#include "IncAdaptiveBlocks.h"

namespace Sampler {
namespace Adaptive {

AdaptiveBlock::AdaptiveBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel) :
		mcmcMgr(Parallel::mcmcMgr()), block(aBlock), model(aModel), state(STATELESS) {
}

AdaptiveBlock::AdaptiveBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		const CF::ConfigFactory::sharedPtr_t &aCfgFac) :
		mcmcMgr(Parallel::mcmcMgr()), block(aBlock), model(aModel),  state(STATELESS), cfgFac(aCfgFac) {
}

AdaptiveBlock::~AdaptiveBlock() {
}

const ParameterBlock::Block::sharedPtr_t AdaptiveBlock::getBlock() const {
	return block;
}


AdaptiveBlock::state_t AdaptiveBlock::getState() const {
	return state;
}

CF::ConfigFactory::sharedPtr_t AdaptiveBlock::getCfgFac() const {
	return cfgFac;
}

AdaptiveBlock::sharedPtr_t AdaptiveBlock::createFixedProposal(const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel) {
	return sharedPtr_t(new FixedProposalBlock(aBlock, aModel));
}

AdaptiveBlock::sharedPtr_t AdaptiveBlock::createDefault(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return sharedPtr_t(new DefaultAdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}
AdaptiveBlock::sharedPtr_t AdaptiveBlock::createMixed(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return sharedPtr_t(new MixedAdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}

AdaptiveBlock::sharedPtr_t AdaptiveBlock::createPCA(const Sample &initSample,
		const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel,
		CF::ConfigFactory::sharedPtr_t aCfgFac) {
	return sharedPtr_t(new PCA_AdaptiveBlock(initSample, aBlock, aModel, aCfgFac));
}



void AdaptiveBlock::createDefault1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) {
	for(size_t iP=0; iP<block->getPIndices().size(); ++iP){
		Sample newSample(curSample);
		unsigned int aInd = block->getPIndices()[iP];
		double aVal = nDSample.getDoubleParameter(aInd);
		newSample.setDoubleParameter(aInd, aVal);
		newSamples.push_back(newSample);
	}
}


double AdaptiveBlock::getBestAlpha(size_t nP, size_t nPPB){

	uint nAlpha = 1000;
	double endAlpha = 0.999;
	double alphas[nAlpha], E[nAlpha], D[nAlpha];

	for(uint i=0; i<nAlpha; ++i){
		alphas[i] = (1+i)*endAlpha/nAlpha;
	}

	// Tweaking in function of nPPB
	double exponent = (2.-0.9/exp((double)nPPB/10.));

	// Efficiency in function of alphas
	boost::math::normal norm;
	for(uint i=0; i<nAlpha; ++i){
		E[i] = alphas[i]*pow(abs(boost::math::quantile(norm, alphas[i]/2.)), exponent);

		// Gains in function of processors
		D[i] = 1.;
		for(uint iP=2; iP<=nP; ++iP){
			D[i] += pow((1.-alphas[i]), (double)(iP-1));
		}
	}

	// Get overall efficiency and get max value
	int iMax = -1;
	double max = -1.;

	// Get max alpha
	for(uint i=0; i<nAlpha; ++i){
		E[i] *= D[i];
		if(E[i] > max){
			iMax = i;
			max = E[i];
		}
	}

	return alphas[iMax];
}


} /* namespace Adaptive */
} /* namespace Sampler */
