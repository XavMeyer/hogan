//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PCAAdaptiveBlock.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include "PCAAdaptiveBlock.h"

namespace Sampler {
namespace Adaptive {

PCA_AdaptiveBlock::PCA_AdaptiveBlock(const Sample &initSample, const ParameterBlock::Block::sharedPtr_t aBlock,
		StatisticalModel::Model &aModel, const CF::ConfigFactory::sharedPtr_t &aCfgFac) :
				AdaptiveBlock(aBlock, aModel, aCfgFac) {
	init(initSample);
}

PCA_AdaptiveBlock::~PCA_AdaptiveBlock() {
}

void PCA_AdaptiveBlock::init(const Sample &aSample) {
	// Got this from mesures (best alpha in function of nP)
	unsigned int nProp = mcmcMgr.getNProposal();

	state = PCA_INDEP;

	alpha = AdaptiveBlock::getBestAlpha(nProp, block->size());
	//std::cout << block->getId() << " :: target alpha : " << alpha << std::endl;

	assert(cfgFac != NULL);
	bStat.reset(new ParameterBlock::EvoPCABS(alpha, block, cfgFac->createConfig(block->size())));
	bStat->initSample(aSample);

	hashSigma = -1.;
}


void PCA_AdaptiveBlock::updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample) {
	if(!accepted){
		bStat->update(oldSample, 1);
	} else {
		bStat->update(curSample, 1);
	}
	/*if(block->getId() == 7) { // FIXME
		std::cout << "Sd : " << bStat->getSd() << std::endl;
		std::cout << "OS : " <<  oldSample.toString() << std::endl;
		std::cout << "CS : " << curSample.toString() << std::endl;
		std::stringstream ss;
		for(size_t i=0; i< block->size()*block->size(); ++i) {
			ss << bStat->getSigma()[i] << "\t";
		}
		ss << std::endl;
		std::cout << ss.str() << std::endl;
	}*/
}

AdaptiveBlock::conv_signal_t PCA_AdaptiveBlock::updateLambda(const double aAlpha) {
	using ParameterBlock::BlockModifier;
	conv_signal_t signal = NO_SIGNAL;

	if(bStat->isReady()){
		bStat->updateSD(aAlpha);

		if(state == PCA_INDEP){ // Update independent parameters
			updateIndepWindows();
			if(bStat->hasSigmaConverged()){

				// Check if there is some correlations
				if(bStat->hasCorrelation() && !bStat->hasDisabledFactors()){
					// We try toc reate a PCA proposal if there is correlation
					if(bStat->computeSigmaEIG() == bStat->SUCCESS) {
						signal = SIGNAL_CORREL;

						// Reset factor localSd and so on.
						bStat->resetForCorrel();

						// Get Scaled matrix S
						std::vector<double> scaledS;
						computeScaledS(scaledS);

						// Create new windows modifier
						block->setBlockModifier(BlockModifier::createPCAWindow(block->size(), scaledS, model.getParams()));
						for(size_t iParam=0; iParam<block->size(); ++iParam){
							block->setBlockModifier(iParam,  BlockModifier::sharedPtr_t());
						}
						state = PCA_CORREL;
					} else {
						if(Parallel::mpiMgr().isMainProcessor()) {
							cerr << "[Warning] in AdaptiveBlock::conv_signal_t PCA_AdaptiveBlock::updateLambda(const double alpha);" <<endl;
							cerr << "Eigen values no ok for block : " << block->getName() << " -> reset block." <<endl;
						}
						// Fully reset, not possible to get this kind of covariance
						bStat->reset();
					}
				} else {
					// Otherwise we stop
					bStat->resetForIndep();
					state = PCA_INDEP_P2;
					signal = SIGNAL_INDEP;
				}
			}
		} else if(state == PCA_CORREL) { // Update PCA proposals
			updateCorrelWindows();
			if(bStat->hasSigmaConverged() && bStat->hasLambdaConverged()) {
				state = CONVERGED;
				signal = SIGNAL_CONV;
			}
		} else if(state == PCA_INDEP_P2) { // Finish the second indep phase (for increased accuracy)
			updateIndepWindows();
			if(bStat->hasSigmaConverged() && bStat->hasLambdaConverged()) {
				state = CONVERGED;
				signal = SIGNAL_CONV;
			}
		} else {
			if(Parallel::mpiMgr().isMainProcessor()) {
				std::cerr << "[Error] in AdaptiveBlock::conv_signal_t PCA_AdaptiveBlock::updateLambda(const double alpha);" << std::endl;
				std::cerr << "Block [" << block->getName() << "is in an incorrect state." << std::endl;
				abort();
			}
		}
	}

	return signal;
}

void PCA_AdaptiveBlock::create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples) {
	if(state == PCA_CORREL) {
		createSamplesPCA(curSample, newSamples);
	} else {
		AdaptiveBlock::createDefault1DMoves(curSample, nDSample, newSamples);
	}
}

void PCA_AdaptiveBlock::updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
												   const std::vector<double> &percConvergedLambda) {

	assert(percConvergedSigma.size() >= 2 && percConvergedLambda.size() >= 2);
	double pSigmaP1 = percConvergedSigma[0];
	double pSigmaP2 = percConvergedSigma[1];
	double pLambdaP1 = percConvergedLambda[0];
	double pLambdaP2 = percConvergedLambda[1];

	double relaxTolSigmaP1 = 2.*std::max(1.0, pow(pSigmaP1/50.,2));
	double relaxTolLambdaP1 = 2.*std::max(1.0, pow((pSigmaP1 + pLambdaP1)/100.,3));
	double relaxTolSigmaP2 = std::max(1.0, pow(pSigmaP2/40.,2));
	double relaxTolLambdaP2 = std::max(1.0, pow((pSigmaP2 + pLambdaP2)/80.,3));

	if(state == PCA_INDEP){
		bStat->relaxToleranceSigma(relaxTolSigmaP1, 2.5*relaxTolSigmaP1);
		bStat->relaxToleranceLambda(relaxTolLambdaP1);
	} else if (state == PCA_CORREL){
		bStat->relaxToleranceSigma(relaxTolSigmaP2, relaxTolSigmaP2);
		bStat->relaxToleranceLambda(relaxTolLambdaP2);
	} else if (state == PCA_INDEP_P2){
		bStat->relaxToleranceSigma(relaxTolSigmaP2, 4.*relaxTolSigmaP2);
		bStat->relaxToleranceLambda(relaxTolLambdaP2);
	}
}

bool PCA_AdaptiveBlock::hasSigmaConverged() const {
	return bStat->hasSigmaConverged();
}

bool PCA_AdaptiveBlock::hasLambdaConverged() const {
	return bStat->hasLambdaConverged();
}

bool PCA_AdaptiveBlock::isReadyForLocUpdate() const {
	return bStat->isReadyForLocUpdate();
}

bool PCA_AdaptiveBlock::hasLocLambdaConverged() const {
	return bStat->hasLocLambdaConverged();
}

void PCA_AdaptiveBlock::updateLocSD(std::vector<double> &localAlphas) {
	bStat->updateLocSD(localAlphas);
}

void PCA_AdaptiveBlock::updateIndepWindows() {
	double Sd = bStat->getSd();
	for(uint iP=0; iP<block->getPIndices().size(); ++iP){
		double var = Sd*bStat->getLocSd(iP)*bStat->getSigmaIdx(iP);

		if(var > 0.){
			assert(block->hasBlockModifier(iP));
			block->getBlockModifier(iP)->setWindowSize(std::vector<double>(1,sqrt(var)));
		}
	}
}

void PCA_AdaptiveBlock::updateCorrelWindows() {
	std::vector<double> scaledSMatrix;
	double tmpHS = computeScaledS(scaledSMatrix);

	// Set new windows size
	if(tmpHS != hashSigma){
		hashSigma = tmpHS;
		block->getBlockModifier()->setWindowSize(scaledSMatrix);
	}
}

double PCA_AdaptiveBlock::computeScaledS(vector<double> &matrix) const {

	const uint iB = block->getId();
	size_t sizeCM = block->size();

	bStat->computeSigmaEIG();
	const std::vector<double> &eigL = bStat->getValuesEIG();
	const std::vector<double> &eigV = bStat->getVectorsEIG();

	matrix = eigV;

	double Sd = bStat->getSd();

	// Process scaled covariance matrix
	double tmpHS = 0.;
	for(size_t i=0; i<sizeCM; ++i){
		const double locSd = bStat->getLocSd(i);
		const double eVal = eigL[i];
		if(eVal < 1e-300) {
			cerr << "Block : " << iB << " eigL[" << i << "] = " << eigL[i] << endl;
		}

		for(size_t j=0; j<sizeCM; ++j){
			matrix[i*sizeCM+j] *= sqrt(eVal*locSd*Sd);
			tmpHS += matrix[i*sizeCM+j];
		}
	}
	return tmpHS;
}


void PCA_AdaptiveBlock::createSamplesPCA(const Sample &curSample, std::vector<Sample> &newSamples) const {
	using ParameterBlock::EllipsoidReflection;

	const uint n = block->getPIndices().size();

	std::vector<double> scaledS;
	computeScaledS(scaledS);

	std::vector<double>buffer, result(n, 0.);
	buffer = Parallel::mpiMgr().getPRNG()->genStandardNormal(n);

	// For each dimension of the block
	for(size_t iP=0; iP<block->size(); ++iP){
		Sample baseSample(curSample);

		std::vector<double> incr;
		// For each component of the PCA
		for(size_t iB=0; iB<block->size(); ++iB){ // loop over component
			incr.push_back(buffer[iP]*scaledS[iP*block->size()+iB]);
		}

		std::vector<double> X;
		// Sample the parameter
		for(size_t iB=0; iB < block->size(); ++iB){
			double val = baseSample.getDoubleParameter(block->getPIndices()[iB]);
			X.push_back(val);
		}

		// Apply the Ellipsoid reflection
		EllipsoidReflection er(X, incr, model.getParams(), block->getPIndices());
		X = er.getReflectedValues();


		// Create the new sample
		bool areSame = true;
		for(size_t iB=0; iB < block->size(); ++iB){
			areSame = areSame && (baseSample.getDoubleParameter(block->getPIndices()[iB]) == X[iB]);
			baseSample.setDoubleParameter(block->getPIndices()[iB], X[iB]);
		}
		if(areSame) {
			std::cerr << "Same" << std::endl;
			std::cerr << baseSample.toString() << std::endl;
			for(size_t i=0; i < X.size(); i++) std::cerr << X[i] << "\t";
			std::cerr << std::endl;
			for(size_t i=0; i < incr.size(); i++) std::cerr << incr[i] << "\t";
			std::cerr << std::endl;
		}

		newSamples.push_back(baseSample);
	}

}
std::string PCA_AdaptiveBlock::getSummaryString() const {
	std::stringstream ss;
	ss << "PCA adaptive block" << std::endl;
	ss << bStat->getSummaryString();
	return ss.str();
}

} /* namespace Adaptive */
} /* namespace Sampler */
