//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FixedProposalBlock.h
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#ifndef FIXEDPROPOSALBLOCK_H_
#define FIXEDPROPOSALBLOCK_H_

#include "AdaptiveBlock.h"

namespace Sampler {
namespace Adaptive {

class FixedProposalBlock : public AdaptiveBlock {
public:
	FixedProposalBlock(const ParameterBlock::Block::sharedPtr_t aBlock, StatisticalModel::Model &aModel);
	~FixedProposalBlock();

	//! Update the covariance matrix Sigma
	void updateSigma(const bool accepted, const Sample &oldSample, const Sample &curSample);
	//! Update the global scaling factor
	AdaptiveBlock::conv_signal_t updateLambda(const double alpha);
	//! Create 1D moves required to the update of local alphas
	void create1DMoves(const Sample &curSample, const Sample &nDSample, std::vector<Sample> &newSamples);
	//! Check the convergence of the adaptive phase
	void updateRelaxationTolerances(const std::vector<double> &percConvergedSigma,
			   	   	   	   	   	    const std::vector<double> &percConvergedLambda);
	//! Return true if the adaptive phase has converged
	bool hasConverged() const;
	bool hasSigmaConverged() const;
	bool hasLambdaConverged() const;

	bool isReadyForLocUpdate() const;
	bool hasLocLambdaConverged() const;

	void updateLocSD(std::vector<double> &localAlphas);

	std::string getSummaryString() const;

};

} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* FIXEDPROPOSALBLOCK_H_ */
