//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PFAMCMC.cpp
 *
 * @date Nov 8, 2015
 * @author meyerx
 * @brief
 */
#include "PFAMCMC.h"
#include "Strategy/AcceptanceStrategy/BaseMHAS.h"
#include "Utils/Code/SerializationSupport.h"

#include "Model/Likelihood/TreeInference/Base.h"


#define _VOLATILE
#define TIME_MEASURE

namespace Sampler {

const uint PFAMCMC::NB_BLOCK_RELAXATION_TH = 4;

PFAMCMC::PFAMCMC(Sample &initSample, Model &aModel, BlockSelector::sharedPtr_t aBS,
				 ModifierStrategy::sharedPtr_t aMS, AcceptanceStrategy::sharedPtr_t aAS,
				 ConfigFactory::sharedPtr_t aCfgFac) :
		MCMC(initSample, aModel), blockSel(aBS), modStrat(aMS), acceptStrat(aAS), cfgFac(aCfgFac) {

	nAdaptiveBlocks = 0;

	initTimes();

	if(!mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		vector<Sample> s; s.push_back(samples.front());
		samples.clear();
		exchangeSample(mcmcMgr.getManagerProposal(), 0, s);
		exchangeState(mcmcMgr.getManagerProposal(), 0);
	} else {
		samples.back().setChanged(true);
		curSample = samples.front();
		curSample.setChanged(true);
		if(model.getLikelihood()->isUpdatable()){
			model.getLikelihood()->getStateLH(curState);
		}
	}

	initAdaptive();

	mc3_rng = RNG::createSitmo11RNG(1234567);
}

PFAMCMC::~PFAMCMC() {
	// MC3
	if(mcmcMgr.isManagerMC3()) {
		delete evoMC3;
	}
}

void PFAMCMC::initTimes(){
	timeP1 = 0.;
	timeP2 = 0.;
	timeI = 0.;
	timeB = 0.;
	timeB2 = 0.;
	timeC = 0.;
	timeTot = 0.;
	timeP1_1 = 0.;
	timeP1_2 = 0.;
	timeP1_3 = 0.;
	timeMC3 = 0.;
	timeW = 0.;
}


void PFAMCMC::initAdaptive() {
	convIt = convS = 0;
	fullConvergence = false;

	for(uint iB=0; iB<model.getBlocks().size(); ++iB){
		const Block::sharedPtr_t block = model.getBlocks().getBlock(iB);

		if(block->isNotAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::AdaptiveBlock::createFixedProposal(block, model));
		} else if(block->isSingleDoubleAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::AdaptiveBlock::createDefault(curSample, block, model, cfgFac));
			nAdaptiveBlocks++;
		} else if(block->isMixedDoubleAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::AdaptiveBlock::createMixed(curSample, block, model, cfgFac));
			nAdaptiveBlocks++;
		} else if(block->isPCADoubleAdaptive()) {
			adaptiveBlocks.push_back(Adaptive::AdaptiveBlock::createPCA(curSample, block, model, cfgFac));
			nAdaptiveBlocks++;
		}
	}

	// MC3
	if(mcmcMgr.isActiveMC3()) {
		cfgMC3 = cfgFac->createConfig(1);
		evoMC3 = new BatchEvoRU(0.15, 1, cfgMC3->MC3, cfgMC3->CONV_CHECKER);
	}
}


void PFAMCMC::processNextIteration(){

	int acceptedId = 0;
	int nReject = 0;
	vector<double> alphas;

	TIME_MEASURE _VOLATILE double s1 = 0., sP1 = 0., sP2 = 0., sP1_1 = 0., sP1_2 = 0., sP1_3 = 0.;
	TIME_MEASURE _VOLATILE double e1 = 0., eP1 = 0., eP2 = 0., eP1_1 = 0., eP1_2 = 0., eP1_3 = 0.;

	TIME_MEASURE s1 = MPI_Wtime();

	// If no roles, return
	if(mcmcMgr.checkRoleProposal(mcmcMgr.None)) return;

	/*if(Parallel::mpiMgr().isMainProcessor() && ((iT+1)%50 == 0)) {
		double sum = 0;
		for(size_t iB=3; iB<model.getBlocks().size(); ++iB) {
			sum += model.getBlocks().getBlock(iB).getTotalTime();
		}
		sum /= (model.getBlocks().size()-3);
		std::cout << "Average time per branch length block = " << sum << std::endl;
	}*/

	// Save curSample
	curSample.setChanged(false);
	oldSample = curSample;

	if(doMC3()) return;

	// ALL : Get block
	TIME_MEASURE _VOLATILE double sI1 = MPI_Wtime();
	const int myIdProposal = mcmcMgr.getRankProposal();
	const vector<int> iBlocks(blockSel->selectBlocks(curSample));
	const Block::sharedPtr_t block = model.getBlocks().getBlock(iBlocks[myIdProposal]);

	// ALL : Generate sample(s)
	vector<Sample> propSamples;
	modStrat->proposeNextSamples(block, curSample, propSamples);
	TIME_MEASURE _VOLATILE double eI1 = MPI_Wtime();
	TIME_MEASURE timeI += eI1 - sI1;

#ifdef DBG_PREDICTIVE
	DBG_logPredictive(block, iBlocks, propSamples);
#endif

	// ALL : Process sample(s)
	TIME_MEASURE _VOLATILE double sC1 = MPI_Wtime();
	if(model.getLikelihood()->isUpdatable()){
		processSample(block->getPIndices(), propSamples);
	} else {
		processSample(propSamples);
	}

	// ALL Process score(s)
	vector<double> scores(acceptStrat->getScores(curSample, block, propSamples));

	block->updateMeanTimes(eI1 - sI1, MPI_Wtime()-sC1); // Do not take into account the wasted time

	TIME_MEASURE _VOLATILE double sW1 = MPI_Wtime();
	TIME_MEASURE mcmcMgr.syncProposals(); // FIXME
	TIME_MEASURE _VOLATILE double eW1 = MPI_Wtime();
	TIME_MEASURE timeW += eW1 - sW1;

	TIME_MEASURE _VOLATILE double eC1 = MPI_Wtime();
	TIME_MEASURE timeC += eC1-sC1;

	if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {

		// Do the acceptance check and update based on blocks, proposed samples and scores.
		doSequentialAcceptanceAndUpdate(iBlocks, propSamples, scores);

		TIME_MEASURE e1 = MPI_Wtime();
		TIME_MEASURE timeTot += e1-s1;

		return;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager)) { // MASTER
		// Gather scores
		size_t nScores = mcmcMgr.getNProposal()*propSamples.size();
		vector<double> globalScores(nScores, 0.);

		TIME_MEASURE sP1 = MPI_Wtime();
		TIME_MEASURE sP1_1 = MPI_Wtime();
		mcmcMgr.gatherScores(scores.size(), &scores[0], &globalScores[0]);
		TIME_MEASURE eP1_1 = MPI_Wtime();
		TIME_MEASURE timeP1_1 += eP1_1 - sP1_1;

		// Check acceptance
		TIME_MEASURE sP1_2 = MPI_Wtime();
		acceptedId = acceptanceCheck(globalScores, nReject);

		// Send one mean alpha per block
		vector<double> buffer;
		if(hasConverged()) {
			buffer.assign(2, 0.);
		} else {
			buffer.assign(2+iBlocks.size(), 0.);
			prepareScores(propSamples.size(), globalScores, alphas);
			if(block->size() == 1 && model.getParams().isOverflowed(block->getPIndices()[0])){
				alphas[0] *= -1.; // In case of overflow : notify the block
			}
		}

		buffer[0] = acceptedId;
		buffer[1] = nReject;
		for(uint iB=2; iB<buffer.size(); ++iB){
			buffer[iB] = alphas[iB-2];
		}
		TIME_MEASURE eP1_2 = MPI_Wtime();
		TIME_MEASURE timeP1_2 += eP1_2 - sP1_2;

		// Broadcast the acceptance check result
		TIME_MEASURE sP1_3 = MPI_Wtime();
		int root = mcmcMgr.getManagerProposal();
		mcmcMgr.bcastProposal(buffer.size(), &buffer[0], root);
		TIME_MEASURE eP1_3 = MPI_Wtime();
		TIME_MEASURE timeP1_3 += eP1_3 - sP1_3;

		TIME_MEASURE eP1 = MPI_Wtime();
		TIME_MEASURE timeP1 += eP1 - sP1;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // SLAVE
		// Send Score(s)
		double *dummyPtr = NULL;
		mcmcMgr.gatherScores(scores.size(), &scores[0], dummyPtr);

		vector<double> buffer;
		if(hasConverged()) {
			buffer.assign(2, 0.);
		} else {
			buffer.assign(2+iBlocks.size(), 0.);
		}

		// Get acceptance check result
		int dest = mcmcMgr.getManagerProposal();
		mcmcMgr.bcastProposal(buffer.size(), &buffer[0], dest);
		acceptedId = buffer[0];
		nReject = buffer[1];

		if(!hasConverged()){
			for(uint iB=2; iB<buffer.size(); ++iB){
				alphas.push_back(buffer[iB]);
			}
		}
	}

	TIME_MEASURE sP2 = MPI_Wtime();
	int aRank, aRow;
	defineSamplePosition(acceptedId, aRank, aRow, propSamples.size());

	// ALL : Exchange sample
	exchangeSample(aRank, aRow, propSamples);

	// ALL :Exchange state
	exchangeState(aRank, aRow);
	TIME_MEASURE eP2 = MPI_Wtime();
	TIME_MEASURE timeP2 += eP2 - sP2;

	// ALL : adapt blocks
	TIME_MEASURE _VOLATILE double sB1 = MPI_Wtime();
	adaptBlocks(iBlocks, acceptedId, alphas);
	TIME_MEASURE _VOLATILE double eB1 = MPI_Wtime();
	TIME_MEASURE timeB += eB1-sB1;

	// ALL : Update local alphas
	TIME_MEASURE _VOLATILE double sB2 = MPI_Wtime();
	updateLocalAlphas(iBlocks);
	TIME_MEASURE _VOLATILE double eB2 = MPI_Wtime();
	TIME_MEASURE timeB2 += eB2-sB2;


	TIME_MEASURE e1 = MPI_Wtime();
	TIME_MEASURE timeTot += e1-s1;

}

void PFAMCMC::doSequentialAcceptanceAndUpdate(const vector<int> &iBlocks, const vector<Sample> &propSamples, const vector<double> &scores) {
	vector<double> alphas;
	int nReject = 0;

	const int myIdProposal = mcmcMgr.getRankProposal();
	const Block::sharedPtr_t block = model.getBlocks().getBlock(iBlocks[myIdProposal]);

	TIME_MEASURE double sAcc = MPI_Wtime();

	// Do acceptance check
	int acceptedId = acceptanceCheck(scores, nReject);

	// If accepted update likelihood
	if(acceptedId >= 0){
		if(model.getLikelihood()->isUpdatable()) {
			model.getLikelihood()->getStateLH(curState);
		}
		if(mcmcMgr.isOutputManager()) {
			samples.push_back(propSamples[acceptedId]);
			samples.back().setChanged(true);
		}

		curSample = propSamples[acceptedId];
		curSample.setChanged(true);
	} else if(model.getLikelihood()->isUpdatable()) {
		model.getLikelihood()->setStateLH(curState);
	}

	TIME_MEASURE double eAcc = MPI_Wtime();
	TIME_MEASURE timeP1 += eAcc-sAcc;

	// Prepare the score for the adaptive phase
	TIME_MEASURE _VOLATILE double sB1 = MPI_Wtime();
	prepareScores(propSamples.size(), scores, alphas);

	if(block->size() == 1 && model.getParams().isOverflowed(block->getPIndices()[0])){
		alphas[0] *= -1.; // In case of overflow : notify the block
	}

	// Adapt the blocks
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		bool isMyBlock = myIdProposal == (int)iB;
		bool accepted = (acceptedId == (int)iB);
		bool rejected = (acceptedId == -1) || ((int)iB < acceptedId);
		if(accepted) {
			model.getBlocks().getBlock(iBlocks[iB])->accepted(isMyBlock);
			adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
		} else if(rejected) {
			model.getBlocks().getBlock(iBlocks[iB])->rejected(isMyBlock);
			adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
		} else { // If not accepted or rejected : then its thrown away
			model.getBlocks().getBlock(iBlocks[iB])->thrown(isMyBlock);
		}
	}

	for(uint iB=0; iB<iBlocks.size(); ++iB){
		size_t idBlock = iBlocks[iB];
		displayBlockConvergence(idBlock, adaptiveBlocks[idBlock]->updateLambda(alphas[iB]));
	}
	checkConvergence();

	//if(acceptedId >= 0){
 	blockSel->rewind(acceptedId);
	//}

	TIME_MEASURE _VOLATILE double eB1 = MPI_Wtime();
	TIME_MEASURE timeB += eB1-sB1;

	// ALL : Update local alphas
	TIME_MEASURE _VOLATILE double sB2 = MPI_Wtime();
	updateLocalAlphas(iBlocks);
	TIME_MEASURE _VOLATILE double eB2 = MPI_Wtime();
	TIME_MEASURE timeB2 += eB2-sB2;

}

void PFAMCMC::adaptBlocks(const vector<int> &iBlocks, const int acceptedId, const vector<double> &alphas) {

	const int myIdProposal = mcmcMgr.getRankProposal();

	for(uint iB=0; iB<iBlocks.size(); ++iB){ // for each block
		bool isMyBlock = myIdProposal == (int)iB;
		bool accepted = (acceptedId == (int)iB);
		bool rejected = (acceptedId == -1) || ((int)iB < acceptedId);
		if(accepted) {
			model.getBlocks().getBlock(iBlocks[iB])->accepted(isMyBlock);
			if(!hasConverged()) {
				adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
			}
		} else if(rejected) {
			model.getBlocks().getBlock(iBlocks[iB])->rejected(isMyBlock);
			if(!hasConverged()) {
				adaptiveBlocks[iBlocks[iB]]->updateSigma(accepted, oldSample, curSample);
			}
		} else { // If not accepted or rejected : then its thrown away
			model.getBlocks().getBlock(iBlocks[iB])->thrown(isMyBlock);
		}
	}

	if(!hasConverged()) {
		for(uint iB=0; iB<iBlocks.size(); ++iB){
			size_t idBlock = iBlocks[iB];
			displayBlockConvergence(idBlock, adaptiveBlocks[idBlock]->updateLambda(alphas[iB]));
		}
		checkConvergence();
	}

	blockSel->rewind(acceptedId);
}

void PFAMCMC::processSample(vector<Sample> &proposedSamples) {
	for(size_t iS=0; iS<proposedSamples.size(); ++iS){
		// Process prior value
		proposedSamples[iS].prior = model.processPriorValue(proposedSamples[iS]);
		if(isPriorPossible(proposedSamples[iS])){
			// Then we process the likelihood value
			proposedSamples[iS].likelihood = model.getLikelihood()->processLikelihood(proposedSamples[iS]);
			proposedSamples[iS].setMarkers(model.getLikelihood()->getMarkers());

			proposedSamples[iS].posterior = processPosterior(proposedSamples[iS].prior, proposedSamples[iS].likelihood);
		}
	}
}

void PFAMCMC::processSample(const vector<size_t> &pInd, vector<Sample> &proposedSamples) {
	if(proposedSamples.size() == 1) {
		// Process prior value
		proposedSamples.back().prior = model.processPriorValue(proposedSamples.back());
		if(isPriorPossible(proposedSamples.back())){
			// Then we process the likelihood value
			proposedSamples.back().likelihood = model.getLikelihood()->update(pInd, proposedSamples.back());
			proposedSamples.back().setMarkers(model.getLikelihood()->getMarkers());

			proposedSamples.back().posterior = processPosterior(proposedSamples.back().prior, proposedSamples.back().likelihood);
		}
	} else {
		for(size_t iS=0; iS<proposedSamples.size(); ++iS){
			// Set state
			if(iS != 0){
				model.getLikelihood()->setStateLH(curState);
			}
			// Process prior value
			proposedSamples[iS].prior = model.processPriorValue(proposedSamples[iS]);
			if(isPriorPossible(proposedSamples[iS])){
				// Then we process the likelihood value
				proposedSamples[iS].likelihood = model.getLikelihood()->update(pInd, proposedSamples[iS]);
				proposedSamples[iS].setMarkers(model.getLikelihood()->getMarkers());

				proposedSamples[iS].posterior = processPosterior(proposedSamples[iS].prior, proposedSamples[iS].likelihood);
			}
		}
	}
}

int PFAMCMC::acceptanceCheck(const vector<double> &scores, int &nReject) {
	vector<int> accepted = acceptStrat->acceptanceCheck(scores);
	// Check for acceptance
	nReject = 0;
	if(!accepted.empty()){
		for(vector<int>::iterator itA=accepted.begin(); itA != accepted.end(); ++itA){
			if(*itA < 0) {
				for(int i=0; i<abs(*itA); ++i){
					if(mcmcMgr.isOutputManager()) {
						samples.push_back(samples.back());
						samples.back().setChanged(false);
					}
					++nReject;
				}
			} else {
				return *itA;
			}
		}
	} else {
		if(mcmcMgr.isOutputManager()) {
			samples.push_back(samples.back());
			samples.back().setChanged(false);
		}
		++nReject;
	}
	return -1;
}

void PFAMCMC::defineSamplePosition(const int acceptId, int &aRank, int &aRow, const int propSize) {
	if(acceptId < 0) {
		aRank = -1;
		aRow = -1;
	} else {
		aRank = floor((float)acceptId / propSize); // Rank in Proposal communicator are 0...NProp-1
		aRow = acceptId % propSize;
	}
}

void PFAMCMC::exchangeSample(const int aRank, const int aRow, const vector<Sample> &propSamples) {

	if(aRank < 0 || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential) || mcmcMgr.checkRoleProposal(mcmcMgr.None)) {
		//pMCMC::mpiMgr().print("here");
		return;
	}

	// A sample has been accepted :
	Sample newSample;
	char data[serializedSizeMC3];

	// aRank send the serialized sample
	if(mcmcMgr.getRankProposal() == aRank) {
		newSample = propSamples[aRow];
		newSample.serialize(data);
		mcmcMgr.bcastProposal(serializedSizeMC3, data, aRank);
	} else { // Other recieve it
		mcmcMgr.bcastProposal(serializedSizeMC3, data, aRank);
		newSample.initFromSerialized(data);
	}

	// Update the current state
	curSample = newSample;
	curSample.setChanged(true);

	// Manager must update the sample file
	if(mcmcMgr.isOutputManager()) {
		samples.push_back(newSample);
		samples.back().setChanged(true);
	}
}

void PFAMCMC::exchangeState(const int aRank, const int aRow) {

	// If the likelihood is not update prone or a state of size 0
	// or we are in sequential we quit this function
	if(!model.getLikelihood()->isUpdatable() || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)
			|| mcmcMgr.checkRoleProposal(mcmcMgr.None) || model.getLikelihood()->stateSize() == 0) {
		return;
	}

	// Exchange the rank of processor whom sample as been accepted
	// If there has been an acceptance
	if(aRank >= 0) {
		int size = static_cast<int>(model.getLikelihood()->stateSize());
		// If i'm the one who have produced the sample
		if(mcmcMgr.getRankProposal() == aRank) {
			// I get and send the state
			model.getLikelihood()->getStateLH(curState);

			mcmcMgr.bcastProposal(size, curState, aRank);
		} else { // else I receive and set the state
			mcmcMgr.bcastProposal(size, curState, aRank);

			model.getLikelihood()->setStateLH(curState);
		}
	} else {
		model.getLikelihood()->setStateLH(curState);
	}
}

void PFAMCMC::prepareScores(const uint nPropSample, const vector<double> &scores, vector<double> &alphas) const {
	uint nProp = mcmcMgr.getNProposal();
    alphas.assign(nProp, 0.);

    // Get the alphas score given the acceptance score
	for(size_t iB=0; iB<nProp; ++iB){
		// if multiple sample where proposed, alphas[i] define the average acceptance rate
		for(size_t iP=0; iP<nPropSample; ++iP){
			alphas[iB] += (std::min(scores[iB*nPropSample+iP], 1.0));
		}
		alphas[iB] /= (double) nPropSample;
	}

}

bool PFAMCMC::hasConverged() const {
	return fullConvergence;
}

void PFAMCMC::displayBlockConvergence(const size_t iB, Adaptive::AdaptiveBlock::conv_signal_t convSignal) const {
	using Adaptive::AdaptiveBlock;

	if(convSignal == AdaptiveBlock::NO_SIGNAL) return;

	if(!fullConvergence && verbLvl >= 2 && Parallel::mpiMgr().isMainProcessor()) {

		AdaptiveBlock::state_t state = adaptiveBlocks[iB]->getState();
		if(state == AdaptiveBlock::DEFAULT_ADAPT) {
			if(convSignal == AdaptiveBlock::SIGNAL_CONV) {
				std::cout << "[" << iT << " :: " << this->getNWSamples()  << "] Convergence for block : ";
				std::cout << model.getBlocks().getBlock(iB)->getName() << std::endl;
			}
		} else if(state == AdaptiveBlock::PCA_INDEP || state == AdaptiveBlock::PCA_INDEP_P2 ||
				  state == AdaptiveBlock::PCA_CORREL || state == AdaptiveBlock::CONVERGED) {
			if(convSignal == AdaptiveBlock::SIGNAL_INDEP) {
				std::cout << "[" << iT << " :: " << this->getNWSamples()  << "] No correlations detected in block : ";
				std::cout << model.getBlocks().getBlock(iB)->getName() << std::endl;
			} else if(convSignal == AdaptiveBlock::SIGNAL_CORREL) {
				std::cout << "[" << iT << " :: " << this->getNWSamples()  << "] Correlations detected in block : ";
				std::cout << model.getBlocks().getBlock(iB)->getName() << std::endl;
			} else if(convSignal == AdaptiveBlock::SIGNAL_CONV) {
				std::cout << "[" << iT << " :: " << this->getNWSamples()  << "] Convergence for block : ";
				std::cout << model.getBlocks().getBlock(iB)->getName() << std::endl;
			}
		}
	}
}

bool PFAMCMC::checkConvergence() {
	using Adaptive::AdaptiveBlock;

	if(iT % 10 == 0 && !fullConvergence){
		uint nSigmaP1=0, nLambdaP1=0;
		uint nSigmaP2=0, nLambdaP2=0;
		for(size_t iB=0; iB<adaptiveBlocks.size(); ++iB){

			bool hasSigConv = adaptiveBlocks[iB]->hasSigmaConverged();
			bool hasLambConv = adaptiveBlocks[iB]->hasLambdaConverged();

			AdaptiveBlock::state_t state = adaptiveBlocks[iB]->getState();
			bool isP1 = state == AdaptiveBlock::PCA_INDEP;
			bool isP2 = state == AdaptiveBlock::DEFAULT_ADAPT ||
					state == AdaptiveBlock::PCA_INDEP_P2 ||
					state == AdaptiveBlock::PCA_CORREL;
			bool hasConv = state == AdaptiveBlock::CONVERGED;

			if(isP1) {
				if(hasSigConv) nSigmaP1++;
				if(hasLambConv) nLambdaP1++;
			}

			if(isP2) {
				nSigmaP1++;
				nLambdaP1++;
				if(hasSigConv) nSigmaP2++;
				if(hasLambConv) nLambdaP2++;
			}

			if(hasConv) {
				nSigmaP1++;
				nLambdaP1++;
				nSigmaP2++;
				nLambdaP2++;
			}
		}

		std::vector<double> pSigma, pLambda;
		pSigma.push_back(100.*(double)nSigmaP1/(double)nAdaptiveBlocks);
		pSigma.push_back(100.*(double)nSigmaP2/(double)nAdaptiveBlocks);
		pLambda.push_back(100.*(double)nLambdaP1/(double)nAdaptiveBlocks);
		pLambda.push_back(100.*(double)nLambdaP2/(double)nAdaptiveBlocks);

		if(nAdaptiveBlocks >= NB_BLOCK_RELAXATION_TH || (nAdaptiveBlocks < NB_BLOCK_RELAXATION_TH && iT > 2000)) {
			for(size_t iB=0; iB<adaptiveBlocks.size(); ++iB){
				adaptiveBlocks[iB]->updateRelaxationTolerances(pSigma, pLambda);
			}
		}

		if((nSigmaP2 != nAdaptiveBlocks || nLambdaP2 != nAdaptiveBlocks) && iT % 10000 == 0){
			if((Parallel::mpiMgr().isMainProcessor()) && verbLvl >= 2) {
				std::cout << "*** Phase 1 (" << nAdaptiveBlocks << ") : sigma=" << pSigma[0] <<"% (" << nSigmaP1;
				std::cout << ") :: lambda=" << pLambda[0] <<"% (" << nLambdaP1 << ") && ";
				std::cout << "Phase 2 : sigma=" << pSigma[1] <<"% (" << nSigmaP2 << ") :: lambda=";
				std::cout << pLambda[1] <<"% (" << nLambdaP2 << ") ***" << std::endl;
			}
		} else if ((nSigmaP2 == nAdaptiveBlocks && nLambdaP2 == nAdaptiveBlocks)){
			if(Parallel::mpiMgr().isMainProcessor()) {
				if(verbLvl == 2) {
					std::cout << "*** Phase 1 (" << nAdaptiveBlocks << ") : sigma=" << pSigma[0] <<"% (" << nSigmaP1 << ") :: lambda=" << pLambda[0] <<"% (" << nLambdaP1 << ") && ";
					std::cout << "Phase 2 : sigma=" << pSigma[1] <<"% (" << nSigmaP2 << ") :: lambda=" << pLambda[1] <<"% (" << nLambdaP2 << ") ***" << endl;
					std::cout << "FULL CONVERGENCE REACHED at iteration " << iT << " [ " << this->getNWSamples() << " ] !!" << endl;
				}
				convIt = iT;
				convS = this->getNWSamples();
			}
			fullConvergence = true;
		}
	}

	return fullConvergence;
}
void PFAMCMC::updateLocalAlphas(const std::vector<int> &iBlocks) {
	bool doUpdate = false;
	std::vector<int> tmpIBlocks(iBlocks);

	// If at least one block must be upated : then updated all
	std::set<int> toComputeBlock;
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		if(toComputeBlock.count(iBlocks[iB]) == 0) {
			toComputeBlock.insert(iBlocks[iB]);
			if(adaptiveBlocks[iBlocks[iB]]->isReadyForLocUpdate()){
				doUpdate = true;
				break;
			}
		}
	}

	// If the bloc has already converged we remove the computation request
	toComputeBlock.clear();
	size_t cntConv = 0;
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		if(adaptiveBlocks[iBlocks[iB]]->hasLocLambdaConverged()){
			tmpIBlocks[iB] = -1;
			cntConv++;
		} else {
			if (toComputeBlock.count(iBlocks[iB]) == 0) {
				toComputeBlock.insert(iBlocks[iB]);
			} else {
				tmpIBlocks[iB] = -1;
			}
		}
	}
	// Check that there are still some unconverged blocks
	doUpdate = doUpdate && (cntConv != iBlocks.size());

	// If there are some blocks to update
	if(doUpdate){
		// We call the dedicated method that update indep MVN and PCA proposals
		std::vector< std::vector<double> > lmAlpha;
		if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
			lmAlpha = this->updateLocalAlpha(tmpIBlocks);
		} else {
			lmAlpha = this->balancedLocalAlphaUpdate(tmpIBlocks);
			//lmAlpha = this->updateLocalAlpha(tmpIBlocks, propSamples);
		}

		for(uint iB=0; iB<iBlocks.size(); ++iB){
			if (tmpIBlocks[iB] >= 0) {
				adaptiveBlocks[iBlocks[iB]]->updateLocSD(lmAlpha[iB]);
			}
		}
	}
}

std::vector< std::vector<double> > PFAMCMC::updateLocalAlpha(const std::vector<int> &iBlocks) {
	const int myIdProposal = mcmcMgr.getRankProposal();

	char *state = NULL;
	std::vector<double> tmpAlpha(1, 0);

	if(iBlocks[myIdProposal] >= 0) {
		const Block::sharedPtr_t block = model.getBlocks().getBlock(iBlocks[myIdProposal]);
		tmpAlpha.assign(block->getPIndices().size(), 0.);

		if(model.getLikelihood()->isUpdatable()){
			state = new char[model.getLikelihood()->stateSize()];
			model.getLikelihood()->getStateLH(state);
		}

		// ALL : Generate sample(s)
		std::vector<Sample> propSamples;
		modStrat->proposeNextSamples(block, curSample, propSamples);

		// For each proposed sample
		for(size_t iPS=0; iPS<propSamples.size(); ++iPS){
			std::vector<Sample> uniqueDiff;
			// For each index in block
			adaptiveBlocks[iBlocks[myIdProposal]]->create1DMoves(curSample, propSamples[iPS], uniqueDiff);

			// Process samples for 1D moves
			if(model.getLikelihood()->isUpdatable()){
				processSample(block->getPIndices(), uniqueDiff);
				model.getLikelihood()->setStateLH(state);
			} else {
				processSample(uniqueDiff);
			}

			// Process scores for 1D moves
			std::vector<double> scores(acceptStrat->getScores(curSample, block, uniqueDiff));

			// Update local mean
			for(size_t iS=0; iS<block->getPIndices().size(); ++iS){
				tmpAlpha[iS] += std::min(scores[iS], 1.0)/(double)propSamples.size();
			}
		}
		for(size_t iS=0; iS<block->getPIndices().size(); ++iS){
			if(model.getParams().isOverflowed(block->getPIndices()[iS])){
				tmpAlpha[iS] *= -1.;
			}
		}
	}

	// Compute total size and block size for each blocks
	int totalSize = 0;
	int blockSizes[iBlocks.size()];
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		if(iBlocks[iB] >= 0) {
			blockSizes[iB] = model.getBlocks().getBlock(iBlocks[iB])->size();
			totalSize += blockSizes[iB];
		} else {
			blockSizes[iB] = 1;
			totalSize += 1;
		}
	}

	std::vector<double> locAlphas(totalSize, 0.);
	// Everybody share informations
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) {
		locAlphas = tmpAlpha;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // MASTER
		mcmcMgr.exchangeAllProposal(tmpAlpha.size(), &tmpAlpha[0], blockSizes, &locAlphas[0]);
	}

	uint displ = 0;
	std::vector< std::vector <double> > result;
	for(uint iB=0; iB<iBlocks.size(); ++iB){
		double *beg = &locAlphas[displ];
		double *end = beg + blockSizes[iB];
		result.push_back(std::vector<double>(beg, end));
		displ += blockSizes[iB];
	}

	if(model.getLikelihood()->isUpdatable()){
		delete [] state;
	}

	return result;
}

std::vector< std::vector<double> > PFAMCMC::balancedLocalAlphaUpdate(const std::vector<int> &iBlocks) {
	const int myIdProposal = mcmcMgr.getRankProposal();

	// Define jobs
	std::vector< std::pair<size_t, size_t> > jobs;
	for(size_t iB=0; iB<iBlocks.size(); ++iB) {
		if(iBlocks[iB] >= 0) {
			const Block::sharedPtr_t block = model.getBlocks().getBlock(iBlocks[iB]);
			for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
				jobs.push_back(std::pair<size_t, size_t>(iBlocks[iB], iP));
			}
		}
	}

	// Define job attributions
	div_t resDiv = std::div(jobs.size(), mcmcMgr.getNProposal());
	std::vector<size_t> chunkSize, displ;
	for(size_t iP=0; iP<(size_t)mcmcMgr.getNProposal(); ++iP) {
		// Define displacement
		if(iP==0) {
			displ.push_back(0);
		} else {
			displ.push_back(displ.back()+chunkSize.back());
		}

		// Define chunk size
		size_t aChunkSize = resDiv.quot;
		if(iP < (size_t)resDiv.rem) {
			aChunkSize += 1;
		}
		chunkSize.push_back(aChunkSize);
	}

	// Compute alphas
	std::vector<double> myLocAlphas;
	if(chunkSize[myIdProposal] > 0) {
		int iB = -1;
		std::vector<size_t> myPInd;
		for(size_t iJ=displ[myIdProposal]; iJ < displ[myIdProposal]+chunkSize[myIdProposal]; ++iJ) {
			bool hasBlockChanged = (int)jobs[iJ].first != iB;
			bool sameBlockOverAgain = !hasBlockChanged && !myPInd.empty() && jobs[iJ].second < myPInd.back();
			if( hasBlockChanged || sameBlockOverAgain ) { // we change block
				if(iB != -1) {
					// Call computation
					localAlphaComputation(iB, myPInd, myLocAlphas);
				}

				// Reset
				iB = jobs[iJ].first;
				myPInd.clear();
			}
			myPInd.push_back(jobs[iJ].second);
		}
		// Call computation
		localAlphaComputation(iB, myPInd, myLocAlphas);

	} else {
		myLocAlphas.push_back(-1.);
	}

	// Share results
	// Compute total size and block size for each blocks
	int totalSize = 0;
	int msgSizes[chunkSize.size()];
	for(size_t iP=0; iP<chunkSize.size(); ++iP){
		msgSizes[iP] = chunkSize[iP] > 0 ? chunkSize[iP] : 1;
		totalSize += msgSizes[iP];
	}

	std::vector<double> locAlphas(totalSize, 0.);
	// Everybody share informations
	mcmcMgr.exchangeAllProposal(myLocAlphas.size(), &myLocAlphas[0], msgSizes, &locAlphas[0]);

	size_t idx = 0;
	std::vector< std::vector <double> > result;
	for(size_t iB=0; iB<iBlocks.size(); ++iB){
		std::vector<double> blockRes;
		if(iBlocks[iB] >= 0) {
			for(size_t iP=0; iP<model.getBlocks().getBlock(iBlocks[iB])->size(); ++iP) {
				blockRes.push_back(locAlphas[idx]);
				idx++;
			}
		}
		result.push_back(blockRes);
	}

	return result;
}


void PFAMCMC::localAlphaComputation(const size_t iB, const std::vector<size_t> &pInd, std::vector<double> &locAlphas) {
	char *state = NULL;
	const Block::sharedPtr_t block = model.getBlocks().getBlock(iB);

	if(model.getLikelihood()->isUpdatable()){
		state = new char[model.getLikelihood()->stateSize()];
		model.getLikelihood()->getStateLH(state);
	}

	// ALL : Generate sample(s)
	std::vector<Sample> propSamples;
	modStrat->proposeNextSamples(block, curSample, propSamples);

	// For each proposed sample
	std::vector<Sample> uniqueDiff, computeUD;
	// For each index in block
	adaptiveBlocks[iB]->create1DMoves(curSample, propSamples.back(), uniqueDiff);

	for(size_t iP=0; iP<pInd.size(); ++iP) {
		computeUD.push_back(uniqueDiff[pInd[iP]]);
	}

	// Process samples for 1D moves
	if(model.getLikelihood()->isUpdatable()){
		processSample(block->getPIndices(), computeUD);
		model.getLikelihood()->setStateLH(state);
	} else {
		processSample(computeUD);
	}

	// Process scores for 1D moves
	std::vector<double> scores(acceptStrat->getScores(curSample, block, computeUD));

	// Update local mean
	for(size_t iS=0; iS<scores.size(); ++iS){
		double alpha = std::min(scores[iS], 1.0);
		if(model.getParams().isOverflowed(block->getPIndices()[pInd[iS]])){
			alpha *= -1.;
		}
		locAlphas.push_back(alpha);
	}

	if(model.getLikelihood()->isUpdatable()){
		delete [] state;
	}
}


void PFAMCMC::printTimes(){
	if(Parallel::mpiMgr().isMainProcessor()){
		cout << "Number of iteration : " << iT << endl;
		cout << "Time B :\t" << timeB << "\t[ " << timeB/(double)iT << " ]" << endl;
		cout << "Time B2 :\t" << timeB2 << "\t[ " << timeB2/(double)iT << " ]" << endl;
		cout << "Time P1 :\t" << timeP1 << "\t[ " << timeP1/(double)iT << " ]" << endl;
		cout << "Time P2 :\t" << timeP2 << "\t[ " << timeP2/(double)iT << " ]" << endl;
		cout << "Time Tot :\t" << timeTot << "\t[ " << timeTot/(double)iT << " ]" << endl;
	}
	MCMC::printTimes();
}

string PFAMCMC::getPerfReport() const {
	stringstream sstr;
	sstr << "[Final] Number of iteration :\t" << iT << endl;
	sstr << "[Final] Number of samples :\t" << 	this->getNWSamples() << endl;
	sstr << "[Final] Mean sample per iteration :\t" << (float)this->getNWSamples() / (float)iT << endl;
	if(convS != 0 && convIt != 0) {
		sstr << "[Final] Conv at iteration :\t" << convIt << endl;
		sstr << "[Final] Conv at sample :\t" << convS << endl;
		sstr << "[Final] Ratio conv :\t" << (double)convS/(double)this->getNWSamples() << endl;
	} else if(!hasConverged()) {
		sstr << "[Final] [WARNING] ADAPTIVE LEARNING HAS NOT YET CONVERGED!" << endl;
	}
	sstr << "[Final] Time Block Sel + propose sample :\t" << timeI << "\t" << timeI/(double)iT << "\t" << 100*timeI/timeGenSample << endl;
	sstr << "[Final] Time Mu, Sigma, Lambda :\t" << timeB << "\t" << timeB/(double)iT << "\t" << 100*timeB/timeGenSample << endl;
	sstr << "[Final] Time LocLambda :\t" << timeB2 << "\t" << timeB2/(double)iT << "\t" << 100*timeB2/timeGenSample << endl;
	if(mcmcMgr.isProposalSequential()) {
		sstr << "[Final] Time ACC :\t" << timeP1 << "\t" << timeP1/(double)iT << "\t" << 100*timeP1/timeGenSample << endl;
	} else {
		sstr << "[Final] Time COM 1 :\t" << timeP1 << "\t" << timeP1/(double)iT << "\t" << 100*timeP1/timeGenSample << endl;
		sstr << "[Final] Time COM 1_1 :\t" << timeP1_1 << "\t" << timeP1_1/(double)iT << "\t" << 100*timeP1_1/timeGenSample << endl;
		sstr << "[Final] Time COM 1_2 :\t" << timeP1_2 << "\t" << timeP1_2/(double)iT << "\t" << 100*timeP1_2/timeGenSample << endl;
		sstr << "[Final] Time COM 1_3 :\t" << timeP1_3 << "\t" << timeP1_3/(double)iT << "\t" << 100*timeP1_3/timeGenSample << endl;
		sstr << "[Final] Time COM 2 :\t" << timeP2 << "\t" << timeP2/(double)iT << "\t" << 100*timeP2/timeGenSample << endl;
	}
	sstr << "[Final] Time COMPUTE :\t" << timeC << "\t" << timeC/(double)iT << "\t" << 100*timeC/timeGenSample << endl;
	if(!mcmcMgr.isProposalSequential()) {
		sstr << "[Final] >>Time WASTED :\t" << timeW << "\t" << timeW/(double)iT << "\t" << 100*timeW/timeC << endl;
	}
	if(mcmcMgr.isActiveMC3()) {
		sstr << "[Final] Time MC3 :\t" << timeMC3 << "\t" << timeMC3/(double)iT << "\t" << 100*timeMC3/timeGenSample << endl;
		sstr << "[Final] >> Acceptance ratio MC3 :\t " << (double)nAccStepMC3/(double)nStepMC3 << "\t(" << nAccStepMC3  << " / " << nStepMC3 << ")" << endl;
		if(mcmcMgr.getMyChainMC3() > 0) {
			sstr << "[Final] >> Temperature for MC3 :\t" << acceptStrat->getInvTemperature() << " ( Rho= " << evoMC3->getRho() << ")" << endl;
			sstr << "[Final] << MC3 convergence at iteration :\t" << convIterMC3 << std::endl;
		}
	}
	sstr << "[Final] Time for PFAMCMC :\t" << timeTot << "\t" << timeTot/(double)iT << "\t" << 100*timeTot/timeGenSample << endl;
	sstr << "[Final] Time for write :\t" << timeWrite << "\t" << timeWrite/(double)iT << "\t" << 100*timeWrite/timeGenSample << endl;
	sstr << "[Final] Time Total :\t" << timeGenSample << "\t" << timeGenSample/(double)iT  << endl;

	return sstr.str();
}

BlockSelector::sharedPtr_t PFAMCMC::getBlockSel() const {
	return blockSel;
}

ModifierStrategy::sharedPtr_t PFAMCMC::getModStrat() const {
	return modStrat;
}

AcceptanceStrategy::sharedPtr_t PFAMCMC::getAcceptStrat() const {
	return acceptStrat;
}

void PFAMCMC::defineExchange(int &coldChain, int &hotChain, double &unifDbl) {
	const int nChain = mcmcMgr.getNChainMC3();
	// Who will exchange ?
	coldChain = mc3_rng->genUniformInt(0, nChain-2);
	hotChain = coldChain + 1;

	// Generate random for test
	unifDbl = mc3_rng->genUniformDbl();
}

double PFAMCMC::defineExchangeRatio(const int myChain, const int otherChain, char *dataRecv, Sample &newSample) {

	//const double LAMBDA = Strategies::BaseMHAS::LAMBDA;
	char dataSend[serializedSizeMC3];

	//double myPower = 1./(1.+myChain*LAMBDA);
	//double hisPower = 1./(1.+otherChain*LAMBDA);

	bool dummy = false;
	double myInvTemp = acceptStrat->getInvTemperature();
	double hisInvTemp = 0.0;
	packInfoMC3(dataSend, dummy, myInvTemp, curSample);
	mcmcMgr.exchangeSerializedSample(serializedSizeMC3, dataSend, dataRecv, otherChain);
	unPackInfoMC3(dataRecv, dummy, hisInvTemp, newSample);

	/*std::cout << "[" << Parallel::mpiMgr().getRank() << "] it = " << iT << " | " << "myChain = ";
	std::cout <<  mcmcMgr.getMyChainMC3() << " | nChain = " << mcmcMgr.getNChainMC3();
	std::cout << " | temperatures (my, his) = (" << myInvTemp << ", " << hisInvTemp << ")" << std::endl;*/

	double ratio = 0.;
	if(model.getLikelihood()->isLog()) {
		ratio = myInvTemp*newSample.posterior + hisInvTemp*curSample.posterior;
		ratio -= hisInvTemp*newSample.posterior + myInvTemp*curSample.posterior;
		ratio = std::min(1., exp(ratio));
	} else {
		ratio = (pow(newSample.posterior, myInvTemp) * pow(curSample.posterior, hisInvTemp));
		ratio /= (pow(newSample.posterior, hisInvTemp) * pow(curSample.posterior, myInvTemp));
		ratio = std::min(1., ratio);
	}

	// Adaptive (See "Adaptive Parallel Tempering Algorithm" Miasojedow et al. 2012)
	if(myChain > otherChain) { 								// If I'm the hot chain
		if(!evoMC3->hasConverged() || !hasConverged()) { 	// - If not converged then (wait for all adaptive process)
			evoMC3->addVal(ratio); 							// -- Adaptive learning of the temperature
		} else if (evoMC3->hasConverged() &&
				hasConverged() && convIterMC3 == 0) {		// - Else if convergence for the first time
			convIterMC3 = iT;								// -- Keep track of it
		}
		// Update my temperature since Rho or other chain temperature might have changed
		double newInvTemp = hisInvTemp * exp(-exp(evoMC3->getRho()));
		acceptStrat->setInvTemperature(newInvTemp);
	}

	return ratio;
}

void PFAMCMC::acceptExchangeManager(Sample &newSample, const int otherChain) {
	if(mcmcMgr.isOutputManager()) {
		samples.push_back(newSample);
		samples.back().setChanged(true);
	}

	curSample = newSample;
	curSample.setChanged(true);

	// Call likelihood specialised exchange
	Utils::Serialize::buffer_t bufferIn, bufferOut;
	model.getLikelihood()->saveInternalState(oldSample, bufferIn);
	mcmcMgr.exchangeBufferMC3(bufferIn, bufferOut, otherChain);
	model.getLikelihood()->loadInternalState(oldSample, bufferOut);
	// Share the information with our proposal workers
	mcmcMgr.shareWithWorkersBufferMC3(bufferOut);
}

void PFAMCMC::acceptExchangeWorker(Sample &newSample) {
	curSample = newSample;
	curSample.setChanged(true);

	// Share the information with our proposal workers
	Utils::Serialize::buffer_t bufferOut;
	mcmcMgr.receiveFromManagerBufferMC3(bufferOut);
	model.getLikelihood()->loadInternalState(oldSample, bufferOut);
}


bool PFAMCMC::doMC3() {
	if(!mcmcMgr.isActiveMC3()) return false;
	//std::cout << "[" << Parallel::mpiMgr().getRank() << "] it = " << iT << " | " << "myChain = " <<  mcmcMgr.getMyChainMC3() << " | nChain = " << mcmcMgr.getNChainMC3() << std::endl;

	// Are we doing an exchange ?
	const int nChain = mcmcMgr.getNChainMC3();
	bool doExchange = iT % (cfgMC3->FREQ_MC3/nChain) == 0;
	if(!doExchange) return false;

	// Get my chain
	const int myChain = mcmcMgr.getMyChainMC3();
	//if(myChain > 0) iT += 0.015*(cfgMC3->FREQ_MC3/nChain);

	// Who will exchange ?
	int coldChain, hotChain;
	double unifRand;
	defineExchange(coldChain, hotChain, unifRand);
	//std::cout << "[" << Parallel::mpiMgr().getRank() << "] it = " << iT << " coldChain = " << coldChain << " | hotChain = " << hotChain << "  | rand = " << unifRand << std::endl;

	// If we are not part of this exchange we continue MCMC
	if(myChain != coldChain && myChain != hotChain)  return false;
	//std::cout << "[" << Parallel::mpiMgr().getRank() << "] START EXCHANGE "<< std::endl;
	double startMC3 = MPI_Wtime();

	// We add one character to signal if the sample is accepted
	char dataRecv[serializedSizeMC3];
	if(mcmcMgr.checkRoleProposal(mcmcMgr.Manager) || mcmcMgr.checkRoleProposal(mcmcMgr.Sequential)) { // Managers do that
		// Swap on chain 2 the order to make operations "symmetric"
		int otherChain = myChain == coldChain ? hotChain : coldChain;

		Sample newSample;
		double ratio = defineExchangeRatio(myChain, otherChain, dataRecv, newSample);

		if(unifRand < ratio){
			//std::cout << "Exchanging : " << myChain << " (" << curSample.posterior << ") and " << otherChain << " (" << newSample.posterior << ") with ratio=" << ratio << " > " << unifRand << endl;
			//dataRecv[serializedSizeMC3] = 'Y';
			packPartialInfoMC3(dataRecv, true, acceptStrat->getInvTemperature());
			// Signal outcome to other member of parallel proposal
			mcmcMgr.bcastProposal(serializedSizeMC3, dataRecv, mcmcMgr.getManagerProposal());
			// Do stuff related with accepted MC3
			acceptExchangeManager(newSample, otherChain);
			nAccStepMC3++;
		} else {
			//std::cout << "Rejected : " << myChain << " (" << curSample.posterior << ") and " << otherChain << " (" << newSample.posterior << ") with ratio="  << ratio << " > " << unifRand << endl;
			//dataRecv[serializedSizeMC3] = 'N';
			packPartialInfoMC3(dataRecv, false, acceptStrat->getInvTemperature());
			// Signal outcome to other member of parallel proposal
			mcmcMgr.bcastProposal(serializedSizeMC3, dataRecv, mcmcMgr.getManagerProposal());
			// update SampleVector if needed
			if(mcmcMgr.isOutputManager()) {
				samples.push_back(samples.back());
				samples.back().setChanged(false);
			}
		}
		nStepMC3++;
	} else if(mcmcMgr.checkRoleProposal(mcmcMgr.Worker)) { // Slaves
		bool accepted = false;
		double newInvTemp = 0.;
		Sample newSample;
		// Get message
		mcmcMgr.bcastProposal(serializedSizeMC3, dataRecv, mcmcMgr.getManagerProposal());
		unPackInfoMC3(dataRecv, accepted, newInvTemp, newSample);
		// Set new temperature
		acceptStrat->setInvTemperature(newInvTemp);
		// Do acceptance / rejection
		if(accepted) { // Sample has been exchanged
			acceptExchangeWorker(newSample);
		} else if(mcmcMgr.isOutputManager()) {
			samples.push_back(samples.back());
			samples.back().setChanged(false);
		}
	}

	double endMC3 = MPI_Wtime();
	timeMC3 += endMC3 - startMC3;

	return true;
}

std::string PFAMCMC::reportStatus() const {
	stringstream sstr;
	sstr << "[Partial] Number of iteration :\t" << iT << endl;
	sstr << "[Partial] Number of samples :\t" << 	this->getNWSamples() << endl;
	sstr << "[Partial] Mean sample per iteration :\t" << (float)this->getNWSamples() / (float)iT << endl;
	if(convS != 0 && convIt != 0) {
		sstr << "[Partial] Conv at iteration :\t" << convIt << endl;
		sstr << "[Partial] Conv at sample :\t" << convS << endl;
		sstr << "[Partial] Ratio conv :\t" << (double)convS/(double)this->getNWSamples() << endl;
	} else if(hasConverged()) {
		sstr << "[Partial] [WARNING] ADAPTIVE LEARNING HAS NOT YET CONVERGED!" << endl;
	}
	sstr << "[Partial] Time Block Sel + propose sample :\t" << timeI << "\t" << timeI/(double)iT << endl;
	sstr << "[Partial] Time Mu, Sigma, Lambda :\t" << timeB << "\t" << timeB/(double)iT << "\t" << endl;
	sstr << "[Partial] Time LocLambda :\t" << timeB2 << "\t" << timeB2/(double)iT << endl;
	if(mcmcMgr.isProposalSequential()) {
		sstr << "[Partial] Time ACC :\t" << timeP1 << "\t" << timeP1/(double)iT << endl;
	} else {
		sstr << "[Partial] Time COM 1 :\t" << timeP1 << "\t" << timeP1/(double)iT << endl;
		sstr << "[Partial] Time COM 1_1 :\t" << timeP1_1 << "\t" << timeP1_1/(double)iT << endl;
		sstr << "[Partial] Time COM 1_2 :\t" << timeP1_2 << "\t" << timeP1_2/(double)iT <<  endl;
		sstr << "[Partial] Time COM 1_3 :\t" << timeP1_3 << "\t" << timeP1_3/(double)iT << endl;
		sstr << "[Partial] Time COM 2 :\t" << timeP2 << "\t" << timeP2/(double)iT << "\t" << endl;
	}
	sstr << "[Partial] Time COMPUTE :\t" << timeC << "\t" << timeC/(double)iT << "\t" << endl;
	if(!mcmcMgr.isProposalSequential()) {
		sstr << "[Partial] >>Time WASTED :\t" << timeW << "\t" << timeW/(double)iT << endl;
	}
	if(mcmcMgr.isActiveMC3()) {
		sstr << "[Partial] Time MC3 :\t" << timeMC3 << "\t" << timeMC3/(double)iT << endl;
		if(mcmcMgr.getMyChainMC3() > 0) {
			sstr << "[Partial] >> Temperature for MC3 :\t" << acceptStrat->getInvTemperature() << " ( Rho= " << evoMC3->getRho() << ")" << endl;
			sstr << "[Partial] << MC3 convergence at iteration :\t" << convIterMC3 << std::endl;
		}
		sstr << "[Partial] >> Acceptance ratio MC3 :\t " << (double)nAccStepMC3/(double)nStepMC3 << "\t(" << nAccStepMC3  << " / " << nStepMC3 << ")" << endl;
	}
	sstr << "[Partial] Time for PFAMCMC :\t" << timeTot << "\t" << timeTot/(double)iT << endl;
	sstr << "[Partial] Time for write :\t" << timeWrite << "\t" << timeWrite/(double)iT  << endl;

	return sstr.str();
}

} /* namespace Sampler */
