//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * HelperInterface.h
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#ifndef HELPERINTERFACE_H_
#define HELPERINTERFACE_H_

#include "../Likelihoods.h"
#include "Sampler/Samples/Sample.h"
#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Blocks.h"

#include <boost/shared_ptr.hpp>

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

using namespace ParameterBlock;

class HelperInterface {
public:
	typedef boost::shared_ptr<HelperInterface> sharedPtr_t;

public:
	HelperInterface();
	virtual ~HelperInterface();

	static sharedPtr_t createHelper(LikelihoodInterface *likPtr);

	virtual void defineParameters(Parameters &params) const = 0;
	virtual void defineBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const = 0;
	virtual void defineOptiBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	virtual void defineRandomBlocks(Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	virtual Sample defineInitSample(const Parameters &params) const = 0;

	virtual void printSample(const Parameters &params, const Sample &sample, const char sep='\t') const;
};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
#endif /* HELPERINTERFACE_H_ */
