//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeInferenceHlp.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "TreeInferenceHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

const double TreeInferenceHlp::DEFAULT_P = 0.75;

TreeInferenceHlp::TreeInferenceHlp(TreeInference::Base *aPtrLik) :
	ptrLik(aPtrLik){

}

TreeInferenceHlp::~TreeInferenceHlp() {
}

void TreeInferenceHlp::defineParameters(Parameters &params) const {

	double treeFreq = 0.3;
	double branchFreq = 0.68;
	double rateFreq = 0.02;

	const size_t N_CLASS = ptrLik->getNClass();
	const TreeInference::modelType_t modelType = ptrLik->getModelType();
	const bool reflect = true;
	RNG *myRNG = Parallel::mpiMgr().getPRNG();

	ParamIntDef pTree(TreeInference::Base::NAME_TREE_PARAMETER, PriorInterface::createNoPrior(), reflect);
	pTree.setFreq(treeFreq/2.);
	pTree.setType(0);
	params.addParameter(pTree);

	size_t nRate = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nRate = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;;
		for(size_t iT=0; iT<nRate; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT;
			//ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setFreq(rateFreq/(double)(nRate+N_CLASS));
			pTheta.setMin(0.);
			if(nRate == 6) {
				pTheta.setMax(1.);
			} else {
				pTheta.setMax(500.);
			}
			pTheta.setType(1);
			params.addParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nRate = 1;
		ParamDblDef pKappa("Kappa", PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
		pKappa.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pKappa.setMin(0);
		pKappa.setMax(100.);
		pKappa.setType(1);
		params.addParameter(pKappa);
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineParameters(Parameters &params) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	// Ugly but well....
	if(modelType == TreeInference::M0) {
		ParamDblDef pW("W", PriorInterface::createExponentialBoost(myRNG, 10.), reflect);
		pW.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW.setMin(0.);
		pW.setMax(500.);
		pW.setType(1);
		params.addParameter(pW);
	} else if(modelType == TreeInference::M1) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(1);
		params.addParameter(pP0);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW0.setMin(0.);
		pW0.setMax(0.);
		pW0.setType(1);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(0);
		params.addParameter(pW1);
	} else if(modelType == TreeInference::M2) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(1);
		params.addParameter(pP0);

		ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
		pP1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP1.setMin(0.);
		pP1.setMax(1.);
		pP1.setType(1);
		params.addParameter(pP1);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW0.setMin(0.);
		pW0.setMax(0.);
		pW0.setType(1);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(1);
		params.addParameter(pW1);

		ParamDblDef pW2("W2", PriorInterface::createExponentialBoost(myRNG, 10.), reflect);
		pW2.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW2.setMin(0.);
		pW2.setMax(500.);
		pW2.setType(1);
		params.addParameter(pW2);
	} else if(modelType == TreeInference::M1a) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(1);
		params.addParameter(pP0);

		ParamDblDef pW0("W0", PriorInterface::createBetaBoost(myRNG, 1., 1.), reflect);
		pW0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW0.setMin(0.);
		pW0.setMax(1.);
		pW0.setType(1);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(1);
		params.addParameter(pW1);
	} else if(modelType == TreeInference::M2a) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(1);
		params.addParameter(pP0);

		ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
		pP1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pP1.setMin(0.);
		pP1.setMax(1.);
		pP1.setType(1);
		params.addParameter(pP1);

		ParamDblDef pW0("W0", PriorInterface::createBetaBoost(myRNG, 1., 1.), reflect);
		pW0.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW0.setMin(0.);
		pW0.setMax(1.);
		pW0.setType(1);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(1);
		params.addParameter(pW1);

		ParamDblDef pW2("W2", PriorInterface::createExponentialBoost(myRNG, 10.), reflect);
		pW2.setFreq(rateFreq/(double)(nRate+N_CLASS));
		pW2.setMin(1.);
		pW2.setMax(500.);
		pW2.setType(1);
		params.addParameter(pW2);
	}

	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		ParamDblDef pBL(ssBranch.str(), PriorInterface::createExponentialBoost(myRNG, 10.), reflect);
		pBL.setFreq(branchFreq/(double)ptrLik->getNBranch());
		pBL.setMin(0.);
		pBL.setMax(1000.);
		pBL.setType(2);
		params.addParameter(pBL);
	}

}

void TreeInferenceHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {

	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = params.size() - (ptrLik->getNBranch()+1);
	//std::cout << "Params size = " << params.size() << " :: nBranch = " << ptrLik->getNBranch() << " :: nElem = " << nElemBRate << std::endl;

	if(ptrLik->getTreeUpdateStrategy() != StatisticalModel::Likelihood::TreeInference::Base::FIXED_TREE_STRATEGY) {
		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("TreeSTNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), params),
				params.getParameterFreq(iParam));

		blocks.addBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("TreeESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), params),
				params.getParameterFreq(iParam));

		blocks.addBlock(bTreeESPR);
	}
	iParam++;

	Block::sharedPtr_t bRate(new Block(adaptiveType));
	bRate->setName("BlockRate");
	// Add kappa or thethas, Px and Wx
	for(size_t i=0; i<nElemBRate; ++i) {
		if(params.getBoundMin(iParam) < params.getBoundMax(iParam)) {
			bRate->addParameter(iParam,
					BlockModifier::createGaussianWindow(0.03, params),
					params.getParameterFreq(iParam));
		}
		iParam++;
	}
	blocks.addBlock(bRate);

	// Branch length
	Block::sharedPtr_t bBL[nB_BL];
	for(size_t iB=0; iB < nB_BL; ++iB){
		bBL[iB].reset(new Block(adaptiveType));
		stringstream ss;
		ss << "BranchLength_" << iB;
		bBL[iB]->setName(ss.str());
	}

	std::vector<size_t> activeParamsIdx;
	for(size_t iBL=iParam; iBL<iParam+ptrLik->getNBranch(); ++iBL){
		activeParamsIdx.push_back(iBL);
	}
	//std::random_shuffle(activeParamsIdx.begin(), activeParamsIdx.end());

	size_t iP = 0;
	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		uint idx = iBL/nBLPerB;
		if(idx >= nB_BL) idx = iBL % nB_BL;
		iParam = activeParamsIdx[iP];
		const double l = ptrLik->getOptiL(Parallel::mcmcMgr().getNProposal(), 1, 0.002*0.002)/pow(nBLPerB, 0.75);
		bBL[idx]->addParameter(iParam,
				BlockModifier::createGaussianWindow(l, params),
				params.getParameterFreq(iParam));
		++iP;
	}

	for(size_t iB=0; iB < nB_BL; ++iB){
		blocks.addBlock(bBL[iB]);
	}
}

void TreeInferenceHlp::defineRandomBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {

	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = params.size() - (ptrLik->getNBranch()+1);
	//std::cout << "Params size = " << params.size() << " :: nBranch = " << ptrLik->getNBranch() << " :: nElem = " << nElemBRate << std::endl;

	if(ptrLik->getTreeUpdateStrategy() != StatisticalModel::Likelihood::TreeInference::Base::FIXED_TREE_STRATEGY) {
		Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeSTNNI->setName("TreeSTNNI");
		bTreeSTNNI->addParameter(iParam,
				BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), params),
				params.getParameterFreq(iParam));

		blocks.addBlock(bTreeSTNNI);

		Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bTreeESPR->setName("TreeESPR");
		bTreeESPR->addParameter(iParam,
				BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), params),
				params.getParameterFreq(iParam));

		blocks.addBlock(bTreeESPR);
	}
	iParam++;

	Block::sharedPtr_t bRate(new Block(adaptiveType));
	bRate->setName("BlockRate");
	// Add kappa or thethas, Px and Wx
	for(size_t i=0; i<nElemBRate; ++i) {
		if(params.getBoundMin(iParam) < params.getBoundMax(iParam)) {
			bRate->addParameter(iParam,
					BlockModifier::createGaussianWindow(0.03, params),
					params.getParameterFreq(iParam));
		}
		iParam++;
	}
	blocks.addBlock(bRate);

	// Branch length
	Block::sharedPtr_t bBL[nB_BL];
	for(size_t iB=0; iB < nB_BL; ++iB){
		bBL[iB].reset(new Block(adaptiveType));
		stringstream ss;
		ss << "BranchLength_" << iB;
		bBL[iB]->setName(ss.str());
	}

	std::vector<size_t> activeParamsIdx;
	for(size_t iBL=iParam; iBL<iParam+ptrLik->getNBranch(); ++iBL){
		activeParamsIdx.push_back(iBL);
	}

	Parallel::mpiMgr().getPRNG()->randomShuffle(activeParamsIdx);

	size_t iP = 0;
	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		uint idx = iBL/nBLPerB;
		if(idx >= nB_BL) idx = iBL % nB_BL;
		iParam = activeParamsIdx[iP];
		const double l = ptrLik->getOptiL(Parallel::mcmcMgr().getNProposal(), 1, 0.002*0.002)/pow(nBLPerB, 0.75);
		bBL[idx]->addParameter(iParam,
				BlockModifier::createGaussianWindow(l, params),
				params.getParameterFreq(iParam));
		++iP;
	}

	for(size_t iB=0; iB < nB_BL; ++iB){
		blocks.addBlock(bBL[iB]);
	}

}

Sample TreeInferenceHlp::defineInitSample(const Parameters &params) const {
	const double epsilon = 1e-2;
	const size_t N_CLASS = ptrLik->getNClass();
	const TreeInference::modelType_t modelType = ptrLik->getModelType();
	const RNG* rng = Parallel::mpiMgr().getPRNG();

	Sampler::Sample sample;

	sample.intValues.push_back(ptrLik->getCurrentTree()->getHashKey());

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;
		for(size_t iT=0; iT<nSubParams; ++iT) {
			sample.dblValues.push_back(rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nSubParams = 1;
		sample.dblValues.push_back(0.5+rng->genUniformDbl());		// [0] Rand Kappa
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineBlocks(const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	if(modelType != TreeInference::NOT_CODON) {
		for(size_t i=0; i<N_CLASS-1; ++i) {
			sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon)); // Px between 0. and 1.
		}

		for(size_t iC=0; iC<N_CLASS; ++iC) {
			switch (modelType) {
				case TreeInference::M1 :
					assert(iC < 2);
					if(iC == 0) sample.dblValues.push_back(0.);
					if(iC == 1) sample.dblValues.push_back(1.);
					break;
				case TreeInference::M2 :
					assert(iC < 3);
					if(iC == 0) sample.dblValues.push_back(0.);
					if(iC == 1) sample.dblValues.push_back(1.);
					if(iC == 2) sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 10.));
					break;
				case TreeInference::M1a :
					assert(iC < 2);
					if(iC == 0) sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon));
					if(iC == 1) sample.dblValues.push_back(1.);
					break;
				case TreeInference::M2a :
					assert(iC < 3);
					if(iC == 0) sample.dblValues.push_back(0.);
					if(iC == 1) sample.dblValues.push_back(1.);
					if(iC == 2) sample.dblValues.push_back(rng->genUniformDbl(1.0+epsilon, 10.));
					break;
				default:
					sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 10.-epsilon)); // W between 0. and 500.
					break;
			}
		}
	}

	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		sample.dblValues.push_back(rng->genExponential(65.)); // Branches between 0.5 and 25.
	}

	return sample;
}

void TreeInferenceHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const size_t N_CLASS = ptrLik->getNClass();
	std::cout << "NCLASS = " << N_CLASS << std::endl;
	const TreeInference::modelType_t modelType = ptrLik->getModelType();
	const std::vector<double> &values = sample.getDblValues();

	size_t nNuclParams = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nNuclParams = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;;
		std::cout << "Thetas :\t";
		for(size_t iT=0; iT<5; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nNuclParams = 1;
		std::cout << "Kappa :\t" << values[0] << std::endl;
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}

	size_t iParam = nNuclParams;
	if(modelType != TreeInference::NOT_CODON) {
		std::vector<double> Px;
		std::cout << "Px :\t";
		for(size_t iC=0; iC<N_CLASS-1; ++iC) {
			std::cout << values[iParam] << "\t";
			Px.push_back(values[iParam]);
			iParam++;
		}
		std::cout << std::endl;

		TreeInference::Proportions props(N_CLASS);
		props.set(Px);
		std::cout << "Props :\t";
		for(size_t iC=0; iC<N_CLASS; ++iC) {
			std::cout << props.getProportions()[iC] << "\t";
		}
		std::cout << std::endl;

		std::cout << "Omegas :\t";
		for(size_t iC=0; iC<N_CLASS; ++iC) {
			std::cout << values[iParam] << "\t";
			iParam++;
		}
		std::cout << std::endl;
	}

	std::cout << "Branch lenghts :\t";
	for(size_t i=iParam; i<values.size(); ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;

	std::cout << "Topology :\t";
	std::cout << ptrLik->getCurrentTree()->getIdString();
	std::cout << endl;
}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
