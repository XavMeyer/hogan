//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IndepLogNormalHlp.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#include "IndepLogNormalHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

IndepLogNormalHlp::IndepLogNormalHlp(LogNormalNDLH *aPtrLik) : ptrLik(aPtrLik) {
}

IndepLogNormalHlp::~IndepLogNormalHlp() {
}


void IndepLogNormalHlp::defineParameters(Parameters &params) const {
	for(size_t i=0; i<ptrLik->getNDim(); ++i){
		stringstream ssMu;
		ssMu << "mu_" << i;
		ParamDblDef pd1(ssMu.str(), PriorInterface::createNoPrior());
		pd1.setMin(-numeric_limits<double>::max());
		pd1.setMax(numeric_limits<double>::max());
		params.addParameter(pd1);


		stringstream ssSigma;
		ssSigma << "sigma_" << i;
		ParamDblDef pd2(ssSigma.str(), PriorInterface::createNoPrior());
		pd2.setMin(-numeric_limits<double>::max());
		pd2.setMax(numeric_limits<double>::max());
		params.addParameter(pd2);
	}
}

void IndepLogNormalHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()*2/blockSize[0];
	double l = 0.22;

	for(size_t iG=0; iG < nG; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType));
		for(size_t i=0; i<blockSize[0]; ++i){
			b->addParameter(iP);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
	}
}

void IndepLogNormalHlp::define2XBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()*2/blockSize[0];
	double l=0.22;

	for(size_t iG=0; iG < nG/2; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType)), b2(new Block(adaptiveType));
		for(size_t i=0; i<blockSize[0]; ++i){
			b->addParameter(iP);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;

			b2->addParameter(iP);
			b2->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
		blocks.addBlock(b2);
	}
}

void IndepLogNormalHlp::defineOptiBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	size_t nP = Parallel::mcmcMgr().getNProposal();
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()*2/blockSize[0];
	for(size_t iG=0; iG < nG; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType));
		for(size_t i=0; i<blockSize[0]; ++i){
			b->addParameter(iP);
			const double l = ptrLik->getOptiL(nP, 1, ptrLik->getLogNormalLH(iP)->getSigma())/sqrt(blockSize[0]);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
	}
}

void IndepLogNormalHlp::defineGoodBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	size_t nP = Parallel::mcmcMgr().getNProposal();
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()*2/blockSize[0];
	const double meanVar = ptrLik->getAveragVar(0, (ptrLik->getNDim()*2)-1);
	for(size_t iG=0; iG < nG; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType));
		for(size_t i=0; i<blockSize[0]; ++i){
			b->addParameter(iP);
			const double l = ptrLik->getOptiL(nP, 1, meanVar)/sqrt(blockSize[0]);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
	}
}

Sample IndepLogNormalHlp::defineInitSample(const Parameters &params) const {
	Sample mySample;
	double rand;
	for(uint iD=0; iD < ptrLik->getNDim(); ++iD){
		rand = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
		mySample.dblValues.push_back(ptrLik->getLogNormalLH(iD)->getMu()+rand);
		rand = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
		mySample.dblValues.push_back(ptrLik->getLogNormalLH(iD)->getSigma()+rand);
	}
	return mySample;
}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
