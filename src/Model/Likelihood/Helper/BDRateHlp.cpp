//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BDRate.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "BDRateHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

template <class BDRateLik>
BDRateHlp<BDRateLik>::BDRateHlp(BDRateLik *aPtrLik) :
	HelperInterface(), ptrLik(aPtrLik)
{
}

template <class BDRateLik>
BDRateHlp<BDRateLik>::~BDRateHlp() {
}

template <>
void BDRateHlp<BDRate::Standard::StdLik>::defineParameters(Parameters &params) const {
	BDRate::Helper::initStdParameters(ptrLik, params);
}

template <>
void BDRateHlp<BDRate::Alpha::AlphaLik>::defineParameters(Parameters &params) const {
	BDRate::Helper::initAlphaParameters(ptrLik, params);
}

template <>
void BDRateHlp<BDRate::Standard::StdLik>::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	BDRate::Helper::initStdBlocks(adaptiveType, ptrLik, 1e-2, 1., 1e-2, params, blocks, blockSize[0]);
}

template <>
void BDRateHlp<BDRate::Alpha::AlphaLik>::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	BDRate::Helper::initAlphaBlocks(adaptiveType, ptrLik, 1e-2, 1., 1e-2, params, blocks, blockSize[0]);
}

template <>
Sample BDRateHlp<BDRate::Standard::StdLik>::defineInitSample(const Parameters &params) const {
	Sample mySample;
	BDRate::Helper::initFirstSampleStd(ptrLik, mySample);
	return mySample;
}

template <>
Sample BDRateHlp<BDRate::Alpha::AlphaLik>::defineInitSample(const Parameters &params) const {
	Sample mySample;
	if(ptrLik->isHPP()){
		BDRate::Helper::initFirstSamplePlant(ptrLik, mySample);
	} else {
		BDRate::Helper::initFirstSampleAlpha(ptrLik, mySample);
	}
	return mySample;
}

template class BDRateHlp<BDRate::Standard::StdLik>;
template class BDRateHlp<BDRate::Alpha::AlphaLik>;


} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
