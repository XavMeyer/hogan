//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * HelperInterface.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "HelperInterface.h"
#include "Helpers.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

HelperInterface::HelperInterface() {
}

HelperInterface::~HelperInterface() {
}

HelperInterface::sharedPtr_t HelperInterface::createHelper(LikelihoodInterface *likPtr) {
	if(LogNormalNDLH *ptr = dynamic_cast<LogNormalNDLH*>(likPtr)){
		return sharedPtr_t(new IndepLogNormalHlp(ptr));
	} else if(CorrelatedNormals *ptr = dynamic_cast<CorrelatedNormals*>(likPtr)) {
		return sharedPtr_t(new CorrelNormalHlp(ptr));
	} else if(NormalLH *ptr = dynamic_cast<NormalLH*>(likPtr)) {
		return sharedPtr_t(new UnivarDistribHlp<NormalLH>(ptr));
	} else if(LogNormalLH *ptr = dynamic_cast<LogNormalLH*>(likPtr)) {
		return sharedPtr_t(new UnivarDistribHlp<LogNormalLH>(ptr));
	} else if(BDRate::Standard::StdLik *ptr = dynamic_cast<BDRate::Standard::StdLik*>(likPtr)) {
		return sharedPtr_t(new BDRateHlp<BDRate::Standard::StdLik>(ptr));
	} else if(BDRate::Alpha::AlphaLik *ptr = dynamic_cast<BDRate::Alpha::AlphaLik*>(likPtr)) {
			return sharedPtr_t(new BDRateHlp<BDRate::Alpha::AlphaLik>(ptr));
	} else if(CodonModels::Base *ptr = dynamic_cast<CodonModels::Base *>(likPtr)) {
			return sharedPtr_t(new CodonModelsHlp(ptr));
	} else if(TreeInference::Base *ptr = dynamic_cast<TreeInference::Base *>(likPtr)) {
			return sharedPtr_t(new TreeInferenceHlp(ptr));
	} else {
		std::cerr << "Helper::sharedPtr_t Helper::createHelper(LikelihoodInterface *likPtr);" << std::endl;
		std::cerr << "No Likelihood::Helper available for likelihood '" << likPtr->getName() << "'" << std::endl;
		abort();
	}
	return sharedPtr_t();
}

void HelperInterface::defineOptiBlocks(
		Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize,
		const Parameters &params, Blocks &blocks) const {
	std::cout << "[WARNING] Optimized blocks creation is not define for this Likelihood." << std::endl;
	std::cout << "[WARNING] ... default block creation will be used." << std::endl;
	defineBlocks(adaptiveType, blockSize, params, blocks);
}

void HelperInterface::defineRandomBlocks(
		Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize,
		const Parameters &params, Blocks &blocks) const {
	std::cout << "[WARNING] Randomized blocks creation is not define for this Likelihood." << std::endl;
	std::cout << "[WARNING] ... default block creation will be used." << std::endl;
	defineBlocks(adaptiveType, blockSize, params, blocks);
}

void HelperInterface::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	if(sep == '\n') {
		for(size_t iP=0; iP<params.size(); ++iP) {
			std::cout << params.getName(iP) << " =\t";
			std::cout << std::scientific << std::setprecision(3) << sample.dblValues[iP] << std::endl;
		}
	} else {
		// Print params names
		for(size_t iP=0; iP<params.size(); ++iP) {
			std::cout << params.getName(iP) << sep;
		}
		std::cout << std::endl;
		// Print sample values
		for(size_t iP=0; iP<params.size(); ++iP) {
			std::cout << std::scientific << std::setprecision(3) << sample.dblValues[iP] << sep;
		}
		std::cout << std::endl;
	}
}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
