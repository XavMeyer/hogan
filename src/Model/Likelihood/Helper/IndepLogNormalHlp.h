//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IndepLogNormalHlp.h
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef INDEPLOGNORMALHLP_H_
#define INDEPLOGNORMALHLP_H_

#include "HelperInterface.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

class IndepLogNormalHlp: public HelperInterface {
public:
	IndepLogNormalHlp(LogNormalNDLH *aPtrLik);
	~IndepLogNormalHlp();

	void defineParameters(Parameters &params) const;
	void defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	void define2XBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	void defineOptiBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	void defineGoodBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;
	Sample defineInitSample(const Parameters &params) const;

private:
	LogNormalNDLH *ptrLik;
};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* INDEPLOGNORMALHLP_H_ */
