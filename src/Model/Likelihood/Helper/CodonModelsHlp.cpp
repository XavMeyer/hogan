//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodonModelsHlp.cpp
 *
 * @date Sep 6, 2015
 * @author meyerx
 * @brief
 */
#include "CodonModelsHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

CodonModelsHlp::CodonModelsHlp(CodonModels::Base *aPtrLik) : ptrLik(aPtrLik) {
}

CodonModelsHlp::~CodonModelsHlp() {
}

void CodonModelsHlp::defineParameters(Parameters &params) const {

	const size_t N_CLASS = ptrLik->getNClass();
	const CodonModels::modelType_t modelType = ptrLik->getModelType();
	const bool reflect = false;

	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<5; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT;
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setMin(0.);
			pTheta.setMax(100.);
			pTheta.setType(0);
			params.addParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		ParamDblDef pKappa("Kappa", PriorInterface::createNoPrior(), reflect);
		pKappa.setMin(0);
		pKappa.setMax(100.);
		pKappa.setType(0);
		params.addParameter(pKappa);
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineParameters(Parameters &params) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	// Ugly but well....
	if(modelType == CodonModels::M0) {
		ParamDblDef pW("W", PriorInterface::createNoPrior(), reflect);
		pW.setMin(0.);
		pW.setMax(500.);
		pW.setType(0);
		params.addParameter(pW);
	} else if(modelType ==  CodonModels::M1) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(0);
		params.addParameter(pP0);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setMin(0.);
		pW0.setMax(0.);
		pW0.setType(0);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(0);
		params.addParameter(pW1);
	} else if(modelType ==  CodonModels::M2) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(0);
		params.addParameter(pP0);

		ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
		pP1.setMin(0.);
		pP1.setMax(1.);
		pP1.setType(0);
		params.addParameter(pP1);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setMin(0.);
		pW0.setMax(0.);
		pW0.setType(0);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(0);
		params.addParameter(pW1);

		ParamDblDef pW2("W2", PriorInterface::createExponentialBoost(Parallel::mpiMgr().getPRNG(), 0.3), reflect);
		pW2.setMin(0.);
		pW2.setMax(500.);
		pW2.setType(0);
		params.addParameter(pW2);
	} else if(modelType ==  CodonModels::M1a) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(0);
		params.addParameter(pP0);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setMin(0.);
		pW0.setMax(1.);
		pW0.setType(0);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(0);
		params.addParameter(pW1);
	} else if(modelType ==  CodonModels::M2a) {
		ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
		pP0.setMin(0.);
		pP0.setMax(1.);
		pP0.setType(0);
		params.addParameter(pP0);

		ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
		pP1.setMin(0.);
		pP1.setMax(1.);
		pP1.setType(0);
		params.addParameter(pP1);

		ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
		pW0.setMin(0.);
		pW0.setMax(1.);
		pW0.setType(0);
		params.addParameter(pW0);

		ParamDblDef pW1("W1", PriorInterface::createNoPrior(), reflect);
		pW1.setMin(1.);
		pW1.setMax(1.);
		pW1.setType(0);
		params.addParameter(pW1);

		//ParamDblDef pW2("W2", PriorInterface::createNoPrior(), reflect);
		ParamDblDef pW2("W2", PriorInterface::createExponentialBoost(Parallel::mpiMgr().getPRNG(), 0.3), reflect);
		pW2.setMin(1.);
		pW2.setMax(500.);
		pW2.setType(0);
		params.addParameter(pW2);
	} else {
		for(size_t iC=0; iC<N_CLASS-1; ++iC) {
			std::stringstream ss;
			ss << "P" << iC;
			ParamDblDef pPx(ss.str(), PriorInterface::createNoPrior(), reflect);
			pPx.setMin(0.);
			pPx.setMax(1.);
			pPx.setType(0);
			params.addParameter(pPx);
		}

		for(size_t iC=0; iC<N_CLASS; ++iC) {
			std::stringstream ss;
			ss << "W" << iC;
			ParamDblDef pWx(ss.str(), PriorInterface::createNoPrior(), reflect);
			pWx.setMin(0.);
			pWx.setMax(500.);
			pWx.setType(0);
			params.addParameter(pWx);
		}
	}

	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		ParamDblDef pBL(ssBranch.str(), PriorInterface::createNoPrior(), reflect);
		pBL.setMin(0.);
		pBL.setMax(50.);
		pBL.setType(1);
		params.addParameter(pBL);
	}

}

void CodonModelsHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {

	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = params.size() - ptrLik->getNBranch();
	Block::sharedPtr_t bRate(new Block(adaptiveType));
	bRate->setName("BlockRate");

	// Add kappa or thethas, Px and Wx
	for(size_t i=0; i<nElemBRate; ++i) {
		if(params.getBoundMin(iParam) < params.getBoundMax(iParam)) {
			bRate->addParameter(iParam, BlockModifier::createGaussianWindow(0.1, params), params.getParameterFreq(iParam));
		}
		iParam++;
	}
	blocks.addBlock(bRate);

	// Branch length
	Block::sharedPtr_t bBL[nB_BL];
	for(size_t iB=0; iB < nB_BL; ++iB){
		bBL[iB].reset(new Block(adaptiveType));
		stringstream ss;
		ss << "BranchLength_" << iB;
		bBL[iB]->setName(ss.str());
	}

	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		uint idx = iBL/nBLPerB;
		if(idx >= nB_BL) idx = iBL % nB_BL;
		bBL[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(0.1, params), params.getParameterFreq(iParam));
		++iParam;
	}

	for(size_t iB=0; iB < nB_BL; ++iB){
		blocks.addBlock(bBL[iB]);
	}
}

void CodonModelsHlp::defineOneBlock(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {

	Block::sharedPtr_t bBigBlock(new Block(adaptiveType));
	bBigBlock->setName("TheBlock");

	// Add kappa or thethas, Px and Wx
	for(size_t iParam=0; iParam<params.size(); ++iParam) {
		if(params.getBoundMin(iParam) < params.getBoundMax(iParam)) {
			bBigBlock->addParameter(iParam, BlockModifier::createGaussianWindow(0.1, params), params.getParameterFreq(iParam));
		}
	}
	blocks.addBlock(bBigBlock);
}


void CodonModelsHlp::defineRandomBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {

	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = params.size() - ptrLik->getNBranch();
	Block::sharedPtr_t bRate(new Block(adaptiveType));
	bRate->setName("BlockRate");

	// Add kappa or thethas, Px and Wx
	for(size_t i=0; i<nElemBRate; ++i) {
		if(params.getBoundMin(iParam) < params.getBoundMax(iParam)) {
			bRate->addParameter(iParam, BlockModifier::createGaussianWindow(0.1, params), params.getParameterFreq(iParam));
		}
		iParam++;
	}
	blocks.addBlock(bRate);

	// Branch length
	std::vector<size_t> bIdx;
	for(size_t iP=iParam; iP<iParam+ptrLik->getNBranch(); ++iP) {
		bIdx.push_back(iP);
	}

	Parallel::mpiMgr().getPRNG()->randomShuffle(bIdx);

	Block::sharedPtr_t bBL[nB_BL];
	for(size_t iB=0; iB < nB_BL; ++iB){
		bBL[iB].reset(new Block(adaptiveType));
		stringstream ss;
		ss << "BranchLength_" << iB;
		bBL[iB]->setName(ss.str());
	}

	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		uint idx = iBL/nBLPerB;
		if(idx >= nB_BL) idx = iBL % nB_BL;
		bBL[idx]->addParameter(bIdx[iBL], BlockModifier::createGaussianWindow(0.1, params), params.getParameterFreq(bIdx[iBL]));
	}

	for(size_t iB=0; iB < nB_BL; ++iB){
		blocks.addBlock(bBL[iB]);
	}
}

Sample CodonModelsHlp::defineInitSample(const Parameters &params) const {
	const double epsilon = 1e-2;
	const size_t N_CLASS = ptrLik->getNClass();
	const CodonModels::modelType_t modelType = ptrLik->getModelType();
	const RNG* rng = Parallel::mpiMgr().getPRNG();

	Sampler::Sample sample;

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = 5;
		for(size_t iT=0; iT<nSubParams; ++iT) {
			sample.dblValues.push_back(0.5+rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nSubParams = 1;
		sample.dblValues.push_back(0.5+rng->genUniformDbl());		// [0] Rand Kappa
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineBlocks(const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	for(size_t i=0; i<N_CLASS-1; ++i) {
		sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon)); // Px between 0. and 1.
	}

	for(size_t iC=0; iC<N_CLASS; ++iC) {
		switch (modelType) {
			case CodonModels::M1 :
				assert(iC < 2);
				if(iC == 0) sample.dblValues.push_back(0.);
				if(iC == 1) sample.dblValues.push_back(1.);
				break;
			case CodonModels::M2 :
				assert(iC < 3);
				if(iC == 0) sample.dblValues.push_back(0.);
				if(iC == 1) sample.dblValues.push_back(1.);
				if(iC == 2) sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 10.));
				break;
			case CodonModels::M1a :
				assert(iC < 2);
				if(iC == 0) sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon));
				if(iC == 1) sample.dblValues.push_back(1.);
				break;
			case CodonModels::M2a :
				assert(iC < 3);
				if(iC == 0) sample.dblValues.push_back(0.);
				if(iC == 1) sample.dblValues.push_back(1.);
				if(iC == 2) sample.dblValues.push_back(rng->genUniformDbl(1.0+epsilon, 10.));
				break;
			default:
				sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 10.-epsilon)); // W between 0. and 500.
				break;
		}
	}

	for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
		sample.dblValues.push_back(rng->genUniformDbl(0.0+epsilon, 1.5)); // Branches between 0.5 and 25.
	}

	return sample;
}

void CodonModelsHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const size_t N_CLASS = ptrLik->getNClass();
	const std::vector<double> &values = sample.getDblValues();

	size_t nNuclParams = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nNuclParams = 5;
		std::cout << "Thetas :\t";
		for(size_t iT=0; iT<5; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nNuclParams = 1;
		std::cout << "Kappa :\t" << values[0] << std::endl;
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}

	std::vector<double> Px;
	size_t iParam = nNuclParams;
	std::cout << "Px :\t";
	for(size_t iC=0; iC<N_CLASS-1; ++iC) {
		std::cout << values[iParam] << "\t";
		Px.push_back(values[iParam]);
		iParam++;
	}
	std::cout << std::endl;

	CodonModels::Proportions props(N_CLASS);
	props.set(Px);
	std::cout << "Props :\t";
	for(size_t iC=0; iC<N_CLASS; ++iC) {
		std::cout << props.getProportions()[iC] << "\t";
	}
	std::cout << std::endl;

	std::cout << "Omegas :\t";
	for(size_t iC=0; iC<N_CLASS; ++iC) {
		std::cout << values[iParam] << "\t";
		iParam++;
	}
	std::cout << std::endl;

	std::cout << "Branch lenghts :\t";
	for(size_t i=iParam; i<values.size(); ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;

}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
