//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CorrelNormalHlp.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#include "CorrelNormalHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

CorrelNormalHlp::CorrelNormalHlp(CorrelatedNormals *aPtrLik) : ptrLik(aPtrLik){
}

CorrelNormalHlp::~CorrelNormalHlp() {
}


void CorrelNormalHlp::defineParameters(Parameters &params) const {
	bool reflection = true;
	for(size_t i=0; i<ptrLik->getNDim(); ++i){
		stringstream ssMu;
		ssMu << "mu_" << i;
		ParamDblDef pd1(ssMu.str(), PriorInterface::createNoPrior(), reflection);
		pd1.setMin(-numeric_limits<double>::max());
		pd1.setMax(numeric_limits<double>::max());
		params.addParameter(pd1);
	}
}

void CorrelNormalHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	initBlockMu(adaptiveType,blockSize[0], params, blocks);
}

void CorrelNormalHlp::defineOptiBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	initBlockMuOpti(adaptiveType, blockSize[0], params, blocks);
}

void CorrelNormalHlp::initBlockMu(ParameterBlock::Block::adaptiveType_t adaptiveType, size_t groupSize, const Parameters &params, Blocks &blocks) const {
	size_t nP = Parallel::mcmcMgr().getNProposal();
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()/groupSize;
	for(size_t iG=0; iG < nG; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType));
		for(size_t i=0; i<groupSize; ++i){
			b->addParameter(iP);
			const double l = ptrLik->getOptiL(nP, 1, 1e-2)/sqrt(groupSize);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
	}
}

void CorrelNormalHlp::initBlockMuOpti(ParameterBlock::Block::adaptiveType_t adaptiveType, size_t groupSize, const Parameters &params, Blocks &blocks) const{
	size_t nP = Parallel::mcmcMgr().getNProposal();
	size_t iP = 0;
	size_t nG = ptrLik->getNDim()/groupSize;
	for(size_t iG=0; iG < nG; ++iG){
		Block::sharedPtr_t b(new Block(adaptiveType));
		for(size_t i=0; i<groupSize; ++i){
			b->addParameter(iP);
			const double l = ptrLik->getOptiL(nP, 1, ptrLik->getSigmaDiag(iP))/sqrt(groupSize);
			b->setBlockModifier(i, BlockModifier::createGaussianWindow(l, params));
			++iP;
		}
		blocks.addBlock(b);
	}
}

Sample CorrelNormalHlp::defineInitSample(const Parameters &params) const {
	Sample mySample;
	double rand;
	for(uint iD=0; iD < ptrLik->getNDim(); ++iD){
		rand = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
		//rand = 2.0;
		mySample.dblValues.push_back(rand);
	}
	return mySample;
}

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
