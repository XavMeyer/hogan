//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file UnivarDistribHlp.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#include "UnivarDistribHlp.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

template<typename distribution>
UnivarDistribHlp<distribution>::UnivarDistribHlp(distribution *aPtrLik) : ptrLik(aPtrLik) {
}

template<typename distribution>
UnivarDistribHlp<distribution>::~UnivarDistribHlp() {
}

template<typename distribution>
void UnivarDistribHlp<distribution>::defineParameters(Parameters &params) const {
	ParamDblDef pd1("alpha", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 0., 10.));
	pd1.setMin(-0.);
	pd1.setMax(10.);
	params.addParameter(pd1);

	ParamDblDef pd2("beta", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 0., 10.));
	pd2.setMin(-0.);
	pd2.setMax(10.);
	params.addParameter(pd2);

}

template<typename distribution>
void UnivarDistribHlp<distribution>::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, const Parameters &params, Blocks &blocks) const {
	Block::sharedPtr_t b1(new Block(adaptiveType)), b2(new Block(adaptiveType));
	b1->addParameter(0, BlockModifier::createGaussianWindow(1.e-1, params));
	blocks.addBlock(b1);
	b2->addParameter(1, BlockModifier::createGaussianWindow(1.e-1, params));
	blocks.addBlock(b2);
}

template<typename distribution>
Sample UnivarDistribHlp<distribution>::defineInitSample(const Parameters &params) const {
	return params.generateRandomSample();
}

template<>
Sample UnivarDistribHlp<LogNormalLH>::defineInitSample(const Parameters &params) const {
	Sample mySample;
	double rand = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
	mySample.dblValues.push_back(ptrLik->getMu()+rand);
	rand = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
	mySample.dblValues.push_back(ptrLik->getSigma()+rand);
	return mySample;
}

template class UnivarDistribHlp<NormalLH>;
template class UnivarDistribHlp<LogNormalLH>;

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
