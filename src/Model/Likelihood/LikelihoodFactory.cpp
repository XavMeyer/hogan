//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelilhoodFactory.cpp
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#include "LikelihoodFactory.h"
#include "Likelihoods.h"

namespace StatisticalModel {
namespace Likelihood {

LikelihoodFactory::LikelihoodFactory() {
}

LikelihoodFactory::~LikelihoodFactory() {
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createLogNormal(const size_t nData, const double mu, const double sigma){
	return LikelihoodInterface::sharedPtr_t(new LogNormalLH(nData, mu, sigma));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createLogNormalNDim(const size_t nDim, const size_t nData){
	return LikelihoodInterface::sharedPtr_t(new LogNormalNDLH(nDim, nData));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createLogNormalNDim(const size_t nDim, const size_t nData, const double *mu, const double *sigma){
	return LikelihoodInterface::sharedPtr_t(new LogNormalNDLH(nDim, nData, mu, sigma));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createNormal(const string filename){
	return LikelihoodInterface::sharedPtr_t(new NormalLH(filename));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCorrelatedNormals(const size_t nData, const std::string &fName) {
        return LikelihoodInterface::sharedPtr_t(new CorrelatedNormals(nData, fName));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createBDRateDAG(const size_t nThread, const std::string &fName, const bool isHPP) {
        return LikelihoodInterface::sharedPtr_t (new BDRate::Standard::StdLik(nThread, fName, isHPP));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createBDRAlphaDAG(const size_t nThread, const unsigned int nQuantAlpha, const std::string &fName, const bool isHPP) {
        return LikelihoodInterface::sharedPtr_t (new BDRate::Alpha::AlphaLik(nThread, nQuantAlpha, fName, isHPP));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonM0(const CodonModels::scalingType_t aScalingType,
												 	   const bool aUseCompression, const size_t aNThread,
												 	   const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
												 	   const std::string &aFileAlign, const std::string &aFileTree) {

	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(1, aScalingType, CodonModels::M0, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));

}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonM1(const CodonModels::scalingType_t aScalingType,
														const bool aUseCompression, const size_t aNThread,
														const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
														const std::string &aFileAlign, const std::string &aFileTree){

	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(2, aScalingType, CodonModels::M1, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));

}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonM2(const CodonModels::scalingType_t aScalingType,
														const bool aUseCompression, const size_t aNThread,
														const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
														const std::string &aFileAlign, const std::string &aFileTree){

	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(3, aScalingType, CodonModels::M2, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonM1a(const CodonModels::scalingType_t aScalingType,
														const bool aUseCompression, const size_t aNThread,
														const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
														const std::string &aFileAlign, const std::string &aFileTree) {

	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(2, aScalingType, CodonModels::M1a, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));

}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonM2a(const CodonModels::scalingType_t aScalingType,
														const bool aUseCompression, const size_t aNThread,
														const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
														const std::string &aFileAlign, const std::string &aFileTree) {

	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(3, aScalingType, CodonModels::M2a, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createCodonModel(const size_t aNClass, const CodonModels::scalingType_t aScalingType,
														 const bool aUseCompression, const size_t aNThread,
														 const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
														 const std::string &aFileAlign, const std::string &aFileTree) {
	return LikelihoodInterface::sharedPtr_t (new CodonModels::Base(aNClass, aScalingType, CodonModels::OTHER, aUseCompression, aNThread,
																	aNuclModel, aCFreqType, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createTreeInference(const TreeInference::scalingType_t aScalingType,
															const TreeInference::modelType_t aModelType, const size_t aNThread,
		   	   	   	   	   	   	   	   	   	   	   	   	    const TreeInference::nuclModel_t aNuclModel, const TreeInference::codonFreq_t aCFreqType,
		   	   	   	   	   	   	   	   	   	   	   	   	    MolecularEvolution::DataLoader::NewickParser *aNP, const std::string &aFileAlign,
		   	   	   	   	   	   	   	   	   	   	   	   	    TreeInference::Base::treeUpdateStrategy_t aStrat) {
	return LikelihoodInterface::sharedPtr_t (new TreeInference::Base(aScalingType, aModelType, aNThread, aNuclModel, aCFreqType, aNP, aFileAlign, aStrat));
}


} /* namespace Likelihood */
} /* namespace StatisticalModel */
