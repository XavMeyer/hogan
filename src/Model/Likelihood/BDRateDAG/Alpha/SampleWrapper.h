//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLEWRAPPERALPHA_H_
#define SAMPLEWRAPPERALPHA_H_

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Alpha {

class SampleWrapper {

	friend class AlphaLik;

public:
	SampleWrapper(const size_t aNSpecies, const size_t aNLambda,
				  const size_t aNMu);
	~SampleWrapper();

	void setSample(const std::vector<double> &sample);

	const double& getQ() const;
	const double& getAlpha() const;
	const double& getAlphaMu() const;
	const double& getAlphaLambda() const;
	const double& getBetaMu() const;
	const double& getBetaLambda() const;

	const std::vector<double>& getTs() const;
	const std::vector<double>& getTe() const;
	const std::vector<double>& getTL() const;
	const std::vector<double>& getTM() const;
	const std::vector<double>& getLambda() const;
	const std::vector<double>& getMu() const;


	std::string toString() const;

private:

	const size_t nSpecies, nLambda, nMu;

	double q;
	double alpha;
	double alphaM;
	double betaM;
	double alphaL;
	double betaL;
	std::vector<double> ts;
	std::vector<double> te;
	std::vector<double> tL;
	std::vector<double> tM;
	std::vector<double> lambda;
	std::vector<double> mu;
};

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SAMPLEWRAPPERALPHA_H_ */
