//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaV2Lik.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef ALPHAV2LIK_H_
#define ALPHAV2LIK_H_

#include "../FileReader.h"
#include "SampleWrapper.h"

#include "DAG/Node/Base/IncNodes.h"
#include "../Nodes/IncNodes.h"

#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"

#include "Model/Likelihood/LikelihoodInterface.h"

#include <string>
#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Alpha {

class AlphaV2Lik : public LikelihoodInterface {
public:
	AlphaV2Lik(const size_t aNThread, const size_t aNAlpha, const std::string aFileName, const bool aUseHPP=true);
	~AlphaV2Lik();

	double processLikelihood(const Sampler::Sample &sample);
	string getName(char sep) const;

private :
	typedef DAG::ReduceResultNode<double, DAG::Operator::Sum<double> > ReduceNode;

	const bool useHPP;
	const size_t nThread, nAlpha;
	size_t nLambda, nMu;

	std::vector<double> nrmTe, nrmTs, augTe, augTs, Qs;
	std::vector<std::vector<double> > fossils, nrmlFossils, augFossils;
	SampleWrapper *sw;
	//std::vector<double> aQs;

	/* DAG Scheduler */
	DAG::Scheduler::BaseScheduler *scheduler;

	/* DAG Root */
	ReduceNode *rootDAG;

	/* DAG Nodes */
	std::vector<PartialAlphaQuantileNode*> partialAQNodes;
	MergePartialAQNode* mergeAQNode;
	std::vector<NHPPNode*> NHPPNodes;
	std::vector<DataAugNode*> AugNodes;


	AlphaQuantileNode* aQNode;
	AugQuantileNode* augNode;
	std::vector<MeansHPPNode*> meansHPPNodes;
	std::vector<MeansNHPPNode*> meansNHPPNodes;
	ReduceNode *likHPP;

	TimeVectorNode *muTimesNode, *lambdaTimesNode;
	std::vector<BDRateLikNode*> muBDPartialNodes, lambdaBDPartialNodes;
	ReduceNode *likBDP;

	RateLikNode *muRateNode, *lambdaRateNode;

	void initFossils();

	void createDAG();
	void createAugNodes();
	void createNHPPNodes();
	void createMeansHPPNodes();

	//void initAlphaQuantile();
	void setSample(const std::vector<double> &sample);

};

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ALPHAV2LIK_H_ */
