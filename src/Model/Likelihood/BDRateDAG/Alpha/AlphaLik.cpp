//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaLik.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "AlphaLik.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Alpha {

AlphaLik::AlphaLik(const size_t aNThread, const size_t aNAlpha, const std::string aFileName, const bool aUseHPP) :
	useHPP(aUseHPP), nThread(aNThread), nAlpha(aNAlpha), fName(aFileName) {

	isLogLH = true;
	isUpdatableLH = false;

	// Read the file
	FileReader fr(aFileName);
	nLambda = fr.getNLambda();
	nMu = fr.getNMu();
	fossils = fr.getFossils();

    sw = new SampleWrapper(fossils.size(), nLambda, nMu);

	createDAG();

	if(nThread > 1) {
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

}

AlphaLik::~AlphaLik() {
	delete sw;
}

double AlphaLik::processLikelihood(const Sampler::Sample &sample) {
	// Update the values

	setSample(sample.getDblValues());
	//std::cout << sw->toString() << std::endl;

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	/*std::cout << "HMU : " << muRateNode->getResult() << std::endl;
	std::cout << "HLAMBDA : " << lambdaRateNode->getResult() << std::endl;
	std::cout << "muPart : " << muBDPartialNodes.front()->getResult() << std::endl;
	std::cout << "lbPart : " << lambdaBDPartialNodes.front()->getResult() << std::endl;
	std::cout << "HPP : " << meansHPPNodes.front()->getResult() << std::endl;
	std::cout << "Root : " << rootDAG->getResult() << std::endl;
	abort();*/

	// return the likelihood
	return rootDAG->getResult();
}

std::string AlphaLik::getName(char sep) const {
	return std::string("BDRateAlphaV1DAG");
}

std::string AlphaLik::getFName() const {
	return fName;
}

size_t AlphaLik::getNThread() const {
		return nThread;
}

size_t AlphaLik::getNAlpha() const {
		return nAlpha;
}


const std::vector< std::vector<double> >& AlphaLik::getFossils() const {
	return fossils;
}

const size_t AlphaLik::getNLambda() const {
	return nLambda;
}

const size_t AlphaLik::getNMu() const {
	return nMu;
}

bool AlphaLik::isHPP() const {
	return useHPP;
}

void AlphaLik::createDAG() {

	rootDAG = new ReduceNode();

	aQNode = new AlphaQuantileNode(nAlpha, sw->getQ(), sw->getAlpha());
	if(useHPP) {
		augNode = NULL;
	} else {
		augNode = new AugQuantileNode(sw->getMu().back());
	}


	// NHPP
	if(nThread == 1) {
		if(useHPP) {
			MeansHPPNode *ptr;
			ptr = new MeansHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe());
			ptr->addChild(aQNode);
			meansHPPNodes.push_back(ptr);
			rootDAG->addChild(ptr);
		} else {
			MeansNHPPNode *ptr;
			ptr = new MeansNHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe(), sw->getQ(), sw->getMu().back());
			ptr->addChild(aQNode);
			ptr->addChild(augNode);
			meansNHPPNodes.push_back(ptr);
			rootDAG->addChild(ptr);
		}
	} else {

		//likHPP = new ReduceNode();
		size_t nChunk = std::min(4*nThread, fossils.size()/20);
		size_t sizeChunk = ceil(static_cast<double>(fossils.size()) / static_cast<double>(nChunk));
		for(size_t iC=0; iC<nChunk; ++iC) {
			size_t start = iC*sizeChunk;
			size_t end = std::min((iC+1)*sizeChunk, fossils.size());
			//std::cout << "Beg : " << start << " -- End : " << end << std::endl;

			if(useHPP) {
				MeansHPPNode *ptr;
				ptr = new MeansHPPNode(start, end, nAlpha, fossils, sw->getTs(), sw->getTe());
				ptr->addChild(aQNode);
				meansHPPNodes.push_back(ptr);
				rootDAG->addChild(ptr);
			} else {
				MeansNHPPNode *ptr;
				ptr = new MeansNHPPNode(start, end, nAlpha, fossils, sw->getTs(), sw->getTe(), sw->getQ(), sw->getMu().back());
				ptr->addChild(aQNode);
				ptr->addChild(augNode);
				meansNHPPNodes.push_back(ptr);
				rootDAG->addChild(ptr);
			}
		}
		//rootDAG->addChild(likHPP);
	}

	// BD
	//likBDP = new ReduceNode();
	muTimesNode = new TimeVectorNode(TimeVectorNode::MU, sw->getTs(), sw->getTM());
	lambdaTimesNode = new TimeVectorNode(TimeVectorNode::LAMBDA, sw->getTs(), sw->getTL());

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::MU, fossils.size(), sw->getMu(), sw->getTs(), sw->getTe());
		ptr->addChild(muTimesNode);
		muBDPartialNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::LAMBDA, fossils.size(), sw->getLambda(), sw->getTs(), sw->getTe());
		ptr->addChild(lambdaTimesNode);
		lambdaBDPartialNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}

	//rootDAG->addChild(likBDP);

	// Rates
	muRateNode = new RateLikNode(sw->getAlphaMu(), sw->getBetaMu(), sw->getMu());
	lambdaRateNode = new RateLikNode(sw->getAlphaLambda(), sw->getBetaLambda(), sw->getLambda());
	rootDAG->addChild(muRateNode);
	rootDAG->addChild(lambdaRateNode);
}

/*void AlphaLik::initAlphaQuantile() {
	boost::math::gamma_distribution<double> gammaDist(sw->getAlpha(), 1./sw->getAlpha());
	double incr = 1./(double)nAlpha;
	size_t id=0;
	double sum = 0.;
	for(double pos=incr/2.; pos<1.0; pos+=incr){
		double quantile = boost::math::quantile( gammaDist, pos );
		aQs[id] = quantile;
		sum += aQs[id]; // Get m_i
		id++;
	}
	sum /= (double)aQs.size();

	for(uint iAQ=0; iAQ<aQs.size(); ++iAQ){
		aQs[iAQ] /= sum; // normalize m_i => w_i
		aQs[iAQ] *= sw->getQ(); // multiply q by w_i
	}

}*/

void AlphaLik::setSample(const std::vector<double> &sample) {

	sw->setSample(sample);

	// Alpha quantiles
	aQNode->updated();

	// meanHPP
	if(useHPP) {
		for(size_t iF=0; iF<meansHPPNodes.size(); ++iF) {
			meansHPPNodes[iF]->updated();
		}
	} else {
		for(size_t iF=0; iF<meansNHPPNodes.size(); ++iF) {
			meansNHPPNodes[iF]->updated();
		}
	}

	// BD partial MU
	muTimesNode->updated();
	for(size_t iM=0; iM<muBDPartialNodes.size(); ++iM) {
		muBDPartialNodes[iM]->updated();
	}

	// BD partial Lambda
	lambdaTimesNode->updated();
	for(size_t iL=0; iL<lambdaBDPartialNodes.size(); ++iL) {
		lambdaBDPartialNodes[iL]->updated();
	}

	muRateNode->updated();
	lambdaRateNode->updated();
}

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
