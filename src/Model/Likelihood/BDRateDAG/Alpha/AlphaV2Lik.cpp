//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaV2Lik.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "AlphaV2Lik.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Alpha {

AlphaV2Lik::AlphaV2Lik(const size_t aNThread, const size_t aNAlpha, const std::string aFileName, const bool aUseHPP) :
	useHPP(aUseHPP), nThread(aNThread), nAlpha(aNAlpha), Qs(nAlpha, 0.) {

	isLogLH = true;
	isUpdatableLH = false;

	// Read the file
	FileReader fr(aFileName);
	nLambda = fr.getNLambda();
	nMu = fr.getNMu();
	fossils = fr.getFossils();
	initFossils();

    sw = new SampleWrapper(fossils.size(), nLambda, nMu);

	createDAG();

	if(nThread > 1) {
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

}

AlphaV2Lik::~AlphaV2Lik() {
	delete sw;
}

void AlphaV2Lik::initFossils() {
	for(size_t iF=0; iF<fossils.size(); ++iF) {
		if(fossils[iF].back() == 0.0) {
			augFossils.push_back(fossils[iF]);
		} else {
			nrmlFossils.push_back(fossils[iF]);
		}
	}
}

double AlphaV2Lik::processLikelihood(const Sampler::Sample &sample) {
	// Update the values

	setSample(sample.getDblValues());

	//initAlphaQuantile();

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	/*std::cout << "HMU : " << muRateNode->getResult() << std::endl;;
	std::cout << "HLAMBDA : " << lambdaRateNode->getResult() << std::endl;;
	std::cout << "HPP : " << likHPP->getResult() << std::endl;;
	std::cout << "BDP : " << likBDP->getResult() << std::endl;;*/

	// return the likelihood
	return rootDAG->getResult();
}

std::string AlphaV2Lik::getName(char sep) const {
	return std::string("BDRateAlphaV2DAG");
}

void AlphaV2Lik::createDAG() {

	rootDAG = new ReduceNode();

	// NHPP
	if(nThread == 1) {

		aQNode = new AlphaQuantileNode(nAlpha, sw->getQ(), sw->getAlpha());
		if(useHPP) {
			augNode = NULL;
			MeansHPPNode *ptr;
			ptr = new MeansHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe());
			ptr->addChild(aQNode);
			meansHPPNodes.push_back(ptr);
			rootDAG->addChild(ptr);
		} else {
			augNode = new AugQuantileNode(sw->getMu().back());
			MeansNHPPNode *ptr;
			ptr = new MeansNHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe(), sw->getQ(), sw->getMu().back());
			ptr->addChild(aQNode);
			ptr->addChild(augNode);
			meansNHPPNodes.push_back(ptr);
			rootDAG->addChild(ptr);
		}
	} else {

		// Cut alpha quantile processing
		{
			mergeAQNode = new MergePartialAQNode(nAlpha, sw->getQ(), Qs);

			size_t nChunk = std::min(nThread, nAlpha/10);
			size_t sizeChunk = ceil(static_cast<double>(nAlpha) / static_cast<double>(nChunk));
			for(size_t iC=0; iC<nChunk; ++iC) {
				size_t start = iC*sizeChunk;
				size_t end = std::min((iC+1)*sizeChunk, nAlpha);

				PartialAlphaQuantileNode *ptr;
				ptr = new PartialAlphaQuantileNode(nAlpha, sw->getQ(), sw->getAlpha(), start, end, Qs);
				partialAQNodes.push_back(ptr);
				mergeAQNode->addChild(ptr);
			}

		}

		//std::cout << "AQ ok" << std::endl;

		if(useHPP) {
			createMeansHPPNodes();
		} else {
			createAugNodes();
			//std::cout << "AugNode ok" << std::endl;
			createNHPPNodes();
			//std::cout << "NHPP ok" << std::endl;
		}
	}

	// BD
	//likBDP = new ReduceNode();
	muTimesNode = new TimeVectorNode(TimeVectorNode::MU, sw->getTs(), sw->getTM());
	lambdaTimesNode = new TimeVectorNode(TimeVectorNode::LAMBDA, sw->getTs(), sw->getTL());

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::MU, fossils.size(), sw->getMu(), sw->getTs(), sw->getTe());
		ptr->addChild(muTimesNode);
		muBDPartialNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::LAMBDA, fossils.size(), sw->getLambda(), sw->getTs(), sw->getTe());
		ptr->addChild(lambdaTimesNode);
		lambdaBDPartialNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}

	//rootDAG->addChild(likBDP);

	// Rates
	muRateNode = new RateLikNode(sw->getAlphaMu(), sw->getBetaMu(), sw->getMu());
	lambdaRateNode = new RateLikNode(sw->getAlphaLambda(), sw->getBetaLambda(), sw->getLambda());
	rootDAG->addChild(muRateNode);
	rootDAG->addChild(lambdaRateNode);
}

void AlphaV2Lik::createAugNodes() {

	augNode = new AugQuantileNode(sw->getMu().back());

	size_t nChunk = std::min(2*nThread, augFossils.size()/20);
	size_t sizeChunk = ceil(static_cast<double>(augFossils.size()) / static_cast<double>(nChunk));
	for(size_t iC=0; iC<nChunk; ++iC) {
		size_t start = iC*sizeChunk;
		size_t end = std::min((iC+1)*sizeChunk, augFossils.size());

		DataAugNode *ptr;
		ptr = new DataAugNode(start, end, augFossils, augTs, augTe, sw->getQ(), sw->getMu().back());
		ptr->addChild(augNode);
		AugNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}

}

void AlphaV2Lik::createNHPPNodes() {

	size_t nChunk = std::min(3*nThread, nrmlFossils.size()/20);
	size_t sizeChunk = ceil(static_cast<double>(nrmlFossils.size()) / static_cast<double>(nChunk));
	for(size_t iC=0; iC<nChunk; ++iC) {
		size_t start = iC*sizeChunk;
		size_t end = std::min((iC+1)*sizeChunk, nrmlFossils.size());

		NHPPNode *ptr;
		ptr = new NHPPNode(start, end, nAlpha, nrmlFossils, nrmTs, nrmTe, sw->getQ(), sw->getMu().back());
		ptr->addChild(mergeAQNode);
		NHPPNodes.push_back(ptr);
		rootDAG->addChild(ptr);
	}
}

void AlphaV2Lik::createMeansHPPNodes() {

	size_t nChunk = std::min(3*nThread, fossils.size()/20);
	size_t sizeChunk = ceil(static_cast<double>(fossils.size()) / static_cast<double>(nChunk));
	for(size_t iC=0; iC<nChunk; ++iC) {
		size_t start = iC*sizeChunk;
		size_t end = std::min((iC+1)*sizeChunk, fossils.size());

		MeansHPPNode *ptr;
		ptr = new MeansHPPNode(start, end, nAlpha, fossils, sw->getTs(), sw->getTe());
		ptr->addChild(aQNode);
		meansHPPNodes.push_back(ptr);
		rootDAG->addChild(ptr);

	}

}


void AlphaV2Lik::setSample(const std::vector<double> &sample) {

	sw->setSample(sample);

	// Get te thingy
	nrmTe.clear();
	nrmTs.clear();
	augTe.clear();
	augTs.clear();

	for(size_t iF=0; iF<fossils.size(); ++iF) {
		if(fossils[iF].back() == 0.0) {
			augTs.push_back(sw->getTs()[iF]);
			augTe.push_back(sw->getTe()[iF]);
		} else {
			nrmTs.push_back(sw->getTs()[iF]);
			nrmTe.push_back(sw->getTe()[iF]);
		}
	}


	// Alpha quantiles
	aQNode->updated();

	// meanHPP
	if(useHPP) {
		for(size_t iF=0; iF<meansHPPNodes.size(); ++iF) {
			meansHPPNodes[iF]->updated();
		}
	} else {
		for(size_t iF=0; iF<meansNHPPNodes.size(); ++iF) {
			meansNHPPNodes[iF]->updated();
		}
	}

	// BD partial MU
	muTimesNode->updated();
	for(size_t iM=0; iM<muBDPartialNodes.size(); ++iM) {
		muBDPartialNodes[iM]->updated();
	}

	// BD partial Lambda
	lambdaTimesNode->updated();
	for(size_t iL=0; iL<lambdaBDPartialNodes.size(); ++iL) {
		lambdaBDPartialNodes[iL]->updated();
	}

	muRateNode->updated();
	lambdaRateNode->updated();
}

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
