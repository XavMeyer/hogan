//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FileReader.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef FILEREADER_H_
#define FILEREADER_H_

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class FileReader {
public:
	FileReader(const std::string &fileName);
	 ~FileReader();

	 size_t getNLambda() const;
	 size_t getNMu() const;
	 size_t getNExtant() const;

	 std::vector<std::vector<double> > getFossils() const;

private:
	 size_t nLambda, nMu, nExtant;
	 std::vector<std::vector<double> > fossils;

	 void read(const std::string &fileName);
};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* FILEREADER_H_ */
