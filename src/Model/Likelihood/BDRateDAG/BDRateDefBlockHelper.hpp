//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BDRateDefBlockHelper.hpp
 *
 * @date May 5, 2015
 * @author meyerx
 * @brief
 */

#ifndef BDRATEHELPER_H_
#define BDRATEHELPER_H_

#include "ParameterBlock/Block.h"
#include "ParameterBlock/Blocks.h"
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Helper {

using namespace ParameterBlock;

template <typename T>
void initStdParameters(const T* likBDRate, Parameters &params) {
	bool reflect = true;
	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	// Frequencies
	// bQ ==> 5%
	double qFreq = 0.05;
	// bTS/TE ==> 50%
	double tsteFreq = 0.65/(2.*(double)fossils.size());
	// bTL ==> 12.5%
	double tlFreq = 0.075/(double)(nLambda-1);
	// bTM ==> 12.5%
	double tmFreq = 0.075/(double)(nMu-1);
	// bRate ==> 20%
	double rateFreq = 0.15/(double)(nMu+nLambda);

	// Init Q rate
	ParamDblDef pQ("q_rate", PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.5, 1./1.1), reflect);
	pQ.setMin(0.);
	pQ.setMax(1000.);
	pQ.setFreq(qFreq);
	pQ.setType(1);
	params.addParameter(pQ);

	// Init ts
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double max = fossils[iF].front();

		std::stringstream ssTS;
		ssTS << "ts_" << iF;
		ParamDblDef pTS(ssTS.str(), PriorInterface::createNoPrior(), reflect);
		// Bounded between maximum time and infinity
		pTS.setMin(max);
		pTS.setMax(std::numeric_limits<double>::max());
		pTS.setFreq(tsteFreq);
		pTS.setType(3);
		params.addParameter(pTS);
	}

	// Init te
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double min = fossils[iF].back();
		std::stringstream ssTE;
		ssTE << "te_" << iF;
		ParamDblDef pTE(ssTE.str(), PriorInterface::createNoPrior(), reflect);
		// Bounded between minium time and 0
		pTE.setMin(0.);
		pTE.setMax(min);
		pTE.setFreq(tsteFreq);
		pTE.setType(3);
		params.addParameter(pTE);
	}

	// Init TL
	for(size_t iL=0; iL<nLambda-1; ++iL){
		std::stringstream ss;
		ss << "tL_" << iL;
		ParamDblDef pL(ss.str(), PriorInterface::createNoPrior(), reflect);
		pL.setMin(0.);
		pL.setMax(std::numeric_limits<double>::max());
		pL.setFreq(tlFreq);
		pL.setType(2);
		params.addParameter(pL);
	}

	// Init TM
	for(size_t iM=0; iM<nMu-1; ++iM){
		std::stringstream ss;
		ss << "tM_" << iM;
		ParamDblDef pM(ss.str(), PriorInterface::createNoPrior(), reflect);
		pM.setMin(0.);
		pM.setMax(std::numeric_limits<double>::max());
		pM.setFreq(tmFreq);
		pM.setType(2);
		params.addParameter(pM);
	}

	// Init lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		std::stringstream ss;
		ss << "lambda_" << iL;
		ParamDblDef pL(ss.str(), PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.1, 1./1.1), reflect);
		pL.setMin(0.);
		pL.setMax(100.0);
		pL.setFreq(rateFreq);
		pL.setType(1);
		params.addParameter(pL);
	}

	// Init mu
	for(size_t iM=0; iM<nMu; ++iM){
		std::stringstream ss;
		ss << "mu_" << iM;
		ParamDblDef pM(ss.str(), PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.1, 1./1.1), reflect);
		pM.setMin(0.);
		pM.setMax(100.0);
		pM.setFreq(rateFreq);
		pM.setType(1);
		params.addParameter(pM);
	}
}

template <typename T>
void initAlphaParameters(const T* likBDRate, Parameters &params) {
	bool reflect = true;
	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	// Frequencies
	// bQ ==> 5%
	double qFreq = 0.005;
	double alphaFreq = 0.0025;
	// bTS/TE ==> 50%
	double tsteFreq = 0.9/(2.*(double)fossils.size());
	// Rate freq
	double rateFreq = 0.08/(double)(nMu+nLambda);

	// Init Q rate
	ParamDblDef pQ("q_rate", PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.5, 1./1.1), reflect);
	pQ.setMin(0.);
	pQ.setMax(1000.);
	pQ.setFreq(qFreq);
	pQ.setType(1);
	params.addParameter(pQ);

	// Init Alpha, Beta
	ParamDblDef pAlpha("alpha", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 0.05, 20.), reflect);
	pAlpha.setMin(0.05);
	pAlpha.setMax(20.);
	pAlpha.setFreq(qFreq);
	pAlpha.setType(1);
	params.addParameter(pAlpha);

	ParamDblDef pAM("alphaM", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1., 20.), reflect);
	pAM.setMin(1.);
	pAM.setMax(20.);
	pAM.setFreq(alphaFreq);
	pAM.setType(1);
	params.addParameter(pAM);

	ParamDblDef pBM("betaM", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1., 20.), reflect);
	pBM.setMin(1.);
	pBM.setMax(20.);
	pBM.setFreq(alphaFreq);
	pBM.setType(1);
	params.addParameter(pBM);

	ParamDblDef pAL("alphaL",PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1., 20.), reflect);
	pAL.setMin(1.);
	pAL.setMax(20.);
	pAL.setFreq(alphaFreq);
	pAL.setType(1);
	params.addParameter(pAL);

	ParamDblDef pBL("betaL", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1., 20.), reflect);
	pBL.setMin(1.);
	pBL.setMax(20.);
	pBL.setFreq(alphaFreq);
	pBL.setType(1);
	params.addParameter(pBL);

	// Init ts
	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Since it is sorted, max elements is always first
		double max = fossils[iF].front();

		stringstream ssTS;
		ssTS << "ts_" << iF;
		ParamDblDef pTS(ssTS.str(), PriorInterface::createNoPrior(), reflect);
		// Bounded between maximum time and infinity
		pTS.setMin(max);
		pTS.setMax(numeric_limits<double>::max());
		pTS.setFreq(tsteFreq);
		pTS.setType(3);
		params.addParameter(pTS);
	}

	// Init te
	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Since it is sorted, max elements is always last
		double min = fossils[iF].back();
		stringstream ssTE;
		ssTE << "te_" << iF;
		ParamDblDef pTE(ssTE.str(), PriorInterface::createNoPrior(), reflect);
		// Bounded between minium time and 0
		pTE.setMin(0.);
		pTE.setMax(min);
		pTE.setFreq(tsteFreq);
		pTE.setType(3);
		params.addParameter(pTE);
	}

	// Init TL
	for(size_t iL=0; iL<nLambda-1; ++iL){
		stringstream ss;
		ss << "tL_" << iL;
		ParamDblDef pL(ss.str(), PriorInterface::createNoPrior(), reflect);
		pL.setMin(0.);
		pL.setMax(numeric_limits<double>::max());
		pL.setType(2);
		params.addParameter(pL);
	}

	// Init TM
	for(size_t iM=0; iM<nMu-1; ++iM){
		stringstream ss;
		ss << "tM_" << iM;
		ParamDblDef pM(ss.str(), PriorInterface::createNoPrior(), reflect);
		pM.setMin(0.);
		pM.setMax(numeric_limits<double>::max());
		pM.setType(2);
		params.addParameter(pM);
	}

	// Init lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		stringstream ss;
		ss << "lambda_" << iL;
		ParamDblDef pL(ss.str(), PriorInterface::createNoPrior(), reflect);
		pL.setMin(0.);
		pL.setMax(100.0);
		pL.setFreq(rateFreq);
		pL.setType(1);
		params.addParameter(pL);
	}

	// Init mu
	for(size_t iM=0; iM<nMu; ++iM){
		stringstream ss;
		ss << "mu_" << iM;
		ParamDblDef pM(ss.str(), PriorInterface::createNoPrior(), reflect);
		pM.setMin(0.);
		pM.setMax(100.0);
		pM.setFreq(rateFreq);
		pM.setType(1);
		params.addParameter(pM);
	}
}


template <typename T>
void initStdBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const T* likBDRate,
				   const double sigmaTSE, const double sigmaTLM, const double sigmaR,
				   const Parameters &params, Blocks &blocks, const uint NBPPB) {

	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	size_t iParam = 0;

	const size_t nBPerF = NBPPB;
	div_t divRes = div((int)fossils.size(), (int)nBPerF);

	size_t nBTSE = divRes.quot;
	if(divRes.rem > (int)((double)nBPerF*4./5.)) nBTSE++;

	size_t nP = Parallel::mcmcMgr().getNProposal();
	const double lTSE = T::getOptiL(nP, 2*nBPerF, sigmaTSE)/sqrt(2*nBPerF);
	const double lTLM = T::getOptiL(nP, 2*(nMu-1), sigmaTLM)/sqrt(2*(nMu-1));
	const double lR = T::getOptiL(nP, 1+2*nMu, sigmaR)/sqrt(1+2*nMu);

	// init block and block modifier
    Block::sharedPtr_t bRate(new Block());
    bRate->setName("BlockRate");

    Block::sharedPtr_t bT(new Block());
    bT->setName("Times");


	Block::sharedPtr_t bTSE[nBTSE];
	for(size_t iB=0; iB < nBTSE; ++iB){
		bTSE[iB].reset(new Block());
		stringstream ss;
		ss << "TimesSE_" << iB;
		bTSE[iB]->setName(ss.str());
	}

	// Add block Q
	bRate->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;

	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Ts
		uint idx = iF/nBPerF;
		if(idx >= nBTSE) idx = iF % nBTSE;
		bTSE[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lTSE, params), params.getParameterFreq(iParam));
		++iParam;
	}

	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Te
		// Te, if Te is equal to 0 we dont want to touch it
		if(fossils[iF].back() != 0.){
			uint idx = iF/nBPerF;
			if(idx >= nBTSE) idx = iF % nBTSE;
			bTSE[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lTSE, params), params.getParameterFreq(iParam));
		}
		++iParam;
	}

	// tL
	for(size_t iL=0; iL<nLambda-1; ++iL){
		bT->addParameter(iParam,  BlockModifier::createGaussianWindow(lTLM, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// tM
	for(size_t iM=0; iM<nMu-1; ++iM){
		bT->addParameter(iParam, BlockModifier::createGaussianWindow(lTLM, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// Lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		bRate->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// Mu
	for(size_t iM=0; iM<nMu; ++iM){
		bRate->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
		++iParam;
	}

	blocks.addBlock(bRate);
	blocks.addBlock(bT);
	for(size_t iB=0; iB < nBTSE; ++iB){
		 blocks.addBlock(bTSE[iB]);
	}
}

template <typename T>
void initAlphaBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const T* likBDRate,
		             const double sigmaTSE, const double sigmaTLM, const double sigmaR,
	     	 	 	 const Parameters &params, Blocks &blocks, const uint NPPB) {

	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	size_t iParam = 0;

	const size_t nBPerF = NPPB;

	div_t divRes = div((int)fossils.size(), (int)nBPerF);
	size_t nBTSE = divRes.quot;
	if(divRes.rem > (int)((double)nBPerF*4./5.)) nBTSE++;

	div_t divResLM = div((int)nLambda, (int)nBPerF);
	size_t nBLM = divResLM.quot;
	if(divResLM.rem > (int)((double)nBPerF*4./5.)) nBLM++;

	size_t nP = Parallel::mcmcMgr().getNProposal();
	const double lTSE = T::getOptiL(nP, 2*nBPerF, sigmaTSE)/sqrt(2*nBPerF);
	const double lR = T::getOptiL(nP, nMu, sigmaR)/sqrt(nMu);

	// init block and block modifier
    Block::sharedPtr_t bQ(new Block());
    bQ->setName("RateQ");

    Block::sharedPtr_t bAB(new Block());
    bAB->setName("AlphaBeta");

    Block::sharedPtr_t bRateL[nBLM];
    Block::sharedPtr_t bRateM[nBLM];
	for(size_t iB=0; iB < nBLM; ++iB){
		bRateL[iB].reset(new Block());
		bRateM[iB].reset(new Block());

		stringstream ssL, ssM;
		ssL << "RateL_" << iB;
		ssM << "RateM_" << iB;
		bRateL[iB]->setName(ssL.str());
		bRateM[iB]->setName(ssM.str());
	}

	Block::sharedPtr_t bTSE[nBTSE];
	for(size_t iB=0; iB < nBTSE; ++iB){
		bTSE[iB].reset(new Block());
		stringstream ss;
		ss << "TimesSE_" << iB;
		bTSE[iB]->setName(ss.str());
	}

	// Block::sharedPtr_t for parameter Q and alpha
	bQ->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;
	bQ->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;

	// Block::sharedPtr_t for alphaMu, BetaMu, alphaLambda, betaLambda
	bAB->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;
	bAB->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;
	bAB->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;
	bAB->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
	++iParam;

	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Ts
		uint idx = iF/nBPerF;
		if(idx >= nBTSE) idx = iF % nBTSE;
		bTSE[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lTSE, params), params.getParameterFreq(iParam));
		++iParam;
	}

	for(size_t iF=0; iF<fossils.size(); ++iF){
		// Te, if Te is equal to 0 we dont want to touch it
		if(fossils[iF].back() != 0.){
			uint idx = iF/nBPerF;
			if(idx >= nBTSE) idx = iF % nBTSE;
			bTSE[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lTSE, params), params.getParameterFreq(iParam));
		}
		++iParam;
	}

	// tL
	for(size_t iL=0; iL<nLambda-1; ++iL){
		//bTL->addParameter(iParam,  BlockModifier::createGaussianWindow(sigmaTLM, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// tM
	for(size_t iM=0; iM<nMu-1; ++iM){
		//bTM->addParameter(iParam, BlockModifier::createGaussianWindow(sigmaTLM, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// Lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		uint idx = iL/nBPerF;
		if(idx >= nBLM) idx = iL % nBLM;
		bRateL[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
		++iParam;
	}

	// Mu
	for(size_t iM=0; iM<nMu; ++iM){
		uint idx = iM/nBPerF;
		if(idx >= nBLM) idx = iM % nBLM;
		bRateM[idx]->addParameter(iParam, BlockModifier::createGaussianWindow(lR, params), params.getParameterFreq(iParam));
		++iParam;
	}

    blocks.addBlock(bQ);
    blocks.addBlock(bAB);
	for(size_t iB=0; iB < nBLM; ++iB){
		blocks.addBlock(bRateM[iB]);
		blocks.addBlock(bRateL[iB]);
	}
	for(size_t iB=0; iB < nBTSE; ++iB){
		 blocks.addBlock(bTSE[iB]);
	}
}


template <typename T>
void initFirstSampleStd(const T* likBDRate, Sample &sample){
	const double expParam = 1.;
	const RNG *rng = Parallel::mpiMgr().getPRNG();

	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	// qRate
	sample.dblValues.push_back(rng->genExponential(expParam));

	// Init ts
	double maxTime = 0.0;
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double max = fossils[iF].front();
		double tmpVal = max+rng->genExponential(0.75);
		maxTime = std::max(tmpVal, maxTime);
		sample.dblValues.push_back(tmpVal);
	}

	// Init te
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double min = fossils[iF].back();
		sample.dblValues.push_back(min*rng->genBeta(2.5,1));

	}

	// init tL
	double sliceTL = maxTime / (nLambda);
	for(size_t iL=0; iL<nLambda-1; ++iL){
		sample.dblValues.push_back(sliceTL);
	}

	// init tM
	double sliceTM = maxTime / (nMu);
	sliceTM = (sliceTM == sliceTL) ? sliceTM+1.0 : sliceTM;
	for(size_t iM=0; iM<nMu-1; ++iM){
		sample.dblValues.push_back(sliceTM);
	}

	// Init lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}


	// Init mu
	for(size_t iM=0; iM<nMu; ++iM){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}
}

template <typename T>
void initFirstSampleAlpha(const T* likBDRate, Sample &sample){
	const double expParam = 1.;
	const RNG *rng = Parallel::mpiMgr().getPRNG();

	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	// qRate and alpha
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.0);

	//  alphaMu, BetaMu, alphaLambda, betaLambda
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.);
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.);

	// Init ts
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double max = fossils[iF].front();
		sample.dblValues.push_back(max*1.05);
	}

	// Init te
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double min = fossils[iF].back();
		sample.dblValues.push_back(min*0.95);

	}

	// init tL
	double sliceTL = 1.;
	for(size_t iL=0; iL<nLambda-1; ++iL){
		sample.dblValues.push_back(sliceTL);
	}

	// init tM
	double sliceTM = 1.;
	for(size_t iM=0; iM<nMu-1; ++iM){
		sample.dblValues.push_back(sliceTM);
	}

	// Init lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}


	// Init mu
	for(size_t iM=0; iM<nMu; ++iM){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}
}

template <typename T>
void initFirstSamplePlant(const T* likBDRate, Sample &sample){
	const double expParam = 1.;
	const RNG *rng = Parallel::mpiMgr().getPRNG();

	const std::vector< std::vector<double> > &fossils = likBDRate->getFossils();
	size_t nLambda = likBDRate->getNLambda();
	size_t nMu = likBDRate->getNMu();

	const double epochs[] = {0., 2.588, 5.333, 23.03, 33.9, 56.0, 66.0, 100.5, 145.0, 163.5, 174.1, 201.3,
							  237, 247.2, 252.17, 272.3, 298.9, 323.2, 358.9, 382.7, 393.3, 419.2, 423.0,
							  427.4, 433.4, 443.4, 458.4, 470.0, 485.4, 497, 541.0 };

	assert(31 == nLambda);

	// qRate and alpha
	//sample.dblValues.push_back(rng->genExponential(expParam));
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.0);

	//  alphaMu, BetaMu, alphaLambda, betaLambda
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.);
	sample.dblValues.push_back(3.0);
	sample.dblValues.push_back(5.);

	// Init ts
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double max = fossils[iF].front();
		sample.dblValues.push_back(max*1.05);
	}

	// Init te
	for(size_t iF=0; iF<fossils.size(); ++iF){
		double min = fossils[iF].back();
		sample.dblValues.push_back(min*0.95);

	}

	// init tL
	for(size_t iL=0; iL<nLambda-1; ++iL){
		sample.dblValues.push_back(epochs[iL+1]-epochs[iL]);
	}

	// init tM
	for(size_t iM=0; iM<nMu-1; ++iM){
		sample.dblValues.push_back(epochs[iM+1]-epochs[iM]);
	}

	// Init lambda
	for(size_t iL=0; iL<nLambda; ++iL){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}


	// Init mu
	for(size_t iM=0; iM<nMu; ++iM){
		sample.dblValues.push_back(rng->genExponential(expParam));
	}
}


} /* namespace Helper */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BDRATEHELPER_H_ */
