//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PriorTXNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "PriorTXNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

PriorTXNode::PriorTXNode(const double dirichletAlpha, const size_t nLambda) :
		DAG::ResultNode<double>(){
	vecTimeLNode = vecTimeMNode = NULL;

	dirichCst = 1.;
	double tmp = boost::math::tgamma(dirichletAlpha);
	for(unsigned int i=0; i<nLambda; ++i){
		dirichCst *=  tmp;
	}
	dirichCst /= boost::math::tgamma(nLambda * dirichletAlpha);
}

PriorTXNode::~PriorTXNode() {
}

double PriorTXNode::processPriorTx(const std::vector<double> &vTx) {
	double prior = 1.0;
	for(size_t i=1; i<vTx.size(); ++i){
		prior *= (vTx[i-1]-vTx[i])/vTx[0];
	}

	prior /= dirichCst;
	if(prior < 0) {
		prior = 0.;
	}

	return log(prior);
}

void PriorTXNode::doProcessing() {
	this->result = processPriorTx(vecTimeLNode->getVecTime());
	this->result += processPriorTx(vecTimeMNode->getVecTime());
}

bool PriorTXNode::processSignal(BaseNode* aChild) {
	return true;
}

void PriorTXNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(TimeVectorNode)){
		TimeVectorNode *tmpPtr = dynamic_cast<TimeVectorNode*>(aChild);
		if(tmpPtr->getEventType() == TimeVectorNode::LAMBDA) {
			vecTimeLNode = tmpPtr;
		} else {
			vecTimeMNode = tmpPtr;
		}
	}
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
