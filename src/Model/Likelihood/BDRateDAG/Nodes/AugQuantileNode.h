//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AugQuantileNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef AUGQUANTILENODE_H_
#define AUGQUANTILENODE_H_

#include <vector>

#include <boost/math_fwd.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/distributions.hpp>

#include <DAG/Node/Base/BaseNode.h>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class AugQuantileNode: public DAG::BaseNode {
public:
	AugQuantileNode(const double &aExtRate);
	~AugQuantileNode();

	/*void setQ(const double aQ);
	void setAlpha(const double aAlpha);*/

	const std::vector<double>& getGammaPDF() const;
	const std::vector<double>& getGM() const;
	const double& getSumGammaPDF() const;

private:

	static const unsigned int nQuantile;
	static const double quantile[];

	const double &extRate;
	double sumGammaPDF;
	std::vector<double> GM, gammaPDF;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* AUGQUANTILENODE_H_ */
