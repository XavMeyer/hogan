//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TimeVectorNode.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "TimeVectorNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

TimeVectorNode::TimeVectorNode(const event_t aEventType, const std::vector<double> &aTs, const std::vector<double> &aTx) :
		DAG::BaseNode(), eventType(aEventType), ts(aTs), tx(aTx) {
	//ptrTs = ptrRate = NULL;
}

TimeVectorNode::~TimeVectorNode() {
}

/*void TimeVectorNode::setPtrTs(std::vector<double> *aPtrTs) {
	ptrTs = aPtrTs;
	updated();
}

void TimeVectorNode::setPtrRate(std::vector<double> *aPtrRate) {
	ptrRate = aPtrRate;
	updated();
}*/

TimeVectorNode::event_t TimeVectorNode::getEventType() const {
	return eventType;
}

const std::vector<double>& TimeVectorNode::getVecTime() const {
	return vecTime;
}

void TimeVectorNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	double rootTime = *max_element(ts.begin(), ts.end());

	vecTime.resize(tx.size()+2);

	vecTime[0] = rootTime;
	vecTime[vecTime.size()-1] = 0.;

	for(size_t iT=vecTime.size()-2; iT > 0; --iT){
		//std::cout << iT << "\t" << vecTime[iT+1] << "\t" << tx[iT-1] << std::endl;
		vecTime[iT] = vecTime[iT+1] + tx[iT-1];
	}
	//std::cout << std::endl;

	if(vecTime[0] < vecTime[1]) vecTime[0] = vecTime[1]+1;

	/*cp.endTime();
	std::cout << "[TimeVectorNode] Duration : " << cp.duration() << std::endl;*/
}

bool TimeVectorNode::processSignal(BaseNode* aChild) {
	return true;
}

void TimeVectorNode::doAddChild(const size_t rowId, BaseNode* aChild) {

}



} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
