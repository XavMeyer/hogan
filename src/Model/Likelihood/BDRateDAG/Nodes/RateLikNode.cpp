//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RateLikNode.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "RateLikNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

RateLikNode::RateLikNode(const double &aAlpha, const double &aBeta, const std::vector<double> &aRates) :
		DAG::ResultNode<double>(), alpha(aAlpha), beta(aBeta), rates(aRates) {
	//alpha = beta = 0.;
}

RateLikNode::~RateLikNode() {
}

/*void RateLikNode::setAlpha(const double aAlpha) {
	alpha = aAlpha;
	updated();
}

void RateLikNode::setBeta(const double aBeta) {
	beta = aBeta;
	updated();
}

void RateLikNode::setRates(std::vector<double> *aPtrRates) {
	ptrRates = aPtrRates;
	updated();
}*/

void RateLikNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	boost::math::gamma_distribution<double> gammaDist(alpha, 1./beta);
	this->result = 0;
	for(size_t iR=0; iR<rates.size(); ++iR){
		this->result += log(boost::math::pdf(gammaDist, rates[iR]));
	}

	/*cp.endTime();
	std::cout << "[RateLikNode] Duration : " << cp.duration() << std::endl;*/
}

bool RateLikNode::processSignal(BaseNode* aChild) {
	return true;
}

void RateLikNode::doAddChild(const size_t rowId, BaseNode* aChild) {

}


} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
