//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HPBDNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "HPBDNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

HPBDNode::HPBDNode(const size_t aNExtant, const std::vector<double>& aLambda, const std::vector<double>& aMu) :
		DAG::ResultNode<double>(), nExtant(aNExtant), lambda(aLambda), mu(aMu) {
	//rate = 0.;
	vecTimeLNode = vecTimeMNode = NULL;
	//ptrTe = ptrTs = NULL;
}

HPBDNode::~HPBDNode() {
}

void HPBDNode::doProcessing() {

	const std::vector<double> & vTL = vecTimeLNode->getVecTime();
	const std::vector<double> & vTM = vecTimeMNode->getVecTime();

	double alpha = 0., beta = 0.;
	double endTF, startTF = vTL[0];
	size_t iVTL=0, iVTM=0;

	while((iVTL < (vTL.size()-1)) && (iVTM < (vTM.size()-1))) {
		// We define the diff of rate for the current (upcoming) timeframe
		//cout << "Lambda = " << lambda[iVTL] << " -- Mu = " << mu[iVTM] << endl;
		double tmpL = lambda[iVTL];
		double diff = lambda[iVTL] - mu[iVTM];
		// Then we determine which time is closer between tl and tm
		// and we update the end event of the next timeframe we consider
		if(vTL[iVTL+1] > vTM[iVTM+1]) { // Move iVTL
			++iVTL;
			endTF = vTL[iVTL];
		} else if (vTM[iVTM+1] > vTL[iVTL+1]) { // Move iVTM
			++iVTM;
			endTF = vTM[iVTM];
		} else { // Move both
			++iVTM;
			++iVTL;
			endTF = vTL[iVTL];
		}

		// We now have the timeframe length and we can compute everything
		double Dt = startTF - endTF;
		double expLDt = exp(diff*Dt);
		alpha += tmpL*expLDt;
		beta += expLDt;

		// The next timeframe start at the end of the current timeframe
		startTF = endTF;
	}

	double logAlpha = log(alpha);
	double logAlphaP1 = log(alpha+1.);
	this->result = (log(beta)-(logAlpha+logAlphaP1)) + (logAlpha-logAlphaP1)*nExtant;

	if(this->result != this->result){
		this->result = -numeric_limits<double>::infinity();
	}
	/*cp.endTime();
	std::cout << "[HPBDNode] Duration : " << cp.duration() << std::endl;*/
}

bool HPBDNode::processSignal(BaseNode* aChild) {
	return true;
}

void HPBDNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(TimeVectorNode)){
		TimeVectorNode *tmpPtr = dynamic_cast<TimeVectorNode*>(aChild);
		if(tmpPtr->getEventType() == TimeVectorNode::LAMBDA) {
			vecTimeLNode = tmpPtr;
		} else {
			vecTimeMNode = tmpPtr;
		}
	}
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
