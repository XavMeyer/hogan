//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeanHPPNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "MeanHPPNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

MeanHPPNode::MeanHPPNode(const size_t aNFossils, const double &aTs, const double &aTe) :
		DAG::ResultNode<double>(), nFossils(aNFossils), ts(aTs), te(aTe){
	//ts = te = 0.;
	aqNode = NULL;
}

MeanHPPNode::~MeanHPPNode() {
}

/*void MeanHPPNode::setTs(const double aTs) {
	ts = aTs;
	updated();
}

void MeanHPPNode::setTe(const double aTe) {
	te = aTe;
	updated();
}*/

void MeanHPPNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	const std::vector<double> &aQs = aqNode->getAlphaQuantile();

	const double tl = ts-te;
	std::vector<double> logL(aQs.size(), 0.0);
	double nF = (double)nFossils;

	for(uint iQ=0; iQ<aQs.size(); ++iQ){
		const double qtl = aQs[iQ]*tl;
		logL[iQ] = -qtl + nF*log(aQs[iQ]);
	}

	double meanLogL = logL[0];
	for(size_t iQ=1; iQ<aQs.size(); ++iQ){
		meanLogL = sumValLog(meanLogL, logL[iQ]);
	}
	meanLogL -= log(aQs.size());
	this->result = meanLogL;

	//usleep(200);

	/*cp.endTime();
	std::cout << "[MeanHPPNode] Duration : " << cp.duration() << std::endl;*/

}

bool MeanHPPNode::processSignal(BaseNode* aChild) {
	return true;
}

void MeanHPPNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(AlphaQuantileNode)){
		aqNode = dynamic_cast<AlphaQuantileNode*>(aChild);
	}
}

double MeanHPPNode::sumValLog(const double x, const double y) const {
	return (x>y ? x+log(1+exp(y-x)) : y+log(1+exp(x-y)));
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
