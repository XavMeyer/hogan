//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeansHPPNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef MEANSHPPNODE_H_
#define MEANSHPPNODE_H_

#include <unistd.h>

#include <DAG/Node/Base/BaseNode.h>
#include <DAG/Node/Base/ResultNode.h>
#include "AlphaQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class MeansHPPNode: public DAG::ResultNode<double> {
public:
	MeansHPPNode(const size_t aStart, const size_t aEnd, const size_t aNAlpha, const std::vector< std::vector<double> > &aFossils,
			const std::vector<double> &aTs, const std::vector<double> &aTe);
	~MeansHPPNode();

	/*void setTs(const double aTs);
	void setTe(const double aTe);*/

private:

	const size_t start, end, nAlpha;
	const std::vector< std::vector<double> > &fossils;
	const std::vector<double> &ts, &te;
	//const std::vector<double>& aQs;
	AlphaQuantileNode *aqNode;

	std::vector<double> logL;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

	double sumValLog(const double x, const double y) const;

};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MEANSHPPNODE_H_ */
