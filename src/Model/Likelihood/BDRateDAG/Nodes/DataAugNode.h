//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DataAugNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef DATAAUGNODE_H_
#define DATAAUGNODE_H_

#include <unistd.h>

#include <DAG/Node/Base/BaseNode.h>
#include <DAG/Node/Base/ResultNode.h>
#include "AlphaQuantileNode.h"
#include "AugQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class DataAugNode: public DAG::ResultNode<double> {
public:
	DataAugNode(const size_t aStart, const size_t aEnd, const std::vector< std::vector<double> > &aAugFossils,
			const std::vector<double> &aTs, const std::vector<double> &aTe, const double &aQ, const double &aExtRate);
	~DataAugNode();

	/*void setTs(const double aTs);
	void setTe(const double aTe);*/

private:

	static const unsigned int nQuantile;
	static const double minTimeframe;
	static const double quantile[];
	static const double dirichletAlpha;

	const size_t start, end;
	const std::vector< std::vector<double> > &fossils;
	const std::vector<double> &ts, &te;
	const double &q, &extRate;
	double a, am1, b, bm1, c, fBeta;
	//const std::vector<double>& aQs;
	AugQuantileNode *augNode;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

	double processMeanNHPPLikelihood(const size_t iS, const std::vector<double> &Qs);
	double processDataAugNHPP(const size_t iS);

	double sumValLog(const double x, const double y) const;

};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* DATAAUGNODE_H_ */
