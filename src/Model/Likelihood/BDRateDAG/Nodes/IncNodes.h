//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IncNodes.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef INCALPHANODES_H_
#define INCALPHANODES_H_

#include "AlphaQuantileNode.h"
#include "AugQuantileNode.h"
#include "BDPartialLikNode.h"
#include "BDRateLikNode.h"
#include "DataAugNode.h"
#include "HPBDNode.h"
#include "MeanHPPNode.h"
#include "MeansHPPNode.h"
#include "MeansNHPPNode.h"
#include "MergePartialAQNode.h"
#include "NHPPNode.h"
#include "PartialAlphaQuantileNode.h"
#include "PriorTXNode.h"
#include "RateLikNode.h"
#include "TimeVectorNode.h"

//namespace AlphaNode = ::DAG::BDRate::AlphaModel;

#endif /* INCALPHANODES_H_ */
