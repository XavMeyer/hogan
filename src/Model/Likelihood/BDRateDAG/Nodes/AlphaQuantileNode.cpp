//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaQuantileNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "AlphaQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

AlphaQuantileNode::AlphaQuantileNode(const size_t aNAlpha, const double &aQ, const double &aAlpha) :
		DAG::BaseNode(), nAlpha(aNAlpha), q(aQ), alpha(aAlpha) {
	//q = alpha = 0.;
}

AlphaQuantileNode::~AlphaQuantileNode() {
}

/*void AlphaQuantileNode::setQ(const double aQ) {
	q = aQ;
}


void AlphaQuantileNode::setAlpha(const double aAlpha) {
	alpha = aAlpha;
}*/

const std::vector<double>& AlphaQuantileNode::getAlphaQuantile() const {
	return alphaQuant;
}

void AlphaQuantileNode::initAlphaQuantile(const double alpha) {
	alphaQuant.clear();
	boost::math::gamma_distribution<double> gammaDist(alpha, 1./alpha);
	double incr = 1./(double)nAlpha;
	for(double pos=incr/2.; pos<1.0; pos+=incr){
		double quantile = boost::math::quantile( gammaDist, pos );
		alphaQuant.push_back(quantile);
	}
}

void AlphaQuantileNode::doProcessing() {

	//CustomProfiling cp;
	//cp.startTime();

	if(nAlpha == 1) {
		alphaQuant.clear();
		alphaQuant.push_back(q);
		return;
	}

	initAlphaQuantile(alpha);

	double sum = 0.;
	for(uint iAQ=0; iAQ<alphaQuant.size(); ++iAQ){
		sum += alphaQuant[iAQ]; // Get m_i
	}
	sum /= (double)alphaQuant.size();

	for(uint iAQ=0; iAQ<alphaQuant.size(); ++iAQ){
		alphaQuant[iAQ] /= sum; // normalize m_i => w_i
		alphaQuant[iAQ] *= q; // multiply q by w_i
	}

	//cp.endTime();
	//std::cout << "[AlphaQuantile] Duration : " << cp.duration() << std::endl;
}

bool AlphaQuantileNode::processSignal(BaseNode* aChild){
	return true;
}

void AlphaQuantileNode::doAddChild(const size_t rowId, BaseNode* aChild){
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
