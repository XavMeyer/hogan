//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RateLikNode.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef RATELIKNODE_H_
#define RATELIKNODE_H_

#include <DAG/Node/Base/ResultNode.h>
#include <DAG/Node/Base/BaseNode.h>

#include <boost/math_fwd.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/distributions.hpp>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class RateLikNode: public DAG::ResultNode<double> {
public:
	RateLikNode(const double &aAlpha, const double &aBeta, const std::vector<double> &aRates);
	~RateLikNode();

	/*void setAlpha(const double aAlpha);
	void setBeta(const double aBeta);
	void setRates(std::vector<double> *aPtrRates);*/

private:

	const double &alpha, &beta;
	const std::vector<double> &rates;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);
};


} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* RATELIKNODE_H_ */
