//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BDRateLikNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef BDRATELIKNODE_H_
#define BDRATELIKNODE_H_

#include <DAG/Node/Base/BaseNode.h>
#include <DAG/Node/Base/ResultNode.h>
#include "TimeVectorNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class BDRateLikNode: public DAG::ResultNode<double> {
public:
	enum event_t {LAMBDA=0, MU=1};

	BDRateLikNode(const event_t aEventType, const size_t aNSpecies,
			const std::vector<double>& aRates, const std::vector<double>& ts, const std::vector<double>& te);
	~BDRateLikNode();

	/*void setPtrTs(std::vector<double> *aPtrTs);
	void setPtrTe(std::vector<double> *aPtrTe);
	void setRate(const double aRate);*/

private:

	static const double minTimeframe;

	const event_t eventType;
	const size_t nSpecies;

	const std::vector<double> &rates, &ts, &te;

	TimeVectorNode *vecTimeNode;

	template<int>
	unsigned int countEvent(const size_t iT, const double start, const double end) const;
	double getTimeInsideTF(double ts, double te, const double start, const double end) const;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BDRATELIKNODE_H_ */
