//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaQuantileNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef ALPHAQUANTILENODE_H_
#define ALPHAQUANTILENODE_H_

#include <vector>

#include <boost/math_fwd.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/distributions.hpp>

#include <DAG/Node/Base/BaseNode.h>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class AlphaQuantileNode: public DAG::BaseNode {
public:
	AlphaQuantileNode(const size_t aNAlpha, const double &aQ, const double &aAlpha);
	~AlphaQuantileNode();

	/*void setQ(const double aQ);
	void setAlpha(const double aAlpha);*/

	const std::vector<double>& getAlphaQuantile() const;

private:

	const size_t nAlpha;
	const double &q, &alpha;
	std::vector<double> alphaQuant;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

	void initAlphaQuantile(const double alpha);
};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ALPHAQUANTILENODE_H_ */
