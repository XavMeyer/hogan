//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BDRateLikNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "BDRateLikNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

const double BDRateLikNode::minTimeframe = 1.0;

BDRateLikNode::BDRateLikNode(const event_t aEventType, const size_t aNSpecies,
		const std::vector<double>& aRates, const std::vector<double>& aTs, const std::vector<double>& aTe) :
		DAG::ResultNode<double>(), eventType(aEventType), nSpecies(aNSpecies),
		rates(aRates), ts(aTs), te(aTe){
	//rate = 0.;
	vecTimeNode = NULL;
	//ptrTe = ptrTs = NULL;
}

BDRateLikNode::~BDRateLikNode() {
}

/*void BDRateLikNode::setPtrTs(std::vector<double> *aPtrTs) {
	ptrTs = aPtrTs;
	updated();
}

void BDRateLikNode::setPtrTe(std::vector<double> *aPtrTe) {
	ptrTe = aPtrTe;
	updated();
}

void BDRateLikNode::setRate(const double aRate) {
	rate = aRate;
	updated();
}*/

template<>
unsigned int BDRateLikNode::countEvent<BDRateLikNode::LAMBDA>(const size_t iT,
												const double start, const double end) const {
	unsigned int cnt=0;
	for(size_t iS=0; iS<nSpecies; ++iS) {
		if(ts[iS] <= start && ts[iS] > end){
			++cnt;
		}
	}
	return cnt;
}

template<>
unsigned int BDRateLikNode::countEvent<BDRateLikNode::MU>(const size_t iT,
											const double start, const double end) const {
	unsigned int cnt=0;
	for(size_t iS=0; iS<nSpecies; ++iS) {
		if(te[iS] <= start && te[iS] > end){
			++cnt;
		}
	}
	return cnt;
}

double BDRateLikNode::getTimeInsideTF(double ts, double te, const double start, const double end) const {
	// Check if ts is at least bigger than the end of the timeframe
	if(ts > end){
		// If so check if it is bigger than the start of the TF
		if(ts >= start) {
			// Then we have to change it to start
			ts = start;
		}
	} else {
		// we are not in TF
		return 0.;
	}

	// Check if te is at least smaller than the start of the TF
	if(te < start){
		// If so check if it is small than the end of the TF
		if(te <= end) {
			// Then we have to change it to end
			te = end;
		}
	} else {
		// we are not in TF
		return 0.;
	}

	return ts-te;
}

void BDRateLikNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	//std::cout << "ok 1" << std::endl;

	const std::vector<double> & vTx = vecTimeNode->getVecTime();
	this->result = 0.;

	//std::cout << "ok 2" << std::endl;

	for(size_t iR=0; iR<rates.size(); ++iR){
		double start = vTx[iR];
		double end = vTx[iR+1];

		if(start-end < minTimeframe) {
			this->result = -numeric_limits<double>::infinity();
			return;
		} else {
		}

		unsigned int nEvent;
		if(eventType == LAMBDA){
			nEvent = countEvent<LAMBDA>(iR, start, end);
		} else {
			nEvent = countEvent<MU>(iR, start, end);
		}
		double totalBL = 0; // Total branch length
		for(size_t iS=0; iS<nSpecies; ++iS) {
			totalBL += getTimeInsideTF(ts[iS], te[iS], start, end);
		}

		this->result +=  nEvent*log(rates[iR]) + -rates[iR]*totalBL;
	}

	/*cp.endTime();
	std::cout << "[BDRateLikNode] Duration : " << cp.duration() << std::endl;*/
}

bool BDRateLikNode::processSignal(BaseNode* aChild) {
	return true;
}

void BDRateLikNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(TimeVectorNode)){
		vecTimeNode = dynamic_cast<TimeVectorNode*>(aChild);
	}
}


} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
