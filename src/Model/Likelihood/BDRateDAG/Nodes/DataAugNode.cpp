//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DataAugNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "DataAugNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

const unsigned int DataAugNode::nQuantile = 4;
const double DataAugNode::minTimeframe = 1.0;
const double DataAugNode::quantile[] = {0.125, 0.375, 0.625, 0.875};
const double DataAugNode::dirichletAlpha = 2.5;

DataAugNode::DataAugNode(const size_t aStart, const size_t aEnd, const std::vector< std::vector<double> > &aAugFossils,
						const std::vector<double> &aTs, const std::vector<double> &aTe, const double &aQ, const double &aExtRate) :
		DAG::ResultNode<double>(), start(aStart), end(aEnd), fossils(aAugFossils), ts(aTs), te(aTe), q(aQ), extRate(aExtRate) {
	//ts = te = 0.;
	augNode = NULL;

	c = 0.5;
	a = 5.-(c*4.);
	am1 = a-1.;
	b = 1. + (c*4.);
	bm1 = b-1.;
	fBeta = boost::math::beta(a, b);
}

DataAugNode::~DataAugNode() {
}

double DataAugNode::processDataAugNHPP(const size_t iS) {
	double likelihood = 0.;
	double locLiks[nQuantile];

	if(augNode->getSumGammaPDF() <= 0.0) return log(0);

	double globLik = 0.;
	for(unsigned int iX=0; iX<fossils[iS].size()-1; ++iX){
		globLik += log(pow((ts[iS]-fossils[iS][iX]),bm1));
	}
	globLik += static_cast<double>(fossils[iS].size()-1)*log(q);
	globLik -= log(static_cast<double>(nQuantile));

	// For each quantile
	for(unsigned int iQ=0; iQ<nQuantile; ++iQ){
		double tl = ts[iS]-augNode->getGM()[iQ];
		//assert(tl != 0);
		double xB = (ts[iS])/tl; // Is fossils[iS][last] always tStart-0 ?
		double intQ = boost::math::ibeta(a, b, xB) * tl * q;

		// Processing the equivalent of function logPERT4_density
		double F=0.;
		for(unsigned int iX=0; iX<fossils[iS].size()-1; ++iX){
			F += log(pow((fossils[iS][iX]-augNode->getGM()[iQ]), am1));
		}
		F -= static_cast<double>(fossils[iS].size()-1)*log(fBeta*pow(tl,4.));
		F += globLik;

		// Likelihood
		locLiks[iQ] = F - intQ  + log(augNode->getGammaPDF()[iQ]/augNode->getSumGammaPDF());
		locLiks[iQ] -= log(1.-exp(-intQ));
	}

	likelihood = locLiks[0];
	for(unsigned int iQ=1; iQ<nQuantile; ++iQ){
		likelihood = sumValLog(likelihood, locLiks[iQ]);
	}

	if(likelihood > 100000) return log(0);
	return likelihood;
}

void DataAugNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	this->result = 0.;

	for(size_t iF=start; iF<end; ++iF) {
		this->result += processDataAugNHPP(iF);
	}

	//usleep(200);

	/*cp.endTime();
	std::cout << "[DataAugNode start=" << start << "] Duration : " << cp.duration() << std::endl;*/

}

bool DataAugNode::processSignal(BaseNode* aChild) {
	return true;
}

void DataAugNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(AugQuantileNode)){
		augNode = dynamic_cast<AugQuantileNode*>(aChild);
	}
}

double DataAugNode::sumValLog(const double x, const double y) const {
	return (x>y ? x+log(1+exp(y-x)) : y+log(1+exp(x-y)));
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
