//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MergePartialAQNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "MergePartialAQNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

MergePartialAQNode::MergePartialAQNode(const size_t aNAlpha, const double &aQ, std::vector<double> &aQs) :
		DAG::BaseNode(), nAlpha(aNAlpha), q(aQ), Qs(aQs) {
	//q = alpha = 0.;
}

MergePartialAQNode::~MergePartialAQNode() {
}

/*void MergePartialAQNode::setQ(const double aQ) {
	q = aQ;
}


void MergePartialAQNode::setAlpha(const double aAlpha) {
	alpha = aAlpha;
}*/

const std::vector<double>& MergePartialAQNode::getAlphaQuantile() const {
	return Qs;
}


void MergePartialAQNode::doProcessing() {

	//CustomProfiling cp;
	//cp.startTime();

	double sum = 0.;
	for(uint iAQ=0; iAQ<nAlpha; ++iAQ){
		sum += Qs[iAQ]; // Get m_i
	}
	sum /= (double)nAlpha;

	for(uint iAQ=0; iAQ<nAlpha; ++iAQ){
		Qs[iAQ] /= sum; // normalize m_i => w_i
		Qs[iAQ] *= q; // multiply q by w_i
	}

	//cp.endTime();
	//std::cout << "[AlphaQuantile] Duration : " << cp.duration() << std::endl;
}

bool MergePartialAQNode::processSignal(BaseNode* aChild){
	return true;
}

void MergePartialAQNode::doAddChild(const size_t rowId, BaseNode* aChild){
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
