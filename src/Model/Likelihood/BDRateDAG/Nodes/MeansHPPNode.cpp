//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeansHPPNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "MeansHPPNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

MeansHPPNode::MeansHPPNode(const size_t aStart, const size_t aEnd, const size_t aNAlpha, const std::vector< std::vector<double> > &aFossils,
		const std::vector<double> &aTs, const std::vector<double> &aTe) :
		DAG::ResultNode<double>(), start(aStart), end(aEnd), nAlpha(aNAlpha), fossils(aFossils), ts(aTs), te(aTe), logL(nAlpha, 0.) {
	//ts = te = 0.;
	aqNode = NULL;
}

MeansHPPNode::~MeansHPPNode() {
}

/*void MeansHPPNode::setTs(const double aTs) {
	ts = aTs;
	updated();
}

void MeansHPPNode::setTe(const double aTe) {
	te = aTe;
	updated();
}*/

void MeansHPPNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	const std::vector<double> &aQs = aqNode->getAlphaQuantile();

	this->result = 0;
	for(size_t iF=start; iF<end; ++iF) {
		const double tl = ts[iF]-te[iF];
		double nF = fossils[iF].size();
		nF = (double)(fossils[iF].back() == 0 ? nF-1 : nF);

		for(uint iQ=0; iQ<nAlpha; ++iQ){
			const double qtl = aQs[iQ]*tl;
			logL[iQ] = -qtl + nF*log(aQs[iQ]);
		}

		double meanLogL = logL[0];
		for(size_t iQ=1; iQ<nAlpha; ++iQ){
			meanLogL = sumValLog(meanLogL, logL[iQ]);
		}
		meanLogL -= log(nAlpha);
		this->result += meanLogL;
	}

	/*cp.endTime();
	std::cout << "[MeansHPPNode] Duration : " << cp.duration() << std::endl;*/

}

bool MeansHPPNode::processSignal(BaseNode* aChild) {
	return true;
}

void MeansHPPNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(AlphaQuantileNode)){
		aqNode = dynamic_cast<AlphaQuantileNode*>(aChild);
	}
}

double MeansHPPNode::sumValLog(const double x, const double y) const {
	return (x>y ? x+log(1+exp(y-x)) : y+log(1+exp(x-y)));
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
