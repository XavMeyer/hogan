//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeanHPPNode.h
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef MEANHPPNODE_H_
#define MEANHPPNODE_H_

#include <unistd.h>

#include <DAG/Node/Base/BaseNode.h>
#include <DAG/Node/Base/ResultNode.h>
#include "AlphaQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

class MeanHPPNode: public DAG::ResultNode<double> {
public:
	MeanHPPNode(const size_t aNFossils, const double &aTs, const double &aTe);
	~MeanHPPNode();

	/*void setTs(const double aTs);
	void setTe(const double aTe);*/

private:

	size_t nFossils;
	const double &ts, &te;
	AlphaQuantileNode *aqNode;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

	double sumValLog(const double x, const double y) const;

};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MEANHPPNODE_H_ */
