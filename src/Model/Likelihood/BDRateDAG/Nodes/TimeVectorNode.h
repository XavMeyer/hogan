//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TimeVectorNode.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef TIMEVECTORNODE_H_
#define TIMEVECTORNODE_H_

#include "DAG/Node/Base/BaseNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {


class TimeVectorNode : public DAG::BaseNode {
public:
	enum event_t {LAMBDA=0, MU=1};
public:
	TimeVectorNode(const event_t aEventType, const std::vector<double> &aTs, const std::vector<double> &aTx);
	~TimeVectorNode();

	/*void setPtrTs(std::vector<double> *aPtrTs);
	void setPtrRate(std::vector<double> *aPtrRate);*/

	event_t getEventType() const;
	const std::vector<double>& getVecTime() const;

private:

	const event_t eventType;
	const std::vector<double> &ts, &tx;
	std::vector<double> vecTime;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);
};

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TIMEVECTORNODE_H_ */
