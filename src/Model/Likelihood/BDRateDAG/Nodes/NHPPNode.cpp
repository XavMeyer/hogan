//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NHPPNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "NHPPNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

NHPPNode::NHPPNode(const size_t aStart, const size_t aEnd, const size_t aNAlpha, const std::vector< std::vector<double> > &aFossils,
		const std::vector<double> &aTs, const std::vector<double> &aTe, const double &aQ, const double &aExtRate) :
		DAG::ResultNode<double>(), start(aStart), end(aEnd), nAlpha(aNAlpha), fossils(aFossils), ts(aTs), te(aTe), q(aQ), extRate(aExtRate), logL(nAlpha, 0.0) {
	//ts = te = 0.;
	aqNode = NULL;

	c = 0.5;
	a = 5.-(c*4.);
	am1 = a-1.;
	b = 1. + (c*4.);
	bm1 = b-1.;
	fBeta = boost::math::beta(a, b);
}

NHPPNode::~NHPPNode() {
}

/*void NHPPNode::setTs(const double aTs) {
	ts = aTs;
	updated();
}

void NHPPNode::setTe(const double aTe) {
	te = aTe;
	updated();
}*/


double NHPPNode::processMeanNHPPLikelihood(const size_t iS, const std::vector<double> &Qs) {

	const double tl = ts[iS]-te[iS];
	//vector<double> qtls(Qs);
	//vector<double> logL(Qs.size(), 0.0);

	double globalLik=0.;
	// F :
	// F += sum_i [log((ts-x_i)^(b-1)*(x_i-te)^(a-1))]
	for(unsigned int iX=0; iX<fossils[iS].size(); ++iX){
		globalLik += log(pow((ts[iS]-fossils[iS][iX]),bm1)*pow((fossils[iS][iX]-te[iS]), am1));
	}
	// F -= nFossils * (log(tl^4) * fBeta(a,b)))
	globalLik -= static_cast<double>(fossils[iS].size())*log(fBeta*pow(tl,4.));

	for(uint iQ=0; iQ<nAlpha; ++iQ){
		const double qtl = Qs[iQ]*tl;
		logL[iQ] = 1.-exp(-qtl);
		if(logL[iQ] == 0){
			return log(0);
		}
		logL[iQ] = -qtl - log(logL[iQ]);
		logL[iQ] += globalLik;
		logL[iQ] += static_cast<double>(fossils[iS].size())*log(Qs[iQ]);
	}

	double meanLogL = logL[0];
	for(size_t iQ=1; iQ<nAlpha; ++iQ){
		meanLogL = sumValLog(meanLogL, logL[iQ]);
	}
	meanLogL -= log(nAlpha);

	return meanLogL;
}

void NHPPNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	const std::vector<double> &Qs = aqNode->getAlphaQuantile();

	this->result = 0.;

	for(size_t iF=start; iF<end; ++iF) {
		this->result += processMeanNHPPLikelihood(iF, Qs);
	}

	//usleep(200);

	/*cp.endTime();
	std::cout << "[NHPPNode start=" << start << "] Duration : " << cp.duration() << std::endl;*/

}

bool NHPPNode::processSignal(BaseNode* aChild) {
	return true;
}

void NHPPNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MergePartialAQNode)){
		aqNode = dynamic_cast<MergePartialAQNode*>(aChild);
	}
}

double NHPPNode::sumValLog(const double x, const double y) const {
	return (x>y ? x+log(1+exp(y-x)) : y+log(1+exp(x-y)));
}


} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
