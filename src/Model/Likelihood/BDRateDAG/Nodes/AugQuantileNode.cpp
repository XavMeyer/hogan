//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AugQuantileNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "AugQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

const unsigned int AugQuantileNode::nQuantile = 4;
const double AugQuantileNode::quantile[] = {0.125, 0.375, 0.625, 0.875};

AugQuantileNode::AugQuantileNode(const double &aExtRate) :
		DAG::BaseNode(), extRate(aExtRate), GM(nQuantile, 0.), gammaPDF(nQuantile, 0.) {
	sumGammaPDF = 0.;
}

AugQuantileNode::~AugQuantileNode() {
}

/*void AugQuantileNode::setQ(const double aQ) {
	q = aQ;
}


void AugQuantileNode::setAlpha(const double aAlpha) {
	alpha = aAlpha;
}*/

const std::vector<double>& AugQuantileNode::getGammaPDF() const {
	return gammaPDF;
}

const std::vector<double>& AugQuantileNode::getGM() const {
	return GM;
}

const double& AugQuantileNode::getSumGammaPDF() const {
	return sumGammaPDF;
}


void AugQuantileNode::doProcessing() {

	/*CustomProfiling cp;
	cp.startTime();*/

	boost::math::gamma_distribution<double> gammaDist(1., extRate);

	sumGammaPDF = 0.;
	for(unsigned int iQ=0; iQ<nQuantile; ++iQ){
		GM[iQ] = log(1.-quantile[iQ])/extRate;
		gammaPDF[iQ] = boost::math::pdf(gammaDist, -GM[iQ]);
		sumGammaPDF += gammaPDF[iQ];
	}

	/*cp.endTime();
	std::cout << "[AlphaQuantile] Duration : " << cp.duration() << std::endl;*/
}

bool AugQuantileNode::processSignal(BaseNode* aChild){
	return true;
}

void AugQuantileNode::doAddChild(const size_t rowId, BaseNode* aChild){
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
