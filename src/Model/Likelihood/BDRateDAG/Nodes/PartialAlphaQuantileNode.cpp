//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PartialAlphaQuantileNode.cpp
 *
 * @date Mar 13, 2015
 * @author meyerx
 * @brief
 */
#include "PartialAlphaQuantileNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

PartialAlphaQuantileNode::PartialAlphaQuantileNode(const size_t aNAlpha, const double &aQ, const double &aAlpha,
												   const size_t aStart, const size_t aEnd, std::vector<double> &aQs) :
		DAG::BaseNode(), nAlpha(aNAlpha), start(aStart), end(aEnd), q(aQ), alpha(aAlpha), Qs(aQs) {
	//q = alpha = 0.;
}

PartialAlphaQuantileNode::~PartialAlphaQuantileNode() {
}

void PartialAlphaQuantileNode::doProcessing() {

	//CustomProfiling cp;
	//cp.startTime();

	boost::math::gamma_distribution<double> gammaDist(alpha, 1./alpha);
	double incr = 1./(double)nAlpha;
	double offset = incr/2.;
	for(double i=start; i<end; ++i) {
	//for(double pos=start*incr/2.; pos<end*incr/2.; pos+=incr){
	//for(double pos=incr/2.; pos<1.0; pos+=incr){
		double pos=offset + i*incr;
		double quantile = boost::math::quantile( gammaDist, pos );
		Qs[i] = quantile;
	}

	//cp.endTime();
	//std::cout << "[AlphaQuantile] Duration : " << cp.duration() << std::endl;
}

bool PartialAlphaQuantileNode::processSignal(BaseNode* aChild){
	return true;
}

void PartialAlphaQuantileNode::doAddChild(const size_t rowId, BaseNode* aChild){
}


} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
