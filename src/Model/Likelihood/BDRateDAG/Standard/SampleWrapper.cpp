//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "SampleWrapper.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Standard {

SampleWrapper::SampleWrapper(const size_t aNSpecies, const size_t aNLambda,
		  const size_t aNMu) : nSpecies(aNSpecies), nLambda(aNLambda), nMu(aNMu),
		  ts(nSpecies), te(nSpecies), tL(nLambda-1), tM(nMu-1), lambda(nLambda), mu(nMu) {

	q = 0.;

}

SampleWrapper::~SampleWrapper() {

}

void SampleWrapper::setSample(const std::vector<double> &sample) {
	size_t idx = 0;

	q = sample[idx];
	idx++;

	ts.assign(&sample[idx], &sample[idx]+nSpecies);
	idx += nSpecies;

	te.assign(&sample[idx], &sample[idx]+nSpecies);
	idx += nSpecies;

	tL.assign(&sample[idx], &sample[idx]+(nLambda-1));
	idx += nLambda-1;

	tM.assign(&sample[idx], &sample[idx]+(nMu-1));
	idx += nMu-1;

	lambda.assign(&sample[idx], &sample[idx]+nLambda);
	idx += nLambda;

	mu.assign(&sample[idx], &sample[idx]+nMu);
	idx += nMu;
}

const double& SampleWrapper::getQ() const {
	return q;
}

const std::vector<double>& SampleWrapper::getTs() const {
	return ts;
}

const std::vector<double>& SampleWrapper::getTe() const {
	return te;
}

const std::vector<double>& SampleWrapper::getTL() const {
	return tL;
}

const std::vector<double>& SampleWrapper::getTM() const {
	return tM;
}

const std::vector<double>& SampleWrapper::getLambda() const {
	return lambda;
}

const std::vector<double>&SampleWrapper:: getMu() const {
	return mu;
}

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
