//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaLik.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef BDRATESTDLIK_H_
#define BDRATESTDLIK_H_

#include "../FileReader.h"
#include "SampleWrapper.h"

#include "DAG/Node/Base/IncNodes.h"
#include "../Nodes/IncNodes.h"

#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"

#include "Model/Likelihood/LikelihoodInterface.h"

#include "../BDRateDefBlockHelper.hpp"

#include <string>
#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Standard {

class StdLik : public LikelihoodInterface {
public:
	StdLik(const size_t aNThread, const std::string aFileName, const bool aUseHPP=true);
	~StdLik();

	double processLikelihood(const Sampler::Sample &sample);
	string getName(char sep) const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	const std::vector< std::vector<double> >& getFossils() const;
	const size_t getNLambda() const;
	const size_t getNMu() const;

	std::string getFName() const;
	size_t getNThread() const;


private :
	typedef DAG::ReduceResultNode<double, DAG::Operator::Sum<double> > ReduceNode;

	static const double ALPHA;

	const bool useHPP;
	bool first;
	const size_t nThread, nAlpha;
	const std::string fName;
	size_t nExtant, nLambda, nMu;
	std::vector<std::vector<double> > fossils;

	SampleWrapper *sw;
	//std::vector<double> aQs;

	/* DAG Scheduler */
	DAG::Scheduler::BaseScheduler *scheduler;

	/* DAG Root and reduce */
	ReduceNode *rootDAG, *likBDP;
	DAG::ResultNode<double> *likHPP;

	/* DAG Nodes */
	AlphaQuantileNode* aQNode;
	AugQuantileNode* augNode;
	std::vector<MeansHPPNode*> meansHPPNodes;
	std::vector<MeansNHPPNode*> meansNHPPNodes;

	TimeVectorNode *muTimesNode, *lambdaTimesNode;
	std::vector<BDRateLikNode*> muBDPartialNodes, lambdaBDPartialNodes;
	PriorTXNode *priorTX;
	HPBDNode *likHPBD;

	void createDAG();
	void setSample(const std::vector<double> &sample);
	void updateSample(const std::vector<size_t>& pIndices, const std::vector<double> &sample);
	void setMarkers();

};

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BDRATESTDLIK_H_ */
