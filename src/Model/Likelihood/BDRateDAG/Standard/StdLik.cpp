//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AlphaLik.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "StdLik.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {
namespace Standard {

const double StdLik::ALPHA = 1.;

StdLik::StdLik(const size_t aNThread, const std::string aFileName, const bool aUseHPP) :
	useHPP(aUseHPP), nThread(aNThread), nAlpha(1), fName(aFileName) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	// Read the file
	FileReader fr(aFileName);
	nExtant = fr.getNExtant();
	nLambda = fr.getNLambda();
	nMu = fr.getNMu();
	fossils = fr.getFossils();

    sw = new SampleWrapper(fossils.size(), nLambda, nMu);

	createDAG();

	if(nThread > 1) {
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	markerNames.push_back("LH_NHPP");
	markerNames.push_back("LH_BDPart");
	markerNames.push_back("HP_BD");
	markerNames.push_back("Root age");
	markers.resize(markerNames.size());

}

StdLik::~StdLik() {
	delete sw;
}

double StdLik::processLikelihood(const Sampler::Sample &sample) {
	// Update the values
	setSample(sample.getDblValues());

	//initAlphaQuantile();

	// Reset the DAG
	scheduler->resetDAG();
	//std::cout << rootDAG->subtreeToSting() << std::endl;

	// Process the tree
	scheduler->process();

	/*std::cout << "HMU : " << muRateNode->getResult() << std::endl;;
	std::cout << "HLAMBDA : " << lambdaRateNode->getResult() << std::endl;;
	std::cout << "HPP : " << likHPP->getResult() << std::endl;;
	std::cout << "BDP : " << likBDP->getResult() << std::endl;;*/

	setMarkers();

	// return the likelihood
	return rootDAG->getResult();
}

std::string StdLik::getName(char sep) const {
	return std::string("BDRateAlphaV1DAG");
}

size_t StdLik::stateSize() const {
	return 3*sizeof(double);
}
void StdLik::setStateLH(const char* aState) {
	const double *tmp = reinterpret_cast<const double *>(aState);
	likHPP->setResult(tmp[0]);
	likBDP->setResult(tmp[1]);
	likHPBD->setResult(tmp[2]);
}

void StdLik::getStateLH(char* aState) const {
	double *tmp = reinterpret_cast<double *>(aState);
	tmp[0] = likHPP->getResult();
	tmp[1] = likBDP->getResult();
	tmp[2] = likHPBD->getResult();
}

double StdLik::update(const std::vector<size_t>& pIndices, const Sample& sample) {

	// Update the tree
	updateSample(pIndices, sample.getDblValues());
	//std::cout << rootDAG->subtreeToSting() << std::endl;

	// Partial reset
	scheduler->resetPartial();
	//std::cout << rootDAG->subtreeToSting() << std::endl;

	// Process the tree
	scheduler->process();

	setMarkers();

	return rootDAG->getResult();
}

std::string StdLik::getFName() const {
	return fName;
}

size_t StdLik::getNThread() const {
	return nThread;
}


const std::vector< std::vector<double> >& StdLik::getFossils() const {
	return fossils;
}

const size_t StdLik::getNLambda() const {
	return nLambda;
}

const size_t StdLik::getNMu() const {
	return nMu;
}

void StdLik::createDAG() {

	rootDAG = new ReduceNode();
	likBDP = new ReduceNode();

	aQNode = new AlphaQuantileNode(nAlpha, sw->getQ(), ALPHA);
	if(useHPP) {
		augNode = NULL;
	} else {
		augNode = new AugQuantileNode(sw->getMu().back());
	}

	// NHPP
	if(nThread == 1) {
		if(useHPP) {
			MeansHPPNode *ptr;
			ptr = new MeansHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe());
			ptr->addChild(aQNode);
			meansHPPNodes.push_back(ptr);
			likHPP = ptr;
		} else {
			MeansNHPPNode *ptr;
			ptr = new MeansNHPPNode(0, fossils.size(), nAlpha, fossils, sw->getTs(), sw->getTe(), sw->getQ(), sw->getMu().back());
			ptr->addChild(aQNode);
			ptr->addChild(augNode);
			meansNHPPNodes.push_back(ptr);
			likHPP = ptr;
		}
	} else {
		ReduceNode *ptrHPP  = new ReduceNode();
		size_t nChunk = std::min(4*nThread, fossils.size()/20);
		size_t sizeChunk = ceil(static_cast<double>(fossils.size()) / static_cast<double>(nChunk));
		for(size_t iC=0; iC<nChunk; ++iC) {
			size_t start = iC*sizeChunk;
			size_t end = std::min((iC+1)*sizeChunk, fossils.size());
			//std::cout << "Beg : " << start << " -- End : " << end << std::endl;

			if(useHPP) {
				MeansHPPNode *ptr;
				ptr = new MeansHPPNode(start, end, nAlpha, fossils, sw->getTs(), sw->getTe());
				ptr->addChild(aQNode);
				meansHPPNodes.push_back(ptr);
				ptrHPP->addChild(ptr);
			} else {
				MeansNHPPNode *ptr;
				ptr = new MeansNHPPNode(start, end, nAlpha, fossils, sw->getTs(), sw->getTe(), sw->getQ(), sw->getMu().back());
				ptr->addChild(aQNode);
				ptr->addChild(augNode);
				meansNHPPNodes.push_back(ptr);
				ptrHPP->addChild(ptr);
			}
		}
		likHPP = ptrHPP;
	}
	rootDAG->addChild(likHPP);

	// BD
	muTimesNode = new TimeVectorNode(TimeVectorNode::MU, sw->getTs(), sw->getTM());
	lambdaTimesNode = new TimeVectorNode(TimeVectorNode::LAMBDA, sw->getTs(), sw->getTL());

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::MU, fossils.size(), sw->getMu(), sw->getTs(), sw->getTe());
		ptr->addChild(muTimesNode);
		muBDPartialNodes.push_back(ptr);
		likBDP->addChild(ptr);
	}

	{
		BDRateLikNode *ptr;
		ptr = new BDRateLikNode(BDRateLikNode::LAMBDA, fossils.size(), sw->getLambda(), sw->getTs(), sw->getTe());
		ptr->addChild(lambdaTimesNode);
		lambdaBDPartialNodes.push_back(ptr);
		likBDP->addChild(ptr);
	}
	priorTX = new PriorTXNode(2.5, getNLambda());
	priorTX->addChild(lambdaTimesNode);
	priorTX->addChild(muTimesNode);
	likBDP->addChild(priorTX);

	rootDAG->addChild(likBDP);

	//HPBD
	likHPBD = new HPBDNode(nExtant, sw->getLambda(), sw->getMu());
	likHPBD->addChild(lambdaTimesNode);
	likHPBD->addChild(muTimesNode);
	rootDAG->addChild(likHPBD);

}

void StdLik::setSample(const std::vector<double> &sample) {

	sw->setSample(sample);

	// Alpha quantiles
	aQNode->updated();

	// meanHPP
	if(useHPP) {
		for(size_t iF=0; iF<meansHPPNodes.size(); ++iF) {
			meansHPPNodes[iF]->updated();
		}
	} else {
		augNode->updated();
		for(size_t iF=0; iF<meansNHPPNodes.size(); ++iF) {
			meansNHPPNodes[iF]->updated();
		}
	}

	// BD partial MU
	muTimesNode->updated();
	for(size_t iM=0; iM<muBDPartialNodes.size(); ++iM) {
		muBDPartialNodes[iM]->updated();
	}

	// BD partial Lambda
	lambdaTimesNode->updated();
	for(size_t iL=0; iL<lambdaBDPartialNodes.size(); ++iL) {
		lambdaBDPartialNodes[iL]->updated();
	}

	// HPDD
	likHPBD->updated();
}

void StdLik::updateSample(const std::vector<size_t>& pIndices, const std::vector<double> &sample) {

	sw->setSample(sample);

	if(pIndices.front() <= (2*fossils.size()) || first) { // qRate + Ts + Te not touched
		// Alpha quantiles
		aQNode->updated();

		// meanHPP
		if(useHPP) {
			for(size_t iF=0; iF<meansHPPNodes.size(); ++iF) {
				meansHPPNodes[iF]->updated();
			}
		} else {
			augNode->updated();
			for(size_t iF=0; iF<meansNHPPNodes.size(); ++iF) {
				meansNHPPNodes[iF]->updated();
			}
		}

		first = false;
	} else if(pIndices.front() >= (2*fossils.size()+(nLambda-1)+(nMu-1))) { // nLambda + nMu-1 not touched
		if(!useHPP) {
			augNode->updated();
			for(size_t iF=0; iF<meansNHPPNodes.size(); ++iF) {
				meansNHPPNodes[iF]->updated();
			}
		}
	}

	// BD partial MU
	muTimesNode->updated();
	for(size_t iM=0; iM<muBDPartialNodes.size(); ++iM) {
		muBDPartialNodes[iM]->updated();
	}

	// BD partial Lambda
	lambdaTimesNode->updated();
	for(size_t iL=0; iL<lambdaBDPartialNodes.size(); ++iL) {
		lambdaBDPartialNodes[iL]->updated();
	}

	priorTX->updated();

	// HPDD
	likHPBD->updated();

}

void StdLik::setMarkers() {
	markers[0] = likHPP->getResult();
	markers[1] = likBDP->getResult();
	markers[2] = likHPBD->getResult();
	markers[3] = *max_element(sw->getTs().begin(), sw->getTs().end());
}

} /* namespace Alpha */
} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
