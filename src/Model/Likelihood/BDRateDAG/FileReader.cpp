//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FileReader.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include "FileReader.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BDRate {

FileReader::FileReader(const std::string &fileName) {
	read(fileName);
}

FileReader::~FileReader() {
}

size_t FileReader::getNLambda() const {
	return nLambda;
}

size_t FileReader::getNMu() const {
	return nMu;
}

size_t FileReader::getNExtant() const {
	return nExtant;
}

std::vector<std::vector<double> > FileReader::getFossils() const {
	return fossils;
}

void FileReader::read(const std::string &fileName) {
	std::ifstream iFile(fileName.c_str());
	size_t nLine;

	if(iFile.fail()) {
		std::cerr << "void FileReader::read(const std::string &fileName);" << std::endl;
		std::cerr << "Error : file '" << fileName << "' not found." << std::endl;
	}

	iFile >> nLambda;
	iFile >> nMu;
	iFile >> nExtant;
	iFile >> nLine;


	for(size_t iL=0; iL<nLine; ++iL){
		unsigned int nData;
		iFile >> nData;

		std::vector<double> data(nData, 0.);
		for(size_t iD=0; iD<nData; ++iD){
			iFile >> data[iD];
		}

		std::sort(data.begin(), data.end(), std::greater<double>());
		fossils.push_back(data);
	}
}

} /* namespace BDRate */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
