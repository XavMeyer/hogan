//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LikelihoodProcessor.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "LikelihoodInterface.h"

namespace StatisticalModel {
namespace Likelihood {

LikelihoodInterface::LikelihoodInterface() : isLogLH(false), isUpdatableLH(false){
}

LikelihoodInterface::~LikelihoodInterface(){
}

size_t LikelihoodInterface::stateSize() const {
	cerr << "LikelihoodInterface::stateSize() not defined." << endl;
	abort();
	return 0;
}

void LikelihoodInterface::setStateLH(const char* aState) {
	cerr << "LikelihoodInterface::setStateLH(void* aState) not defined." << endl;
	abort();
}

void LikelihoodInterface::getStateLH(char* aState) const {
	cerr << "LikelihoodInterface::getStateLH(void* aState) not defined." << endl;
	abort();
}

double LikelihoodInterface::update(const vector<size_t>& pIndices, const Sampler::Sample& sample) {
	cerr << "double LikelihoodInterface::update(const vector<int>& pIndices, const Sampler::Sample& sample) not defined." << endl;
	abort();
	return processLikelihood(sample);
}

void LikelihoodInterface::initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, std::streampos offset) {
}

std::streampos LikelihoodInterface::getCustomizedLogSize() const {
	return 0;
}

void LikelihoodInterface::writeCustomizedLog(const std::vector<Sample> &samples){

}

DAG::Scheduler::BaseScheduler* LikelihoodInterface::getScheduler() const {
	return NULL;
}

std::vector<DAG::BaseNode*> LikelihoodInterface::defineDAGNodeToCompute(const Sample& sample) {
	cerr << "std::vector<DAG::BaseNode*> LikelihoodInterface::defineDAGNodeToCompute(const Sample& sample) not defined." << endl;
	abort();
}

const vector<string>& LikelihoodInterface::getMarkerNames() const {
	return markerNames;
}

const vector<double>& LikelihoodInterface::getMarkers() const {
	return markers;
}

bool LikelihoodInterface::isLog() const {
	return isLogLH;
}

bool LikelihoodInterface::isUpdatable() const {
	return isUpdatableLH;
}

double LikelihoodInterface::getAlphaFromL(double l, double var) {
	// Efficiency in function of alphas
	boost::math::normal norm;
	return 2.*boost::math::cdf(norm, -l/(2*sqrt(var)));
}


double LikelihoodInterface::getOptiL(uint nP, uint nPPB, double var) {
	uint nL = 10000;
	double endL = 100.0*var;
	double L[nL], E[nL], D[nL];

	for(uint i=0; i<nL; ++i){
		L[i] = (1+i)*endL/(double)nL;
	}

	// Tweaking in function of nPPB
	double exponent = 2.;//(2.-0.96/exp((double)nPPB/100.));

	// Efficiency in function of alphas
	boost::math::normal norm;
	for(uint i=0; i<nL; ++i){
		E[i] = 2.*pow(L[i], exponent)*boost::math::cdf(norm, -L[i]/(2*sqrt(var)));

		// Gains in function of processors
		D[i] = 1.;
		double alpha = getAlphaFromL(L[i], var);
		for(uint iP=2; iP<=nP; ++iP){
			D[i] += pow((1.-alpha), iP-1.);
		}
	}

	// Get overall efficiency and get max value
	int iMax = -1;
	double max = -1.;

	// Get max alpha
	for(uint i=0; i<nL; ++i){
		E[i] *= D[i];
		if(E[i] > max){
			iMax = i;
			max = E[i];
		}
	}

	return L[iMax];
}

} // namespace Likelihood
} // namespace StatisticalModel

