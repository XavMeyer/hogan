//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NormalLH.cpp
 *
 * @date 10 oct. 2013
 * @author meyerx
 * @brief
 */

#include "NormalLH.h"

namespace StatisticalModel {
namespace Likelihood {

NormalLH::NormalLH(const string filename) : LikelihoodInterface() {
	isLogLH = true;
	isUpdatableLH = false;
	initData(filename);
}

NormalLH::~NormalLH() {
}

double NormalLH::processLikelihood(const Sampler::Sample &sample) {
	return processLikelihood(sample.getDoubleParameter(0), sample.getDoubleParameter(1));
}

double NormalLH::processLikelihood(const double mu, const double sigma) {
	if(sigma < 0.0) return 0.;
    boost::math::normal_distribution<> dist(mu, sigma);
	double likelihood=0.;
	for(size_t i=0; i<data.size(); ++i){
		likelihood += log(boost::math::pdf(dist, data[i]));
	}
	return likelihood;
}

double NormalLH::processLikelihood(const int idx, const double mu, const double sigma) {
	if(sigma < 0.0) return 0.;
	boost::math::normal_distribution<> dist(mu, sigma);
	double lh = log(pdf(dist, data[idx]));
	return lh;
}

void NormalLH::initData(const string filename) {

	ifstream inFile;

	// Get NB line
	int nData;
	if(Parallel::mpiMgr().isMainProcessor()){
		inFile.open(filename.c_str());
		inFile >> nData;
	}
	Parallel::mpiMgr().broadcastValues(1, &nData);

	// Get Data
	double tmpData[nData];
	if(Parallel::mpiMgr().isMainProcessor()){
		for(int i=0; i<nData; ++i){
			inFile >> tmpData[i];
		}
	}
	Parallel::mpiMgr().broadcastValues(nData, tmpData);

	for(int i=0; i<nData; ++i){
		data.push_back(tmpData[i]);
	}

}

string NormalLH::getName(char sep) const {
	stringstream ss;
	ss << "Log" << sep << "Normal";
	return ss.str();
}

} // namespace Likelihood
} // namespace StatisticalModel

