//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogNormalNDLH.cpp
 *
 * @date 14 oct. 2013
 * @author meyerx
 * @brief
 */

#include "LogNormalNDLH.h"
#include "LogNormalLH.h"

namespace StatisticalModel {
namespace Likelihood {

const double LogNormalNDLH::MU_ARR[] = {0.0, 1.0, -1.0, 2.0, -2.0, 3.0, -3.0, 4.0, -4.0, 5.0, -5.0,
		 	 	 	 	 	 	 	   6.0, -6.0, 7.0, -7.0, 5.0, -5.0, 2.0, -2.0, 1.0, -1.0};
const double LogNormalNDLH::SIGMA_ARR[] = {0.5, 1., 1., 2.0, 2.0, 3., 3., 4., 4., 5., 5.,
										   2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0};


LogNormalNDLH::LogNormalNDLH(const unsigned int aNDim, const size_t aNData, const double *mu, const double *sigma) :
		LikelihoodInterface(), nDim(aNDim), nData(aNData)
{
	isLogLH = true;
	isUpdatableLH = false;

	logNormPtr = new logNormPtr_t[nDim];

	for(unsigned int i=0; i<nDim; ++i){
		logNormPtr[i] = new LogNormalLH(nData, mu[i], sigma[i]);
	}
}

LogNormalNDLH::~LogNormalNDLH() {

	for(unsigned int i=0; i<nDim; ++i){
		delete logNormPtr[i];
	}

	delete [] logNormPtr;
}

double LogNormalNDLH::processLikelihood(const Sampler::Sample &sample) {
	double lh = 0.0;
	for(size_t i=0; i<nData; ++i) {
		for(unsigned int j=0; j<nDim; ++j){
			lh += log(logNormPtr[j]->processLikelihood(i, sample.getDoubleParameter(2*j), sample.getDoubleParameter((2*j)+1)));
		}
	}

	return lh;
}

string LogNormalNDLH::getName(char sep) const {
	stringstream ss;
	ss << "Log" << sep << "Normal" << sep << nDim << "D";
	return ss.str();
}

size_t LogNormalNDLH::getNDim() {
	return nDim;
}

size_t LogNormalNDLH::getNData() {
	return nData;
}

LogNormalLH* LogNormalNDLH::getLogNormalLH(size_t idx) {
	return logNormPtr[idx];
}

double LogNormalNDLH::getAveragVar(size_t startP, size_t endP) const {
	double val = 0.;

	for(uint i=startP; i<endP; ++i){
		val += logNormPtr[i]->getSigma();
	}
	return val/(double)(endP-startP);
}

} // namespace Likelihood
} // namespace StatisticalModel

