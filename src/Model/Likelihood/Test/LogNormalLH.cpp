//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogNormalLH.cpp
 *
 * @date 10 oct. 2013
 * @author meyerx
 * @brief
 */

#include "LogNormalLH.h"

namespace StatisticalModel {
namespace Likelihood {

LogNormalLH::LogNormalLH(const size_t nData, const double aMu, const double aSigma) :
		LikelihoodInterface(), mu(aMu), sigma(aSigma) {
	isLogLH = false;
	isUpdatableLH = false;
	initData(nData, mu, sigma);
}

LogNormalLH::~LogNormalLH() {
}

double LogNormalLH::processLikelihood(const Sampler::Sample &sample) {
	return processLikelihood(sample.getDoubleParameter(0), sample.getDoubleParameter(1));
}

double LogNormalLH::processLikelihood(const double mu, const double sigma) {
	if(sigma < 0.0) return 0.;
    boost::math::lognormal_distribution<> dist(mu, sigma);
	double likelihood=1.;
	for(size_t i=0; i<data.size(); ++i){
		likelihood *= boost::math::pdf(dist, data[i]);
	}
	return likelihood;
}

double LogNormalLH::processLikelihood(const int idx, const double mu, const double sigma) {
	if(sigma < 0.0) return 0.;
	boost::math::lognormal_distribution<> dist(mu, sigma);
	double lh = pdf(dist, data[idx]);
	return lh;
}

void LogNormalLH::initData(const size_t nData, const double mu, const double sigma) {

	double tmpData[nData];

	if(Parallel::mpiMgr().isMainProcessor()){
		for(size_t i=0; i<nData; ++i){
			tmpData[i] = Parallel::mpiMgr().getPRNG()->genLogNormal(mu, sigma);
		}
	}

	Parallel::mpiMgr().broadcastValues(nData,tmpData);

	for(size_t i=0; i<nData; ++i){
		data.push_back(tmpData[i]);
	}

}

string LogNormalLH::getName(char sep) const {
	stringstream ss;
	ss << "Log" << sep << "Normal";
	return ss.str();
}

} // namespace Likelihood
} // namespace StatisticalModel

