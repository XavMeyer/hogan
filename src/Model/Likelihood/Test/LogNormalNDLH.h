//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogNormalNDLH.h
 *
 * @date 14 oct. 2013
 * @author meyerx
 * @brief
 */

#ifndef LOGNORMALNDLH_H_
#define LOGNORMALNDLH_H_

#include "../LikelihoodInterface.h"

namespace StatisticalModel {
namespace Likelihood {

class LogNormalLH;

class LogNormalNDLH: public LikelihoodInterface {
public:
	LogNormalNDLH(const unsigned int aNDim, const size_t aNData, const double *mu=MU_ARR, const double *sigma=SIGMA_ARR);
	~LogNormalNDLH();

	double processLikelihood(const Sampler::Sample &sample);
	string getName(char sep=' ') const;

	size_t getNDim();
	size_t getNData();
	LogNormalLH* getLogNormalLH(size_t idx);

	double getAveragVar(size_t startP, size_t endP) const;

private:

	static const double MU_ARR[], SIGMA_ARR[];

	unsigned int nDim;
	size_t nData;

	typedef LogNormalLH* logNormPtr_t;
	logNormPtr_t *logNormPtr;

};

} // namespace Likelihood
} // namespace StatisticalModel

#endif /* LOGNORMALNDLH_H_ */
