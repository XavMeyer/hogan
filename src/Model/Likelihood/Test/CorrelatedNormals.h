//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CorrelatedNormals.h
 *
 * @date Oct 24, 2014
 * @author meyerx
 * @brief
 */
#ifndef CORRELATEDNORMALS_H_
#define CORRELATEDNORMALS_H_

#include "../LikelihoodInterface.h"
#include "Parallel/Parallel.h"

#include <boost/math/distributions.hpp>

#include <Eigen/Cholesky>

namespace StatisticalModel {
namespace Likelihood {

class CorrelatedNormals : public LikelihoodInterface {
public:
	CorrelatedNormals(const size_t aNData, const string &aFName);
	~CorrelatedNormals();

	double processLikelihood(const Sampler::Sample &sample);
	string getName(char sep=' ') const;

	std::string getFName() const;
	size_t getNData() const;
	size_t getNDim() const;
	double getSigmaDiag(size_t idx) const;

private:

	static const double TRUE_VAR[];

	std::string fName;
	unsigned int nDim;
	size_t nData;

	vector<double> mu, sigma, cholesky;
	vector< vector<double> > data;

	void readFile(const string &fName);
	void initData();

};

} // namespace Likelihood
} // namespace StatisticalModel


#endif /* CORRELATEDNORMALS_H_ */
