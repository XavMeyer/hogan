//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogNormalLH.cpp
 *
 * @date 10 oct. 2013
 * @author meyerx
 * @brief
 */

#ifndef LOGNORMALLH_H_
#define LOGNORMALLH_H_

#include "math.h"
#include "../LikelihoodInterface.h"
#include "LogNormalNDLH.h"
#include "Parallel/Parallel.h"

#include <boost/math/distributions.hpp>
#include <vector>

namespace StatisticalModel {
namespace Likelihood {

class LogNormalLH: public LikelihoodInterface {
public:
	LogNormalLH(const size_t nData, const double aMu, const double aSigma);
	~LogNormalLH();

    double processLikelihood(const Sampler::Sample &sample);
	string getName(char sep=' ') const;

	const double getMu() const { return mu; }
	const double getSigma() const { return sigma; };

private:

	const double mu, sigma;
    vector<double> data;

    void initData(const size_t nData, const double mu, const double sigma);
    double processLikelihood(const double mu, const double sigma);
    double processLikelihood(const int idx, const double mu, const double sigma);
    friend double LogNormalNDLH::processLikelihood(const Sampler::Sample &sample);

};

} // namespace Likelihood
} // namespace StatisticalModel

#endif /* LOGNORMALLH_H_ */
