//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CorrelatedNormals.cpp
 *
 * @date Oct 24, 2014
 * @author meyerx
 * @brief
 */
#include "CorrelatedNormals.h"

namespace StatisticalModel {
namespace Likelihood {

const double CorrelatedNormals::TRUE_VAR[] = {
		5.38e-3, 2.93e-3, 2.62e-2, 1.41e-2, 1.93e-2,
		1.04e-2, 7.59e-2, 3.87e-2, 8.64e-2, 4.63e-2,
		1.69e-1, 8.77e-2, 1.62e-1, 8.66e-2, 2.89e-1,
		1.57e-1, 3.87e-1, 1.99e-1, 4.19e-1, 2.27e-1 };

CorrelatedNormals::CorrelatedNormals(const size_t aNData, const string &aFName) :
		LikelihoodInterface(), fName(aFName), nData(aNData) {
	isLogLH = true;
	isUpdatableLH = false;

	readFile(fName);
	initData();

}

CorrelatedNormals::~CorrelatedNormals() {
}

void CorrelatedNormals::readFile(const string &fName) {
	ifstream iFile(fName.c_str());

	iFile >> nDim;

	mu.resize(nDim, 0.);
	sigma.resize(nDim*nDim, 0.);
	cholesky.resize(nDim * nDim, 0.);

    for(uint iF=0; iF<nDim; ++iF) {
    	for(uint jF=0; jF<nDim; ++jF) {
			iFile >> sigma[iF*nDim+jF];
		}
    }
}

void CorrelatedNormals::initData(){
	const RNG *myRNG = Parallel::mpiMgr().getPRNG();

	Eigen::Map<Eigen::MatrixXd> mappedMatrix(sigma.data(), nDim, nDim);
	Eigen::Map<Eigen::MatrixXd> mappedCholesky(cholesky.data(), nDim, nDim);

	Eigen::LLT<Eigen::MatrixXd> choleskyDecomp(mappedMatrix);
	if(choleskyDecomp.info() == Eigen::NumericalIssue) { cerr << "error in cholesky decomposition." << endl; abort();}
	if(choleskyDecomp.info() == Eigen::NoConvergence) { cerr << "error in cholesky decomposition." << endl; abort();}
	if(choleskyDecomp.info() == Eigen::InvalidInput) { cerr << "error in sigma input." << endl; abort();}

	mappedCholesky = choleskyDecomp.matrixL();

	if(Parallel::mpiMgr().isMainProcessor()){
		for(size_t i=0; i<nData; ++i){
			data.push_back(myRNG->genMultivariateGaussian(nDim, &cholesky[0]));
			for(size_t j=0; j<data[i].size(); ++j){
				data[i][j] += mu[j];
			}
		}
	} else {
		for(size_t i=0; i<nData; ++i){
			data.push_back(vector<double>(nDim, 0.));
		}
	}

	for(size_t i=0; i<nData; ++i){
		Parallel::mpiMgr().broadcastValues(nDim, &data[i][0]);
	}
}

double CorrelatedNormals::processLikelihood(const Sampler::Sample &sample) {
	const RNG *myRng = Parallel::mpiMgr().getPRNG();
	double lh = 0.0;

	vector<double> sampledMu;
	for(uint i=0; i<nDim; ++i){
		sampledMu.push_back(sample.getDoubleParameter(i));
		//sampledMu.push_back(0.);
		//cout << sampledMu.back() << "\t";
	}
	//cout << endl << "---------------------------------------" << endl;


	vector<double> pdf = myRng->pdfMVN(data, sampledMu, sigma);
	for(size_t iDat=0; iDat<nData; ++iDat) {
		lh += log(pdf[iDat]);
	}

	return lh;
}

string CorrelatedNormals::getName(char sep) const {
	stringstream ss;
	ss << "Correlated" << sep << nDim << "D";
	return ss.str();
}

std::string CorrelatedNormals::getFName() const {
	return fName;
}

size_t CorrelatedNormals::getNData() const {
	return nData;
}

size_t CorrelatedNormals::getNDim() const {
	return nDim;
}

double CorrelatedNormals::getSigmaDiag(size_t idx) const {
	return sigma[idx*nDim+idx];
}

} // namespace Likelihood
} // namespace StatisticalModel

