//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 *
 */
#ifndef BASE_CODONMODELS_H_
#define BASE_CODONMODELS_H_

#include "TreeNode.h"
#include "SampleWrapper.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"
#include "Nodes/IncNodes.h"

#include "Model/Likelihood/LikelihoodInterface.h"

#include <iostream>
#include <fstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;
typedef DL::CodonFrequencies::codonFrequencies_t codonFreq_t;

class Base : public LikelihoodInterface {
public:
	Base(const size_t aNClass, const scalingType_t aScalingType, const modelType_t aModelType,
			   const bool aUseCompression, const size_t aNThread,
			   const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
			   const std::string &aFileAlign, const std::string &aFileTree);
	~Base();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	nuclModel_t getNucleotideModel() const;
	codonFreq_t getCodonFrequencyType() const;
	size_t getNClass() const;
	modelType_t getModelType() const;
	scalingType_t getScalingType() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	bool isUsingCompression() const;
	size_t getNThread() const;
	std::string getAlignFile() const;
	std::string getTreeFile() const;

	double getGlobalCompressionRate() const;
	std::string getCompressionReport() const;
	std::string getNewickString() const;
	void printDAG() const;
	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

private:

	/* Input data */
	const size_t nClass;
	scalingType_t scalingType;
	modelType_t modelType;
	bool useCompression, first;
	size_t nThread, nSite;
	const nuclModel_t nuclModel;
	const codonFreq_t cFreqType;
	std::string fileAlign, fileTree;
	DL::FastaReader fr;
	DL::MSA msa;
	DL::CompressedAlignements compressedAligns;
	DL::NewickParser np;
	const DL::TreeNode &newickRoot;

	/* Internal data */
	TreeNode rootTN;
	std::vector<TreeNode*> branchPtr;
	DL::Utils::Frequencies frequencies;
	std::vector<size_t> labelBranches;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	accComp_t accComp, accTotal;

	/* DAG data */
	std::vector<MatrixNode*> ptrQs;
	MatrixScalingNode *scalingMat;
	FinalResultNode *rootDAG;

	/* DAG Scheduler */
	DAG::Scheduler::BaseScheduler *scheduler;


	//! Called by constructor to init everything
	void init(const bool aUseCompression);

	//! Prepare DAG elements (matrices, scaling matrix
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG();

	//! Labelize the internal branches
	void labelizeBranch(const DL::TreeNode &newickNode);

	//! Create recursively the internal tree based on the newick one
	void createTree(const DL::TreeNode &newickNode, TreeNode *node);
	//! Add the adequate BranchMatrix node to the PS node
	void addBranchMatrixNodes(TreeNode *node);

	//! Create recursively a sub-DAG (based on the internal Tree)
	void createSubDAG(size_t idClass, const std::vector<size_t> &subSites, TreeNode *node, DAG::BaseNode *nodeDAG);

	// Create Nodes
	// virtual void createMatrixNode();

	void cleanTreeAndDAG();

	// Setters for the samples
	void setSample(const SampleWrapper &sw);
	void setKappa(const SampleWrapper &sw);
	void setThetas(const SampleWrapper &sw);
	void setProportions(const SampleWrapper &sw);
	void setOmegas(const SampleWrapper &sw);
	void setBranchLength(const SampleWrapper &sw);

};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BASE_CODONMODELS_H_ */
