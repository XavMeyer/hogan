//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "TreeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

TreeNode::TreeNode(const size_t aNClass, const MolecularEvolution::DataLoader::TreeNode &aNewickNode) :
	nClass(aNClass), branchMDAG(nClass, NULL){
	id = aNewickNode.getId();
	name = aNewickNode.getName();
	branchLength = aNewickNode.getLength();

	leaf = false;

}

TreeNode::~TreeNode() {
}


void TreeNode::addChild(TreeNode *child) {
	children.push_back(child);
}

TreeNode* TreeNode::getChild(size_t id) {
	return children[id];
}


const treeNodePS_children& TreeNode::getChildren() const {
	return children;
}

const std::string& TreeNode::getName() const {
	return name;
}


void TreeNode::addBranchMatrixNode(BranchMatrixNode *node) {
	branchMDAG[node->getIDClass()] = node;
}

BranchMatrixNode* TreeNode::getBranchMatrixNode(size_t aIDClass) {
	assert(aIDClass < nClass);
	return branchMDAG[aIDClass];
}

void TreeNode::setBranchLength(const double aBL) {
	if(branchLength != aBL) {
		branchLength = aBL;

		for(size_t i=0; i<branchMDAG.size(); ++i){
			branchMDAG[i]->setBranchLength(branchLength);
		}
	}
}

void TreeNode::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNode::isLeaf() const {
	return leaf;
}

const std::string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << branchLength;
	if(leaf) ss << " - isLeaf";
	ss << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]" << children[i]->name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNode::deleteChildren() {
	while(!children.empty()) {
		TreeNode *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string TreeNode::buildNewick() const {
	std::string nwkStr;
	addNodeToNewick(nwkStr);
	return nwkStr;
}

void TreeNode::addNodeToNewick(std::string &nwk) const {
	if(!children.empty()) {
		nwk.push_back('(');
		children[0]->addNodeToNewick(nwk);

		for(size_t iC=1; iC<children.size(); ++iC) {
			nwk.push_back(',');
			nwk.push_back(' ');
			children[iC]->addNodeToNewick(nwk);
		}
		nwk.push_back(')');
	}

	nwk.append(name);
	nwk.push_back(':');
	std::stringstream ss;
	ss << branchLength;
	nwk.append(ss.str());
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
