//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "SampleWrapper.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

const size_t SampleWrapper::N_KAPPA = 1;
const size_t SampleWrapper::N_THETA = 5;

SampleWrapper::SampleWrapper(const size_t aNClass, const MU::nuclModel_t aNuclModel, const std::vector<double> &sample) :
		N_CLASS(aNClass), N_BL(sample.size()-(nuclModelSize(aNuclModel)+(2*N_CLASS-1))) {

	// [Nucl. model parameters (1 or 5)] [Px (nClass-1)] [omegas (nClass)] [branches remainder]
	size_t iS=0;

	// [Nucl. model parameters (1 or 5)]
	if(aNuclModel == MU::GTR) {
		for(size_t iT=0; iT<N_THETA; iT++) {
			thetas.push_back(sample[iS]);
			++iS;
		}
	} else if(aNuclModel == MU::K80) {
		K = sample[iS];
		++iS;
	}

	// [Px (nClass-1)]
	for(size_t iC=0; iC<N_CLASS-1; ++iC) {
		Px.push_back(sample[iS]);
		++iS;
	}

	// [omegas (nClass)]
	for(size_t iC=0; iC<N_CLASS; ++iC) {
		omegas.push_back(sample[iS]);
		++iS;
	}

	// [branches remainder]
	for(size_t iB=0; iB<N_BL; ++iB) {
		branchLength.push_back(sample[iS]);
		++iS;
	}
}

SampleWrapper::~SampleWrapper() {
}

std::string SampleWrapper::toString() const {
	std::stringstream ss;
	ss.precision(4);
	ss << std::fixed;

	if(thetas.size() > 0 ) {
		ss << "Thetas :\t";
		for(size_t iT=0; iT<N_THETA; ++iT) {
			ss << thetas[iT] << "\t";
		}
		ss << std::endl;
	} else {
		ss << "Kappa :\t" << K << std::endl;
	}

	for(size_t iC=0; iC<N_CLASS-1; ++iC) {
		ss << "P" << iC << " = " << Px[iC] << "\t";
	}
	if(N_CLASS-1>0) ss << std::endl;

	for(size_t iC=0; iC<N_CLASS; ++iC) {
		ss << "W" << iC << " = " << omegas[iC] << "\t";
	}
	ss << std::endl;

	for(size_t i=0; i<N_BL; ++i) {
		ss << branchLength[i] << "\t";
	}

	return ss.str();
}

size_t SampleWrapper::nuclModelSize(const MU::nuclModel_t aNuclModel) {
	if(aNuclModel == MU::GTR) {
		return N_THETA;
	} else if(aNuclModel == MU::K80) {
		return N_KAPPA;
	} else {
		std::cerr << "Error : size_t SampleWrapper::offset(const MU::nuclModel_t aNuclModel);"<< std::endl;
		std::cerr << "Nucleotide model not supported."<< std::endl;
		abort();
		return 0;
	}
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
