//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "Proportions.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {


Proportions::Proportions(size_t aNClass) : P(aNClass-1, 1.), proportions(aNClass, 1.) {
	proportions.front() = 1.;
}

Proportions::Proportions(const std::vector<double> &aP) : P(aP), proportions(P.size()+1, 0.) {
	set(P);
}

Proportions::~Proportions() {
}

void Proportions::set(const std::vector<double> &aP) {

	P = aP;

	/*if(proportions.size() == 3) {
		double eV0 = exp((P[0]-0.5)*2.*99.);
		double eV1 = exp((P[1]-0.5)*2.*99.);
		// Compute proportions
		proportions[0] = eV0/(1.+eV0+eV1);
		proportions[1] = eV1/(1.+eV0+eV1);
		proportions[2] = 1-(proportions[0] + proportions[1]);
	} else {*/
		for(size_t iProp=0; iProp<proportions.size(); ++iProp) {	// For the N classes
			if(iProp == 0) {										// Class 0 ==> prop_0 = prod(P[j]) with j in [0..N-1]
				proportions[iProp] = 1.;
				for(size_t iP=0; iP<P.size(); ++iP) {
					proportions[iProp] *= P[iP];
				}
			} else {												// Class i with i>0 ==> prop_i = prod(P[j]) * (1-P[i-1])
				proportions[iProp] = 1.;							//					==> with j in [0..N-1 \ i]
				for(size_t iP=0; iP<P.size(); ++iP) {
					if(iP == (iProp-1)) {
						proportions[iProp] *= (1.-P[iP]);
					} else {
						proportions[iProp] *= P[iP];
					}
				}
			}
		}
	//}
}

double Proportions::getPx(const size_t idx) const {
	assert(idx < P.size());
	return P[idx];
}

const std::vector<double>& Proportions::getProportions() const {
	return proportions;
}

double Proportions::getProportion(const size_t idx) const {
	assert(idx < proportions.size());
	return proportions[idx];
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
