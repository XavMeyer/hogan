//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPV.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "CPV.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

CPV::CPV(size_t aSitePos, int aIndex) :
		sitePos(aSitePos), myIndex(aIndex){
	compressed = false;
}

CPV::~CPV() {
}

bool CPV::operator== (const CPV &cpv) const {
	return (sitePos == cpv.sitePos) &&
		   (myIndex == cpv.myIndex) &&
		   (compressed == cpv.compressed);
}

size_t CPV::getSitePos() const {
	return sitePos;
}

int CPV::getIndex() const {
	return myIndex;
}

void CPV::setIndex(int aVecIndex) {
	myIndex = aVecIndex;
}

bool CPV::isCompressed() const {
	return compressed;
}

void CPV::setCompressed(const bool aCompressed) {
	compressed = aCompressed;
}

bool CPV::canCompress(const CPV &other) const {
	return hasSameSignature(other);
}

void CPV::addToSignature(const size_t nVals, const std::vector<unsigned short int> &aIDs) {
	std::vector<bool> subSignature(nVals, false);

	// Build the signature
	for(size_t i=0; i<aIDs.size(); ++i){
		subSignature[aIDs[i]] = true;
	}
	// Add it
	signature.push_back(subSignature);

}

void CPV::addToSignature(const CPV &other) {
	// Add all the signatures
	signature.insert(signature.end(), other.signature.begin(), other.signature.end());
}

bool CPV::hasSameSignature(const CPV &other) const {
	if(signature.size() != other.signature.size()) return false;

	for(size_t iS=0; iS<signature.size(); ++iS) {
		for(size_t iB=0; iB<signature[iS].size(); ++iB){
			if(signature[iS][iB] != other.signature[iS][iB]) return false;
		}
	}
	return true;
}

std::string CPV::toString() const {
	std::stringstream ss;
	ss << sitePos << " :: " << myIndex;
	return ss.str();
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
