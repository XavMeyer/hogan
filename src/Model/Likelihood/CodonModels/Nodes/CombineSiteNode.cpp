//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "CombineSiteNode.h"
#include "Types.h"

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

CombineSiteNode::CombineSiteNode(const size_t aNClass, const std::vector<size_t> &aSites,
		DL::CompressedAlignements &ca) :
		DAG::BaseNode(), nClass(aNClass), factors(ca.getNSite(), 1) {

	mScaling = NULL;

	for(size_t i=0; i<ca.getNSite(); ++i) {
		factors(i) = ca.getSiteFactor(i);
	}

	// init ptr to likelihoods
	for(size_t i=0; i<aSites.size(); ++i) {
		vecSSLik.push_back(SiteSubLikelihood(nClass, aSites[i]));
	}

}

CombineSiteNode::~CombineSiteNode() {
}

void CombineSiteNode::doProcessing() {

	const Proportions &prop = mScaling->getProportions();

	if(EdgeNode::SCALING_TYPE == EdgeNode::LOG) {
		// LOG_LIKELIHOOD
		likelihood = 0.;
		for(size_t iS=0; iS<vecSSLik.size(); ++iS) {
			// Combine sub-likelihood at class level
			double logLikSite = 0;
			for(size_t iC=0; iC<nClass; ++iC) {
				int idx = vecSSLik[iS].idxSLik[iC];
				const EigenVec_t &logNorm = vecSSLik[iS].ptrNode[iC]->getNorm();
				const EigenVec_t &subLik = vecSSLik[iS].ptrNode[iC]->getLikelihood();

				double tmpSublik = subLik(idx) >= 0. ? subLik(idx) : 0.;
				double tmpLogLik = log(prop.getProportion(iC) * tmpSublik) + logNorm(idx);
				//if(tmpLogLik != tmpLogLik) {
				//	std::cout << "tmpLogLik = " << tmpLogLik << "\t::\t" << prop.getProportion(iC) << " | "<< subLik(idx) << "\t::\t" << logNorm(idx) << std::endl;
				//}

				if(iC == 0) {
					logLikSite = tmpLogLik;
				} else {
					logLikSite = LOG_PLUS(logLikSite, tmpLogLik);
				}
			}

			// Conbine likelihood at site level
			double factor = factors(vecSSLik[iS].site);
			likelihood += factor*logLikSite;
		}
	} else {
		// LOG_LIKELIHOOD
		likelihood = 0.;
		for(size_t iS=0; iS<vecSSLik.size(); ++iS) {
			// Combine sub-likelihood at class level
			double likSite = 0;
			for(size_t iC=0; iC<nClass; ++iC) {
				int idx = vecSSLik[iS].idxSLik[iC];
				const EigenVec_t &subLik = vecSSLik[iS].ptrNode[iC]->getLikelihood();
				likSite += prop.getProportion(iC) * subLik(idx);
			}

			// Conbine likelihood at site level
			double factor = factors(vecSSLik[iS].site);
			likelihood += factor*log(likSite);
		}
	}
}

bool CombineSiteNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootNode)){
		RootNode *node = dynamic_cast<RootNode*>(aChild);
		linkLikSite(node);
	} else if(childtype == typeid(MatrixScalingNode)){
		mScaling = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

void CombineSiteNode::linkLikSite(RootNode *node) {

	// Get node class
	const size_t iC = node->getIDClass();

	for(size_t iS=0; iS<vecSSLik.size(); ++iS){
		int idxSLik = node->getLikelihoodIndex(vecSSLik[iS].site);
		if(idxSLik >= 0) {
			vecSSLik[iS].idxSLik[iC] = idxSLik;
			vecSSLik[iS].ptrNode[iC] = node;
		}
	}
}


} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
