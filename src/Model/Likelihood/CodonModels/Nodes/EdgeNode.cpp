//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "EdgeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

const EdgeNode::scaling_t EdgeNode::SCALING_TYPE = NONE;

EdgeNode::EdgeNode(const size_t aIDClass, const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), idClass(aIDClass), frequencies(aFrequencies)  {

	bmNode = NULL;
	init(aSitePos);
}

EdgeNode::~EdgeNode() {
}


void EdgeNode::init(const std::vector<size_t> &sitePos) {

	size_t cnt = 0;

	for(size_t iP=0; iP<sitePos.size(); ++iP) {
			CPV tmp(sitePos[iP], cnt);
			vecCPV.push_back(tmp);
			++cnt;
	}

	// Create vectors
	H.resize(frequencies.size(), vecCPV.size());

	if(SCALING_TYPE != NONE) {
		nrm.resize(vecCPV.size());
	}
}

const CPV* EdgeNode::getPtrCPV(const size_t aSitePos) const {
	cstItVecCPV_t it;
	it = std::find_if(vecCPV.begin(), vecCPV.end(), HasSameSitePosition(aSitePos));
	// If the vector is not find there, return NULL
	if(it == vecCPV.end()) {
		return NULL;
	} else {
		return &(*it);
	}
}

const EigenMat_t& EdgeNode::getH() const {
	return H;
}

const EigenVec_t& EdgeNode::getNorm() const {
	return nrm;
}

size_t EdgeNode::getIDClass() const {
	return idClass;
}

bool EdgeNode::hasEqualVecCPV(const EdgeNode &other) const {
	if(this->vecCPV.size() != other.vecCPV.size()) {
		return false;
	}

	for(size_t i=0; i<this->vecCPV.size(); ++i) {
		bool isEqual = this->vecCPV[i] == other.vecCPV[i];
		if(!isEqual) {
			return false;
		}
	}

	return true;
}

size_t EdgeNode::compressVecCPV() {

	size_t iBase = 0, nCompressed = 0;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) { // For each CPVs
		// If this element iCPV is already compressed, continue
		if(vecCPV[iCPV].isCompressed()) continue;

		// Set the new position in G and G
		vecCPV[iCPV].setIndex(iBase);
		++iBase;

		for(size_t jCPV=iCPV+1; jCPV<vecCPV.size(); ++jCPV) { // For next CPVs
			// If this element jCPV is already compressed, continue
			if(vecCPV[jCPV].isCompressed()) continue;

			if(vecCPV[iCPV].canCompress(vecCPV[jCPV])){
				// Compress cVal2 -> cVal1
				vecCPV[jCPV].setIndex(vecCPV[iCPV].getIndex());
				vecCPV[jCPV].setCompressed(true);
				nCompressed++;
			}
		}
	}
	return nCompressed;
}

size_t EdgeNode::getNCompressed() const {
	size_t nCompressed = 0;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) {
			nCompressed++;
		}
	}
	return nCompressed;
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
