//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef EDGENODE_CODONMODELS_H_
#define EDGENODE_CODONMODELS_H_

#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

#include "DAG/Node/Base/BaseNode.h"
#include "BranchMatrixNode.h"
#include "CPV.h"
#include "Types.h"

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class EdgeNode : public DAG::BaseNode {
public:
	EdgeNode(const size_t aIdClass, const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies);
	virtual ~EdgeNode();

	const CPV* getPtrCPV(const size_t aSitePos) const;
	const EigenMat_t& getH() const;
	const EigenVec_t& getNorm() const;
	size_t getIDClass() const;

	bool isFGBranch() const;
	bool isAfterFGBranch() const;

	bool hasEqualVecCPV(const EdgeNode &other) const;

	size_t compressVecCPV();

	size_t getNCompressed() const;

public:
	enum scaling_t {NONE, NORMAL, LOG};
	static const scaling_t SCALING_TYPE;

protected:

	typedef std::vector<CPV> vecCPV_t; // CPV vector
	typedef vecCPV_t::const_iterator cstItVecCPV_t; // Associated const iterator

	const size_t idClass;
	DL_Utils::Frequencies &frequencies;
	BranchMatrixNode *bmNode;
	Eigen::MatrixXd H;
	Eigen::VectorXd nrm;

	vecCPV_t vecCPV;

	void init(const std::vector<size_t> &sitePos);
};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* EDGENODE_CODONMODELS_H_ */
