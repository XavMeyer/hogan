//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINESITENODE_CODONMODELS_H_
#define COMBINESITENODE_CODONMODELS_H_

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"
#include "MatrixScalingNode.h"
#include "RootNode.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

#include "Types.h"
#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

namespace DL = ::MolecularEvolution::DataLoader;

class CombineSiteNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	CombineSiteNode(const size_t aNClass, const std::vector<size_t> &aSites, DL::CompressedAlignements &ca);
	~CombineSiteNode();

private:

	struct SiteSubLikelihood {
		size_t site;
		std::vector<int> idxSLik;
		std::vector<RootNode*> ptrNode;
		SiteSubLikelihood(const size_t aNClass, const size_t aSite) :
						  site(aSite), idxSLik(aNClass), ptrNode(aNClass) {
			for(size_t iC=0; iC<aNClass; ++iC){idxSLik[iC] = -1; ptrNode[iC] = NULL;}
		}
		~SiteSubLikelihood(){}
		bool operator== (const SiteSubLikelihood &aPLS) const{
			return aPLS.site == site;
		}
	};

	typedef std::vector<SiteSubLikelihood> vecSSLik_t;
	typedef vecSSLik_t::iterator itVecPtrLS_t;

	const size_t nClass;
	MatrixScalingNode *mScaling;
	vecSSLik_t vecSSLik;
	EigenArr_t factors;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void linkLikSite(RootNode *node);

};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINESITENODE_CODONMODELS_H_ */
