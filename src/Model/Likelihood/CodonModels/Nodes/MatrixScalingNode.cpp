//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixScalingNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixScalingNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

MatrixScalingNode::MatrixScalingNode(const size_t aNClass) : DAG::BaseNode(), nClass(aNClass),
		Px(aNClass-1, 0.), proportions(aNClass), ptrQs(aNClass, NULL) {

	scaling = 0.;
}

MatrixScalingNode::~MatrixScalingNode() {
}

double MatrixScalingNode::getScaling() const {
	return scaling;
}

const Proportions& MatrixScalingNode::getProportions() const {
	return proportions;
}

void MatrixScalingNode::setPx(const std::vector<double> &aPx) {
	assert(aPx.size() == Px.size());

	bool signalUpdated = false;
	for(size_t iP=0; iP<Px.size(); ++iP) {
		if(aPx[iP] != Px[iP]) {
			Px[iP] = aPx[iP];
			signalUpdated = true;
		}
	}

	if(signalUpdated) {
		DAG::BaseNode::updated();
	}
}

void MatrixScalingNode::doProcessing() {
	proportions.set(Px);

	// Proportional scaling
	scaling = 0.;
	for(size_t iC=0; iC<nClass; ++iC) {
		scaling += proportions.getProportion(iC)*ptrQs[iC]->getMatrixQScaling();
	}
}

bool MatrixScalingNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void MatrixScalingNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		MatrixNode* matrixNode = dynamic_cast<MatrixNode*>(aChild);
		assert(matrixNode->getIDClass() < ptrQs.size());
		ptrQs[matrixNode->getIDClass()] = matrixNode;
	} else {
		std::cerr << "Child nodes should be MatrixNode only." << std::endl;
		abort();
	}
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
