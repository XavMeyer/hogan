//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "RootNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

RootNode::RootNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies, const bool compress) :
	 			 ParentNode(aIDClass, aSitePos, aFrequencies, compress) {
}

RootNode::~RootNode() {
}

int RootNode::getLikelihoodIndex(const size_t aSitePos) const {
	const CPV* tmp = getPtrCPV(aSitePos);
	if(tmp != NULL) return tmp->getIndex();
	else return -1;
}

const EigenVec_t& RootNode::getLikelihood() const {
	return likelihood;
}



void RootNode::doProcessing() {

	Eigen::MatrixXd G(H.rows(), H.cols());

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<hChildrenBlock.size(); ++iE) {
		if(iE == 0) {
			G = hChildrenBlock[iE]->getH();
			first = false;
			if(SCALING_TYPE != NONE) {
				nrm = hChildrenBlock[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(hChildrenBlock[iE]->getH());
			if(SCALING_TYPE == NORMAL) {
				nrm = nrm.cwiseProduct(hChildrenBlock[iE]->getNorm());
			} else if(SCALING_TYPE == LOG) {
				nrm += hChildrenBlock[iE]->getNorm();
			}
		}
	}

	for(size_t iH=0; iH<hChildrenPos.size(); ++iH) {
		for(size_t iC=0; iC<hChildrenPos[iH].size(); ++iC) {
			const EigenMat_t &tmpH = hChildrenPos[iH][iC].node->getH();
			const EigenVec_t &tmpV = tmpH.col(hChildrenPos[iH][iC].cpv->getIndex());
			const EigenVec_t &tmpNrm = hChildrenPos[iH][iC].node->getNorm();

			if(first && iC==0) { // first element : copy
				G.col(iH) = tmpV;
				if(SCALING_TYPE != NONE) {
					nrm(iH) = tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				}
			} else { // then : cwiseProd
				G.col(iH) = G.col(iH).cwiseProduct(tmpV);
				if(SCALING_TYPE == NORMAL) {
					nrm(iH) = nrm(iH)*tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				} else if(SCALING_TYPE == LOG) {
					nrm(iH) += tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				}
			}
		}
	}

	if(SCALING_TYPE == NORMAL) {
		for(size_t iC=0; iC<(size_t)G.cols(); ++iC){
			G.col(iC) = G.col(iC) * nrm(iC);
		}
	}

	likelihood = frequencies.getEigen().transpose() * G;

}

bool RootNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		linkChildren(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		linkChildren(leafNode);
	}
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
