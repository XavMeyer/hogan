//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixNode.h"

extern "C" int dsyevr_(char *jobz, char *range, char *uplo, int *n,
					double *a, int *lda, double *vl, double *vu,
					int *il, int *iu, double *abstol, int *m, double *w,
					double *z__, int *ldz, int *isuppz, double *work,
					int *lwork, int *iwork, int *liwork, int *info);


namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

MatrixNode::MatrixNode(size_t aIDClass, const MU::nuclModel_t aNuclModel, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), nuclModel(aNuclModel), idClass(aIDClass), frequencies(aFrequencies), soMF(nuclModel),
		D(frequencies.size()), V(frequencies.size(),frequencies.size()), scaledV(frequencies.size(), frequencies.size()) {

	coefficients.assign(soMF.getNBCoefficients(), 0.);
	soMF.setFrequencies(frequencies.getStd());
	matrixScaling = 0;
}

MatrixNode::~MatrixNode() {
}

void MatrixNode::setKappa(const double aK) {
	assert(nuclModel == MU::K80);
	if(aK != coefficients[0]) {
		coefficients[0] = aK;
		BaseNode::updated(); // signal that the node must be recomputed
	}
}

void MatrixNode::setThetas(const std::vector<double> &aThetas) {
	assert(nuclModel == MU::GTR);
	for(size_t iT=0; iT < aThetas.size(); ++iT) {
		if(aThetas[iT] != coefficients[iT]) {
			coefficients[iT] = aThetas[iT];
			BaseNode::updated(); // signal that the node must be recomputed
		}
	}
}

void MatrixNode::setOmega(const double aW) {
	if(aW != coefficients.back()) {
		coefficients.back() = aW;
		BaseNode::updated(); // signal that the node must be recomputed
	}
}

const size_t MatrixNode::getIDClass() const {
	return idClass;
}

const double MatrixNode::getMatrixQScaling() const {
	return matrixScaling;
}

const std::vector<double>& MatrixNode::getMatrixQ() const {
	return soMF.getMatrix();
}

const EigenSquareMat_t& MatrixNode::getMatrixA() const {
	return A;
}

const EigenSquareMat_t& MatrixNode::getEigenVectors() const {
	return V;
}

const EigenSquareMat_t& MatrixNode::getScaledEigenVectors() const {
	return scaledV;
}


const EigenVec_t& MatrixNode::getEigenValues() const {
	return D;
}

size_t MatrixNode::serializedSize() const {
	const size_t N = soMF.getMatrixDimension();
	return (1+soMF.getNBCoefficients()+N+N*N)*sizeof(double);
}

void MatrixNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;
	for(size_t i=0; i<soMF.getNBCoefficients(); ++i) {
		tmp[cnt] = coefficients[i]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		tmp[cnt+i] = *(scaledV.data() + i);
	}
	cnt += scaledV.size();
}

void MatrixNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;

	for(size_t i=0; i<soMF.getNBCoefficients(); ++i) {
		hasChanged = hasChanged || coefficients[i] != tmp[cnt];
		coefficients[i] = tmp[cnt]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		*(scaledV.data() + i) = tmp[cnt+i];
	}
	cnt += scaledV.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}
}

void MatrixNode::doProcessing() {

	// shortcut to sqrt frequencies
	const EigenVec_t &sqrtPi = frequencies.getSqrtEigen();
	const EigenVec_t &sqrtInvPi = frequencies.getSqrtInvEigen();

	// Fill the matrix given the new coefficient
	soMF.fillMatrix(coefficients);
	matrixScaling = soMF.getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = soMF.getMatrixDimension();
	Eigen::Map< EigenSquareMat_t > mappedMat(soMF.getMatrix().data(), N, N);
	Q = mappedMat;

	// Compute
	/*
	 * Q = S * PI
	 * S = Q*PI^(-1)
	 *
	 * A = PI^(1/2)*S*PI^(1/2) = PI^(1/2)*Q*PI^(-1)*PI^(1/2) = PI^(1/2)*Q*PI^(-1/2)
	 */
	A = sqrtPi.asDiagonal()*Q*sqrtInvPi.asDiagonal();

	comprEIG();
}

bool MatrixNode::processSignal(BaseNode* aChild) {
	return true;
}


void MatrixNode::comprEIG() {
	size_t nValidF = frequencies.getNValidFreq();
	const std::vector<bool> &validF = frequencies.getValidFreq();
	const EigenVec_t &sqrtPi = frequencies.getSqrtEigen();

	Eigen::MatrixXd compactA(nValidF, nValidF);

	compactA.setZero();

	// Compress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					compactA(jR, jC) = A(iR, iC);
					jR++;
				} // end if
			} // end for
			jC++;
		} // end if
	} // end for

	// Do EIG
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es;
	es.compute(compactA);

	/*V.resize(A.rows(), A.cols());
	scaledV.resize(A.rows(), A.cols());

	D.resize(A.rows());*/
	//D.setZero();
	//V.setZero();
	//scaledV.setZero();

	// Decompress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			D(iC) = es.eigenvalues()(jC);
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					V(iR, iC) = es.eigenvectors()(jR, jC);
					scaledV(iR, iC) = sqrtPi(iR)*es.eigenvectors()(jR, jC);
					jR++;
				} else {
					V(iR, iC) = 0.;
					scaledV(iR, iC) = 0.;
				} // end if
			} // end for
			jC++;
		} else {
			D(iC) = 0.;
			for(size_t iR=0; iR<(size_t)A.rows(); ++iR) {
				V(iR, iC) = iC == iR ? 1.0 : 0.0;
				scaledV(iR, iC) = iC == iR ? sqrtPi(iR) : 0.0;
			}
		} // end if
	} // end for

}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
