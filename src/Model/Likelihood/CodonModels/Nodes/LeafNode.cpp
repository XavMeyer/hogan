//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.cpp
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#include "LeafNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

LeafNode::LeafNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				   const std::string &aSpecieName, const DL::CompressedAlignements& ca,
				   DL_Utils::Frequencies &aFrequencies, bool compress) :
						   EdgeNode(aIDClass, aSitePos, aFrequencies) {

	if(!compress) {
		init(aSpecieName, ca);
	} else {
		createSignatureCPV(aSpecieName, ca);
		size_t nCompressed = compressVecCPV();
		if(nCompressed == 0) {
			init(aSpecieName, ca);
		} else {
			initCompressed(nCompressed, aSpecieName, ca);
		}
	}
}

LeafNode::~LeafNode() {
}

void LeafNode::createSignatureCPV(const std::string &aSpecieName, const DL::CompressedAlignements& ca) {

	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		//const std::vector<int>& cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		vecCPV[iCPV].addToSignature(frequencies.size(), cdnVals);
	}

}

void LeafNode::init(const std::string &aSpecieName, const DL::CompressedAlignements& ca) {
	G.resize(frequencies.size(), vecCPV.size());
	G.setZero();
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		//const std::vector<int>& cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
			G(cdnVals[iCod], iCPV) =  1.0;
		}
	}
}

void LeafNode::initCompressed(const size_t nCompressed, const std::string &aSpecieName, const DL::CompressedAlignements& ca) {
	// resize to take account of compression
	G.resize(frequencies.size(), vecCPV.size()-nCompressed);
	H.resize(frequencies.size(), vecCPV.size()-nCompressed);

	if(SCALING_TYPE != NONE) {
		nrm.resize(vecCPV.size()-nCompressed);
	}

	// Then we initialize G
	G.setZero();

	//std::cout << "Start init compressed" << std::endl;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) continue; // Skipping compressed CPV

		//const std::vector<int>& cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
			G(cdnVals[iCod], vecCPV[iCPV].getIndex()) = 1.0;
		}
	}

	//std::cout << "end init compressed" << std::endl;
}

void LeafNode::doProcessing() {

	const EigenSquareMat_t &Z = bmNode->getZ();
	const EigenVec_t &invPi = frequencies.getInvEigen();

	// Multiplication
	H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);

	if(SCALING_TYPE == NORMAL) {
		nrm.setOnes();
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			double tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = tmpNrm;
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	} else if(SCALING_TYPE == LOG) {
		nrm.setZero();
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			double tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = log(tmpNrm);
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	}
}

bool LeafNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		bmNode = dynamic_cast<BranchMatrixNode*>(aChild);
		if(bmNode->getIDClass() != idClass){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found CLASS_" << bmNode->getIDClass();
			std::cerr << " while waiting for CLASS_" <<  idClass << std::endl;
			abort();
		}
	}
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
