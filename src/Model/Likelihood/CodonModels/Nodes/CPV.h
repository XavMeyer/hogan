//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPV.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef CPV_CODONMODELS_H_
#define CPV_CODONMODELS_H_

#include "Types.h"

#include <string>
#include <sstream>
#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

class CPV {

public:
	CPV(size_t aSitePos, int aIndex);
	~CPV();
	bool operator== (const CPV &cpv) const;

	//! Site position accessor
	size_t getSitePos() const;

	//! Position in G and H (Getter, Setter)
	int getIndex() const;
	void setIndex(int aIndex);

	//! Is compressed if cpvBase as been set
	bool isCompressed() const;
	void setCompressed(const bool aCompressed);
	bool canCompress(const CPV &other) const;

	/* TODO : Assume that tree is always explored with the same pattern
	 In order to give a safer method and more generic, the bool vectors should be
	 ordered by the ID of the branch per example */
	//! Add the codons IDs of a leaf to the signature
	void addToSignature(const size_t nVals, const std::vector<unsigned short int> &aIDs);
	//! Add the codons IDs of a leaf to the signature
	void addToSignature(const CPV &other);
	//! Compare two signatures
	bool hasSameSignature(const CPV &other) const;

	std::string toString() const;

private:
	size_t sitePos;
	int myIndex;
	bool compressed;

	typedef std::vector<std::vector<bool> > signature_t;
	signature_t signature;
};

//! Compare two CPV given their sitePosition and siteClass
struct HasSameSitePosition {
	size_t sitePos;
	HasSameSitePosition(size_t aSitePos) :
		sitePos(aSitePos){}
    bool operator() (const CPV &cpv) {
    	return cpv.getSitePos() == sitePos;
    }
};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* CPV_CODONMODELS_H_ */
