//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef TYPES_CODONMODELS_H_
#define TYPES_CODONMODELS_H_

#include <stddef.h>
#include <assert.h>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

    enum scalingType_t {NONE=0, LOCAL=1, GLOBAL=2};
    enum modelType_t {M0=0, M1=1, M2=2, M1a=3, M2a=4, OTHER=5};

    typedef Eigen::VectorXd EigenVec_t;
	//typedef Eigen::Matrix<double, 61, 61, Eigen::ColMajor> EigenSquareMat_t;
    //typedef Eigen::Matrix<double, 61, Eigen::Dynamic, Eigen::ColMajor> EigenMat_t;
    typedef Eigen::MatrixXd EigenSquareMat_t;
    typedef Eigen::MatrixXd EigenMat_t;
    typedef Eigen::ArrayXd EigenArr_t;

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_CODONMODELS_H_ */
