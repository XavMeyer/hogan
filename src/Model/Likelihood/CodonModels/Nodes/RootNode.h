//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef ROOTNODE_CODONMODELS_H_
#define ROOTNODE_CODONMODELS_H_

#include "CPVNode.h"
#include "LeafNode.h"
#include "ParentNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class RootNode: public ParentNode {
public:
	RootNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
			 DL_Utils::Frequencies &aFrequencies, const bool compress = false);
	~RootNode();

	int getLikelihoodIndex(const size_t aSitePos) const;
	const EigenVec_t& getLikelihood() const;

private:

	EigenVec_t likelihood;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ROOTNODE_CODONMODELS_H_ */
