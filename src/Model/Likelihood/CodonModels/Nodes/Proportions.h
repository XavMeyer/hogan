//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef PROPORTIONS_CODONMODELS_H_
#define PROPORTIONS_CODONMODELS_H_

#include "stddef.h"
#include "Types.h"

#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

class Proportions {
public:
	Proportions(size_t aNClass);
	Proportions(const std::vector<double> &aP);
	~Proportions();

	void set(const std::vector<double> &aP);
	double getPx(size_t id) const;
	double getProportion(const size_t idx) const;
	const std::vector<double>& getProportions() const;

private:
	std::vector<double> P, proportions;

};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PROPORTIONS_CODONMODELS_H_ */
