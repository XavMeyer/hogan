//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODECODONMODEL_H_
#define TREENODECODONMODEL_H_

#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Nodes/IncNodes.h"

#include <vector>
#include <sstream>

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

class TreeNode;

typedef std::vector<TreeNode*> treeNodePS_children;

class TreeNode {
public:
	TreeNode(const size_t aNClass, const MolecularEvolution::DataLoader::TreeNode &aNewickNode);
	~TreeNode();

	void addChild(TreeNode *child);

	TreeNode* getChild(size_t id);
	const treeNodePS_children& getChildren() const;
	const std::string& getName() const;

	void addBranchMatrixNode(BranchMatrixNode *node);
	BranchMatrixNode* getBranchMatrixNode(size_t aIDClass);

	void setBranchLength(const double aBL);
	void setLeaf(const bool aLeaf);

	bool isLeaf() const;

	const std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

private:

	typedef std::vector<BranchMatrixNode *> branchMDAG_t;

	/* Default data */
	size_t id;
	const size_t nClass;
	std::string name;
	double branchLength;

	/* state linked to foreground branch */
	bool leaf;
	treeNodePS_children children;

	/* Linkage with DAG elements */
	branchMDAG_t branchMDAG;



};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODECODONMODEL_H_ */
