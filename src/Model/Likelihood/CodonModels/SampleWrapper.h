//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLEWRAPPER_CODONMODELS_H_
#define SAMPLEWRAPPER_CODONMODELS_H_

#include <vector>
#include <sstream>
#include <iostream>

#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

namespace MU = ::MolecularEvolution::MatrixUtils;
class Base;

class SampleWrapper {
	friend class Base;
public:
	SampleWrapper(const size_t aNClass, const MU::nuclModel_t aNuclModel, const std::vector<double> &sample);
	~SampleWrapper();

	std::string toString() const;

private:
	static const size_t N_KAPPA, N_THETA;

	const size_t N_CLASS, N_BL;
	double K;
	std::vector<double> thetas, Px, omegas, branchLength;

	size_t nuclModelSize(const MU::nuclModel_t aNuclModel);
};

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SAMPLEWRAPPER_CODONMODELS_H_ */
