//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "Base.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CodonModels {

Base::Base(const size_t aNClass, const scalingType_t aScalingType, const modelType_t aModelType,
		   const bool aUseCompression, const size_t aNThread,
		   const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
		   const std::string &aFileAlign, const std::string &aFileTree) :
				nClass(aNClass), scalingType(aScalingType), modelType(aModelType), nThread(aNThread),
				nuclModel(aNuclModel), cFreqType(aCFreqType), fileAlign(aFileAlign), fileTree(aFileTree),
				fr(fileAlign), msa(DL::MSA::CODON, fr.getAlignments(), false), compressedAligns(msa, true),
				np(fileTree), newickRoot(np.getRoot()), rootTN(nClass, newickRoot),
				frequencies(MD::Codons::NB_CODONS_WO_STOP, DL::CodonFrequencies(msa).getCodonFrequency(cFreqType))  {
	init(aUseCompression);
}

Base::~Base() {
	rootTN.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
	delete scheduler;
}

void Base::init(const bool aUseCompression) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	useCompression = aUseCompression;
	/*if(useCompression) {
		msa.sortByDistance();
	}*/

	nSite = compressedAligns.getNSite();

	prepareDAG();
	createTreeAndDAG();

	if(nThread > 1) {
		Eigen::initParallel();
		scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	for(size_t iC=0; iC<nClass; ++iC) {
		std::stringstream ss;
		ss << "Prop_" << iC;
		markerNames.push_back(ss.str());
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<6; ++iT) {
			std::stringstream ss;
			ss << "NormTheta_" << iT;
			markerNames.push_back(ss.str());
		}
	}

	markers.resize(markerNames.size());

}

double Base::processLikelihood(const Sampler::Sample &sample) {

	// Update the values
	SampleWrapper sw(nClass, nuclModel, sample.getDblValues());
	setSample(sw);

	//std::cout << sw.toString() << std::endl;

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	for(size_t iC=0; iC<nClass; ++iC) {
		markers[iC] = scalingMat->getProportions().getProportion(iC);
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		size_t offset = nClass;

		double sum = sw.thetas.size() == 5 ? 1. : 0.;
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			sum += sw.thetas[iT];
		}
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			markers[offset+iT] = sw.thetas[iT]/sum;
		}
		if(sw.thetas.size() == 5) {
			markers[offset+5] = 1/sum;
		}
	}

	/*std::cout << rootDAG->getLikelihood() << std::endl;
	getchar();*/


	// return the likelihood
	return rootDAG->getLikelihood();
}

std::string Base::getName(char sep) const {
	return std::string("CodonModels");
}

size_t Base::getNBranch() const {
	return branchPtr.size();
}

nuclModel_t Base::getNucleotideModel() const {
	return nuclModel;
}

codonFreq_t Base::getCodonFrequencyType() const {
	return cFreqType;
}

size_t Base::getNClass() const {
	return nClass;
}

modelType_t Base::getModelType() const {
	return modelType;
}

scalingType_t Base::getScalingType() const {
	return scalingType;
}

size_t Base::stateSize() const {
	size_t size = 0;
	for(size_t i=0; i<nClass; ++i) {
		size += ptrQs[i]->serializedSize();
	}
	return size;
}

void Base::setStateLH(const char* aState) {
	size_t offset = 0;
	for(size_t i=0; i<nClass; ++i) {
		ptrQs[i]->serializeFromBuffer(aState+offset);
		offset += ptrQs[i]->serializedSize();
	}

}

void Base::getStateLH(char* aState) const {
	size_t offset = 0;
	for(size_t i=0; i<nClass; ++i) {
		ptrQs[i]->serializeToBuffer(aState+offset);
		offset += ptrQs[i]->serializedSize();
	}
}

double Base::update(const vector<size_t>& pIndices, const Sample& sample) {

	SampleWrapper sw(nClass, nuclModel, sample.getDblValues());
	setSample(sw);

	//std::cout << sw.toString() << std::endl;

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial()) {
			std::cerr << "Reset partial did not found any change for parameters : " << std::endl;
			for(size_t i=0; i< pIndices.size(); ++i) {
				std::cerr << "[" << pIndices[i] << "] = "  << sample.getDoubleParameter(i) << "\t";
			}
			std::cerr << std::endl;
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	// Process the tree
	scheduler->process();

	for(size_t iC=0; iC<nClass; ++iC) {
		markers[iC] = scalingMat->getProportions().getProportion(iC);
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		size_t offset = nClass;

		double sum = sw.thetas.size() == 5 ? 1. : 0.;
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			sum += sw.thetas[iT];
		}
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			markers[offset+iT] = sw.thetas[iT]/sum;
		}
		if(sw.thetas.size() == 5) {
			markers[offset+5] = 1/sum;
		}
	}

	/*std::cout << rootDAG->getLikelihood() << std::endl;
	getchar();*/

	return rootDAG->getLikelihood();
}

bool Base::isUsingCompression() const {
	return useCompression;
}

size_t Base::getNThread() const {
	return nThread;
}

std::string Base::getAlignFile() const {
	return fileAlign;
}

std::string Base::getTreeFile() const {
	return fileTree;
}

double Base::getGlobalCompressionRate() const {
	double nCompressed = boost::accumulators::sum(accComp);
	double nTotal = boost::accumulators::sum(accTotal);

	return nCompressed/nTotal;
}

std::string Base::getCompressionReport() const {
	std::stringstream ss;
	double nCompressed = boost::accumulators::sum(accComp);
	double nTotal = boost::accumulators::sum(accTotal);


	ss << "Total number of site : " << msa.getNValidSite() << std::endl;
	ss << "Number of unique site : " << compressedAligns.getNSite() << std::endl;
	ss << "Total number of CPV : " << nTotal << std::endl;
	ss << "Number of compressed CPV : " << nCompressed << std::endl;
	ss << "Global compression rate : " << nCompressed/nTotal << std::endl;
	ss << "Average CPV per node : " << boost::accumulators::mean(accTotal);
	ss << std::endl;
	ss << "Average Comp. CPV per node : " << boost::accumulators::mean(accComp);
	ss << std::endl;

	return ss.str();
}

std::string Base::getNewickString() const {
	return rootTN.buildNewick();
}

void Base::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

DAG::Scheduler::BaseScheduler* Base::getScheduler() const {
	return scheduler;
}

std::vector<DAG::BaseNode*> Base::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	SampleWrapper sw(nClass, nuclModel, sample.getDblValues());
	setSample(sw);

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

void Base::prepareDAG() {

	// Create Matrix Scaling and proportions node
	scalingMat = new MatrixScalingNode(nClass);

	ptrQs.assign(nClass, NULL);
	for(size_t iC=0; iC<nClass; ++iC) {
		ptrQs[iC] = new MatrixNode(iC, nuclModel, frequencies);
		scalingMat->addChild(ptrQs[iC]);
	}

	// Root of the DAG
	rootDAG = new FinalResultNode();

}

void Base::createTreeAndDAG() {

	// Labelize internal branches
	labelizeBranch(newickRoot);


	// Root
	createTree(newickRoot, &rootTN);

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > 2){
		size_t tmpVal = ceil((double)nThread/2.);
		nSubDAG = std::min(tmpVal, nSite);
	}
	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);

		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteNode* ptrCSN = new CombineSiteNode(nClass, subSites, compressedAligns);
		ptrCSN->addChild(scalingMat); // Add scaling mat to CombineSiteNode

		// Create root(s) and subDAG for class iC
		for(size_t iC=0; iC<nClass; ++iC) {
			RootNode *ptrRoot = new RootNode(iC, subSites, frequencies, useCompression);
			createSubDAG(iC, subSites, &rootTN, ptrRoot); // Create sub DAG for class iC

			if(useCompression) {
				ptrRoot->compressNode();
				accComp(ptrRoot->getNCompressed());
				accTotal(end-start);
			}

			// Add root to the CombineSiteNode
			ptrCSN->addChild(ptrRoot);
		}

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSN);
	}

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << getCompressionReport();
		std::cout << "**************************************" << std::endl;
	}
}

/**
 * Define the internal branch number for each branch of the tree.
 * Branch number are mapped using the internalBranches vectors
 *
 * @param newickNode node of the newick tree
 */
void Base::labelizeBranch(const DL::TreeNode &newickNode) {
	if((newickNode.children.size() > 0) && &newickNode != &newickRoot) {
		labelBranches.push_back(newickNode.getId());
	}

	for(size_t i=0; i<newickNode.children.size(); ++i) {
		labelizeBranch(newickNode.children[i]);
	}
}



void Base::addBranchMatrixNodes(TreeNode *node) {

	// Add BranchMatrixNodes for each classes
	for(size_t iC=0; iC<nClass; ++iC) {
		BranchMatrixNode *ptrBMN;
		ptrBMN = new BranchMatrixNode(iC, scalingType, frequencies);
		if(scalingType != NONE) {
			ptrBMN->addChild(scalingMat);
		}
		ptrBMN->addChild(ptrQs[iC]);
		node->addBranchMatrixNode(ptrBMN);
	}
}


/**
 * @param newickNode a new from DataLoader::TreeNode. i.e. the ones returned from NewickParser
 * @param node a node in the CodonModels tree
 */
void Base::createTree(const DL::TreeNode &newickNode, TreeNode *node) {

	if(newickNode.children.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			TreeNode *childNode = new TreeNode(nClass, newickNode.getChildren()[i]);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update

			createTree(newickNode.getChildren()[i], childNode);
			node->addChild(childNode);
			addBranchMatrixNodes(childNode); // Add BranchMatrixNode(s)
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

/**
 * This method create one sub-DAG for a set of sites for a given class.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param idClass the matrix Q class
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 */
void Base::createSubDAG(size_t idClass, const std::vector<size_t> &subSites, TreeNode *node, DAG::BaseNode *nodeDAG) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TreeNode *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafNode *newNode;
			newNode = new LeafNode(idClass, subSites, childNode->getName(), compressedAligns, frequencies, useCompression);
			newNodeDAG = newNode;
			if(useCompression) {
				accComp(newNode->getNCompressed());
				accTotal(subSites.size());
			}
		} else { // create CPV element
			CPVNode *newNode;
			newNode = new CPVNode(idClass, subSites, frequencies, useCompression);
			newNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(idClass, subSites, childNode, newNode);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		BranchMatrixNode *ptrBMN = childNode->getBranchMatrixNode(idClass);
		newNodeDAG->addChild(ptrBMN);

	}

	// We try to compress each node
	if(useCompression) {
		const std::type_info &type = typeid(*nodeDAG);
		if(type == typeid(CPVNode)){
			CPVNode *pNode = dynamic_cast<CPVNode*>(nodeDAG);
			pNode->compressNode();
			accComp(pNode->getNCompressed());
			accTotal(subSites.size());
		}
	}
}

//void Base::createMatrixNode() {
//}

void Base::setSample(const SampleWrapper &sw) {
	if(nuclModel == MU::GTR) {
		setThetas(sw);
	} else if(nuclModel == MU::K80) {
		setKappa(sw);
	} else {
		std::cerr << "Error : void Base::setSample(const SampleWrapper &sw);" << std::endl;
		std::cerr << "Nucleotide model not supported by Light::Base" << std::endl;
		abort();
	}

	setOmegas(sw);
	setProportions(sw);
	setBranchLength(sw);
}

void Base::setKappa(const SampleWrapper &sw) {
	for(size_t iC=0; iC<nClass; ++iC) {
		ptrQs[iC]->setKappa(sw.K);
	}
}

void Base::setThetas(const SampleWrapper &sw) {
	for(size_t iC=0; iC<nClass; ++iC) {
		ptrQs[iC]->setThetas(sw.thetas);
	}
}

void Base::setOmegas(const SampleWrapper &sw) {
	for(size_t iC=0; iC<nClass; ++iC) {
		ptrQs[iC]->setOmega(sw.omegas[iC]);
	}
}

void Base::setProportions(const SampleWrapper &sw) {
	scalingMat->setPx(sw.Px);
}

void Base::setBranchLength(const SampleWrapper &sw) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setBranchLength(sw.branchLength[i]);
	}
}

void Base::cleanTreeAndDAG() {
	rootTN.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
}

} /* namespace CodonModels */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
