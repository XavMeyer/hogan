//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LikelihoodProcessors.h
 *
 *  Created on: 23 sept. 2013
 *      Author: meyerx
 */

#ifndef LIKELIHOODPROCESSORS_H_
#define LIKELIHOODPROCESSORS_H_

#include "Test/NormalLH.h"
#include "Test/LogNormalLH.h"
#include "Test/LogNormalNDLH.h"
#include "Test/CorrelatedNormals.h"

#include "BDRateDAG/Standard/StdLik.h"
#include "BDRateDAG/Alpha/AlphaLik.h"

#include "CodonModels/Base.h"

#include "TreeInference/Base.h"

#endif /* LIKELIHOODPROCESSORS_H_ */
