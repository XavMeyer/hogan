//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelilhoodFactory.h
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef LIKELILHOODFACTORY_H_
#define LIKELILHOODFACTORY_H_

#include "LikelihoodInterface.h"
#include "Likelihoods.h"

#include <string>

namespace StatisticalModel {
namespace Likelihood {

class LikelihoodFactory {
public:
	LikelihoodFactory();
	~LikelihoodFactory();

	static LikelihoodInterface::sharedPtr_t createLogNormal(const size_t nData, const double mu, const double sigma);
	static LikelihoodInterface::sharedPtr_t createLogNormalNDim(const size_t nDim, const size_t nData);
	static LikelihoodInterface::sharedPtr_t createLogNormalNDim(const size_t nDim, const size_t nData, const double *mu, const double *sigma);
	static LikelihoodInterface::sharedPtr_t createNormal(const std::string filename);
	static LikelihoodInterface::sharedPtr_t createCorrelatedNormals(const size_t nData, const std::string &fName);

	static LikelihoodInterface::sharedPtr_t createBDRateDAG(const size_t nThread, const std::string &fName, const bool isHPP=false);
	static LikelihoodInterface::sharedPtr_t createBDRAlphaDAG(const size_t nThread, const unsigned int nQuantAlpha, const std::string &fName, const bool isHPP);

	static LikelihoodInterface::sharedPtr_t createCodonM0(const CodonModels::scalingType_t aScalingType,
													 	   const bool aUseCompression, const size_t aNThread,
													 	   const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
													 	   const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createCodonM1(const CodonModels::scalingType_t aScalingType,
															const bool aUseCompression, const size_t aNThread,
															const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
															const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createCodonM2(const CodonModels::scalingType_t aScalingType,
															const bool aUseCompression, const size_t aNThread,
															const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
															const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createCodonM1a(const CodonModels::scalingType_t aScalingType,
															const bool aUseCompression, const size_t aNThread,
															const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
															const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createCodonM2a(const CodonModels::scalingType_t aScalingType,
															const bool aUseCompression, const size_t aNThread,
															const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
															const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createCodonModel(const size_t aNClass, const CodonModels::scalingType_t aScalingType,
															 const bool aUseCompression, const size_t aNThread,
															 const CodonModels::nuclModel_t aNuclModel, const CodonModels::codonFreq_t aCFreqType,
															 const std::string &aFileAlign, const std::string &aFileTree);

	static LikelihoodInterface::sharedPtr_t createTreeInference(const TreeInference::scalingType_t aScalingType, const TreeInference::modelType_t aModelType,
			   	   	   	   	   	   	   	   	   	   	   	   	    const size_t aNThread, const TreeInference::nuclModel_t aNuclModel, const TreeInference::codonFreq_t aCFreqType,
			   	   	   	   	   	   	   	   	   	   	   	   	    MolecularEvolution::DataLoader::NewickParser *aNP, const std::string &aFileAlign,
			   	   	   	   	   	   	   	   	   	   	   	   	    TreeInference::Base::treeUpdateStrategy_t aStrat);


};

} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LIKELILHOODFACTORY_H_ */
