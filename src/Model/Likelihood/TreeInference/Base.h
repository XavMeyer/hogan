//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 *
 */
#ifndef BASE_TREEINFERENCE_H_
#define BASE_TREEINFERENCE_H_

#include "TreeNode.h"
#include "SampleWrapper.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/NucleotideFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"

#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/HKY85MF.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h"

#include "Model/Prior/DirichletPDF.h"

#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"
#include "Nodes/IncNodes.h"

#include "Model/Likelihood/LikelihoodInterface.h"

#include <iostream>
#include <fstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;
namespace TR = MolecularEvolution::TreeReconstruction;

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;
typedef DL::CodonFrequencies::codonFrequencies_t codonFreq_t;

class Base : public LikelihoodInterface {
public:
	static const std::string NAME_TREE_PARAMETER;
	enum treeUpdateStrategy_t {FIXED_TREE_STRATEGY=0, OPTIMIZED_STRATEGY=1};

public:
	Base(const scalingType_t aScalingType, const modelType_t aModelType,
			   const size_t aNThread, const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
			   DL::NewickParser *aNP, const std::string &aFileAlign, treeUpdateStrategy_t aStrat);
	~Base();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	nuclModel_t getNucleotideModel() const;
	codonFreq_t getCodonFrequencyType() const;
	size_t getNClass() const;
	modelType_t getModelType() const;
	scalingType_t getScalingType() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	void saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff);
	void loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff);

	void initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, std::streampos offset);
	std::streampos getCustomizedLogSize() const;
	void writeCustomizedLog(const std::vector<Sample> &samples);

	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	size_t getNThread() const;
	std::string getAlignFile() const;

	TR::Tree::sharedPtr_t getCurrentTree() const;
	TR::TreeManager::sharedPtr_t getTreeManager() const;

	std::string getCompressionReport(DL::MSA &msa, DL::CompressedAlignements &compressedAligns) const;

	treeUpdateStrategy_t getTreeUpdateStrategy() const;
	void printDAG() const;

private:


	enum debug_t {DISABLE_DEBUG=0, GLOBAL_DEBUG=1, TREE_DEBUG=2, UPDATE_DEBUG=3};
	static const debug_t doDebug;
	std::ofstream dbgFile;

	/* Input data */
	scalingType_t scalingType;
	modelType_t modelType;
	const size_t nClass;
	bool first, isTreeExternalyImported;
	size_t nThread, nSite;
	const nuclModel_t nuclModel;
	const codonFreq_t cFreqType;
	std::string fileAlign;
	//DL::FastaReader fr;
	//DL::MSA msa;
	//DL::CompressedAlignements compressedAligns;

	/* Internal data */
	const treeUpdateStrategy_t TREE_UPDATE_STRATEGY;
	TreeNode *rootTree;
	long int curHash;
	TR::Tree::sharedPtr_t curTree;
	TR::TreeManager::sharedPtr_t treeManager;
	std::vector<TreeNode*> branchPtr;

	typedef std::map<size_t, TreeNode*> branchMap_t;
	branchMap_t branchMap;
	DL::Utils::Frequencies frequencies;
	std::vector<size_t> labelBranches;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	accComp_t accComp, accTotal;

	/* DAG data */
	std::vector<CombineSiteNode*> cmbNodes;
	std::vector<MatrixNode*> ptrQs;
	MatrixScalingNode *scalingMat;
	FinalResultNode *rootDAG;

	/* DAG Scheduler */
	size_t cntErrMsg;
	DAG::Scheduler::BaseScheduler *scheduler;

	// Prior
	StatisticalModel::DirichletPDF::sharedPtr_t ptrDirichletPr, ptrGTRPr;


	//! Called by constructor to init everything
	void init(DL::NewickParser *aNP);

	//! Define the type of sequence in fonction of the model type
	DL::MSA::sequence_t defineSeqType() const;

	//! Define the number of symbols (nucl / codons)
	size_t defineNSymbols() const;

	//! Define the empiric symbol frequency
	std::vector<double> defineFrequency(DL::MSA &msa) const;

	//! Define the number of codon class in function of the model
	size_t defineNClass() const;

	//! Prepare DAG elements (matrices, scaling matrix
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG(DL::FastaReader &fr, DL::MSA &msa, DL::CompressedAlignements &compressedAligns);

	//! Labelize the internal branches
	void labelizeBranch(const DL::TreeNode &newickNode);

	//! Create recursively the internal tree based on the newick one
	void createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node);
	//! Add the adequate BranchMatrix node to the PS node
	void addBranchMatrixNodes(TreeNode *node);

	//! Create recursively a sub-DAG (based on the internal Tree)
	void createSubDAG(size_t idClass, const std::vector<size_t> &subSites, TreeNode *node,
			DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns);

	// Create Nodes
	// virtual void createMatrixNode();

	void cleanTreeAndDAG();

	// Setters for the samples
	void setSample(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setTree(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setKappa(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setThetas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setProportions(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setOmegas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setBranchLength(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);

	void addEdge(TreeNode *parent, TreeNode *child);
	void removeEdge(TreeNode *parent, TreeNode *child);
	void orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR);

	void getDifference(const TR::vecTN_t &childrenTR, const treeNodeTI_children &children,
			std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove);
	void applyChangesToTreeAndDAG(std::vector<TR::TreeNode *> &updTreeNodes);
	void signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
			std::set<DAG::BaseNode*> &updatedNodes);
	void updateTreeAndDAG(std::set<DAG::BaseNode*> &updatedNodes);
	bool fullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR,
			TreeNode *node,	std::set<DAG::BaseNode*> &updatedNodes);

	double getTotalTreeLength(const SampleWrapper &sw);

	std::vector<std::string> getOrderedTaxaNames(DL::MSA &msa) const;

};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BASE_TREEINFERENCE_H_ */
