//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODE_TREEINFERENCE_H_
#define TREENODE_TREEINFERENCE_H_

#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Nodes/IncNodes.h"

#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

#include <vector>
#include <sstream>
#include <algorithm>

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace TR = MolecularEvolution::TreeReconstruction;

class TreeNode;

typedef std::vector<TreeNode*> treeNodeTI_children;

class TreeNode {
public:
	TreeNode(const size_t aNClass, const std::string aName, const TR::TreeNode *aTreeNodeTR);
	~TreeNode();

	void addChild(TreeNode *child);
	bool removeChild(TreeNode *child);

	bool isParent(const TreeNode *aChild) const;
	bool isChild(const TreeNode *aParent) const;

	TreeNode* getChild(size_t id);
	treeNodeTI_children& getChildren();
	const treeNodeTI_children& getChildren() const;
	const std::string& getName() const;
	size_t getId() const;

	size_t getNumberEdgeNode() const;
	void addEdgeNode(EdgeNode *node);
	EdgeNode* getEdgeNode(size_t id);

	void addBranchMatrixNode(BranchMatrixNode *node);
	BranchMatrixNode* getBranchMatrixNode(size_t aIDClass);

	void setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes);
	void setLeaf(const bool aLeaf);

	double getBranchLength() const;
	bool isLeaf() const;

	void setRequireReinitDAG(const bool aRequire);
	bool doRequireReinitDAG() const;

	const std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

private:

	typedef std::vector<EdgeNode *> treeStructDAG_t;
	typedef std::vector<BranchMatrixNode *> branchMDAG_t;

	/* Default data */
	size_t id;
	const size_t nClass;
	std::string name;
	double branchLength;

	/* state */
	bool leaf, requireReinitDAG;
	treeNodeTI_children children;

	/* Linkage with DAG elements */
	treeStructDAG_t treeStructDAG;
	branchMDAG_t branchMDAG;

};


struct SortTreeNodePtrById
{
    bool operator()( const TreeNode* n1, const TreeNode* n2 ) const {
    	return n1->getId() < n2->getId();
    }
};


} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODE_TREEINFERENCE_H_ */
