//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalResultNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "FinalResultNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

FinalResultNode::FinalResultNode() : DAG::BaseNode(), DAG::LikelihoodNode() {
}

FinalResultNode::~FinalResultNode() {
}


void FinalResultNode::doProcessing() {

	// LOG_LIKELIHOOD
	likelihood = 0.;
	for(size_t iN=0; iN<csNodes.size(); ++iN) {
		likelihood += csNodes[iN]->getLikelihood();
	}

}

bool FinalResultNode::processSignal(DAG::BaseNode* aChild) {

	return true;
}

void FinalResultNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombineSiteNode)){
		CombineSiteNode *node = dynamic_cast<CombineSiteNode*>(aChild);
		csNodes.push_back(node);
	}
}

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
