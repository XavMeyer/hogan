//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXNODE_TREEINFERENCE_H_
#define MATRIXNODE_TREEINFERENCE_H_

#include "Types.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;
namespace MU = ::MolecularEvolution::MatrixUtils;

class MatrixNode: public DAG::BaseNode {

public:
	MatrixNode(size_t aIDClass, const MU::nuclModel_t aNuclModel,
			   MU::MatrixFactory::sharedPtr_t aMF, DL_Utils::Frequencies &aFrequencies);
	~MatrixNode();

	bool setKappa(const double aK);
	bool setThetas(const std::vector<double> &aThetas);
	bool setOmega(const double aW);

	const size_t getIDClass() const;
	const double getMatrixQScaling() const;
	const TI_EigenEIGMatrix_t& getScaledEigenVectors() const;
	const TI_EigenEIGVector_t& getEigenValues() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

private:

	// Coefficients are [kappa, omega] or [thetas, omega]
	MU::nuclModel_t nuclModel;
	size_t idClass;
	std::vector<double> coefficients;
	DL_Utils::Frequencies &frequencies;

	double matrixScaling;
	MU::MatrixFactory::sharedPtr_t ptrMF;
	TI_EigenEIGVector_t D;
	TI_EigenEIGMatrix_t A, Q, scaledV;

	void doProcessing();
	bool processSignal(BaseNode* aChild);
	void comprEIG();

};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXNODE_TREEINFERENCE_H_ */
