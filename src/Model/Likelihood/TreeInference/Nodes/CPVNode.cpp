//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "CPVNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

CPVNode::CPVNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				 DL_Utils::Frequencies &aFrequencies) :
		        ParentNode(aIDClass, aSitePos, aFrequencies) {

}

CPVNode::~CPVNode() {
}

void CPVNode::doProcessing() {

	nrm.resize(0);

	TI_EigenMatrix_t G;

	const TI_EigenSquareMatrix_t &Z = bmNode->getZ();
#if TI_USE_FLOAT
	const TI_EigenVector_t &invPi = frequencies.getInvEigenFlt();
#else
	const TI_EigenVector_t &invPi = frequencies.getInvEigen();
#endif

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			first = false;
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm = children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				if(nrm.size() == 0) {
					nrm = children[iE]->getNorm();
				} else {
					nrm += children[iE]->getNorm();
				}
			}
		}
	}

	// Multiplication
	H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);

	if(SCALING_TYPE == LOG) {
		TI_EigenVector_t vecMax = H.colwise().maxCoeff();
		if(SCALING_THRESHOLD >= 1. || vecMax.minCoeff() < SCALING_THRESHOLD) {
			if(nrm.size() == 0) {
				nrm = TI_EigenVector_t::Zero(vecMax.size());
			}
			for(size_t iC=0; iC<(size_t)vecMax.size(); ++iC){
				if(vecMax(iC) > 0 && (SCALING_THRESHOLD >= 1. || vecMax(iC) < SCALING_THRESHOLD)){
					int exponent;
					frexp(vecMax(iC), &exponent);
					nrm(iC) += exponent*LOG_OF_2;
					H.col(iC) /= ldexp((float)1., exponent); // TODO CHECK IF CORRECT
				}
			}
		}
	}

}

bool CPVNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		bmNode = dynamic_cast<BranchMatrixNode*>(aChild);
		if(bmNode->getIDClass() != idClass){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found CLASS_" << bmNode->getIDClass();
			std::cerr << " while waiting for CLASS_" << idClass << std::endl;
			abort();
		}
	} else if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		addChild(leafNode);
	}
}

void CPVNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		removeChild(leafNode);
	}
}


} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
