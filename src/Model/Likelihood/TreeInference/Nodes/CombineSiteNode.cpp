//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "CombineSiteNode.h"
#include "Types.h"

extern "C" {
#include "Utils/Code/icsilog.h"
}

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

const bool CombineSiteNode::USE_FAST_LOG = false;
const int CombineSiteNode::LOG_N_MANTISSA = 12;
const TI_TYPE CombineSiteNode::MIN_PROBABILITY = 1e-300;
const TI_TYPE CombineSiteNode::LOG_MIN = log(std::numeric_limits<TI_TYPE>::min());
const TI_TYPE CombineSiteNode::WORST_VAL = -std::numeric_limits<TI_TYPE>::max();

CombineSiteNode::CombineSiteNode(const size_t aNClass, const std::vector<size_t> &aSites,
		 DL::CompressedAlignements &ca) :
		DAG::BaseNode(), nClass(aNClass), nSubSite(aSites.size()),
		rootNodes(nClass, NULL), sites(aSites), compressedAligns(ca), factors(nSubSite, 1) {
	mScaling = NULL;

	for(size_t i=0; i<nSubSite; ++i) {
		factors(i) = ca.getSiteFactor(sites[i]);
	}

	LOOKUP_TABLE = new float[((int) pow(2.,LOG_N_MANTISSA))]; //memory allocation for the table
	fill_icsi_log_table2(LOG_N_MANTISSA,LOOKUP_TABLE); //fill the table

}

CombineSiteNode::~CombineSiteNode() {
	delete [] LOOKUP_TABLE;
}

std::string CombineSiteNode::toString() const {
	std::stringstream ss;
	ss << BaseNode::toString();
	/*for(size_t i=0; i<vecSSLik.size(); ++i) {
		ss << i << " :: ";
		for(size_t j=0; j<nClass; ++j) {
			ss << vecSSLik[i].idxSLik[j] << " | " << vecSSLik[i].ptrNode[j]->getIDClass() << ", ";
		}
		ss << std::endl;
	}*/
	return ss.str();
}


void CombineSiteNode::combineSite() {
	const Proportions &prop = mScaling->getProportions();

	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(USE_FAST_LOG) {
		if(nClass == 1) {
			TI_EigenArray_t siteLik = rootNodes[0]->getLikelihood();
			for(size_t i=0; i<(size_t)siteLik.size(); ++i) {
				float tmp = siteLik(i);
				if(siteLik(i) <= MIN_PROBABILITY) {
					siteLik(i) = WORST_VAL;
				} else {
					siteLik(i) = icsi_log_v2(tmp,LOOKUP_TABLE, LOG_N_MANTISSA);
				}
			}
			TI_EigenArray_t scaledSiteLik = siteLik.array()*factors;
			likelihood = scaledSiteLik.sum();
		} else {
			TI_EigenVector_t siteLik = prop.getProportion(0) * rootNodes[0]->getLikelihood();
			for(size_t iC=1; iC<nClass; ++iC) {
				double p = prop.getProportion(iC);
				siteLik = siteLik + p * rootNodes[iC]->getLikelihood();
			}
			for(size_t i=0; i<(size_t)siteLik.size(); ++i) {
				float tmp = siteLik(i);
				if(siteLik(i) <= MIN_PROBABILITY) {
					siteLik(i) = WORST_VAL;
				} else {
					siteLik(i) = icsi_log_v2(tmp,LOOKUP_TABLE, LOG_N_MANTISSA);
				}
			}
			TI_EigenArray_t scaledSiteLik = siteLik.array()*factors;
			likelihood = scaledSiteLik.sum();
		}
	} else {
		if(nClass == 1) {
			TI_EigenArray_t scaledSiteLik = rootNodes[0]->getLikelihood().array().log()*factors;
			for(size_t i=0; i<(size_t)scaledSiteLik.size(); ++i) {
				if(scaledSiteLik(i) == -std::numeric_limits<TI_TYPE>::infinity()) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
				} else if(rootNodes[0]->getLikelihood()(i) < MIN_PROBABILITY) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
 				}
			}
			likelihood = scaledSiteLik.sum();
		} else {
			TI_EigenVector_t siteLik = prop.getProportion(0) * rootNodes[0]->getLikelihood();
			for(size_t iC=1; iC<nClass; ++iC) {
				double p = prop.getProportion(iC);
				siteLik = siteLik + p * rootNodes[iC]->getLikelihood();
			}
			TI_EigenArray_t scaledSiteLik = siteLik.array().log()*factors;
			for(size_t i=0; i<(size_t)scaledSiteLik.size(); ++i) {
				if(scaledSiteLik(i) == -std::numeric_limits<TI_TYPE>::infinity()) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
				} else if(rootNodes[0]->getLikelihood()(i) < MIN_PROBABILITY) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
 				}
			}
			likelihood = scaledSiteLik.sum();
		}
	}
}

void CombineSiteNode::combineSiteWithLogScaling() {
	const Proportions &prop = mScaling->getProportions();

	// LOG_LIKELIHOOD
	likelihood = 0.;

	if(USE_FAST_LOG) {
		if(nClass == 1) {
			const TI_EigenVector_t &logNorm = rootNodes[0]->getNorm();
			TI_EigenArray_t siteLik = rootNodes[0]->getLikelihood();
			for(size_t i=0; i<(size_t)siteLik.size(); ++i) {
				TI_TYPE tmp = siteLik(i);
				if(siteLik(i) <= MIN_PROBABILITY) {
					siteLik(i) = MIN_PROBABILITY;
				} else {
					siteLik(i) = icsi_log_v2(tmp,LOOKUP_TABLE, LOG_N_MANTISSA) + logNorm(i);
				}
			}
			TI_EigenArray_t scaledSiteLik = siteLik.array()*factors;
			likelihood = scaledSiteLik.sum();
		} else {
			for(size_t iS=0; iS<nSubSite; ++iS) {
				TI_TYPE logLikSite = 0;
				for(size_t iC=0; iC<nClass; ++iC) {
					const TI_EigenVector_t &logNorm = rootNodes[iC]->getNorm();
					const TI_EigenVector_t &subLik = rootNodes[iC]->getLikelihood();

					TI_TYPE tmpSublik = subLik(iS) >= MIN_PROBABILITY ? subLik(iS) : 0.;
					tmpSublik *= prop.getProportion(iC);
					TI_TYPE tmpLogLik = icsi_log_v2(tmpSublik, LOOKUP_TABLE, LOG_N_MANTISSA) + logNorm(iS);

					if(iC == 0) {
						logLikSite = tmpLogLik;
					} else {
						TI_TYPE x = logLikSite;
						TI_TYPE y = tmpLogLik;
						if(x<=y) std::swap(x,y);
						logLikSite = x + icsi_log_v2(1.+exp(y-x), LOOKUP_TABLE, LOG_N_MANTISSA);
					}
				}

				// Combine likelihood at site level
				TI_TYPE factor = compressedAligns.getSiteFactor(sites[iS]);
				likelihood += factor*logLikSite;
			}
		}
	} else {
		if(nClass == 1) {
			const TI_EigenVector_t &logNorm = rootNodes[0]->getNorm();
			TI_EigenArray_t scaledSiteLik = (rootNodes[0]->getLikelihood().array().log() + logNorm.array())*factors;
			for(size_t i=0; i<(size_t)scaledSiteLik.size(); ++i) {
				if(scaledSiteLik(i) == -std::numeric_limits<TI_TYPE>::infinity()) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
				} else if(rootNodes[0]->getLikelihood()(i) < MIN_PROBABILITY) {
					scaledSiteLik(i) = factors(i)*WORST_VAL;
 				}
			}
			likelihood = scaledSiteLik.sum();
		} else {
			for(size_t iS=0; iS<nSubSite; ++iS) {
				TI_TYPE logLikSite = 0;
				for(size_t iC=0; iC<nClass; ++iC) {
					const TI_EigenVector_t &logNorm = rootNodes[iC]->getNorm();
					const TI_EigenVector_t &subLik = rootNodes[iC]->getLikelihood();

					TI_TYPE tmpSublik = subLik(iS) >= MIN_PROBABILITY ? subLik(iS) : 0.;
					tmpSublik *= prop.getProportion(iC);
					TI_TYPE tmpLogLik = log(tmpSublik) + logNorm(iS);

					if(iC == 0) {
						logLikSite = tmpLogLik;
					} else {
						TI_TYPE x = logLikSite;
						TI_TYPE y = tmpLogLik;
						if(x<=y) std::swap(x,y);
						logLikSite = x + log(1.+exp(y-x));
					}
				}

				// Combine likelihood at site level
				TI_TYPE factor = compressedAligns.getSiteFactor(sites[iS]);
				likelihood += factor*logLikSite;
			}
		}
	}
}


void CombineSiteNode::doProcessing() {

	if(EdgeNode::SCALING_TYPE == EdgeNode::LOG) {
		combineSiteWithLogScaling();
	} else {
		combineSite();
	}
}

bool CombineSiteNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootNode)){
		RootNode *node = dynamic_cast<RootNode*>(aChild);
		const size_t iC = node->getIDClass();
		rootNodes[iC] = node;
	} else if(childtype == typeid(MatrixScalingNode)){
		mScaling = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}


} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
