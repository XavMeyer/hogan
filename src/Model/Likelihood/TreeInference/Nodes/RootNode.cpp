//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "RootNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

RootNode::RootNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies) :
	 			 ParentNode(aIDClass, aSitePos, aFrequencies) {


	nrm.resize(H.cols());
	H.resize(4,0);
}

RootNode::~RootNode() {
}

const TI_EigenVector_t& RootNode::getLikelihood() const {
	return likelihood;
}



void RootNode::doProcessing() {

	// FIXME std::cout << "ROOT [ " << getId() << "] :: H(" << H.rows() << "x" << H.cols() << ") :: G(" << G.rows() << "x" << G.cols() << ")" << std::endl;
	TI_EigenMatrix_t G;
	nrm.setZero();

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			first = false;
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		}
	}

#if TI_USE_FLOAT
	likelihood = frequencies.getEigenFlt().transpose() * G;
#else
	//likelihood = G.transpose()*frequencies.getEigen();
	likelihood = frequencies.getEigen().transpose() * G;
#endif

	//FIXME std::cout << "ROOT [ " << getId() << "] done " << std::endl;

}

bool RootNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		addChild(leafNode);
	}
}

void RootNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		removeChild(leafNode);
	}
}

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
