//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParentNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef PARENTNODE_TREEINFERENCE_H_
#define PARENTNODE_TREEINFERENCE_H_

#include "EdgeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

class ParentNode : public EdgeNode {
public:
	ParentNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
			   DL_Utils::Frequencies &aFrequencies);
	virtual ~ParentNode();

	std::string toString() const;

protected: // Variables

	std::vector<EdgeNode*> children;

protected: // Methods
	void addChild(EdgeNode* childNode);
	bool removeChild(EdgeNode* childNode);

};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PARENTNODE_TREEINFERENCE_H_ */
