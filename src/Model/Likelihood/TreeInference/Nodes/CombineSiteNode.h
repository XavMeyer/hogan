//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINESITENODE_TREEINFERENCE_H_
#define COMBINESITENODE_TREEINFERENCE_H_

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"
#include "MatrixScalingNode.h"
#include "RootNode.h"

#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

#include "Types.h"
#include <vector>

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace DL = ::MolecularEvolution::DataLoader;

class CombineSiteNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	CombineSiteNode(const size_t aNClass, const std::vector<size_t> &aSites, DL::CompressedAlignements &ca);
	~CombineSiteNode();

	std::string toString() const;

private:

	struct SiteSubLikelihood {
		size_t site;
		std::vector<RootNode*> ptrNode;
		SiteSubLikelihood(const size_t aNClass, const size_t aSite) :
						  site(aSite), ptrNode(aNClass) {
			for(size_t iC=0; iC<aNClass; ++iC){ptrNode[iC] = NULL;}
		}
		~SiteSubLikelihood(){}
		bool operator== (const SiteSubLikelihood &aPLS) const{
			return aPLS.site == site;
		}
	};

	typedef std::vector<SiteSubLikelihood> vecSSLik_t;
	typedef vecSSLik_t::iterator itVecPtrLS_t;

	static const bool USE_FAST_LOG;
	static const int LOG_N_MANTISSA;
	static const TI_TYPE MIN_PROBABILITY;
	static const TI_TYPE LOG_MIN, WORST_VAL;

	const size_t nClass, nSubSite;
	MatrixScalingNode *mScaling;
	std::vector<RootNode*> rootNodes;
	std::vector<size_t> sites;
	DL::CompressedAlignements &compressedAligns;

	TI_EigenArray_t factors;
	float *LOOKUP_TABLE;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void combineSite();
	void combineSiteWithLogScaling();

};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINESITENODE_TREEINFERENCE_H_ */
