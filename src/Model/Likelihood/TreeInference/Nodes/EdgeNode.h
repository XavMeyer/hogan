//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef EDGENODE_TREEINFERENCE_H_
#define EDGENODE_TREEINFERENCE_H_

#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

#include "DAG/Node/Base/BaseNode.h"
#include "BranchMatrixNode.h"
#include "Types.h"

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class EdgeNode : public DAG::BaseNode {
public:
public:
	EdgeNode(const size_t aIdClass, const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies);
	virtual ~EdgeNode();

	const TI_EigenMatrix_t& getH() const;
	const TI_EigenVector_t& getNorm() const;
	size_t getIDClass() const;

public:
	enum scaling_t {NONE, LOG};
	static const scaling_t SCALING_TYPE;
	static const float SCALING_THRESHOLD;

protected:

	const size_t idClass;
	DL_Utils::Frequencies &frequencies;
	BranchMatrixNode *bmNode;
	TI_EigenMatrix_t H;
	TI_EigenVector_t nrm;

	void init(const std::vector<size_t> &sitePos);
};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* EDGENODE_TREEINFERENCE_H_ */
