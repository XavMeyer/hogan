//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "LeafNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

LeafNode::LeafNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				   const std::string &aSpecieName, const DL::CompressedAlignements& ca,
				   DL_Utils::Frequencies &aFrequencies) :
						   EdgeNode(aIDClass, aSitePos, aFrequencies) {

	init(aSpecieName, ca, aSitePos);
}

LeafNode::~LeafNode() {
}

void LeafNode::init(const std::string &aSpecieName, const DL::CompressedAlignements& ca,
		const std::vector<size_t> &aSitePos) {

	if(frequencies.size() != 4) {
		G.resize(frequencies.size(), aSitePos.size());
		G.setZero();
		for(size_t iS=0; iS<aSitePos.size(); ++iS) {
			DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, aSitePos[iS]);

			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				G(cdnVals[iCod], iS) = 1.0;
			}
		}
	} else {
		G.resize(frequencies.size(), 4*4);
		G.setZero();
		for(size_t iC=0; iC<(size_t)G.cols(); ++iC) {
			for(size_t iB=0; iB<4; ++iB) {
				G(iB, iC) = (iC >> iB) & 1;
			}
		}
		for(size_t iS=0; iS<aSitePos.size(); ++iS) {
			DL::Alignment::vecCode_t cdnVals = ca.getSiteCode(aSpecieName, aSitePos[iS]);

			unsigned short int code = 0;
			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				code = code | 1 << cdnVals[iCod];
			}
			siteCode.push_back(code);
		}
	}
	//getchar();
}

void LeafNode::doProcessing() {

	const TI_EigenSquareMatrix_t &Z = bmNode->getZ();
#if TI_USE_FLOAT
	const TI_EigenVector_t &invPi = frequencies.getInvEigenFlt();
#else
	const TI_EigenVector_t &invPi = frequencies.getInvEigen();
#endif


	// Multiplication
	if(frequencies.size() != 4) {
		H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);
	} else {
		TI_EigenMatrix_t tmp = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);
		for(size_t iS=0; iS<siteCode.size(); ++iS){
			H.col(iS) = tmp.col(siteCode[iS]);
		}
	}

	//FIXME std::cout << "Leaf[" << getId() << "] bl = " << bmNode->getBranchLength() << std::endl << H << std::endl;
}

bool LeafNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		bmNode = dynamic_cast<BranchMatrixNode*>(aChild);
		if(bmNode->getIDClass() != idClass){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found CLASS_" << bmNode->getIDClass();
			std::cerr << " while waiting for CLASS_" <<  idClass << std::endl;
			abort();
		}
	}
}

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
