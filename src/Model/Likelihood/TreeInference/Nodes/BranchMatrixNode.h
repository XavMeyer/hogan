//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixNode.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef BRANCHMATRIXNODE_TREEINFERENCE_H_
#define BRANCHMATRIXNODE_TREEINFERENCE_H_

#include "DAG/Node/Base/BaseNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

#include "Types.h"
#include "MatrixNode.h"
#include "MatrixScalingNode.h"

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class BranchMatrixNode: public DAG::BaseNode {
public:
	BranchMatrixNode(const size_t aIdClass, const scalingType_t aScalingType, DL_Utils::Frequencies &aFrequencies);
	~BranchMatrixNode();

	void setBranchLength(const double aBL);
	double getBranchLength() const;

	size_t getIDClass() const;

	const TI_EigenSquareMatrix_t& getZ() const;

private:

	const size_t idClass;
	scalingType_t scalingType;
	double branchLength;
	DL_Utils::Frequencies &frequencies;

	MatrixNode *matrixNode;
	MatrixScalingNode *scalingNode;

	TI_EigenSquareMatrix_t Z;


	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BRANCHMATRIXNODE_TREEINFERENCE_H_ */
