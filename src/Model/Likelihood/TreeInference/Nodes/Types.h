//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef TYPES_TREEINFERENCE_H_
#define TYPES_TREEINFERENCE_H_

#include <stddef.h>
#include <assert.h>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

#define TI_NSYMBOLS 			4
#define TI_USE_FLOAT 			1
#if TI_USE_FLOAT
typedef Eigen::Matrix<float, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenSquareMatrix_t;
typedef Eigen::Matrix<float, TI_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_EigenMatrix_t;
typedef Eigen::MatrixXf TI_EigenMatrixDyn_t;
typedef Eigen::VectorXf TI_EigenVector_t;
typedef Eigen::ArrayXf TI_EigenArray_t;
#define TI_TYPE					float
#else
typedef Eigen::Matrix<double, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenSquareMatrix_t;
typedef Eigen::Matrix<double, TI_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_EigenMatrix_t;
typedef Eigen::MatrixXd TI_EigenMatrixDyn_t;
typedef Eigen::VectorXd TI_EigenVector_t;
typedef Eigen::ArrayXd TI_EigenArray_t;
#define TI_TYPE					double
#endif

typedef Eigen::VectorXd TI_EigenEIGVector_t;
typedef Eigen::ArrayXd TI_EigenEIGArray_t;
typedef Eigen::Matrix<double, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenEIGMatrix_t;

	static const TI_TYPE LOG_OF_2 = log(2.);

    enum scalingType_t {NONE=0, LOCAL=1, GLOBAL=2};
    enum modelType_t {M0=0, M1=1, M2=2, M1a=3, M2a=4, NOT_CODON=5};

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_TREEINFERENCE_H_ */
