//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "EdgeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

const EdgeNode::scaling_t EdgeNode::SCALING_TYPE = LOG;
const float EdgeNode::SCALING_THRESHOLD = 1.; // Scale all with 1.


EdgeNode::EdgeNode(const size_t aIDClass, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), idClass(aIDClass), frequencies(aFrequencies)  {

	bmNode = NULL;
	init(aSitePos);
}

EdgeNode::~EdgeNode() {
}


void EdgeNode::init(const std::vector<size_t> &sitePos) {
	// Create vectors
	H.resize(frequencies.size(), sitePos.size());

	if(SCALING_TYPE == LOG) {
		nrm.resize(0);//nrm.resize(sitePos.size());
	}
}


const TI_EigenMatrix_t& EdgeNode::getH() const {
	return H;
}

const TI_EigenVector_t& EdgeNode::getNorm() const {
	return nrm;
}

size_t EdgeNode::getIDClass() const {
	return idClass;
}


} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
