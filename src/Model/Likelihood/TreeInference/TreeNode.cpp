//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "TreeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

TreeNode::TreeNode(const size_t aNClass, const std::string aName, const TR::TreeNode *aTreeNodeTR) :
	nClass(aNClass), branchMDAG(nClass, NULL) {
	id = aTreeNodeTR->getId();
	name = aName;
	branchLength = 0.;

	leaf = false;

	requireReinitDAG = false;
}

TreeNode::~TreeNode() {
}


void TreeNode::addChild(TreeNode *child) {
	children.push_back(child);
	//sortChildren();
}

bool TreeNode::removeChild(TreeNode *child) {
	std::vector<TreeNode*>::iterator position = std::find(children.begin(), children.end(), child);
	if (position != children.end()) {
		children.erase(position);
		//sortChildren();
		return true;
	}
	return false;
}

bool TreeNode::isParent(const TreeNode *aChild) const {
	std::vector<TreeNode*>::const_iterator position = std::find(children.begin(), children.end(), aChild);
	return position != children.end();
}

bool TreeNode::isChild(const TreeNode *aParent) const {
	return aParent->isParent(this);
}

TreeNode* TreeNode::getChild(size_t id) {
	return children[id];
}


treeNodeTI_children& TreeNode::getChildren() {
	return children;
}

const treeNodeTI_children& TreeNode::getChildren() const {
	return children;
}

const std::string& TreeNode::getName() const {
	return name;
}

size_t TreeNode::getId() const {
	return id;
}


void TreeNode::addBranchMatrixNode(BranchMatrixNode *node) {
	branchMDAG[node->getIDClass()] = node;
}

size_t TreeNode::getNumberEdgeNode() const {
	return treeStructDAG.size();
}

void TreeNode::addEdgeNode(EdgeNode *node) {
	treeStructDAG.push_back(node);
}

EdgeNode* TreeNode::getEdgeNode(size_t id) {
	return treeStructDAG[id];
}

BranchMatrixNode* TreeNode::getBranchMatrixNode(size_t aIDClass) {
	assert(aIDClass < nClass);
	return branchMDAG[aIDClass];
}

void TreeNode::setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes) {
	if(branchLength != aBL) {
		// FIXME
		//std::stringstream ss;
		//ss << "[" << Parallel::mcmcMgr().getRankProposal() << "] upd : " << branchLength << " -> " << aBL << " (" << branchMDAG.size() << " -> " ;
		branchLength = aBL;

		for(size_t i=0; i<branchMDAG.size(); ++i){
			branchMDAG[i]->setBranchLength(branchLength);
			updatedNodes.insert(branchMDAG[i]);
			//ss << branchMDAG[i]->getId() << ", ";
		}
		//ss << std::endl;
		//std::cout << ss.str();
	}
}

double TreeNode::getBranchLength() const {
	return branchLength;
}

void TreeNode::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNode::isLeaf() const {
	return leaf;
}

void TreeNode::setRequireReinitDAG(const bool aRequire) {
	requireReinitDAG = aRequire;
}

bool TreeNode::doRequireReinitDAG() const {
	return requireReinitDAG;
}


const std::string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << branchLength;
	if(leaf) ss << " - isLeaf";
	ss << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]" << children[i]->name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNode::deleteChildren() {
	while(!children.empty()) {
		TreeNode *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string TreeNode::buildNewick() const {
	std::string nwkStr;
	addNodeToNewick(nwkStr);
	return nwkStr;
}

void TreeNode::addNodeToNewick(std::string &nwk) const {
	if(!children.empty()) {
		nwk.push_back('(');
		children[0]->addNodeToNewick(nwk);

		for(size_t iC=1; iC<children.size(); ++iC) {
			nwk.push_back(',');
			nwk.push_back(' ');
			children[iC]->addNodeToNewick(nwk);
		}
		nwk.push_back(')');
	}

	nwk.append(name);
	nwk.push_back(':');
	std::stringstream ss;
	ss << branchLength;
	nwk.append(ss.str());
}

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
