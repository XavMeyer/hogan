//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.cpp
 *
 * @date Nov 9, 2015
 * @author meyerx
 * @brief
 */
#include "Base.h"

namespace StatisticalModel {
namespace Likelihood {
namespace TreeInference {

const std::string Base::NAME_TREE_PARAMETER = "TreeHash";
const Base::debug_t Base::doDebug = DISABLE_DEBUG;

Base::Base(const scalingType_t aScalingType, const modelType_t aModelType,
		   const size_t aNThread, const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
		   DL::NewickParser *aNP, const std::string &aFileAlign, treeUpdateStrategy_t aStrat) :
				scalingType(aScalingType), modelType(aModelType),
				nClass(defineNClass()), nThread(aNThread),
				nuclModel(aNuclModel), cFreqType(aCFreqType), fileAlign(aFileAlign),
				TREE_UPDATE_STRATEGY(aStrat), frequencies(defineNSymbols())  {

	if(doDebug != DISABLE_DEBUG) {
		std::stringstream ss;
		ss << "dbgFile_" << Parallel::mcmcMgr().getRankProposal() << "_" << Parallel::mcmcMgr().getNProposal() << "P.txt";
		dbgFile.open(ss.str().c_str());
	}

	init(aNP);

	MPI_Barrier(MPI_COMM_WORLD);
}

Base::~Base() {
	//rootDAG->deleteChildren();
	//delete rootDAG;

	cleanTreeAndDAG();
	delete scheduler;
}

DL::MSA::sequence_t Base::defineSeqType() const {
	if(modelType == NOT_CODON) {
		return DL::MSA::NUCLEOTIDE;
	} else {
		return DL::MSA::CODON;
	}
}

size_t Base::defineNSymbols() const {
	if(modelType == NOT_CODON) {
		return MD::Nucleotides::NB_BASE_NUCL;
	} else {
		return MD::Codons::NB_CODONS_WO_STOP;
	}
}

std::vector<double> Base::defineFrequency(DL::MSA &msa) const {
	if(modelType == NOT_CODON) {
		return DL::NucleotideFrequencies(msa).getUniform();
		//return DL::NucleotideFrequencies(msa).getEmpiric(false);
	} else {
		return DL::CodonFrequencies(msa).getCodonFrequency(cFreqType);
	}
}

size_t Base::defineNClass() const {
	if(modelType == NOT_CODON) {
		return 1;
	} else {
		if(modelType == M0) {
			return 1;
		} else if (modelType == M1 || modelType == M1a) {
			return 2;
		} else if (modelType == M2 || modelType == M2a) {
			return 3;
		}
	}
	return 0;
}

void Base::init(DL::NewickParser *aNP) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	isTreeExternalyImported = false;

	DL::FastaReader fr(fileAlign);
	DL::MSA msa(defineSeqType(), fr.getAlignments(), false);
	//std::cout << "Nb position with only gaps : " << msa.countAllGaps() << std::endl;
	DL::CompressedAlignements compressedAligns(msa, false, false);
	frequencies.set(defineFrequency(msa));

	std::vector<std::string> orderedTaxaNames = getOrderedTaxaNames(msa);
	treeManager.reset(new TR::TreeManager(orderedTaxaNames));

	if(aNP != NULL) {
		curTree.reset(new TR::Tree(aNP->getRoot(), treeManager->getNameMapping()));
	} else {
		curTree.reset(new TR::Tree(true, msa, treeManager->getNameMapping()));
	}
	//treePool->addTree(curTree, false);
	treeManager->setTree(curTree);
	treeManager->memorizeTree();
	curHash = curTree->getHashKey();

	//nSite = msa.getNValidSite();
	nSite = compressedAligns.getNSite();

	prepareDAG();
	createTreeAndDAG(fr, msa, compressedAligns);

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << getCompressionReport(msa, compressedAligns);
		std::cout << "**************************************" << std::endl;
	}

	cntErrMsg = 0;
	if(nThread > 1) {
		Eigen::initParallel();
		//scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	for(size_t iC=0; iC<nClass; ++iC) {
		std::stringstream ss;
		ss << "Prop_" << iC;
		markerNames.push_back(ss.str());
	}

	if(nClass > 1) {
		ptrDirichletPr.reset(new StatisticalModel::DirichletPDF(nClass, 1.));
		markerNames.push_back("PriorProportion");
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<6; ++iT) {
			std::stringstream ss;
			ss << "NormTheta_" << iT;
			markerNames.push_back(ss.str());
		}
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		ptrGTRPr.reset(new StatisticalModel::DirichletPDF(MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF, 1.));
		markerNames.push_back("PriorGTR");
	}

	markerNames.push_back("Total_BL");

	markers.resize(markerNames.size());

}

double Base::processLikelihood(const Sampler::Sample &sample) {

	// Update the values
	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(nClass, getNBranch(), modelType, nuclModel, sample.getIntValues(), sample.getDblValues());
	setSample(sw, updatedNodes);

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	for(size_t iC=0; iC<nClass; ++iC) {
		markers[iC] = scalingMat->getProportions().getProportion(iC);
	}

	/*std::cout << rootDAG->getLikelihood() << std::endl;
	getchar();*/

	double prior = 0;
	if(nClass > 1) {
		std::vector<double> props;
		for(size_t iC=0; iC<nClass; ++iC) {
			props.push_back(markers[iC]);
		}
		prior = log(ptrDirichletPr->computePDF(props));

		markers[nClass] = prior;
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		size_t offset = nClass > 1 ? nClass+1 : nClass;

		double sum = sw.thetas.size() == 5 ? 1. : 0.;
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			sum += sw.thetas[iT];
		}
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			std::stringstream ss;
			ss << "NormTheta_" << iT;
			markers[offset+iT] = sw.thetas[iT]/sum;
		}
		if(sw.thetas.size() == 5) {
			markers[offset+5] = 1/sum;
		}
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		double priorGTR = log(ptrGTRPr->computePDF(sw.thetas));

		markers[markers.size()-2] = priorGTR;
		prior += priorGTR;
	}

	markers.back() = getTotalTreeLength(sw);

	double lik = rootDAG->getLikelihood();

	// return the likelihood
	return prior + lik;
}

std::string Base::getName(char sep) const {
	return std::string("TreeInference");
}

size_t Base::getNBranch() const {
	return branchPtr.size();
}

nuclModel_t Base::getNucleotideModel() const {
	return nuclModel;
}

codonFreq_t Base::getCodonFrequencyType() const {
	return cFreqType;
}

size_t Base::getNClass() const {
	return nClass;
}

modelType_t Base::getModelType() const {
	return modelType;
}

scalingType_t Base::getScalingType() const {
	return scalingType;
}

size_t Base::stateSize() const {
	size_t size = 0;
	/*for(size_t i=0; i<nClass; ++i) {
		size += ptrQs[i]->serializedSize();
	}*/
	return size;
}

void Base::setStateLH(const char* aState) {
	// TODO DO NOT FORGET TO UPDATE THE UPDATEDNODES LIST FOR PARTIAL SCHEDULER UPDATE
	/*size_t offset = 0;
	for(size_t i=0; i<nClass; ++i) {
		ptrQs[i]->serializeFromBuffer(aState+offset);
		offset += ptrQs[i]->serializedSize();
	}*/
}

void Base::getStateLH(char* aState) const {
	/*size_t offset = 0;
	for(size_t i=0; i<nClass; ++i) {
		ptrQs[i]->serializeToBuffer(aState+offset);
		offset += ptrQs[i]->serializedSize();
	}*/
}

double Base::update(const vector<size_t>& pIndices, const Sample& sample) {

	volatile double sSample, eSample, sDAG, eDAG, sProc, eProc;

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Indices : ";
		for(size_t i=0; i<pIndices.size(); ++i) {
			dbgFile << pIndices[i] << ", ";
		}
		dbgFile << std::endl;
		sSample = MPI_Wtime();
	}

	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(nClass, getNBranch(), modelType, nuclModel, sample.getIntValues(), sample.getDblValues());
	setSample(sw, updatedNodes);

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eSample = MPI_Wtime();
		sDAG = MPI_Wtime();
	}

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial(updatedNodes) && (!pIndices.empty()) && (pIndices.front() != 0)) { // its possible that the tree is the same
			if(cntErrMsg < 1000) {
				std::cerr << "[" << Parallel::mcmcMgr().getMyChainMC3() << "][" << Parallel::mcmcMgr().getRankProposal();
				std::cerr <<  "] First pInd = " << pIndices.front();
				cntErrMsg++;
				if(cntErrMsg == 1000) {
					std::cerr << " || no more warning reporting (cnt error > 1000)...";
				}
				std::cerr << std::endl;
			}
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eDAG = MPI_Wtime();
		sProc = MPI_Wtime();
	}

	// Process the tree
	scheduler->process();

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eProc = MPI_Wtime();
	}

	for(size_t iC=0; iC<nClass; ++iC) {
		markers[iC] = scalingMat->getProportions().getProportion(iC);
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Update::setSample : " << eSample -sSample << std::endl;
		dbgFile << "Update::resetPartial : " << eDAG - sDAG << std::endl;
		dbgFile << "Update::process : " << eProc - sProc << std::endl;
		dbgFile << "----------------------------------------------" << std::endl;
	}

	double prior = 0;
	if(nClass > 1) {
		std::vector<double> props;
		for(size_t iC=0; iC<nClass; ++iC) {
			props.push_back(markers[iC]);
		}
		prior = log(ptrDirichletPr->computePDF(props));

		markers[nClass] = prior;
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		size_t offset = nClass > 1 ? nClass+1 : nClass;

		double sum = sw.thetas.size() == 5 ? 1. : 0.;
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			sum += sw.thetas[iT];
		}
		for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
			markers[offset+iT] = sw.thetas[iT]/sum;
		}
		if(sw.thetas.size() == 5) {
			markers[offset+5] = 1/sum;
		}
	}

	if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
		double priorGTR = log(ptrGTRPr->computePDF(sw.thetas));

		markers[markers.size()-2] = priorGTR;
		prior += priorGTR;
	}

	markers.back() = getTotalTreeLength(sw);

	double lik = rootDAG->getLikelihood();

	// return the likelihood
	return prior + lik;
}

void Base::saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff) {
	// Serialize the tree to the other chain (get the good tree)
	TR::Tree::sharedPtr_t aTree = treeManager->getCurrentTree();
	if(sample.intValues.front() != aTree->getHashKey()) {
		aTree = treeManager->getTree(sample.intValues.front());
	}
	// Serialize it
	std::string aString = aTree->getIdString();
	std::copy(aString.begin(), aString.end(), std::back_inserter(buff));
}

void Base::loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff) {

	// Get treeString
	std::string aString;
	std::copy(buff.begin(), buff.end(), std::back_inserter(aString));

	// Create tree
	TR::Tree::sharedPtr_t aTree(new TR::Tree(aString));

	// Add it to the pool
	treeManager->setTree(aTree);
	treeManager->memorizeTree();
	isTreeExternalyImported = true;
}


void Base::initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, std::streampos offset) {
	//if(TREE_UPDATE_STRATEGY != FIXED_TREE_STRATEGY) {
		treeManager->initTreeFile(fnPrefix, fnSuffix, offset);
	//}
}

std::streampos Base::getCustomizedLogSize() const {
	//if(TREE_UPDATE_STRATEGY != FIXED_TREE_STRATEGY) {
		return treeManager->getTreeFileLength();
	/*} else {
		return 0;
	}*/
}

void Base::writeCustomizedLog(const std::vector<Sample> &samples) {
	//if(TREE_UPDATE_STRATEGY != FIXED_TREE_STRATEGY) {
		// Keep track of trees hash
		std::vector<long int> treesHash;
		for(size_t iS=0; iS<samples.size(); ++iS) {
			treesHash.push_back(samples[iS].intValues.front());
		}

		treeManager->writeTreeString(treesHash);
		/*std::vector<size_t> STS = curTree->getRoot()->getSubTreesSize(NULL);// FIXME
		std::cout << "Subtree sizes : ";
		std::copy(STS.begin(), STS.end(), std::ostream_iterator<size_t>(std::cout, ", "));
		std::cout << std::endl;
		curTree->defineBalancingRoot();*/
	//}
}

std::vector<DAG::BaseNode*> Base::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	std::set<DAG::BaseNode*> updatedNodes;
	SampleWrapper sw(nClass, getNBranch(), modelType, nuclModel, sample.getIntValues(), sample.getDblValues());
	setSample(sw, updatedNodes);

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

size_t Base::getNThread() const {
	return nThread;
}

std::string Base::getAlignFile() const {
	return fileAlign;
}

TR::Tree::sharedPtr_t Base::getCurrentTree() const {
	return curTree;
}

TR::TreeManager::sharedPtr_t Base::getTreeManager() const {
	return treeManager;
}

std::string Base::getCompressionReport(DL::MSA &msa, DL::CompressedAlignements &compressedAligns) const {
	std::stringstream ss;
	ss << "Total number of site : " << msa.getNSite() << std::endl;
	ss << "Number of unique site : " << compressedAligns.getNSite() << std::endl;

	return ss.str();
}

Base::treeUpdateStrategy_t Base::getTreeUpdateStrategy() const {
	return TREE_UPDATE_STRATEGY;
}

void Base::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

DAG::Scheduler::BaseScheduler* Base::getScheduler() const {
	return scheduler;
}

void Base::prepareDAG() {

	using MolecularEvolution::MatrixUtils::MatrixFactory;
	using MolecularEvolution::MatrixUtils::SingleOmegaMF;
	using MolecularEvolution::MatrixUtils::GTR_MF;
	using MolecularEvolution::MatrixUtils::HKY85_MF;

	// Create Matrix Scaling and proportions node
	scalingMat = new MatrixScalingNode(nClass);

	ptrQs.assign(nClass, NULL);
	for(size_t iC=0; iC<nClass; ++iC) {

		MatrixFactory::sharedPtr_t ptr;
		if(modelType == NOT_CODON) {
			if(nuclModel == MolecularEvolution::MatrixUtils::GTR) {
				ptr.reset(new GTR_MF());
			} else {
				ptr.reset(new HKY85_MF());
			}
		} else {
			ptr.reset(new SingleOmegaMF(nuclModel));
		}

		ptrQs[iC] = new MatrixNode(iC, nuclModel, ptr, frequencies);
		scalingMat->addChild(ptrQs[iC]);

	}

	// Root of the DAG
	rootDAG = new FinalResultNode();
}

void Base::createTreeAndDAG(DL::FastaReader &fr, DL::MSA &msa, DL::CompressedAlignements &compressedAligns) {

	// Reset accumulators
	accComp = accComp_t();
	accTotal = accComp_t();

	// Root
	rootTree = new TreeNode(nClass, treeManager->getTreeNodeName(curTree->getRoot()), curTree->getRoot());
	branchMap.insert(std::make_pair(rootTree->getId(), rootTree));
	createTree(curTree->getRoot(), NULL,  rootTree);
	std::sort(branchPtr.begin(), branchPtr.end(), SortTreeNodePtrById());

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > 1){
		size_t tmpVal = ceil((double)nThread/2.0);
		// TODO OPTIMIZE THAT nProc = 2 with nSubDag = 2 -> nThread = 4 nSubDag = ? etc..
		nSubDAG = std::min(tmpVal, nSite);
	}
	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);

		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteNode* ptrCSN = new CombineSiteNode(nClass, subSites, compressedAligns);
		cmbNodes.push_back(ptrCSN);
		ptrCSN->addChild(scalingMat); // Add scaling mat to CombineSiteNode

		// Create root(s) and subDAG for class iC
		for(size_t iC=0; iC<nClass; ++iC) {
			RootNode *ptrRoot = new RootNode(iC, subSites, frequencies);
			createSubDAG(iC, subSites, rootTree, ptrRoot, compressedAligns); // Create sub DAG for class iC
			rootTree->addEdgeNode(ptrRoot);

			// Add root to the CombineSiteNode
			ptrCSN->addChild(ptrRoot);

			/*std::cout << ptrRoot->toString() << std::endl;
			std::cout << "----------------------------" << std::endl;
			ptrRoot->init();
			std::cout << ptrRoot->toString() << std::endl;
			getchar();*/
		}

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSN);
	}
}


void Base::addBranchMatrixNodes(TreeNode *node) {

	// Add BranchMatrixNodes for each classes
	for(size_t iC=0; iC<nClass; ++iC) {
		BranchMatrixNode *ptrBMN;
		ptrBMN = new BranchMatrixNode(iC, scalingType, frequencies);
		if(scalingType != NONE) {
			ptrBMN->addChild(scalingMat);
		}
		ptrBMN->addChild(ptrQs[iC]);
		node->addBranchMatrixNode(ptrBMN);
	}
}


/**
 * @param nodeTR a node in the TreeReconstruction tree
 * @param node a node in the TreeInference tree
 */
void Base::createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node) {

	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);

	if(childrenTR.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t iC=0; iC<childrenTR.size(); ++iC){
			TreeNode *childNode = new TreeNode(nClass, treeManager->getTreeNodeName(childrenTR[iC]), childrenTR[iC]);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update
			branchMap.insert(std::make_pair(childNode->getId(), childNode));

			createTree(childrenTR[iC], nodeTR, childNode);
			node->addChild(childNode);
			addBranchMatrixNodes(childNode); // Add BranchMatrixNode(s)
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

/**
 * This method create one sub-DAG for a set of sites for a given class.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param idClass the matrix Q class
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 * @param compressedAligns compressed alignements
 */
void Base::createSubDAG(size_t idClass, const std::vector<size_t> &subSites, TreeNode *node, DAG::BaseNode *nodeDAG, DL::CompressedAlignements &compressedAligns) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TreeNode *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafNode *newNode;
			newNode = new LeafNode(idClass, subSites, childNode->getName(), compressedAligns, frequencies);
			childNode->addEdgeNode(newNode);
			newNodeDAG = newNode;
		} else { // create CPV element
			CPVNode *newNode;
			newNode = new CPVNode(idClass, subSites, frequencies);
			childNode->addEdgeNode(newNode);
			newNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(idClass, subSites, childNode, newNode, compressedAligns);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		BranchMatrixNode *ptrBMN = childNode->getBranchMatrixNode(idClass);
		newNodeDAG->addChild(ptrBMN);

	}

}

void Base::setSample(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	volatile double sTime, eTime;
	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setTree(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setTree : " << eTime-sTime << std::endl;
	}

	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	if(nuclModel == MU::GTR) {
		setThetas(sw, updatedNodes);
	} else if(nuclModel == MU::K80) {
		setKappa(sw, updatedNodes);
	} else {
		std::cerr << "Error : void Base::setSample(const SampleWrapper &sw);" << std::endl;
		std::cerr << "Nucleotide model not supported by Light::Base" << std::endl;
		abort();
	}
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setMatrix : " << eTime-sTime << std::endl;
	}


	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	if(modelType != NOT_CODON) {
		setOmegas(sw, updatedNodes);
		setProportions(sw, updatedNodes);
	}
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setOmegasProps : " << eTime-sTime << std::endl;
	}


	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setBranchLength(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setBranchLength : " << eTime-sTime << std::endl;
	}

}

void Base::setTree(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	volatile double sMPI, eMPI;

	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sMPI = MPI_Wtime();
	//treePool->updatePool();
	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
		eMPI = MPI_Wtime();
		dbgFile << "SetTree::UpdatePools : " << eMPI - sMPI << std::endl;
	}

	// FIXME std::cout << curTree->getHashKey() << " -> " << sw.treeH << std::endl;

	if(curHash != sw.treeH || curTree->getHashKey() != sw.treeH) {
		if(TREE_UPDATE_STRATEGY == OPTIMIZED_STRATEGY) {
			volatile double sTree, eTree, sUpd, eUpd, sReinit, eReinit;

			// Find tree
			// FIXME std::cout << "Current tree : " << curTree->getString() << std::endl;
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sTree = MPI_Wtime();
			//std::cout << "Base tree : " << curTree->getIdString() << std::endl; // FIXME
			//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] curTree -> " << curTree->getHashKey() << "\t ";
			//std::cout << " sample -> " << sw.treeH << "\t curHash -> " << curHash << std::endl; // FIXME
			curTree = treeManager->getTree(sw.treeH);
			//std::cout << "New tree : " << curTree->getIdString() << std::endl; // FIXME
			if(curTree == NULL) {
				std::cerr << "[Error] " << sw.treeH << std::endl;
			}
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eTree = MPI_Wtime();
			// FIXME std::cout << "Next tree : " << curTree->getString() << std::endl;

			// Update tree + DAG
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sUpd = MPI_Wtime();
			if(!isTreeExternalyImported) {
				updateTreeAndDAG(updatedNodes);
			} else {
				fullUpdateTreeAndDAG(curTree->getRoot(), NULL,  rootTree, updatedNodes);
				isTreeExternalyImported = false;
			}
			curHash = curTree->getHashKey();
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eUpd = MPI_Wtime();

			// Update scheduler
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sReinit = MPI_Wtime();
			scheduler->reinitialize(false); // only a soft reset
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eReinit = MPI_Wtime();

			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
				dbgFile << "SetTree::getTree : " << eTree - sTree << std::endl;
				dbgFile << "SetTree::UpdateDaG : " << eUpd - sUpd << std::endl;
				dbgFile << "SetTree::ReinitSched : " << eReinit - sReinit << std::endl;
				if(doDebug == TREE_DEBUG) {
					dbgFile << "----------------------------------------------" << std::endl;
				}
			}
		} else if(TREE_UPDATE_STRATEGY == FIXED_TREE_STRATEGY) {
			assert("[Error] Likelihood strategy is 'FIXED_TREE_STRATEGY' and topological moves are happening.");
		}
	}
}

void Base::setKappa(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	//std::cout << "Here " << std::endl;
	for(size_t iC=0; iC<nClass; ++iC) {
		//std::cout << "Here :: " << iC << std::endl;
		bool isUpdated = ptrQs[iC]->setKappa(sw.K);
		if(isUpdated) updatedNodes.insert(ptrQs[iC]);
}
	}

void Base::setThetas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t iC=0; iC<nClass; ++iC) {
		bool isUpdated = ptrQs[iC]->setThetas(sw.thetas);
		if(isUpdated) updatedNodes.insert(ptrQs[iC]);
	}
}

void Base::setOmegas(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t iC=0; iC<nClass; ++iC) {
		bool isUpdated = ptrQs[iC]->setOmega(sw.omegas[iC]);
		if(isUpdated) updatedNodes.insert(ptrQs[iC]);
	}
}

void Base::setProportions(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	bool isUpdated = scalingMat->setPx(sw.Px);
	if(isUpdated) updatedNodes.insert(scalingMat);
}

void Base::setBranchLength(const SampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setBranchLength(sw.branchLength[i], updatedNodes);
	}
}


void Base::addEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);

	// Add tree link
	parent->addChild(child);
	parent->setRequireReinitDAG(true);

	// Add DAG link
	for(size_t iE=0; iE<parent->getNumberEdgeNode(); ++iE) {
		parent->getEdgeNode(iE)->addChild(child->getEdgeNode(iE));
	}
}

void Base::removeEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);
	// Was only required for moves
	// orderNodes(parent, child);

	// Remove DAG link
	for(size_t iE=0; iE<parent->getNumberEdgeNode(); ++iE) {
		EdgeNode *pNode = parent->getEdgeNode(iE);
		assert(pNode != NULL);
		EdgeNode *cNode = child->getEdgeNode(iE);
		assert(cNode != NULL);
		pNode->removeChild(cNode);
	}

	// Remove tree link
	parent->removeChild(child);
}

void Base::orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR) {
	treeNodeTI_children &childrenInt = nodeInt->getChildren();
	assert(childrenTR.size() == childrenInt.size());

	// Sort childrenInt according to childrenTR
	for(size_t iC=0; iC<childrenTR.size()-1; ++iC) {
		if(childrenTR[iC]->getId() != childrenInt[iC]->getId()) {
			bool found = false;
			for(size_t jC=iC+1; jC<childrenInt.size(); ++jC) {
				found = childrenTR[iC]->getId() == childrenInt[jC]->getId();
				if(found) {
					std::swap(childrenInt[iC], childrenInt[jC]);
					break;
				}
			}
			assert(found);
		}
	}
}



void Base::getDifference(const TR::vecTN_t &childrenTR, const treeNodeTI_children &children,
		std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove) {

	// We check if childrenTR are in children
	for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
		bool found = false;
		for(size_t iInt=0; iInt<children.size(); ++iInt) { // for each INT child
			found = childrenTR[iTR]->getId() == children[iInt]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toAdd
			toAdd.push_back(branchMap[childrenTR[iTR]->getId()]);
		}
	}

	// We check if children are in childrenTR
	for(size_t iInt=0; iInt<children.size(); ++iInt) {  // for each INT child
		bool found = false;
		for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
			found = children[iInt]->getId() == childrenTR[iTR]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toRemove
			toRemove.push_back(children[iInt]);
		}
	}
}


void Base::applyChangesToTreeAndDAG(std::vector<TR::TreeNode *> &updTreeNodes) {
	std::vector<TR::TreeNode *> updTNodes = curTree->getUpdatedNodes();

	size_t iN=0;
	while(iN<updTNodes.size()) { // updTNodes.size() may change during the loop
		// Get TR children
		TR::TreeNode *nodeTR = updTNodes[iN];
		TR::TreeNode *parentNodeTR = nodeTR->getParent();
		TR::vecTN_t childrenTR = nodeTR->getChildren();

		// Get Internal children
		TreeNode *node = branchMap[nodeTR->getId()];
		const treeNodeTI_children &children = node->getChildren();

		if(parentNodeTR != NULL) { // If we are not at the root node
			TreeNode *parentNode = branchMap[parentNodeTR->getId()];
			// If the edge with the parent has changed
			if(!parentNode->isParent(node)) {
				// We must update the parent too
				updTNodes.push_back(parentNodeTR);
			}
		}

		// Get difference
		std::vector<TreeNode*> toAdd, toRemove;
		getDifference(childrenTR, children, toAdd, toRemove);

		if(!toRemove.empty() || !toAdd.empty()) { // If there are differences
			// Remove edges
			for(size_t iR=0; iR<toRemove.size(); ++iR) {
				removeEdge(node, toRemove[iR]);
			}

			// Add edges
			for(size_t iA=0; iA<toAdd.size(); ++iA) {
				addEdge(node, toAdd[iA]);
			}

			for(size_t iE=0; iE<node->getNumberEdgeNode(); ++iE) {
				EdgeNode *pNode = node->getEdgeNode(iE);
				pNode->setDone(); // A bit dirty but necessary
			}

			// This node has been updated
			updTreeNodes.push_back(nodeTR);
		}
		iN++;
	}

}

void Base::signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
		std::set<DAG::BaseNode*> &updatedNodes) {
	std::set<size_t> markerNodes;
	for(size_t iN=0; iN<updTreeNodes.size(); ++iN) {
		// TR children
		TR::TreeNode *nodeTR = updTreeNodes[iN];
		TR::vecTN_t childrenTR;
		// Internal node
		TreeNode *node;

		while (nodeTR != NULL) { // While we are not at the root

			// Get children and node
			childrenTR = nodeTR->getChildren();
			node = branchMap[nodeTR->getId()];
			// If visited, we are done
			if(markerNodes.count(node->getId()) > 0) {
				break;
			}

			// Order nodes
			orderChildren(node, childrenTR);
			// Signal node updated
			for(size_t iE=0; iE<node->getNumberEdgeNode(); ++iE){
				ParentNode *pNode = dynamic_cast<ParentNode*>(node->getEdgeNode(iE));
				if(pNode) {
					pNode->updated();
					updatedNodes.insert(pNode);
				}
			}
			// Mark node
			markerNodes.insert(node->getId());
			// Next node
			nodeTR = nodeTR->getParent();
		}
	}
}

void Base::updateTreeAndDAG(std::set<DAG::BaseNode*> &updatedNodes) {
	// Vector to keep track of changed tree nodes
	std::vector<TR::TreeNode *> updTreeNodes;
	// Change TreeNode and dag Topology
	applyChangesToTreeAndDAG(updTreeNodes);
	// Signal which node must be updated in DAG and order the ndoes
	signalUpdatedTreeAndNodes(updTreeNodes, updatedNodes);
	// At this point both tree are cleared : no need to keep track of TR::Tree updated nodes
	curTree->clearUpdatedNodes();
}

bool Base::fullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node,
		std::set<DAG::BaseNode*> &updatedNodes) {
	bool requireReinit = false;

	// Get TR children
	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);
	// Get Internal children
	const treeNodeTI_children &children = node->getChildren();

	// Get difference
	std::vector<TreeNode*> toAdd, toRemove;
	getDifference(childrenTR, children, toAdd, toRemove);

	// Remove edges
	for(size_t iR=0; iR<toRemove.size(); ++iR) {
		removeEdge(node, toRemove[iR]);
	}

	// Add edges
	for(size_t iA=0; iA<toAdd.size(); ++iA) {
		addEdge(node, toAdd[iA]);
	}

	// If the node is a leaf, we don't have to reinit it
	if(node->isLeaf()) return false;

	// Order nodes
	orderChildren(node, childrenTR);

	// Apply the same for all children
	for(size_t iC=0; iC<childrenTR.size(); ++iC) {
		bool childRequireInit = fullUpdateTreeAndDAG(childrenTR[iC], nodeTR, node->getChild(iC), updatedNodes);
		requireReinit = requireReinit || childRequireInit;
	}
	// From there my subtree is correct
	// If one of my children was reinit'd or if I require to be reinit'd
	if(requireReinit || node->doRequireReinitDAG()) {
		// reinit each internal DAG nodes
		for(size_t iE=0; iE<node->getNumberEdgeNode(); ++iE){
			ParentNode *pNode = dynamic_cast<ParentNode*>(node->getEdgeNode(iE));
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
		node->setRequireReinitDAG(false); // I did it
		return true; // But by dad as to do it
	} else {
		return false;
	}
}




void Base::cleanTreeAndDAG() {
	rootTree->deleteChildren();
	delete rootTree;
	rootDAG->deleteChildren();
	delete rootDAG;

	branchPtr.clear();
	branchMap.clear();
}

double Base::getTotalTreeLength(const SampleWrapper &sw){
	double sum = 0.;
	for(size_t i=0; i<sw.N_BL; ++i) {
		sum += sw.branchLength[i];
	}
	return sum;
}

std::vector<std::string> Base::getOrderedTaxaNames(DL::MSA &msa) const {
	std::vector<std::string> orderedTaxaNames;

	for(size_t iA=0; iA<msa.getNAlignments(); ++iA) {
		orderedTaxaNames.push_back(msa.getAlignments()[iA].getName());
	}

	std::sort(orderedTaxaNames.begin(), orderedTaxaNames.end());

	return orderedTaxaNames;
}

} /* namespace TreeInference */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
