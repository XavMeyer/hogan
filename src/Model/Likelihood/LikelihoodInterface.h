//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LikelihoodProcessor.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef LIKELIHOODPROCESSOR_H_
#define LIKELIHOODPROCESSOR_H_

#include "Utils/Uncopyable.h"
#include "Utils/Code/SerializationSupport.h"
#include "Sampler/Samples/Sample.h"
#include "DAG/Scheduler/BaseScheduler.h"

#include "math.h"

#include <vector>
#include <boost/math/distributions.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

namespace StatisticalModel {
namespace Likelihood {

using Sampler::Sample;

class LikelihoodInterface : private Uncopyable {
public:
	typedef boost::shared_ptr< LikelihoodInterface > sharedPtr_t;

public:
	LikelihoodInterface();
	virtual ~LikelihoodInterface();

	virtual double processLikelihood(const Sampler::Sample &sample) = 0;
	virtual string getName(char sep=' ') const = 0;

	virtual size_t stateSize() const;
	virtual void setStateLH(const char* aState);
	virtual void getStateLH(char* aState) const;
	virtual double update(const vector<size_t>& pIndices, const Sample& sample);

	virtual void saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff){};
	virtual void loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff){};

	virtual void initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, std::streampos offset);
	virtual std::streampos getCustomizedLogSize() const;
	virtual void writeCustomizedLog(const std::vector<Sample> &samples);

	virtual DAG::Scheduler::BaseScheduler* getScheduler() const;
	virtual std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	const vector<string>& getMarkerNames() const;
	const vector<double>& getMarkers() const;

	bool isLog() const;
	bool isUpdatable() const;

	static double getAlphaFromL(double l, double var);
	static double getOptiL(uint nP, uint nPPB, double var);

protected:
	bool isLogLH, isUpdatableLH;
	vector<string> markerNames;
	mutable vector<double> markers;

};

} // namespace Likelihood
} // namespace StatisticalModel

#endif /* LIKELIHOODPROCESSOR_H_ */
