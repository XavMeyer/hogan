//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Parameters.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Parameters.h"

namespace StatisticalModel {

Parameters::Parameters() {

}

Parameters::~Parameters() {
}

void Parameters::addParameter(ParamIntDef aParam){
	intParams.push_back(aParam);
	if(!dynamic_cast<NoPrior*>(aParam.getPrior())){
		intPrior.push_back(make_pair(intParams.size()-1, aParam.getPrior()));
	}
}

void Parameters::addParameter(ParamDblDef aParam){
	dblParams.push_back(aParam);
	if(!dynamic_cast<NoPrior*>(aParam.getPrior())){
		dblPrior.push_back(make_pair(dblParams.size()-1, aParam.getPrior()));
	}
}

string Parameters::getName(int aInd) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].getName();
	} else {
		return dblParams[aInd-ivSize].getName();
	}
}

vector<string> Parameters::getNames() const {
	vector<string> names;

	for(pIntCstIt_t itI=intParams.begin(); itI!=intParams.end(); ++itI){
		names.push_back(itI->getName());
	}

	for(pDblCstIt_t itD=dblParams.begin(); itD!=dblParams.end(); ++itD){
		names.push_back(itD->getName());
	}

	return names;
}

Sampler::Sample Parameters::generateRandomSample() const {
	Sampler::Sample sample;

	for(pIntCstIt_t itI=intParams.begin(); itI!=intParams.end(); ++itI){
		sample.intValues.push_back(itI->generateRandom());
	}

	for(pDblCstIt_t itD=dblParams.begin(); itD!=dblParams.end(); ++itD){
		sample.dblValues.push_back(itD->generateRandom());
	}

	return sample;
}

double Parameters::processPriorValue(const Sampler::Sample &sample) const {
	double val=1.0;

	for(uint i=0; i<intParams.size(); ++i){
		long int val = sample.getIntParameter(i);
		if(!intParams[i].isInBound(val)) return 0.;
	}

	for(uint i=0; i<dblParams.size(); ++i){
		double val = sample.getDoubleParameter(i + intParams.size());
		if(!dblParams[i].isInBound(val)) return 0.;
	}

	for(priorCstIt_t itI=intPrior.begin(); itI!=intPrior.end(); ++itI){
		val *= itI->second->computePDF(itI->first, sample);
	}

	for(priorCstIt_t itD=dblPrior.begin(); itD!=dblPrior.end(); ++itD){
		val *= itD->second->computePDF(itD->first+intParams.size(), sample);
	}

	return val;
}

double Parameters::processLogPriorValue(const Sampler::Sample &sample) const {
	double val=0.0;

	for(uint i=0; i<intParams.size(); ++i){
		long int val = sample.getIntParameter(i);
		if(!intParams[i].isInBound(val)) {
			return -numeric_limits<double>::infinity();
		}
	}

	for(uint i=0; i<dblParams.size(); ++i){
		double val = sample.getDoubleParameter(i + intParams.size());
		if(!dblParams[i].isInBound(val)) {
			return -numeric_limits<double>::infinity();
		}
	}

	for(priorCstIt_t itI=intPrior.begin(); itI!=intPrior.end(); ++itI){
		val += log(itI->second->computePDF(itI->first, sample));
	}

	for(priorCstIt_t itD=dblPrior.begin(); itD!=dblPrior.end(); ++itD){
		val += log(itD->second->computePDF(itD->first+intParams.size(), sample));
	}

	return val;
}

size_t Parameters::size() const {
	return intParams.size() + dblParams.size();
}

void Parameters::signalReflections(int aInd, int cnt) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].signalReflections(cnt);
	} else {
		return dblParams[aInd-ivSize].signalReflections(cnt);
	}
}

bool Parameters::isOverflowed(int aInd) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].isOverflowed();
	} else {
		return dblParams[aInd-ivSize].isOverflowed();
	}
}

double Parameters::getParameterFreq(int aInd) const {
	int ivSize = intParams.size();
	double freq;
	if(aInd < ivSize){
		freq = intParams[aInd].getFreq();
	} else {
		freq = dblParams[aInd-ivSize].getFreq();
	}
	return freq;
}

int Parameters::getType(int aInd) const {
	int cat;
	int ivSize = intParams.size();
	if(aInd < ivSize){
		cat = intParams[aInd].getType();
	} else {
		cat  = dblParams[aInd-ivSize].getType();
	}
	return cat;
}

bool Parameters::isInt(size_t aInd) const {
	size_t ivSize = intParams.size();
	return (aInd < ivSize);
}

bool Parameters::isDbl(size_t aInd) const {
	return !isInt(aInd);
}

bool Parameters::isInBound(int aInd, const double value) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].isInBound(value);
	} else {
		return dblParams[aInd-ivSize].isInBound(value);
	}
}

bool Parameters::requireReflection(int aInd) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].requireReflection();
	} else {
		return dblParams[aInd-ivSize].requireReflection();
	}
}

double Parameters::getBoundMin(int aInd) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].getBoundMin();
	} else {
		return dblParams[aInd-ivSize].getBoundMin();
	}
}

double Parameters::getBoundMax(int aInd) const {
	int ivSize = intParams.size();
	if(aInd < ivSize){
		return intParams[aInd].getBoundMax();
	} else {
		return dblParams[aInd-ivSize].getBoundMax();
	}
}

} // namespace StatisticalModel

