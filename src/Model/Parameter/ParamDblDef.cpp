//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParamDblDef.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "ParamDblDef.h"

namespace StatisticalModel {

ParamDblDef::ParamDblDef(string aName, PriorInterface::sharedPtr_t aPrior, const bool aReflect) : min(0.), max(1.), freq(1.), type(0), name(aName), prior(aPrior) {
	doReflect = aReflect;
	cntReflection = 0;
}

ParamDblDef::~ParamDblDef() {
}

double ParamDblDef::generateRandom() const {
	if(dynamic_cast<NoPrior*>(prior.get())){
		return Parallel::mpiMgr().getPRNG()->genUniformDbl(min, max);
	} else {
		double rnd = prior->sample();

		if(min == max) {
			return min;
		} else {
			while(rnd < min || rnd > max){
				if(rnd < min){
					rnd = 2.*min-rnd;
				} else if(rnd > max){
					rnd = 2.*max-rnd;
				}
			}
		}

		return rnd;
	}
}

double ParamDblDef::computePriorPDF(const size_t iP, const Sampler::Sample &sample) const {
	return prior->computePDF(iP, sample);
}

void ParamDblDef::setMin(double aMin){
	min=aMin;
}

void ParamDblDef::setMax(double aMax){
	max=aMax;
}

void ParamDblDef::setFreq(double aFreq){
	freq=aFreq;;
}

void ParamDblDef::setType(int aType){
	type=aType;
}

bool ParamDblDef::isInBound(const double value) const {
	//if(!(value >= min && value <= max)) std::cerr << name << " not in bound with value : " << value << std::endl;
	return value >= min && value <= max;
}

void ParamDblDef::signalReflections(int cnt) const {
	if(cnt > 2 && cntReflection < 8) {
		cntReflection++;
	} else if (cnt <=2 && cntReflection > 0) {
		cntReflection--;
	}
}

bool ParamDblDef::isOverflowed() const {
	return cntReflection >= 5;
}

string ParamDblDef::getName() const {
	return name;
}

} // namespace StatisticalModel

