//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParamIntDef.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef PARAMINTDEF_H_
#define PARAMINTDEF_H_

#include "Parallel/Parallel.h"
#include "Model/Prior/PriorInterface.h"
#include "Model/Prior/NoPrior.h"
#include <string>

using namespace std;

namespace StatisticalModel {

class ParamIntDef {
public:
	ParamIntDef(string aName, PriorInterface::sharedPtr_t aPrior, const bool aReflect=true);
	virtual ~ParamIntDef();

	long int generateRandom() const ;
	double computePriorPDF(const size_t iP, const Sampler::Sample &sample) const;

	void setMin(long int aMin);
	void setMax(long int aMax);
	void setFreq(double aFreq);
	void setType(int iType);

	bool isInBound(const long int value) const;
	void signalReflections(int cnt) const;
	bool isOverflowed() const;

	bool requireReflection() const {return doReflect;}
	long int getBoundMin() const {return min;}
	long int getBoundMax() const {return max;}

	string getName() const;
	PriorInterface* getPrior() const {return prior.get();}
	double getFreq() const {return freq;}
	int getType() const {return type;}

private:
	long int min, max;
	double freq;
	int type;
	mutable int cntReflection;
	bool doReflect;
	string name;
	PriorInterface::sharedPtr_t prior;
};

} // namespace StatisticalModel


#endif /* PARAMINTDEF_H_ */
