//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Parameters.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include "ParamDblDef.h"
#include "ParamIntDef.h"
#include "Sampler/Samples/Sample.h"

#include <set>
#include <vector>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

namespace StatisticalModel {

class Parameters : private Uncopyable {
public:
	Parameters();
	~Parameters();

	void addParameter(ParamIntDef aParam);
	void addParameter(ParamDblDef aParam);
	string getName(int aInd) const;
	vector<string> getNames() const;
	double getParameterFreq(int aInd) const;
	int getType(int aInd) const;

	Sampler::Sample generateRandomSample() const;

	double processPriorValue(const Sampler::Sample &sample) const;
	double processLogPriorValue(const Sampler::Sample &sample) const;

	size_t size() const;

	void signalReflections(int aInd, int cnt) const;
	bool isOverflowed(int aInd) const;

	bool isInt(size_t aInd) const;
	bool isDbl(size_t aInd) const;

	bool isInBound(int aInd, const double value) const;
	bool requireReflection(int aInd) const;
	double getBoundMin(int aInd) const;
	double getBoundMax(int aInd) const;


private:

	typedef vector<ParamIntDef>::iterator pIntIt_t;
	typedef vector<ParamDblDef>::iterator pDblIt_t;

	typedef vector<ParamIntDef>::const_iterator pIntCstIt_t;
	typedef vector<ParamDblDef>::const_iterator pDblCstIt_t;

	vector<ParamIntDef> intParams;
	vector<ParamDblDef> dblParams;

	typedef vector<pair<uint, PriorInterface*> >::iterator priorIt_t;
	typedef vector<pair<uint, PriorInterface*> >::const_iterator priorCstIt_t;

	vector<pair<uint, PriorInterface*> > intPrior, dblPrior;
};

} // namespace StatisticalModel


#endif /* PARAMETERS_H_ */
