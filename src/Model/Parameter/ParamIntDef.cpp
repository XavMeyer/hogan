//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParamIntDef.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "ParamIntDef.h"

namespace StatisticalModel {

ParamIntDef::ParamIntDef(string aName, PriorInterface ::sharedPtr_t aPrior, const bool aReflect) : freq(1.), type(0), name(aName), prior(aPrior) {
	doReflect = aReflect;
	cntReflection = 0;
	min = -std::numeric_limits<long int>::max();
	max = std::numeric_limits<long int>::max();
}

ParamIntDef::~ParamIntDef() {
}

long int ParamIntDef::generateRandom() const {
	if(dynamic_cast<NoPrior*>(prior.get())){
		return Parallel::mpiMgr().getPRNG()->genUniformInt(min, max);
	} else {
		return prior->sample();
	}
}

double ParamIntDef::computePriorPDF(const size_t iP, const Sampler::Sample &sample) const {
	return prior->computePDF(iP, sample);
}


void ParamIntDef::setMin(long int aMin){
	min=aMin;
}

void ParamIntDef::setMax(long int aMax){
	max=aMax;
}

void ParamIntDef::setFreq(double aFreq){
	freq=aFreq;
}

void ParamIntDef::setType(int aType){
	type=aType;
}

bool ParamIntDef::isInBound(const long int value) const {
	return value >= min && value <= max;
}

void ParamIntDef::signalReflections(int cnt) const {
	if(cnt > 2 && cntReflection < 8) {
		cntReflection++;
	} else if (cnt <=2 && cntReflection > 0) {
		cntReflection--;
	}
}

bool ParamIntDef::isOverflowed() const {
	return cntReflection >= 5;
}

string ParamIntDef::getName() const {
	return name;
}

} // namespace StatisticalModel

