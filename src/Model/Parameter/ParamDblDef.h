//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParamDblDef.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef PARAMDBLDEF_H_
#define PARAMDBLDEF_H_

#include "Parallel/Parallel.h"
#include "Model/Prior/PriorInterface.h"
#include "Model/Prior/NoPrior.h"
#include <string>
#include <iostream>

using namespace std;

namespace StatisticalModel {

class ParamDblDef {
public:
	ParamDblDef(string aName, PriorInterface::sharedPtr_t aPrior, const bool aReflect=true);
	virtual ~ParamDblDef();

	double generateRandom() const;
	double computePriorPDF(const size_t iP, const Sampler::Sample &sample) const;

	void setMin(double aMin);
	void setMax(double aMax);
	void setFreq(double aFreq);
	void setType(int iType);

	bool isInBound(const double value) const;
	void signalReflections(int cnt) const;
	bool isOverflowed() const;

	string getName() const;
	PriorInterface* getPrior() const {return prior.get();}
	double getFreq() const {return freq;}
	int getType() const {return type;}

	bool requireReflection() const {return doReflect;}
	double getBoundMin() const {return min;}
	double getBoundMax() const {return max;}

private:
	double min, max, freq;
	int type;
	bool doReflect;
	mutable int cntReflection;
	string name;
	PriorInterface::sharedPtr_t prior;
};

} // namespace StatisticalModel

#endif /* PARAMDBLDEF_H_ */
