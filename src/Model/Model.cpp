//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Model.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Model.h"

namespace StatisticalModel {


Model::Model(LikelihoodInterface::sharedPtr_t aProc) : proc(aProc){
}

Model::~Model() {
}


Sampler::Sample Model::getRandomSample() const {
	return params.generateRandomSample();
}

double Model::processPriorValue(const Sampler::Sample &sample) const{
	if(proc->isLog()){
		return params.processLogPriorValue(sample);
	} else {
		return params.processPriorValue(sample);
	}
}

Parameters& Model::getParams() {
	return params;
}

const Parameters& Model::getParams() const {
	return params;
}

LikelihoodInterface::sharedPtr_t Model::getLikelihood() {
	return proc;
}

const LikelihoodInterface::sharedPtr_t Model::getLikelihood() const {
	return proc;
}


void Model::initBlocks(){
	for(size_t iP=0; iP < params.size(); ++iP){
		Block::sharedPtr_t block(new Block());
		block->addParameter(iP);
		blocks.addBlock(block);
	}
}

Blocks& Model::getBlocks() {
	return blocks;
}

const Blocks& Model::getBlocks() const {
	return blocks;
}
} // namespace StatisticalModel

