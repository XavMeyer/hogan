//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * PriorInterface.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef PRIOR_H_
#define PRIOR_H_

#include "Parallel/RNG/RNG.h"
#include "Sampler/Samples/Sample.h"

#include <boost/math/distributions.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>
#include <iostream>

using namespace std;

namespace StatisticalModel {

class PriorInterface {
public:
	typedef boost::shared_ptr<PriorInterface> sharedPtr_t;

public:
	virtual ~PriorInterface(){};

	virtual double sample() const = 0;
	virtual double computePDF(const size_t iP, const Sampler::Sample &sample) const = 0;

	static sharedPtr_t createNoPrior();
	static sharedPtr_t createUniformBoost(const RNG *aRNG, double aStart, double aEnd);
	static sharedPtr_t createGaussianBoost(const RNG *aRNG, double aSigma);
	static sharedPtr_t createBetaBoost(const RNG *aRNG, double aAlpha, double aBeta);
	static sharedPtr_t createGammaBoost(const RNG *aRNG, double aScale, double aShape);
	static sharedPtr_t createExponentialBoost(const RNG *aRNG, double aLambda);

};

} // namespace StatisticalModel

#endif /* PRIOR_H_ */
