//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Gaussian_Boost.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Gaussian_Boost.h"

namespace StatisticalModel {

Gaussian_Boost::Gaussian_Boost(const RNG *aRNG, double aSigma) : r(aRNG), sigma(aSigma){
}

Gaussian_Boost::~Gaussian_Boost() {
}

double Gaussian_Boost::sample() const {
	return r->genGaussian(sigma);
}

double Gaussian_Boost::computePDF(const size_t iP, const Sampler::Sample &sample) const {
	boost::math::normal_distribution<> dist(0., sigma);
	return boost::math::pdf(dist, sample.getDoubleParameter(iP));
}

} // namespace StatisticalModel

