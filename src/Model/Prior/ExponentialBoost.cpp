//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ExponentialBoost.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "ExponentialBoost.h"

namespace StatisticalModel {

Exponential_Boost::Exponential_Boost(const RNG *aRNG, double aLambda) : r(aRNG), lambda(aLambda) {

}

Exponential_Boost::~Exponential_Boost() {
}

double Exponential_Boost::sample() const {
	return r->genExponential(lambda);
}

double Exponential_Boost::computePDF(const size_t iP, const Sampler::Sample &sample) const {
	boost::math::exponential_distribution<> dist(lambda);
	return boost::math::pdf(dist, sample.getDoubleParameter(iP));
}



} /* namespace StatisticalModel */
