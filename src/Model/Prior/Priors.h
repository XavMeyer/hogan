//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Priors.h
 *
 *  Created on: 23 sept. 2013
 *      Author: meyerx
 */

#ifndef PRIORS_H_
#define PRIORS_H_

#include "NoPrior.h"
#include "Gaussian_Boost.h"
#include "Beta_Boost.h"
#include "Gamma_Boost.h"
#include "Uniform_Boost.h"
#include "ExponentialBoost.h"

#endif /* PRIORS_H_ */
