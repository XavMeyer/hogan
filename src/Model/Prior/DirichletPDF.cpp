//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletPDF.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "Model/Prior/DirichletPDF.h"

namespace StatisticalModel {

DirichletPDF::DirichletPDF(size_t nAlphas, double aAlpha){

	if(aAlpha == 1.) {
		flatDirichlet = true;
	} else {
		flatDirichlet = false;
	}

	for(size_t i=0; i<nAlphas; ++i) {
		alphas.push_back(aAlpha);
	}

	defineConstBeta();
}



DirichletPDF::DirichletPDF(std::vector<double> &aAlphas) : alphas(aAlphas){

	flatDirichlet = true;
	for(size_t i=0; i<alphas.size(); ++i) {
		if(alphas[i] != 1.) {
			flatDirichlet = false;
			break;
		}
	}

	defineConstBeta();
}


DirichletPDF::~DirichletPDF() {
}


double DirichletPDF::computePDF(const std::vector<double> &x) const {
	assert(x.size() == alphas.size());

	double p=1.;
	if(!flatDirichlet) {
		for(size_t i=0; i<alphas.size(); ++i) {
			p *= std::pow(x[i], alphas[i]-1.);
		}
	}

	return p/constBeta;
}

void DirichletPDF::defineConstBeta() {

	double sumAlpha = 0.;
	double prodGamma = 1.;
	for(size_t i=0; i<alphas.size(); ++i) {
		sumAlpha += alphas[i];
		prodGamma *= boost::math::tgamma(alphas[i]);
	}
	constBeta = prodGamma/boost::math::tgamma(sumAlpha);
}

} /* namespace StatisticalModel */
