//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BetaGSL.cpp
 *
 *  Created on: 23 sept. 2013
 *      Author: meyerx
 */

#include "Beta_Boost.h"

namespace StatisticalModel {

Beta_Boost::Beta_Boost(const RNG *aRNG, double aAlpha, double aBeta) : r(aRNG), alpha(aAlpha), beta(aBeta) {

}

Beta_Boost::~Beta_Boost() {
}

double Beta_Boost::sample() const {
	return r->genBeta(alpha, beta);
}

double Beta_Boost::computePDF(const size_t iP, const Sampler::Sample &sample) const {
	boost::math::beta_distribution<> dist(alpha, beta);
	return boost::math::pdf(dist, sample.getDoubleParameter(iP));
}

} // namespace StatisticalModel

