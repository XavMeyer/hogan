//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Prior.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "PriorInterface.h"
#include "Priors.h"

namespace StatisticalModel {

PriorInterface::sharedPtr_t PriorInterface::createNoPrior(){
	return sharedPtr_t(new NoPrior());
}

PriorInterface::sharedPtr_t PriorInterface::createUniformBoost(const RNG *aRNG, double aStart, double aEnd) {
	return sharedPtr_t(new Uniform_Boost(aRNG, aStart, aEnd));
}

PriorInterface::sharedPtr_t PriorInterface::createGaussianBoost(const RNG *aRNG, double aSigma){
	return sharedPtr_t(new Gaussian_Boost(aRNG, aSigma));
}

PriorInterface::sharedPtr_t PriorInterface::createBetaBoost(const RNG *aRNG, double aAlpha, double aBeta){
	return sharedPtr_t(new Beta_Boost(aRNG, aAlpha, aBeta));
}

PriorInterface::sharedPtr_t PriorInterface::createGammaBoost(const RNG *aRNG, double aScale, double aShape) {
	return sharedPtr_t(new Gamma_Boost(aRNG, aScale, aShape));
}

PriorInterface::sharedPtr_t PriorInterface::createExponentialBoost(const RNG *aRNG, double aLambda) {
	return sharedPtr_t(new Exponential_Boost(aRNG, aLambda));
}

} // namespace StatisticalModel

