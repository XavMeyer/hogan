//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixFactory.h
 *
 * @date Feb 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXFACTORY_H_
#define MATRIXFACTORY_H_

#include "Operations/Base.h"
#include "Operations/Diagonal.h"
#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cmath>

#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace MatrixUtils {

namespace MD = ::MolecularEvolution::Definition;

enum nuclModel_t {GTR=1, K80=2};

class MatrixFactory {
public:
	typedef boost::shared_ptr<MatrixFactory> sharedPtr_t;

public:

	MatrixFactory(const size_t aN, const size_t aNB_COEFF);
	MatrixFactory(const size_t aN, const size_t aNB_COEFF, bool aDoRowsSumToZero);
	virtual ~MatrixFactory();

	// Steps
	void initOperations();
	void setFrequencies(const std::vector<double>& aFrequencies);
	void setCoefficients(const std::vector<double>& aCoefficients);

	void fillMatrix(const std::vector<double>& aCoefficients);

	size_t getNBCoefficients() const;
	double getScalingFactor() const;
	size_t getMatrixDimension() const;
	std::vector<double>& getMatrix();
	const std::vector<double>& getMatrix() const;

public:

	const size_t N, NB_COEFF;

protected:

	const bool doRowsSumtoZero;

	double scalingFactor;

	std::vector<double> matrix;
	std::vector<double> frequencies, freqDotFreq, coefficients;

	// Operations required to fill the matrix
	std::vector<Operations::Base*> operations;
	std::vector<Operations::Diagonal*> diagonalOperations;
	std::vector<Operations::MultStore> instructions;

	virtual void doInitOperations() = 0;

	virtual Operations::Base* defineOperation(size_t iFrom, size_t iTo) const = 0;

private:

};

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* MATRIXFACTORY_H_ */
