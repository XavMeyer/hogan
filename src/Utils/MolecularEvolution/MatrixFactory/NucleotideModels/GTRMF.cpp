//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HKY85MF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "GTRMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

const size_t GTR_MF::GTR_NB_COEFF = 5;

GTR_MF::GTR_MF() : NucleotideMF(GTR_NB_COEFF) {
	MatrixFactory::initOperations();
}

GTR_MF::GTR_MF(const bool aDoRowsSumtoZero) : NucleotideMF(GTR_NB_COEFF, aDoRowsSumtoZero) {
	MatrixFactory::initOperations();
}


GTR_MF::~GTR_MF() {
}

void GTR_MF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}

Operations::Base* GTR_MF::defineOperation(size_t iFrom, size_t iTo) const {

	std::vector<size_t> coeffId;

	// Get correct theta index
	size_t thetaIdx = defineThetaIdx(iFrom, iTo);
	if(thetaIdx < GTR_NB_COEFF) coeffId.push_back(thetaIdx); // Transition T<->G fixed at 1.
	//coeffId.push_back(thetaIdx); // Its not the transition T<->G fixed at 1.

	Operations::Base* myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);

	//std::cout << myOp->getIdxMatrix() << std::endl;
	return myOp;

	/*
	// sort idx, consider only the upper part of the symmetric matrix
	size_t smallIdx = std::min(iFrom, iTo);
	size_t bigIdx = std::max(iFrom, iTo);

	// The coefficient have the following position on the upper part of the matrix
	// {1:a, 2:b, 3:c, 6:d, 7:e, 11:f}
	size_t position = smallIdx*N+bigIdx;

	std::vector<size_t> coeffId;
	switch (position) {
		case 1: // coefficient "a" at position 0
			coeffId = boost::assign::list_of(0);
			break;
		case 2: // coefficient "b" at position 1
			coeffId = boost::assign::list_of(1);
			break;
		case 3: // coefficient "c" at position 2
			coeffId = boost::assign::list_of(2);
			break;
		case 6: // coefficient "d" at position 3
			coeffId = boost::assign::list_of(3);
			break;
		case 7: // coefficient "e" at position 4
			coeffId = boost::assign::list_of(4);
			break;
		case 11: // coefficient "f" at position 5
			coeffId = boost::assign::list_of(5);
			break;
		default:
			std::cerr << "Operations::Base* GTR_MF::defineOperation(size_t iFrom, size_t iTo) const;" << std::endl;
			std::cerr << "Impossible position (" << position << ") in the matrix." << std::endl;
			abort();
			break;
	}

	Operations::Base* myOp;
	myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);

	return myOp;
	*/
}

int GTR_MF::defineThetaIdx(size_t iFrom, size_t iTo) const {
	const MD::Nucleotides *nucl = MD::getNucleotides();

	if((iFrom < 0) || (iTo < 0)) {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
	}

	char from = nucl->NUCL_BASE[iFrom];
	char to = nucl->NUCL_BASE[iTo];


	// We want the nucleotide with smallest index to be in from
	if(iFrom > iTo) std::swap(from, to);

	// For each possible nucleotide transition return the index
	if(from == 'A' && to == 'C') {
		return 0;
	} else if(from == 'A' && to == 'G') {
		return 1;
	} else if(from == 'A' && to == 'T') {
		return 2;
	} else if(from == 'C' && to == 'G') {
		return 3;
	} else if(from == 'C' && to == 'T') {
		return 4;
	} else if(from == 'T' && to == 'G') {
		return 5;
	} else {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
		return -1;
	}
}


} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
