//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HKY85MF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "HKY85MF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

const size_t HKY85_MF::HKY85_NB_COEFF = 1;

HKY85_MF::HKY85_MF() : NucleotideMF(HKY85_NB_COEFF) {
	MatrixFactory::initOperations();
}

HKY85_MF::HKY85_MF(const bool aDoRowsSumtoZero) : NucleotideMF(HKY85_NB_COEFF, aDoRowsSumtoZero) {
	MatrixFactory::initOperations();
}


HKY85_MF::~HKY85_MF() {
}

void HKY85_MF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}

Operations::Base* HKY85_MF::defineOperation(size_t iFrom, size_t iTo) const {
	const MD::Nucleotides *Nucl = MD::getNucleotides();

	std::vector<size_t> coeffId;

	bool isTransition = Nucl->isTransition(iFrom, iTo);
	if(isTransition) {
		coeffId.push_back(0); // Its a transition, push back id of kappa
	}

	Operations::Base* myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);
	return myOp;


	/*if(Nucl->isTransition(iFrom, iTo)) { // if transition
		// Create an operation Operations::CoeffsDotFreq htat will process
		// frequency[iTo]*Coefficients[0] ==> FreqNucl * Alpha
		Operations::Base* myOp;
		std::vector<size_t> tmp = boost::assign::list_of(0);
		myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, tmp);
		return myOp;
	} else { // if transversion
		// Create an operation Operations::CoeffsDotFreq htat will process
		// frequency[iTo]*Coefficients[0] ==> FreqNucl * Beta
		Operations::Base* myOp;
		std::vector<size_t> tmp = boost::assign::list_of(1);
		myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, tmp);
		return myOp;
	}*/

}

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
