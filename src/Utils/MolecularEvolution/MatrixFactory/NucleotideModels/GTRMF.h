//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GTRMF.h
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief The GTR_MF class represent the required implementation for the GTR model
 *
 * This class represent the required implementation for the GTR model. The diagonal operation
 * (Operations::Diagonal) is used to ensure that the sum of a row is equal to 0. The diagonal object
 * is already pre-processed during the scan of the nucleotides.
 * The vector of coefficient contains [a, b, c, d, e, f].
 * We use the operation Operations::CoeffsDotFreq to couple each substitution with its coefficient
 * and nucleotide frequency.
 */
#ifndef GTRMF_H_
#define GTRMF_H_

#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/NucleotideMF.h"

#include <boost/assign/list_of.hpp>

namespace MolecularEvolution {
namespace MatrixUtils {

class GTR_MF: public NucleotideMF {
public:
	static const size_t GTR_NB_COEFF;
public:
	GTR_MF();
	GTR_MF(const bool aDoRowsSumtoZero);
	~GTR_MF();

private:

	typedef NucleotideMF super;

	void doInitOperations();

	Operations::Base* defineOperation(size_t iFrom, size_t iTo) const;
	int defineThetaIdx(size_t iFrom, size_t iTo) const;
};

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* GTRMF_H_ */
