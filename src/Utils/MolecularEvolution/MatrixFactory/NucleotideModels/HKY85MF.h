//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HKY85MF.h
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief The HKY85_MF class represent the required implementation for the HKY85 model
 *
 * This class represent the required implementation for the HKY85 model. The diagonal operation
 * (Operations::Diagonal) is used to ensure that the sum of a row is equal to 0. The diagonal object
 * is already pre-processed during the scan of the nucleotides.
 * The vector of coefficient contains [Alpha, Beta],
 * If we encounter a transition A-G / T-C we use the coefficient Alpha (index 0) otherwise Beta (index 1).
 * For that we use the operation Operations::CoeffsDotFreq that couple the coefficient id 0 for transition,
 * 1 for transversion, with the nucleotide frequency.
 * to use.
 */
#ifndef HKY85MF_H_
#define HKY85MF_H_

#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/NucleotideMF.h"

#include <boost/assign/list_of.hpp>

namespace MolecularEvolution {
namespace MatrixUtils {

class HKY85_MF: public NucleotideMF {
public:
	HKY85_MF();
	HKY85_MF(const bool aDoRowsSumtoZero);
	~HKY85_MF();

private:

	typedef NucleotideMF super;

	static const size_t HKY85_NB_COEFF;

	void doInitOperations();

	Operations::Base* defineOperation(size_t iFrom, size_t iTo) const;
};

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* HKY85MF_H_ */
