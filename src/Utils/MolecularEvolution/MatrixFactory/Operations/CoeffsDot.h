//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoeffsDot.h
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef COEFFSDOT_H_
#define COEFFSDOT_H_

#include "Base.h"

#include <vector>

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

class CoeffsDot: public Base {
public:
	//CoeffsDot();
	CoeffsDot(size_t aIdxFrom, size_t aIdxTo, const size_t N, std::vector<size_t> aCoeffsId);
	~CoeffsDot();

	std::vector<MultStore> toInstructions(double &scalingFactor,
										  std::vector<double>& matrix,
										  std::vector<double>& frequencies,
										  std::vector<double>& freqDotFreq,
										  std::vector<double> &coefficients) const;

private:
	std::vector<size_t> coeffsId;
};

} /* namespace Operations */

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* COEFFDOT_H_ */
