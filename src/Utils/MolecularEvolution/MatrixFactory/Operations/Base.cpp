//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.cpp
 *
 * @date Feb 3, 2015
 * @author meyerx
 * @brief
 */
#include "Base.h"

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

Base::Base(size_t aIdxFrom, size_t aIdxTo, const size_t N) : idxFrom(aIdxFrom), idxTo(aIdxTo), idxMatrix(idxFrom + idxTo*N) {
}

Base::~Base() {
}

size_t Base::getIdxMatrix() const {
	return idxMatrix;
}

size_t Base::getIdxFrom() const {
	return idxFrom;
}

size_t Base::getIdxTo() const {
	return idxTo;
}

} /* namespace Operations */
} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */
