//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoeffsDotFreq.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "CoeffsDotFreq.h"

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

/*CoeffsDotFreq::CoeffsDotFreq() : Base() {
}*/

CoeffsDotFreq::CoeffsDotFreq(size_t aIdxFrom, size_t aIdxTo, const size_t N, std::vector<size_t> &aCoeffsId) :
		Base(aIdxFrom, aIdxTo, N), coeffsId(aCoeffsId) {
}

CoeffsDotFreq::~CoeffsDotFreq() {

}

std::vector<MultStore> CoeffsDotFreq::toInstructions(double &scalingFactor,
													 std::vector<double>& matrix,
													 std::vector<double>& frequencies,
													 std::vector<double>& freqDotFreq,
													 std::vector<double>& coefficients) const {

	std::vector<MultStore> instrs;

	if(coeffsId.empty()) {
		// matrix = 0*matrix + 1 * 1
		instrs.push_back(MultStore(MultStore::ONE, frequencies[idxTo], MultStore::ZERO, matrix[this->getIdxMatrix()]));
	} else {
		// matrix = 0*matrix + freq * coeff
		instrs.push_back(MultStore(frequencies[idxTo], coefficients[coeffsId[0]], MultStore::ZERO, matrix[this->getIdxMatrix()]));
		for(size_t i=1; i<coeffsId.size(); ++i) {
			// matrix = 0*matrix + matrix * coeff
			instrs.push_back(MultStore(matrix[this->getIdxMatrix()], coefficients[coeffsId[i]], MultStore::ZERO, matrix[this->getIdxMatrix()]));
		}
	}

	// adding scaling factor : sf = 1*sf + freqij * matrix
	if(idxFrom > idxTo){
		instrs.push_back(MultStore(matrix[this->getIdxMatrix()], frequencies[idxFrom], MultStore::ONE, scalingFactor));
	}

	return instrs;
}


} /* namespace Operations */

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
