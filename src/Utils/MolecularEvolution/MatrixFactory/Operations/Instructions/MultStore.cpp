//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MultStore.cpp
 *
 * @date Feb 5, 2015
 * @author meyerx
 * @brief
 */
#include "MultStore.h"

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

const double MultStore::ZERO = 0.;
const double MultStore::ONE = 1.;
const double MultStore::NEG_ONE = -1.;

MultStore::MultStore(const double &aOpA, const double &aOpB, const double &aAlpha, double &aDest) :
	opA(&aOpA), opB(&aOpB), alpha(&aAlpha), dest(&aDest){

	alphaEqualsZero = alpha == &ZERO;
}

MultStore::~MultStore() {
}

std::string MultStore::toString() const {
	std::stringstream ss;

	ss << "ptrA = " << opA << " :: ";
	ss << "ptrB = " << opB << " :: ";
	ss << "alpha = " << alpha << " :: ";
	ss << "dest = " << dest;

	return ss.str();
}

} /* namespace Operations */

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
