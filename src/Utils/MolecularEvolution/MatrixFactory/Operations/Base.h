//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Operation.h
 *
 * @date Feb 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef OPERATION_H_
#define OPERATION_H_

#include "Instructions/MultStore.h"

#include <iostream>
#include <functional>
#include <vector>

namespace MolecularEvolution {
namespace MatrixUtils {
namespace Operations {

class Base {
public:
	//Base();
	Base(size_t aIdxFrom, size_t aIdxTo, const size_t N);
	virtual ~Base();

	size_t getIdxMatrix() const;
	size_t getIdxFrom() const;
	size_t getIdxTo() const;

	/*
	 * Produce the "instructions" corresponding to the operations.
	 * The instructions encompass the instructions for the matrix coefficient and
	 * the scaling factor for the upper matrix (only).
	 */
	virtual std::vector<MultStore> toInstructions(double &scalingFactor,
												  std::vector<double>& matrix,
												  std::vector<double>& frequencies,
												  std::vector<double>& freqDotFreq,
												  std::vector<double> &coefficients) const = 0;

protected:

	const size_t idxFrom, idxTo, idxMatrix;

};

// Functors used for sorting operations from min idxMatrix to max idxMatrix.
struct OperationMaxIdxMatrixCompare : public std::binary_function<const Base*, const Base*, bool> {
	bool operator()(const Base* lhs, const Base* rhs) const {
		return lhs->getIdxMatrix() < rhs->getIdxMatrix();
	}
};

} /* namespace Operations */
} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */



#endif /* OPERATION_H_ */
