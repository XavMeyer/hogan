//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleOmegaMF.h
 *
 * @date Jul 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef SINGLEOMEGAMF_H_
#define SINGLEOMEGAMF_H_

#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDot.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.h"
#include "SingleSubCodonMF.h"

#include <boost/assign/list_of.hpp>

namespace MolecularEvolution {
namespace MatrixUtils {

class SingleOmegaMF: public SingleSubCodonMF {
public:
	SingleOmegaMF(const nuclModel_t aNucModel);
	~SingleOmegaMF();

private:
	typedef SingleSubCodonMF super;
	const nuclModel_t nucModel;

	const size_t OMEGA_POS;

	size_t defineNParameters(const nuclModel_t aNucModel) const;
	int defineThetaIdx(char from, char to) const;

	void doInitOperations();
	Operations::Base* defineOperation(size_t iFrom, size_t iTo) const;

};

} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */

#endif /* SINGLEOMEGAMF_H_ */
