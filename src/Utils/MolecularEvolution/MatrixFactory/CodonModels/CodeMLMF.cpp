//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodeMLMF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "CodeMLMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

const size_t CodeML_MF::CODEML_NB_COEFF = 2;

CodeML_MF::CodeML_MF(const bool aComputeSMatrix) :
		SingleSubCodonMF(CODEML_NB_COEFF, false), computeSMatrix(aComputeSMatrix) { // no stop codon
	MatrixFactory::initOperations();
}

CodeML_MF::~CodeML_MF() {
}

void CodeML_MF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}


Operations::Base* CodeML_MF::defineOperation(size_t iFrom, size_t iTo) const {
	using std::string;

	const MD::Nucleotides *nucl = MD::getNucleotides();
	string codonFrom = MD::getCodons()->getCodonVector()[iFrom];
	string codonTo = MD::getCodons()->getCodonVector()[iTo];

	// Check if its a transition or a transversion
	size_t iPos = SingleSubCodonMF::findChangedPosition(codonFrom, codonTo);
	bool isTransition = nucl->isTransition(codonFrom[iPos], codonTo[iPos]);

	// Check if its synonymous or not
	bool isSynonymous = MD::getCodons()->isSynonymous(codonFrom, codonTo);

	std::vector<size_t> coeffId;
	if(isTransition) coeffId.push_back(0); // Its a transition, push back id of kappa
	if(!isSynonymous) coeffId.push_back(1); // Its non-synonymous, push back id of omega

	Operations::Base* myOp = NULL;
	if(computeSMatrix) {
		/*Operations::CoeffsDot cd(iFrom, iTo, N, coeffId);
		cdVec.push_back(cd);
		myOp = &cdVec.back();*/
		myOp = new Operations::CoeffsDot(iFrom, iTo, N, coeffId);
	} else {
		myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);
	}

	//std::cout << myOp->getIdxMatrix() << std::endl;
	return myOp;
}

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
