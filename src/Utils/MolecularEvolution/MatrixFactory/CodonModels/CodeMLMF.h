//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodeMLMF.h
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief The CodeML_MF class represent the required implementation for the CodeML model
 *
 * Coefficient required to form the matrix are the following [kappa, omega].
 * We use a Operations::CoeffsDotFreq. If there has been a transision we add kappa idx (0).
 * If it is a non-synonymous change we add omega idx (1).
 *
 * It is possible to compute the S matrix matrix instead of the Q matrix by setting
 * aCopmuteSMatrix to true. Otherwise the Q matrix is computed.
 * (Q = S dot CodonFreq)
 *
 */
#ifndef CodeMLMF_H_
#define CodeMLMF_H_

#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDot.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.h"
#include "SingleSubCodonMF.h"

#include <boost/assign/list_of.hpp>

namespace MolecularEvolution {
namespace MatrixUtils {

class CodeML_MF: public SingleSubCodonMF {
public:
	CodeML_MF(const bool aComputeSMatrix = false);
	~CodeML_MF();

private:

	typedef SingleSubCodonMF super;

	static const size_t CODEML_NB_COEFF;

	const bool computeSMatrix;

	void doInitOperations();

	//size_t findChangedPosition(const std::string &codonFrom, const std::string &codonTo) const;

	Operations::Base* defineOperation(size_t iFrom, size_t iTo) const;
};

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* CodeMLMF_H_ */
