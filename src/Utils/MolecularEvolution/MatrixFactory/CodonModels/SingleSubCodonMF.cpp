//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleSubCodonMF.cpp
 *
 * @date Feb 4, 2015
 * @author meyerx
 * @brief
 */
#include "SingleSubCodonMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

SingleSubCodonMF::SingleSubCodonMF(const size_t aNB_COEFF, const bool aWithStopCodon) :
		MatrixFactory(aWithStopCodon ? MD::getCodons()->NB_CODONS : MD::getCodons()->NB_CODONS_WO_STOP, aNB_COEFF),
		withStopCodon(aWithStopCodon) {

	/*if(withStopCodon) {
		N = MD::getCodons()->NB_CODONS;
	} else {
		N = MD::getCodons()->NB_CODONS_W_STOP;
	}

	matrix.assign(N*N, 0.);*/
}

SingleSubCodonMF::SingleSubCodonMF(const size_t aNB_COEFF, const bool aWithStopCodon, const bool aDoRowsSumtoZero) :
		MatrixFactory(aWithStopCodon ? MD::getCodons()->NB_CODONS : MD::getCodons()->NB_CODONS_WO_STOP, aNB_COEFF, aDoRowsSumtoZero),
		withStopCodon(aWithStopCodon) {


	/*if(withStopCodon) {
		N = MD::getCodons()->NB_CODONS;
	} else {
		N = MD::getCodons()->NB_CODONS_W_STOP;
	}*/
}

SingleSubCodonMF::~SingleSubCodonMF() {
}

void SingleSubCodonMF::scanCombination() {
	using std::string;
	using std::vector;

	const vector<string> &codonVec = MD::getCodons()->getCodonVector();

	// Diagonal operations
	//Operations::Diagonal* diagOperations[N];

	// For each codon
	for (size_t iFrom = 0; iFrom < N; ++iFrom) {
		if(doRowsSumtoZero) { // If we want rows to sum to 0, init a diagOperation
			diagonalOperations[iFrom] = new Operations::Diagonal(iFrom, iFrom, N);
		}

		// Init codon
		string baseCodon = codonVec[iFrom];

		// for each position of the codon
		for (size_t iPos = 0; iPos < MD::getCodons()->NB_NUCL_PER_CODON; ++iPos) {
			scanSubstitution(iFrom, iPos, baseCodon);
		}
	}

	// Diagonal values
	if(!doRowsSumtoZero){
		for (size_t iDiag = 0; iDiag < N; ++iDiag) {
			Operations::Base *op = defineOperation(iDiag, iDiag);
			operations.push_back(op);
		}
	}
}

void SingleSubCodonMF::scanSubstitution(const size_t iFrom, const size_t iPos,
										const std::string &baseCodon) {
	using std::string;
	using std::vector;

	const string NUC_BASE(MD::getNucleotides()->NUCL_BASE, MD::getNucleotides()->NB_BASE_NUCL);

	// For each Nucleotide
	for (size_t iN = 0; iN < NUC_BASE.size(); ++iN) {

		if(baseCodon[iPos] != NUC_BASE[iN]) { // We only want to see substitutions
			// Init codon
			string toCodon = baseCodon;
			toCodon[iPos] = NUC_BASE[iN];

			size_t iTo = MD::getCodons()->getCodonIdx(toCodon);
			if(iTo < N) { // Use to drop STOP codon if we have withStopCodon=false
				// add the operation
				Operations::Base *op = defineOperation(iFrom, iTo);
				operations.push_back(op);

				// If diagonal sum to 0 add the element to the diagonal
				if(doRowsSumtoZero) { // If we want rows to sum to 0, build the sum of term
					diagonalOperations[iFrom]->addOffDiagOperation(op);
				}
			}
		}
	}
}

size_t SingleSubCodonMF::findChangedPosition(const std::string &codonFrom, const std::string &codonTo) const {
	// Get the position of change
	int posChange = -1;
	for(size_t i=0; i<MD::getCodons()->NB_NUCL_PER_CODON; ++i) {
		if(codonTo[i] != codonFrom[i]) {
			if(posChange < 0) {
				posChange = i;
			} else { // Check for multiple substitution : shouldn't happen
				std::cerr << "Operations::Base* CodeML_MF::defineOperation(size_t iFrom, size_t iTo) const;" << std::endl;
				std::cerr << "Error : Only one change should happen. CodonFrom='" << codonFrom << "' and CodonTo='" << codonTo << "'." << std::endl;
				abort();
			}
		}
	}

	if(posChange < 0) { // check for no substitution : shouldn't happen
		std::cerr << "Operations::Base* CodeML_MF::defineOperation(size_t iFrom, size_t iTo) const;" << std::endl;
		std::cerr << "Error : No change between both codon. CodonFrom='" << codonFrom << "' and CodonTo='" << codonTo << "'." << std::endl;
		abort();
	}

	return posChange;
}



} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */
