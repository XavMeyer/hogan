//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleOmegaMF.cpp
 *
 * @date Jul 14, 2015
 * @author meyerx
 * @brief
 */
#include <Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h>

namespace MolecularEvolution {
namespace MatrixUtils {



SingleOmegaMF::SingleOmegaMF(const nuclModel_t aNucModel) :
		SingleSubCodonMF(defineNParameters(aNucModel), false), nucModel(aNucModel), OMEGA_POS(NB_COEFF-1){
	MatrixFactory::initOperations();
}

SingleOmegaMF::~SingleOmegaMF() {
}

size_t SingleOmegaMF::defineNParameters(const nuclModel_t aNucModel) const {
	if(aNucModel == GTR) {
		return 1+5; // omega + 5-GTR model
	} else if(aNucModel == K80){
		return 1+1; // omega + kappa
	} else {
		std::cerr << "size_t SingleOmegaMF::defineNParameters(const nucleotideModel_t aNucModel) const;" << std::endl;
		std::cerr << "Error : Nucleotide type is not supported by the SingleOmegaMF matrix factory." << std::endl;
		abort();
		return 0;
	}
}

int SingleOmegaMF::defineThetaIdx(char from, char to) const {
	const MD::Nucleotides *nucl = MD::getNucleotides();
	int iFrom = nucl->getNucleotideIdx(from);
	int iTo = nucl->getNucleotideIdx(to);

	if((iFrom < 0) || (iTo < 0)) {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
	}

	// We want the nucleotide with smallest index to be in from
	if(iFrom > iTo) std::swap(from, to);

	// For each possible nucleotide transition return the index
	if(from == 'T' && to == 'G') {
		return 0;
	} else if(from == 'C' && to == 'T') {
		return 1;
	} else if(from == 'A' && to == 'C') {
		return 2;
	} else if(from == 'C' && to == 'G') {
		return 3;
	} else if(from == 'A' && to == 'T') {
		return 4;
	} else if(from == 'A' && to == 'G') {
		return 5;
	} else {
		std::cerr << "size_t BranchSiteREL_MF::defineThetaIdx(char from, char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
		return -1;
	}
}


void SingleOmegaMF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}


Operations::Base* SingleOmegaMF::defineOperation(size_t iFrom, size_t iTo) const {
	using std::string;

	const MD::Nucleotides *nucl = MD::getNucleotides();
	string codonFrom = MD::getCodons()->getCodonVector()[iFrom];
	string codonTo = MD::getCodons()->getCodonVector()[iTo];

	// Found changed position
	size_t iPos = SingleSubCodonMF::findChangedPosition(codonFrom, codonTo);

	// Check if its synonymous or not
	bool isSynonymous = MD::getCodons()->isSynonymous(codonFrom, codonTo);

	std::vector<size_t> coeffId;

	if(nucModel == K80) {
		// Check if its a transition or a transversion
		bool isTransition = nucl->isTransition(codonFrom[iPos], codonTo[iPos]);
		if(isTransition) coeffId.push_back(0); // Its a transition, push back id of kappa
	} else if(nucModel == GTR) {
		// Get correct theta index
		size_t thetaIdx = defineThetaIdx(codonFrom[iPos], codonTo[iPos]);
		if(thetaIdx < 5) coeffId.push_back(thetaIdx); // Its not the transition T<->G fixed at 1.
	} else {
		std::cerr << "Operations::Base* SingleOmegaMF::defineOperation(size_t iFrom, size_t iTo) const;" << std::endl;
		std::cerr << "Error : Nucleotide type is not supported by the SingleOmegaMF matrix factory." << std::endl;
		abort();
	}

	if(!isSynonymous) coeffId.push_back(OMEGA_POS); // Its non-synonymous, push back id of omega

	Operations::Base* myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);

	//std::cout << myOp->getIdxMatrix() << std::endl;
	return myOp;
}


} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */
