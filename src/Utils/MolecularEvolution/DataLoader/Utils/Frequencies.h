//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Frequencies.h
 *
 * @date Feb 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef FREQUENCIES_H_
#define FREQUENCIES_H_

#include <vector>
#include <algorithm>
#include <iostream>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace MolecularEvolution {
namespace DataLoader {
namespace Utils {

class Frequencies {
public:
	Frequencies(const size_t aN);
	Frequencies(const size_t aN, const std::vector<double> &aFrequencies);
	~Frequencies();

	void set(const std::vector<double> &aFrequencies);

	const size_t size() const;
	const std::vector<double>& getStd() const;
	const Eigen::VectorXd& getEigen() const;
	const Eigen::VectorXd& getInvEigen() const;
	const Eigen::VectorXd& getSqrtEigen() const;
	const Eigen::VectorXd& getSqrtInvEigen() const;

	const Eigen::VectorXf& getEigenFlt() const;
	const Eigen::VectorXf& getInvEigenFlt() const;
	const Eigen::VectorXf& getSqrtEigenFlt() const;
	const Eigen::VectorXf& getSqrtInvEigenFlt() const;

	size_t getNValidFreq() const;
	const std::vector<bool>& getValidFreq() const;

private:

	const size_t N;

	std::vector<bool> validFreq;
	std::vector<double> frequencies;
	Eigen::VectorXf freqFlt, invFreqFlt, sqrtFreqFlt, sqrtInvFreqFlt;
	Eigen::VectorXd freq, invFreq, sqrtFreq, sqrtInvFreq;

	void init(const std::vector<double> &aFrequencies);

};

} /* namespace Utils */
} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* FREQUENCIES_H_ */
