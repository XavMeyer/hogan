//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MSA.cpp
 *
 * @date Feb 17, 2015
 * @author meyerx
 * @brief
 */
#include "MSA.h"

namespace MolecularEvolution {
namespace DataLoader {

MSA::MSA(const sequence_t &aSeqType, std::vector<Alignment> &aAligns, const bool aWithStops) :
		withStops(aWithStops), seqType(aSeqType), alignments(aAligns){

	nNucl = aAligns.front().getNbNucleotides();
	if(seqType == NUCLEOTIDE) {
		nSite = nNucl;
	} else if (seqType == CODON) {
		if (nNucl % 3 != 0) {
			std::cerr << "MSA::MSA(const sequence_t &aSeqType, const std::vector<Alignment> &aAligns, const bool withStops);" << std::endl;
			std::cerr << "Warning : Number of nucleotide is not divisible by three." << std::endl;
			abort();
		}
		nSite = nNucl/3;
	}

	 checkAlignments(alignments);

	if(seqType == NUCLEOTIDE) {
		checkValidNucleotide();
	} else {
		checkValidCodon();
	}

	sortByDistance();
}

MSA::~MSA() {
}

void MSA::checkAlignments(const std::vector<Alignment> &aAligns) {

	for(size_t iA=0; iA<aAligns.size(); ++iA){
		if(aAligns[iA].getNbNucleotides() != nNucl){
			std::cerr << "void MSA::addAlignement(Alignment &aAlign);" << std::endl;
			std::cerr << "Error : Alignment [ " << aAligns[iA].getName() << " ] doesn't have the number of nucleotides." << std::endl;
		}
	}
}

void MSA::checkValidNucleotide() {
	const size_t NB_BASE_NUCL = MolecularEvolution::Definition::Nucleotides::NB_BASE_NUCL;

	// Prepare
	siteValidity.assign(nSite, true);

	// Check if GAP
	for(size_t iN=0; iN<nSite; ++iN) { 	// For each position
		for(size_t iA=0; iA<alignments.size(); ++iA) { 	// For each position
			bool hasGap = alignments[iA].getNucleotideSequence(iN).size() == NB_BASE_NUCL;
			siteValidity[iN] = siteValidity[iN] && !hasGap;
		}
	}
}

void MSA::checkValidCodon() {
	// Prepare
	size_t N = 0;
	const size_t nCodon = nSite;
	siteValidity.assign(nCodon, true);
	if(withStops) {
		N = MolecularEvolution::Definition::Codons::NB_CODONS;
	} else {
		N = MolecularEvolution::Definition::Codons::NB_CODONS_WO_STOP;
	}

	// Check if GAP and/or STOPS codon
	for(size_t iC=0; iC<nCodon; ++iC) { 	// For each position
		bool allGaps = true; /* NEW WAY */
		bool hasEmpty = false; /* NEW WAY */
		for(size_t iA=0; iA<alignments.size(); ++iA) { 	// For each position
			Alignment::vecCode_t code = alignments[iA].getCodonSequence(iC, withStops);
			bool hasGap = code.size() == N;
			allGaps = allGaps && hasGap; /* NEW WAY */
			hasEmpty = hasEmpty || code.empty(); // Means has only stop codon

			if (code.empty()) std::cout << "Is stop : " << iA << " -- " << iC <<  std::endl;

			/* OLD WAY
			siteValidity[iC] = siteValidity[iC] && !hasGap;
			siteValidity[iC] = siteValidity[iC] && (withStops || !isStop);
			*/
		}

		/* NEW WAY */
		// its valid if there is they aren't all with gaps and there is no stops
		siteValidity[iC] = !allGaps && !hasEmpty;
		if(!siteValidity[iC]) std::cout << "Not valid site pos : " << iC << (allGaps ? " - all Gaps " : "- n") <<  (hasEmpty ? " - has stops " : "- n")  <<  std::endl;
	}
}


MSA::sequence_t MSA::getSequenceType() const {
	return seqType;
}

size_t MSA::getNSite() const {
	return nSite;
}

size_t MSA::getNValidSite() const {
	return std::count(siteValidity.begin(), siteValidity.end(), true);
}

size_t MSA::getNAlignments() const {
	return alignments.size();
}

const std::vector<bool>& MSA::getSiteValidity() const {
	return siteValidity;
}

void MSA::sortByDistance() {
	if(seqType == NUCLEOTIDE) {
		sortByNucleotideDistance();
	} else if (seqType == CODON) {
		sortByCodonDistance();
	}
}

void MSA::sortByNucleotideDistance() {

	for(size_t iA=0; iA < alignments.size()-1; ++iA) { // Current alignment
		size_t nearestAlign = 0;
		double smallestDist = std::numeric_limits<double>::max();
		for(size_t jA=iA+1; jA < alignments.size(); ++jA) { // Next unsorted alignments

			double dist = alignments[iA].getNucleotideDistance(alignments[jA], siteValidity);
			if(dist < smallestDist) { // if its smaller than the smallest distance, we keep it.
				nearestAlign = jA;
				smallestDist = dist;
				// If it's the same, we keep it directly
				if(smallestDist == 0.) break;
			}
		}
		// Swap the nearest alignment to the next place
		std::swap(alignments[iA+1], alignments[nearestAlign]);
		//siteValidity.swap(siteValidity[iA+1], siteValidity[nearestAlign]);
	}

}

void MSA::sortByCodonDistance() {
	//std::cout << "Valid site : " << getNValidSite() << std::endl;

	for(size_t iA=0; iA < alignments.size()-1; ++iA) { // Current alignment
		size_t nearestAlign = 0;
		double smallestDist = std::numeric_limits<double>::max();
		for(size_t jA=iA+1; jA < alignments.size(); ++jA) { // Next unsorted alignments

			double dist = alignments[iA].getCodonDistance(alignments[jA], siteValidity);
			if(dist < smallestDist) { // if its smaller than the smallest distance, we keep it.
				nearestAlign = jA;
				smallestDist = dist;
				// If it's the same, we keep it directly
				if(smallestDist == 0.) break;
			}
		}
		// Swap the nearest alignment to the next place
		std::swap(alignments[iA+1], alignments[nearestAlign]);
		//siteValidity.swap(siteValidity[iA+1], siteValidity[nearestAlign]);
		//std::cout << "Valid site : " << getNValidSite() << std::endl;
	}

}

const Alignment& MSA::getAlignment(const std::string &name) const {

	std::vector<Alignment>::const_iterator it =
			std::find_if(alignments.begin(), alignments.end(),
						 FindAlignmentByName(name));

	if (it == alignments.end()) {
		std::cerr << "const Alignment& MSA::getAlignment(const std::string &name) const" << std::endl;
		std::cerr << "Error : alignment named '" << name << "'does not exist." << std::endl;
		abort();
	}

	return *it;
}

const std::vector<Alignment>& MSA::getAlignments() const {
	return alignments;
}

Alignment::vecCode_t MSA::getFullSiteCode(const std::string &alignementName, const size_t posSite) const {
	const Alignment& align = getAlignment(alignementName);

	if(seqType == NUCLEOTIDE) {
		return align.getNucleotideSequence(posSite);
	} else if(seqType == CODON) {
		return align.getCodonSequence(posSite, withStops);
	}

	std::cerr << "const std::vector<int>& MSA::getFullSiteCode(const std::string &alignementName, const size_t posSite) const;" << std::endl;
	std::cerr << "Position : [" << posSite << "] not found." << std::endl;
	abort();

	return align.getNucleotideSequence(0);
}

Alignment::vecCode_t MSA::getValidSiteCode(const std::string &alignementName, const size_t posValidSite) const {
	const Alignment& align = getAlignment(alignementName);

	size_t cnt=0;
	for(size_t iS=0; iS<getNSite(); ++iS){
		if(siteValidity[iS]) { // Count the number of valid codon encountered
			++cnt;
		}

		// Since posValidSize is 0 index, we add 1.
		// If we are at the cnt'th valid element
		if(cnt == (posValidSite+1)){
			if(seqType == NUCLEOTIDE) {
				return align.getNucleotideSequence(iS);
			} else if(seqType == CODON) {
				return align.getCodonSequence(iS, withStops);
			}
		}
	}

	std::cerr << "const std::vector<int>& MSA::getCodonIdx(const std::string &alignementName, const size_t posValidSite) const;" << std::endl;
	std::cerr << "Position : [" << posValidSite << " not found." << std::endl;
	abort();

	return align.getCodonSequence(0, withStops);
}

bool MSA::isWithStops() const {
	return withStops;
}

size_t MSA::countAllGaps() const {
	size_t nSymbol = 0;
	if(seqType == NUCLEOTIDE) {
		nSymbol = MolecularEvolution::Definition::Nucleotides::NB_BASE_NUCL;
	} else {
		if(withStops) {
			nSymbol = MolecularEvolution::Definition::Codons::NB_CODONS;
		} else {
			nSymbol = MolecularEvolution::Definition::Codons::NB_CODONS_WO_STOP;
		}
	}

	size_t count = 0;
	for(size_t iS=0; iS<getNSite(); ++iS){
		bool allGap = true;
		for(size_t iA=0; iA<getNAlignments(); ++iA){
			if(seqType == NUCLEOTIDE) {
				if(alignments[iA].getNucleotideSequence(iS).size() != nSymbol) {
					allGap = false;
					break;
				}
			} else if(seqType == CODON) {
				if(alignments[iA].getCodonSequence(iS, withStops).size() != nSymbol) {
					allGap = false;
					break;
				}
			}
		}

		if(allGap) count++;
	}
	return count;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
