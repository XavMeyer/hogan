//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MSA.h
 *
 * @date Feb 17, 2015
 * @author meyerx
 * @brief
 */
#ifndef MSA_H_
#define MSA_H_

#include <string>
#include <boost/bind.hpp>

#include "Alignment.h"

namespace MolecularEvolution {
namespace DataLoader {

class MSA {
public:
	enum SEQUENCE_TYPE {NUCLEOTIDE=0, CODON=1};
	typedef SEQUENCE_TYPE sequence_t;

public:
	MSA(const sequence_t &aSeqType, std::vector<Alignment> &aAligns, const bool aWithStops=false);
	~MSA();

	void sortByDistance();

	sequence_t getSequenceType() const;

	size_t getNSite() const;
	size_t getNValidSite() const;
	size_t getNAlignments() const;

	const std::vector<bool>& getSiteValidity() const;

	const Alignment& getAlignment(const std::string &name) const;
	const std::vector<Alignment>& getAlignments() const;
	Alignment::vecCode_t getFullSiteCode(const std::string &alignementName, const size_t posSite) const;
	Alignment::vecCode_t getValidSiteCode(const std::string &alignementName, const size_t posValidSite) const;

	bool isWithStops() const;

	size_t countAllGaps() const;

private:
	const bool withStops;
	const sequence_t seqType;
	size_t nNucl, nSite;
	std::vector<Alignment> &alignments;
	std::vector<bool> siteValidity;

	void sortByCodonDistance();
	void sortByNucleotideDistance();

	void checkValidNucleotide();
	void checkValidCodon();

	void checkAlignments(const std::vector<Alignment> &aAligns);

};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* MSA_H_ */
