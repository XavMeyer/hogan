//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CompressedAlignements.cpp
 *
 * @date Nov 25, 2015
 * @author meyerx
 * @brief
 */
#include "CompressedAlignements.h"

namespace MolecularEvolution {
namespace DataLoader {

CompressedAlignements::CompressedAlignements(const MSA &aMSA, bool aOnlyValidSite, bool aRemoveAllGap) :
		onlyValidSite(aOnlyValidSite), removeAllGap(aRemoveAllGap), msa(aMSA),
		alignmentCodesLength(msa.getNAlignments()), alignmentCodes(msa.getNAlignments()) {
	nCompressed = 0;
	compress();
}

CompressedAlignements::~CompressedAlignements() {
	for(size_t iA=0; iA<alignmentCodes.size(); ++iA) {
		for(size_t iP=0; iP<alignmentCodes[iA].size(); ++iP) {
			delete [] alignmentCodes[iA][iP];
		}
		alignmentCodes[iA].clear();
	}
	alignmentCodes.clear();
}

size_t CompressedAlignements::getNSite() const {
	assert(!alignmentCodes.empty());
	return alignmentCodes.front().size();
}

size_t CompressedAlignements::getNCompressed() const {
	return nCompressed;
}

Alignment::vecCode_t CompressedAlignements::getSiteCode(const std::string &alignementName, const size_t posSite) const {

	namesMap_t::const_iterator it;
	it = namesMap.find(alignementName);
	if(it != namesMap.end()) {
		assert(posSite < getNSite());
		return createCode(it->second, posSite);//alignmentCodes[it->second][posSite];
	} else {
		std::cerr << "const std::vector<int>& CompressedAlignements::getSiteCode(const std::string &alignementName, const size_t posSite) const;" << std::endl;
		std::cerr << "Alignement name : [" << alignementName << "] not found." << std::endl;
		abort();

	}

	return createCode(0, 0);
}

double CompressedAlignements::getSiteFactor(const size_t posSite) const {
	assert(posSite < getNSite());
	return factor[posSite];
}


std::vector< std::vector<bool> > CompressedAlignements::getSignatures() {

	size_t nSite = msa.getNSite();
	if(onlyValidSite) {
		nSite = msa.getNValidSite();
	}

	// Get alignments
	const std::vector<Alignment>& aligns = msa.getAlignments();
	// Prepare signature vector
	std::vector< std::vector<bool> > signatures(nSite);
	// Get Number of possible symbols
	size_t nSymbol = 0;
	if(msa.getSequenceType() == MSA::NUCLEOTIDE) {
		nSymbol = Definition::Nucleotides::NB_BASE_NUCL;
	} else if(msa.getSequenceType() == MSA::CODON) {
		nSymbol = Definition::Codons::NB_CODONS;
	}


	// Compute the signatures
	for(size_t iA=0; iA<aligns.size(); ++iA) { // For each alignments
		namesMap[aligns[iA].getName()] = iA;

		for(size_t iS=0; iS<nSite; ++iS) { // For each sites
			// Get the site code
			Alignment::vecCode_t code;
			if(onlyValidSite) {
				code = msa.getValidSiteCode(aligns[iA].getName(), iS);
			} else {
				if(msa.getSequenceType() == MSA::NUCLEOTIDE) {
					code = aligns[iA].getNucleotideSequence(iS);
				} else if(msa.getSequenceType() == MSA::CODON) {
					code = aligns[iA].getCodonSequence(iS, msa.isWithStops());
				}
			}

			// Get the site signature
			std::vector<bool> siteSign(nSymbol, false);
			for(size_t i=0; i<code.size(); ++i){
				siteSign[code[i]] = true;
			}

			// Add to the site signature
			signatures[iS].insert(signatures[iS].end(), siteSign.begin(), siteSign.end());
		}
	}

	return signatures;
}


void CompressedAlignements::compress() {
	// Get alignments
	const std::vector<Alignment>& aligns = msa.getAlignments();

	// Get the signatures
	std::vector< std::vector<bool> > signatures = getSignatures();
	std::vector<bool> allGap(signatures.front().size(), true);

	// Compress
	Alignment::vecCode_t code;
	std::vector<bool> compressed(signatures.size(), false);
	for(size_t iS=0; iS<signatures.size(); ++iS) { // For each CPVs
		// If this site is already compressed, continue
		if(compressed[iS]) continue;

		if(removeAllGap && signatures[iS] == allGap) {
			continue;
		}

		// Not compressed : Add this codes to alignments
		factor.push_back(1);
		for(size_t iA=0; iA<aligns.size(); ++iA) {
			if(onlyValidSite) {
				code = msa.getValidSiteCode(aligns[iA].getName(), iS);
			} else {
				code.clear();
				if(msa.getSequenceType() == MSA::NUCLEOTIDE) {
					aligns[iA].getNucleotideSequence(iS, code);
				} else if(msa.getSequenceType() == MSA::CODON) {
					aligns[iA].getCodonSequence(iS, msa.isWithStops(), code);
				}
			}
			//alignmentCodes[iA].push_back(code);
			addCodeArray(iA, code);
		}

		for(size_t jS=iS+1; jS<signatures.size(); ++jS) { // For next sites
			// If this site jS is already compressed, continue
			if(compressed[jS]) continue;

			// Else check if it can be compressed
			bool isEqual = signatures[iS] == signatures[jS];
			if(isEqual){
				factor.back()++;
				compressed[jS] = true;
				nCompressed++;
			}
		}
	}
}

Alignment::vecCode_t CompressedAlignements::createCode(size_t iAlign, size_t iPos) const {
	Alignment::vecCode_t code;
	unsigned short int size = alignmentCodesLength[iAlign][iPos];
	for(size_t iC=0; iC<size; ++iC) {
		code.push_back(alignmentCodes[iAlign][iPos][iC]);
	}
	return code;
}

void CompressedAlignements::addCodeArray(size_t iAlign, const Alignment::vecCode_t &code) {
	unsigned short int *array = new unsigned short int[code.size()];

	assert(code.size() < std::numeric_limits<unsigned short int>::max());
	alignmentCodesLength[iAlign].push_back(code.size());

	for(size_t iC=0; iC<code.size(); ++iC) {
		assert(code[iC] < std::numeric_limits<unsigned short int>::max());
		array[iC] = code[iC];
	}
	alignmentCodes[iAlign].push_back(array);
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
