//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Alignment.cpp
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#include "Alignment.h"

namespace MolecularEvolution {
namespace DataLoader {

const size_t Alignment::SIGNATURE_LENGTH = 64;

Alignment::Alignment(const std::string &aName, const std::string &aAlignment) :
		name(aName), alignment(aAlignment) {

	boost::to_upper(alignment);
	std::replace(alignment.begin(), alignment.end(), 'U', 'T');
}

Alignment::~Alignment() {
}

std::string Alignment::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << ">>" << name << " (of size : " << alignment.size() <<  ")";
	ss << std::endl << alignment;

	return ss.str();
}

Alignment::vecCode_t Alignment::getNucleotideSequence(size_t position) const {
	using std::string;
	using std::vector;
	using MolecularEvolution::Definition::Nucleotides;

	assert(position < alignment.size());

	const Nucleotides *nucl = Nucleotides::getInstance();
	string possibleNucl = nucl->ambiguousToBase(alignment[position]);
	vecCode_t code;

	for (size_t iN = 0; iN < possibleNucl.size(); ++iN) {
		int idxNucl = nucl->getNucleotideIdx(possibleNucl[iN]);
		if (idxNucl < 0) {
			std::cerr << "std::vector<int> Alignment::getNucleotideSequence() const ;" << std::endl;
			std::cerr << "Error : Nucleotide [" << idxNucl << "] does not exist." << std::endl;
			abort();
		} else {
			code.push_back(idxNucl);
		}
	}

	return code;
}

void Alignment::getNucleotideSequence(size_t position, Alignment::vecCode_t &code) const {
	using std::string;
	using std::vector;
	using MolecularEvolution::Definition::Nucleotides;

	assert(position < alignment.size());

	const Nucleotides *nucl = Nucleotides::getInstance();
	string possibleNucl = nucl->ambiguousToBase(alignment[position]);

	for (size_t iN = 0; iN < possibleNucl.size(); ++iN) {
		int idxNucl = nucl->getNucleotideIdx(possibleNucl[iN]);
		if (idxNucl < 0) {
			std::cerr << "std::vector<int> Alignment::getNucleotideSequence() const ;" << std::endl;
			std::cerr << "Error : Nucleotide [" << idxNucl << "] does not exist." << std::endl;
			abort();
		} else {
			code.push_back(idxNucl);
		}
	}
}

Alignment::vecCode_t Alignment::getCodonSequence(size_t position, bool withStops) const {

	using std::string;
	using std::vector;
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	const Nucleotides *nuc = Nucleotides::getInstance();
	const Codons *cod = Codons::getInstance();

	assert(position*3 < alignment.size());

	if (alignment.size() % 3 != 0) {
		std::cerr << "std::vector<int> Alignment::getCodonSequence() const ;" << std::endl;
		std::cerr << "Warning : Number of nucleotide is not divisible by three." << std::endl;
		assert(alignment.size() % 3 != 0);
	}

	string possibleNucleotides[3];
	//For each nucleotide
	for (int iN = 0; iN < 3; ++iN) {
		possibleNucleotides[iN] = nuc->ambiguousToBase(alignment[position*3 + iN]);
	}

	vecCode_t code;
	// Form each possible codons given the ambiguous base
	string tmpCodon(3, '-');
	for (size_t iN1 = 0; iN1 < possibleNucleotides[0].size(); ++iN1) {
		tmpCodon[0] = possibleNucleotides[0][iN1];
		for (size_t iN2 = 0; iN2 < possibleNucleotides[1].size(); ++iN2) {
			tmpCodon[1] = possibleNucleotides[1][iN2];
			for (size_t iN3 = 0; iN3 < possibleNucleotides[2].size();
					++iN3) {
				tmpCodon[2] = possibleNucleotides[2][iN3];
				int idxCodon = cod->getCodonIdx(tmpCodon);
				if (idxCodon < 0) {
					std::cerr << "[Error] std::vector<int> Alignment::getCodonSequence() const ;" << std::endl;
					std::cerr << "Codon [" << tmpCodon << "] does not exist." << std::endl;
					assert(idxCodon >= 0);
				} else {
					if(withStops || (!withStops && idxCodon < 61)) { // Remove stop codon if  needed
						code.push_back(idxCodon);
					}
				}
			}
		}
	}

	if(code.empty()) {
		std::cerr << "[Error] std::vector<int> Alignment::getCodonSequence() const" << std::endl;
		std::cerr << "No codon found for position : " << position << " of " << name << std::endl;
		assert(!code.empty());
	}

	return code;
}

void Alignment::getCodonSequence(size_t position, bool withStops, Alignment::vecCode_t &code) const {

	using std::string;
	using std::vector;
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	const Nucleotides *nuc = Nucleotides::getInstance();
	const Codons *cod = Codons::getInstance();

	assert(position*3 < alignment.size());

	if (alignment.size() % 3 != 0) {
		std::cerr << "std::vector<int> Alignment::getCodonSequence() const ;" << std::endl;
		std::cerr << "Warning : Number of nucleotide is not divisible by three." << std::endl;
		assert(alignment.size() % 3 != 0);
	}

	string possibleNucleotides[3];
	//For each nucleotide
	for (int iN = 0; iN < 3; ++iN) {
		possibleNucleotides[iN] = nuc->ambiguousToBase(alignment[position*3 + iN]);
	}

	// Form each possible codons given the ambiguous base
	string tmpCodon(3, '-');
	for (size_t iN1 = 0; iN1 < possibleNucleotides[0].size(); ++iN1) {
		tmpCodon[0] = possibleNucleotides[0][iN1];
		for (size_t iN2 = 0; iN2 < possibleNucleotides[1].size(); ++iN2) {
			tmpCodon[1] = possibleNucleotides[1][iN2];
			for (size_t iN3 = 0; iN3 < possibleNucleotides[2].size();
					++iN3) {
				tmpCodon[2] = possibleNucleotides[2][iN3];
				int idxCodon = cod->getCodonIdx(tmpCodon);
				if (idxCodon < 0) {
					std::cerr << "[Error] std::vector<int> Alignment::getCodonSequence() const ;" << std::endl;
					std::cerr << "Codon [" << tmpCodon << "] does not exist." << std::endl;
					assert(idxCodon >= 0);
				} else {
					if(withStops || (!withStops && idxCodon < 61)) { // Remove stop codon if  needed
						code.push_back(idxCodon);
					}
				}
			}
		}
	}

	if(code.empty()) {
		std::cerr << "[Error] std::vector<int> Alignment::getCodonSequence() const" << std::endl;
		std::cerr << "No codon found for position : " << position << " of " << name << std::endl;
		assert(!code.empty());
	}
}

std::string Alignment::getAligmnmentAsString() const {
	return alignment;
}

size_t Alignment::getNbNucleotides() const {
	return alignment.size();
}

const std::string& Alignment::getName() const {
	return name;
}

double Alignment::getNucleotideDistance(const Alignment &other, const std::vector<bool> &validNucl) const {
	double res = 0.;
	for(size_t iV=0; iV<validNucl.size(); ++iV) {
		if(validNucl[iV]) {
			std::vector<bool> signature1 = createSignature(getNucleotideSequence(iV));
			std::vector<bool> signature2 = createSignature(other.getNucleotideSequence(iV));

			bool areEquals = !std::equal(signature1.begin(), signature1.end(),
					signature2.begin());
			if(!areEquals) res += 1.0;
		}
	}
	return res;
}

double Alignment::getCodonDistance(const Alignment &other, const std::vector<bool> &validCodon) const {
	double res = 0.;
	for(size_t iV=0; iV<validCodon.size(); ++iV) {
		if(validCodon[iV]) {
			std::vector<bool> signature1 = createSignature(getCodonSequence(iV, true));
			std::vector<bool> signature2 = createSignature(other.getCodonSequence(iV, true));
			bool areEquals = !std::equal(signature1.begin(), signature1.end(),
					signature2.begin());
			if(!areEquals) res += 1.0;
		}
	}
	return res;
}

std::vector<bool> Alignment::createSignature(const vecCode_t &intVec) const {
	std::vector<bool> signature(SIGNATURE_LENGTH, false);

	for(size_t i=0; i<intVec.size(); ++i){
		signature[intVec[i]] = true;
	}

	return signature;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
