//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CompressedAlignements.h
 *
 * @date Nov 25, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMPRESSEDALIGNEMENTS_H_
#define COMPRESSEDALIGNEMENTS_H_

#include "MSA.h"

#include <string>
#include <vector>
#include <map>

namespace MolecularEvolution {
namespace DataLoader {

class CompressedAlignements {
public:
	CompressedAlignements(const MSA &aMSA, bool aOnlyValidSite, bool aRemoveAllGap=false);
	~CompressedAlignements();

	size_t getNSite() const;
	size_t getNCompressed() const;
	Alignment::vecCode_t getSiteCode(const std::string &alignementName, const size_t posSite) const;
	double getSiteFactor(const size_t posSite) const;

private:
	const bool onlyValidSite, removeAllGap;
	size_t nCompressed;
	const MSA &msa;
	std::vector< unsigned int > factor;
	std::vector< std::vector <unsigned short int> > alignmentCodesLength;
	std::vector< std::vector <Alignment::code_t*> > alignmentCodes;

	typedef std::map<std::string, size_t> namesMap_t;
	namesMap_t namesMap;

	void compress();
	std::vector< std::vector<bool> > getSignatures();

	Alignment::vecCode_t createCode(size_t iAlign, size_t iPos) const;
	void addCodeArray(size_t iAlign, const Alignment::vecCode_t &code);
};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* COMPRESSEDALIGNEMENTS_H_ */
