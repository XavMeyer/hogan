//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotideFrequencies.cpp
 *
 * @date Feb 18, 2015
 * @author meyerx
 * @brief
 */
#include "NucleotideFrequencies.h"

namespace MolecularEvolution {
namespace DataLoader {

NucleotideFrequencies::NucleotideFrequencies(const MSA &aMSA) : msa(aMSA){
	assert(msa.getSequenceType() == MSA::NUCLEOTIDE);
}

NucleotideFrequencies::~NucleotideFrequencies() {
}

std::vector<double> NucleotideFrequencies::getUniform() {
	using MolecularEvolution::Definition::Nucleotides;

	size_t NB_NUCL = Nucleotides::NB_BASE_NUCL;
	double value = 1./NB_NUCL;

	std::vector<double> nFreq(NB_NUCL, value);

	return nFreq;
}


std::vector<double> NucleotideFrequencies::getEmpiric(bool onlyValid) {
	using MolecularEvolution::Definition::Nucleotides;

	if(!empiricFrequencies.empty()) return empiricFrequencies;

	const std::vector<Alignment> &alignments = msa.getAlignments();
	const std::vector<bool> &validSite = msa.getSiteValidity();

	empiricFrequencies.assign(Nucleotides::getInstance()->NB_BASE_NUCL, 0.);

	for(size_t iA=0; iA<msa.getNAlignments(); ++iA) {  // For each alignment
		// Count each nucleotide
		for(size_t i=0; i<msa.getNSite(); ++i) {
			if(!onlyValid || validSite[i]) {
				Alignment::vecCode_t code = alignments[iA].getNucleotideSequence(i);
				// Increment is 1/(NB_NUCLEOTIDE*NB_AMBIGUOUS_NUCLEOTIDE)
				double val = 1./(double)(code.size());
				for(size_t j=0; j<code.size(); ++j) {
					empiricFrequencies[code[j]] += val;
				}
			}
		}
	}

	double denom = 0;
	if(onlyValid) {
		denom = msa.getNValidSite()*alignments.size();
	} else {
		denom = msa.getNSite()*alignments.size();
	}
	for(size_t i=0; i<empiricFrequencies.size(); ++i) {
		empiricFrequencies[i] /= denom;
	}

	return empiricFrequencies;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
