//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodonFrequencies.cpp
 *
 * @date Feb 18, 2015
 * @author meyerx
 * @brief
 */
#include "CodonFrequencies.h"

namespace MolecularEvolution {
namespace DataLoader {

/// Max number of iterations to resolve codon frequencies in presence of ambiguous codons.
const size_t CodonFrequencies::CODON_FREQ_MAX_ITER  = 20;
/// Max cumulative difference between an iteration and the next to stop iterations to resolve codon frequencies in presence of ambiguous codons.
const double CodonFrequencies::CODON_FREQ_MAX_ERROR = 1e-12;

CodonFrequencies::CodonFrequencies(const MSA &aMSA, const bool aWithStops) : msa(aMSA), withStops(aWithStops) {
	assert(msa.getSequenceType() == MSA::CODON);

}

CodonFrequencies::~CodonFrequencies() {
}



std::vector<double> CodonFrequencies::getCodonF61() {
	using MolecularEvolution::Definition::Codons;

	if(!frequenciesF61.empty()) return frequenciesF61;

	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;
	const std::vector<bool> &validSite = msa.getSiteValidity();
	const std::vector<Alignment> &alignments = msa.getAlignments();

	frequenciesF61.assign(NB_CODON, 0.);

	for(size_t iA=0; iA<msa.getNAlignments(); ++iA) {  // For each alignment

		for(size_t iC=0; iC< msa.getNSite(); ++iC) {
			if(validSite[iC]) {
				Alignment::vecCode_t code = alignments[iA].getCodonSequence(iC, msa.isWithStops());
				// Increment is 1/(NB_CODON*NB_AMBIGUOUS_CODON)
				//double val = 1./(double)(codonSequence[i].size());
				double val = 1.;
				for(size_t j=0; j<code.size(); ++j) {
					frequenciesF61[code[j]] += val;
				}
			}
		}
	}

	double denom = msa.getNValidSite()*alignments.size();
	for(size_t i=0; i<frequenciesF61.size(); ++i) {
		frequenciesF61[i] /= denom;
	}

	return frequenciesF61;
}


void CodonFrequencies::addCodonToF3x4Frequencies(const size_t codonIdx, const double incr,
												 vecDbl2D_t &F3x4, double cntF3x4[]) const {
	using MolecularEvolution::Definition::Codons;
	using MolecularEvolution::Definition::Nucleotides;
	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;

	std::string codon = Codons::getInstance()->getCodonStr(codonIdx);
	for(size_t iPos=0; iPos<NB_NUCL_PER_CODON; ++iPos) { // for each position in codon
		// Get nucleotide index
		const int nucIdx = Nucleotides::getInstance()->getNucleotideIdx(codon[iPos]);
		 // add increment
		F3x4[iPos][nucIdx] += incr;
		cntF3x4[iPos] += incr;
	}
}

std::vector<double> CodonFrequencies::getAmbiguousCodonsProportion(const Alignment::vecCode_t &ambCodons) const {
	double sumFreq = 0;
	std::vector<double> proportions(ambCodons.size(), 0.);
	for(size_t iAC=0; iAC<ambCodons.size(); ++iAC) {
		proportions[iAC] = frequenciesF3x4[ambCodons[iAC]];
		sumFreq += frequenciesF3x4[ambCodons[iAC]];
	}

	for(size_t iAC=0; iAC<ambCodons.size(); ++iAC) {
		proportions[iAC] /= sumFreq;
	}

	return proportions;
}


bool CodonFrequencies::buildF3x4Frequencies(vecDbl2D_t &F3x4) const {
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	bool hasAmbiguous = false;

	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;
	const size_t NB_BASE_NUCL = Nucleotides::getInstance()->NB_BASE_NUCL;

	const std::vector<bool> &validSite = msa.getSiteValidity();
	const std::vector<Alignment> &alignments = msa.getAlignments();

	double cntF3x4[NB_NUCL_PER_CODON];

	// initialize to 0
	F3x4.resize(NB_NUCL_PER_CODON);
	for(size_t i=0; i<NB_NUCL_PER_CODON; ++i) {
		cntF3x4[i] = 0.;
		F3x4[i].assign(NB_BASE_NUCL, 0.);
	}

	for(size_t iA=0; iA<msa.getNAlignments(); ++iA) { // For each alignment

		// Counts nucleotides at each position
		for(size_t iC=0; iC<msa.getNSite(); ++iC) { // For each site in alignement
			// if the site is not valid we skip it
			if(!validSite[iC]) continue;

			Alignment::vecCode_t code = alignments[iA].getCodonSequence(iC, msa.isWithStops());

			// Not ambiguous, we add codon to F3x4
			if(code.size() == 1) {
				addCodonToF3x4Frequencies(code.front(), 1.0, F3x4, cntF3x4);

			// If ambiguous and frequenciesF3x4 exists, we proportionnaly add ambiguous codon to F3x4
			} else if(code.size() > 1) {
				hasAmbiguous = true;

				if(!frequenciesF3x4.empty()) {
					// Get the proportions from base frequencies
					std::vector<double> proportions = getAmbiguousCodonsProportion(code);
					for(size_t iAC=0; iAC < code.size(); ++iAC) { // For each ambiguous codons
						addCodonToF3x4Frequencies(code[iAC], proportions[iAC], F3x4, cntF3x4);
					}
				}
			}
		}
	}

	// Normalize F3x4 frequencies
	for(size_t iPos=0; iPos<NB_NUCL_PER_CODON; ++iPos) {
		for(size_t iN=0; iN<NB_BASE_NUCL; ++iN) {
			F3x4[iPos][iN] /= cntF3x4[iPos];
		}
	}

	return hasAmbiguous;
}

void CodonFrequencies::computeCodonF3x4Frequencies(vecDbl2D_t &F3x4) {
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;
	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;

	// Codon frequencies
	double sumFreq = 0.;
	frequenciesF3x4.assign(NB_CODON, 0.);
	const std::vector<std::string> &cVec = Codons::getInstance()->getCodonVector();

	for(size_t iC=0; iC<NB_CODON; ++iC) { // for each codon
		double val = 1.0;
		for(size_t iN=0; iN<NB_NUCL_PER_CODON; ++iN) { // for each nucleotide in codon
			int nuclIdx = Nucleotides::getInstance()->getNucleotideIdx(cVec[iC][iN]);
			val *= F3x4[iN][nuclIdx];
		}
		frequenciesF3x4[iC] = val;
		sumFreq += val;
	}

	for(size_t iC=0; iC<NB_CODON; ++iC) { // for each codon
		frequenciesF3x4[iC] /= sumFreq;
	}
}

std::vector<double> CodonFrequencies::getCodonF3x4() {
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	// If already defined we return the results
	if(!frequenciesF3x4.empty()) return frequenciesF3x4;

	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;

	// Build F3x4 and compute codon frequencies (first time)
	std::vector<std::vector<double> > F3x4;
	bool hasAmbiguous = buildF3x4Frequencies(F3x4);
	computeCodonF3x4Frequencies(F3x4);

	if(!hasAmbiguous) {
		return frequenciesF3x4;
	} else { // Apply the same algorithm as in FastCodeML
		for(size_t i=0; i<CODON_FREQ_MAX_ITER; ++i) {
			// Save the previous frequencies value
			std::vector<double> oldCodonFreq(frequenciesF3x4);

			// Recompute them (will take account of ambiguous codon this time
			buildF3x4Frequencies(F3x4);
			computeCodonF3x4Frequencies(F3x4);

			// Compute cumulative change over previous value
			double err = 0.;
			for(size_t j=0; j < NB_CODON; ++j) err += fabs(oldCodonFreq[j] - frequenciesF3x4[j]);
			//std::cout << "here... :: " << i << " :: " << err << std::endl;

			// If the frequencies values do not change, stop
			if(err < 1e-12) break;
		}
	}

	return frequenciesF3x4;
}

std::vector<double> CodonFrequencies::getCodonFrequency(const codonFrequencies_t codonFreqType) {
	if(codonFreqType == Uniform) {
		return getCodonUniform();
	} else if(codonFreqType == F61) {
		return getCodonF61();
	} else if(codonFreqType == F3x4) {
		return getCodonF3x4();
	} else {
		std::cerr << "Error : std::vector<double> CodonFrequencies::getCodonFrequency(const codonFrequencies_t codonFreqType); "<< std::endl;
		std::cerr << "Unknown type of codon frequency."<< std::endl;
		abort();
		return std::vector<double>(1,0.);
	}
}

std::vector<double> CodonFrequencies::getCodonUniform() {
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;
	double value = 1./NB_CODON;

	std::vector<double> cFreq(NB_CODON, value);

	return cFreq;
}



} /* namespace DataLoader */
} /* namespace MolecularEvolution */
