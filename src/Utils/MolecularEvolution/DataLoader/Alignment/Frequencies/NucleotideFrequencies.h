//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotideFrequencies.h
 *
 * @date Feb 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef NUCLEOTIDEFREQUENCIES_H_
#define NUCLEOTIDEFREQUENCIES_H_

#include "../MSA.h"

#include <assert.h>

namespace MolecularEvolution {
namespace DataLoader {

class NucleotideFrequencies {
public:
	NucleotideFrequencies(const MSA &msa);
	~NucleotideFrequencies();

	std::vector<double> getUniform();
	std::vector<double> getEmpiric(bool onlyValid = false);

private:
	const MSA &msa;
	std::vector<double> empiricFrequencies;
};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* NUCLEOTIDEFREQUENCIES_H_ */
