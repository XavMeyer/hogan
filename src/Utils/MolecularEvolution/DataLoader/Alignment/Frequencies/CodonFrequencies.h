//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodonFrequencies.h
 *
 * @date Feb 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef CODONFREQUENCIES_H_
#define CODONFREQUENCIES_H_

#include "../MSA.h"

#include <assert.h>

namespace MolecularEvolution {
namespace DataLoader {

class CodonFrequencies {
public:

	enum codonFrequencies_t {Uniform=1, F61=2, F3x4=3} ;

public:
	CodonFrequencies(const MSA &aMSA, const bool aWithStops=false);
	~CodonFrequencies();

	std::vector<double> getCodonFrequency(const codonFrequencies_t codonFreqType);

	std::vector<double> getCodonUniform();
	std::vector<double> getCodonF61();
	std::vector<double> getCodonF3x4();

private:
	typedef std::vector<std::vector<double> > vecDbl2D_t;

	// From FastCodeML, used to compute F3X4 when there are ambiguous codons
	static const size_t CODON_FREQ_MAX_ITER;
	static const double CODON_FREQ_MAX_ERROR;

	const MSA &msa;
	const bool withStops;
	std::vector<double> frequenciesF61, frequenciesF3x4, frequenciesCF3x4;
	bool buildF3x4Frequencies(vecDbl2D_t &F3x4) const;
	void addCodonToF3x4Frequencies(const size_t codonIdx, const double incr,
								   vecDbl2D_t &F3x4, double cntF3x4[]) const;
	std::vector<double> getAmbiguousCodonsProportion(const Alignment::vecCode_t &ambCodons) const;
	void computeCodonF3x4Frequencies(vecDbl2D_t &F3x4);
};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* CODONFREQUENCIES_H_ */
