//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FastaReader.cpp
 *
 * @date Jan 22, 2015
 * @author meyerx
 * @brief
 */
#include "FastaReader.h"

namespace MolecularEvolution {
namespace DataLoader {

FastaReader::FastaReader(const std::string aFileName) {
	readFile(aFileName);
}

FastaReader::~FastaReader() {
}

bool FastaReader::readFile(const std::string &aFileName) {
	using std::ifstream;
	using std::vector;

	ifstream iFile(aFileName.c_str());
	if (!iFile.good()) {
		std::cerr << "bool FastaReader::readFile(const std::string &aFileName)" << std::endl;
		std::cerr << "Fasta file '" << aFileName << "' not found." << std::endl;
		return false;
	}

	// FIXME Check that there is only one alignment per species
	std::vector<std::string> names;

	std::string line, name, content;
	while (std::getline(iFile, line).good()) {
		boost::algorithm::trim(line);
		if (line.empty() || line[0] == '>') { // Identifier marker
			if (!name.empty()) { // Print out what we read from the last entry
				if(std::find(names.begin(), names.end(), name) != names.end()){ // check for alignment unicity per name
					std::cerr << "bool FastaReader::readFile(const std::string &aFileName)" << std::endl;
					std::cerr << "Alignment '" << name << "' is defined more than one time." << std::endl;
				} else {
					names.push_back(name);
				}
				alignments.push_back(Alignment(name, content));
				name.clear();
			}
			if (!line.empty()) {
				name = line.substr(1);
			}
			content.clear();
		} else if (!name.empty()) {
			if (line.find(' ') != std::string::npos) { // Invalid sequence--no spaces allowed
				std::cerr << "Warning : bool FastaReader::readFile(const std::string &aFileName)" << std::endl;
				std::cerr << "Invalid sequence with name " << name << std::endl;
				name.clear();
				content.clear();
			} else {
				content += line;
			}
		}
	}
	if (!name.empty()) { // Print out what we read from the last entry
		alignments.push_back(Alignment(name, content));
	}

	return true;
}

std::vector<Alignment>& FastaReader::getAlignments() {
	return alignments;
}

const std::vector<Alignment>& FastaReader::getAlignments() const {
	return alignments;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
