//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FastaReader.h
 *
 * @date Jan 22, 2015
 * @author meyerx
 * @brief
 */
#ifndef FASTAREADER_H_
#define FASTAREADER_H_

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "Alignment.h"
#include "MSA.h"

namespace MolecularEvolution {
namespace DataLoader {

class FastaReader {
public:
	FastaReader(const std::string aFileName);
	virtual ~FastaReader();

	std::vector<Alignment>& getAlignments();
	const std::vector<Alignment>& getAlignments() const;

private:
	bool readFile(const std::string &aFileName);

	std::vector<Alignment> alignments;

};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* FASTAREADER_H_ */
