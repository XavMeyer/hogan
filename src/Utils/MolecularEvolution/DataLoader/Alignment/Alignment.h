//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Alignment.h
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#ifndef ALIGNMENT_H_
#define ALIGNMENT_H_

#include <sstream>
#include <vector>
#include <map>
#include <string>

#include <boost/lambda/lambda.hpp>
#include <boost/algorithm/string.hpp>

#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"


namespace MolecularEvolution {
namespace DataLoader {

class Alignment {
public:
	typedef unsigned short int code_t;
	typedef std::vector<code_t> vecCode_t;
public:
	Alignment(const std::string &aName, const std::string &aAlignment);
	~Alignment();

	std::string toString() const;

	std::string getAligmnmentAsString() const;
	vecCode_t getNucleotideSequence(size_t position) const;
	vecCode_t getCodonSequence(size_t position, bool withStops) const;

	void getNucleotideSequence(size_t position, Alignment::vecCode_t &code) const;
	void getCodonSequence(size_t position, bool withStops, Alignment::vecCode_t &code) const;

	size_t getNbNucleotides() const;
	const std::string& getName() const;

	double getNucleotideDistance(const Alignment &other, const std::vector<bool> &validNucl) const;
	double getCodonDistance(const Alignment &other, const std::vector<bool> &validCodon) const;

private:

	static const size_t SIGNATURE_LENGTH;

	std::string name;
	std::string alignment;

	std::vector<bool> createSignature(const vecCode_t &intVec) const;

};

struct FindAlignmentByName {
    const std::string &name;
    FindAlignmentByName(const std::string &aName) : name(aName) {}
    bool operator () (const Alignment& align) const
    {
        return align.getName() == name;
    }
};


} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* ALIGNMENT_H_ */
