//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.cpp
 *
 * @date Jan 22, 2015
 * @author meyerx
 * @brief
 */

#include "TreeNode.h"

namespace MolecularEvolution {
namespace DataLoader {

size_t TreeNode::idSeq = 0;

TreeNode::TreeNode() : id(idSeq++), length(0.) {
	/*std::stringstream ss;
	ss << "InternalName_" << id;
	name = ss.str();*/
}


TreeNode::~TreeNode() {

}

string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << length << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i].id << "]" << children[i].name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();

}

std::string TreeNode::subtreeToString() const {
	std::stringstream ss;
	ss << "-------------------------------------------" << std::endl;
	ss << toString() << std::endl;

	for (uint i = 0; i < children.size(); ++i) {
		ss << children[i].subtreeToString();
	}

	return ss.str();
}

void TreeNode::resetIdSeq() {
	idSeq = 0;
}

size_t TreeNode::getId() const {
	return id;
}

string TreeNode::getName() const {
	return name;
}

double TreeNode::getLength() const {
	return length;
}

const TreeNode_children& TreeNode::getChildren() const {
	return children;
}

bool TreeNode::isLeaf() const {
	return children.empty();
}

}
}
