//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Jan 21, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODEDL_H_
#define TREENODEDL_H_

#include <vector>
#include <sstream>

#include <boost/fusion/adapted/adt/adapt_adt.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>

namespace MolecularEvolution {
namespace DataLoader {

using std::string;
using std::vector;

class TreeNode;

typedef vector<TreeNode> TreeNode_children;

class TreeNode {
public:

	TreeNode();
	~TreeNode();

	size_t getId() const;
	string getName() const;
	double getLength() const;
	const TreeNode_children& getChildren() const;

	bool isLeaf() const;

	std::string toString() const;
	std::string subtreeToString() const;

	static void resetIdSeq();

private:
	static size_t idSeq;
	size_t id;

public:
	string name;
	double length;
	TreeNode_children children;

	/*void setName(const string &aName) {
		name = aName;
	}

	void setChildren(const TreeNode_children &aChildren) {
		children = aChildren;
	}

	void setLength(double aLength) {
		length = aLength;
	}

	string getName() const {
		return name;
	}

	const TreeNode_children& getChildren() const {
		return children;
	}

	double getLength() const{
		return length;
	}*/
};

}
}

BOOST_FUSION_ADAPT_STRUCT(MolecularEvolution::DataLoader::TreeNode,
		(MolecularEvolution::DataLoader::TreeNode_children, children) (std::string, name) (double, length))

/*BOOST_FUSION_ADAPT_ADT(
		DataLoader::TreeNode,
		(const DataLoader::TreeNode_children&, const DataLoader::TreeNode_children&, obj.getChildren(), obj.setChildren(val))
		(std::string, const std::string&, obj.getName(), obj.setName(val))
		(double, double, obj.getLength(), obj.setLength(val))
)*/

#endif /* TREENODEDL_H_ */
