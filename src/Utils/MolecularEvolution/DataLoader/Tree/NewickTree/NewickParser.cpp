//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NewickParser.cpp
 *
 * @date Jan 21, 2015
 * @author meyerx
 * @brief
 */
#include "NewickParser.h"

namespace MolecularEvolution {
namespace DataLoader {

NewickParser::NewickParser(const string aString, const stringType_t stringType) :
		fileName(aString) {

	string str;
	if(stringType == IS_FILE_NAME) {
		readFile(str);
	} else {
		str = aString;
	}
	parseFile(str);

}

NewickParser::~NewickParser() {
}

bool NewickParser::readFile(string &str) {
	using std::ifstream;

	ifstream iFile(fileName.c_str());
	if (!iFile.good()) {
		std::cerr << "bool NewickParser::readFile(string &str)" << std::endl;
		std::cerr << "Newick file not found." << std::endl;
		return false;
	}

	getline(iFile, str);

	return !str.empty();
}

bool NewickParser::parseFile(string &str) {
	namespace qi = ::boost::spirit::qi;

	//str = "(B:6.0,(A:5.0,C:3.0,E:4.0):5.0,D:11.0);";
	//str = "(((One:0.1,Two:0.2)Sub1:0.3,(Three:0.4,Four:0.5)Sub2:0.6)Sub3:0.7,Five:0.8)Root:0.9;";
	//str = "(((One:0.2,Two:0.3):0.3,(Three:0.5,Four:0.3):0.2):0.3,Five:0.7):0.0;";
	//std::cout << str << std::endl;

	NewickGrammar<string::const_iterator> grammar;
	// Parse
	string::const_iterator iter = str.begin();
	string::const_iterator end = str.end();
	bool result = qi::phrase_parse(iter, end, grammar, qi::space, root);

	//std::cout << root << std::endl;

	if (!result) {
		std::cerr << "bool NewickParser::parseFile(string &str)" << std::endl;
		std::cerr << "Newick file badly formated." << std::endl;
		return false;
	}

	TreeNode::resetIdSeq();

	return true;
}

const TreeNode& NewickParser::getRoot() const {
	return root;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
