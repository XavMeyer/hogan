//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PartitionStatistics.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef PARTITIONSTATS_TREERECONSTRUCTION_H_
#define PARTITIONSTATS_TREERECONSTRUCTION_H_

#include "DecisionTree.h"
#include "Utils/Code/Algorithm.h"

#include <Eigen/Core>
#include <Eigen/Dense>

#include <fstream>
#include <iomanip>

namespace MolecularEvolution {
namespace TreeReconstruction {

class PartitionStatistics {
public:
	PartitionStatistics(const size_t aMaxIter, const std::vector<FinalNode::sharedPtr_t> &aFinalNode);
	~PartitionStatistics();

	void computeSplitFrequencies(double freqThreshold);
	void writeSplits(const std::string &fname);

private:

	const size_t MAX_ITER;
	std::vector<FinalNode::sharedPtr_t> finalNodes;
	class OrderedSplit;
	std::vector<OrderedSplit> splits;
	Eigen::MatrixXd splitsFreq;
	Eigen::MatrixXi splitsCount;

	struct OrderedSplit {
		double freq;
		std::string splitStr;
		FinalNode::sharedPtr_t fNode;

		bool operator< (const OrderedSplit &rhs) const {
		    if(freq < rhs.freq) {
		    	return true;
		    } else if(freq == rhs.freq) {
		    	return std::lexicographical_compare(splitStr.begin(), splitStr.end(),
		    			rhs.splitStr.begin(), rhs.splitStr.end());
		    } else {
		    	return false;
		    }
		}

		bool operator> (const OrderedSplit &rhs) const {
		    if(freq > rhs.freq) {
		    	return true;
		    } else if(freq == rhs.freq) {
		    	return !std::lexicographical_compare(splitStr.begin(), splitStr.end(),
		    			rhs.splitStr.begin(), rhs.splitStr.end());
		    } else {
		    	return false;
		    }
		}
	};

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* PARTITIONSTATS_TREERECONSTRUCTION_H_ */
