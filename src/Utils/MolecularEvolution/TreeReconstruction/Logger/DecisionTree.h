//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DecisionTree.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef DECISIONTREE_TREERECONSTRUCTION_H_
#define DECISIONTREE_TREERECONSTRUCTION_H_

#include "FinalNode.h"
#include "DecisionNode.h"

#include <vector>
#include <iostream>
#include <algorithm>
#include <boost/functional/hash.hpp>
#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

class DecisionTree {
public:
	DecisionTree();
	~DecisionTree();

	void insertPartition(size_t iteration, const split_t partition);
	const std::vector<FinalNode::sharedPtr_t>& getFinalNodes() const;

private:
	DecisionNode rootNode;
	std::vector<FinalNode::sharedPtr_t> finalNodes;

	DecisionNode* nextNode(const bool isLeft, DecisionNode *cNode);

};



} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* DECISIONTREE_TREERECONSTRUCTION_H_ */
