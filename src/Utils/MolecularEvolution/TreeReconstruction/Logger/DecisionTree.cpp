//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DecisionTree.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "DecisionTree.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

DecisionTree::DecisionTree() {
}

DecisionTree::~DecisionTree() {
	finalNodes.clear();
}

void DecisionTree::insertPartition(size_t iteration, const split_t partition) {

	// Start at root
	DecisionNode *cNode = &rootNode;

	// Left partition should always have the min element
	assert(!partition.front());

	// Go down in the tree
	for(size_t iP=0; iP<partition.size(); ++iP) {
		bool isLeft = partition[iP];
		cNode = nextNode(isLeft, cNode);
	}

	// Create final nodes if not yet existing (new partition)
	if(!cNode->final) {
		cNode->final.reset(new FinalNode());
		cNode->final->partition = partition;
		finalNodes.push_back(cNode->final);
	}
	cNode->final->iterations.push_back(iteration);
}

const std::vector<FinalNode::sharedPtr_t>& DecisionTree::getFinalNodes() const {
	return finalNodes;
}

DecisionNode* DecisionTree::nextNode(const bool isLeft, DecisionNode *cNode) {
	if(isLeft) {
		if(cNode->left == NULL) {
			cNode->left = new DecisionNode();
		}
		return cNode->left;
	} else {
		if(cNode->right == NULL) {
			cNode->right = new DecisionNode();
		}
		return cNode->right;
	}
}




} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
