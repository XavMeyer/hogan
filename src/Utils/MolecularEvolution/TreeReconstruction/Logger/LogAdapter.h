//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogAdapter.h
 *
 * @date Nov 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef LOGADAPTER_H_
#define LOGADAPTER_H_

#include "SplitLogger.h"
#include "Sampler/MCMC.h"
#include "Model/Likelihood/TreeInference/Base.h"
#include "Utils/Code/Algorithm.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Logger {

class LogAdapter {
public:
	LogAdapter(Sampler::MCMC *aSampler, TreeManager::sharedPtr_t aTreeManager);
	~LogAdapter();

	void adaptLog(bool withSplitFrequencies=false);

private:
	Sampler::MCMC *sampler;
	TreeManager::sharedPtr_t treeManager;

	std::vector<size_t> countTreeBinaryLog(std::vector<std::string> &trees, std::map<long int, size_t> &mapping);
	std::vector<size_t> countTreeTracerLog(std::vector<std::string> &trees, std::map<long int, size_t> &mapping);

	std::vector<long int> adaptTracerLog(std::map<long int, size_t> &mapping, std::map<size_t, size_t> &mappingOrderedId);
	std::vector<long int> adaptBinaryLog();

	void writeTrees(std::vector<std::string> &trees, std::vector<size_t> &orderedId, std::vector<size_t> &count) const;
	size_t getTreeHashPosition(std::string &aLine) const;
};

} /* namespace Logger */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* LOGADAPTER_H_ */
