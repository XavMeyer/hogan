//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SplitLogger.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include <Utils/MolecularEvolution/TreeReconstruction/Logger/SplitLogger.h>

namespace MolecularEvolution {
namespace TreeReconstruction {

SplitLogger::SplitLogger(double aBurnin, double aFreqThres, TreeManager::sharedPtr_t aTreeManager, const std::string &aOutFName) :
	BURNIN(aBurnin), FREQUENCY_THRESHOLD(aFreqThres), outFName(aOutFName), treeManager(aTreeManager){

}

SplitLogger::~SplitLogger() {
}

void SplitLogger::processTreeSeq(const std::vector<long int> &treeSeq) {
	// Remove n first elements
	size_t offset = BURNIN*(double)treeSeq.size();

	DecisionTree dt;
	for(size_t iT=offset; iT < treeSeq.size(); ++iT) {
		std::string idString = treeManager->getIdString(treeSeq[iT]);
		Tree::sharedPtr_t aTree(new Tree(idString));
		//Tree::sharedPtr_t aTree = treePool->getTree(treeSeq[iT]);

		if(leaves.empty()) {
			defineLeaves(aTree.get());
		}

		computeSplits(iT, aTree.get(), dt);
	}

	PartitionStatistics pStats(treeSeq.size()-offset, dt.getFinalNodes());
	pStats.computeSplitFrequencies(FREQUENCY_THRESHOLD);
	pStats.writeSplits(outFName);
	writeTaxaOrder(outFName);
}

void SplitLogger::defineLeaves(Tree *aTree) {
	std::vector<TreeNode*> &nodes = aTree->getTerminals();

	for(size_t iL=0; iL< nodes.size(); ++iL) {
		leaf_t leaf = {nodes[iL]->getIdName(), treeManager->getTreeNodeName(nodes[iL])};
		leaves.push_back(leaf);
	}

	std::sort(leaves.begin(), leaves.end());

	for(size_t iL=0; iL< leaves.size(); ++iL) {
		leavesMapping[leaves[iL].id] = iL;
	}
}

void SplitLogger::computeSplits(size_t iT, Tree *aTree, DecisionTree &dt) {
	std::vector<Tree::edge_t> intEdge = aTree->getInternalEdges();

	for(size_t iE=0; iE<intEdge.size(); ++iE) {
		split_t split(leaves.size(), false);
		createSplit(intEdge[iE].n1, intEdge[iE].n2, split);
		if(split.front()) {
			split.flip();
		}
		dt.insertPartition(iT, split);
	}
}

void SplitLogger::createSplit(TreeNode* cNode, TreeNode* pNode, split_t &split) {
	if(cNode->isTerminal()) {
		size_t leafId = leavesMapping[cNode->getIdName()];
		split[leafId] = true;
	} else {
		vecTN_t children = cNode->getChildren(pNode);
		for(size_t iC=0; iC<children.size(); ++iC) {
			createSplit(children[iC], cNode, split);
		}
	}
}

void SplitLogger::writeTaxaOrder(const std::string &fName) {
	std::stringstream ssTax;
	ssTax << fName << ".taxa";
	std::ofstream oFileTax(ssTax.str().c_str());
	oFileTax.fill(' ');
	oFileTax.precision(2);
	oFileTax << std::setw(10) << "id" << "\tTaxon" << std::endl;
	for(size_t iT=0; iT<leaves.size(); ++iT) {
		oFileTax << std::setw(10) << iT << "\t" << leaves[iT].name << std::endl;
	}
	oFileTax.close();
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
