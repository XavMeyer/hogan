//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PartitionStatistics.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "PartitionStatistics.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

PartitionStatistics::PartitionStatistics(const size_t aMaxIter, const std::vector<FinalNode::sharedPtr_t> &aFinalNodes) :
		MAX_ITER(aMaxIter), finalNodes(aFinalNodes) {
}

PartitionStatistics::~PartitionStatistics() {
}

void PartitionStatistics::computeSplitFrequencies(double freqThreshold) {

	// Resize split count to get all splits
	splitsCount.resize(finalNodes.size(), MAX_ITER);

	// Define node to keep and their final frequency
	for(size_t iN=0; iN<finalNodes.size(); ++iN) {
		double nodeFreq = (double)finalNodes[iN]->iterations.size()/(double)MAX_ITER;
		if(nodeFreq > freqThreshold) {
			//splits.push_back(finalNodes[iN]);
			//nodeFreq = round( nodeFreq * 100.0 ) / 100.0;
			OrderedSplit os = {nodeFreq, finalNodes[iN]->getSplitAsString(), finalNodes[iN]};
			splits.push_back(os);
		}
		splitsCount.row(iN) = finalNodes[iN]->getCounts(MAX_ITER);
	}

	// Resize res array
	splitsFreq.resize(splits.size(), MAX_ITER);

	// Order nodes
	//bool ascending = false;
	//std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(finalFreq, ascending);
	//Utils::Algorithm::orderVector(orderedId, splits);
	std::sort(splits.begin(), splits.end(), std::greater<OrderedSplit>());

	// Compute frequencies
	for(size_t iS=0; iS<splits.size(); ++iS) {
		splitsFreq.row(iS) = splits[iS].fNode->getFrequencies(MAX_ITER);
	}
}

void PartitionStatistics::writeSplits(const std::string &fname) {

	std::stringstream ssParts;
	ssParts << fname << ".split";
	std::ofstream oFileParts(ssParts.str().c_str());
	oFileParts.fill(' ');
	oFileParts.precision(4);
	//oFileParts.width(10);
	oFileParts << std::setw(10)<< "id" << "\tFinalFreq \tPartition" << std::endl;
	for(size_t iS=0; iS<splits.size(); ++iS) {
		oFileParts << std::setw(10) << iS << "\t" << std::fixed;
		oFileParts << std::setw(10) << splits[iS].freq << "\t";
		oFileParts << splits[iS].splitStr << std::endl;
	}
	oFileParts.close();

	std::stringstream ssFreqs;
	ssFreqs << fname << ".splitFreq";
	std::ofstream oFileFreqs(ssFreqs.str().c_str());
	oFileFreqs.fill(' ');
	oFileFreqs.precision(3);
	//oFileFreqs.width(10);
	for(size_t iS=0; iS<splits.size(); ++iS) {
		oFileFreqs << std::setw(10) << iS << "\t" << std::scientific;
		for(size_t iI=0; iI<MAX_ITER; ++iI) {
			oFileFreqs << splitsFreq(iS, iI) << "\t";
		}
		oFileFreqs << std::endl;
	}
	oFileFreqs.close();

	std::stringstream ssCounts;
	ssCounts << fname << ".splitCnt";
	std::ofstream oFileCnt(ssCounts.str().c_str());
	oFileCnt.fill(' ');
	oFileCnt.precision(3);
	//oFileFreqs.width(10);
	for(size_t iN=0; iN<finalNodes.size(); ++iN) {
		oFileCnt << std::setw(10) << finalNodes[iN]->getSplitAsString() << "\t" << std::scientific;
		for(size_t iI=0; iI<MAX_ITER; ++iI) {
			oFileCnt << splitsCount(iN, iI) << "\t";
		}
		oFileCnt << std::endl;
	}
	oFileCnt.close();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
