//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalNode.h
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#ifndef FINALNODE_H_
#define FINALNODE_H_

#include <Eigen/Core>
#include <Eigen/Dense>


#include <algorithm>
#include <vector>
#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

typedef std::vector<bool> split_t;

class FinalNode {
public:
	typedef boost::shared_ptr<FinalNode> sharedPtr_t;
public:
	FinalNode();
	~FinalNode();

	double getFrequency(size_t iter);
	Eigen::VectorXd getFrequencies(size_t iter);
	Eigen::VectorXi getCounts(size_t iter);

	std::string getSplitAsString() const;

public:
	split_t partition;
	std::vector<size_t> iterations;
};

struct FindFinalNodeIfGreaterThan {
	FindFinalNodeIfGreaterThan(size_t aLI) : limitIter(aLI) {}
	bool operator()(size_t iter) const {
		return iter > limitIter;
	}
private:
	size_t limitIter;
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* FINALNODE_H_ */
