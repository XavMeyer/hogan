//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogAdapter.cpp
 *
 * @date Nov 12, 2015
 * @author meyerx
 * @brief
 */
#include "LogAdapter.h"

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Logger {

LogAdapter::LogAdapter(Sampler::MCMC *aSampler, TreeManager::sharedPtr_t aTreeManager) :
	sampler(aSampler), treeManager(aTreeManager) {
}

LogAdapter::~LogAdapter() {
}

void LogAdapter::adaptLog(bool withSplitFrequencies) {

	std::vector<std::string> trees;
	std::map<long int, size_t> mapping;
	// Summarize tree
	treeManager->summarizeTrees(trees, mapping);

	// Count tree appearance
	std::vector<size_t> count;
	if(sampler->isBinaryWriter()) {
		count = countTreeBinaryLog(trees, mapping);
	} else {
		count = countTreeTracerLog(trees, mapping);
	}

	// Order trees in function of appearance
	bool ascending = false;
	std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(count, ascending);
	std::map<size_t, size_t> mappingOrderedId = Utils::Algorithm::getOrderedIndicesMapping(count, ascending);

	// Write trees (sorted by descending count)
	writeTrees(trees, orderedId, count);

	// Adapt log file
	std::vector<long int> hashSeq;
	if(sampler->isBinaryWriter()) {
		hashSeq = adaptBinaryLog();
	} else {
		hashSeq = adaptTracerLog(mapping, mappingOrderedId);
	}

	if(withSplitFrequencies) {
		SplitLogger sl(0.0, 0.1, treeManager, sampler->getOutputFullFileName());
		sl.processTreeSeq(hashSeq);
	}
}

std::vector<size_t> LogAdapter::countTreeBinaryLog(std::vector<std::string> &trees, std::map<long int, size_t> &mapping) {
	// TODO Implement.
	std::cerr << "Not yet implemented." << std::endl;
	abort();
}

std::vector<size_t> LogAdapter::countTreeTracerLog(std::vector<std::string> &trees, std::map<long int, size_t> &mapping) {
	std::vector<size_t> count(trees.size(), 0);

	std::string line;
	std::stringstream ssIn;

	ssIn << sampler->getOutputFullFileName() << ".log";
	std::ifstream iFile(ssIn.str().c_str());

	// Copy header
	std::getline(iFile, line);

	// Get tree hash position
	size_t posTH = getTreeHashPosition(line);

	while (std::getline(iFile, line)) {
		// Correct the tree thingy
		// Get words
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t "));

		// Convert from hash to idx
		assert(words.size() > posTH);
		long int hash = atol(words[posTH].c_str()); // get hash
		count[mapping[hash]]++;
	}

	return count;
}

std::vector<long int> LogAdapter::adaptTracerLog(std::map<long int, size_t> &mapping, std::map<size_t, size_t> &mappingOrderedId) {
	std::string line;
	std::stringstream ssIn, ssOut;
	std::vector<long int> hashSeq;

	ssIn << sampler->getOutputFullFileName() << ".log";
	std::ifstream iFile(ssIn.str().c_str());
	ssOut << sampler->getOutputFullFileName() << "_Corr.log";
	std::ofstream oFile(ssOut.str().c_str());

	// Copy header
	std::getline(iFile, line);
	oFile << line << std::endl;

	// Get tree hash position
	size_t posTH = getTreeHashPosition(line);

	while (std::getline(iFile, line)) {
		// Correct the tree thingy
		// Get words
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t "));

		// Convert from hash to idx
		assert(words.size() > posTH);
		long int hash = atol(words[posTH].c_str()); // get hash
		hashSeq.push_back(hash);
		size_t idx = mapping[hash]; // get idx
		size_t sortedIdx = mappingOrderedId[idx];

		std::stringstream ss;
		ss << sortedIdx;
		words[posTH] = ss.str(); // set new value

		// Rewrite
		for(size_t iW=0; iW<words.size()-1; ++iW) {
			oFile << words[iW] << "\t";
		}
		oFile << words.back() << std::endl;
	}

	return hashSeq;
}

std::vector<long int> LogAdapter::adaptBinaryLog() {
	std::vector<long int> hashSeq;
	// TODO Implement.
	std::cerr << "Not yet implemented." << std::endl;
	abort();
	return hashSeq;
}

void LogAdapter::writeTrees(std::vector<std::string> &trees, std::vector<size_t> &orderedId, std::vector<size_t> &count) const {
	std::stringstream ss;
	ss << sampler->getOutputFullFileName() << ".trees";
	std::ofstream oFile(ss.str().c_str());
	oFile.fill(' ');
	oFile.precision(3);

	size_t sum = 0;
	for(size_t i=0; i<count.size(); ++i) {
		sum += count[i];
	}

	for(size_t iT=0; iT<trees.size(); ++iT) {
		size_t idTree = orderedId[iT];
		if(count[idTree] == 0) break;
		oFile << std::setw(4) << iT << "\t" << std::setw(6)<< 100.*((double)count[idTree]/(double)sum) << "\t" << trees[idTree] << std::endl;
	}
}


size_t LogAdapter::getTreeHashPosition(std::string &aLine) const {
	std::vector<std::string> words;
	boost::split(words, aLine, boost::is_any_of("\t "));

	std::vector<std::string>::iterator it = std::find(words.begin(), words.end(),
			StatisticalModel::Likelihood::TreeInference::Base::NAME_TREE_PARAMETER);
	size_t index = std::distance(words.begin(), it);

	if(index == words.size()) {
		std::cerr << "[ERROR] in size_t LogAdapter::getTreeHashPosition(std::string &aLine) const;" << std::endl;
		std::cerr << "Could not find parameter '" << StatisticalModel::Likelihood::TreeInference::Base::NAME_TREE_PARAMETER << "'" << std::endl;
		abort();
	}

	return index;
}

} /* namespace Logger */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
