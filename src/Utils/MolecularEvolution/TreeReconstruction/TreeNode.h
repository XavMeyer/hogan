//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Sep 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODE_TREERECONSTRUCTION_H_
#define TREENODE_TREERECONSTRUCTION_H_

#include "TreeParser/TmpNode.h"
#include "Utils/Code/Algorithm.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

#include <assert.h>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <limits>

namespace MolecularEvolution {
namespace TreeReconstruction {

namespace DL = ::MolecularEvolution::DataLoader;

class TreeNode;

typedef std::vector<TreeNode*> vecTN_t;

class TreeNode {
public:
	TreeNode(const int aIdName);
	TreeNode(const TmpNode &aNode);
	TreeNode(const DL::TreeNode &aNode, const std::map<std::string, int> & nameMapping);
	~TreeNode();

	void addNeighbour(TreeNode *child, bool isParent=false);
	void removeNeighbour(TreeNode *child);

	void setId(size_t aId);
	size_t getId() const;
	int getIdName() const;
	const std::string& getIdString() const;

	size_t getMinId() const;
	void setMinId(size_t aMinId);

	TreeNode* getNeighbour(size_t id);
	const vecTN_t& getNeighbours() const;

	vecTN_t getChildren(const TreeNode *parent) const;
	vecTN_t getNonTerminalChildren(const TreeNode *parent) const;
	TreeNode* getParent() const;
	vecTN_t getChildren() const;

	void setParent(TreeNode *aParent);

	bool isTerminal() const;

	const std::string toString() const;

	void deleteNeighbours(TreeNode *parent=NULL);

	std::string buildOrderedString() const;
	std::string buildOrderedString(const std::vector<std::string> &names) const;
	size_t addNodeToOrderedString(std::string &str, const TreeNode *parent,
			const std::vector<std::string> *names) const;

	void setMySubtreeSize(size_t aSTS);
	size_t getMySubtreeSize() const;
	std::vector<size_t> setSubTreesSize(const TreeNode *parent);

private:

	/* Default data */
	size_t id;
	int idName;
	std::string strIdName;
	TreeNode *parent;
	size_t minId, subtreeSize;

	void setIdString();

public:
	vecTN_t neighbours;
	std::string subString;
};

struct SortTreeNodePtrById
{
    bool operator()( const TreeNode* n1, const TreeNode* n2 ) const {
    	return n1->getId() < n2->getId();
    }
};

struct SortTreeNodePtrByIdName
{
    bool operator()( const TreeNode* n1, const TreeNode* n2 ) const {
    	return n1->getIdName() < n2->getIdName();
    }
};


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */


#endif /* TREENODE_TREERECONSTRUCTION_H_ */
