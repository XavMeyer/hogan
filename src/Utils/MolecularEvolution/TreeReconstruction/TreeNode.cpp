//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.cpp
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */
#include "TreeNode.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

TreeNode::TreeNode(const int aIdName) :
		id(0), idName(aIdName)
{
	minId = std::numeric_limits<size_t>::max();
	subtreeSize = 0;
	parent = NULL;
	setIdString();
}

TreeNode::TreeNode(const TmpNode &aNode) :
		id(aNode.id), idName(aNode.idName)
{
	minId = std::numeric_limits<size_t>::max();
	subtreeSize = 0;
	parent = NULL;
	setIdString();
}

TreeNode::TreeNode(const DL::TreeNode &aNode, const std::map<std::string, int> & nameMapping) :
		id(aNode.getId()), idName(-1)
{
	if(aNode.isLeaf()) {
		std::map<std::string, int>::const_iterator it;
		it = nameMapping.find(aNode.name);
		if(it != nameMapping.end()) {
			idName = it->second;
		} else {
			std::cerr << "[ERROR] TreeNode::TreeNode(const DL::TreeNode &aNode, const std::map<std::string, int> & nameMapping);" << std::endl;
			std::cerr << "Id name not found for name : '" << aNode.name << "'." << std::endl;
			assert(it == nameMapping.end());
		}
	}
	minId = std::numeric_limits<size_t>::max();
	subtreeSize = 0;
	parent = NULL;
	setIdString();
}


TreeNode::~TreeNode() {
}


void TreeNode::addNeighbour(TreeNode *child, bool isParent) {
	neighbours.push_back(child);
	if(isParent) {
		parent = child;
	}  else if(neighbours.size() > 1) {
		std::swap(neighbours[neighbours.size()-1], neighbours[neighbours.size()-2]);
	}
	//sortNeighbours();
}

void TreeNode::removeNeighbour(TreeNode *child) {
	std::vector<TreeNode*>::iterator position = std::find(neighbours.begin(), neighbours.end(), child);
	if (position != neighbours.end()) {
		neighbours.erase(position);
		if(child == parent) {
			parent = NULL;
		}
		//sortNeighbours();
	}
}

TreeNode* TreeNode::getNeighbour(size_t id) {
	return neighbours[id];
}

const vecTN_t& TreeNode::getNeighbours() const {
	return neighbours;
}


TreeNode* TreeNode::getParent() const {
	return parent;
}

vecTN_t TreeNode::getChildren() const {
	vecTN_t vecTN;

	size_t nChild = neighbours.size();
	if(parent != NULL) {
		nChild--;
	}

	for(size_t iN=0; iN<nChild; ++iN) {
		vecTN.push_back(neighbours[iN]);
	}

	return vecTN;
}

void TreeNode::setParent(TreeNode *aParent) {
	parent = aParent;
}


vecTN_t TreeNode::getChildren(const TreeNode *parent) const {
	vecTN_t vecTN;

	for(size_t iN=0; iN<neighbours.size(); ++iN) {
		if(neighbours[iN] != parent) {
			vecTN.push_back(neighbours[iN]);
		}
	}

	return vecTN;
}

vecTN_t TreeNode::getNonTerminalChildren(const TreeNode *parent) const {
	vecTN_t vecTN;

	for(size_t iN=0; iN<neighbours.size(); ++iN) {
		if(neighbours[iN] != parent && !neighbours[iN]->isTerminal()) {
			vecTN.push_back(neighbours[iN]);
		}
	}

	return vecTN;
}

void TreeNode::setId(size_t aId) {
	id = aId;
	setIdString();
}

size_t TreeNode::getId() const {
	return id;
}

int TreeNode::getIdName() const {
	return idName;
}

const std::string& TreeNode::getIdString() const {
	return strIdName;
}

size_t TreeNode::getMinId() const {
	return minId;
}

void TreeNode::setMinId(size_t aMinId) {
	minId = aMinId;
}


bool TreeNode::isTerminal() const {
	return (neighbours.size() == 1);
}

const std::string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "] (" << minId << ") "<< idName;
	if(isTerminal()) ss << " - isTerminal";
	ss << " - Parent : " << (parent==NULL ? (int)-1 : (int)parent->getId()) << " :: " << (parent==NULL ? (int)-1 : (int)neighbours.back()->getId()) << std::endl;

	for(uint i=0; i<neighbours.size(); ++i){
		ss << "[" << neighbours[i]->id << "] (" << neighbours[i]->minId << ") " << neighbours[i]->idName;
		if(i < neighbours.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNode::deleteNeighbours(TreeNode *parent) {
	while(!neighbours.empty()) {
		TreeNode *nbr = neighbours.back();
		neighbours.pop_back();
		if(nbr != parent) {
			nbr->deleteNeighbours(this);
			delete nbr;
		}
	}
}

std::string TreeNode::buildOrderedString() const {
	std::string nwkStr;
	addNodeToOrderedString(nwkStr, NULL, NULL);
	nwkStr.append(";");
	return nwkStr;
}

std::string TreeNode::buildOrderedString(const std::vector<std::string> &names) const {
	std::string nwkStr;
	addNodeToOrderedString(nwkStr, NULL, &names);
	nwkStr.append(";");
	return nwkStr;
}

size_t TreeNode::addNodeToOrderedString(std::string &str, const TreeNode *parent,
		const std::vector<std::string> *names) const {

	size_t minId = std::numeric_limits<size_t>::max();

	if (isTerminal()) {
		minId = getIdName();
		if(names != NULL) {
			//str.append((*names)[idName]);
			std::stringstream ssIdName;
			ssIdName << idName;
			str.append(ssIdName.str());
		}
	} else {
		vecTN_t children = getChildren(parent);
		std::vector<size_t> minIdSubT(children.size());
		std::vector<std::string> subString(children.size());

		for(size_t iC=0; iC<children.size(); ++iC) {
			assert(children[iC]);
			minIdSubT[iC] = children[iC]->addNodeToOrderedString(subString[iC], this, names);
			minId = std::min(minId, minIdSubT[iC]);
		}

		// Order nodes neighbours
		bool ascending = false;
		std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(minIdSubT, ascending);

		// Add subtrees ordered by min leaf ID
		bool isFirst = true;
		for(size_t iC=0; iC<children.size(); ++iC) {
			if(isFirst) {
				str.push_back('(');
				isFirst = false;
			} else {
				str.push_back(',');
			}
			str.append(subString[orderedId[iC]]);
		}
		str.push_back(')');
	}

	if(names == NULL) {
		std::stringstream ss;
		ss << id;
		if(isTerminal()) {
			ss << ":" << idName;
		}
		str.append(ss.str());
	}

	return minId;
}

void TreeNode::setIdString() {
	std::stringstream ss;
	ss << id;
	if(idName != -1) {
		ss << ":" << idName;
	}
	strIdName = ss.str();
}


void TreeNode::setMySubtreeSize(size_t aSTS) {
	subtreeSize = aSTS;
}

size_t TreeNode::getMySubtreeSize() const {
	return subtreeSize;
}

std::vector<size_t> TreeNode::setSubTreesSize(const TreeNode *parent) {
	if(!isTerminal()) {
		vecTN_t children = getChildren(parent);
		subtreeSize = 1;
		std::vector<size_t> mySTS(children.size());
		for(size_t iC=0; iC<children.size(); ++iC) {
			size_t hisSTS = 1;
			std::vector<size_t> childSTS = children[iC]->setSubTreesSize(this);
			for(size_t iS=0; iS<childSTS.size(); ++iS){
				hisSTS += childSTS[iS];
			}
			mySTS[iC] = hisSTS;
			subtreeSize += children[iC]->getMySubtreeSize();
		}
		return mySTS;
	} else {
		std::vector<size_t> mySTS(0);
		subtreeSize=1;
		return mySTS;
	}
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
