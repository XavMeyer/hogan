//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Parser.cpp
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */
#include "Parser.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

Parser::Parser(const std::string &aStr) : str(aStr)  {

	treeRoot = NULL;

	parseTree();

}

Parser::~Parser() {
}

void Parser::parseTree() {
	namespace qi = ::boost::spirit::qi;

	//str = "(B:6.0,(A:5.0,C:3.0,E:4.0):5.0,D:11.0);";
	//str = "(((One:0.1,Two:0.2)Sub1:0.3,(Three:0.4,Four:0.5)Sub2:0.6)Sub3:0.7,Five:0.8)Root:0.9;";
	//str = "(((One:0.2,Two:0.3):0.3,(Three:0.5,Four:0.3):0.2):0.3,Five:0.7):0.0;";
	//std::cout << str << std::endl;

	Grammar<string::const_iterator> grammar;
	// Parse
	string::const_iterator iter = str.begin();
	string::const_iterator end = str.end();
	bool result = qi::phrase_parse(iter, end, grammar, qi::space, root);

	//std::cout << root << std::endl;

	if (!result) {
		std::cerr << "[Error] in TreeNode* Parser::parseTree(string &str)" << std::endl;
		std::cerr << "Tree string couldnt be read : " << str << std::endl;
		abort();
	}
}


TreeNode* Parser::getTreeRoot() {

	if(treeRoot == NULL) {
		TreeNode *tmpNode = new TreeNode(root);
		createTree(root, tmpNode);
		if(tmpNode->getNeighbours().size() == 2) {
			// Create an unrooted tree
			assert(tmpNode->getNeighbours().size() == 3);
			// Define root and other
			size_t newRoot = tmpNode->getNeighbour(0)->isTerminal() ? 1 : 0;
			size_t other = 1-newRoot;

			// Get new root
			treeRoot = tmpNode->getNeighbour(newRoot);
			TreeNode *otherNode = tmpNode->getNeighbour(other);

			treeRoot->removeNeighbour(tmpNode); // Remove root node from treeRoot
			otherNode->removeNeighbour(tmpNode); // Remove root node from other
			treeRoot->addNeighbour(otherNode); // Create edges
			otherNode->addNeighbour(treeRoot);

			// Remove wrong root
			delete tmpNode;
		} else {
			treeRoot = tmpNode;
		}
	}

	return treeRoot;
}


void Parser::createTree(const TmpNode &tmpNode, TreeNode *node) {
	if(tmpNode.children.size() > 0) { // If node has children, process
		for(size_t i=0; i<tmpNode.getChildren().size(); ++i){
			TreeNode *childNode = new TreeNode(tmpNode.getChildren()[i]);
			createTree(tmpNode.getChildren()[i], childNode);
			node->addNeighbour(childNode);
			childNode->addNeighbour(node);
		}
	}
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
