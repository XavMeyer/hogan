//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef TMPNODE_H_
#define TMPNODE_H_

#include <vector>
#include <sstream>

#include <boost/fusion/adapted/adt/adapt_adt.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

using std::string;
using std::vector;

class TmpNode;

typedef vector<TmpNode> TmpNode_children;

class TmpNode {
public:

	TmpNode();
	~TmpNode();

	size_t getId() const;
	int getIdName() const;
	const TmpNode_children& getChildren() const;

	bool isLeaf() const;

	std::string toString() const;
	std::string subtreeToString() const;

public:
	size_t id;
	int idName;
	TmpNode_children children;
};

}
}

BOOST_FUSION_ADAPT_STRUCT(MolecularEvolution::TreeReconstruction::TmpNode,
		(MolecularEvolution::TreeReconstruction::TmpNode_children, children) (size_t, id) (int, idName))


#endif /* TMPNODE_H_ */
