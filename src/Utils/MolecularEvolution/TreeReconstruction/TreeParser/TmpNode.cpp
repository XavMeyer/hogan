//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TmpNode
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */

#include "TmpNode.h"

namespace MolecularEvolution {
namespace TreeReconstruction {


TmpNode::TmpNode() : id(0), idName(-1) {

}


TmpNode::~TmpNode() {

}

string TmpNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i].id << "]";
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();

}

std::string TmpNode::subtreeToString() const {
	std::stringstream ss;
	ss << "-------------------------------------------" << std::endl;
	ss << toString() << std::endl;

	for (uint i = 0; i < children.size(); ++i) {
		ss << children[i].subtreeToString();
	}

	return ss.str();
}

size_t TmpNode::getId() const {
	return id;
}

int TmpNode::getIdName() const {
	return idName;
}

const TmpNode_children& TmpNode::getChildren() const {
	return children;
}

bool TmpNode::isLeaf() const {
	return children.empty();
}

}
}
