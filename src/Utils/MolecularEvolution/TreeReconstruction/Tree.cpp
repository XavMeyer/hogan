//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Tree.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "Tree.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

/*Tree::Tree(const Tree &other) {
	copy(other);
}*/

Tree::Tree(bool doShuffle, const DataLoader::MSA &msa, const std::map<std::string, int>& nameMapping) {
	// Create an initial tree from MSA
	TreeNode *tmpNode = createFromMSA(doShuffle, msa, nameMapping);

	if(tmpNode->getNeighbours().size() == 2) { // If tree is rooted
		// Create an unrooted tree
		// Define root and other
		size_t newRoot = tmpNode->getNeighbour(0)->isTerminal() ? 1 : 0;
		size_t other = 1-newRoot;

		// Get new root
		root = tmpNode->getNeighbour(newRoot);
		TreeNode *otherNode = tmpNode->getNeighbour(other);

		root->removeNeighbour(tmpNode); // Remove root node from treeRoot
		otherNode->removeNeighbour(tmpNode); // Remove root node from other
		root->addNeighbour(otherNode); // Create edges
		otherNode->addNeighbour(root);

		// Remove wrong root
		delete tmpNode;
	} else {
		root = tmpNode;
	}

	defineNodesCategory();
	//orderNodes();
	idString = root->buildOrderedString();
	createHashKey();
}



Tree::Tree(const DL::TreeNode &newickNode, const std::map<std::string, int> & nameMapping) {
	// Create tree
	TreeNode *tmpNode = new TreeNode(newickNode, nameMapping);
	createRecursive(newickNode, tmpNode, nameMapping);

	if(tmpNode->getNeighbours().size() == 2) { // If tree is rooted
		// Create an unrooted tree
		// Define root and other
		size_t newRoot = tmpNode->getNeighbour(0)->isTerminal() ? 1 : 0;
		size_t other = 1-newRoot;

		// Get new root
		root = tmpNode->getNeighbour(newRoot);
		TreeNode *otherNode = tmpNode->getNeighbour(other);

		root->removeNeighbour(tmpNode); // Remove root node from treeRoot
		otherNode->removeNeighbour(tmpNode); // Remove root node from other
		root->addNeighbour(otherNode); // Create edges
		otherNode->addNeighbour(root);

		// Remove wrong root
		delete tmpNode;
	} else {
		root = tmpNode;
	}

	// define ids for all the nodes
	size_t id = 0;
	assignIdRecursively(id, root, NULL);

	defineNodesCategory();
	//orderNodes();

	idString =  root->buildOrderedString();
	createHashKey();
}

Tree::Tree(const std::string &aTreeStr) {
	// Create from string
	Parser parser(aTreeStr);
	root = parser.getTreeRoot();

	defineNodesCategory();

	//orderNodes();
	idString = aTreeStr;
	createHashKey();
}

Tree::Tree(const Utils::Serialize::buffer_t &buffer) {
	// Load from buffer (treeString and move type)
	load(buffer);

	// Create tree etc.
	defineNodesCategory();

}

Tree::~Tree() {
	root->deleteNeighbours();
	delete root;
}

bool Tree::isEqualTo(const Tree* other) const {
	return (getHashKey() == other->getHashKey()) &&
		   (getIdString() == other->getIdString());
}

std::string Tree::getIdString() const {
	return idString;
}

std::string Tree::getNameString(const std::vector<std::string> &names) const {
	TreeNode *child_0 = terminals.front();
	TreeNode *startingNode = child_0->getNeighbour(0);

	return startingNode->buildOrderedString(names);
}

TreeNode* Tree::getRoot() {
	return root;
}

TreeNode* Tree::getNode(size_t id) {
	itNodesMap_t it = nodesMap.find(id);
	assert(it != nodesMap.end());
	return it->second;
}

std::vector<Tree::edge_t> Tree::getInternalEdges() {
	std::vector<Tree::edge_t> intEdges;
	for(size_t iN=0; iN<internals.size(); ++iN) {
		if(root != internals[iN]) {
			edge_t edge = {internals[iN]->getParent(), internals[iN]};
			intEdges.push_back(edge);
		}
	}
	return intEdges;
}

std::vector<TreeNode*>& Tree::getInternals() {
	return internals;
}

std::vector<TreeNode*>& Tree::getTerminals() {
	return terminals;
}

long int Tree::getHashKey() const {
	return myH;
}

void Tree::createHashKey() {
	boost::hash<std::string> string_hash;
	myH = string_hash(getIdString());
}

/*Tree* Tree::clone() const {
	Tree* newTree = new Tree(*this);
	return newTree;
}*/

const std::vector<TreeNode*>& Tree::getUpdatedNodes() const {
	return updatedNodes;
}

void Tree::clearUpdatedNodes() {
	updatedNodes.clear();
}

void Tree::update(const std::vector<TreeNode*> &aUpdatedNodes) {

	// Add to updatedNodes vector
	updatedNodes.insert(updatedNodes.end(), aUpdatedNodes.begin(), aUpdatedNodes.end());

	// update the tree using updated nodes
	updateTree(aUpdatedNodes);

	// Get the tree
	idString = root->subString;
	idString.append(";");

	// Update the hash key
	createHashKey();
}

const Utils::Serialize::buffer_t& Tree::save() {
	serializedBuffer.clear();
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}


void Tree::DBG_checkChildenOrder(TreeNode *node, TreeNode *parent) {

	vecTN_t children = node->getChildren(parent);

	for(size_t iC=0; iC<children.size(); ++iC) {
		assert(children[iC]->neighbours.back() == node);
		DBG_checkChildenOrder(children[iC], node);
	}
}

void Tree::load(const Utils::Serialize::buffer_t &buffer) {
	Utils::Serialize::load(buffer, *this);
}

void Tree::defineNodesCategory() {
	// Clear before
	internals.clear();
	terminals.clear();
	nodesMap.clear();

	// Then populate vectors
	exploreTree(root, NULL);

	// Sort leaves by idNames
	std::sort(terminals.begin(), terminals.end(), SortTreeNodePtrByIdName());
}

void Tree::exploreTree(TreeNode *node, TreeNode *parent) {
	nodesMap[node->getId()] = node;

	if(node->isTerminal()) { // If terminal its easy
		terminals.push_back(node);
		// Init internal values
		node->setParent(parent);
		node->setMinId(node->getIdName());
		node->subString = node->getIdString();
	} else { // If internal : we memorize edges and we order nodes
		size_t minId = std::numeric_limits<size_t>::max();

		// Get children
		vecTN_t children = node->getChildren(parent);
		std::vector<size_t> minIdSubT(children.size());

		for(size_t iC=0; iC<children.size(); ++iC) {
			// Explore the subtree
			exploreTree(children[iC], node);
			minIdSubT[iC] = children[iC]->getMinId();
			// Keep min Id for local node
			minId = std::min(minId, minIdSubT[iC]);
		}
		node->setMinId(minId);

		// If we are not terminal we are internal then
		internals.push_back(node);

		// Order nodes neighbours
		node->neighbours.clear(); // Clear old neighbours
		bool ascending = false;
		std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(minIdSubT,ascending);

		// Add children ordered by min leaf ID
		for(size_t iC=0; iC<children.size(); ++iC) {
			node->neighbours.push_back(children[orderedId[iC]]);
		}
		// Then add parent
		if(parent != NULL) {
			node->setParent(parent);
			node->neighbours.push_back(parent);
		}
		// Add subtrees ordered by min leaf ID
		node->subString = "(";
		for(size_t iC=0; iC<children.size(); ++iC) {
			if(iC>0) {
				node->subString.push_back(',');
			}
			node->subString.append(children[orderedId[iC]]->subString);
		}
		node->subString.push_back(')');
		node->subString.append(node->getIdString());
	}
}

void Tree::updateTree(const std::vector<TreeNode*> &aUpdatedNodes) {

	std::list<TreeNode*> listNodes;

	// Initial nodes to look-up
	for(size_t iN=0; iN<aUpdatedNodes.size(); ++iN) {
		if(std::find(listNodes.begin(), listNodes.end(), aUpdatedNodes[iN]) == listNodes.end()) {
			listNodes.push_back(aUpdatedNodes[iN]);
		}
	}

	while(!listNodes.empty()) {
		// Get first node
		TreeNode *node = listNodes.front();
		listNodes.pop_front();

		if(node->isTerminal()) {
			continue;
		}

		// reset minId
		size_t minId = std::numeric_limits<size_t>::max();
		// Get children
		vecTN_t children = node->getChildren();
		std::vector<size_t> minIdSubT(children.size());
		for(size_t iC=0; iC<children.size(); ++iC) {
			minIdSubT[iC] = children[iC]->getMinId();
			// Keep min Id for local node
			minId = std::min(minId, minIdSubT[iC]);
		}
		node->setMinId(minId);

		// Order nodes neighbours
		node->neighbours.clear(); // Clear old neighbours
		bool ascending = false;
		std::vector<size_t> orderedId = Utils::Algorithm::getOrderedIndices(minIdSubT,ascending);

		// Add children ordered by min leaf ID
		for(size_t iC=0; iC<children.size(); ++iC) {
			node->neighbours.push_back(children[orderedId[iC]]);
		}
		// Then add parent
		if(node->getParent() != NULL) {
			node->neighbours.push_back(node->getParent());
			if(std::find(listNodes.begin(), listNodes.end(), node->getParent()) == listNodes.end()) {
				listNodes.push_back(node->getParent());
			}
		}

		// Add subtrees ordered by min leaf ID
		node->subString = "(";
		for(size_t iC=0; iC<children.size(); ++iC) {
			if(iC>0) {
				node->subString.push_back(',');
			}
			node->subString.append(children[orderedId[iC]]->subString);
		}
		node->subString.push_back(')');
		node->subString.append(node->getIdString());
	}
}

void Tree::createRecursive(const DL::TreeNode &newickNode, TreeNode *node,
		const std::map<std::string, int> & nameMapping) {
	if(!newickNode.isLeaf()) {  // If node has children, process
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			TreeNode *childNode = new TreeNode(newickNode.getChildren()[i], nameMapping);
			createRecursive(newickNode.getChildren()[i], childNode, nameMapping);
			node->addNeighbour(childNode);
			childNode->addNeighbour(node);
		}
	}
}

TreeNode* Tree::createFromMSA(bool doShuffle, const DataLoader::MSA &msa,
		const std::map<std::string, int>& nameMapping) {
	const std::vector<DL::Alignment>& aligns = msa.getAlignments();
	std::vector<TreeNode*> curNodes, lastNodes;

	// Index vector
	std::vector<size_t> ind;
	for(size_t i=0; i<aligns.size(); ++i) {
		ind.push_back(i);
	}

	// shuffle index if requested
	if(doShuffle) {
		//Parallel::mpiMgr().getPRNG()->randomShuffle(ind);
		std::random_shuffle(ind.begin(), ind.end());
	}

	// Init leaves
	for(size_t iL=0; iL<aligns.size(); ++iL) {
		size_t indice = ind[iL];
		std::map<std::string, int>::const_iterator it;
		it = nameMapping.find(aligns[indice].getName());
		if(it != nameMapping.end()) {
			size_t idName = it->second;
			TreeNode *node = new TreeNode(idName);
			curNodes.push_back(node);
		} else {
			std::cerr << "[ERROR] TreeNode::TreeNode(const DL::TreeNode &aNode, const std::map<std::string, int> & nameMapping);" << std::endl;
			std::cerr << "Id name not found for name : '" << aligns[indice].getName() << "'." << std::endl;
			assert(it == nameMapping.end());
		}
	}

	// Create tree structure
	lastNodes = curNodes;
	curNodes.clear();
	while(lastNodes.size() > 1) { // While we have more than one node per level
		bool isOdd = (lastNodes.size() % 2) == 1;
		size_t nPair = (isOdd ? lastNodes.size()-1 : lastNodes.size()) / 2;
		for(size_t iL=0; iL<nPair; ++iL) { // Create internal node for each pair of nodes
			// Create internal nodes
			TreeNode *node = new TreeNode(-1);
			// Add edges
			node->addNeighbour(lastNodes[iL*2]);
			lastNodes[iL*2]->addNeighbour(node);
			node->addNeighbour(lastNodes[iL*2+1]);
			lastNodes[iL*2+1]->addNeighbour(node);
			// add node to current layer
			curNodes.push_back(node);
		}
		if(isOdd) { // If the number of nodes is odd, we add the last one as such
			curNodes.push_back(lastNodes.back());
		}

		// Prepare for next iteration
		lastNodes = curNodes;
		curNodes.clear();
	}
	// Get the new tree root
	assert(lastNodes.size() == 1);
	TreeNode *tmpRoot = lastNodes.back();

	// define ids for all the nodes
	size_t id = 0;
	assignIdRecursively(id, tmpRoot, NULL);

	return tmpRoot;
}

void Tree::assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent) {
	// Assign the id of current node
	node->setId(id);
	nodesMap[node->getId()] = node;
	id++;

	// For all children do the same
	vecTN_t children = node->getChildren(parent);
	for(size_t iC=0; iC<children.size(); ++iC) {
		assignIdRecursively(id, children[iC], node);
	}
}

void Tree::defineBalancingRoot() {
	TreeNode *curNode = root;

	std::vector<size_t> sts = root->setSubTreesSize(NULL);
	std::cout << "node [" << root->getId() << "] -> Subtree sizes : ";
	std::copy(sts.begin(), sts.end(), std::ostream_iterator<size_t>(std::cout, ", "));
	std::cout << std::endl;

	bool improve = true;
	do {
		size_t iMax=0, max=0, sumOthers=0;
		for(size_t iS=0; iS<curNode->getNeighbours().size(); ++iS) {
			sumOthers += curNode->getNeighbour(iS)->getMySubtreeSize();
			if(curNode->getNeighbour(iS)->getMySubtreeSize() > max) {
				iMax = iS;
				max = curNode->getNeighbour(iS)->getMySubtreeSize();
			}
		}
		sumOthers -= max;

		improve = max > sumOthers+1;
		if(improve) {
			curNode->setMySubtreeSize(sumOthers+1);
			curNode = curNode->getNeighbour(iMax);
			curNode->setMySubtreeSize(max+sumOthers+1);
		}

	} while(improve);

	sts = curNode->setSubTreesSize(NULL);
	std::cout << "curNode [" << curNode->getId() << "] -> Subtree sizes : ";
	std::copy(sts.begin(), sts.end(), std::ostream_iterator<size_t>(std::cout, ", "));
	std::cout << std::endl;

}

template<class Archive>
void Tree::save(Archive & ar, const unsigned int version) const {
	ar & idString;
}

template<class Archive>
void Tree::load(Archive & ar, const unsigned int version) {
	//std::string treeString;
	ar & idString;

	Parser parser(idString);
	root = parser.getTreeRoot();
	createHashKey();
}

/*void Tree::copy(const Tree &other) {
	myH = other.myH;
	idString = other.idString;
	root = cloneTree(other.root, NULL);
}*/

/*TreeNode* Tree::cloneTree(TreeNode *node, TreeNode *parent) {

	// Create the new node
	TreeNode *newNode = new TreeNode(node->getIdName());
	newNode->setId(node->getId());

	if(node->isTerminal()) { // If is terminal
		terminals.push_back(newNode);
	} else {
		// For all children of the node to copy
		vecTN_t children = node->getChildren(parent);
		for(size_t iC=0; iC<children.size(); ++iC) {
			TreeNode *childNode = cloneTree(children[iC], node);
			// Create links
			newNode->addNeighbour(childNode);
			childNode->addNeighbour(newNode);
		}
		internals.push_back(newNode);
	}

	return newNode;
}*/

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
