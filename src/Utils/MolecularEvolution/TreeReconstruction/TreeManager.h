//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeManager.h
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREEMANAGER_H_
#define TREEMANAGER_H_

#include "Tree.h"
#include "TreeNode.h"
#include "Move/ESPR.h"
#include "Move/STNNI.h"
#include "Move/MoveManager.h"
#include "Parallel/Parallel.h"
#include "Utils/Code/CheckpointFile.h"

#include <string>
#include <vector>
#include <list>
#include <map>

#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

class TreeManager {
public:
	typedef boost::shared_ptr<TreeManager> sharedPtr_t;
	typedef std::map<std::string, int> nameMap_t;
public:
	TreeManager(const std::vector<std::string> &aOrderedTaxaNames);
	~TreeManager();

	void setTree(Tree::sharedPtr_t aTree);
	Tree::sharedPtr_t getCurrentTree();
	Tree::sharedPtr_t getTree(long int aTreeIdx);

	void memorizeTree();

	void setProposedMove(Move::sharedPtr_t aMove);
	Move::sharedPtr_t getProposedMove();
	void rejectProposedMove();
	void acceptProposedMove();
	void acceptRemoteMove();

	const std::vector<std::string>& getOrderedNames() const;
	const nameMap_t& getNameMapping() const;
	std::string getTreeNodeName(const size_t id) const;
	std::string getTreeNodeName(const TreeNode *node) const;
	std::string getIdString(long int aTreeIdx) const;

	void summarizeTrees(std::vector<std::string> &summary, std::map<long int, size_t> &mapping);

	void initTreeFile(const std::string &fnPrefix, const std::string &fnSuffix, std::streampos offset);
	std::streampos getTreeFileLength() const;
	void writeTreeString(const std::vector<long int> &treesHash);

private:

	const Parallel::MCMCManager &mcmcMgr;

	Tree::sharedPtr_t tree;
	MoveManager moveManager;
	STNNI stnni;
	ESPR espr;

	typedef std::map<long int, std::string> treeStringMap_t;
	typedef treeStringMap_t::iterator itTreeStringMap_t;
	typedef treeStringMap_t::const_iterator itConstTreeStringMap_t;
	treeStringMap_t treeStringMap;

	std::vector<std::string> orderedTaxaNames;
	nameMap_t taxaNameMap;

	std::string treeStringFN;
	Utils::Checkpoint::File::sharedPtr_t oFile;

	void reloadTreeStringMap();
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREEMANAGER_H_ */
