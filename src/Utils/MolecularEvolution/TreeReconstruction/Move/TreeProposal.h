//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeProposal.h
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREEPROPOSAL_H_
#define TREEPROPOSAL_H_

#include "Move.h"
#include "../Tree.h"
#include "Parallel/Parallel.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class TreeProposal {
public:
	enum edge_direction_t {FROM_N1_TO_N2, FROM_N2_TO_N1};
public:
	TreeProposal();
	virtual ~TreeProposal();

	virtual Move::sharedPtr_t proposeNewTree(const double pe, Tree::sharedPtr_t tree) = 0;
	virtual void applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) = 0;
	virtual void undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) = 0;

protected:

	const RNG *aRNG;

	Tree::edge_t getRndInternalBranch(Tree::sharedPtr_t tree);

	edge_direction_t getEdgeDirection(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2);

	bool containsRootNode(Tree::sharedPtr_t tree, TreeNode* inNode, TreeNode* outNode);
	void reverseDirection(Tree::sharedPtr_t tree, std::vector<TreeNode*> path);

	void addEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2, edge_direction_t direction);
	TreeProposal::edge_direction_t removeEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREEPROPOSAL_H_ */
