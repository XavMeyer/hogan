//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MoveManager.h
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef MOVEMANAGER_H_
#define MOVEMANAGER_H_

#include "Move.h"
#include "Parallel/Parallel.h"

#include <string>
#include <vector>
#include <map>
#include <list>

#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

class MoveManager {
public:
	typedef boost::shared_ptr<MoveManager> sharedPtr_t;
public:
	MoveManager();
	~MoveManager();

	void setLocalMove(Move::sharedPtr_t aMove);
	Move::sharedPtr_t getLocalMove();
	void shareLocalMove();
	void resetLocalMove();

	Move::sharedPtr_t getRemoteMove();

	//void shareMove(Move::sharedPtr_t aMove);
	//Move::sharedPtr_t getMove(long int fromH, long int toH);

private:

	const Parallel::MCMCManager &mcmcMgr;
	Move::sharedPtr_t localMove, sentMove;

	Move::sharedPtr_t waitForAMove();

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* MOVEMANAGER_H_ */
