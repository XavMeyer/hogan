//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPR.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef ESPR_H_
#define ESPR_H_

#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class ESPR : public TreeProposal {
public:
	enum moveInfoESPR_t {
		BOTH_CONSTRAINED=0,
		BOTH_UNCONSTRAINED=1,
		UNCONSTRAINED_TO_CONSTRAINED=2,
		CONSTRAINED_TO_UNCONSTRAINED=3
	};

	enum branchLabel_t { NODE_M1 = 0, NODE_M2 = 1, NODE_S1 = 2, NODE_S2 = 3, NODE_E1 = 4, NODE_E2 = 5 };

public:
	ESPR();
	~ESPR();

	Move::sharedPtr_t proposeNewTree(const double pe, Tree::sharedPtr_t tree);

	void applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);
	void undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);

private:

	bool labelSubTrees(Tree::edge_t &edge, TreeNode* &toGraft, TreeNode* &toExplore);
	bool canBeExplored(TreeNode *toExplore, TreeNode *parent);
	bool findExploreDirection(TreeNode *toExplore, TreeNode *parent, TreeNode* &toExploreClad);
	size_t findGraftPoint(const double pe, TreeNode *toExplore, TreeNode *parent, Tree::edge_t &graftPoint, std::vector<TreeNode*> &path);
	void pruneAndGraft(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);


	std::string toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
			Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* ESPR_H_ */
