//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file STNNI.h
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef STNNI_H_
#define STNNI_H_

#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

class STNNI : public TreeProposal {
public:
	enum branchLabel_t {
		NODE_N1=0, NODE_N2=1, NODE_B=2, NODE_OTHER=3
	};

public:
	STNNI();
	~STNNI();

	Move::sharedPtr_t proposeNewTree(const double pe, Tree::sharedPtr_t tree);
	void applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);
	void undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move);

private:

	std::string toString(Tree::sharedPtr_t tree, TreeNode *ptrB, TreeNode *ptrOther, TreeNode *ptrN1, TreeNode *ptrN2);
	void swapSubtrees(Tree::sharedPtr_t tree, TreeNode* ptrAdjN1, TreeNode* ptrAdjN2, Tree::edge_t edge);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* STNNI_H_ */
