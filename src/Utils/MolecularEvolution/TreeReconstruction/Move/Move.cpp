//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Move.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "Move.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

Move::Move(const Utils::Serialize::buffer_t &aBuffer) {
	load(aBuffer);
}

Move::Move(long int aFromH, long int aToH, moveType_t aMoveType) :
		fromH(aFromH), toH(aToH), moveType(aMoveType), probability(0.) {
	moveInfo = 0;
	nEdgeDistance = 0;
}

Move::~Move() {
}

long int Move::getFromH() const {
	return fromH;
}

long int Move::getToH() const {
	return toH;
}

Move::moveType_t Move::getMoveType() const {
	return moveType;
}

void Move::setProbability(double aP) {
	probability = aP;
}

double Move::getProbability() const {
	return probability;
}

void Move::setMoveInfo(const int aMoveInfo) {
	moveInfo = aMoveInfo;
}

int Move::getMoveInfo() const {
	return moveInfo;
}

void Move::setNEdgeDistance(const size_t aNEdge) {
	nEdgeDistance = aNEdge;
}

bool Move::equals(long int aFromH, long int aToH, Move::moveType_t aMoveType) const {
	return  moveType == aMoveType &&
			fromH == aFromH &&
			toH == aToH;

}


void Move::addInvolvedNode(size_t id) {
	involvedNode.push_back(id);
}

const std::vector<size_t>& Move::getInvolvedNode() const {
	return involvedNode;
}

size_t Move::getNEdgeDistance() const {
	return nEdgeDistance;
}

const Utils::Serialize::buffer_t& Move::save() {
	serializedBuffer.clear();
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}

std::string Move::toString() const {
	std::stringstream ss;
	ss << "[" << static_cast<int>(moveType) << "]" << fromH << " -> " << toH << " :: p = " << probability ;

	return ss.str();
}

void Move::load(const Utils::Serialize::buffer_t& buffer) {
	Utils::Serialize::load(buffer, *this);
}

template<class Archive>
void Move::save(Archive & ar, const unsigned int version) const {
	int tmpMoveType = static_cast<int>(moveType);
	ar & fromH;
	ar & toH;
	ar & tmpMoveType;
	ar & moveInfo;
	ar & nEdgeDistance;
	ar & probability;
	ar & involvedNode;
}

template<class Archive>
void Move::load(Archive & ar, const unsigned int version) {
	int tmpMoveType;
	ar & fromH;
	ar & toH;
	ar & tmpMoveType;
	moveType = static_cast<moveType_t>(tmpMoveType);
	ar & moveInfo;
	ar & nEdgeDistance;
	ar & probability;
	ar & involvedNode;
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
