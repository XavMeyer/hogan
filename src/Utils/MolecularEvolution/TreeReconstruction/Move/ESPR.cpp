//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPR.cpp
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#include "ESPR.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

ESPR::ESPR() : TreeProposal(){
}

ESPR::~ESPR() {
}

/**
 * Propose a new tree by doing an eSPR move.
 *
 * @param h Hash key of the current tree
 * @return The move produced
 */
Move::sharedPtr_t ESPR::proposeNewTree(const double pe, Tree::sharedPtr_t tree) {

	assert( pe >= 0 && pe <= 1.);

	long int h = tree->getHashKey();

	bool canChange = false;
	Tree::edge_t edgeToPrune;
	TreeNode *toGraft, *toExplore, *parent;
	while(!canChange) {
		// Randomly select an internal edge
		edgeToPrune = getRndInternalBranch(tree);

		// Label the subtrees
		canChange = labelSubTrees(edgeToPrune, toGraft, toExplore);
		if(!canChange) {
			std::cout << "Cannot change!" << std::endl;
		}
	}

	// The tree may change, we then search for the direction
	parent = toGraft;
	TreeNode* toExploreClad;
	bool isBackwardConstrained = findExploreDirection(toExplore, parent, toExploreClad);

	// We now have to find a grafting point
	parent = toExplore;
	Tree::edge_t graftEdge;
	std::vector<TreeNode*> path;
	size_t nEdge = findGraftPoint(pe, toExploreClad, parent, graftEdge, path);
	bool isForwardConstrained = graftEdge.n2->isTerminal();

	// We keep track of old "toExplore neighbor" that will form the collapsed edge
	vecTN_t vecTN = toExplore->getChildren(toGraft);
	assert(vecTN.size() == 2);
	// Keep track of collapsed edge --> s1 is always equal to "toExploreClad"
	Tree::edge_t collapseEdge = {vecTN[0], vecTN[1]};
	if(collapseEdge.n1 != toExploreClad) std::swap(collapseEdge.n1, collapseEdge.n2);

	// We now have to prune and graft
	pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);

	// get new hash
	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::ESPR));

	// Register involved nodes (edges)
	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	// m1 <--> m2 represents the edge to move (m1=toGraft, m2=toExplore)
	newMove->addInvolvedNode(toGraft->getId());
	newMove->addInvolvedNode(toExplore->getId());
	// s1 <--> s2 is the collapsed starting edge
	newMove->addInvolvedNode(collapseEdge.n1->getId());
	newMove->addInvolvedNode(collapseEdge.n2->getId());
	// e1 <--> e2 is the augmented ending edge
	newMove->addInvolvedNode(graftEdge.n1->getId());
	newMove->addInvolvedNode(graftEdge.n2->getId());

	// Keep track of path
	for(size_t iP=0; iP<path.size(); ++iP) {
		newMove->addInvolvedNode(path[iP]->getId());
	}

	// Set probability and define move info
	moveInfoESPR_t moveInfoESPR = BOTH_UNCONSTRAINED;
	if(isForwardConstrained == isBackwardConstrained) {
		newMove->setProbability(1.);
		moveInfoESPR = isForwardConstrained ? BOTH_CONSTRAINED : BOTH_UNCONSTRAINED;
	} else if(isForwardConstrained) {
		newMove->setProbability((1.-pe));
		moveInfoESPR = UNCONSTRAINED_TO_CONSTRAINED;
	} else if(isBackwardConstrained) {
		newMove->setProbability(1./(1.-pe));
		moveInfoESPR = CONSTRAINED_TO_UNCONSTRAINED;
	}

	// Add some info to move
	newMove->setNEdgeDistance(nEdge);
	newMove->setMoveInfo(static_cast<int>(moveInfoESPR));

	return newMove;
}

/**
 * Randomly label two subtree, one can be explored the other as to be grafted somewhere
 * The subtree to explore must have at least one non-terminal edge
 *
 * @param edge The edge to prune (toGraftClad <- edge -> toExploreClad)
 * @param toGraft The first node of the clad to graft
 * @param toExplore The first node of the clad to explore
 * @return True if two subtree can be found, false else.
 */
bool ESPR::labelSubTrees(Tree::edge_t &edge, TreeNode* &toGraft, TreeNode* &toExplore) {

	bool canExploreN1 = canBeExplored(edge.n1, edge.n2);
	bool canExploreN2 = canBeExplored(edge.n2, edge.n1);

	if(canExploreN1 && canExploreN2) {
		bool draw = (aRNG->genUniformInt(0, 1) == 0);
		if(draw) {
			toGraft = edge.n1;
			toExplore = edge.n2;
		} else {
			toGraft = edge.n2;
			toExplore = edge.n1;
		}
	} else if(canExploreN1) {
		toGraft = edge.n2;
		toExplore = edge.n1;
	} else if(canExploreN2) {
		toGraft = edge.n1;
		toExplore = edge.n2;
	} else {
		return false;
	}

	return true;
}

/**
 * Check if a subtree can be explore
 * parentNode -edge-> toExplore(clad)
 *
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @return True if it can be explored, false else.
 */
bool ESPR::canBeExplored(TreeNode *toExplore, TreeNode *parent) {

	bool areBothEdgeTerminal = true;

	for(size_t iN=0; iN<toExplore->getNeighbours().size(); ++iN) {
		TreeNode *nbr = toExplore->getNeighbour(iN);
		if(nbr != parent) {
			areBothEdgeTerminal = areBothEdgeTerminal && nbr->isTerminal();
		}
	}

	return !areBothEdgeTerminal;
}

/**
 * Randomly assign the first direction to take in the exploration
 * Only internal edges can be explored for this first step.
 *
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @param toExploreClad The first node in the clad to explore (direction is : toExplore -edge-> toExploreClad)
 * @return True if the reverse movement is constrained (non-explored node is terminal), false else
 */
bool ESPR::findExploreDirection(TreeNode *toExplore, TreeNode *parent, TreeNode* &toExploreClad) {
	vecTN_t set = toExplore->getNonTerminalChildren(parent);
	assert(set.size() <= 2 && set.size() > 0);

	size_t direction = 0;
	if(set.size() > 1) {
		direction = aRNG->genUniformInt(0, set.size()-1);
	}
	toExploreClad = set[direction];
	return set.size() == 1;
}

/**
 *
 * @param pe The "extending" probability of the move
 * @param toExplore The first node of the subtree to explore
 * @param parent The parent node that give the direction of the edge
 * @param graftPoint edge with direction {parent -edge-> child}
 * @param path Path along which we move the clad to graft
 * @return The number of edge between the pruning and grafing point
 */
size_t ESPR::findGraftPoint(const double pe, TreeNode *toExplore, TreeNode *parent,
		Tree::edge_t &graftPoint, std::vector<TreeNode*> &path) {

	size_t nEdge = 1;
	TreeNode *pNode = parent;
	TreeNode *cNode = toExplore;

	while(!cNode->isTerminal()) { // While we have not reached a terminal edge
		// Add current node to path
		path.push_back(cNode);
		// Get nbr nodes
		vecTN_t set = cNode->getChildren(pNode);
		assert(set.size() == 2);
		// Move in one direction (chose one node)
		int direction = aRNG->genUniformInt(0, 1);
		pNode = cNode;
		cNode = set[direction];
		// Check if we have to continue
		bool doContinue = (aRNG->genUniformDbl() < pe);
		if(!doContinue) {
			break; // We have found an internal edge
		}
		nEdge++;
	}

	// We have found an edge (from parent -> child)
	graftPoint.n1 = pNode;
	graftPoint.n2 = cNode;

	return nEdge;
}

/**
 * Prune the subtree to graft an graft it on the graft point.
 * @param tree The tree
 * @param toGraft First node of the subtree to graft
 * @param toExplore Parent of the first node of the subtree to graft
 * @param graftPoint Edge on which we want to graft the subtree
 * @param path Path along which we move the clad to graft
 */
void ESPR::pruneAndGraft(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {

	// (1) Prune subtree and clean edge
	//		toGraft   <--edge0-->  toExplore
	//		child1    <--edge1-->  toExplore  <--edge2--> child2
	// Transformed to
	//		toGraft   <--edge-->  toExplore
	//		child1    <--edge-->  child2

	// Change path direction if needed
	edge_direction_t directionRoot = getEdgeDirection(tree, toGraft, toExplore);
	bool rootIsInGraftClad = (directionRoot == FROM_N2_TO_N1) || (toExplore == tree->getRoot());
	if(path.size() > 1 && rootIsInGraftClad) { // n2 = toExplore --> n1 = toGraft
		reverseDirection(tree, path);
	}

	// Prune graft clad
	// Remove their edge to "toExplore"
	edge_direction_t dirTC2 = removeEdge(tree, collapseEdge.n2, toExplore);
	removeEdge(tree, toExplore, collapseEdge.n1);

	// Collapse edge
	addEdge(tree, collapseEdge.n2, collapseEdge.n1, dirTC2);

	// (2) Graft subtree and add edges
	//		graftPoint.n1   <--edge0--> graftPoint.n2
	// Transformed to
	//		graftPoint.n1   <--edge1--> toExplore <--edge2--> graftPoint.n2

	// (a) Break edge0
	// n1 is where toGraft is comming from (n1 is last element of path)
	edge_direction_t dirGraftPoint = removeEdge(tree, graftEdge.n1, graftEdge.n2);
	// (b) Create edge2
	addEdge(tree, toExplore, graftEdge.n2, dirGraftPoint);
	// (c) Create edge1
	if(rootIsInGraftClad) {
		addEdge(tree, graftEdge.n1, toExplore, FROM_N1_TO_N2);
	} else {
		addEdge(tree, graftEdge.n1, toExplore, dirGraftPoint);
	}

	//Signal changed node to tree
	std::vector<TreeNode*> updatedNodes;
	updatedNodes.push_back(toGraft);
	updatedNodes.push_back(toExplore);
	updatedNodes.push_back(collapseEdge.n1);
	updatedNodes.push_back(collapseEdge.n2);
	updatedNodes.push_back(graftEdge.n1);
	updatedNodes.push_back(graftEdge.n2);

	// update newTree internals
	tree->update(updatedNodes);
}


void ESPR::applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) {

	assert(tree->getHashKey() == move->getFromH());

	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	std::vector<size_t> involvedNodes = move->getInvolvedNode();
	TreeNode *toGraft = tree->getNode(involvedNodes[NODE_M1]); // m1
	TreeNode *toExplore = tree->getNode(involvedNodes[NODE_M2]); // m2
	TreeNode *ptrS1 = tree->getNode(involvedNodes[NODE_S1]); // s1
	TreeNode *ptrS2 = tree->getNode(involvedNodes[NODE_S2]); // s2
	TreeNode *ptrE1 = tree->getNode(involvedNodes[NODE_E1]); // e1
	TreeNode *ptrE2 = tree->getNode(involvedNodes[NODE_E2]); // e2

	// Get path
	std::vector<TreeNode *> path;
	for(size_t iN=6; iN<involvedNodes.size(); ++iN) {
		path.push_back(tree->getNode(involvedNodes[iN]));
	}

	// Move subtrees
	// We now have to prune and graft
	// forward move is : m1<-->m2 in between e1<-->e2
	// reverse move is : m1<-->m2 in between s1<-->s2
	Tree::edge_t graftEdge = {ptrE1, ptrE2};
	Tree::edge_t collapseEdge = {ptrS1, ptrS2};

	pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);
}


void ESPR::undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) {
	assert(tree->getHashKey() == move->getToH());

	// Keep track of nodes <m1, m2, s1, s2, e1, e2>
	std::vector<size_t> involvedNodes = move->getInvolvedNode();
	TreeNode *toGraft = tree->getNode(involvedNodes[NODE_M1]); // m1
	TreeNode *toExplore = tree->getNode(involvedNodes[NODE_M2]); // m2
	TreeNode *ptrS1 = tree->getNode(involvedNodes[NODE_S1]); // s1
	TreeNode *ptrS2 = tree->getNode(involvedNodes[NODE_S2]); // s2
	TreeNode *ptrE1 = tree->getNode(involvedNodes[NODE_E1]); // e1
	TreeNode *ptrE2 = tree->getNode(involvedNodes[NODE_E2]); // e2

	// Get path
	std::vector<TreeNode *> path;
	for(size_t iN=6; iN<involvedNodes.size(); ++iN) {
		path.push_back(tree->getNode(involvedNodes[iN]));
	}

	// Move subtrees
	// We now have to prune and graft
	// forward move was : m1<-->m2 in between e1<-->e2
	// reverse move is : m1<-->m2 in between s1<-->s2
	Tree::edge_t graftEdge = {ptrS1, ptrS2};
	Tree::edge_t collapseEdge = {ptrE1, ptrE2};

	pruneAndGraft(tree, toGraft, toExplore, collapseEdge, graftEdge, path);
}

std::string ESPR::toString(Tree::sharedPtr_t tree, TreeNode *toGraft, TreeNode *toExplore,
		Tree::edge_t collapseEdge, Tree::edge_t graftEdge, std::vector<TreeNode*> &path) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "toGraft : " << toGraft->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "toExplore : " << toExplore->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s1 : " << collapseEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "s2 : " << collapseEdge.n2->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e1 : " << graftEdge.n1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "e2 : " << graftEdge.n2->toString() << std::endl;
	for(size_t i=0; i<path.size(); ++i) {
		ss << "**" << std::endl;
		ss << "p" << i << " : " << path[i]->toString() << std::endl;
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
