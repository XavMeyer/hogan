//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Move.h
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef MOVE_H_
#define MOVE_H_

#include "Utils/Code/SerializationSupport.h"

#include <sstream>
#include <vector>
#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

class Move {
public:
	enum moveType_t {NONE=-1, STNNI=0, ESPR=1};
	typedef boost::shared_ptr<Move> sharedPtr_t;
public:
	Move(const Utils::Serialize::buffer_t &aBuffer);
	Move(long int aFromH, long int aToH, moveType_t aMoveType);
	~Move();

	long int getFromH() const;
	long int getToH() const;

	moveType_t getMoveType() const;

	void setProbability(double aP);
	double getProbability() const;

	void setMoveInfo(const int aMoveInfo);
	int getMoveInfo() const;

	void setNEdgeDistance(const size_t aNEdge);
	size_t getNEdgeDistance() const;

	bool equals(long int fromH, long int toH, Move::moveType_t moveType) const;

	 void addInvolvedNode(size_t id);
	 const std::vector<size_t>& getInvolvedNode() const;

	const Utils::Serialize::buffer_t& save();

	std::string toString() const;

private:

	long int fromH, toH;
	moveType_t moveType;
	int moveInfo;
	size_t nEdgeDistance;
	double probability;
	std::vector<size_t> involvedNode;

	void load(const Utils::Serialize::buffer_t &buffer);

	// Serialization
	Utils::Serialize::buffer_t serializedBuffer;

    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int version) const;

    template<class Archive>
    void load(Archive & ar, const unsigned int version);
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* MOVE_H_ */
