//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file STNNI.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "STNNI.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

STNNI::STNNI() : TreeProposal() {
}

STNNI::~STNNI() {
}

Move::sharedPtr_t STNNI::proposeNewTree(const double pe, Tree::sharedPtr_t tree) {

	long int h = tree->getHashKey();

	// Randomly select an internal edge
	Tree::edge_t edge = getRndInternalBranch(tree);

	// Chose which kind of exchanges
	//		{A, B} --> n1 <--edge--> n2 <-- {C, D}
	// If cross exchange :
	//		{A, C} --> n1 <--edge--> n2 <-- {B, D}
	// Else
	//		{A, D} --> n1 <--edge--> n2 <-- {C, B}

	bool cross = (aRNG->genUniformInt(0, 1) == 0);
	vecTN_t childrenN1 = edge.n1->getChildren(edge.n2); // {A,B}
	vecTN_t childrenN2 = edge.n2->getChildren(edge.n1); // {C,D
	assert(childrenN1.size()==2 && childrenN2.size()==2);

	// Define who are swapped
	TreeNode *ptrB = childrenN1[1]; // Get B
	TreeNode *ptrOther;
	if(cross) {
		ptrOther = childrenN2[0]; // Get C
	} else {
		ptrOther = childrenN2[1]; // Get D
	}

	// Swap the subtrees
	swapSubtrees(tree, ptrB, ptrOther, edge);

	long int newH = tree->getHashKey();

	// Register movement
	Move::sharedPtr_t newMove(new Move(h, newH, Move::STNNI));

	// Set probability
	newMove->setProbability(1.);

	// Register involved nodes (edges)
	// Keep track of nodes <n1, n2, B, C/D>
	// n1 <--> n2 selected edge
	newMove->addInvolvedNode(edge.n1->getId());
	newMove->addInvolvedNode(edge.n2->getId());
	// node B
	newMove->addInvolvedNode(ptrB->getId());
	// node C or D
	newMove->addInvolvedNode(ptrOther->getId());

	return newMove;
}

void STNNI::applyMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) {

	assert(tree->getHashKey() == move->getFromH());

	std::vector<size_t> involvedNodes = move->getInvolvedNode();
	TreeNode *ptrN1 = tree->getNode(involvedNodes[0]); // n1
	TreeNode *ptrN2 = tree->getNode(involvedNodes[1]); // n2
	TreeNode *ptrB = tree->getNode(involvedNodes[2]); // B
	TreeNode *ptrOther = tree->getNode(involvedNodes[3]); // C/D

	Tree::edge_t edge = {ptrN1, ptrN2};

	// Swap the subtrees
	swapSubtrees(tree, ptrB, ptrOther, edge);
}


void STNNI::undoMove(Tree::sharedPtr_t tree, Move::sharedPtr_t move) {

	assert(tree->getHashKey() == move->getToH());

	std::vector<size_t> involvedNodes = move->getInvolvedNode();
	TreeNode *ptrN1 = tree->getNode(involvedNodes[0]); // n1
	TreeNode *ptrN2 = tree->getNode(involvedNodes[1]); // n2
	TreeNode *ptrB = tree->getNode(involvedNodes[2]); // B
	TreeNode *ptrOther = tree->getNode(involvedNodes[3]); // C/D

	Tree::edge_t edge = {ptrN1, ptrN2};

	// Swap the subtrees
	swapSubtrees(tree, ptrOther, ptrB, edge);
}

std::string STNNI::toString(Tree::sharedPtr_t tree, TreeNode *ptrB, TreeNode *ptrOther, TreeNode *ptrN1, TreeNode *ptrN2) {
	std::stringstream ss;
	ss << "-------------------------------------------------------" << std::endl;
	ss << "ptrB : " << ptrB->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "prtOther : " << ptrOther->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "n1 : " << ptrN1->toString() << std::endl;
	ss << "--" << std::endl;
	ss << "n2 : " << ptrN2->toString() << std::endl;
	return ss.str();
}

/**
 * Swap the subtree with first node ptrAdjN1 adjacent to edge.n1 with
 * the subtree with first node ptrAdjN2 adjacent to edge.n2.
 *
 * FROM : ptrAdjN1 <--> edge.n1 <--> edge.n2 <--> ptrAdjN2
 * TO : ptrAdjN2 <--> edge.n1 <--> edge.n2 <--> ptrAdjN1
 *
 * @param tree The tree that will be modified
 * @param ptrAdjN1 The first subtree node adjacent to edge.n1
 * @param ptrAdjN2 The first subtree node adjacent to edge.n1
 * @param edge The edge interconnecting both subtrees
 */
void STNNI::swapSubtrees(Tree::sharedPtr_t tree, TreeNode* ptrAdjN1, TreeNode* ptrAdjN2, Tree::edge_t edge) {
	// check if the edge should change its direction
	bool doesBcontainsRoot = containsRootNode(tree, ptrAdjN1, edge.n1);
	bool doesOthercontainsRoot = containsRootNode(tree, ptrAdjN2, edge.n2);

	// Reverse edge if needed
	if(doesBcontainsRoot || doesOthercontainsRoot) {
		std::vector<TreeNode*> path;
		path.push_back(edge.n1);
		path.push_back(edge.n2);
		reverseDirection(tree, path);
	}

	// Move subtrees
	edge_direction_t dirN1 = removeEdge(tree, ptrAdjN1, edge.n1);
	edge_direction_t dirN2 = removeEdge(tree, ptrAdjN2, edge.n2);

	addEdge(tree, ptrAdjN1, edge.n2, dirN1);
	addEdge(tree, ptrAdjN2, edge.n1, dirN2);

	//Signal changed node to tree
	std::vector<TreeNode*> updatedNodes;
	updatedNodes.push_back(edge.n1);
	updatedNodes.push_back(edge.n2);
	updatedNodes.push_back(ptrAdjN1);
	updatedNodes.push_back(ptrAdjN2);

	// update newTree internals
	tree->update(updatedNodes);
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
