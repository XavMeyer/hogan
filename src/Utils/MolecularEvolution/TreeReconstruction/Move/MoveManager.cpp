//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MoveManager.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "MoveManager.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

MoveManager::MoveManager() : mcmcMgr(Parallel::mcmcMgr())  {
}

MoveManager::~MoveManager() {
}

/*void MoveManager::shareMove(Move::sharedPtr_t aMove) {
	currentMove = aMove;
	if(!mcmcMgr.isProposalSequential()) {
		//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] Send : " << aMove->getFromH() << " :: " << aMove->getToH() << std::endl; // FIXME
		mcmcMgr.sendTreeMove(currentMove->save());
	}
}

Move::sharedPtr_t MoveManager::getMove(long int fromH, long int toH) {
	if(!mcmcMgr.isProposalSequential()) {
		Move::sharedPtr_t aMove = waitForMove(fromH, toH);
		return aMove;
	} else {
		std::cerr << "[ERROR] Move::sharedPtr_t MoveManager::getMove(long int fromH, long int toH);" << std::endl;
		std::cerr << " Cannot wait for a remote process to send a move when the proposal is sequential." << std::endl;
		assert(!mcmcMgr.isProposalSequential());
		Move::sharedPtr_t aMove;
		return aMove;
	}
}*/

void MoveManager::setLocalMove(Move::sharedPtr_t aMove) {
	localMove = aMove;
}

Move::sharedPtr_t MoveManager::getLocalMove() {
	return localMove;
}

void MoveManager::shareLocalMove() {
	sentMove = localMove;
	if(!mcmcMgr.isProposalSequential()) {
		//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] Send : " << aMove->getFromH() << " :: " << aMove->getToH() << std::endl; // FIXME
		mcmcMgr.sendTreeMove(sentMove->save());
	}
}

void MoveManager::resetLocalMove() {
	localMove.reset();
}

Move::sharedPtr_t MoveManager::getRemoteMove() {
	return waitForAMove();
}

Move::sharedPtr_t MoveManager::waitForAMove() {
	if(mcmcMgr.isProposalSequential()) {
		std::cerr << "[ERROR] Cannot wait for a remote process to send a move when the proposal is sequential." << std::endl;
		assert(!mcmcMgr.isProposalSequential());
	}

	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] Wait for : " << fromH << " :: " << toH << std::endl; // FIXME
	Utils::Serialize::buffer_t buffer; 				// Tree buffer
	mcmcMgr.waitTreeMove(buffer); 					// Wait for a move
	Move::sharedPtr_t aMove(new Move(buffer)); 					// Create it
	return aMove;
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
