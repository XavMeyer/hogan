//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeProposal.cpp
 *
 * @date Nov 5, 2015
 * @author meyerx
 * @brief
 */
#include "TreeProposal.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

TreeProposal::TreeProposal() : aRNG(Parallel::mpiMgr().getPRNG()) {

}

TreeProposal::~TreeProposal() {
}

/**
 * Get a random internal edge of a tree.
 *
 * @param tree The tree
 * @return The randomly selected edge of the tree
 */
Tree::edge_t TreeProposal::getRndInternalBranch(Tree::sharedPtr_t tree) {
	TreeNode *intNode = tree->getRoot();
	while(intNode == tree->getRoot()) {
		size_t id = aRNG->genUniformInt(0, tree->getInternals().size()-1);
		intNode = tree->getInternals()[id];
	}

	Tree::edge_t edge = {intNode->getParent(), intNode};
	return edge;
}

TreeProposal::edge_direction_t TreeProposal::getEdgeDirection(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2) {
	// If n1 is the root then direction is N2 to N1
	if(n1 == tree->getRoot()){
		return FROM_N2_TO_N1;
	}
	// If the parent is the root : its false
	if(n2 == tree->getRoot()){
		return FROM_N1_TO_N2;
	}

	// Get the the edge direction
	TreeNode* n1Parent = n1->getParent();
	if(n1Parent == n2) { // The root node is toward out node
		return FROM_N1_TO_N2;
	} else { // The root node point inside the subtree
		return FROM_N2_TO_N1;
	}
}

/**
 * Return true if the root node is in the clad : parent --> {child --> clad}
 * @param tree the tree in question
 * @param inNode The first node inside of the subtree (clad)
 * @param outNode the first node out of the subtree
 * @return True is the root node is in the subtree, false else
 */
bool TreeProposal::containsRootNode(Tree::sharedPtr_t tree, TreeNode* inNode, TreeNode* outNode) {
	edge_direction_t direction = getEdgeDirection(tree, inNode, outNode);
	return direction == FROM_N2_TO_N1;
}

void TreeProposal::reverseDirection(Tree::sharedPtr_t tree, std::vector<TreeNode*> path) {
	assert(path.size() > 1);
	// Get path direction
	edge_direction_t direction = getEdgeDirection(tree, path[0], path[1]);
	edge_direction_t oppositeDirection = direction == FROM_N1_TO_N2 ? FROM_N2_TO_N1 : FROM_N1_TO_N2;

	// Remove each edge of the path
	for(size_t iP=0; iP<path.size()-1; ++iP) {
		removeEdge(tree, path[iP], path[iP+1]);
	}

	// Remove each edge of the path
	for(size_t iP=0; iP<path.size()-1; ++iP) {
		addEdge(tree, path[iP], path[iP+1], oppositeDirection);
	}
}

void TreeProposal::addEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2, edge_direction_t direction) {

	// Swap the last element of neighbours to be sure that the parent finish in the good spot
	TreeNode *cNode, *pNode;
	if(direction == FROM_N1_TO_N2) { // Parent is N2
		cNode = n1;
		pNode = n2;
	} else {
		cNode = n2;
		pNode = n1;
	}

	// Add edge
	cNode->addNeighbour(pNode, true);
	pNode->addNeighbour(cNode, false);

}

TreeProposal::edge_direction_t TreeProposal::removeEdge(Tree::sharedPtr_t tree, TreeNode* n1, TreeNode* n2) {
	edge_direction_t dir = getEdgeDirection(tree, n1, n2);
	n1->removeNeighbour(n2);
	n2->removeNeighbour(n1);
	return dir;
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
