//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Tree.h
 *
 * @date Nov 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREE_TREERECONSTRUCTION_H_
#define TREE_TREERECONSTRUCTION_H_

#include "TreeNode.h"
#include "Move/Move.h"
#include "TreeParser/Parser.h"
#include "Utils/Code/Algorithm.h"
#include "Utils/Code/SerializationSupport.h"
#include "Parallel/Manager/MpiManager.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"

#include <algorithm>
#include <boost/functional/hash.hpp>
#include <boost/shared_ptr.hpp>

namespace MolecularEvolution {
namespace TreeReconstruction {

class Tree {
public:
	typedef boost::shared_ptr<Tree> sharedPtr_t;

	struct edge_t {
		TreeNode* n1;
		TreeNode* n2;
	};

public:
	//Tree(const Tree &other);
	Tree(bool doShuffle, const DataLoader::MSA &, const std::map<std::string, int> & nameMapping);
	Tree(const DL::TreeNode &newickNode, const std::map<std::string, int> & nameMapping);
	Tree(const std::string &aTreeStr);
	Tree(const Utils::Serialize::buffer_t &buffer);
	~Tree();

	bool isEqualTo(const Tree* other) const;

	std::string getIdString() const;
	std::string getNameString(const std::vector<std::string> &names) const;

	TreeNode* getRoot();
	TreeNode* getNode(size_t id);
	std::vector<edge_t> getInternalEdges();
	std::vector<TreeNode*>& getInternals();
	std::vector<TreeNode*>& getTerminals();

	long int getHashKey() const;

	void defineNodesCategory();

	//Tree* clone() const; // DEPRECATED AND NOT WORKING

	void update(const std::vector<TreeNode*> &aUpdatedNodes);
	const std::vector<TreeNode*>& getUpdatedNodes() const;
	void clearUpdatedNodes();

	const Utils::Serialize::buffer_t& save();

	void DBG_checkChildenOrder(TreeNode *node, TreeNode *parent);

    void defineBalancingRoot();

private:

	long int myH;
	TreeNode *root;
	std::string idString;

	std::vector<TreeNode*> internals;
	std::vector<TreeNode*> terminals;
	std::vector<TreeNode*> updatedNodes;

	typedef std::map<size_t, TreeNode*> nodesMap_t;
	typedef nodesMap_t::iterator itNodesMap_t;
	nodesMap_t nodesMap;

	void createHashKey();

	void exploreTree(TreeNode *node, TreeNode *parent);
	void updateTree(const std::vector<TreeNode*> &aUpdatedNodes);
	void createRecursive(const DL::TreeNode &newickNode, TreeNode *node,
			const std::map<std::string, int> & nameMapping);

	TreeNode* createFromMSA(bool doShuffle, const DataLoader::MSA &msa,
			const std::map<std::string, int>& nameMapping);
	void assignIdRecursively(size_t &id, TreeNode *node, TreeNode *parent);

	void load(const Utils::Serialize::buffer_t &buffer);

	// Serialization
	Utils::Serialize::buffer_t serializedBuffer;
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int version) const;

    template<class Archive>
    void load(Archive & ar, const unsigned int version);
    BOOST_SERIALIZATION_SPLIT_MEMBER()

    //void copy(const Tree &other);
    //TreeNode* cloneTree(TreeNode *node, TreeNode *parent);

};

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* TREE_TREERECONSTRUCTION_H_ */
