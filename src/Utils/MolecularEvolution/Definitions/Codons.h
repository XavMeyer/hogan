//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Codons.h
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#ifndef CODONS_H_
#define CODONS_H_

#include "AminoAcids.h"

#include <stdlib.h>

#include <vector>
#include <map>
#include <string>
#include <iostream>

namespace MolecularEvolution {
namespace Definition {

class Codons {
public:

	struct index_t {
		int idCodon, idAA;
	};

public:

	static const Codons* getInstance();

	std::string getCodonStr(const int codonIdx) const;
	int getCodonIdx(const std::string &codonStr) const;
	int getAminoAcidIdx(const std::string &codonStr) const;


	const std::map<std::string, index_t>& getCodonMap() const;
	const std::vector<std::string>& getCodonVector() const;

	bool isSynonymous(const std::string &codon1, const std::string &codon2) const;

public:

	static const size_t NB_NUCL_PER_CODON, NB_CODONS, NB_CODONS_WO_STOP;

private:

	static Codons* instance;

	typedef std::vector<std::string> codonVector_t;

	typedef std::map<std::string, index_t> codonMap_t;
	typedef codonMap_t::iterator itCodonMap_t;
	typedef codonMap_t::const_iterator itCstCodonMap_t;

	codonVector_t codonVector;
	codonMap_t codonMap;

	void init();

protected:
	Codons();
	~Codons();
};

} /* namespace Definition */
} /* namespace MolecularEvolution */

#endif /* CODONS_H_ */
