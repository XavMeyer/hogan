//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Nucleotides.cpp
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#include "Nucleotides.h"

namespace MolecularEvolution {
namespace Definition {

const uint Nucleotides::NB_BASE_NUCL = 4;
const char Nucleotides::NUCL_BASE[] = { 'A', 'C', 'T', 'G'};
const char Nucleotides::NUCL_AMB[] = { 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V', 'N', '.', '-', '?' };

Nucleotides* Nucleotides::instance = NULL;

const Nucleotides* Nucleotides::getInstance() {
	if(!instance) {
		instance = new Nucleotides;
	}
	return instance;
}


Nucleotides::Nucleotides() {

}

Nucleotides::~Nucleotides() {
}

int Nucleotides::getNucleotideIdx(const char &nuclChar) const {
	for(uint i=0; i<NB_BASE_NUCL; i++){
		if (nuclChar == NUCL_BASE[i]) {
			return i;
		}
	}

	return -1;
}

std::string Nucleotides::ambiguousToBase(const char &codonStr) const {

	switch (codonStr) {
	case 'A':
		return "A";
	case 'C':
		return "C";
	case 'T':
		return "T";
	case 'G':
		return "G";
	case 'R':
		return "AG";
	case 'Y':
		return "CT";
	case 'S':
		return "GC";
	case 'W':
		return "AT";
	case 'K':
		return "GT";
	case 'M':
		return "AC";
	case 'B':
		return "CGT";
	case 'D':
		return "AGT";
	case 'H':
		return "ACT";
	case 'V':
		return "ACG";
	case 'X':
	case 'N':
	case '.':
	case '-' :
	case '?' :
		return "ACTG";
	default:
		std::cerr << "[ERROR] std::string Nucleotides::ambiguousToBase(const char &codonStr) const;" << std::endl;
		std::cerr << "Character '" << codonStr << "' not correct." << std::endl;
		abort();
		return "";
	}
}

bool Nucleotides::isTransversion(const char from, const char to) const {
	return !isTransition(from, to);
}

bool Nucleotides::isTransversion(const size_t idxFrom, const size_t idxTo) const {
	assert(idxFrom < NB_BASE_NUCL && idxTo < NB_BASE_NUCL);
	return !isTransition(NUCL_BASE[idxFrom], NUCL_BASE[idxTo]);
}

bool Nucleotides::isTransition(const char from, const char to) const {

	int iFrom = getNucleotideIdx(from);
	int iTo = getNucleotideIdx(to);

	if((iFrom < 0) || (iTo < 0)) {
		std::cerr << "bool Nucleotides::isTransition(const char from, const char to) const;" << std::endl;
		std::cerr << "Error : character doesn't represent a non-ambiguous nucleotide." << std::endl;
		abort();
	}

	if((from == 'A' && to == 'G') || (from == 'G' && to == 'A')) {
		return true;
	}
	if((from == 'C' && to == 'T') || (from == 'T' && to == 'C')) {
		return true;
	}
	return false;
}

bool Nucleotides::isTransition(const size_t idxFrom, const size_t idxTo) const {
	assert(idxFrom < NB_BASE_NUCL && idxTo < NB_BASE_NUCL);
	return isTransition(NUCL_BASE[idxFrom], NUCL_BASE[idxTo]);
}

const size_t Nucleotides::getNBNucleotideBase() const {
	return NB_BASE_NUCL;
}

const std::string Nucleotides::getNucleotideVector() const {
	return std::string(NUCL_BASE, NB_BASE_NUCL);
}

} /* namespace Definition */
} /* namespace MolecularEvolution */
