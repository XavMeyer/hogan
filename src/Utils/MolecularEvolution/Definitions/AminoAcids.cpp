//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AminoAcids.cpp
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#include "AminoAcids.h"

namespace MolecularEvolution {
namespace Definition {

AminoAcids* AminoAcids::instance = NULL;

const AminoAcids* AminoAcids::getInstance() {
	if(!instance) {
		instance = new AminoAcids;
	}
	return instance;
}

AminoAcids::AminoAcids() {
	createAminoAcids();
}

AminoAcids::~AminoAcids() {
}

void AminoAcids::createAminoAcids() {
	// Isoleucine - I
	AA.push_back(AminoAcid("Isoleucine", 'I', boost::assign::list_of("ATT")("ATC")("ATA")));
	// Leucine - L
	AA.push_back(AminoAcid("Leucine", 'L', boost::assign::list_of("CTT")("CTC")("CTA")("CTG")("TTA")("TTG")));
	// Valine - V
	AA.push_back(AminoAcid("Valine", 'V', boost::assign::list_of("GTT")("GTC")("GTA")("GTG")));
	// Phenylalanine - F
	AA.push_back(AminoAcid("Phenylalanine", 'F', boost::assign::list_of("TTT")("TTC")));
	// Methionine - M
	AA.push_back(AminoAcid("Methionine", 'M', boost::assign::list_of("ATG")));
	// Cysteine - C
	AA.push_back(AminoAcid("Cysteine", 'C', boost::assign::list_of("TGT")("TGC")));
	// Alanine - A
	AA.push_back(AminoAcid("Alanine", 'A', boost::assign::list_of("GCT")("GCC")("GCA")("GCG")));
	// Glycine - G
	AA.push_back(AminoAcid("Glycine", 'G', boost::assign::list_of("GGT")("GGC")("GGA")("GGG")));
	// Proline - P
	AA.push_back(AminoAcid("Proline", 'P', boost::assign::list_of("CCT")("CCC")("CCA")("CCG")));
	// Threonine - T
	AA.push_back(AminoAcid("Threonine", 'T', boost::assign::list_of("ACT")("ACC")("ACA")("ACG")));
	// Serine - S
	AA.push_back(AminoAcid("Serine", 'S', boost::assign::list_of("TCT")("TCC")("TCA")("TCG")("AGT")("AGC")));
	// Tyrosine - Y
	AA.push_back(AminoAcid("Tyrosine", 'Y', boost::assign::list_of("TAT")("TAC")));
	// Tryptophan - W
	AA.push_back(AminoAcid("Tryptophan", 'W', boost::assign::list_of("TGG")));
	// Glutamine - Q
	AA.push_back(AminoAcid("Glutamine", 'Q', boost::assign::list_of("CAA")("CAG")));
	// Asparagine - N
	AA.push_back(AminoAcid("Asparagine", 'N', boost::assign::list_of("AAT")("AAC")));
	// Histidine - H
	AA.push_back(AminoAcid("Histidine", 'H', boost::assign::list_of("CAT")("CAC")));
	// Glutamic acid - E
	AA.push_back(AminoAcid("Glutamic acid", 'E', boost::assign::list_of("GAA")("GAG")));
	// Aspartic acid - D
	AA.push_back(AminoAcid("Aspartic acid", 'D', boost::assign::list_of("GAT")("GAC")));
	// Lysine - K
	AA.push_back(AminoAcid("Lysine", 'K', boost::assign::list_of("AAA")("AAG")));
	// Arginine - R
	AA.push_back(AminoAcid("Arginine", 'R', boost::assign::list_of("CGT")("CGC")("CGA")("CGG")("AGA")("AGG")));
	// Stop codons - Stop
	AA.push_back(AminoAcid("Stop codons", '-', boost::assign::list_of("TAA")("TAG")("TGA")));
}

const std::vector< AminoAcid >& AminoAcids::getAminoAcids() const {
	return AA;
}

} /* namespace Definition */
} /* namespace MolecularEvolution */
