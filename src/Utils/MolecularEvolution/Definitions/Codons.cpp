//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Codons.cpp
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#include "Codons.h"

namespace MolecularEvolution {
namespace Definition {

const size_t Codons::NB_NUCL_PER_CODON = 3;
const size_t Codons::NB_CODONS = 64;
const size_t Codons::NB_CODONS_WO_STOP = 61;

Codons* Codons::instance = NULL;

const Codons* Codons::getInstance() {
	if(!instance) {
		instance = new Codons;
	}
	return instance;
}

Codons::Codons() {
	init();
}

Codons::~Codons() {
}

void Codons::init() {
	using std::pair;

	const AminoAcids *AA = AminoAcids::getInstance();
	const std::vector<AminoAcid> &aaVec = AA->getAminoAcids();

	int idCodon = 0;
	for(size_t iAA = 0; iAA < aaVec.size(); ++iAA) {
		for(size_t iC = 0; iC < aaVec[iAA].codons.size(); ++iC) {
			codonVector.push_back(aaVec[iAA].codons[iC]);
			index_t index = {(int)idCodon, (int)iAA};
			codonMap.insert(pair<std::string, index_t>(aaVec[iAA].codons[iC], index));
			++idCodon;
		}
	}
}

std::string Codons::getCodonStr(const int codonIdx) const {
	return codonVector[codonIdx];
}


int Codons::getCodonIdx(const std::string &codonStr) const {
	assert(codonStr.size() == 3);
	itCstCodonMap_t itCM = codonMap.find(codonStr);
	if(itCM != codonMap.end()) {
		return itCM->second.idCodon;
	} else {
		return -1;
	}
}

int Codons::getAminoAcidIdx(const std::string &codonStr) const {
	assert(codonStr.size() == 3);
	itCstCodonMap_t itCM = codonMap.find(codonStr);
	if(itCM != codonMap.end()) {
		return itCM->second.idAA;
	} else {
		return -1;
	}
}

bool Codons::isSynonymous(const std::string &codon1, const std::string &codon2) const {
	int idxAA1 = getAminoAcidIdx(codon1);
	int idxAA2 = getAminoAcidIdx(codon2);

	if((idxAA1 < 0) || (idxAA2 < 0)) {
		std::cerr << "bool Codons::isSynonymous(const std::string &codon1, const std::string &codon2) const;" << std::endl;
		std::cerr << "Error : string (" << codon1 << ", " << codon2 << ") doesn't represent a codon." << std::endl;
		abort();
	}

	return idxAA1 == idxAA2;
}

const std::map<std::string, Codons::index_t>& Codons::getCodonMap() const {
	return codonMap;
}

const std::vector<std::string>& Codons::getCodonVector() const {
	return codonVector;
}

} /* namespace Definition */
} /* namespace MolecularEvolution */
