//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AminoAcids.h
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#ifndef AMINOACIDS_H_
#define AMINOACIDS_H_

#include <stdlib.h>
#include <iostream>

#include "AminoAcid.h"
#include <boost/assign/list_of.hpp>

namespace MolecularEvolution {
namespace Definition {

class AminoAcids {
public:

	static const AminoAcids* getInstance();
	const std::vector< AminoAcid >& getAminoAcids() const;

private:

	static AminoAcids* instance;

	std::vector< AminoAcid > AA;

	void createAminoAcids();

protected:

	AminoAcids();
	~AminoAcids();

};

} /* namespace Definition */
} /* namespace MolecularEvolution */

#endif /* AMINOACIDS_H_ */
