//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Nucleotides.h
 *
 * @date Jan 23, 2015
 * @author meyerx
 * @brief
 */
#ifndef NUCLEOTIDES_H_
#define NUCLEOTIDES_H_

#include <stdlib.h>

#include <assert.h>
#include <iostream>
#include <string>

namespace MolecularEvolution {
namespace Definition {

class Nucleotides {
public:

	static const Nucleotides* getInstance();


	int getNucleotideIdx(const char &codonStr) const;
	std::string ambiguousToBase(const char &codonStr) const;

	bool isTransversion(const char from, const char to) const;
	bool isTransition(const char from, const char to) const;

	bool isTransversion(const size_t idxFrom, const size_t idxTo) const;
	bool isTransition(const size_t idxFrom, const size_t idxTo) const;

	const size_t getNBNucleotideBase() const;
	const std::string getNucleotideVector() const;

public:

	static const uint NB_BASE_NUCL;
	static const char NUCL_BASE[];
	static const char NUCL_AMB[];

private:
	static Nucleotides*	instance;

protected:
	Nucleotides();
	~Nucleotides();

};

} /* namespace Definition */
} /* namespace MolecularEvolution */

#endif /* NUCLEOTIDES_H_ */
