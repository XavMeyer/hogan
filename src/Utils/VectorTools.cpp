//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file VectorTools.cpp
 *
 * @date Nov 24, 2014
 * @author meyerx
 * @brief
 */
#include "VectorTools.h"

namespace Utils {
namespace Vector {

vector<double> sum(const vector<double> &V1, const vector<double> &V2) {

	assert(V1.size() == V2.size());
	vector<double> res(V1);
	for(size_t i=0; i<V1.size(); ++i){
		res[i] += V2[i];
	}
	return res;

}

vector<double> sub(const vector<double> &V1, const vector<double> &V2) {
	assert(V1.size() == V2.size());
	vector<double> res(V1);
	for(size_t i=0; i<V1.size(); ++i){
		res[i] -= V2[i];
	}
	return res;
}

vector<double> scale(const double alpha, const vector<double> &V1) {
	vector<double> res(V1);
	for(size_t i=0; i<V1.size(); ++i){
		res[i] *= alpha;
	}
	return res;
}

double normL2(const vector<double> &V) {
	double nrm = 0.;
	for(size_t i=0; i<V.size(); ++i){
		nrm += V[i]*V[i];
	}
	return sqrt(nrm);
}


/*static vector<double> unit(const vector<double> &V) {
	double nrm = normL2(V);
	return unit(nrm, V);
}*/

vector<double> unit(const double nrm, const vector<double> &V) {
	vector<double> unit(V.size());
	for(size_t i=0; i<V.size(); ++i){
		unit[i] = V[i] / nrm;
	}
	return unit;
}

void daxpy(double alpha, vector<double> &X, vector<double> &Y) {
	assert(X.size() == Y.size());

	Eigen::Map<Eigen::VectorXd> mapX(X.data(), X.size());
	Eigen::Map<Eigen::VectorXd> mapY(Y.data(), Y.size());

	mapY = alpha * mapX;
}

std::string toString(const vector<double> &X) {
	std::stringstream ss;
	for(size_t i=0; i<X.size(); ++i) {
		ss << std::setprecision(4) << std::scientific << std::setw(2) << X[i] << " ";
	}

	return ss.str();
}



} /* namespace Vector */
} /* namespace Utils */
