//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file VectorTools.h
 *
 * @date Nov 7, 2014
 * @author meyerx
 * @brief
 */
#ifndef VECTORTOOLS_H_
#define VECTORTOOLS_H_

#include <cassert>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include <Eigen/Core>

using namespace std;

namespace Utils {
namespace Vector {

vector<double> sum(const vector<double> &V1, const vector<double> &V2);

vector<double> sub(const vector<double> &V1, const vector<double> &V2);

vector<double> scale(const double alpha, const vector<double> &V1);

double normL2(const vector<double> &V);

vector<double> unit(const double nrm, const vector<double> &V);

void daxpy(double alpha, vector<double> &X, vector<double> &Y);

std::string toString(const vector<double> &X);

} /* namespace Vector */
} /* namespace Utils */

#endif /* VECTORTOOLS_H_ */
