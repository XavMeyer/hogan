//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef READERXML_H_
#define READERXML_H_

#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/Sampler/ReaderSamplerXML.h"
#include "Utils/XML/Likelihood/ReaderLikelihoodXML.h"

#include "Tag.h"

#include "Parallel/Parallel.h"

#include <string>
#include <iostream>

namespace XML {

class ReaderXML {
private:
	typedef Sampler::MCMC::sharedPtr_t samplerPtr_t;
	typedef StatisticalModel::Model::sharedPtr_t modelPtr_t;

public:
	ReaderXML(const std::string &fileName);
	~ReaderXML();

	ReaderLikelihoodXML::sharedPtr_t getPtrLikXML();
	ReaderSamplerXML::sharedPtr_t getPtrSamplerXML();
	ReaderLikelihoodXML::modelPtr_t getPtrModel();
	ReaderSamplerXML::samplerPtr_t getPtrSampler();

	size_t getNIteration();

private:
	static const size_t DEFAULT_SEED;

	size_t seed;
	size_t nProposal, nChain, nIteration;

	std::ofstream redirectFile;
	 std::streambuf *coutbuf;

	TiXmlDocument doc;
	TiXmlElement *pRoot;

	ReaderLikelihoodXML::sharedPtr_t ptrLikXML;
	ReaderSamplerXML::sharedPtr_t ptrSamplerXML;

	void initXML();
	void readXML();
	void readLogFile();
	void readSeed();
	void readTopology();
	void readNSample();
	void readLikelihood();
	void readSampler();

};

} /* namespace XML */

#endif /* READERXML_H_ */
