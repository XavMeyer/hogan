//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterXML.h
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef WRITERXML_H_
#define WRITERXML_H_

#include "Tag.h"
#include "Utils/XML/HelpersXML.h"
#include "Likelihood/WriterLikelihoodXML.h"
#include "Sampler/WriterSamplerXML.h"

#include "Model/Model.h"
#include "Sampler/IncSamplers.h"

namespace XML {

class WriterXML {
public:
	WriterXML(const std::string &fileName);
	~WriterXML();

	void writeXML(size_t nIteration, StatisticalModel::Model *aModel, Sampler::MCMC *aSampler);

private:
	TiXmlDocument doc;
	TiXmlElement *pRoot;

	void initXML();
	void writeBase(size_t nIteration);
	void writeLikelihood(StatisticalModel::Model *aModel);
	void writeSampler(Sampler::MCMC *aSampler);

};

} /* namespace XML */

#endif /* WRITERXML_H_ */
