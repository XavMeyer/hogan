//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderXML.h"

namespace XML {

const size_t ReaderXML::DEFAULT_SEED = 1337;

ReaderXML::ReaderXML(const std::string &fileName) : doc(fileName) {

	seed = DEFAULT_SEED;
	nProposal = Parallel::mpiMgr().getNProc();
	nChain = 1;

	coutbuf = NULL;

	initXML();
	readXML();
}

ReaderXML::~ReaderXML() {

	if(coutbuf!=NULL) {
		std::cout.rdbuf(coutbuf);
		redirectFile.close();
	}

}

ReaderLikelihoodXML::sharedPtr_t ReaderXML::getPtrLikXML() {
	return ptrLikXML;
}

ReaderSamplerXML::sharedPtr_t ReaderXML::getPtrSamplerXML() {
	return ptrSamplerXML;
}

ReaderLikelihoodXML::modelPtr_t ReaderXML::getPtrModel() {
	return ptrLikXML->getModelPtr();
}

ReaderSamplerXML::samplerPtr_t ReaderXML::getPtrSampler() {
	return ptrSamplerXML->getSamplerPtr();
}

size_t ReaderXML::getNIteration() {
	return nIteration;
}

void ReaderXML::initXML() {
	if (!doc.LoadFile()) {
		XML::errorMessage(__func__, "Unable to open XML file.");
	}

	pRoot = doc.FirstChildElement(Tag::ROOT_TAG);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void ReaderXML::readXML() {
	readTopology();
	//Parallel::mpiMgr().defineTopology(nProposal, nChain);
	Parallel::mcmcMgr().init(false, 1, nProposal, nChain);

	readLogFile();

	readSeed();
	std::srand(seed); // Every thread as the same std rng
	// During init everybody has the same PRNG
	Parallel::mpiMgr().setSeedPRNG(seed);

	readNSample();

	readLikelihood();
	readSampler();

	// From there each thread as a different PRNG
	const int myRank = Parallel::mpiMgr().getRank();
	Parallel::mpiMgr().setSeedPRNG(myRank*5+seed);


}

void ReaderXML::readLogFile() {
	std::string logFile;

	bool isUsingCheckpoint = pRoot->FirstChildElement(Tag::SAMPLER_TAG) != NULL &&
			 pRoot->FirstChildElement(Tag::SAMPLER_TAG)->FirstChildElement(Tag::CHECKPOINT_TAG)!= NULL;

	if(XML::readElement(__func__, Tag::LOG_FILE_TAG, pRoot, logFile, OPTIONAL)) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			logFile.append(".out");
			if(!isUsingCheckpoint) {
				redirectFile.open(logFile.c_str());
			} else {
				redirectFile.open(logFile.c_str(), std::ios::app);
			}
			coutbuf = std::cout.rdbuf();
			std::cout.rdbuf(redirectFile.rdbuf());
		}
	}
}

void ReaderXML::readSeed() {
	XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL);
}

void ReaderXML::readTopology() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::TOPOLOGY_TAG);
	if(!pChild){
		XML::warningMissingTag(__func__, Tag::TOPOLOGY_TAG);
		return;
	}

	XML::readElement(__func__, Tag::NPROPOSAL_TAG, pChild, nProposal, OPTIONAL);
	XML::readElement(__func__, Tag::NCHAIN_TAG, pChild, nChain, OPTIONAL);

	if(nProposal*nChain != (size_t)Parallel::mpiMgr().getNProc()) {
		std::stringstream ssErr;
		ssErr << "MPI number of processor ( " << Parallel::mpiMgr().getNProc();
		ssErr << " ) doesn't correspond to the requested number of proposal times the number of chain ( ";
		ssErr << nProposal << " x " << nChain << " = " << nProposal*nChain << " ).";
		XML::errorMessage(__func__, ssErr.str());
	}
}

void ReaderXML::readNSample() {
	XML::readElement(__func__, Tag::NITERATION_TAG, pRoot, nIteration, OPTIONAL);
}

void ReaderXML::readLikelihood() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::LIKELIHOOD_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrLikXML.reset(new ReaderLikelihoodXML(*pAttrib, pChild));
		}
	}
}

void ReaderXML::readSampler() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::SAMPLER_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::SAMPLER_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrSamplerXML.reset(new ReaderSamplerXML(*pAttrib, pChild, ptrLikXML->getModelPtr()));
		}
	}
}

} /* namespace XML */
