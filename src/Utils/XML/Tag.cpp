//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Tag.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "Tag.h"

namespace XML {

// Base
const std::string Tag::ROOT_TAG = "config";
const std::string Tag::SEED_TAG = "seed";
const std::string Tag::N_TIME_PER_BLOCK_TAG = "nSuccinctProposalPerBlock";
const std::string Tag::FREQ_TREE_MOVE_TAG = "treeMoveFrequency";
const std::string Tag::TOPOLOGY_TAG = "topology";
const std::string Tag::NPROPOSAL_TAG = "nProposal";
const std::string Tag::NCHAIN_TAG = "nChain";
const std::string Tag::LOG_FILE_TAG = "logFile";
const std::string Tag::STDOUTPUT_FREQUENCY_TAG = "stdOutputFrequency";
// Likelihood
const std::string Tag::LIKELIHOOD_TAG = "likelihood";
// Sampler
const std::string Tag::SAMPLER_TAG = "sampler";
const std::string Tag::NAME_ATT = "name";
// nSamples
const std::string Tag::NITERATION_TAG = "nIteration";

// Likelihood
const std::string Tag::INDEPENDENT_NORMAL_NAME = "independentNormals";
const std::string Tag::CORRELATED_NORMAL_NAME = "correlatedNormals";
const std::string Tag::BDRATE_NAME = "BDRate";
const std::string Tag::BDRATE_UPD_NAME = "BDRateUpd";
const std::string Tag::BDRATE_BIG_UPD_NAME = "BDRateBig";
const std::string Tag::BDRATE_ALPHA_UPD_NAME = "BDRateAlpha";
const std::string Tag::BDRATE_DAG_NAME = "BDRateDAG";
const std::string Tag::BDRATE_ALPHA_DAG_NAME = "BDRateAlphaDAG";
const std::string Tag::SEL_POS_BASE_NAME = "PositiveSelectionBase";
const std::string Tag::SEL_POS_LIGHT_NAME = "PositiveSelectionLight";
const std::string Tag::BRANCH_SITE_REL_NAME = "BranchSiteREL";
const std::string Tag::STOCHASTIC_BS_NAME = "StochasticBS";
const std::string Tag::CODONS_MODEL_NAME = "CodonModels";
const std::string Tag::TREE_INFERENCE_NAME = "TreeInference";

const std::string Tag::FILE_TAG = "filename";
const std::string Tag::NDIM_TAG = "nDim";
const std::string Tag::NDATA_TAG = "nData";
const std::string Tag::NTHREAD_TAG = "nThread";

const std::string Tag::PARAMETERS_TAG = "parameters";
const std::string Tag::BLOCKS_TAG = "blocks";
const std::string Tag::BLOCK_SIZE_TAG = "blockSize";
const std::string Tag::ADAPTIVE_TYPE_TAG = "adaptiveType";
const std::string Tag::NOT_ADAPTIVE_TAG = "NotAdaptive";
const std::string Tag::DEFAULT_ADAPTIVE_TAG = "DefaultAdaptive";
const std::string Tag::MIXED_ADAPTIVE_TAG = "MixedAdaptive";
const std::string Tag::PCA_ADAPTIVE_TAG = "PCA_Adaptive";

const std::string Tag::NQUANTILE_TAG = "nQuantile";
const std::string Tag::ISPLANT_TAG = "isPlant";

const std::string Tag::ALIGN_FILE_TAG = "alignFile";
const std::string Tag::TREE_FILE_TAG = "treeFile";
const std::string Tag::HYPOTHESIS_TAG = "hypothesis";
const std::string Tag::FG_BRANCH_TAG = "foregroundBranch";
const std::string Tag::USE_COMPRESSION_TAG = "isUsingCompression";
const std::string Tag::NUCL_MODEL_TAG = "NucleotideModel_ID";
const std::string Tag::CODON_FREQ_TYPE_TAG = "CodonFrequencyType_ID";
const std::string Tag::SCALING_TYPE_TAG = "scalingType";
const std::string Tag::MODEL_TYPE_TAG = "modelType";
const std::string Tag::TREE_SAMPLING_TAG = "sampleTreeTopology";
const std::string Tag::USE_FIXED_TREE_TOPOLOGY_TAG = "useFixedTree";
const std::string Tag::N_CLASS_TAG = "nClass";

// Sampler
const std::string Tag::PFAMCMC_NAME = "PFAMCMC";

const std::string Tag::BLOCK_SELECTOR_TAG = "blockSelector";
const std::string Tag::MODIFIER_STRAT_TAG = "modifierStrategy";
const std::string Tag::ACCEPTANCE_STRAT_TAG = "acceptanceStrategy";

const std::string Tag::CYCLYC_BS = "cyclic";
const std::string Tag::REVERSIBLE_BS = "reversible";
const std::string Tag::RANDOM_BS = "random";
const std::string Tag::OPTIMISED_BS = "optimised";
const std::string Tag::RANDOM_TREEINFERENCE_BS = "randomTreeInference";
const std::string Tag::OPTIMISED_TREEINFERENCE_BS = "optimisedTreeInference";

const std::string Tag::BLOCK_STAT_CFG_TAG = "blockStatCfg";

const std::string Tag::OUTPUT_TAG = "output";
const std::string Tag::NTHINNING_TAG = "nThinning";
const std::string Tag::VERBOSE_TAG = "verbose";
const std::string Tag::WRITE_BINARY_TAG = "writeBinary";
const std::string Tag::WRITE_TYPE_TAG = "writeType";

const std::string Tag::CHECKPOINT_TAG = "checkpoint";
const std::string Tag::CKP_N_CKP_KEPT = "keepNCheckpoints";
const std::string Tag::CKP_TIME_FREQUENCY_TAG = "timeFrequency";
const std::string Tag::CKP_ITER_FREQUENCY_TAG = "iterationFrequency";
const std::string Tag::CKP_INTERVAL_DURATION = "timeInterval";
const std::string Tag::CKP_NB_DURING_INTERVAL = "nbCheckpointDuringInterval";

// Values
const std::string Tag::DEFAULT = "default";
const std::string Tag::OPTIMIZED = "optimized";
const std::string Tag::RANDOM = "random";
const std::string Tag::SINGLE = "single";
const std::string Tag::WIDE = "wide";
const std::string Tag::PRIOR = "prior";
const std::string Tag::OPTI = "opti";
const std::string Tag::GOOD = "good";
const std::string Tag::SLOW = "slow";
const std::string Tag::FAST = "fast";
const std::string Tag::EXTRAFAST = "extraFast";
const std::string Tag::CUSTOM = "custom";

const std::string Tag::TRUE = "true";
const std::string Tag::FALSE = "false";

const std::string Tag::H0 = "H0";
const std::string Tag::H1 = "H1";

const std::string Tag::FLOAT = "float";
const std::string Tag::DOUBLE = "double";

} /* namespace XML */

