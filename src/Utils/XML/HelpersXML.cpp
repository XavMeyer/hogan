//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Helpers.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "HelpersXML.h"

namespace XML {

void warningMessage(const std::string &place, const std::string &warning) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[XML Config] Warning '" << warning << "'." << std::endl;
		std::cout << "[XML Config] ... at '" << place << "'." << std::endl;
	}

}

void warningMissingAttribute(const std::string &place, const std::string &attribute) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[XML Config] Warning 'missing attribute : " << attribute << "'. Default value used." << std::endl;
		std::cout << "[XML Config] ... at '" << place << "'." << std::endl;
	}

}

void warningMissingTag(const std::string &place, const std::string &tag) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[XML Config] Warning 'missing tag : " << tag << "'. Default value used." << std::endl;
		std::cout << "[XML Config] ... at '" << place << "'." << std::endl;
	}

}


void errorMessage(const std::string &place, const std::string &error) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cerr << "[XML Config] Error '" << error << "'." << std::endl;
		std::cerr << "[XML Config] ... at '" << place << "'." << std::endl;
	}
	Parallel::mpiMgr().abort();

}

void errorMissingAttribute(const std::string &place, const std::string &attribute) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cerr << "[XML Config] Error 'missing attribute : " << attribute << "'." << std::endl;
		std::cerr << "[XML Config] ... at '" << place << "'." << std::endl;
	}
	Parallel::mpiMgr().abort();

}

void errorMissingTag(const std::string &place, const std::string &tag) {

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cerr << "[XML Config] Error 'missing tag : " << tag << "'." << std::endl;
		std::cerr << "[XML Config] ... at '" << place << "'." << std::endl;
	}
	Parallel::mpiMgr().abort();

}

bool readTextAtFirstTag(const std::string &tag, TiXmlElement **pElem, std::string &text) {
	(*pElem) = (*pElem)->FirstChildElement(tag);
	if(!(*pElem)) {
		return false;
	} else {
		text = (*pElem)->GetText();
		return true;
	}
}

bool readTextAtNexTag(const std::string &tag, TiXmlElement **pElem, std::string &text) {
	(*pElem) = (*pElem)->NextSiblingElement(tag);
	if(!(*pElem)) {
		return false;
	} else {
		text = (*pElem)->GetText();
		return true;
	}
}

void printMessage(const std::string &place, const std::string &tag, presence_t presType) {
	if(presType == MANDATORY) {
		errorMissingTag(place, tag);
		abort();
	} else if(presType == OPTIONAL ) {
		warningMissingTag(place, tag);
	}
}


template<>
bool readElement<int>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, int &val, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, text)){
		val = atoi(text.c_str());
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}
}

template<>
bool readElement<size_t>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, size_t &val, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, text)){
		val = atol(text.c_str());
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}
}

template<>
bool readElement<double>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, double &val, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, text)){
		val = atof(text.c_str());
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}
}

template<>
bool readElement<std::string>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, std::string &val, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, val)){
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}
}

template<>
bool readElements<double>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, std::vector<double> &vals, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, text)){ // Read first
		vals.push_back(atof(text.c_str()));
		while(readTextAtNexTag(tag, &pRoot, text)) { // Read next
			vals.push_back(atof(text.c_str()));
		}
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}

}

template<>
bool readElements<size_t>(const std::string &place, const std::string &tag, TiXmlElement *pRoot, std::vector<size_t> &vals, presence_t presType) {
	std::string text;
	if(readTextAtFirstTag(tag, &pRoot, text)){ // Read first
		vals.push_back(atol(text.c_str()));
		while(readTextAtNexTag(tag, &pRoot, text)) { // Read next
			vals.push_back(atol(text.c_str()));
		}
		return true;
	} else {
		printMessage(place, tag, presType);
		return false;
	}
}

TiXmlElement* createTag(TiXmlElement *pRoot, const std::string &tag) {
	TiXmlElement* pChild = new TiXmlElement(tag);
	pRoot->LinkEndChild(pChild);
	return pChild;
}


} /* namespace XML */

