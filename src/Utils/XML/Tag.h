//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Tags.h
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#ifndef TAG_H_
#define TAG_H_

#include <string>

namespace XML {

class Tag {
public:

	// BASE
	static const std::string ROOT_TAG, SEED_TAG, TOPOLOGY_TAG, N_TIME_PER_BLOCK_TAG;
	static const std::string NPROPOSAL_TAG, NCHAIN_TAG;
	static const std::string LIKELIHOOD_TAG, SAMPLER_TAG;
	static const std::string NAME_ATT, NITERATION_TAG, FREQ_TREE_MOVE_TAG;
	static const std::string LOG_FILE_TAG, STDOUTPUT_FREQUENCY_TAG;


	// Model / Likelihood
	static const std::string INDEPENDENT_NORMAL_NAME, CORRELATED_NORMAL_NAME;
	static const std::string BDRATE_NAME, BDRATE_UPD_NAME, BDRATE_BIG_UPD_NAME;
	static const std::string BDRATE_ALPHA_UPD_NAME, BDRATE_DAG_NAME, BDRATE_ALPHA_DAG_NAME;
	static const std::string SEL_POS_BASE_NAME, SEL_POS_LIGHT_NAME, BRANCH_SITE_REL_NAME;
	static const std::string STOCHASTIC_BS_NAME, CODONS_MODEL_NAME, TREE_INFERENCE_NAME;
	static const std::string FILE_TAG, NDIM_TAG, NDATA_TAG, NTHREAD_TAG;
	static const std::string PARAMETERS_TAG, BLOCKS_TAG, BLOCK_SIZE_TAG, ADAPTIVE_TYPE_TAG;
	static const std::string NQUANTILE_TAG, ISPLANT_TAG;
	static const std::string TREE_FILE_TAG, ALIGN_FILE_TAG, HYPOTHESIS_TAG, FG_BRANCH_TAG;
	static const std::string USE_COMPRESSION_TAG, NUCL_MODEL_TAG, CODON_FREQ_TYPE_TAG;
	static const std::string MODEL_TYPE_TAG, N_CLASS_TAG, SCALING_TYPE_TAG;
	static const std::string TREE_SAMPLING_TAG, USE_FIXED_TREE_TOPOLOGY_TAG;

	// Sampler
	static const std::string PFAMCMC_NAME;
	static const std::string BLOCK_SELECTOR_TAG, MODIFIER_STRAT_TAG, ACCEPTANCE_STRAT_TAG;
	static const std::string CYCLYC_BS, REVERSIBLE_BS, RANDOM_BS, RANDOM_TREEINFERENCE_BS;
	static const std::string OPTIMISED_BS, OPTIMISED_TREEINFERENCE_BS;
	static const std::string BLOCK_STAT_CFG_TAG, OUTPUT_TAG, NTHINNING_TAG;
	static const std::string VERBOSE_TAG, WRITE_BINARY_TAG, WRITE_TYPE_TAG;

	static const std::string CHECKPOINT_TAG, CKP_N_CKP_KEPT, CKP_TIME_FREQUENCY_TAG;
	static const std::string CKP_ITER_FREQUENCY_TAG, CKP_INTERVAL_DURATION, CKP_NB_DURING_INTERVAL;


	void setTimeFrequency(double aTimeInSecond);
	void setTimeInterval(double aTimeInSecond, size_t aNumberOfCKP=5);
	void setIterationFrequency(size_t aNumberOfIter);

	// Block
	static const std::string NOT_ADAPTIVE_TAG, DEFAULT_ADAPTIVE_TAG;
	static const std::string MIXED_ADAPTIVE_TAG, PCA_ADAPTIVE_TAG ;

	// General values
	static const std::string DEFAULT, OPTIMIZED, RANDOM, SINGLE, WIDE, PRIOR;
	static const std::string OPTI, GOOD, TRUE, FALSE, H0, H1;
	static const std::string SLOW, FAST, EXTRAFAST, CUSTOM, FLOAT, DOUBLE;


};

} /* namespace XML */
#endif /* TAGS_H_ */
