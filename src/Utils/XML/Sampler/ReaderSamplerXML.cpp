//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SamplerXML.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderSamplerXML.h"

namespace XML {

const size_t ReaderSamplerXML::DEFAULT_SEED = 1234;

ReaderSamplerXML::ReaderSamplerXML(const std::string &aName, TiXmlElement *aPRoot, StatisticalModel::Model::sharedPtr_t aPtrModel) :
		name(aName), pRoot(aPRoot), ptrModel(aPtrModel) {
	readSampler();
}

ReaderSamplerXML::~ReaderSamplerXML() {
}

ReaderSamplerXML::samplerPtr_t ReaderSamplerXML::getSamplerPtr() {
	return ptrSampler;
}

void ReaderSamplerXML::readSampler() {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrModel->getLikelihood().get());
	Sampler::Sample sample = hlp->defineInitSample(ptrModel->getParams());

	if(name == Tag::PFAMCMC_NAME) {
		readStrategies();
		readBlockStatConfig();
		ptrSampler.reset(
				new Sampler::PFAMCMC(sample, *ptrModel.get(),
						ptrBS, ptrMS, ptrAS, ptrCfgFacto));
	} else {
		std::stringstream ss;
		ss << "Sampler '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

	readOutput();
}

void ReaderSamplerXML::readStrategies() {

	// Block Selector
	std::string blockSel;
	if(XML::readElement(__func__, Tag::BLOCK_SELECTOR_TAG, pRoot, blockSel, OPTIONAL)){
		readBlockSelector(blockSel);
	} else {
		ptrBS = Sampler::Strategies::BlockSelector::createSingleCyclic(*ptrModel.get());
	}

	// Modifier Strategy
	std::string modStrat;
	if(XML::readElement(__func__, Tag::MODIFIER_STRAT_TAG, pRoot, modStrat, OPTIONAL)){
		readModifierStrategy(modStrat);
	} else {
		ptrMS = Sampler::Strategies::ModifierStrategy::createDefaultModifierStrategy(*ptrModel.get());

	}

	// Acceptance strategy
	std::string accStrat;
	if(XML::readElement(__func__, Tag::ACCEPTANCE_STRAT_TAG, pRoot, accStrat, OPTIONAL)){
		readAcceptanceStrategy(accStrat);
	} else {
		ptrAS = Sampler::Strategies::AcceptanceStrategy::createDefaultAcceptanceStrategy(ptrMS, *ptrModel.get());
	}

}

void ReaderSamplerXML::readBlockSelector(const std::string &blockSel) {

	if(blockSel == Tag::CYCLYC_BS) {
		ptrBS = Sampler::Strategies::BlockSelector::createSingleCyclic(*ptrModel.get());
	} else if (blockSel == Tag::REVERSIBLE_BS) {
		ptrBS = Sampler::Strategies::BlockSelector::createSingleRevCyclic(*ptrModel.get());
	} else if (blockSel == Tag::RANDOM_TREEINFERENCE_BS) {
		size_t seed;
		if(!XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL)){ // If value not correctly read
			seed = DEFAULT_SEED; // set to default
		}
		double freqTreeMove=0.;
		if(!XML::readElement(__func__, Tag::FREQ_TREE_MOVE_TAG, pRoot, freqTreeMove, OPTIONAL)){ // If value not correctly read
			freqTreeMove = 0.32; // set to default
		}
		ptrBS = Sampler::Strategies::BlockSelector::createRandomTreeInference(*ptrModel.get(), freqTreeMove, seed);
	} else if (blockSel == Tag::OPTIMISED_TREEINFERENCE_BS) {
		size_t seed;
		if(!XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL)){ // If value not correctly read
			seed = DEFAULT_SEED; // set to default
		}
		double freqTreeMove=0.;
		if(!XML::readElement(__func__, Tag::FREQ_TREE_MOVE_TAG, pRoot, freqTreeMove, OPTIONAL)){ // If value not correctly read
			freqTreeMove = 0.32; // set to default
		}
		ptrBS = Sampler::Strategies::BlockSelector::createOptimisedTreeInference(*ptrModel.get(), freqTreeMove, seed);
	} else if (blockSel == Tag::RANDOM_BS) {
		size_t seed;
		if(!XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL)){ // If value not correctly read
			seed = DEFAULT_SEED; // set to default
		}
		ptrBS = Sampler::Strategies::BlockSelector::createRandom(*ptrModel.get(), seed);
	} else if (blockSel == Tag::OPTIMISED_BS) {
		size_t seed;
		if(!XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL)){ // If value not correctly read
			seed = DEFAULT_SEED; // set to default
		}
		size_t NTPB;
		if(!XML::readElement(__func__, Tag::N_TIME_PER_BLOCK_TAG, pRoot, NTPB, OPTIONAL)){ // If value not correctly read
			NTPB = 1; // set to default
		}
		ptrBS = Sampler::Strategies::BlockSelector::createOptimised(*ptrModel.get(), seed, NTPB);
	} else {
		std::stringstream ss;
		ss << "BlockSelector '" << blockSel << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
}

void ReaderSamplerXML::readModifierStrategy(const std::string &modStrat) {

	if(modStrat == Tag::DEFAULT) {
		ptrMS = Sampler::Strategies::ModifierStrategy::createDefaultModifierStrategy(*ptrModel.get());
	} else {
		std::stringstream ss;
		ss << "ModifierStrategy '" << modStrat << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
}

void ReaderSamplerXML::readAcceptanceStrategy(const std::string &accStrat) {

	if(accStrat == Tag::DEFAULT) {
		ptrAS = Sampler::Strategies::AcceptanceStrategy::createDefaultAcceptanceStrategy(ptrMS, *ptrModel.get());
	} else {
		std::stringstream ss;
		ss << "AcceptanceStrategy '" << accStrat << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
}

void ReaderSamplerXML::readBlockStatConfig(){

	std::string blocStat;
	if(!XML::readElement(__func__, Tag::BLOCK_STAT_CFG_TAG, pRoot, blocStat, OPTIONAL)){
		ptrCfgFacto = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
		return;
	}

	if(blocStat == Tag::DEFAULT) {
		ptrCfgFacto = ParameterBlock::Config::ConfigFactory::createDefaultFactory();
	} else if (blocStat == Tag::FAST) {
		ptrCfgFacto = ParameterBlock::Config::ConfigFactory::createFastFactory();
	} else if (blocStat == Tag::EXTRAFAST) {
		ptrCfgFacto = ParameterBlock::Config::ConfigFactory::createExtraFastFactory();
	} else if (blocStat == Tag::SLOW) {
		ptrCfgFacto = ParameterBlock::Config::ConfigFactory::createSlowFactory();
	} else {
		std::stringstream ss;
		ss << "BlockStatConfig '" << blocStat << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
}

void ReaderSamplerXML::readOutput() {

	// If there is no output tag we use MCMC class default
	TiXmlElement *pOutput = pRoot->FirstChildElement(Tag::OUTPUT_TAG);
	if(!pOutput){
		warningMissingTag(__func__, Tag::OUTPUT_TAG);
		return;
	}

	// Output File
	std::string outputFile;
	if(XML::readElement(__func__, Tag::FILE_TAG, pOutput, outputFile, OPTIONAL)){
		ptrSampler->setOutputFile(outputFile);
	}

	// Output Frequency
	size_t outFrequency;
	if(XML::readElement(__func__, Tag::STDOUTPUT_FREQUENCY_TAG, pOutput, outFrequency, OPTIONAL)) {
		ptrSampler->setOutputFrequency(outFrequency);
	}

	// Verbosity
	size_t verbosity;
	if(XML::readElement(__func__, Tag::VERBOSE_TAG, pOutput, verbosity, OPTIONAL)){
		ptrSampler->setVerbose(verbosity);
	}

	// Thinning
	size_t nThin;
	if(XML::readElement(__func__, Tag::NTHINNING_TAG, pOutput, nThin, OPTIONAL)){
		ptrSampler->setThinning(nThin);
	}

	// Write binary
	std::string isBinary;
	if(XML::readElement(__func__, Tag::WRITE_BINARY_TAG, pOutput, isBinary, OPTIONAL)){
		if(isBinary == Tag::TRUE) {
			ptrSampler->setWriteBinary(true);
		} else if(isBinary == Tag::FALSE) {
			ptrSampler->setWriteBinary(false);
		} else {
			std::stringstream ss;
			ss << "writeBinary must be (true/false) : '" << isBinary << "' is not a correct value.";
			errorMessage(__func__,  ss.str());
		}
	}

	// Write float
	std::string isFloat;
	if(XML::readElement(__func__, Tag::WRITE_TYPE_TAG, pOutput, isFloat, OPTIONAL)){
		if(isFloat == Tag::FLOAT) {
			ptrSampler->setWriteFloat(true);
		} else if(isFloat == Tag::DOUBLE) {
			ptrSampler->setWriteFloat(false);
		} else {
			std::stringstream ss;
			ss << "writeType must be (float/double) : '" << isFloat << "' is not a correct value.";
			errorMessage(__func__,  ss.str());
		}
	}
}

} /* namespace XML */

