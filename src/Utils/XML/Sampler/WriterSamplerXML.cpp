//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterSamplerXML.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#include "WriterSamplerXML.h"

namespace XML {

WriterSamplerXML::WriterSamplerXML() {

}

WriterSamplerXML::~WriterSamplerXML() {
}


void WriterSamplerXML::writeSampler(TiXmlElement *aPRoot, Sampler::MCMC *sampler) {
	using namespace StatisticalModel::Likelihood;

	TiXmlElement *pXML = createTag(aPRoot, Tag::SAMPLER_TAG);

	if(Sampler::PFAMCMC *ptr = dynamic_cast<Sampler::PFAMCMC*>(sampler)) {
		writeStrategies(pXML, ptr);
		pXML->SetAttribute(Tag::NAME_ATT, Tag::PFAMCMC_NAME);

		ParameterBlock::Config::ConfigFactory *ptrCF = ptr->getAdaptiveBlock().back()->getCfgFac().get();
		if(ptrCF != NULL) {
			writeBlockStatConfig(pXML, ptrCF);
		}
	} else {
		XML::errorMessage(__func__, "Sampler not supported by WriterXML.");
	}

	writeOutput(pXML, sampler);
}

void WriterSamplerXML::writeStrategies(TiXmlElement *pXML, Sampler::PFAMCMC *sampler) {
	using namespace Sampler::Strategies;

	// Block Selector
	BlockSelector *ptrBS = sampler->getBlockSel().get();
	if(dynamic_cast<SingleCyclicBS*>(ptrBS)) {
		createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::CYCLYC_BS);
	} else if(dynamic_cast<SingleRevCyclicBS*>(ptrBS)) {
		createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::REVERSIBLE_BS);
	} else if(RandomBS *ptr = dynamic_cast<RandomBS*>(ptrBS)) {
		TiXmlElement *tmp = createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::RANDOM_BS);
		createTagAndText(tmp, Tag::SEED_TAG, ptr->getSeed());
	} else if(OptimisedBS *ptr = dynamic_cast<OptimisedBS*>(ptrBS)) {
		TiXmlElement *tmp = createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::OPTIMISED_BS);
		createTagAndText(tmp, Tag::SEED_TAG, ptr->getSeed());
	}  else if(RandomTreeInferenceBS *ptr = dynamic_cast<RandomTreeInferenceBS*>(ptrBS)) {
		TiXmlElement *tmp = createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::RANDOM_TREEINFERENCE_BS);
		createTagAndText(tmp, Tag::SEED_TAG, ptr->getSeed());
	} else if(OptimisedTIBS *ptr = dynamic_cast<OptimisedTIBS*>(ptrBS)) {
		TiXmlElement *tmp = createTagAndText(pXML, Tag::BLOCK_SELECTOR_TAG, Tag::OPTIMISED_TREEINFERENCE_BS);
		createTagAndText(tmp, Tag::SEED_TAG, ptr->getSeed());
	} else {
		XML::errorMessage(__func__, "BlockSelector not supported by WriterXML.");
	}

	// Modifier Strategy
	ModifierStrategy *ptrMS = sampler->getModStrat().get();
	if(dynamic_cast<DefaultMS*>(ptrMS)) {
		createTagAndText(pXML, Tag::MODIFIER_STRAT_TAG, Tag::DEFAULT);
	} else {
		XML::errorMessage(__func__, "ModifierStrategy not supported by WriterXML.");
	}

	// Acceptance strategy
	AcceptanceStrategy *ptrAcc = sampler->getAcceptStrat().get();
	if(dynamic_cast<BaseMHAS*>(ptrAcc)) {
		createTagAndText(pXML, Tag::ACCEPTANCE_STRAT_TAG, Tag::DEFAULT);
	} else {
		XML::errorMessage(__func__, "AcceptanceStrategy not supported by WriterXML.");
	}

}


void WriterSamplerXML::writeBlockStatConfig(TiXmlElement *pXML, ParameterBlock::Config::ConfigFactory *ptrCfgFactory) {
	using namespace ParameterBlock::Config;
	if(dynamic_cast<DefaultFactory*>(ptrCfgFactory)){
		createTagAndText(pXML, Tag::BLOCK_STAT_CFG_TAG, Tag::DEFAULT);
	} else if(dynamic_cast<FastFactory*>(ptrCfgFactory)){
		createTagAndText(pXML, Tag::BLOCK_STAT_CFG_TAG, Tag::FAST);
	} else if(dynamic_cast<ExtraFastFactory*>(ptrCfgFactory)){
		createTagAndText(pXML, Tag::BLOCK_STAT_CFG_TAG, Tag::EXTRAFAST);
	} else if(dynamic_cast<SlowFactory*>(ptrCfgFactory)){
			createTagAndText(pXML, Tag::BLOCK_STAT_CFG_TAG, Tag::SLOW);
	} else {
		createTagAndText(pXML, Tag::BLOCK_STAT_CFG_TAG, Tag::CUSTOM);
		XML::warningMessage(__func__, "ConfigFactory not supported by WriterXML.");
	}
}

void WriterSamplerXML::writeOutput(TiXmlElement *pXML, Sampler::MCMC *sampler) {
	TiXmlElement *pOutput = createTag(pXML, Tag::OUTPUT_TAG);

	createTagAndText(pOutput, Tag::FILE_TAG, sampler->getOutputFile());
	createTagAndText(pOutput, Tag::VERBOSE_TAG, sampler->getVerbose());
	createTagAndText(pOutput, Tag::STDOUTPUT_FREQUENCY_TAG, sampler->getOutputFrequency());
	createTagAndText(pOutput, Tag::NTHINNING_TAG, sampler->getThinning());

	if(sampler->isBinaryWriter()) {
		createTagAndText(pOutput, Tag::WRITE_BINARY_TAG, Tag::TRUE);
	} else {
		createTagAndText(pOutput, Tag::WRITE_BINARY_TAG, Tag::FALSE);
	}

	if(sampler->isFloatWriter()) {
		createTagAndText(pOutput, Tag::WRITE_TYPE_TAG, Tag::FLOAT);
	} else {
		createTagAndText(pOutput, Tag::WRITE_TYPE_TAG, Tag::DOUBLE);
	}
}

} /* namespace XML */
