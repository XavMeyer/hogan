//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterSamplerXML.h
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef WRITERSAMPLERXML_H_
#define WRITERSAMPLERXML_H_

#include "Utils/XML/HelpersXML.h"

#include "Sampler/IncSamplers.h"
#include "Sampler/Strategy/IncStrategy.h"
#include "ParameterBlock/BlockStats/Config/IncConfigFactory.h"

#include "../Tag.h"

namespace XML {

class WriterSamplerXML {
public:
	WriterSamplerXML();
	~WriterSamplerXML();

	void writeSampler(TiXmlElement *aPRoot, Sampler::MCMC *sampler);

private:

	void writeStrategies(TiXmlElement *pXML, Sampler::PFAMCMC *sampler);
	void writeModifierStrategy(TiXmlElement *pXML, Sampler::MCMC *sampler);
	void writeAcceptanceStrategy(TiXmlElement *pXML, Sampler::MCMC *sampler);
	void writeBlockStatConfig(TiXmlElement *pXML, ParameterBlock::Config::ConfigFactory *ptrCfgFactory);
	void writeOutput(TiXmlElement *pXML, Sampler::MCMC *sampler);

};

} /* namespace XML */

#endif /* WRITERSAMPLERXML_H_ */
