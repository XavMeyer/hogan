//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SamplerXML.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLER_H_
#define SAMPLER_H_

#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/TinyXML/tinyxml.h"

#include "../Tag.h"

#include "Model/Model.h"

#include "Sampler/IncSamplers.h"
#include "ParameterBlock/BlockStats/Config/Container/BlockStatCfg.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"

#include "Model/Likelihood/Helper/Helpers.h"
#include "Model/Likelihood/Helper/HelperInterface.h"

#include <boost/shared_ptr.hpp>

namespace XML {

class ReaderSamplerXML {
public:
	typedef boost::shared_ptr<ReaderSamplerXML> sharedPtr_t;
	typedef Sampler::MCMC::sharedPtr_t samplerPtr_t;

public:
	ReaderSamplerXML(const std::string &aName, TiXmlElement *aPRoot, StatisticalModel::Model::sharedPtr_t aPtrModel);
	~ReaderSamplerXML();

	samplerPtr_t getSamplerPtr();

private:
	static const size_t DEFAULT_SEED;

	const std::string name;
	TiXmlElement *pRoot;
	boost::shared_ptr< StatisticalModel::Model > ptrModel;

	Sampler::Strategies::BlockSelector::sharedPtr_t ptrBS;
	Sampler::Strategies::AcceptanceStrategy::sharedPtr_t ptrAS;
	Sampler::Strategies::ModifierStrategy::sharedPtr_t ptrMS;
	ParameterBlock::Config::ConfigFactory::sharedPtr_t ptrCfgFacto;

	samplerPtr_t ptrSampler;

	void readSampler();
	void readStrategies();

	void readBlockSelector(const std::string &name);
	void readModifierStrategy(const std::string &name);
	void readAcceptanceStrategy(const std::string &name);
	void readBlockStatConfig();
	void readOutput();

};

} /* namespace XML */

#endif /* SAMPLER_H_ */

