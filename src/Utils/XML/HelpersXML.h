//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Helpers.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef HELPERSXML_H_
#define HELPERSXML_H_

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>

#include "Utils/XML/TinyXML/tinyxml.h"
#include "Parallel/Parallel.h"

namespace XML {

enum presence_t {MANDATORY, OPTIONAL, NO_OUTPUT};


void warningMessage(const std::string &place, const std::string &warning);
void warningMissingAttribute(const std::string &place, const std::string &attribute);
void warningMissingTag(const std::string &place, const std::string &tag);

void errorMessage(const std::string &place, const std::string &error);
void errorMissingAttribute(const std::string &place, const std::string &attribute);
void errorMissingTag(const std::string &place, const std::string &tag);

bool readTextAtFirstTag(const std::string &tag, TiXmlElement **pElem, std::string &text);
bool readTextAtNexTag(const std::string &tag, TiXmlElement **pElem, std::string &text);

void printMessage(const std::string &place, const std::string &tag, presence_t presType);

template<typename T>
bool readElement(const std::string &place, const std::string &tag, TiXmlElement *pRoot, T& val, presence_t resType=MANDATORY);

template<typename T>
bool readElements(const std::string &place, const std::string &tag, TiXmlElement *pRoot, std::vector<T>& vals, presence_t resType=MANDATORY);

TiXmlElement* createTag(TiXmlElement *pRoot, const std::string &tag);

template<typename T>
TiXmlElement* createTagAndText(TiXmlElement *pRoot, const std::string &tag, T val) {
	std::stringstream ss; ss << val;
	TiXmlElement *pNewEl = createTag(pRoot, tag);
	TiXmlText *pText = new TiXmlText(ss.str());
	pNewEl->LinkEndChild(pText);
	return pNewEl;
}

template<typename T>
TiXmlElement* createTagAndAttribute(TiXmlElement *pRoot, const std::string &tag, const std::string &attribute, T& val) {
	std::stringstream ss; ss << val;
	TiXmlElement *pNewEl = createTag(pRoot, tag);
	pNewEl->SetAttribute(attribute, ss.str());
	return pNewEl;
}


} /* namespace XML */

#endif /* HELPERSXML_H_ */
