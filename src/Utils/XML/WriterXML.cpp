//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterXML.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */

#include "WriterXML.h"

namespace XML {

WriterXML::WriterXML(const std::string &fileName) : doc(fileName) {
	initXML();
}

WriterXML::~WriterXML() {
}

void WriterXML::writeXML(size_t nIteration, StatisticalModel::Model *aModel, Sampler::MCMC *aSampler) {
	writeBase(nIteration);
	writeLikelihood(aModel);
	writeSampler(aSampler);

	if(!doc.SaveFile()) {
		XML::warningMessage(__func__, "Couldn't write XML file (check the path).");
	}
}

void WriterXML::initXML() {
	pRoot = new TiXmlElement(Tag::ROOT_TAG);
	doc.LinkEndChild(pRoot);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void WriterXML::writeBase(size_t nIteration) {
	size_t nProp = Parallel::mcmcMgr().getNProposal();
	size_t nChain = Parallel::mcmcMgr().getNChain();

	// TOPO
	TiXmlElement *pTopo = createTag(pRoot, Tag::TOPOLOGY_TAG);
	createTagAndText(pTopo, Tag::NPROPOSAL_TAG, nProp);
	createTagAndText(pTopo, Tag::NCHAIN_TAG, nChain);

	// Seed
	size_t seed = Parallel::mpiMgr().getPRNG()->getSeed();
	createTagAndText(pRoot, Tag::SEED_TAG, seed);
	// Niteration
	createTagAndText(pRoot, Tag::NITERATION_TAG, nIteration);
}

void WriterXML::writeLikelihood(StatisticalModel::Model *aModel) {
	WriterLikelihoodXML writerLik;
	writerLik.writeLikelihood(pRoot, aModel);
}

void WriterXML::writeSampler(Sampler::MCMC *aSampler) {
	WriterSamplerXML writerSampler;
	writerSampler.writeSampler(pRoot, aSampler);
}





} /* namespace XML */
