//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterLikelihoodXML.h
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef WRITERLIKELIHOODXML_H_
#define WRITERLIKELIHOODXML_H_

#include "Utils/XML/HelpersXML.h"
#include "../Tag.h"
#include "Model/Model.h"

#include "Model/Likelihood/Likelihoods.h"

namespace XML {

class WriterLikelihoodXML {
public:
	WriterLikelihoodXML();
	~WriterLikelihoodXML();

	void writeLikelihood(TiXmlElement *aPRoot, StatisticalModel::Model *aModel);

private:

	void writeIndepNormals(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::LogNormalNDLH *ptr,
			StatisticalModel::Model *aModel);

	void writeCorrelNormals(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::CorrelatedNormals *ptr,
			StatisticalModel::Model *aModel);

	void writeBDRateDAG(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::BDRate::Standard::StdLik *ptr,
			StatisticalModel::Model *aModel);

	void writeBDRAlphaDAG(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::BDRate::Alpha::AlphaLik *ptr,
			StatisticalModel::Model *aModel);

	void writeCodonModels(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::CodonModels::Base *ptr,
			StatisticalModel::Model *aModel);

	void writeTreeInference(TiXmlElement *aPRoot,
			StatisticalModel::Likelihood::TreeInference::Base *ptr,
			StatisticalModel::Model *aModel);


	std::string adaptiveTypeToTag(ParameterBlock::Block::adaptiveType_t adaptiveType) const;

};

} /* namespace XML */

#endif /* WRITERLIKELIHOODXML_H_ */
