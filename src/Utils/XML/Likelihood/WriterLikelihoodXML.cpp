//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WriterLikelihoodXML.cpp
 *
 * @date May 12, 2015
 * @author meyerx
 * @brief
 */
#include "WriterLikelihoodXML.h"

namespace XML {

WriterLikelihoodXML::WriterLikelihoodXML() {
}

WriterLikelihoodXML::~WriterLikelihoodXML() {
}

void WriterLikelihoodXML::writeLikelihood(TiXmlElement *aPRoot, StatisticalModel::Model *aModel) {
	using namespace StatisticalModel::Likelihood;
	LikelihoodInterface *lik = aModel->getLikelihood().get();

	if(LogNormalNDLH *ptr = dynamic_cast<LogNormalNDLH*>(lik)) {
		writeIndepNormals(aPRoot, ptr, aModel);
	} else if(CorrelatedNormals *ptr = dynamic_cast<CorrelatedNormals*>(lik)) {
		writeCorrelNormals(aPRoot, ptr, aModel);
	} else if(BDRate::Standard::StdLik *ptr = dynamic_cast<BDRate::Standard::StdLik *>(lik)) {
		writeBDRateDAG(aPRoot, ptr, aModel);
	} else if(BDRate::Alpha::AlphaLik *ptr = dynamic_cast<BDRate::Alpha::AlphaLik *>(lik)) {
		writeBDRAlphaDAG(aPRoot, ptr, aModel);
	} else if(CodonModels::Base *ptr = dynamic_cast<CodonModels::Base *>(lik)) {
		writeCodonModels(aPRoot, ptr, aModel);
	} else if(TreeInference::Base *ptr = dynamic_cast<TreeInference::Base *>(lik)) {
		writeTreeInference(aPRoot, ptr, aModel);
	} else {
		XML::errorMessage(__func__, "Likelihood not supported by WriterXML.");
	}
}

void WriterLikelihoodXML::writeIndepNormals(TiXmlElement *aPRoot, StatisticalModel::Likelihood::LogNormalNDLH *ptr, StatisticalModel::Model *aModel) {
	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::INDEPENDENT_NORMAL_NAME);

	createTagAndText(pLik, Tag::NDIM_TAG, ptr->getNDim());
	createTagAndText(pLik, Tag::NDATA_TAG, ptr->getNData());

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(0)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}

void WriterLikelihoodXML::writeCorrelNormals(TiXmlElement *aPRoot, StatisticalModel::Likelihood::CorrelatedNormals *ptr, StatisticalModel::Model *aModel) {
	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::CORRELATED_NORMAL_NAME);

	createTagAndText(pLik, Tag::FILE_TAG, ptr->getFName());
	createTagAndText(pLik, Tag::NDATA_TAG, ptr->getNData());

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(0)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}

void WriterLikelihoodXML::writeBDRateDAG(TiXmlElement *aPRoot, StatisticalModel::Likelihood::BDRate::Standard::StdLik *ptr,
		StatisticalModel::Model *aModel){
	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::BDRATE_DAG_NAME);

	createTagAndText(pLik, Tag::FILE_TAG, ptr->getFName());
	createTagAndText(pLik, Tag::NTHREAD_TAG, ptr->getNThread());

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(0)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}

void WriterLikelihoodXML::writeBDRAlphaDAG(TiXmlElement *aPRoot, StatisticalModel::Likelihood::BDRate::Alpha::AlphaLik *ptr,
		StatisticalModel::Model *aModel){
	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::BDRATE_ALPHA_DAG_NAME);

	createTagAndText(pLik, Tag::FILE_TAG, ptr->getFName());
	createTagAndText(pLik, Tag::NTHREAD_TAG, ptr->getNThread());
	createTagAndText(pLik, Tag::NQUANTILE_TAG, ptr->getNAlpha());

	std::string state = ptr->isHPP() ? Tag::TRUE : Tag::FALSE;
	createTagAndText(pLik, Tag::ISPLANT_TAG, state);

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(0)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}

void WriterLikelihoodXML::writeCodonModels(TiXmlElement *aPRoot,
		StatisticalModel::Likelihood::CodonModels::Base *ptr,
		StatisticalModel::Model *aModel) {
	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::CODONS_MODEL_NAME);

	std::string state = ptr->isUsingCompression() ? Tag::TRUE : Tag::FALSE;
	createTagAndText(pLik, Tag::USE_COMPRESSION_TAG, state);

	createTagAndText(pLik, Tag::NTHREAD_TAG, ptr->getNThread());

	createTagAndText(pLik, Tag::MODEL_TYPE_TAG, static_cast<size_t>(ptr->getModelType()));
	createTagAndText(pLik, Tag::N_CLASS_TAG, ptr->getNClass());
	createTagAndText(pLik, Tag::SCALING_TYPE_TAG, static_cast<size_t>(ptr->getScalingType()));

	createTagAndText(pLik, Tag::NUCL_MODEL_TAG, static_cast<size_t>(ptr->getNucleotideModel()));
	createTagAndText(pLik, Tag::CODON_FREQ_TYPE_TAG, static_cast<size_t>(ptr->getCodonFrequencyType()));

	createTagAndText(pLik, Tag::ALIGN_FILE_TAG, ptr->getAlignFile());
	createTagAndText(pLik, Tag::TREE_FILE_TAG, ptr->getTreeFile());

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(0)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}


void WriterLikelihoodXML::writeTreeInference(TiXmlElement *aPRoot,
		StatisticalModel::Likelihood::TreeInference::Base *ptr,
		StatisticalModel::Model *aModel) {

	namespace DL = MolecularEvolution::DataLoader;
	namespace TR = MolecularEvolution::TreeReconstruction;

	TiXmlElement *pLik = createTagAndAttribute(aPRoot, Tag::LIKELIHOOD_TAG, Tag::NAME_ATT, Tag::TREE_INFERENCE_NAME);

	createTagAndText(pLik, Tag::NTHREAD_TAG, ptr->getNThread());

	createTagAndText(pLik, Tag::MODEL_TYPE_TAG, static_cast<size_t>(ptr->getModelType()));
	createTagAndText(pLik, Tag::SCALING_TYPE_TAG, static_cast<size_t>(ptr->getScalingType()));

	createTagAndText(pLik, Tag::NUCL_MODEL_TAG, static_cast<size_t>(ptr->getNucleotideModel()));
	createTagAndText(pLik, Tag::CODON_FREQ_TYPE_TAG, static_cast<size_t>(ptr->getCodonFrequencyType()));

	createTagAndText(pLik, Tag::ALIGN_FILE_TAG, ptr->getAlignFile());

	createTagAndText(pLik, Tag::TREE_SAMPLING_TAG, static_cast<size_t>(ptr->getTreeUpdateStrategy()));

	createTagAndText(pLik, Tag::PARAMETERS_TAG, Tag::DEFAULT);

	TiXmlElement *pBlock = createTagAndAttribute(pLik, Tag::BLOCKS_TAG, Tag::NAME_ATT, Tag::DEFAULT);
	createTagAndText(pBlock, Tag::BLOCK_SIZE_TAG, aModel->getBlocks().getBlock(0)->size());

	size_t idxLast = aModel->getBlocks().size()-1;
	std::string adaptiveTypeTag = adaptiveTypeToTag(aModel->getBlocks().getBlock(idxLast)->getAdaptiveType());
	createTagAndText(pBlock, Tag::ADAPTIVE_TYPE_TAG, adaptiveTypeTag);
}

std::string WriterLikelihoodXML::adaptiveTypeToTag(ParameterBlock::Block::adaptiveType_t adaptiveType) const {
	using ParameterBlock::Block;
	if(adaptiveType == Block::NOT_ADAPTIVE) {
		return Tag::NOT_ADAPTIVE_TAG;
	} else if(adaptiveType == Block::SINGLE_DOUBLE_VARIABLE) {
		return Tag::DEFAULT_ADAPTIVE_TAG;
	} else if(adaptiveType == Block::MIXED_DOUBLE_VARIABLES) {
		return Tag::MIXED_ADAPTIVE_TAG;
	} else if(adaptiveType == Block::PCA_DOUBLE_VARIABLES) {
		return Tag::PCA_ADAPTIVE_TAG;
	} else {
		std::stringstream ss;
		ss << "Adaptive type is unknown : static_cast<int>(adaptiveType) = " << static_cast<int>(adaptiveType);
		XML::errorMessage(__func__, ss.str());
	}
	return "";
}



} /* namespace XML */
