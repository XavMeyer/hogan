//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.cpp
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderLikelihoodXML.h"

namespace XML {

ReaderLikelihoodXML::ReaderLikelihoodXML(const std::string &aName, TiXmlElement *aPRoot) :
	name(aName), pRoot(aPRoot) {
	readLikelihood();
}

ReaderLikelihoodXML::~ReaderLikelihoodXML() {
}

ReaderLikelihoodXML::likPtr_t ReaderLikelihoodXML::getLikelihoodPtr() {
	return ptrLik;
}

ReaderLikelihoodXML::modelPtr_t ReaderLikelihoodXML::getModelPtr() {
	return ptrModel;
}

void ReaderLikelihoodXML::readLikelihood() {
	if(name == Tag::INDEPENDENT_NORMAL_NAME) {
		readIndepNormals();
	} else if (name == Tag::CORRELATED_NORMAL_NAME) {
		readCorrelNormals();
	} else if (name == Tag::BDRATE_DAG_NAME) {
		readBDRateDAG();
	} else if (name == Tag::BDRATE_ALPHA_DAG_NAME) {
		readBDRAlphaDAG();
	} else if (name == Tag::CODONS_MODEL_NAME) {
		readCodonModels();
	} else if (name == Tag::TREE_INFERENCE_NAME) {
		readTreeInference();
	} else {
		std::stringstream ss;
		ss << "Likelihood '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

}

void ReaderLikelihoodXML::readIndepNormals() {
	// NDIM
	size_t nDim = 0;
	XML::readElement(__func__, Tag::NDIM_TAG, pRoot, nDim, MANDATORY);

	// NDATA
	size_t nData = 0;
	XML::readElement(__func__, Tag::NDATA_TAG, pRoot, nData, MANDATORY);

	// Likelihood
	ptrLik = LikelihoodFactory::createLogNormalNDim(nDim, nData);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));

	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);

	TiXmlElement *pBlock = pRoot->FirstChildElement(Tag::BLOCKS_TAG);
	const std::string *pAttrib = pBlock->Attribute(Tag::NAME_ATT);

	std::vector<size_t> blockSize;
	XML::readElements(__func__, Tag::BLOCK_SIZE_TAG, pBlock, blockSize, MANDATORY);

	std::string adaptiveTypeTag;
	XML::readElement(__func__, Tag::ADAPTIVE_TYPE_TAG, pBlock, adaptiveTypeTag, MANDATORY);
	ParameterBlock::Block::adaptiveType_t adaptiveType = tagToAdaptiveType(adaptiveTypeTag);

	if(pAttrib != NULL) {
		if(*pAttrib == Tag::DEFAULT) {
			hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::OPTI) {
			(static_cast<Helper::IndepLogNormalHlp*>(hlp.get()))->defineOptiBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::GOOD) {
			(static_cast<Helper::IndepLogNormalHlp*>(hlp.get()))->defineGoodBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else {
			std::stringstream ss;
			ss << "Parameters '" << name << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
	}
}

void ReaderLikelihoodXML::readCorrelNormals() {

	std::string fName;
	XML::readElement(__func__, Tag::FILE_TAG, pRoot, fName, MANDATORY);

	size_t nData = 0;
	XML::readElement(__func__, Tag::NDATA_TAG, pRoot, nData, MANDATORY);

	// Likelihood
	ptrLik = LikelihoodFactory::createCorrelatedNormals(nData, fName);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);

	TiXmlElement *pBlock = pRoot->FirstChildElement(Tag::BLOCKS_TAG);
	const std::string *pAttrib = pBlock->Attribute(Tag::NAME_ATT);

	std::vector<size_t> blockSize;
	XML::readElements(__func__, Tag::BLOCK_SIZE_TAG, pBlock, blockSize, MANDATORY);

	std::string adaptiveTypeTag;
	XML::readElement(__func__, Tag::ADAPTIVE_TYPE_TAG, pBlock, adaptiveTypeTag, MANDATORY);
	ParameterBlock::Block::adaptiveType_t adaptiveType = tagToAdaptiveType(adaptiveTypeTag);

	if(pAttrib != NULL) {
		if(*pAttrib == Tag::DEFAULT) {
			hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::OPTI) {
				(static_cast< Helper::CorrelNormalHlp* >(hlp.get()))->defineOptiBlocks(adaptiveType,blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else {
			std::stringstream ss;
			ss << "Parameters '" << name << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
	}
}

void ReaderLikelihoodXML::readBDRateDAG() {
	std::string fName;
	XML::readElement(__func__, Tag::FILE_TAG, pRoot, fName, MANDATORY);

	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	// Likelihood
	ptrLik = LikelihoodFactory::createBDRateDAG(nThread, fName, false);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);
	readDefaultBlocksType(hlp);

}

void ReaderLikelihoodXML::readBDRAlphaDAG() {

	size_t nQuantile=0;
	XML::readElement(__func__, Tag::NQUANTILE_TAG, pRoot, nQuantile, MANDATORY);

	bool isPlant = true;
	std::string strIsPlant;
	XML::readElement(__func__, Tag::ISPLANT_TAG, pRoot, strIsPlant, MANDATORY);
	if(strIsPlant == Tag::TRUE) {
		isPlant = true;
	} else if(strIsPlant == Tag::FALSE) {
		isPlant = false;
	} else {
		std::stringstream ss;
		ss << "isPlant must be (true/false) : '" << strIsPlant << "' is not a correct value.";
		XML::errorMessage(__func__, ss.str());
	}

	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	std::string fName;
	XML::readElement(__func__, Tag::FILE_TAG, pRoot, fName, MANDATORY);

	// Likelihood
	ptrLik = LikelihoodFactory::createBDRAlphaDAG(nThread, nQuantile, fName, isPlant);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);
	readDefaultBlocksType(hlp);
}

void ReaderLikelihoodXML::readCodonModels() {

	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	size_t nClass;
	XML::readElement(__func__, Tag::N_CLASS_TAG, pRoot, nClass, MANDATORY);

	size_t iModelType = 0;
	XML::readElement(__func__, Tag::MODEL_TYPE_TAG, pRoot, iModelType, MANDATORY);
	StatisticalModel::Likelihood::CodonModels::modelType_t modelType;
	modelType = static_cast<StatisticalModel::Likelihood::CodonModels::modelType_t>(iModelType);

	size_t iScalingType = 0;
	XML::readElement(__func__, Tag::SCALING_TYPE_TAG, pRoot, iScalingType, MANDATORY);
	StatisticalModel::Likelihood::CodonModels::scalingType_t scalingType;
	scalingType = static_cast<StatisticalModel::Likelihood::CodonModels::scalingType_t>(iScalingType);

	nuclModel_t nuclModel = readNucleotideModel();
	codonFreq_t cFreqType = readCodonFrequencyType();

	std::string alignName, treeName;
	XML::readElement(__func__, Tag::ALIGN_FILE_TAG, pRoot, alignName, MANDATORY);
	XML::readElement(__func__, Tag::TREE_FILE_TAG, pRoot, treeName, MANDATORY);;

	bool isUsingCompression = readIsCompressed();

	// Likelihood
	switch (modelType) {
		case StatisticalModel::Likelihood::CodonModels::M0 :
			ptrLik = LikelihoodFactory::createCodonM0(scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		case StatisticalModel::Likelihood::CodonModels::M1 :
			ptrLik = LikelihoodFactory::createCodonM1(scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		case StatisticalModel::Likelihood::CodonModels::M1a :
			ptrLik = LikelihoodFactory::createCodonM1a(scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		case StatisticalModel::Likelihood::CodonModels::M2 :
			ptrLik = LikelihoodFactory::createCodonM2(scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		case StatisticalModel::Likelihood::CodonModels::M2a :
			ptrLik = LikelihoodFactory::createCodonM2a(scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		case StatisticalModel::Likelihood::CodonModels::OTHER :
			ptrLik = LikelihoodFactory::createCodonModel(nClass, scalingType, isUsingCompression, nThread, nuclModel, cFreqType, alignName, treeName);
			break;
		default:
			std::stringstream ss;
			ss << "Codon Model '" << modelType << "' does not exists";
			XML::errorMessage(__func__, ss.str());
			break;
	}

	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);

	TiXmlElement *pBlock = pRoot->FirstChildElement(Tag::BLOCKS_TAG);
	const std::string *pAttrib = pBlock->Attribute(Tag::NAME_ATT);

	std::vector<size_t> blockSize;
	XML::readElements(__func__, Tag::BLOCK_SIZE_TAG, pBlock, blockSize, MANDATORY);

	std::string adaptiveTypeTag;
	XML::readElement(__func__, Tag::ADAPTIVE_TYPE_TAG, pBlock, adaptiveTypeTag, MANDATORY);
	ParameterBlock::Block::adaptiveType_t adaptiveType = tagToAdaptiveType(adaptiveTypeTag);

	if(pAttrib != NULL) {
		if(*pAttrib == Tag::DEFAULT) {
			hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::SINGLE) {
			CodonModelsHlp *tmpHlp = static_cast< CodonModelsHlp *>(hlp.get());
			tmpHlp->defineOneBlock(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::OPTIMIZED) {
			hlp->defineOptiBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::RANDOM) {
			hlp->defineRandomBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else {
			std::stringstream ss;
			ss << "Block type '" << *pAttrib << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
	}
}

void ReaderLikelihoodXML::readTreeInference() {

	namespace DL = MolecularEvolution::DataLoader;
	namespace TR = MolecularEvolution::TreeReconstruction;

	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	size_t iModelType = 5;
	//XML::readElement(__func__, Tag::MODEL_TYPE_TAG, pRoot, iModelType, MANDATORY);
	StatisticalModel::Likelihood::TreeInference::modelType_t modelType;
	modelType = static_cast<StatisticalModel::Likelihood::TreeInference::modelType_t>(iModelType);

	size_t iScalingType = 1;
	//XML::readElement(__func__, Tag::SCALING_TYPE_TAG, pRoot, iScalingType, MANDATORY);
	StatisticalModel::Likelihood::TreeInference::scalingType_t scalingType;
	scalingType = static_cast<StatisticalModel::Likelihood::TreeInference::scalingType_t>(iScalingType);

	nuclModel_t nuclModel = static_cast<MolecularEvolution::MatrixUtils::nuclModel_t>(1);
	codonFreq_t cFreqType = static_cast<MolecularEvolution::DataLoader::CodonFrequencies::codonFrequencies_t>(3);

	std::string alignName, treeName;
	XML::readElement(__func__, Tag::ALIGN_FILE_TAG, pRoot, alignName, MANDATORY);
	bool isInitTreeGiven = XML::readElement(__func__, Tag::TREE_FILE_TAG, pRoot, treeName, OPTIONAL);

	DL::NewickParser *np = NULL;
	if(isInitTreeGiven) {
		np =new DL::NewickParser(treeName);
	}

	size_t iTreeStrat = 0;
	XML::readElement(__func__, Tag::TREE_SAMPLING_TAG, pRoot, iTreeStrat, MANDATORY);
	StatisticalModel::Likelihood::TreeInference::Base::treeUpdateStrategy_t treeStrat;
	treeStrat = static_cast<StatisticalModel::Likelihood::TreeInference::Base::treeUpdateStrategy_t>(iTreeStrat);

	// Likelihood
	ptrLik = LikelihoodFactory::createTreeInference(scalingType, modelType, nThread, nuclModel, cFreqType,
			np, alignName, treeStrat);

	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);
	readDefaultBlocksType(hlp);

	if(np != NULL) {
		delete np;
	}
}

/* Read helpers */
void ReaderLikelihoodXML::readDefaultParametersType(const Helper::HelperInterface::sharedPtr_t &hlp) {
	std::string paramType;
	if(XML::readElement(__func__, Tag::PARAMETERS_TAG, pRoot, paramType, OPTIONAL)) {
		if(paramType == Tag::DEFAULT) {
			hlp->defineParameters(ptrModel->getParams());
		} else {
			std::stringstream ss;
			ss << "Parameters '" << paramType << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineParameters(ptrModel->getParams());
	}

//	for(size_t i=0; i<ptrModel->getParams().size(); ++i) {
//		std::cout << ptrModel->getParams().getName(i) << " :: ";
//		std::cout << ptrModel->getParams().getBoundMin(i) << " --> ";
//		std::cout << ptrModel->getParams().getBoundMax(i) << std::endl;
//	}

}

void ReaderLikelihoodXML::readDefaultBlocksType(const Helper::HelperInterface::sharedPtr_t &hlp) {

	TiXmlElement *pBlock = pRoot->FirstChildElement(Tag::BLOCKS_TAG);
	const std::string *pAttrib = pBlock->Attribute(Tag::NAME_ATT);

	std::vector<size_t> blockSize;
	XML::readElements(__func__, Tag::BLOCK_SIZE_TAG, pBlock, blockSize, MANDATORY);

	std::string adaptiveTypeTag;
	XML::readElement(__func__, Tag::ADAPTIVE_TYPE_TAG, pBlock, adaptiveTypeTag, MANDATORY);
	ParameterBlock::Block::adaptiveType_t adaptiveType = tagToAdaptiveType(adaptiveTypeTag);

	if(pAttrib != NULL) {
		if(*pAttrib == Tag::DEFAULT) {
			hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::OPTIMIZED) {
			hlp->defineOptiBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else if(*pAttrib == Tag::RANDOM) {
			hlp->defineRandomBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
		} else {
			std::stringstream ss;
			ss << "Block type '" << *pAttrib << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineBlocks(adaptiveType, blockSize, ptrModel->getParams(), ptrModel->getBlocks());
	}
}

bool ReaderLikelihoodXML::readIsH1() {
	bool isH1 = true;
	std::string hyp;
	XML::readElement(__func__, Tag::HYPOTHESIS_TAG, pRoot, hyp, MANDATORY);
	if(hyp == Tag::H0) {
		isH1 = false;
	} else if(hyp == Tag::H1) {
		isH1 = true;
	} else {
		std::stringstream ss;
		ss << "hypothesis must be (H0/H1) : '" << hyp << "' is not a correct value.";
		XML::errorMessage(__func__, ss.str());
	}
	return isH1;
}

bool ReaderLikelihoodXML::readIsCompressed() {
	bool isUsingCompression = true;
	std::string strUseCompression;
	if(XML::readElement(__func__, Tag::USE_COMPRESSION_TAG, pRoot, strUseCompression, OPTIONAL)) {
		if(strUseCompression == Tag::TRUE) {
			isUsingCompression = true;
		} else if(strUseCompression == Tag::FALSE) {
			isUsingCompression = false;
		}
	}
	return isUsingCompression;
}

bool ReaderLikelihoodXML::readBool(const std::string &tag) {
	bool value = true;
	std::string str;
	if(XML::readElement(__func__, tag, pRoot, str, MANDATORY)) {
		if(str == Tag::TRUE) {
			value = true;
		} else if(str == Tag::FALSE) {
			value = false;
		}
	}
	return value;
}




ReaderLikelihoodXML::nuclModel_t ReaderLikelihoodXML::readNucleotideModel() {
	size_t iNuclModel = 0;
	XML::readElement(__func__, Tag::NUCL_MODEL_TAG, pRoot, iNuclModel, MANDATORY);
	MolecularEvolution::MatrixUtils::nuclModel_t nuclModel;
	nuclModel = static_cast<MolecularEvolution::MatrixUtils::nuclModel_t>(iNuclModel);
	return nuclModel;
}

ReaderLikelihoodXML::codonFreq_t ReaderLikelihoodXML::readCodonFrequencyType() {
	size_t iCFreqType = 0;
	XML::readElement(__func__, Tag::CODON_FREQ_TYPE_TAG, pRoot, iCFreqType, MANDATORY);
	MolecularEvolution::DataLoader::CodonFrequencies::codonFrequencies_t cFreqType;
	cFreqType = static_cast<MolecularEvolution::DataLoader::CodonFrequencies::codonFrequencies_t>(iCFreqType);
	return cFreqType;
}

ParameterBlock::Block::adaptiveType_t ReaderLikelihoodXML::tagToAdaptiveType(const std::string &text) const {
	using ParameterBlock::Block;
	if(text == Tag::NOT_ADAPTIVE_TAG) {
		return Block::NOT_ADAPTIVE;
	} else if(text == Tag::DEFAULT_ADAPTIVE_TAG) {
		return Block::SINGLE_DOUBLE_VARIABLE;
	} else if(text == Tag::MIXED_ADAPTIVE_TAG) {
		return Block::MIXED_DOUBLE_VARIABLES;
	} else if(text == Tag::PCA_ADAPTIVE_TAG) {
		return Block::PCA_DOUBLE_VARIABLES;
	} else {
		std::stringstream ss;
		ss << "Adaptive type is unknown : " << text;
		XML::errorMessage(__func__, ss.str());
	}
	return Block::NOT_ADAPTIVE;
}


} /* namespace XML */
