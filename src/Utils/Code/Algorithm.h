//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Algorithm.h
 *
 * @date Nov 17, 2015
 * @author meyerx
 * @brief
 */
#ifndef ALGORITHM_H_
#define ALGORITHM_H_

#include <vector>
#include <map>
#include <algorithm>

#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

namespace Utils {
namespace Algorithm {

template <typename T>
std::vector<size_t> getOrderedIndices(std::vector<T> const& values, bool ascending) {
    using namespace boost::phoenix;
    using namespace boost::phoenix::arg_names;

    std::vector<size_t> indices(values.size());
    int i = 0;
    std::transform(values.begin(), values.end(), indices.begin(), ref(i)++);
    if(ascending) {
    	std::sort(indices.begin(), indices.end(), ref(values)[arg1] < ref(values)[arg2]);
    } else {
    	std::sort(indices.begin(), indices.end(), ref(values)[arg1] > ref(values)[arg2]);
    }
    return indices;
}

template <typename T>
void orderVector(const std::vector<size_t> &orderedId, std::vector<T>& values) {
	assert(orderedId.size() == values.size());
	std::vector<T> tmpVal = values;
	values.clear();
	for(size_t iV=0; iV<tmpVal.size(); ++iV) {
		values.push_back(tmpVal[orderedId[iV]]);
	}
}

template <typename T>
std::map<size_t, size_t> getOrderedIndicesMapping(std::vector<T> const& values, bool ascending) {

	std::map<size_t, size_t> indicesMap;
	std::vector<size_t> orderedIndices = getOrderedIndices(values, ascending);

	for(size_t i=0; i<orderedIndices.size(); ++i) {
		indicesMap[orderedIndices[i]] = i;
	}

	return indicesMap;
}


} /* namespace Algorithm */
} /* namespace Utils */

#endif /* ALGORITHM_H_ */
