//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CheckpointFile.cpp
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#include "CheckpointFile.h"

namespace Utils {
namespace Checkpoint {

File::File(const std::string &aFName, std::streampos aOffset, bool aIsBinary) :
		isBinary(aIsBinary), fName(aFName) {

	if(aOffset == 0) { // first call
		if(!isBinary) {
			oFile.open(fName.c_str(), std::ios::out);
		} else {
			oFile.open(fName.c_str(), std::ios::out | std::ios::binary);
		}
	} else {
		recoverFile(aOffset);
	}
}

File::~File() {
}

size_t File::getSize() {

	oFile.flush();

	struct stat64 filestatus;
	stat64( fName.c_str(), &filestatus );

	return filestatus.st_size ;
}

std::ofstream& File::getOStream() {
	return oFile;
}

void File::recoverFile(std::streampos aOffset) {

	// truncate the file to the last checkpoint state
	int res = truncate64(fName.c_str(), aOffset);
	assert(res == 0);

	// Open the file (at the end)
	if(!isBinary) {
		oFile.open(fName.c_str(), std::ios::app);
	} else {
		oFile.open(fName.c_str(), std::ios::app | std::ios::binary);
	}
}

} /* namespace Checkpoint */
} /* namespace Utils */
