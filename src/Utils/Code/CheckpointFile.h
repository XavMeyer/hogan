//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CheckpointFile.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#ifndef CHECKPOINTFILE_H_
#define CHECKPOINTFILE_H_

#include <string>
#include <fstream>
#include <iostream>

#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <boost/shared_ptr.hpp>

namespace Utils {
namespace Checkpoint {

class File {
public:
	typedef boost::shared_ptr< File > sharedPtr_t;

public:
	File(const std::string &aFName, std::streampos aOffset, bool aIsBinary=false);
	~File();

	size_t getSize();
	std::ofstream& getOStream();

private:

	const bool isBinary;
	std::string fName;
	std::ofstream oFile;

	void recoverFile(std::streampos aOffset);


};

} /* namespace Checkpoint */
} /* namespace Utils */

#endif /* CHECKPOINTFILE_H_ */
