//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateRNG.h
 *
 * @date Sep 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef STATERNG_H_
#define STATERNG_H_

#include <boost/math/distributions.hpp>
#include <boost/shared_ptr.hpp>

#include "Utils/Code/SerializationSupport.h"

class Sitmo11RNG;

class StateRNG {
public:
	typedef boost::shared_ptr< StateRNG > sharedPtr_t;

public:
	StateRNG();
	~StateRNG();


private:

	unsigned long int seed;
	short int o_cnt;
	uint64_t s0, s1, s2, s3;
	uint64_t k0, k1, k2, k3;

	friend class Sitmo11RNG;
};

#endif /* STATERNG_H_ */
