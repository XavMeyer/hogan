//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sitmo11RNG.cpp
 *
 *  Created on: 1 oct. 2013
 *      Author: meyerx
 */

#include "Sitmo11RNG.h"

Sitmo11RNG::Sitmo11RNG() {
	//seed = DEFAULT_SEED;
	eng = new sitmo::prng_engine();
	//eng->seed(seed);
	setSeed(DEFAULT_SEED);
}

Sitmo11RNG::Sitmo11RNG(long unsigned int aSeed) {
	eng = new sitmo::prng_engine();
	//eng->seed(seed);
	setSeed(aSeed);
}

Sitmo11RNG::~Sitmo11RNG() {
	delete eng;
}

int Sitmo11RNG::genUniformInt() const {
	uniform_int_distribution<int> distribution;
	return distribution(*eng);
}

int Sitmo11RNG::genUniformInt(const int aMin, const int aMax) const {
	uniform_int_distribution<int> distribution(aMin, aMax);
	if(aMin == aMax) return aMin;
	return distribution(*eng);
}

double Sitmo11RNG::genUniformDbl() const {
	uniform_real_distribution<double> distribution;
	return distribution(*eng);
}

double Sitmo11RNG::genUniformDbl(const double aMin, const double aMax) const {
	uniform_real_distribution<double> distribution(aMin, aMax);
	if(aMin == aMax) return aMin;
	return distribution(*eng);
}

double Sitmo11RNG::genGaussian(const double sigma) const {
	normal_distribution<double> distribution(0, sigma);
	return distribution(*eng);
}

double Sitmo11RNG::genLogNormal(const double mu, const double sigma) const {
	lognormal_distribution<> distribution(mu, sigma);
	return distribution(*eng);
}

double Sitmo11RNG::genBinomial(const double p, const unsigned int n) const {
	binomial_distribution<> distribution(n, p);
	return distribution(*eng);
}

double Sitmo11RNG::genBeta(const double alpha, const double beta) const {
	boost::random::beta_distribution<> distribution(alpha, beta);
	return distribution(*eng);
}

double Sitmo11RNG::genGamma(const double shape, const double scale) const {
	gamma_distribution<> distribution(shape, scale);
	return distribution(*eng);
}

double Sitmo11RNG::genExponential(const double lambda) const {
	exponential_distribution<> distribution(lambda);
	return distribution(*eng);
}


std::vector<double> Sitmo11RNG::genStandardNormal(const size_t n) const {
	vector<double> buffer(n);

	for(uint i=0; i<n; ++i){
		buffer[i] = genGaussian(1.0);
	}

    return buffer;
}

std::vector<double> Sitmo11RNG::genMultivariateGaussian(const size_t n, const double *cholesky) const {
	multivariate_normal_distribution<> distribution(n, cholesky);
	return distribution(*eng);
}

void Sitmo11RNG::setSeed(const unsigned long int aSeed){
	seed = aSeed;
	eng->set_key(seed, seed/2, seed/4, seed/8);
	eng->set_counter();
}

unsigned long int Sitmo11RNG::getSeed() const {
	return seed;
}

void Sitmo11RNG::rewind(const int nSteps) const {
	eng->rewind(nSteps);
}

void Sitmo11RNG::reset() const {
	eng->set_counter();
}

void Sitmo11RNG::saveState(StateRNG &state) const {

	eng->getSeed(state.k0, state.k1, state.k2, state.k3);
	eng->get_counter(state.o_cnt, state.s0, state.s1, state.s2, state.s3);

}

void Sitmo11RNG::loadState(const StateRNG &state, bool onlyCounter) {

	if(!onlyCounter) {
		eng->setSeed(state.k0, state.k1, state.k2, state.k3);
	}
	eng->set_counter(state.o_cnt, state.s0, state.s1, state.s2, state.s3);

}
