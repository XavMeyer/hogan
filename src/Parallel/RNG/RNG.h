//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * RNG.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef RNG_H_
#define RNG_H_

#include <stdlib.h>
#include <vector>
#include <cmath>
#include "Utils/Uncopyable.h"

#include "StateRNG/StateRNG.h"

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Cholesky>

using namespace std;

class RNG : private Uncopyable {
public:
	virtual ~RNG() {};


	virtual void setSeed(unsigned long int aSeed) = 0;
	virtual unsigned long int getSeed() const = 0;

	virtual void saveState(StateRNG &state) const = 0;
	virtual void loadState(const StateRNG &state, bool onlyCounter=false) = 0;

	virtual int genUniformInt() const = 0;
	virtual int genUniformInt(const int aMin, const int aMax) const = 0;

	virtual double genUniformDbl() const = 0;
	virtual double genUniformDbl(const double aMin, const double aMax) const = 0;

	virtual double genGaussian(const double aVar) const = 0;
	virtual double genBeta(const double alpha, const double beta) const = 0;
	virtual double genGamma(const double shape, const double scale) const = 0;

	virtual double genBinomial(const double p, const unsigned int n) const = 0;
	virtual double genLogNormal(const double mu, const double sigma) const = 0;

	virtual double genExponential(const double lambda) const = 0;

	virtual std::vector<double> genStandardNormal(const size_t n) const = 0;
	virtual std::vector<double> genMultivariateGaussian(const size_t n, const double *cholesky) const = 0;

	virtual void rewind(const int nSteps) const = 0;
	virtual void reset() const = 0;

	vector<double> pdfMVN(const vector< vector<double> > &points, const vector<double> &mu, const vector<double> &sigma) const;

	int drawFrom(const std::vector<double> &probs) const;
	int drawFrom(const Eigen::VectorXd &probs) const;

	template <class T>
	void randomShuffle(std::vector<T> &vec) const {
		const size_t N = vec.size();
		for(size_t i=0; i<N; ++i) {
			size_t rndNb = genUniformInt(0, N-1);
			swap (vec[i], vec[rndNb]);
		}
	}

public:

	static RNG* createSitmo11RNG();
	static RNG* createSitmo11RNG(unsigned long int aSeed);

public:
	static const long unsigned int DEFAULT_SEED;

	enum cholesky_status {SUCCESS = 0, ERROR_EIGEN = 1, ERROR_INPUT = 2};

};

#endif /* RNG_H_ */
