//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sitmo11RNG.h
 *
 *  Created on: 1 oct. 2013
 *      Author: meyerx
 */

#ifndef SITMO11RNG_H_
#define SITMO11RNG_H_

#include "RNG.h"
#include "StateRNG/StateRNG.h"

#include <boost/random.hpp>
#include "Engine/prng_engine.hpp"
#include "Distribution/Beta.h"
#include "Distribution/MultivariateNormal.h"
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>


using namespace boost::random;

class Sitmo11RNG: public RNG {
public:
	Sitmo11RNG();
	Sitmo11RNG(long unsigned int aSeed);
	virtual ~Sitmo11RNG();

	int genUniformInt() const;
	int genUniformInt(const int aMin, const int aMax) const;

	double genUniformDbl() const;
	double genUniformDbl(const double aMin, const double aMax) const;

	double genGaussian(const double sigma) const;
	double genLogNormal(const double mu, const double sigma) const;

	double genBinomial(const double p, const unsigned int n) const;

	double genBeta(const double alpha, const double beta) const;
	double genGamma(const double shape, const double scale) const;

	double genExponential(const double lambda) const;

	std::vector<double> genStandardNormal(const size_t n) const;
	std::vector<double> genMultivariateGaussian(const size_t n, const double *cholesky) const;

	void setSeed(const unsigned long int aSeed);
	unsigned long int getSeed() const;

	void rewind(const int nSteps) const;
	void reset() const;

	void saveState(StateRNG &state) const;
	void loadState(const StateRNG &state, bool onlyCounter=false);

private:
	long unsigned int seed;
	sitmo::prng_engine *eng;
};

#endif /* SITMO11RNG_H_ */
