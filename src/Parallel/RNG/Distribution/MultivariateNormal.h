//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Beta.h
 *
 * @date 23 oct. 2013
 * @author meyerx
 * @brief Based on  Matthias Troyer and Wesley P. Petersen 2006
 */


#ifndef MULTIVARIATENORMAL_H_
#define MULTIVARIATENORMAL_H_

#include <cmath>
#include <cassert>
#include <iostream>
#include <boost/limits.hpp>
#include <boost/assert.hpp>
#include <boost/static_assert.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>

#include <Eigen/Core>

template<class RealType = double>
class multivariate_normal_distribution
{
public:
  /// the type of random numbers produced  
  typedef std::vector<RealType> result_type;

  /// the input type
  typedef RealType input_type;

#if !defined(BOOST_NO_LIMITS_COMPILE_TIME_CONSTANTS) && !(defined(BOOST_MSVC) && BOOST_MSVC <= 1300)
/// INTERNAL ONLY
    BOOST_STATIC_ASSERT(!std::numeric_limits<RealType>::is_integer);
#endif

  /// @brief the constructor of the multi-variate normal distribution
  /// @param mean the vector of mean values
  /// @param cholesky the Cholesky decomposition of the covariance matrix
  ///
  /// The Cholesky decomposition has to be a square matrix and its dimension
  /// has to be the same as that of the mean vector. Instead of a Cholesky
  /// decomposition any other square root of the covariance matrix could be passed.
  ///

  
  multivariate_normal_distribution(const size_t n, const RealType* cholesky, const RealType* mean)
    : n_(n)
    , mean_(mean)
    , cholesky_(cholesky)
  {
  }

  /// @brief the constructor of the multi-variate normal distribution with zero mean
  /// @param cholesky the Cholesky decomposition of the covariance matrix
  ///
  /// Instead of a Cholesky decomposition any other square root 
  /// of the covariance matrix could be passed.
  ///
  /// Requires: cholesky.size1() == cholesky.size2()

  
  explicit multivariate_normal_distribution(const size_t n, const RealType* cholesky)
    : n_(n)
    , mean_(0)
    , cholesky_(cholesky)
  {
  }

/// INTERNAL ONLY
  multivariate_normal_distribution(const multivariate_normal_distribution& other)
    : n_(other.n_)
    , mean_(other.mean_)
    , cholesky_(other.cholesky_)
  {
  }

/// INTERNAL ONLY
  multivariate_normal_distribution& operator=(const multivariate_normal_distribution& other)
  {
	n_ = other.n_;
    mean_=other.mean_;
    cholesky_=other.cholesky_;
    return *this;
  }

  /// @return the vector of mean values
  RealType* const& mean() const { return mean_; }

  /// @return the Cholesky decomposition of the covariance marix
  RealType* const& cholesky() const { return cholesky_; }

  void reset() { }

  template<class Engine>
  result_type operator()(Engine& eng)
  {
	  /*std::vector<RealType> buffer(n_), result(n_, 0.);
      boost::variate_generator<Engine&,boost::normal_distribution<RealType> >
	    gen(eng,boost::normal_distribution<RealType>());
      std::generate(buffer.begin(),buffer.end(),gen);

      if(mean_ != 0){
    	  for(size_t iM=0; iM < n_; ++iM){
    		  result[iM] = mean_[iM];
    	  }
      }

      char trans = 'N';
      int size = n_, inc = 1;
      double cst = 1.0;
      dgemv_(&trans, &size, &size, &cst, const_cast<double*>(cholesky_), &size, &buffer[0], &inc, &cst, &result[0], &inc);

      return result;*/

	std::vector<RealType> result(n_, 0.), buffer(n_);
	boost::variate_generator<Engine&,boost::normal_distribution<RealType> >
	gen(eng,boost::normal_distribution<RealType>());
	std::generate(buffer.begin(),buffer.end(),gen);

	if(mean_ != 0){
		for(size_t iM=0; iM < n_; ++iM){
			result[iM] = mean_[iM];
		}
	}

	// Process is result = result + cholesky*buffer
	Eigen::Map<Eigen::VectorXd> mapResult(const_cast<double*>(result.data()), n_);
	Eigen::Map<Eigen::VectorXd> eigenB(const_cast<double*>(buffer.data()), n_);
	Eigen::Map<Eigen::MatrixXd> eigenC(const_cast<double*>(cholesky_), n_, n_);

	mapResult = mapResult + eigenC*eigenB;

	return result;
  }

private:
  const size_t n_;
  const RealType *mean_;
  const RealType *cholesky_;
};

#endif // MULTIVARIATENORMAL_H_


