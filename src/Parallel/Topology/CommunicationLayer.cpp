//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CommunicationLayer.cpp
 *
 * @date Aug 12, 2015
 * @author meyerx
 * @brief
 */
#include <Parallel/Topology/CommunicationLayer.h>

namespace Parallel {

const int CommunicationLayer::LOCAL_MASTER = 0;

CommunicationLayer::CommunicationLayer(const int aRank, const int aNProc, const size_t aLevel,
									   const size_t aGroupSize, CommunicationLayer *aPreviousLayer) :
					sharedMem(false), level(aLevel), localNProc(aGroupSize), myGlobalRank(aRank), globalNProc(aNProc),
					previousLayer(aPreviousLayer), localGroup(MPI_GROUP_NULL), layerGroup(NULL),
					localComm(MPI_COMM_NULL), layerComm(MPI_COMM_NULL)
{

	belonging = false;
	myLocalRank = myMasterGlobalRank = -1;

	if(level == 0) {
		assert(previousLayer == NULL);
		initBottomLayer();
	} else {
		initLayer();
	}


}

CommunicationLayer::CommunicationLayer(const int aRank, const int aNProc, const size_t aLevel, const size_t aGroupSize,
		   	   	   	   	   	   	   	   const std::vector<int> &aLocalProcs, CommunicationLayer *aPreviousLayer) :
					sharedMem(true), level(aLevel), localNProc(aGroupSize), myGlobalRank(aRank), globalNProc(aNProc),
					previousLayer(aPreviousLayer), localGroup(MPI_GROUP_NULL), layerGroup(NULL),
					localComm(MPI_COMM_NULL), layerComm(MPI_COMM_NULL)

{
	if((aLocalProcs.size() % localNProc) != 0) {
		std::cerr << "[Error] Initialisation of the shared communication layer failed." << std::endl;
		std::cerr << "The number of requested shared processors ( " << localNProc;
		std::cerr << " ) exceed the number of available shared processors ( " << aLocalProcs.size() << " ) " << std::endl;
		abort();
	}

	belonging = false;
	myLocalRank = myMasterGlobalRank = -1;

	if(level == 0) {
		assert(previousLayer == NULL);
		initBottomLayer(aLocalProcs);
	} else {
		initLayer();
	}
}

CommunicationLayer::~CommunicationLayer() {

}


bool CommunicationLayer::isBelonging() const {
	return belonging;
}

int CommunicationLayer::getMyGlobakRank() const {
	return myGlobalRank;
}

int CommunicationLayer::getGlobalNProc() const {
	return globalNProc;
}

int CommunicationLayer::getMyMasterGlobalRank() const {
	assert(belonging);
	return myMasterGlobalRank;
}

const std::vector<int>& CommunicationLayer::getGlobalRanks() const {
	assert(belonging);
	return globalRanks;
}


int CommunicationLayer::getMyLocalRank() const {
	assert(belonging);
	return myLocalRank;
}

int CommunicationLayer::getLocalNProc() const {
	return localNProc;
}

int CommunicationLayer::getMyMasterLocalRank() const {
	assert(belonging);
	return LOCAL_MASTER;
}

int CommunicationLayer::getMyLayerRank() const {
	return myLayerRank;
}

int CommunicationLayer::getLayerNProc() const {
	return layerNProc;
}


MPI_Group CommunicationLayer::getLocalGroupMPI() const {
	assert(belonging);
	return localGroup;
}

MPI_Comm CommunicationLayer::getLocalCommMPI() const {
	assert(belonging);
	return localComm;
}

MPI_Group CommunicationLayer::getLayerGroupMPI() const {
	assert(belonging);
	return layerGroup;
}

MPI_Comm CommunicationLayer::getLayerCommMPI() const {
	assert(belonging);
	return layerComm;
}

std::string CommunicationLayer::toString() const {
	std::stringstream ss;
	ss << "Layer level : " << level << "\t" << (sharedMem ? "shared" : "distributed") << std::endl;
	if(belonging) {
		ss << "Global myRank : " << myGlobalRank << "\t nbProc = " << globalNProc << std::endl;
		ss << "Layer myRank : " << myLayerRank << "\t nbProc = " << layerNProc << std::endl;
		ss << "Local myRank : " << myLocalRank << "\t nbProc = " << localNProc << std::endl;
		ss << "Neighbors : ";
		for(size_t iN=0; iN<globalRanks.size(); ++iN) ss << globalRanks[iN] << "\t";
		ss << std::endl << "with master : " << myMasterGlobalRank << std::endl;;
	} else {
		ss << "Not belonging to this layer." << std::endl;;
	}

	return ss.str();
}

void CommunicationLayer::initBottomLayer() {
	assert(!sharedMem);

	// Basic init
	belonging = true;
	myLayerRank = myGlobalRank;
	layerNProc = globalNProc;
	layerComm = MPI_COMM_WORLD;
	MPI_Comm_group(layerComm, &layerGroup);

	// Create the communicator
	div_t resDiv = div(myGlobalRank, localNProc);
	int color = resDiv.quot;
	int key = resDiv.rem;
	MPI_Comm_split(MPI_COMM_WORLD, color, key, &localComm);
	extractInformationFromLocalComm();
}

void CommunicationLayer::initBottomLayer(const std::vector<int> &aLocalProcs) {
	assert(sharedMem);

	// Basic init
	belonging = true;
	myLayerRank = myGlobalRank;
	layerNProc = globalNProc;
	layerComm = MPI_COMM_WORLD;
	MPI_Comm_group(layerComm, &layerGroup);


	// Create the communicator
	size_t myPos = 0;
	for(size_t i=0; i<aLocalProcs.size(); ++i) {
		if(aLocalProcs[i] == myGlobalRank) {
			myPos = i;
			break;
		}
	}

	// Define position in group and group number
	div_t resDiv = div(myPos, localNProc);
	int idxGroup = resDiv.quot;
	//int idxPos = resDiv.rem;

	// "Global group"
	MPI_Group globalGroup;
	MPI_Comm_group(MPI_COMM_WORLD, &globalGroup);

	// Copy the group indices
	globalRanks.assign(&aLocalProcs[idxGroup*localNProc], &aLocalProcs[(idxGroup+1)*localNProc]);

	// Create new group
	MPI_Group_incl(globalGroup, localNProc, globalRanks.data(), &localGroup);

	// Create new communicator and then perform collective communications
	MPI_Comm_create(MPI_COMM_WORLD, localGroup, &localComm);

	// Define local rank
	MPI_Group_translate_ranks(globalGroup, 1, &myGlobalRank, localGroup, &myLocalRank);

	// Define the global rank of the local master
	myMasterGlobalRank = globalRanks[LOCAL_MASTER];
}

void CommunicationLayer::initLayer() {

	// Not event belonging to previous layer
	if(!previousLayer->isBelonging()) {
		belonging = false;
		return;
	}

	// Define the set of processor present in this layer
	int color = 0;
	if(previousLayer->getMyMasterGlobalRank() == myGlobalRank) {
		color = 1;
		belonging = true;
	} else {
		color = 0;
		belonging = false;
	}

	// Create the layer communicator
	MPI_Comm_split(previousLayer->getLayerCommMPI(), color, myGlobalRank, &layerComm);

	if(!belonging) { // if I do not belong to this layer, I quit
		layerComm = MPI_COMM_NULL;
		return;
	}

	// Else I define the layer group, rank and nProc
	MPI_Comm_rank(layerComm, &myLayerRank);
	MPI_Comm_size(layerComm, &layerNProc);
	MPI_Comm_group(layerComm, &layerGroup);
	if((layerNProc%localNProc) != 0) {
		std::cerr << "[Error] Initialisation of the shared communication layer failed." << std::endl;
		std::cerr << "The number of requested processors ( " << localNProc;
		std::cerr << " ) is not a divisor of the number of available processors ( " << layerNProc << " ) " << std::endl;
		abort();
	}

	// And then I define the local information
	div_t resDiv = div(myLayerRank, localNProc);
	color = resDiv.quot;
	int key = resDiv.rem;
	MPI_Comm_split(layerComm, color, key, &localComm);
	extractInformationFromLocalComm();



}

void CommunicationLayer::extractInformationFromLocalComm() {
	std::vector<int> tmpRanks(localNProc, 0);
	for(size_t iN=0; iN<tmpRanks.size(); ++iN) {
		tmpRanks[iN] = iN;
	}

	globalRanks.resize(localNProc);

	MPI_Group globalGroup;
	MPI_Comm_group(localComm, &localGroup);
	MPI_Comm_group(MPI_COMM_WORLD, &globalGroup);
	MPI_Group_translate_ranks(localGroup, localNProc, tmpRanks.data(), globalGroup, globalRanks.data());

	// Define local rank
	MPI_Group_translate_ranks(globalGroup, 1, &myGlobalRank, localGroup, &myLocalRank);

	// Define the global rank of the local master
	myMasterGlobalRank = globalRanks[LOCAL_MASTER];
}


} /* namespace Parallel */
