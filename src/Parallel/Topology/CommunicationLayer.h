//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CommunicationLayer.h
 *
 * @date Aug 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMMUNICATIONLAYER_H_
#define COMMUNICATIONLAYER_H_

#include "mpi.h"
#include <cassert>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <sstream>

namespace Parallel {

class CommunicationLayer {
public:
	CommunicationLayer(const int aRank, const int aNProc, const size_t aLevel, const size_t aGroupSize, CommunicationLayer *aPreviousLayer);
	CommunicationLayer(const int aRank, const int aNProc, const size_t aLevel, const size_t aGroupSize,
					   const std::vector<int> &aLocalProcs, CommunicationLayer *aPreviousLayer);
	~CommunicationLayer();

	bool isBelonging() const;

	int getMyGlobakRank() const;
	int getGlobalNProc() const;
	int getMyMasterGlobalRank() const;
	const std::vector<int>& getGlobalRanks() const;

	int getMyLocalRank() const;
	int getLocalNProc() const;
	int getMyMasterLocalRank() const;

	int getMyLayerRank() const;
	int getLayerNProc() const;

	MPI_Group getLocalGroupMPI() const;
	MPI_Comm getLocalCommMPI() const;

	MPI_Group getLayerGroupMPI() const;
	MPI_Comm getLayerCommMPI() const;


	std::string toString() const;


private:

	static const int LOCAL_MASTER;

	bool belonging;
	const bool sharedMem;
	const size_t level;
	int localNProc, myLocalRank;
	int layerNProc, myLayerRank;
	int myGlobalRank, globalNProc, myMasterGlobalRank;

	std::vector<int> globalRanks;

	CommunicationLayer *previousLayer;

	MPI_Group localGroup, layerGroup;
	MPI_Comm localComm, layerComm;

	void initBottomLayer();
	void initBottomLayer(const std::vector<int> &aLocalProcs);
	void initLayer();

	void extractInformationFromLocalComm();

};

} /* namespace Parallel */

#endif /* COMMUNICATIONLAYER_H_ */
