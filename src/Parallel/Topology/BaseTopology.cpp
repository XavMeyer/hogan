//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseTopology.cpp
 *
 * @date Aug 12, 2015
 * @author meyerx
 * @brief
 */
#include "BaseTopology.h"

namespace Parallel {

BaseTopology::BaseTopology(const int aRank, const int aNProc, const bool aUseShared, const std::vector<size_t> &aGroupSizePerLayer) :
		useShared(aUseShared), myRank(aRank), nProc(aNProc), groupSizePerLayer(aGroupSizePerLayer)
{
	init();
}

BaseTopology::~BaseTopology() {
	for(size_t iL=0; iL<layers.size(); ++iL) {
		delete layers[iL];
		layers[iL] = NULL;
	}
}

void BaseTopology::init() {
	// Check if correct number of CPU
	int prod = groupSizePerLayer[0];
	for(size_t iL=1; iL<groupSizePerLayer.size(); ++iL) prod *= groupSizePerLayer[iL];
	if(prod != nProc) {
		std::cerr << "[Error] BaseTopology::init(...);" << std::endl;
		std::cerr << "The number of MPI processor (" << nProc << ") does not correspond to the product of the group size per layer (" << prod << ")." << std::endl;
		std::abort();
	}

	// If use shared define the processor in shared memory
	// Then push the first layer
	if(useShared) {
		defineLocalProcs();
		layers.push_back(new CommunicationLayer(myRank, nProc, 0, groupSizePerLayer[0], localRanks, NULL));
	} else {
		layers.push_back(new CommunicationLayer(myRank, nProc, 0, groupSizePerLayer[0], NULL));
	}

	for(size_t iL=1; iL<groupSizePerLayer.size(); ++iL) {
		layers.push_back(new CommunicationLayer(myRank, nProc, iL, groupSizePerLayer[iL], layers[iL-1]));
	}
}

void BaseTopology::defineLocalProcs(){
	// Get HostName
	int lenTmpStr = -1;
	char tmpStr[MPI_MAX_PROCESSOR_NAME];
	MPI_Get_processor_name(tmpStr, &lenTmpStr);
	tmpStr[lenTmpStr] = '\0';
	std::string hostName(tmpStr, lenTmpStr);

	// Get color of group by hashing the hostName
	//boost::hash<std::string> stringHash;
	//int color = stringHash(hostName);
	int color = FNV::fnv1a(hostName);

	// Create localComm
	MPI_Comm localComm;
	MPI_Comm_split(MPI_COMM_WORLD, color, myRank, &localComm);

	// Init local information
	int localRank=-1, localNProc=-1;
	MPI_Comm_rank(localComm, &localRank);
	MPI_Comm_size(localComm, &localNProc);

	// Check potential error of hash collision
	char *buffer = new char[MPI_MAX_PROCESSOR_NAME*localNProc];
	MPI_Allgather(tmpStr, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, buffer, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, localComm);

	for(int iN=0; iN<localNProc; ++iN) {
		std::string tmpCmp(&buffer[iN*MPI_MAX_PROCESSOR_NAME]);
		if(hostName != tmpCmp) {
			std::cerr << "[Error] BaseTopology::init(...);" << std::endl;
			std::cerr << "There was an error during the definition of the shared memory Topology (hash collision)." << std::endl;
			std::abort();
		}
	}
	delete [] buffer;

	// Get other process rank
	MPI_Group localGroup, globalGroup;
	MPI_Comm_group(localComm, &localGroup);
	MPI_Comm_group(MPI_COMM_WORLD, &globalGroup);
	std::vector<int> tmpRanks(localNProc, 0);
	for(size_t iN=0; iN<tmpRanks.size(); ++iN) {
		tmpRanks[iN] = iN;
	}

	localRanks.resize(localNProc);
	MPI_Group_translate_ranks(localGroup, localNProc, tmpRanks.data(), globalGroup, localRanks.data());
	//MPI_Allgather(&rank, 1, MPI_INT, localProcs.data(), 1, MPI_INT, localComm);
}

std::string BaseTopology::toString() const {
	std::stringstream ss;
	ss << "N Level : " << layers.size() << std::endl;
	ss << "Layer config : ";
	for(size_t iL=0; iL<groupSizePerLayer.size(); ++iL) {
		ss << groupSizePerLayer[iL] << "\t";
	}
	ss << std::endl;

	for(size_t iL=0; iL<layers.size(); ++iL) {
		ss << "--------------------------------------------------------" << std::endl;
		ss << layers[iL]->toString();
	}
	ss << "--------------------------------------------------------" << std::endl;

	return ss.str();
}

const CommunicationLayer* BaseTopology::getCommunicationLayer(const size_t iLevel) {
	assert(iLevel < layers.size());
	return layers[iLevel];
}

} /* namespace Parallel */
