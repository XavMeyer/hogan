//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParallelLikManager.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include <Parallel/Manager/ParallelLikManager.h>

namespace Parallel {

const size_t ParallelLikManager::MAX_CONCURRENT_BUFFERED_MESSAGES = 64;

ParallelLikManager::ParallelLikManager() {
	roleParLik = None;
	comLayer = NULL;

}

ParallelLikManager::~ParallelLikManager() {
	comLayer = NULL;
}


bool ParallelLikManager::checkRoleLikelihood(role_t aRole)  const {
	return aRole == roleParLik;
}

int ParallelLikManager::getMyLocalManagerRank() const {
	assert(comLayer != NULL);
	return comLayer->getMyMasterLocalRank();
}

int ParallelLikManager::getMyLocalRank() const {
	assert(comLayer != NULL);
	return comLayer->getMyLocalRank();
}


size_t ParallelLikManager::getNProc()  const {
	assert(comLayer != NULL);
	return comLayer->getLocalNProc();
}


ParallelLikManager::task_t ParallelLikManager::codeToTask(const size_t code) const {
	switch (code) {
		case NONE:
			return NONE;
		case INIT:
			return INIT;
		case PROCESS:
			return PROCESS;
		case STOP:
			return STOP;
		default:
			break;
	}
	return NONE;
}

} /* namespace Parallel */
