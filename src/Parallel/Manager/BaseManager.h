//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseManager.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef BASEMANAGER_H_
#define BASEMANAGER_H_

#include <string>
#include "../Topology/CommunicationLayer.h"

namespace Parallel {

class BaseManager {
public:
	enum role_t {None=0, Worker=1, Manager=2, Sequential=3};

public:
	BaseManager();
	virtual ~BaseManager();

	void setActive(const bool aActive);
	bool isActive() const;

protected:

	bool active;

	role_t defineRole(const CommunicationLayer* commLayer);
	std::string roleToString(role_t myRole) const;

};

} /* namespace Parallel */

#endif /* BASEMANAGER_H_ */
