//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MCMCManager.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "MCMCManager.h"

namespace Parallel {

MCMCManager::MCMCManager() {

	roleProposal = None;
	roleMCMC = None;
	roleMC3 = None;

	myChainMC3 = -1;

}

MCMCManager::~MCMCManager() {
}

void MCMCManager::init(const bool sharedMem, const size_t nParallelLik, const size_t nProposal, const size_t nChainMC3) {

	if(hasActiveManager()){
		std::cerr << "[Error] in MCMCManager." << std::endl;
		std::cerr << "Only one ML/MCMC parallel manager should be active at a time." << std::endl;
		std::cerr << "Deinit (using .deinit()) the obsolete manager before creating a new one." << std::endl;
		abort();
	}

	// Define layer group sizes
	std::vector<size_t> procPerGroup(4, 1);
	procPerGroup[PARALLEL_LIK_LAYER_ID] = nParallelLik;
	procPerGroup[PROPOSAL_LAYER_ID] = nProposal;
	procPerGroup[MCMC_LAYER_ID] = 1;
	procPerGroup[MC3_LAYER_ID] = nChainMC3;

	// Check that MPI mgr is init'd
	int myRank = Parallel::mpiMgr().getRank();
	int nProc = Parallel::mpiMgr().getNProc();

	if(myRank < 0 || nProc < 0 ) {
		std::cerr << "[Error] in MCMCManager::init(...);" << std::endl;
		std::cerr << "Parallel::mpiMgr().init(...) must be called prior to this." << std::endl;
		abort();
	}

	// Create layer communicators
	ptrBT.reset(new BaseTopology(myRank, nProc, sharedMem, procPerGroup));
	Parallel::pllMgr().comLayer = ptrBT->getCommunicationLayer(PARALLEL_LIK_LAYER_ID);

	// Define roles
	defineRoles();
	setActive(true);

	myChainMC3 = defineMyChainMC3();

	//std::cout << ptrBT->toString() << std::endl;
}
int MCMCManager::defineMyChainMC3() {
	if(checkRoleProposal(None)) {
		return -1;
	} else if(checkRoleProposal(Sequential)) {
		return getRankMC3();
	} else {
		const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
		// Define root
		int root = commL->getMyMasterLocalRank();
		// Define communicator
		MPI_Comm myCom = commL->getLocalCommMPI();

		int val = -1;
		if(checkRoleProposal(Manager)) {
			val = getRankMC3();
		}
		Parallel::mpiMgr().bcast(&val, 1, root, myCom);
		return val;
	}
	return -1;
}

void MCMCManager::deinit() {
	setActive(false);
	// Deinit roles
	Parallel::pllMgr().roleParLik = None;
	roleProposal = None;
	roleMCMC = None;
	roleMC3 = None;

	// Deinit layers
	Parallel::pllMgr().comLayer = NULL;
	ptrBT.reset();

}

bool MCMCManager::isOutputManager() const {
	return (roleMC3 == Sequential || roleMC3 == Manager) &&
		   (roleProposal == Sequential || roleProposal == Manager);
}

bool MCMCManager::isMCMCSequential() const {
	return roleMCMC == Sequential;
}

bool MCMCManager::isProposalSequential() const {
	return roleProposal == Sequential;
}

bool MCMCManager::isActiveMC3() const {
	return ptrBT->getCommunicationLayer(MC3_LAYER_ID)->getLocalNProc() > 1;
}

bool MCMCManager::isManagerMC3() const {
	return isActiveMC3() &&
			((checkRoleProposal(Manager) ||
			  checkRoleProposal(Sequential)));
}

bool MCMCManager::checkRoleProposal(role_t aRole) const {
	return aRole == roleProposal;
}

bool MCMCManager::checkRoleMCMC(role_t aRole) const {
	return aRole == roleMCMC;
}

bool MCMCManager::checkRoleMC3(role_t aRole) const {
	return aRole == roleMC3;
}


int MCMCManager::getManagerProposal() const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID)->getMyMasterLocalRank();
}

int MCMCManager::getNProposal() const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID)->getLocalNProc();
}

int MCMCManager::getNChain() const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(MCMC_LAYER_ID)->getLocalNProc();
}

int MCMCManager::getRankProposal() const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID)->getMyLocalRank();
}

int MCMCManager::getRankMC3() const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(MC3_LAYER_ID)->getMyLocalRank();
}

int MCMCManager::getMyChainMC3() const {
	return myChainMC3;
}

int MCMCManager::getNChainMC3() const {
	return ptrBT->getCommunicationLayer(MC3_LAYER_ID)->getLocalNProc();
}


void MCMCManager::sendSerializedSample(const int size, char* data, const int dest) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	Parallel::mpiMgr().send(data, size, dest, PROPOSAL_TAG, commL->getLocalCommMPI());
}

void MCMCManager::recvSerializedSample(const int size, char *data, const int src) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	Parallel::mpiMgr().recv(data, size, src, PROPOSAL_TAG, commL->getLocalCommMPI());
}

void MCMCManager::exchangeSerializedSample(const int size, char *dataSend, char *dataRecv, const int other) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(MC3_LAYER_ID);
	MPI_Status status;
	MPI_Sendrecv(dataSend, size, MPI_CHAR, other, MC3_SAMPLE_TAG,
	 	 	  	 dataRecv, size, MPI_CHAR, other, MC3_SAMPLE_TAG,
				 commL->getLocalCommMPI(), &status);
}

void MCMCManager::sendTreeString(const Utils::Serialize::buffer_t &tree) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	for(int dest=0; dest<commL->getLocalNProc(); ++dest) { // For each neighbours
		if(dest != commL->getMyLocalRank()) { // But us
			MPI_Request req; // We send the tree string
			MPI_Isend(const_cast<char*>(tree.data()), tree.size(), MPI_CHAR, dest, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &req);
			MPI_Request_free(&req);
		}
	}
}

void MCMCManager::waitTreeString(Utils::Serialize::buffer_t &buffer) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	MPI_Status status;
	// Check if a message is pending (not blocking)
	MPI_Probe(MPI_ANY_SOURCE, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &status);
	// Prepare the receive using the MPI_STATUS
	int src = status.MPI_SOURCE;
	// Create the temporary buffer
	int size = 0;
	MPI_Get_count(&status, MPI_CHAR, &size);
	buffer.resize(size);
	// Read the message
	MPI_Recv(buffer.data(), size, MPI_CHAR, src, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &status);
}

void MCMCManager::recvTreeString(Utils::Serialize::vecBuffer_t &vecTree) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	int flag; // Flag that tell us if there is pending messages or not
	MPI_Status status;
	// Check if a message is pending (not blocking)
	MPI_Iprobe(MPI_ANY_SOURCE, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &flag, &status);
	while(flag) { // If so, while we have messages
		// Prepare the receive using the MPI_STATUS
		int src = status.MPI_SOURCE;
		// Create the temporary buffer
		int size = 0;
		MPI_Get_count(&status, MPI_CHAR, &size);
		Utils::Serialize::buffer_t buf(size);
		// Read the message
		MPI_Recv(buf.data(), size, MPI_CHAR, src, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &status);
		// Save into the the tree vector
		vecTree.push_back(buf);
		// Check for another message
		MPI_Iprobe(MPI_ANY_SOURCE, TREE_TOPOLOGY_TAG, commL->getLocalCommMPI(), &flag, &status);
	}

}

void MCMCManager::sendTreeMove(const Utils::Serialize::buffer_t &aMove) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	for(int dest=0; dest<commL->getLocalNProc(); ++dest) { // For each neighbours
		if(dest != commL->getMyLocalRank()) { // But us
			MPI_Request req; // We send the move string
			MPI_Isend(const_cast<char*>(aMove.data()), aMove.size(), MPI_CHAR, dest, TREE_MOVE_TAG, commL->getLocalCommMPI(), &req);
			MPI_Request_free(&req);
		}
	}

}

void MCMCManager::waitTreeMove(Utils::Serialize::buffer_t &buffer) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	MPI_Status status;
	// Check if a message is pending (not blocking)
	MPI_Probe(MPI_ANY_SOURCE, TREE_MOVE_TAG, commL->getLocalCommMPI(), &status);
	// Prepare the receive using the MPI_STATUS
	int src = status.MPI_SOURCE;
	// Create the temporary buffer
	int size = 0;
	MPI_Get_count(&status, MPI_CHAR, &size);
	buffer.resize(size);
	// Read the message
	MPI_Recv(buffer.data(), size, MPI_CHAR, src, TREE_MOVE_TAG, commL->getLocalCommMPI(), &status);
}

void MCMCManager::recvTreeMoves(Utils::Serialize::vecBuffer_t &moves) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	int flag; // Flag that tell us if there is pending messages or not
	MPI_Status status;
	// Check if a message is pending (not blocking)
	MPI_Iprobe(MPI_ANY_SOURCE, TREE_MOVE_TAG, commL->getLocalCommMPI(), &flag, &status);
	while(flag) { // If so, while we have messages
		// Prepare the receive using the MPI_STATUS
		int src = status.MPI_SOURCE;
		// Create the temporary buffer
		int size = 0;
		MPI_Get_count(&status, MPI_CHAR, &size);
		Utils::Serialize::buffer_t buf(size);
		// Read the message
		MPI_Recv(buf.data(), size, MPI_CHAR, src, TREE_MOVE_TAG, commL->getLocalCommMPI(), &status);
		// Save into the the tree vector
		moves.push_back(buf);
		// Check for another message
		MPI_Iprobe(MPI_ANY_SOURCE, TREE_MOVE_TAG, commL->getLocalCommMPI(), &flag, &status);
	}
}


void MCMCManager::gatherScores(const int size, double* inData, double* outData) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	// Define root
	int root = commL->getMyMasterLocalRank();
	// Define communicator
	MPI_Comm myCom = commL->getLocalCommMPI();

	// Gather
	int mSize = size;
	MPI_Gather(inData, mSize, MPI_DOUBLE, outData, mSize, MPI_DOUBLE, root, myCom);
}

void MCMCManager::exchangeBufferMC3(Utils::Serialize::buffer_t &bufferIn,
							  	  	Utils::Serialize::buffer_t &bufferOut,
							  	  	const int other) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(MC3_LAYER_ID);
	int size = bufferIn.size();

	if(commL->getMyLocalRank() < other) {
		// Send
		MPI_Send(bufferIn.data(), size, MPI_CHAR, other, MC3_STATE_TAG, commL->getLocalCommMPI());

		// Wait for message
		MPI_Status status;
		MPI_Probe(other, MC3_STATE_TAG, commL->getLocalCommMPI(), &status);

		// Set buffer size
		int recvSize = 0;
		MPI_Get_count(&status, MPI_CHAR, &recvSize);
		bufferOut.resize(recvSize);

		// Receive
		MPI_Recv(bufferOut.data(), recvSize, MPI_CHAR, other, MC3_STATE_TAG, commL->getLocalCommMPI(), &status);
	} else {
		// Wait for message
		MPI_Status status;
		MPI_Probe(other, MC3_STATE_TAG, commL->getLocalCommMPI(), &status);

		// Set buffer size
		int recvSize = 0;
		MPI_Get_count(&status, MPI_CHAR, &recvSize);
		bufferOut.resize(recvSize);

		// Receive
		MPI_Recv(bufferOut.data(), recvSize, MPI_CHAR, other, MC3_STATE_TAG, commL->getLocalCommMPI(), &status);

		// Send
		MPI_Send(bufferIn.data(), size, MPI_CHAR, other, MC3_STATE_TAG, commL->getLocalCommMPI());
	}
}

void MCMCManager::shareWithWorkersBufferMC3(Utils::Serialize::buffer_t &bufferIn) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	for(int dest=0; dest<commL->getLocalNProc(); ++dest) { // For each neighbours
		if(dest != commL->getMyLocalRank()) { // But us
			MPI_Send(bufferIn.data(), bufferIn.size(), MPI_CHAR, dest, MC3_STATE_TAG, commL->getLocalCommMPI());
		}
	}
}

void MCMCManager::receiveFromManagerBufferMC3(Utils::Serialize::buffer_t &bufferOut) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	// Wait for message
	MPI_Status status;
	MPI_Probe(commL->getMyMasterLocalRank(), MC3_STATE_TAG, commL->getLocalCommMPI(), &status);

	// Set buffer size
	int recvSize = 0;
	MPI_Get_count(&status, MPI_CHAR, &recvSize);
	bufferOut.resize(recvSize);

	// Receive
	MPI_Recv(bufferOut.data(), recvSize, MPI_CHAR, commL->getMyMasterLocalRank(), MC3_STATE_TAG, commL->getLocalCommMPI(), &status);
}

void MCMCManager::exchangeAllRequireOpti(bool doIRequireOpti, std::vector<bool> &doTheyRequireOpti) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	int mySize = 1;											// My data size
	char myData = static_cast<char>(doIRequireOpti);		// My data

	std::vector<int> sizes(commL->getLocalNProc(), 1); 		// Everyone has one value
	std::vector<char> recvBuffer(commL->getLocalNProc());	// Receive buffer

	// Do the communication
	Parallel::mpiMgr().exchangeAll(commL->getLocalNProc(), mySize, &myData,
			sizes.data(), recvBuffer.data(), commL->getLocalCommMPI());

	// Fill return vector
	doTheyRequireOpti.resize(commL->getLocalNProc());
	for(size_t i=0; i<recvBuffer.size(); ++i) {
		doTheyRequireOpti[i] = static_cast<bool>(recvBuffer[i]);
	}
}

void MCMCManager::exchangeAllRelativeGains(double myRelativeScore, std::vector<double> &theirRelativeScore) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	int mySize = 1;											// My data size
	double myData = myRelativeScore;						// My data

	std::vector<int> sizes(commL->getLocalNProc(), 1); 		// Everyone has one value
	theirRelativeScore.resize(commL->getLocalNProc());		// Prepare recv buffer

	// Do the communication
	Parallel::mpiMgr().exchangeAll(commL->getLocalNProc(), mySize, &myData,
			sizes.data(), theirRelativeScore.data(), commL->getLocalCommMPI());
}

void MCMCManager::bcastBestClusters(int root, std::vector< std::vector<size_t> > &clusters) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);

	int bufferSize = 0, cntFirstBCast = 1;
	std::vector<size_t> buffer;
	if(commL->getMyLocalRank() == root) {
		// Put 2D array in 1D
		buffer.push_back(clusters.size());	 			// Push nb of 1D array
		for(size_t i=0; i<clusters.size(); ++i) {		// For each 1D array
			// Push size of ith 1D array
			buffer.push_back(clusters[i].size());
			// Push elems of cluster
			buffer.insert(buffer.end(),	clusters[i].begin(), clusters[i].end());
		}
		// set buffer size
		bufferSize = buffer.size();
		// Send buffer size
		Parallel::mpiMgr().bcast(&bufferSize, cntFirstBCast, root, commL->getLocalCommMPI());
	} else {
		// Recv buffer size
		Parallel::mpiMgr().bcast(&bufferSize, cntFirstBCast, root, commL->getLocalCommMPI());
		// Set buffer size
		buffer.resize(bufferSize);
	}

	// Exchange buffer
	Parallel::mpiMgr().bcast(buffer.data(), bufferSize, root, commL->getLocalCommMPI());

	if(commL->getMyLocalRank() != root) {	// If not root, reform cluster
		clusters.clear();					// Empty clusters

		std::vector<size_t>::iterator it = buffer.begin();
		size_t nClust = *it; it++;						// Define nb of 1D array
		clusters.resize(nClust);

		for(size_t iC=0; iC<nClust; ++iC) {
			size_t cSize = *it; it++;					// Define size of ith 1D array
			// Push elem into cluster
			clusters[iC].insert(clusters[iC].end(), it, it+cSize);
			std::advance(it, cSize);
		}
	}

}

void MCMCManager::syncProposals() const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
	MPI_Barrier(commL->getLocalCommMPI());

}

void MCMCManager::defineRoles() {

	Parallel::pllMgr().roleParLik = defineRole(ptrBT->getCommunicationLayer(PARALLEL_LIK_LAYER_ID));
	roleProposal = defineRole(ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID));
	roleMCMC = defineRole(ptrBT->getCommunicationLayer(MCMC_LAYER_ID));
	roleMC3 = defineRole(ptrBT->getCommunicationLayer(MC3_LAYER_ID));

}


} /* namespace Parallel */
