//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseManager.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "BaseManager.h"

namespace Parallel {

BaseManager::BaseManager() : active(false) {

}

BaseManager::~BaseManager() {
}

BaseManager::role_t BaseManager::defineRole(const CommunicationLayer* commLayer) {
	// Are we part of this layer ?
	if(!commLayer->isBelonging()) return None;

	// Are there more than one person on the layer ?
	if(commLayer->getLocalNProc() == 1) return Sequential;

	// Parallel case : Am i the manager ?
	if(commLayer->getMyMasterGlobalRank() == commLayer->getMyGlobakRank()) {
		return Manager; // Yes
	} else {
		return Worker; // No
	}
}

std::string BaseManager::roleToString(role_t myRole) const {
	switch (myRole) {
		case None:
			return "None";
			break;
		case Worker:
			return "Worker";
			break;
		case Manager:
			return "Manager";
			break;
		case Sequential:
			return "Sequential";
			break;
	}

	return "----";
}

void BaseManager::setActive(const bool aActive) {
	active = aActive;
}
bool BaseManager::isActive() const {
	return active;
}


} /* namespace Parallel */
