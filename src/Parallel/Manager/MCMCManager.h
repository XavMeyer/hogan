//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MCMCManager.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef MCMCMANAGER_H_
#define MCMCMANAGER_H_

#include "BaseManager.h"
#include "ManagerUtils.h"
#include "Utils/Code/SerializationSupport.h"
#include "../Topology/BaseTopology.h"

#include <iostream>

namespace Parallel {

class MCMCManager : public BaseManager, private Uncopyable {
public:

	void init(const bool sharedMem, const size_t nParallelLik, const size_t nProposal, const size_t nChainMC3=1);
	void deinit();

	// TOPOLOGICAL INFORMATION
	bool isMCMCSequential() const;
	bool isProposalSequential() const;
	bool isActiveMC3() const;
	bool isManagerMC3() const;
	bool isOutputManager() const;

	bool checkRoleProposal(role_t aRole) const;
	bool checkRoleMCMC(role_t aRole) const;
	bool checkRoleMC3(role_t aRole) const;

	int getManagerProposal() const;
	int getRankProposal() const;

	int getRankMC3() const;
	int getMyChainMC3() const;
	int getNChainMC3() const;

	int getNProposal() const;
	int getNChain() const;


	// COMMUNICATION PRIMITIVE
	void sendSerializedSample(const int size, char* data, const int dest) const;
	void recvSerializedSample(const int size, char *data, const int src) const;
	void exchangeSerializedSample(const int size, char *dataSend, char *dataRecv, const int other) const;

	void sendTreeString(const Utils::Serialize::buffer_t &treeStr) const;
	void recvTreeString(Utils::Serialize::vecBuffer_t &vecTreeStr) const;
	void waitTreeString(Utils::Serialize::buffer_t &buffer) const;

	void sendTreeMove(const Utils::Serialize::buffer_t &aMove) const;
	void recvTreeMoves(Utils::Serialize::vecBuffer_t &moves) const;
	void waitTreeMove(Utils::Serialize::buffer_t &buffer) const;

	void gatherScores(const int size, double *inData, double *outData) const;

	void exchangeBufferMC3(Utils::Serialize::buffer_t &bufferIn,
						   Utils::Serialize::buffer_t &bufferOut,
						   const int other) const;

	void shareWithWorkersBufferMC3(Utils::Serialize::buffer_t &bufferIn) const;
	void receiveFromManagerBufferMC3(Utils::Serialize::buffer_t &bufferOut) const;

	void exchangeAllRequireOpti(bool doIRequireOpti, std::vector<bool> &doTheyRequireOpti) const;
	void exchangeAllRelativeGains(double myRelativeScore, std::vector<double> &theirRelativeScore) const;
	void bcastBestClusters(int root, std::vector< std::vector<size_t> > &clusters) const;

	template <typename T>
	void bcastProposal(const int size, T* data, const int root) const {
		const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
		Parallel::mpiMgr().bcast<T>(data, size, root, commL->getLocalCommMPI());
	}

	void exchangeAllProposal(int mySize, double* myData, int* sizes, double* data) const {
		const CommunicationLayer *commL = ptrBT->getCommunicationLayer(PROPOSAL_LAYER_ID);
		Parallel::mpiMgr().exchangeAll(commL->getLocalNProc(), mySize, myData, sizes, data, commL->getLocalCommMPI());
	}

	// DEBUG
	void syncProposals() const;

private:
	enum tag_t {PARALLEL_LIK_TAG=0, PROPOSAL_TAG=1, MC3_SAMPLE_TAG=2, TREE_TOPOLOGY_TAG=3, TREE_MOVE_TAG=4, MC3_STATE_TAG=5, DEFAULT_TAG=100};
	enum layer_id_t {PARALLEL_LIK_LAYER_ID=0, PROPOSAL_LAYER_ID=1, MCMC_LAYER_ID=2, MC3_LAYER_ID=3};

	int myChainMC3;
	BaseTopology::sharedPtr_t ptrBT;
	role_t roleProposal, roleMCMC, roleMC3;

	void defineRoles();
	int defineMyChainMC3();

private:
	// Defined in the body
	MCMCManager();
	~MCMCManager();
	// Not defined to avoid call
	MCMCManager(const MCMCManager&);
	MCMCManager& operator=(const MCMCManager&);

	friend MCMCManager& mcmcMgr();
};

inline MCMCManager& mcmcMgr() {
    static MCMCManager instance;
    return instance;
}

} /* namespace Parallel */

#endif /* MCMCMANAGER_H_ */
