//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MLManager.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef MLMANAGER_H_
#define MLMANAGER_H_

#include "BaseManager.h"
#include "ManagerUtils.h"
#include "../Topology/BaseTopology.h"

namespace Parallel {

class MLManager : public BaseManager, private Uncopyable {
public:
	enum gradient_signal_t {COMPUTE=0, LB_COMPUTE=1, TERMINATE=2};

public:

	void init(const bool sharedMem, const size_t nParallelLik, const size_t nGradient);
	void deinit();


	bool checkRoleGradient(role_t aRole) const;
	bool checkRoleML(role_t aRole) const;
	bool isMainProcessor() const;

	int getMyRankGradient() const;
	int getNProcGradient() const;

	void broadcastValuesWrk(bool &signal, std::vector<double> &values) const;
	void broadcastValuesMgr(bool signal, const std::vector<double> &values) const;

	void broadcastValuesWrk(gradient_signal_t &signal, std::vector<double> &values) const;
	void broadcastValuesMgr(gradient_signal_t signal, const std::vector<double> &values) const;

	void broadcastAssignement(std::vector<size_t> &assignement) const;

	void gatherLikelihoods(std::vector<double> &localLikelihoods, std::vector<double> &gatherResults) const;


	void gathervDoubleMgr(std::vector<double> &localLikelihoods, std::vector<int> &recvCount,
							   std::vector<double> &gatherResults) const;
	void gathervDoubleWrk(std::vector<double> &localLikelihoods) const;


private:
	enum tag_t {PARALLEL_LIK_TAG=0, GRADIENT_TAG=1, DEFAULT_TAG=100};
	enum layer_id_t {PARALLEL_LIK_LAYER_ID=0, GRADIENT_LAYER_ID=1, ML_LAYER_ID=2};

	BaseTopology::sharedPtr_t ptrBT;
	role_t roleParLik, roleGradient, roleML;

	void defineRoles();


private:
	// Defined in the body
	MLManager();
	~MLManager();
	// Not defined to avoid call
	MLManager(const MLManager&);
	MLManager& operator=(const MLManager&);

	friend MLManager& mlMgr();
};

inline MLManager& mlMgr() {
    static MLManager instance;
    return instance;
}

} /* namespace Parallel */

#endif /* MLMANAGER_H_ */
