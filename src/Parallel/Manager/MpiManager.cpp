//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MpiMgr.cpp
 *
 *  Created on: 30 sept. 2013
 *      Author: meyerx
 */

#include "MpiManager.h"

namespace Parallel {

const int MpiManager::MainProcessor = 0;

MpiManager::MpiManager() : myRank(-1), nProc(-1), startTime(0.) {
	prng = RNG::createSitmo11RNG();
}

MpiManager::~MpiManager() {
	delete prng;
	MPI_Finalize();
}

void MpiManager::init(int *argc, char ***argv){
	MPI_Init(argc, argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProc);

	prng->setSeed(RNG::DEFAULT_SEED*myRank+12);

	startTime = MPI_Wtime();
}

template <>
void MpiManager::send<double>(double* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_DOUBLE, dest, tag, com);
}

template <>
void MpiManager::send<char>(char* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_CHAR, dest, tag, com);
}

template <>
void MpiManager::send<int>(int* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_INT, dest, tag, com);
}

template <>
void MpiManager::send<size_t>(size_t* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_INT, dest, tag, com);
}

template <>
void MpiManager::recv<double>(double* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_DOUBLE, dest, tag, com, &status);
}

template <>
void MpiManager::recv<char>(char* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_CHAR, dest, tag, com, &status);
}

template <>
void MpiManager::recv<int>(int* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_INT, dest, tag, com, &status);
}

template <>
void MpiManager::recv<size_t>(size_t* data, const int size, const int dest, const int tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_UNSIGNED_LONG, dest, tag, com, &status);
}


template <>
void MpiManager::exchangeAll<double>(size_t nProc, int mySize, double* myData, int* sizes, double* data, const MPI_Comm com) const {
	int displs[nProc];

	// Compute displacement vectors
	displs[0] = 0;
	for(uint i=1; i<nProc; ++i){
		displs[i] = displs[i-1] + sizes[i-1];
	}
	MPI_Allgatherv(myData, mySize, MPI_DOUBLE, data, sizes, displs, MPI_DOUBLE, com);
}

template <>
void MpiManager::exchangeAll<char>(size_t nProc, int mySize, char* myData, int* sizes, char* data, const MPI_Comm com) const {
	int displs[nProc];

	// Compute displacement vectors
	displs[0] = 0;
	for(uint i=1; i<nProc; ++i){
		displs[i] = displs[i-1] + sizes[i-1];
	}
	MPI_Allgatherv(myData, mySize, MPI_CHAR, data, sizes, displs, MPI_CHAR, com);
}



template <>
void MpiManager::bcast<double>(double* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_DOUBLE, root, com);
}

template <>
void MpiManager::bcast<char>(char* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_CHAR, root, com);
}

template <>
void MpiManager::bcast<int>(int* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_INT, root, com);
}

template <>
void MpiManager::bcast<size_t>(size_t* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_UNSIGNED_LONG, root, com);
}

template <>
void MpiManager::bcast<void*>(void** data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_BYTE, root, com);
}


void MpiManager::broadcastValues(const int size, int *data) const {
	int mainProc = Parallel::mpiMgr().MainProcessor;
	bcast(data, size, mainProc, MPI_COMM_WORLD);
}

void MpiManager::broadcastValues(const int size, double *data) const {
	int mainProc = Parallel::mpiMgr().MainProcessor;
	bcast(data, size, mainProc, MPI_COMM_WORLD);
}

RNG* MpiManager::getPRNG() const {
	return prng;
}

const int MpiManager::getRank() const {
	return myRank;
}

const int MpiManager::getNProc() const {
	return nProc;
}

double MpiManager::getStartTime() const {
	return startTime;
}

double MpiManager::getElapsedTime() const {
	return MPI_Wtime() - startTime;
}

bool MpiManager::isMainProcessor() const {
	return myRank == MainProcessor;
}

void MpiManager::setSeedPRNG(unsigned long int aSeed) {
	prng->setSeed(aSeed);
}

} /* namespace pMCMC */
