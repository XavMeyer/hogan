//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MLManager.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "MLManager.h"

namespace Parallel {

MLManager::MLManager() {

	roleParLik = None;
	roleGradient = None;
	roleML = None;

}

MLManager::~MLManager() {
}

void MLManager::init(const bool sharedMem, const size_t nParallelLik, const size_t nGradient) {

	if(hasActiveManager()){
		std::cerr << "[Error] in MCMCManager." << std::endl;
		std::cerr << "Only one ML/MCMC parallel manager should be active at a time." << std::endl;
		std::cerr << "Deinit (using .deinit()) the obsolete manager before creating a new one." << std::endl;
		abort();
	}

	// Define layer group sizes
	std::vector<size_t> procPerGroup(4, 1);
	procPerGroup[PARALLEL_LIK_LAYER_ID] = nParallelLik;
	procPerGroup[GRADIENT_LAYER_ID] = nGradient;
	procPerGroup[ML_LAYER_ID] = 1;

	// Check that MPI mgr is init'd
	int myRank = Parallel::mpiMgr().getRank();
	int nProc = Parallel::mpiMgr().getNProc();

	if(myRank < 0 || nProc < 0 ) {
		std::cerr << "[Error] in MCMCManager::init(...);" << std::endl;
		std::cerr << "Parallel::mpiMgr().init(...) must be called prior to this." << std::endl;
		abort();
	}

	// Create layer communicators
	ptrBT.reset(new BaseTopology(myRank, nProc, sharedMem, procPerGroup));
	Parallel::pllMgr().comLayer = ptrBT->getCommunicationLayer(PARALLEL_LIK_LAYER_ID);

	// Define roles
	defineRoles();

	setActive(true);
}

void MLManager::deinit() {

	setActive(false);

	// Deinit roles
	Parallel::pllMgr().roleParLik = None;
	roleGradient = None;
	roleML = None;

	// Deinit layers
	Parallel::pllMgr().comLayer = NULL;
	ptrBT.reset();

}

bool MLManager::checkRoleGradient(role_t aRole)  const {
	return aRole == roleGradient;
}

bool MLManager::checkRoleML(role_t aRole)  const {
	return aRole == roleML;
}

bool MLManager::isMainProcessor() const {
	return roleML == Manager || (roleML == Sequential && (roleGradient == Manager || roleGradient == Sequential));
}

int MLManager::getMyRankGradient() const {
	return ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyLocalRank();
}

int MLManager::getNProcGradient()  const {
	assert(ptrBT);
	return ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalNProc();
}

void MLManager::broadcastValuesWrk(bool &isFinished, std::vector<double> &values) const {
	gradient_signal_t signal;
	broadcastValuesWrk(signal, values);
	isFinished = signal == TERMINATE;
}

void MLManager::broadcastValuesMgr(bool isFinished, const std::vector<double> &values) const {
	gradient_signal_t signal = isFinished ? TERMINATE : COMPUTE;
	broadcastValuesMgr(signal, values);
}


void MLManager::broadcastValuesWrk(gradient_signal_t &signal, std::vector<double> &values) const {
	assert(checkRoleGradient(Worker));
	std::vector<double> buffer(values.size()+1);

	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();
	Parallel::mpiMgr().bcast(buffer.data(), buffer.size(), myGradManagerLocalRank, myGradCom);

	if(static_cast<int>(buffer[0]) == static_cast<int>(COMPUTE)) {
		signal = COMPUTE;
	} else if(static_cast<int>(buffer[0]) == static_cast<int>(LB_COMPUTE)) {
		signal = LB_COMPUTE;
	} else if(static_cast<int>(buffer[0]) == static_cast<int>(TERMINATE)) {
		signal = TERMINATE;
	} else {
		std::cerr << "[Error] in void MLManager::broadcastValuesWrk(...) const;" << std::endl;
		std::cerr << "Unknown type of signal." << std::endl;
		MPI_Abort(MPI_COMM_WORLD, 0);
	}

	for(size_t iV=0; iV<values.size();++iV) {
		values[iV] = buffer[iV+1];
	}
}

void MLManager::broadcastValuesMgr(gradient_signal_t signal, const std::vector<double> &values) const {
	assert(checkRoleGradient(Manager));
	std::vector<double> buffer(values.size()+1);

	buffer[0] = static_cast<int>(signal);
	for(size_t iV=0; iV<values.size();++iV) {
		buffer[iV+1] = values[iV];
	}

	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();
	Parallel::mpiMgr().bcast(buffer.data(), buffer.size(), myGradManagerLocalRank, myGradCom);
}

void MLManager::broadcastAssignement(std::vector<size_t> &assignement) const {
	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();
	Parallel::mpiMgr().bcast(assignement.data(), assignement.size(), myGradManagerLocalRank, myGradCom);
}


void MLManager::gatherLikelihoods(std::vector<double> &localLikelihoods, std::vector<double> &gatherResults) const {
	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();
	MPI_Gather(localLikelihoods.data(), localLikelihoods.size(), MPI_DOUBLE,
			   gatherResults.data(), localLikelihoods.size(), MPI_DOUBLE,
			   myGradManagerLocalRank, myGradCom);
}

void MLManager::gathervDoubleMgr(std::vector<double> &localLikelihoods, std::vector<int> &recvCount, std::vector<double> &gatherResults) const {
	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();

	std::vector<int> displs(recvCount.size(), 0);

	for(size_t i=1; i<recvCount.size(); ++i) {
		displs[i] = displs[i-1] + recvCount[i-1];
	}

	MPI_Gatherv(localLikelihoods.data(), localLikelihoods.size(), MPI_DOUBLE,
				gatherResults.data(), recvCount.data(), displs.data(), MPI_DOUBLE,
				myGradManagerLocalRank, myGradCom);
}

void MLManager::gathervDoubleWrk(std::vector<double> &localLikelihoods) const {
	int myGradManagerLocalRank = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myGradCom = ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID)->getLocalCommMPI();
	double *dummyDblPtr = NULL;
	int *dummyIntPtr = NULL;

	MPI_Gatherv(localLikelihoods.data(), localLikelihoods.size(), MPI_DOUBLE,
				dummyDblPtr, dummyIntPtr, dummyIntPtr, MPI_DOUBLE,
				myGradManagerLocalRank, myGradCom);
}



void MLManager::defineRoles() {

	Parallel::pllMgr().roleParLik = defineRole(ptrBT->getCommunicationLayer(PARALLEL_LIK_LAYER_ID));
	roleGradient = defineRole(ptrBT->getCommunicationLayer(GRADIENT_LAYER_ID));
	roleML = defineRole(ptrBT->getCommunicationLayer(ML_LAYER_ID));

}


} /* namespace Parallel */
