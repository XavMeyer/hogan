//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MpiMgr.h
 *
 *  Created onJ': 30 sept. 2013
 *      Author: meyerx
 */

#ifndef MPIMGR_H_
#define MPIMGR_H_

#include "../RNG/RNG.h"

#include <stdlib.h>
#include <mpi.h>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

namespace Parallel {

class MpiManager : private Uncopyable {
public:
	static const int MainProcessor;

private:
	int myRank, nProc;
	double startTime;

	RNG* prng;

	friend MpiManager& mpiMgr();

public:

	void abort(int errCode=0){
		MPI_Abort(MPI_COMM_WORLD, errCode);
	}

	void print(const string &str){
		cout << "[" << myRank << "] " << str << endl;
	}

	void init(int *argc, char ***argv);

	RNG* getPRNG() const;
	const int getRank() const;
	const int getNProc() const;

	double getStartTime() const;
	double getElapsedTime() const;

	bool isMainProcessor() const;

	void setSeedPRNG(unsigned long int aSeed);

	void broadcastValues(const int size, int *data) const;
	void broadcastValues(const int size, double *data) const;

	template <typename T>
	void send(T* data, const int size, const int dest, const int tag, const MPI_Comm com) const;

	template <typename T>
	void recv(T* data, const int size, const int dest, const int tag, const MPI_Comm com) const;

	template <typename T>
	void bcast(T* data, const int size, const int root, const MPI_Comm com) const;

	template <typename T>
	void exchangeAll(size_t nProc, int mySize, T* myData, int* sizes, T* data, const MPI_Comm com) const;

private:
	// Defined in the body
	MpiManager();
	~MpiManager();
	// Not defined to avoid call
	MpiManager(const MpiManager&);
	MpiManager& operator=(const MpiManager&);

};

inline MpiManager& mpiMgr() {
    static MpiManager instance;
    return instance;
}

} /* namespace pMCMC */
#endif /* MPIMGR_H_ */
