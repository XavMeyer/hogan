//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParallelLikManager.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef PARALLELLIKMANAGER_H_
#define PARALLELLIKMANAGER_H_

#include "MpiManager.h"
#include "BaseManager.h"
#include "../Topology/BaseTopology.h"

namespace Parallel {

class MCMCManager;
class MLManager;

class ParallelLikManager : public BaseManager {
public:
	enum task_t {INIT=0, PROCESS=1, STOP=2, NONE=100};
	enum tag_t {SIGNAL_PARENT_TAG=0, DEFAULT_TAG=100};

public:
	bool checkRoleLikelihood(role_t aRole) const;

	int getMyLocalRank() const;
	int getMyLocalManagerRank() const;

	size_t getNProc() const;;

private:

	role_t roleParLik;
	const CommunicationLayer *comLayer;

	task_t codeToTask(const size_t code) const;

private:

	static const size_t MAX_CONCURRENT_BUFFERED_MESSAGES;

	// Defined in the body
	ParallelLikManager();
	~ParallelLikManager();
	// Not defined to avoid call
	ParallelLikManager(const ParallelLikManager&);
	ParallelLikManager& operator=(const ParallelLikManager&);

	friend ParallelLikManager& pllMgr();
	friend class MCMCManager;
	friend class MLManager;

};

inline ParallelLikManager& pllMgr() {
    static ParallelLikManager instance;
    return instance;
}


} /* namespace Parallel */

#endif /* PARALLELLIKMANAGER_H_ */
