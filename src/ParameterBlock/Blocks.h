//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Blocks.h
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKS_H_
#define BLOCKS_H_

#include "Utils/Uncopyable.h"
#include "ParameterBlock/Block.h"

#include <stdexcept>
#include <vector>
#include <set>

using namespace std;

namespace ParameterBlock {

class Blocks {
public:
	Blocks();
	~Blocks();

	void addBlock(Block::sharedPtr_t aBlock);
	Block::sharedPtr_t getBlock(size_t aInd);
	const Block::sharedPtr_t getBlock(size_t aInd) const;
	Block::sharedPtr_t getLastBlock();
	void removeBlocks(std::vector<size_t> blockId);

	std::vector<size_t> getOptimisableBlocksIdx();
	std::vector<size_t> getOptimisableParametersIdx();

	size_t size() const;
	size_t nbUniqueParam() const;

	const vector<double>& frequencies() const;
	const double frequenciesSum() const;

	void changeFreqs(const vector<double> &newFreqs) const;

	void reset();

private:

	typedef vector<Block::sharedPtr_t> blocks_t;
	typedef vector<Block::sharedPtr_t>::iterator itBlocks_t;
	typedef vector<Block::sharedPtr_t>::const_iterator constItBlocks_t;

	mutable double m_freqSum;
	mutable vector<double> m_frequencies;
	blocks_t m_blocks;

};

} // namespace ParameterBlock

#endif /* BLOCKS_H_ */
