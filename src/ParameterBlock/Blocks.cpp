//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Blocks.cpp
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#include "Blocks.h"

namespace ParameterBlock {

Blocks::Blocks() : m_frequencies(), m_blocks() {
	m_freqSum = 0.0;
}

Blocks::~Blocks() {
}

void Blocks::addBlock(Block::sharedPtr_t aBlock){
	m_blocks.push_back(aBlock);
	m_blocks.back()->setId(m_blocks.size()-1);

	m_freqSum += aBlock->getTotalFreq();
	m_frequencies.push_back(aBlock->getTotalFreq());
}

Block::sharedPtr_t Blocks::getLastBlock() {
	if(size() == 0) {
		throw out_of_range("Blocks.");
	}

	return m_blocks.back();
}

Block::sharedPtr_t Blocks::getBlock(size_t aInd) {
	if(aInd >= size()) {
		throw out_of_range("Blocks.");
	}

	return m_blocks[aInd];
}

const Block::sharedPtr_t Blocks::getBlock(size_t aInd) const {
	if(aInd >= size()) {
		throw out_of_range("Blocks.");
	}

	return m_blocks[aInd];
}

void Blocks::removeBlocks(std::vector<size_t> blockId) {
	std::sort(blockId.begin(), blockId.end());
	if(blockId.back() >= size()) {
		throw out_of_range("Blocks.");
	}

	for(int iB=blockId.size()-1; iB >= 0; --iB) {
		m_blocks.erase(m_blocks.begin()+blockId[iB]);
		m_freqSum -= m_frequencies[blockId[iB]];
		m_frequencies.erase(m_frequencies.begin()+blockId[iB]);
	}

	for(size_t iB=0; iB<m_blocks.size(); ++iB) {
		m_blocks[iB]->setId(iB);
	}
}

std::vector<size_t> Blocks::getOptimisableBlocksIdx() {
	std::vector<size_t> blocksIdx;
	for(size_t iB=0; iB<size(); ++iB) {
		if(m_blocks[iB]->isSupportingBlockOptimisation()) {
			blocksIdx.push_back(iB);
		}
	}
	return blocksIdx;
}

std::vector<size_t> Blocks::getOptimisableParametersIdx() {
	std::vector<size_t> blocksIdx = getOptimisableBlocksIdx();
	std::vector<size_t> paramIdx;
	for(size_t iB=0; iB<blocksIdx.size(); ++iB) {
		const std::vector<size_t> &pInd = m_blocks[blocksIdx[iB]]->getPIndices();
		paramIdx.insert(paramIdx.end(), pInd.begin(), pInd.end());
	}

	std::sort( paramIdx.begin(), paramIdx.end() );
	paramIdx.erase( std::unique( paramIdx.begin(), paramIdx.end() ), paramIdx.end() );

	return paramIdx;
}

size_t Blocks::size() const {
	return m_blocks.size();
}

size_t Blocks::nbUniqueParam() const {
	set<int> tmp;
	for(constItBlocks_t itB=m_blocks.begin(); itB != m_blocks.end(); ++itB){
		tmp.insert((*itB)->getPIndices().begin(), (*itB)->getPIndices().end());
	}

	return tmp.size();
}

const vector<double>& Blocks::frequencies() const {
	return m_frequencies;
}

const double Blocks::frequenciesSum() const {
	return m_freqSum;
}

void Blocks::changeFreqs(const vector<double> &newFreqs) const {
	m_freqSum = 0.;
	for(size_t iF=0; iF<newFreqs.size(); ++iF){
		m_frequencies[iF] = newFreqs[iF];
		m_freqSum += newFreqs[iF];
	}
}

void Blocks::reset() {
	m_blocks.clear();
	m_freqSum = 0.;
	m_frequencies.clear();
}

} // namespace ParameterBlock

