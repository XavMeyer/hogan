//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MVGaussianWindowPM.cpp
 *
 *  Created on: 29 april 2013
 *      Author: meyerx
 */

#include "MVGaussianWindowPM.h"

namespace ParameterBlock {

MVGaussianWindowPM::MVGaussianWindowPM(size_t aN, const double aSigma, const Parameters &aParams) :
		BlockModifier(true, aParams) {
	errorState = false;
	n = aN;
	t= 0;

	sigma = new double[n*n];
	cholesky = new double[n*n];

	buffPtr = n;
	buff = new double[n];

	double tmpSigma[n*n];
	for(size_t i=0; i<n; ++i){
		for(size_t j=0; j<n; ++j){
			if(i!=j){
				tmpSigma[i+j*n] = 0.;
			} else {
				tmpSigma[i+j*n] = aSigma;
			}
		}
	}

	initSigma(tmpSigma);
}

MVGaussianWindowPM::MVGaussianWindowPM(size_t aN, const double* aSigma, const Parameters &aParams) : BlockModifier(true, aParams) {
	errorState = false;
	n = aN;

	sigma = new double[n*n];
	cholesky = new double[n*n];

	buffPtr = n;
	buff = new double[n];

	initSigma(aSigma);

}

MVGaussianWindowPM::~MVGaussianWindowPM() {
	delete sigma;
	delete cholesky;
	delete buff;
}

void MVGaussianWindowPM::initSigma(const double* aSigma){

	errorState = false;

	// Copy sigma (going to be overwritten by dgeev)
	for(size_t i=0; i<n*n; ++i){
		sigma[i] = aSigma[i];
	}

	Eigen::Map<Eigen::MatrixXd> mappedS(sigma, n, n);
	Eigen::LLT<Eigen::MatrixXd> choleskyDecomp(mappedS);

	if(choleskyDecomp.info() == Eigen::NumericalIssue || choleskyDecomp.info() == Eigen::NoConvergence) {
		cerr << "void MVGaussianWindowPM::initSigma(const double* aSigma);" << endl;
		cerr << "Warning : error in cholesky decomposition." << endl;
	} else if(choleskyDecomp.info() == Eigen::InvalidInput) {
		cerr << "void MVGaussianWindowPM::initSigma(const double* aSigma);" << endl;
		cerr << "Warning : error in sigma input." << endl;
	} else { // SUCCESS
		Eigen::Map<Eigen::MatrixXd> mappedC(cholesky, n, n);
		mappedC = choleskyDecomp.matrixL();
	}


}


void MVGaussianWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {

	if(pInd.size() != n) {
		std::cerr << "void MVGaussianWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const;" << std::endl;
		std::cerr << "Error : Number of parameter doesn't correspond to the size of the move." << std::endl;
		abort();
	}

	vector<double> buffer, X;
	for(size_t iB=0; iB < pInd.size(); ++iB){
		double val = sample.getDoubleParameter(pInd[iB]);
		X.push_back(val);
	}

	buffer = Parallel::mpiMgr().getPRNG()->genMultivariateGaussian(n, cholesky);

	for(size_t i=0; i<pInd.size(); ++i){
		X[i] += buffer[i];
	}

	// Check if in bound
	bool inBound = true;
	for(size_t i=0; i<pInd.size(); ++i){
		const int iP = pInd[i];
		if(params.requireReflection(iP) && !params.isInBound(iP, X[i])){
			inBound = false;
			break;
		}
	}

	if(inBound) { // If inBound we set result directly
		for(size_t iB=0; iB < pInd.size(); ++iB){
			sample.setDoubleParameter(pInd[iB], X[iB]);
		}
		return; // quitting
	}

	// If not, we look for reflection (costly)
	if((t%2)==0){
		for(size_t iP=0; iP<pInd.size(); ++iP){ // loop over direction
			EllipsoidReflection er(X, buffer, params, pInd);
			X = er.getReflectedValues();
		}
	} else {
		for(int iP=pInd.size()-1; iP>=0; --iP){ // loop over direction
			EllipsoidReflection er(X, buffer, params, pInd);
			X = er.getReflectedValues();
		}
	}
	++t;

	for(size_t iB=0; iB < pInd.size(); ++iB){
		sample.setDoubleParameter(pInd[iB], X[iB]);
	}

}

double MVGaussianWindowPM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double q = 1.0;
	cerr << "double MVGaussianWindowPM::getUpdateProbability(const Block &block, const Sample &fromSample, const Sample &toSample) const;" << endl;
	cerr << "Error : not yet implemented" << endl;
	abort();
	return q;
}

double MVGaussianWindowPM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return 1.0;
}

void MVGaussianWindowPM::setWindowSize(const vector<double> &aWinSize){
	initSigma(const_cast<double *>(&aWinSize[0]));
}

vector<double> MVGaussianWindowPM::getWindowSize() const {
	vector<double> vecSigma(sigma, sigma+n*n);
	return vecSigma;
}

} // namespace ParameterBlock

