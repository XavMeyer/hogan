//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BlockModificator.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKMODIFICATOR_H_
#define BLOCKMODIFICATOR_H_

#include "Utils/Uncopyable.h"
#include "Sampler/Samples/Sample.h"
#include "Model/Parameter/Parameters.h"

#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

#include <boost/shared_ptr.hpp>

#include <vector>
using namespace std;

namespace ParameterBlock {

using ::Sampler::Sample;
using ::StatisticalModel::Parameters;
namespace TR = MolecularEvolution::TreeReconstruction;

class BlockModifier : private Uncopyable {
public:
	typedef boost::shared_ptr<BlockModifier> sharedPtr_t;
	enum bm_name_t {NONE=0, UNIFORM=1, GAUSSIAN_1D=2, GAUSSIAN_MVN=3, PCA_MVN=4, MULTIPLIER=5, ESPR=6, STNNI=7};

public:
	BlockModifier(const bool aSymmetric, const Parameters &aParam);
	virtual ~BlockModifier();

	bool isSymmetric() const {
		return symmetric;
	}

	virtual void updateParameters(const vector<size_t> &pInd, Sample &sample) const = 0;
	virtual double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const = 0;
	virtual double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const = 0;

	virtual void setWindowSize(const vector<double> &aWinSize) = 0;
	virtual vector<double> getWindowSize() const = 0;

	virtual bm_name_t getName() const = 0;

	virtual void moveAccepted(const bool isLocalProposal) {}
	virtual void moveRejected(const bool isLocalProposal) {}
	virtual void moveThrown(const bool isLocalProposal) {}

	static sharedPtr_t createUniformWindow(const double aWinSize, const Parameters &aParams);
	static sharedPtr_t createGaussianWindow(const double aSigma, const Parameters &aParams);
	static sharedPtr_t createMVGaussianWindow(const size_t n, const double aSigma, const Parameters &aParams);
	static sharedPtr_t createMVGaussianWindow(const size_t n, const double *aSigma, const Parameters &aParams);
	static sharedPtr_t createPCAWindow(const size_t n, const vector<double> &aSigma, const Parameters &aParams);
	static sharedPtr_t createMultiplierWindow(const double aB, const Parameters &aParams);

	static BlockModifier::sharedPtr_t createESPRBM(const double aP,
			TR::TreeManager::sharedPtr_t aTM, const Parameters &aParams);
	static BlockModifier::sharedPtr_t createSTNNIBM(TR::TreeManager::sharedPtr_t aTM,
			const Parameters &aParams);

	static sharedPtr_t createDefaultBM(const Parameters &aParam);

protected:
	const bool symmetric;
	const Parameters &params;

	void defaultReflection(const vector<size_t> &pInd, Sample &sample) const;
	void defDblReflection(const size_t pId, Sample &sample) const;

};

} // namespace ParameterBlock

#endif /* BLOCKMODIFICATOR_H_ */
