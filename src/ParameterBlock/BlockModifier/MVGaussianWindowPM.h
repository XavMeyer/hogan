//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MVGaussianWindowPM.h
 *
 *  Created on: 29 april 2014
 *      Author: meyerx
 */

#ifndef MVGAUSSIANWINDOWPM_H_
#define MVGAUSSIANWINDOWPM_H_

#include "BlockModifierInterface.h"
#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"
#include "EllipsoidReflection.h"

#include <boost/math/distributions.hpp>

namespace ParameterBlock {

class MVGaussianWindowPM: public BlockModifier {
public:
	MVGaussianWindowPM(size_t n, const double aSigma, const Parameters &aParams);
	MVGaussianWindowPM(size_t n, const double *aSigma, const Parameters &aParams);
	virtual ~MVGaussianWindowPM();

	void updateParameters(const vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const vector<double> &aWinSize);
	vector<double> getWindowSize() const;

	bm_name_t getName() const {return GAUSSIAN_MVN;}

private:

	bool errorState;
	size_t n;
	double *sigma, *cholesky;
	mutable uint t;

	//mutable uint cntCorrel;
	mutable size_t buffPtr;
	mutable double *buff;

	void initSigma(const double* aSigma);

};

} // namespace ParameterBlock

#endif /* MVGAUSSIANWINDOWPM_H_ */
