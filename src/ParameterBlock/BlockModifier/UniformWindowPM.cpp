//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * UniformWindowPM.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "UniformWindowPM.h"

namespace ParameterBlock {

UniformWindowPM::UniformWindowPM(double aWinSize, const Parameters &aParams) : BlockModifier(true, aParams), winSize(aWinSize) {
}

UniformWindowPM::~UniformWindowPM() {
}

void UniformWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {
	// For each parameters
	for(size_t iP=0; iP < pInd.size(); ++iP){
		double incr = Parallel::mpiMgr().getPRNG()->genUniformDbl();
		incr -= 0.5;
		incr *= winSize;
		sample.incParameter(pInd[iP], incr);
		BlockModifier::defaultReflection(pInd, sample);
	}
}

double UniformWindowPM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double q = 1.0;
	for(size_t iB=0; iB<pInd.size(); ++iB){
		q *= 1./winSize;
	}
	return q;
}

double UniformWindowPM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return 1.0;
}

void UniformWindowPM::setWindowSize(const vector<double> &aWinSize){
	winSize = aWinSize[0];
}

} // namespace ParameterBlock

