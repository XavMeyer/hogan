//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchHelper.h
 *
 * @date Dec 26, 2015
 * @author meyerx
 * @brief
 */
#ifndef BRANCHHELPER_H_
#define BRANCHHELPER_H_

#include <algorithm>
#include <vector>
#include <map>

#include "Sampler/Samples/Sample.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

namespace ParameterBlock {

namespace TR = MolecularEvolution::TreeReconstruction;

class BranchHelper {
public:
	BranchHelper(TR::Tree::sharedPtr_t aTree);
	~BranchHelper();

	double getBranchLength(const TR::TreeNode *aNode, const Sampler::Sample &aSample) const ;
	void setBranchLength(double aValue, const TR::TreeNode *aNode, Sampler::Sample &aSample) const ;
	double generateMultiplier(const double lambda) const;

	double scaleBranch(const double lambda, const size_t idNodeA, const size_t idNodeB,
			TR::Tree::sharedPtr_t aTree, Sampler::Sample &aSample) const ;

private:
	typedef std::map<size_t, size_t> mapNodeToBranch_t;
	mapNodeToBranch_t mapNodeTobranch;

	void initMapNodeToBranch(TR::Tree::sharedPtr_t aTree);
};

} /* namespace ParameterBlock */

#endif /* BRANCHHELPER_H_ */
