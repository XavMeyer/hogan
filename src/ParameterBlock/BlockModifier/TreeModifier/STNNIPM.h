//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file STNNIPM.h
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef STNNIPM_H_
#define STNNIPM_H_

#include "../BlockModifierInterface.h"
#include "BranchHelper.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"

namespace ParameterBlock {

namespace TR = MolecularEvolution::TreeReconstruction;

class STNNI_PM: public BlockModifier {
public:
	STNNI_PM(TR::TreeManager::sharedPtr_t aTM, const Parameters &aParam);
	~STNNI_PM();

	void updateParameters(const vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const vector<double> &aWinSize);
	vector<double> getWindowSize() const;

	void moveAccepted(const bool isLocalProposal);
	void moveRejected(const bool isLocalProposal);
	void moveThrown(const bool isLocalProposal);

	bm_name_t getName() const {return STNNI;}

private:
	static const bool SCALE_BRANCHES;
	static const double LAMBDA;

	mutable double m;
	//mutable TR::Move::sharedPtr_t lastMove;
	mutable TR::TreeManager::sharedPtr_t treeManager;
	mutable TR::STNNI stnni;

	BranchHelper bh;
};

} /* namespace ParameterBlock */

#endif /* STNNIPM_H_ */
