//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ESPRPM.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "ESPRPM.h"

namespace ParameterBlock {

const bool ESPR_PM::SCALE_BRANCHES = true;
const double ESPR_PM::LAMBDA = 2.*log(1.3);

ESPR_PM::ESPR_PM(const double aP, TR::TreeManager::sharedPtr_t aTM, const Parameters &aParam) :
		BlockModifier(true, aParam), p(aP), treeManager(aTM), bh(treeManager->getCurrentTree()) {
	m = 1.;
}

ESPR_PM::~ESPR_PM() {
}

void ESPR_PM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));

	long int cTreeH = sample.getIntParameter(pInd.front());
	TR::Tree::sharedPtr_t tree = treeManager->getTree(cTreeH);
	TR::Move::sharedPtr_t lastMove = espr.proposeNewTree(p, tree);
	treeManager->setProposedMove(lastMove);
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR propose -> : " << lastMove->getFromH() << " to " << lastMove->getToH() << std::endl; // FIXME
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] ESPR curTree -> : " << tree->getHashKey() << std::endl; // FIXME
	sample.setIntParameter(pInd.front(), lastMove->getToH());

	if(SCALE_BRANCHES) {
		m = 1.0;
		 // Branch to explore and to graft
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M1],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_M2], tree, sample);
		// Branch to explore and "internal" (direction of movment) grafted branch
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::ESPR::NODE_M2],
				lastMove->getInvolvedNode()[TR::ESPR::NODE_E1], tree, sample);
	}
}

double ESPR_PM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));

	size_t nEdge = treeManager->getProposedMove()->getNEdgeDistance();
	TR::ESPR::moveInfoESPR_t moveInfo = static_cast<TR::ESPR::moveInfoESPR_t>(treeManager->getProposedMove()->getMoveInfo());
	assert(!SCALE_BRANCHES); // FIXME UNSURE HOW TO DEAL WITH BRANCH SCALING

	double prob = 0.0;
	if(moveInfo == TR::ESPR::BOTH_CONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge);
	} else if(moveInfo == TR::ESPR::BOTH_UNCONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge)*(1-p);
	} else if(moveInfo == TR::ESPR::CONSTRAINED_TO_UNCONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge);
	} else if(moveInfo == TR::ESPR::UNCONSTRAINED_TO_CONSTRAINED) {
		prob = pow(0.5*p, (double)nEdge)*(1-p);
	}
	return prob;
}

double ESPR_PM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));

	double prob = treeManager->getProposedMove()->getProbability();

	if(SCALE_BRANCHES) {
		prob *= m;
	}

	return prob;
}

void ESPR_PM::setWindowSize(const std::vector<double> &aWinSize) {
	p = aWinSize[0];
}

std::vector<double> ESPR_PM::getWindowSize() const {
	return std::vector<double>(1, p);
}

void ESPR_PM::moveAccepted(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->acceptProposedMove();
	} else {
		treeManager->acceptRemoteMove();
	}
}

void ESPR_PM::moveRejected(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

void ESPR_PM::moveThrown(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

} /* namespace ParameterBlock */
