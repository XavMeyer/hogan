//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchHelper.cpp
 *
 * @date Dec 26, 2015
 * @author meyerx
 * @brief
 */
#include "BranchHelper.h"

namespace ParameterBlock {

BranchHelper::BranchHelper(TR::Tree::sharedPtr_t aTree) {
	initMapNodeToBranch(aTree);
}

BranchHelper::~BranchHelper() {
}

double BranchHelper::getBranchLength(const TR::TreeNode *aNode, const Sampler::Sample &aSample)  const {
	size_t offsetDblSample = aSample.dblValues.size() - mapNodeTobranch.size();
	mapNodeToBranch_t::const_iterator it = mapNodeTobranch.find(aNode->getId());
	assert(it != mapNodeTobranch.end());
	size_t iBranch = it->second;
	return aSample.dblValues[offsetDblSample+iBranch];
}

void BranchHelper::setBranchLength(double aValue, const TR::TreeNode *aNode, Sampler::Sample &aSample)  const {
	size_t offsetDblSample = aSample.dblValues.size() - mapNodeTobranch.size();
	mapNodeToBranch_t::const_iterator it = mapNodeTobranch.find(aNode->getId());
	assert(it != mapNodeTobranch.end());
	size_t iBranch = it->second;
	aSample.dblValues[offsetDblSample+iBranch] = aValue;
}

double BranchHelper::generateMultiplier(const double lambda) const {
	double u = Parallel::mpiMgr().getPRNG()->genUniformDbl();
	return exp(lambda*(u-0.5));
}

double BranchHelper::scaleBranch(const double lambda, const size_t idNodeA, const size_t idNodeB,
		TR::Tree::sharedPtr_t aTree, Sampler::Sample &aSample) const {
	// Get random multiplier
	double m = generateMultiplier(lambda);

	// Get node and define child node
	TR::TreeNode *nodeA = aTree->getNode(idNodeA);
	TR::TreeNode *nodeB = aTree->getNode(idNodeB);
	TR::TreeNode *child = NULL;
	if(nodeA->getParent() == nodeB) {
		child = nodeA;
	} else if(nodeB->getParent() == nodeA) {
		child = nodeB;
	}
	assert(child != NULL);

	// Apply multiplier
	double bl = getBranchLength(child, aSample);
	bl *= m;
	setBranchLength(bl, child, aSample);

	// return m
	return m;
}

void BranchHelper::initMapNodeToBranch(TR::Tree::sharedPtr_t aTree) {
	assert(aTree != NULL);
	std::vector<size_t> nodesIdx;

	// Add internal nodes but root
	const std::vector<TR::TreeNode*> &intNodes = aTree->getInternals();
	for(size_t iN=0; iN<intNodes.size(); ++iN) {
		if(intNodes[iN] != aTree->getRoot()) {
			nodesIdx.push_back(intNodes[iN]->getId());
		}
	}

	// Add terminal nodes but root
	const std::vector<TR::TreeNode*> &termNodes = aTree->getTerminals();
	for(size_t iN=0; iN<termNodes.size(); ++iN) {
		if(termNodes[iN] != aTree->getRoot()) {
			nodesIdx.push_back(termNodes[iN]->getId());
		}
	}

	// Sort nodes by ID as in likelihood
	std::sort(nodesIdx.begin(), nodesIdx.end());

	// Build the mapping
	for(size_t iN=0; iN<nodesIdx.size(); ++iN)  {
		mapNodeTobranch[nodesIdx[iN]] = iN;
	}

}



} /* namespace ParameterBlock */
