//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file STNNIPM.cpp
 *
 * @date Nov 11, 2015
 * @author meyerx
 * @brief
 */
#include "STNNIPM.h"

namespace ParameterBlock {

const bool STNNI_PM::SCALE_BRANCHES = true;
const double STNNI_PM::LAMBDA = 2.*log(1.3);

STNNI_PM::STNNI_PM(TR::TreeManager::sharedPtr_t aTM, const Parameters &aParam) :
		BlockModifier(true, aParam), treeManager(aTM), bh(treeManager->getCurrentTree()) {
	m = 1.;
}

STNNI_PM::~STNNI_PM() {
}

void STNNI_PM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));

	long int cTreeH = sample.getIntParameter(pInd.front());
	TR::Tree::sharedPtr_t tree = treeManager->getTree(cTreeH);
	TR::Move::sharedPtr_t lastMove = stnni.proposeNewTree(1., tree);
	treeManager->setProposedMove(lastMove);
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] STNNI propose -> : " << lastMove->getFromH() << " to " << lastMove->getToH() << std::endl; // FIXME
	//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] STNNI curTree -> : " << tree->getHashKey() << std::endl; // FIXME
	sample.setIntParameter(pInd.front(), lastMove->getToH());


	if(SCALE_BRANCHES) {
		m = 1.0;
		// Selected Branch
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::STNNI::NODE_N1],
				lastMove->getInvolvedNode()[TR::STNNI::NODE_N2], tree, sample);
		// Node C/D and N1
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::STNNI::NODE_N1],
				lastMove->getInvolvedNode()[TR::STNNI::NODE_OTHER], tree, sample);
		// Node B and N2
		m *= bh.scaleBranch(LAMBDA, lastMove->getInvolvedNode()[TR::STNNI::NODE_N2],
				lastMove->getInvolvedNode()[TR::STNNI::NODE_B], tree, sample);
	}

}

double STNNI_PM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));
	assert(!SCALE_BRANCHES); // FIXME UNSURE HOW TO DEAL WITH BRANCH SCALING
	return 1.0;
}

double STNNI_PM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	assert(pInd.size() == 1 && params.isInt(pInd.front()));
	return m;
}

void STNNI_PM::setWindowSize(const std::vector<double> &aWinSize) {

}

std::vector<double> STNNI_PM::getWindowSize() const {
	return std::vector<double>(1,0.);
}

void STNNI_PM::moveAccepted(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->acceptProposedMove();
	} else {
		treeManager->acceptRemoteMove();
	}
}

void STNNI_PM::moveRejected(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

void STNNI_PM::moveThrown(const bool isLocalProposal) {
	if(isLocalProposal) { // I created this move
		treeManager->rejectProposedMove();
	}
}

} /* namespace ParameterBlock */
