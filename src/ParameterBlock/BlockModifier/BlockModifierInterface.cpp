//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BlockModificator.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "BlockModifierInterface.h"
#include "UniformWindowPM.h"
#include "GaussianWindowPM.h"
#include "MVGaussianWindowPM.h"
#include "PCAWindowPM.h"
#include "MultiplierWindowPM.h"

#include "TreeModifier/ESPRPM.h"
#include "TreeModifier/STNNIPM.h"

namespace ParameterBlock {

BlockModifier::BlockModifier(const bool aSymmetric, const Parameters &aParams) : symmetric(aSymmetric), params(aParams) {

}

BlockModifier::~BlockModifier() {
}

BlockModifier::sharedPtr_t BlockModifier::createUniformWindow(const double aWinSize, const Parameters &aParams){
	return sharedPtr_t(new UniformWindowPM(aWinSize, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createGaussianWindow(const double aSigma, const Parameters &aParams){
	return sharedPtr_t(new GaussianWindowPM(aSigma, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createMVGaussianWindow(const size_t n, const double aSigma, const Parameters &aParams){
	return sharedPtr_t(new MVGaussianWindowPM(n, aSigma, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createMVGaussianWindow(const size_t n, const double *aSigma, const Parameters &aParams){
	return sharedPtr_t(new MVGaussianWindowPM(n, aSigma, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createPCAWindow(const size_t n, const vector<double> &aSigma, const Parameters &aParams){
	return sharedPtr_t(new PCAWindowPM(n, aSigma, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createDefaultBM(const Parameters &aParams){
	return createGaussianWindow(0.5, aParams);
}

BlockModifier::sharedPtr_t BlockModifier::createMultiplierWindow(const double aB, const Parameters &aParams) {
	return sharedPtr_t(new MultiplierWindowPM(aB, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createESPRBM(const double aP,
		TR::TreeManager::sharedPtr_t aTM, const Parameters &aParams) {
	return sharedPtr_t(new ESPR_PM(aP, aTM, aParams));
}

BlockModifier::sharedPtr_t BlockModifier::createSTNNIBM(TR::TreeManager::sharedPtr_t aTM,
		const Parameters &aParams) {
	return sharedPtr_t(new STNNI_PM(aTM, aParams));
}

void BlockModifier::defaultReflection(const vector<size_t> &pInd, Sample &sample) const {

	for(size_t i=0; i<pInd.size(); ++i){ 			// For each parameter
		if(params.requireReflection(pInd[i])) { 	// Check if require reflection
			defDblReflection(pInd[i], sample);
		}
	}
}

void BlockModifier::defDblReflection(const size_t pId, Sample &sample) const {
	uint reflectCnt = 0;
	double min = params.getBoundMin(pId);
	double max = params.getBoundMax(pId);
	double value = sample.getDoubleParameter(pId);

	if(min == max) {
		sample.setDoubleParameter(pId, min);
	} else {
		while(value < min || value > max){
			if(value < min){
				value = 2.*min-value;
			} else if(value > max){
				value = 2.*max-value;
			}
			++reflectCnt;
			if(reflectCnt > 50) {
				value = min+(max-min)/2.;
			}
		}
		params.signalReflections(pId, reflectCnt);
		sample.setDoubleParameter(pId, value);
	}
}

} // namespace ParameterBlock

