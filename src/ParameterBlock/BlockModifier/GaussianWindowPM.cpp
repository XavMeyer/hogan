//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * GaussianWindowPM.cpp
 *
 *  Created on: 24 sept. 2013
 *      Author: meyerx
 */

#include "GaussianWindowPM.h"

namespace ParameterBlock {

GaussianWindowPM::GaussianWindowPM(double aSigma, const Parameters &aParams) : BlockModifier(true, aParams), sigma(aSigma), dist(0., sigma) {
}

GaussianWindowPM::~GaussianWindowPM() {
}

void GaussianWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {
	// For each parameters
	for(size_t iB=0; iB < pInd.size(); ++iB){
		double incr = Parallel::mpiMgr().getPRNG()->genGaussian(sigma);
		sample.incParameter(pInd[iB], incr);
	}
	BlockModifier::defaultReflection(pInd, sample);
}

double GaussianWindowPM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double q = 1.0;
	for(size_t iB=0; iB<pInd.size(); ++iB){
		double diff = toSample.getDoubleParameter(pInd[iB]) - fromSample.getDoubleParameter(pInd[iB]);
		q *= boost::math::pdf(dist, diff);
	}
	return q;
}

double GaussianWindowPM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return 1.0;
}

void GaussianWindowPM::setWindowSize(const vector<double> &aWinSize){
	sigma = aWinSize[0];
	dist = boost::math::normal_distribution<>(0., sigma);
}

} // namespace ParameterBlock

