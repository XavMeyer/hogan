//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MultiplierWindowPM.h
 *
 *  Created on: 15 jan. 2014
 *      Author: meyerx
 */

#ifndef MULTIPLIERWINDOWPM_H_
#define MULTIPLIERWINDOWPM_H_

#include "BlockModifierInterface.h"
#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"

#include <boost/math/distributions.hpp>

namespace ParameterBlock {

class MultiplierWindowPM: public BlockModifier {
public:
	MultiplierWindowPM(double aB, const Parameters &aParams);
	~MultiplierWindowPM();

	void updateParameters(const vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const vector<double> &aWinSize);
	vector<double> getWindowSize() const { return vector<double>(1,lambda); }

	bm_name_t getName() const {return MULTIPLIER;}

private:
	double b, lambda;

};

} // namespace ParameterBlock



#endif /* MULTIPLIERWINDOWPM_H_ */
