//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * PCAWindowPM.cpp
 *
 *  Created on: 29 april 2013
 *      Author: meyerx
 */

#include "PCAWindowPM.h"

#define USE_REVERSIBLE 0

namespace ParameterBlock {

PCAWindowPM::PCAWindowPM(size_t aN, const vector <double> &aS, const Parameters &aParams) : BlockModifier(true, aParams) {
	n = aN;
	s = aS;

	t=0;

	buffPtr = n;
	buff = new double[n];

}

PCAWindowPM::~PCAWindowPM() {
	delete buff;
}

void PCAWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {
	std::vector<double>buffer;

	if(pInd.size() != n) {
		std::cerr << "void PCAWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const;" << std::endl;
		std::cerr << "Error : Number of parameter doesn't correspond to the size of the move." << std::endl;
		abort();
	}

	vector<double> X;
	for(size_t iB=0; iB < pInd.size(); ++iB){
		double val = sample.getDoubleParameter(pInd[iB]);
		X.push_back(val);
	}

#if USE_REVERSIBLE
	const double iSqrt2 = 1./sqrt(2.0);
	buffer = Parallel::mpiMgr().getPRNG()->genStandardNormal(n);
	for(int iP=0; iP<block.size(); ++iP){ // loop over direction
		vector<double> incr;
		for(int iB=0; iB<block.size(); ++iB){ // loop over component
			incr.push_back(iSqrt2*buffer[iP]*s[iP*block.size()+iB]);
		}

		EllipsoidReflection er(X, incr, params, block);
		X = er.getReflectedValues();
	}

	buffer = Parallel::mpiMgr().getPRNG()->genStandardNormal(n);
	for(int iP=block.size()-1; iP>=0; --iP){ // loop over direction
		vector<double> incr;
		for(int iB=0; iB<block.size(); ++iB){ // loop over component
			incr.push_back(iSqrt2*buffer[iP]*s[iP*block.size()+iB]);
		}

		EllipsoidReflection er(X, incr, params, block);
		X = er.getReflectedValues();
	}
#else
	vector<double> result(X);
	// Get a sequence of i.i.d Std Normal distributed number
	buffer = Parallel::mpiMgr().getPRNG()->genStandardNormal(n);

	Eigen::Map<Eigen::MatrixXd> mappedS(const_cast<double*>(&s[0]), n, n);
	Eigen::Map<Eigen::VectorXd> mappedB(buffer.data(), n);
	Eigen::Map<Eigen::VectorXd> mappedR(result.data(), n);

	mappedR = mappedR + mappedS*mappedB;

	// Check if in bound
	bool inBound = true;
	for(size_t i=0; i<pInd.size(); ++i){
		const int iP = pInd[i];
		if(params.requireReflection(iP) && !params.isInBound(iP, result[i])){
			inBound = false;
			break;
		}
	}

	if(inBound) { // If inBound we set result directly
		for(size_t iB=0; iB < pInd.size(); ++iB){
			sample.setDoubleParameter(pInd[iB], result[iB]);
		}
		return; // quitting
	}

	// If not, we look for reflection (costly)
	if((t%2)==0){
		for(size_t iP=0; iP<pInd.size(); ++iP){ // loop over direction
			vector<double> incr;
			for(size_t iB=0; iB<pInd.size(); ++iB){ // loop over component
				incr.push_back(buffer[iP]*s[iP*pInd.size()+iB]);
			}

			EllipsoidReflection er(X, incr, params, pInd);
			X = er.getReflectedValues();
		}
	} else {
		for(int iP=pInd.size()-1; iP>=0; --iP){ // loop over direction
			vector<double> incr;
			for(size_t iB=0; iB<pInd.size(); ++iB){ // loop over component
				incr.push_back(buffer[iP]*s[iP*pInd.size()+iB]);
			}

			EllipsoidReflection er(X, incr, params, pInd);
			X = er.getReflectedValues();
		}
	}
	++t;
#endif

	for(size_t iB=0; iB < pInd.size(); ++iB){
		sample.setDoubleParameter(pInd[iB], X[iB]);
	}
}

double PCAWindowPM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double q = 1.0;
	cerr << "double MVGaussianWindowPM::getUpdateProbability(const Block &block, const Sample &fromSample, const Sample &toSample) const;" << endl;
	cerr << "Error : not yet implemented" << endl;
	abort();
	return q;
}

double PCAWindowPM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	return 1.0;
}

void PCAWindowPM::setWindowSize(const vector<double> &aWinSize){
	s = aWinSize;
}

vector<double> PCAWindowPM::getWindowSize() const {
	return s;
}

} // namespace ParameterBlock

