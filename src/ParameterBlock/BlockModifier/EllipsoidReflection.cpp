//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EllipsoidReflection.cpp
 *
 * @date Nov 7, 2014
 * @author meyerx
 * @brief
 */
#include "EllipsoidReflection.h"

namespace ParameterBlock {

EllipsoidReflection::EllipsoidReflection(const vector<double> &aX, const vector<double> &aV, const Parameters &aParams, const vector<size_t> &aPInd)
	: X(aX), V(aV), params(aParams), pInd(aPInd) {

	nrm = 0;
	reflectionCnt.assign(pInd.size(), 0);
}

EllipsoidReflection::~EllipsoidReflection() {
}

vector<double> EllipsoidReflection::getReflectedValues() {

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "-----------------------------------------------------------------" << std::endl;
	}*/

	// Init new values : X2
	X2 = Utils::Vector::sum(V, X);

	bool hasIntersection = true;
	while(hasIntersection) {
		vector<Intersection> inters = getIntersections();

		if(inters.empty()) {
			hasIntersection = false;
			break;
		} // else

		// Process nrm and unitV and find intersections
		nrm = Utils::Vector::normL2(V);
		unitV = Utils::Vector::unit(nrm, V);

		// Find intersections and sort them by increasing distance
		sortIntersectionByDistance(inters); // require X, X2, unitV, nrm

		/*if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "X : " << Utils::Vector::toString(X) << std::endl;
			std::cout << "Norm of V = " << nrm << std::endl;
			std::cout << "Norm of W = " << inters.front().dist << std::endl;
			std::cout << "V : " << Utils::Vector::toString(V) << std::endl;
			std::cout << "X2 : " << Utils::Vector::toString(X2) << std::endl;
			for(size_t i=0; i<inters.size(); i++) {
				std::cout << inters[i].idx << " :: " << inters[i].dist << "\t";
			}
			std::cout << std::endl;
			std::cout << "Overflow" << std::endl;
			getchar();
		}*/

		// Vector from X to intersection (W = nrmW * unitV)
		double nrmW = inters.front().dist;
		std::vector<double> W = Utils::Vector::scale(nrmW, unitV);
		// Intersection point (I=X+W)
		std::vector<double> I = Utils::Vector::sum(X, W);
		// New position of X is I
		X = I;
		// New vector V is -1*(nrmV-nrmW)*unitV or (nrmW-nrmV)*unitV
		V = Utils::Vector::scale((nrmW-nrm), unitV);

		// Init new values : X2
		X2 = Utils::Vector::sum(V, X);

		// Count reflection
		reflectionCnt[inters.front().idx]++;
		if(reflectionCnt[inters.front().idx] > 100){ // TODO CHECK IF WORKING
			for(size_t i=0; i < pInd.size(); ++i){
				const int iP = pInd[i];
				// Refresh reflection for each parameter
				params.signalReflections(iP, reflectionCnt[i]);
			}
			return X2;
		}
	}

	for(size_t i=0; i < pInd.size(); ++i){
		const int iP = pInd[i];
		// Refresh reflection for each parameter
		params.signalReflections(iP, reflectionCnt[i]);
	}

	return X2;
}


vector<EllipsoidReflection::Intersection> EllipsoidReflection::getIntersections() {

	vector<Intersection> inter;
	for(size_t i=0; i < pInd.size(); ++i){
		const int iP = pInd[i];
		// Check if parameter is in bound
		if(params.requireReflection(iP) && !params.isInBound(iP, X2[i])){ // If not get distance to center point
			inter.push_back(Intersection(i, 0.));
		}
	}
	return inter;
}

void EllipsoidReflection::sortIntersectionByDistance(vector<EllipsoidReflection::Intersection> &inter) {

	for(size_t i=0; i < inter.size(); ++i){
		const int idx = inter[i].idx;
		const int iP = pInd[idx];

		const double min = params.getBoundMin(iP);
		const double max = params.getBoundMax(iP);

		// define dist as (Inter-X)/Unit
		// inter[i].dist = X2[idx] > max ? max : min;
		// inter[i].dist = (inter[i].dist-X[idx])/unitV[idx];
		inter[i].dist = X2[idx] > max ? max : min;
		inter[i].dist = nrm * (inter[i].dist-X[idx])/(X2[idx]-X[idx]);

	}

	std::sort(inter.begin(), inter.end());
}

vector<uint> EllipsoidReflection::getReflectionCount() const {
	return reflectionCnt;
}

} // namespace ParameterBlock

