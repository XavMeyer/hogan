//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MultiplierWindowPM.cpp
 *
 *  Created on: 15 jan. 2014
 *      Author: meyerx
 */

#include "MultiplierWindowPM.h"

namespace ParameterBlock {

MultiplierWindowPM::MultiplierWindowPM(double aB, const Parameters &aParams) : BlockModifier(true, aParams), b(aB) {
	lambda = 2*log(b);
	cout << "not yet ready";
	abort();
}

MultiplierWindowPM::~MultiplierWindowPM() {
}

void MultiplierWindowPM::updateParameters(const vector<size_t> &pInd, Sample &sample) const {
	for(size_t iB=0; iB < pInd.size(); ++iB){
		double u = Parallel::mpiMgr().getPRNG()->genUniformDbl()-0.5;
		double m = exp(lambda*u);
		sample.multParameter(pInd[iB], m);
	}

	BlockModifier::defaultReflection(pInd, sample);
}

double MultiplierWindowPM::getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double q = 1.0;
	return q;
}

double MultiplierWindowPM::getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double qRatio = 1.0;
	for(size_t iB=0; iB < pInd.size(); ++iB){
		double m = toSample.getDoubleParameter(pInd[iB]) / fromSample.getDoubleParameter(pInd[iB]);
		qRatio *= m;
	}
	return qRatio;
}

void MultiplierWindowPM::setWindowSize(const vector<double> &aWinSize){
	b = aWinSize[0];
	lambda = 2*log(b);
}

} // namespace ParameterBlock

