//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EllipsoidReflection.h
 *
 * @date Nov 7, 2014
 * @author meyerx
 * @brief
 */
#ifndef ELLIPSOIDREFLECTION_H_
#define ELLIPSOIDREFLECTION_H_

#include "Utils/VectorTools.h"
#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"

#include <vector>
using namespace std;

namespace ParameterBlock {

class EllipsoidReflection {
public:
	EllipsoidReflection(const vector<double> &aMu, const vector<double> &aV, const Parameters &aParams, const vector<size_t> &aPInd);
	~EllipsoidReflection();

	vector<double> getReflectedValues();
	vector<uint> getReflectionCount() const;

private:
	class Intersection; // Defined below

	vector<double> X, V;
	const Parameters &params;
	const vector<size_t> &pInd;
	vector<uint> reflectionCnt;

	double nrm;
	vector<double> unitV, X2;
	vector<Intersection> getIntersections();
	void sortIntersectionByDistance(vector<EllipsoidReflection::Intersection> &inter);


private:
	class Intersection {
	public:
		size_t idx;
		double dist;

		Intersection(const size_t aIdx, const double aDist) : idx(aIdx), dist(aDist) {}
		~Intersection(){}

		bool operator<(const Intersection& rhs) const {
			return this->dist < rhs.dist;
		}

		bool operator>(const Intersection& rhs) const {
			return this->dist > rhs.dist;
		}

	};


};

} // namespace ParameterBlock

#endif /* ELLIPSOIDREFLECTION_H_ */
