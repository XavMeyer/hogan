//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Block.h
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "Sampler/Samples/Sample.h"
#include "BlockModifier/BlockModifierInterface.h"
#include "Utils/Uncopyable.h"

#include <boost/shared_ptr.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

namespace ParameterBlock {

using namespace std;
namespace boostAcc = boost::accumulators ;

class Block {
public:
	typedef boost::shared_ptr<Block> sharedPtr_t;
	enum adaptiveType_t {NOT_ADAPTIVE, SINGLE_DOUBLE_VARIABLE, MIXED_DOUBLE_VARIABLES, PCA_DOUBLE_VARIABLES};
	enum blockOptimisation_t {OPTI_ENABLED, OPTI_DISABLED};
	static const size_t ROLLING_MEAN_SIZE;
public:
	Block(adaptiveType_t aAdaptiveType=PCA_DOUBLE_VARIABLES, blockOptimisation_t aBlockOpti=OPTI_DISABLED);
	~Block();

	void addParameter(size_t aInd, double freq=1.0);
	void addParameter(size_t aInd, BlockModifier::sharedPtr_t aBM, double freq=1.0);

	int getPIndice(size_t aInd) const;
	const vector<size_t>& getPIndices() const;
	const vector<double>& getPFreqs() const;
	double getTotalFreq() const;

	void setName(const string a_name);
	const string getName() const;

	std::vector<double> getTimes() const;
	std::vector<double> getLastTimes() const;
	double getTotalTime() const;

	void proposeMove(Sampler::Sample &sample) const;
	double getMoveProbability(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;
	double getMoveProbabilityRatio(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;

	bool hasBlockModifier() const;
	bool hasBlockModifier(const int ind) const;
	void setBlockModifier(BlockModifier::sharedPtr_t aBM);
	void setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM);
	void setBlockModifier(BlockModifier::sharedPtr_t aBM) const;
	void setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM) const;
	BlockModifier::sharedPtr_t getBlockModifier() const;
	BlockModifier::sharedPtr_t getBlockModifier(const int ind) const;

	void updateMeanTimes(double aPropTime, double aCompTime) const;

	void setId(size_t aId);
	size_t getId() const;

	size_t size() const;

	const string toString() const;
	const string summaryString() const;

	void accepted(const bool isLocalProposal) const;
	void rejected(const bool isLocalProposal) const;
	void thrown(const bool isLocalProposal) const;

	size_t getNAccepted() const;
	size_t getNRejected() const;
	size_t getNThrown() const;
	size_t getNSampled() const;

	void setAdaptiveType(adaptiveType_t aAdaptiveType);
	adaptiveType_t getAdaptiveType() const;
	bool isNotAdaptive() const;
	bool isSingleDoubleAdaptive() const;
	bool isMixedDoubleAdaptive() const;
	bool isPCADoubleAdaptive() const;

	void setBlockOptimisation(bool isActive);
	bool isSupportingBlockOptimisation() const;

private:
	mutable size_t m_rej, m_acc, m_throw;
	adaptiveType_t adaptiveType;
	blockOptimisation_t blockOpti;
	size_t id;
	string m_name;
	vector<size_t> pIndices;
	vector<double> pFreq;
	mutable boostAcc::accumulator_set<double, boostAcc::stats<boostAcc::tag::rolling_mean> > rollMPropTime, rollMCompTime;
	mutable boostAcc::accumulator_set<double, boostAcc::stats<boostAcc::tag::mean> > accPropTime, accCompTime;
	mutable BlockModifier::sharedPtr_t bm;
	mutable vector<BlockModifier::sharedPtr_t> bms;
};

} // namespace ParameterBlock


#endif /* BLOCK_H_ */

