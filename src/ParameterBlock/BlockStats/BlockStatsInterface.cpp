//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStats.cpp
 *
 * @date Mai 7, 2014
 * @author meyerx
 * @brief
 */
#include "BlockStatsInterface.h"

namespace ParameterBlock {

BlockStats::BlockStats(const Block::sharedPtr_t aBlock, const BlockStatCfg::sharedPtr_t aCfg) :
		block(aBlock), cfg(aCfg)
{
	n = block->size();

	sigmaConverged = false;
	lambdaConverged = false;
	locLambdaConverged = false;
}

BlockStats::BlockStats(const BlockStats &toCopy) : block(toCopy.block), cfg(toCopy.cfg) {
	n = toCopy.n;

	sigmaConverged = false;
	lambdaConverged = false;
	locLambdaConverged = false;
}


BlockStats::~BlockStats() {
}

void BlockStats::getXValues(const Sample &sample, vector<double> &X) {
	for(unsigned int i=0; i<n; ++i){
		X.push_back(sample.getDoubleParameter(block->getPIndices()[i]));
	}
}

bool BlockStats::hasSigmaConverged() const {
	return sigmaConverged;
}

bool BlockStats::hasLambdaConverged() const {
	return lambdaConverged;
}

bool BlockStats::hasLocLambdaConverged() const {
	return locLambdaConverged;
}

double BlockStats::getAlphaFromL(const double l, const double var) const {
	// Efficiency in function of alphas
	boost::math::normal norm;
	return 2.*boost::math::cdf(norm, -l/(2*sqrt(var)));
}

double BlockStats::getOptiL(const unsigned int nP, const unsigned int nPPB, const double var) const {
	unsigned int nL = 5000;
	double endL = 100.0*var;
	double L[nL], E[nL], D[nL];

	for(unsigned int i=0; i<nL; ++i){
		L[i] = (1+i)*endL/(double)nL;
	}

	// Efficiency in function of alphas
	double exponent = (2.-0.96/exp((double)nPPB/100.));
	boost::math::normal norm;
	for(unsigned int i=0; i<nL; ++i){
		E[i] = 2*pow(L[i], exponent)*boost::math::cdf(norm, -L[i]/(2*sqrt(var)));

		// Gains in function of processors
		D[i] = 1.;
		double alpha = getAlphaFromL(L[i], var);
		for(unsigned int iP=2; iP<=nP; ++iP){
			D[i] += pow((1.-alpha), iP-1.);
		}
	}

	// Get overall efficiency and get max value
	int iMax = -1;
	double max = -1.;

	// Get max alpha
	for(unsigned int i=0; i<nL; ++i){
		E[i] *= D[i];
		if(E[i] > max){
			iMax = i;
			max = E[i];
		}
	}
	return L[iMax];
}

} // namespace ParameterBlock

