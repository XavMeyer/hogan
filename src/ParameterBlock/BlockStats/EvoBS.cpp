//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoBS.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockStats/EvoBS.h"

namespace ParameterBlock {

EvoBS::EvoBS(double aAlpha, const Block::sharedPtr_t aBlock, const BlockStatCfg::sharedPtr_t aCfg) :
	BlockStats(aBlock, aCfg), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER)
{
	targAlpha = aAlpha;
	locSdT = 0;

	// Init Mu updaters
	for(uint iM=0; iM<n; ++iM){
		evoMu.push_back(new BatchEvoMU(cfg->MU, cfg->CONV_CHECKER));
	}

	// Init Sigma updater
	evoSu = new BatchEvoSU(aBlock->size(), cfg->SIGMA, cfg->CONV_CHECKER);
	evoSu->relaxTolerance(1.0, 2.5);
	initSigma();

	// Init Lu updaters
	evoLu = new BatchEvoLU(aAlpha, aBlock->size(), cfg->LAMBDA, cfg->CONV_CHECKER);
	initLambda(aAlpha);

	for(uint iM=0; iM<n; ++iM){
		evoLocalLu.push_back(new BatchLightEvoLU(pow(aAlpha, 1.5/n), aBlock->size(), cfg->LOC_LAMBDA));
	}

	factors.assign(n, 1.0/(double)n);

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);
	divergeCnt.assign(n, 0);
}

EvoBS::EvoBS(const EvoBS &toCopy) :
	BlockStats(toCopy), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER)
{
	targAlpha = toCopy.targAlpha;
	locSdT = toCopy.locSdT;
	evoSu = new BatchEvoSU(*dynamic_cast<BatchEvoSU*>(toCopy.evoSu));
	for(uint iM=0; iM<toCopy.evoMu.size(); ++iM){
		evoMu.push_back(new BatchEvoMU(*dynamic_cast<BatchEvoMU*>(toCopy.evoMu[iM])));
	}

	evoLu = new BatchEvoLU(*dynamic_cast<BatchEvoLU*>(toCopy.evoLu));
	for(uint iL=0; iL<toCopy.evoLocalLu.size(); ++iL){
		evoLocalLu.push_back(new BatchLightEvoLU(*dynamic_cast<BatchLightEvoLU*>(toCopy.evoLocalLu[iL])));
	}

	factors = toCopy.factors;

	nLocSDDisabled = toCopy.nLocSDDisabled;
	locSDEnabled = toCopy.locSDEnabled;
}

EvoBS::~EvoBS() {
	delete evoSu;

	for(uint iM=0; iM<n; ++iM){
		delete evoMu[iM];
	}

	delete evoLu;
	for(uint iM=0; iM<n; ++iM){
		delete evoLocalLu[iM];
	}
}

void EvoBS::initSigma() {

	if(block->getBlockModifier() != 0){
		vector<double> sigma = block->getBlockModifier()->getWindowSize();
		if(sigma.size() == n*n) {
			evoSu->setInitialSigma(sigma);
		} else {
			double sigma = block->getBlockModifier()->getWindowSize()[0];
			evoSu->setInitialSigma(sigma);
		}
	} else if(block->getBlockModifier(0) != 0){
		for(unsigned int i=0; i<n; ++i){
			double sigma = block->getBlockModifier(i)->getWindowSize()[0];
			evoSu->setInitialSigma(i, sigma);
		}
	} else {
		for(unsigned int i=0; i<n; ++i){
			evoSu->setInitialSigma(1.0);
		}
	}

}

void EvoBS::initLambda(const double myAlpha) {
	uint nP = Parallel::mcmcMgr().getNProposal();
	double var = evoSu->getSigma(0) > 0 ? evoSu->getSigma(0) : 1.e-3;
	double lOp = getOptiL(nP, n, var);
	double newLambda = lOp*lOp/(var);
	newLambda /= evoLu->getSd();
	evoLu->updateSd(newLambda);
}


void EvoBS::initSample(const Sample &sample){
	vector<double> X;
	getXValues(sample, X);

	for(uint iM=0; iM < n; ++iM){
		evoMu[iM]->setInitialMean(X[iM]);
	}
}

void EvoBS::update(const Sample &sample, const uint nTimes){
	if(sigmaConverged) return;
	if(!sample.hasChanged() && (((evoSu->getT()-1)*cfg->SIGMA->MEAN_WINDOW +
			dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) < cfg->VAR_STEP_BOOST)) {
		return;
	}

	vector<double> X;
	getXValues(sample, X);

	for(uint iT=0; iT<nTimes; ++iT){

		// Update mu
		for(uint iM=0; iM<n; ++iM){
			evoMu[iM]->addVal(X[iM]);
		}
		// Update Sigma
		evoSu->addVals(X, evoMu);
	}

	if(hasUpdateConverged()) {
		sigmaConverged = true;
	}
}

void EvoBS::updateSD(const double aAlpha){
	if(lambdaConverged) return;

	if(aAlpha >= 0.){
		evoLu->addVal(aAlpha);
	} else {
		// If does not diverge too much, just scale lamba
		uint nP = Parallel::mcmcMgr().getNProposal();
		if(divergeCnt[0] == 0) {
			double sigma = evoSu->getSigma(0);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = lOp*lOp/(evoSu->getSigma(0));

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
		} else if(divergeCnt[0] == 1){ // Else reduce sigma and lambda and hard reset
			double fac = 1.;
			double sigma = evoSu->getSigma(0)/(fac*10.);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = (lOp*lOp/(evoSu->getSigma(0)))/(fac*10.);

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
			evoLu->updateSd(newLambda);
			evoLu->setTargetAlpha(evoLu->getTraceAlpha()+0.03);

			//evoSu->reset(false);
			//evoSu->setInitialSigma(sigma);
		} else {
			double sigma = evoSu->getSigma(0);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = (lOp*lOp/(evoSu->getSigma(0)))/10.;

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
			evoLu->updateSd(newLambda);

			lambdaConverged = true;
			locLambdaConverged = true;
		}
		divergeCnt[0]++;
	}

	if(evoLu->hasConverged() && sigmaConverged) {
		//if((n > 1 && (evoLocalLu[0]->getT() > MIN_T_LOCSD || locLambdaConverged)) || n == 1) {
		if((n > 1 && locLambdaConverged) || n == 1) {

			evoLu->setAverageSd();

			lambdaConverged = true;
			locLambdaConverged = true;

#ifdef DBG_PREDICTIVE

			if(Parallel::mpiMgr().isMainProcessor()){

				stringstream ssPred;
				ssPred << "PredBlock_" << block->getId() << ".dat";
				ofstream oPF(ssPred.str().c_str());


				oPF << n << endl;
				oPF << scientific << evoLu->getSd() << endl;

				for(uint i=0; i<n ;++i){
					oPF << block->getPIndices()[i] << "\t";
				}
				oPF << endl;

				for(uint i=0; i<n ;++i){
					oPF << evoMu[i]->getMean() << "\t";
				}
				oPF << endl;

				for(uint i=0; i<n ;++i){
					for(uint j=0; j<n ;++j){
					oPF << evoSu->getSigma()[i*n+j] << "\t";
					}
					oPF << endl;
				}

				for(uint i=0; i<n ;++i){
					oPF << factors[i] << "\t";
				}
				oPF << endl;

				oPF.close();
			}
#endif
		}
	}
}

void EvoBS::updateLocSD(const vector<double> &aAlphas) {
	if(lambdaConverged || locLambdaConverged) return;

	double sum = 0.0;
	double facSumSq = 0.0;
	double alphaMean = 0.0;

	// Update local scaling factors
	for(uint iL=0; iL<n; ++iL){
		if(aAlphas[iL] < 0.){ // negative means too much reflection
			evoLocalLu[iL]->addVal(-aAlphas[iL]);
			evoLocalLu[iL]->updateSd(0.5); // reduce local lambda
		} else {
			evoLocalLu[iL]->addVal(aAlphas[iL]);
		}

		// Keep trac for the average
		if(locSDEnabled[iL]){
			sum += evoLocalLu[iL]->getSd();
		}
	}

	double tmpN = (n-nLocSDDisabled);
	for(uint iL=0; iL<n; ++iL){
		if(locSDEnabled[iL]){
			factors[iL] = evoLocalLu[iL]->getSd() / sum; // Normalize
			facSumSq += pow((factors[iL]-(1./tmpN)),2); // sample std
			alphaMean += evoLocalLu[iL]->getTraceAlpha()/tmpN;
		}
	}

	double facVar = 0.;
	if(tmpN > 1){ // If we have more than 1 evolving parameter
		facVar = facSumSq/(tmpN-1.);
	}

	// if the sample std is too high and the global acceptance factor is diverging
	if((tmpN*sqrt(facVar) > 1.5) && evoLu->getSd() > 300.0) {
		// Find biggest factor
		uint idxBF = 0;
		for(uint iL=1; iL<n; ++iL){
			if(locSDEnabled[iL] && factors[iL] > factors[idxBF]){
				idxBF = iL;
			}
		}

		divergeCnt[idxBF]++;
		if(divergeCnt[idxBF] > 8) { // Diverges 8 times consecutively
			// Set idxBF to inactive
			locSDEnabled[idxBF] = false;
			++nLocSDDisabled;

			// Update factors
			double tmpSum = 0.0;
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					tmpSum += factors[iL];
				}
			}
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					factors[iL] /= tmpSum;
				}
			}
			factors[idxBF] = -1;
			// fix this local acceptance rate as average alpha minus 5%
			evoLocalLu[idxBF]->setTargetAlpha(evoLocalLu[idxBF]->getTraceAlpha()-0.05);
			evoLocalLu[idxBF]->reset(false);

			// Change Alpha and SD
			evoLu->updateSd(tmpSum);
		}

	}

	if(evoLocalLu[0]->getTmpT() == 0) {
		mvcc.addValue(sqrt(facVar));
		if(evoLocalLu[0]->getT() > 40){
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					evoLocalLu[iL]->setTargetAlpha(alphaMean);
				}
			}
		}
	}

	// Check for local convergence
	if((n-nLocSDDisabled) == 1 || (mvcc.hasConverged(sqrt(facVar)) && sigmaConverged) || evoLocalLu[0]->getT() >= cfg->LOC_LAMBDA->MAX_T){
		locLambdaConverged = true;
	}

}

bool EvoBS::isReady() {
	bool initC = true;
	bool endC = !(sigmaConverged && lambdaConverged && locLambdaConverged);

	return initC && endC;
}


bool EvoBS::isReadyForLocUpdate(){
	if(lambdaConverged || n == 1) return false;
	bool initOk = ((evoSu->getT()-1)*cfg->LAMBDA->MEAN_WINDOW + dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) > cfg->VAR_STEP_BOOST;
	bool waitSD = evoLu->getT() > (cfg->LAMBDA->NB_BATCH*cfg->LAMBDA->L_BATCH)*0.1;
	//bool periodicUpdate = (evoLu->getT() % STEP_UPDATE_LOCSD == 0)  && (evoLu->getTmpT() == 0);
	bool periodicUpdate = (locSdT % cfg->LOC_LAMBDA_STEP_UPDATE == 0);
	++locSdT;

	return initOk && periodicUpdate && waitSD;
}

bool EvoBS::isDisabled(const unsigned int idx) const {
	return !locSDEnabled[idx];
}

double EvoBS::getMu(size_t idx) const {
	return evoMu[idx]->getMean();
}

double EvoBS::getSd() const {
	return evoLu->getSd();
}
double EvoBS::getLocSd(unsigned int idx) const {
	if(locSDEnabled[idx]){
		return factors[idx];
	} else {
		return evoLocalLu[idx]->getSd()/evoLu->getSd();
	}
}

double EvoBS::getSigmaIdx(unsigned int idx) const {
	return evoSu->getSigma(idx);
}

const double* EvoBS::getSigma() const {
	return evoSu->getSigma();
}

void EvoBS::reduceLocSD(uint idx, const double factor) {
	// Update the local SD
	evoLocalLu[idx]->updateSd(factor);
	if(locSDEnabled[idx]){ // if this loc SD is enabled, updated factors
		double sum = 0.;
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				sum += evoLocalLu[iL]->getSd();
			}
		}
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				factors[iL] = evoLocalLu[iL]->getSd() / sum;
			}
		}
	}
}

bool EvoBS::hasUpdateConverged() const {
	for(uint iM=0; iM<n; ++iM){
		if(!evoMu[iM]->hasConverged()){
			return false;
		}
	}
	return evoSu->hasConverged();
}

void EvoBS::reset(){

	// SU
	evoSu->reset(true);
	BatchEvoSU* BRSUptr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BRSUptr != 0) BRSUptr->reset(true);

	// MU
	for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->reset(true);
		BatchEvoMU* BEMUPtr = dynamic_cast<BatchEvoMU*>(evoMu[iM]);
		if(BEMUPtr != 0) BEMUPtr->reset(true);
	}

	evoLu->reset(true);
	BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) BLELUPtr->reset(true);

	mvcc.reset();
	for(uint iM=0; iM<n; ++iM){
		evoLocalLu[iM]->reset(true);
		BatchLightEvoLU* BLELUPtr = dynamic_cast<BatchLightEvoLU*>(evoLu);
		if(BLELUPtr != 0) BLELUPtr->reset(true);
	}

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);

	sigmaConverged = lambdaConverged = locLambdaConverged = false;
}

void EvoBS::resetMVCC(){

	// SU
	/*BatchEvoSU* BLESUPtr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BLESUPtr != 0) BLESUPtr->resetMVCC();*/

	// MU
	/*for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->resetMVCC();
	}*/

	BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) {
		BLELUPtr->reset(true);
		//BLELUPtr->resetMVCC();
	}

	evoLu->resetAccumulator();

	//mvcc.reset();
	this->relaxToleranceLambda(1.0);
	sigmaConverged = false;
	lambdaConverged = locLambdaConverged = false;
}

bool EvoBS::hasCorrelation() const {
	const double *ptrS = evoSu->getSigma();

	for(uint i=0; i<n; ++i){
		double diagI = 1./sqrt(evoSu->getSigma(i));
		for(uint j=0; j<n; ++j){
			double diagJ = 1./sqrt(evoSu->getSigma(j));
			double corrIJ = diagI*diagJ*fabs(ptrS[i*n+j]);
			if(i!=j && corrIJ > cfg->THRESHOLD_CORRELATION) return true;
		}
	}
	return false;
}

bool EvoBS::hasDisabledFactors() const {
	return nLocSDDisabled > 0;
}

void EvoBS::relaxToleranceSigma(const double tolFactor, const double tolFactorCorrel) {
	double relaxTol = tolFactor;
	for (uint iM=0; iM<evoMu.size(); ++iM){
		evoMu[iM]->relaxTolerance(relaxTol);
	}
	evoSu->relaxTolerance(relaxTol, tolFactorCorrel);
}

void EvoBS::relaxToleranceLambda(const double tolFactor) {
	double relaxTol = tolFactor;
	evoLu->relaxTolerance(relaxTol);
	mvcc.relaxTolerance(relaxTol);
}

std::string EvoBS::getSummaryString() const {
	std::stringstream ss;
	ss << "Acceptance rate :\t" << evoLu->getTraceAlpha() << std::endl;
	ss << "Lambda (val, t, rmc t) :\t" << evoLu->getSd() << "\t" << evoLu->getT() << "\t" << evoLu->getRMC()->getT() << std::endl;
	ss << "Sigma (t, rmc t)       :\t" << evoSu->getT() << "\t" << evoSu->getRMC()->getT() << std::endl;

	ss << setw(10) << "pid\t";
	for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
		ss << std::setw(8) << block->getPIndice(iP) << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "mu \t";
	for(size_t iP=0; iP<evoMu.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoMu[iP]->getMean() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "t(mu)\t";
	for(size_t iP=0; iP<evoMu.size(); ++iP) {
		ss << std::setw(8) << evoMu[iP]->getRMC()->getT() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "Sigma\t";
	for(size_t iP=0; iP<block->getPIndices().size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoSu->getSigma(iP) << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "Factors\t";
	for(size_t iP=0; iP<factors.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << factors[iP] << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "LocSd\t";
	for(size_t iP=0; iP<evoLocalLu.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoLocalLu[iP]->getSd() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "LocSdT\t";
	for(size_t iP=0; iP<evoLocalLu.size(); ++iP) {
		ss << std::setw(8) << evoLocalLu[iP]->getRMC()->getT() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "ScaledSD\t";
	for(size_t iP=0; iP<evoLocalLu.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoSu->getSigma(iP)* factors[iP] * evoLu->getSd() << "\t";
	}
	ss << std::endl;

	return ss.str();
}

} // namespace ParameterBlock
