//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConfigFactory.h
 *
 * @date May 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef BLOCKSTATCFGFACTORY_H_
#define BLOCKSTATCFGFACTORY_H_

#include "Container/BlockStatCfg.h"

#include <boost/shared_ptr.hpp>

namespace ParameterBlock {
namespace Config {

class ConfigFactory {
public:
	typedef boost::shared_ptr<ConfigFactory> sharedPtr_t;

public:
	ConfigFactory();
	virtual ~ConfigFactory();

	virtual BlockStatCfg::sharedPtr_t createConfig(const size_t N) const = 0;

	static sharedPtr_t createDefaultFactory();
	static sharedPtr_t createFastFactory();
	static sharedPtr_t createExtraFastFactory();
	static sharedPtr_t createSlowFactory();
};

} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BLOCKSTATCFGFACTORY_H_ */
