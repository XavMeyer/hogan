//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStatCfg.h
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef BLOCKSTATCFG_H_
#define BLOCKSTATCFG_H_

#include <stddef.h>
#include <boost/shared_ptr.hpp>

#include "UpdaterCfg.h"
#include "ConvCheckerCfg.h"

#include "Utils/Code/SerializationSupport.h"

namespace ParameterBlock {
namespace Config {

class BlockStatCfg {
public:
	typedef boost::shared_ptr<BlockStatCfg> sharedPtr_t;

public:
	BlockStatCfg();
	BlockStatCfg(const unsigned int aLL_STEP_UPD, const unsigned int aVAR_STEP_BOOST,
				 const unsigned int aFREQ_MC3, const double aTH_CORR, const UpdaterCfg *aMU,
				 const UpdaterCfg *aSIGMA, const UpdaterCfg *aLAMBDA,
				 const UpdaterCfg *aLOC_LAMBDA, const UpdaterCfg *aMC3,
				 const ConvCheckerCfg *aCONV_CHECKER);
	~BlockStatCfg();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

	static BlockStatCfg::sharedPtr_t getSlow(const unsigned int size);
	static BlockStatCfg::sharedPtr_t getDefault(const unsigned int size);
	static BlockStatCfg::sharedPtr_t getFast(const unsigned int size);
	static BlockStatCfg::sharedPtr_t getExtraFast(const unsigned int size);

public:
	const unsigned int LOC_LAMBDA_STEP_UPDATE, VAR_STEP_BOOST, FREQ_MC3;
	const double THRESHOLD_CORRELATION;

	const UpdaterCfg *MU;
	const UpdaterCfg *SIGMA;
	const UpdaterCfg *LAMBDA;
	const UpdaterCfg *LOC_LAMBDA;
	const UpdaterCfg *MC3;

	const ConvCheckerCfg *CONV_CHECKER;

private:

	friend class boost::serialization::access;

};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* BLOCKSTATCFG_H_ */
