//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStatCfg.cpp
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#include "BlockStatCfg.h"

namespace ParameterBlock {
namespace Config {

BlockStatCfg::BlockStatCfg() :
		LOC_LAMBDA_STEP_UPDATE(8), VAR_STEP_BOOST(30), FREQ_MC3(149), THRESHOLD_CORRELATION(0.5),
	   MU(new UpdaterCfg()), SIGMA(new UpdaterCfg()),  LAMBDA(new UpdaterCfg()),
	   LOC_LAMBDA(new UpdaterCfg()), MC3(new UpdaterCfg()), CONV_CHECKER(new ConvCheckerCfg()) {
}

BlockStatCfg::BlockStatCfg(const unsigned int aLL_STEP_UPD, const unsigned int aVAR_STEP_BOOST,
		 	 	 	 	   const unsigned int aFREQ_MC3, const double aTH_CORR, const UpdaterCfg *aMU,
		 	 	 	 	   const UpdaterCfg *aSIGMA, const UpdaterCfg *aLAMBDA,
		 	 	 	 	   const UpdaterCfg *aLOC_LAMBDA, const UpdaterCfg *aMC3,
		 	 	 	 	   const ConvCheckerCfg *aCONV_CHECKER) :
	     	 	 	 		   LOC_LAMBDA_STEP_UPDATE(aLL_STEP_UPD), VAR_STEP_BOOST(aVAR_STEP_BOOST),
	     	 	 	 		   FREQ_MC3(aFREQ_MC3), THRESHOLD_CORRELATION(aTH_CORR),
	     	 	 	 		   MU(aMU), SIGMA(aSIGMA), LAMBDA(aLAMBDA), LOC_LAMBDA(aLOC_LAMBDA),
	     	 	 	 		   MC3(aMC3), CONV_CHECKER(aCONV_CHECKER){
}

BlockStatCfg::~BlockStatCfg() {
}

BlockStatCfg::sharedPtr_t BlockStatCfg::getSlow(const unsigned int size) {
	BlockStatCfg::sharedPtr_t temp(
			new BlockStatCfg(8, 30, 199, 0.5,
					UpdaterCfg::getSlowMu(),
					UpdaterCfg::getSlowSigma(),
					UpdaterCfg::getSlowLambda(size),
					UpdaterCfg::getSlowLocLambda(size),
					UpdaterCfg::getSlowMC3(),
					ConvCheckerCfg::getDefault()));
	return temp;
}

BlockStatCfg::sharedPtr_t BlockStatCfg::getDefault(const unsigned int size) {
	BlockStatCfg::sharedPtr_t temp(
			new BlockStatCfg(8, 30, 149, 0.5,
					UpdaterCfg::getDefaultMu(),
					UpdaterCfg::getDefaultSigma(),
					UpdaterCfg::getDefaultLambda(size),
					UpdaterCfg::getDefaultLocLambda(size),
					UpdaterCfg::getDefaultMC3(),
					ConvCheckerCfg::getDefault()));
	return temp;
}

BlockStatCfg::sharedPtr_t BlockStatCfg::getFast(const unsigned int size) {
	BlockStatCfg::sharedPtr_t temp(
			new BlockStatCfg(8, 20, 99, 0.5,
				   UpdaterCfg::getFastMu(),
				   UpdaterCfg::getFastSigma(),
				   UpdaterCfg::getFastLambda(size),
				   UpdaterCfg::getFastLocLambda(size),
				   UpdaterCfg::getFastMC3(),
				   ConvCheckerCfg::getFast()));

	return temp;
}

BlockStatCfg::sharedPtr_t BlockStatCfg::getExtraFast(const unsigned int size) {
	BlockStatCfg::sharedPtr_t temp(
			new BlockStatCfg(8, 15, 49, 0.5,
				   UpdaterCfg::getExtraFastMu(),
				   UpdaterCfg::getExtraFastSigma(),
				   UpdaterCfg::getExtraFastLambda(size),
				   UpdaterCfg::getExtraFastLocLambda(size),
				   UpdaterCfg::getExtraFastMC3(),
				   ConvCheckerCfg::getExtraFast()));

	return temp;
}

} /* namespace Config */
} /* namespace ParameterBlock */

