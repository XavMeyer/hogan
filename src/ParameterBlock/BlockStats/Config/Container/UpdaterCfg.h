//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file UpdaterCfg.h
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef UPDATERCFG_H_
#define UPDATERCFG_H_

#include <stddef.h>

#include "Parallel/Parallel.h"

namespace ParameterBlock {
namespace Config {

class UpdaterCfg {
public:

	UpdaterCfg();
	UpdaterCfg(const double aCST_RM, const double aLAMBDA_RM, const unsigned int aMEAN_WINDOW,
			   const double aEPSILON, const unsigned int aL_WINDOW, const unsigned int aNB_BATCH,
			   const unsigned int aL_BATCH, const unsigned int aMAX_T_MU);
	~UpdaterCfg();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

	static UpdaterCfg* getSlowMu();
	static UpdaterCfg* getSlowSigma();
	static UpdaterCfg* getSlowLambda(const unsigned int N);
	static UpdaterCfg* getSlowLocLambda(const unsigned int N);
	static UpdaterCfg* getSlowMC3();

	static UpdaterCfg* getDefaultMu();
	static UpdaterCfg* getDefaultSigma();
	static UpdaterCfg* getDefaultLambda(const unsigned int N);
	static UpdaterCfg* getDefaultLocLambda(const unsigned int N);
	static UpdaterCfg* getDefaultMC3();

	static UpdaterCfg* getFastMu();
	static UpdaterCfg* getFastSigma();
	static UpdaterCfg* getFastLambda(const unsigned int N);
	static UpdaterCfg* getFastLocLambda(const unsigned int N);
	static UpdaterCfg* getFastMC3();

	static UpdaterCfg* getExtraFastMu();
	static UpdaterCfg* getExtraFastSigma();
	static UpdaterCfg* getExtraFastLambda(const unsigned int N);
	static UpdaterCfg* getExtraFastLocLambda(const unsigned int N);
	static UpdaterCfg* getExtraFastMC3();

public:
	// Robins-Monroe values
	const double CST_RM, LAMBDA_RM; //!< Robbins-Monroe constants
	const unsigned int MEAN_WINDOW; //!< Robbins-Monroe number of averaged sample

	// Convergence values
	const double EPSILON; //!< Epsilon value for convergence checker
	const unsigned int L_WINDOW; //!< Length of the sliding window
	const unsigned int NB_BATCH, L_BATCH; //!< Number and length of batches
	const unsigned int MAX_T; //!< Max number before forced convergence

private:

	static UpdaterCfg *instDefMu, *instDefSigma;
	static UpdaterCfg *instFastMu, *instFastSigma;

	static const unsigned int scaleMeanWindow(const unsigned int N, const unsigned int MEAN_WINDOW, const unsigned int MAX);

	friend class boost::serialization::access;


};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* UPDATERCFG_H_ */
