//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file UpdaterCfg.cpp
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#include "UpdaterCfg.h"

namespace ParameterBlock {
namespace Config {

// Default values
UpdaterCfg * UpdaterCfg::instDefMu = NULL;
UpdaterCfg * UpdaterCfg::instDefSigma = NULL;

// Fast values
UpdaterCfg * UpdaterCfg::instFastMu = NULL;
UpdaterCfg * UpdaterCfg::instFastSigma = NULL;

UpdaterCfg::UpdaterCfg() : CST_RM(0.9), LAMBDA_RM(0.7), MEAN_WINDOW(20), EPSILON(2.5e-1),
		   L_WINDOW(20), NB_BATCH(4), L_BATCH(20), MAX_T(4000){
}

UpdaterCfg::UpdaterCfg(const double aCST_RM, const double aLAMBDA_RM, const unsigned int aMEAN_WINDOW,
		   const double aEPSILON, const unsigned int aL_WINDOW, const unsigned int aNB_BATCH,
		   const unsigned int aL_BATCH, const unsigned int aMAX_T) :
			   CST_RM(aCST_RM), LAMBDA_RM(aLAMBDA_RM), MEAN_WINDOW(aMEAN_WINDOW), EPSILON(aEPSILON),
			   L_WINDOW(aL_WINDOW), NB_BATCH(aNB_BATCH), L_BATCH(aL_BATCH), MAX_T(aMAX_T) {
}

UpdaterCfg::~UpdaterCfg() {
}

UpdaterCfg* UpdaterCfg::getSlowMu() {
	if(instDefMu == NULL) {
		instDefMu = new UpdaterCfg(1.0, 1.5, 15, 2.5e-1, 15, 3, 15, 10000);
	}
	return instDefMu;
}

UpdaterCfg* UpdaterCfg::getSlowSigma() {
	if(instDefSigma == NULL) {
		instDefSigma = new UpdaterCfg(1.0, 1.3, 20, 2.5e-1, 20, 4, 20, 10000);
	}
	return instDefSigma;
}

UpdaterCfg* UpdaterCfg::getSlowLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 20;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 60);
	UpdaterCfg* tmp = new UpdaterCfg(1.5, 1.5, SCALED_MW, 4.e-1, 20, 5, 25, 10000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getSlowLocLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 15;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 30);
	UpdaterCfg* tmp = new UpdaterCfg(1.5, 1.5, SCALED_MW, 4.e-1, 12, 4, 18, 10000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getSlowMC3() {
	UpdaterCfg* tmp = new UpdaterCfg(1.2, 1.2, 10, 4.e-1, 12, 4, 15, 10000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getDefaultMu() {
	if(instDefMu == NULL) {
		instDefMu = new UpdaterCfg(0.7, 0.8, 15, 2.5e-1, 15, 3, 15, 4000);
	}
	return instDefMu;
}

UpdaterCfg* UpdaterCfg::getDefaultSigma() {
	if(instDefSigma == NULL) {
		instDefSigma = new UpdaterCfg(0.9, 0.7, 20, 2.5e-1, 20, 4, 20, 4000);
	}
	return instDefSigma;
}

UpdaterCfg* UpdaterCfg::getDefaultLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 30;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 60);
	UpdaterCfg* tmp = new UpdaterCfg(1., 0.7, SCALED_MW, 4.e-1, 20, 5, 25, 4000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getDefaultLocLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 15;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 30);
	UpdaterCfg* tmp = new UpdaterCfg(1., 0.7, SCALED_MW, 4.e-1, 12, 4, 18, 4000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getDefaultMC3() {
	UpdaterCfg* tmp = new UpdaterCfg(1.0, 0.8, 10, 4.e-1, 10, 4, 15, 4000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getFastMu() {
	if(instFastMu == NULL) {
		instFastMu = new UpdaterCfg(0.7, 0.8, 10, 5.e-1, 10, 3, 10, 2000);
	}
	return instFastMu;
}

UpdaterCfg* UpdaterCfg::getFastSigma() {
	if(instFastSigma == NULL) {
		instFastSigma = new UpdaterCfg(0.9, 0.7, 20, 5.e-1, 15, 4, 15, 2000);
	}
	return instFastSigma;
}

UpdaterCfg* UpdaterCfg::getFastLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 15;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 25);
	UpdaterCfg* tmp = new UpdaterCfg(1., 0.6, SCALED_MW, 8.e-1, 15, 4, 20, 2000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getFastLocLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 8;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 15);
	UpdaterCfg* tmp = new UpdaterCfg(1., 0.6, SCALED_MW, 8.e-1, 10, 3, 15, 2000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getFastMC3() {
	UpdaterCfg* tmp = new UpdaterCfg(1.0, 0.6, 10, 4.e-1, 10, 4, 10, 2000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getExtraFastMu() {
	if(instFastMu == NULL) {
		instFastMu = new UpdaterCfg(0.6, 0.5, 8, 8.e-1, 8, 3, 8, 2000);
	}
	return instFastMu;
}

UpdaterCfg* UpdaterCfg::getExtraFastSigma() {
	if(instFastSigma == NULL) {
		instFastSigma = new UpdaterCfg(0.8, 0.6, 15, 8.e-1, 12, 4, 12, 2000);
	}
	return instFastSigma;
}

UpdaterCfg* UpdaterCfg::getExtraFastLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 10;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 20);
	UpdaterCfg* tmp = new UpdaterCfg(0.8, 0.5, SCALED_MW, 8.e-1, 12, 4, 15, 2000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getExtraFastLocLambda(const unsigned int N) {
	const unsigned int MEAN_WIN = 5;
	const unsigned int SCALED_MW = scaleMeanWindow(N, MEAN_WIN, 12);
	UpdaterCfg* tmp = new UpdaterCfg(0.8, 0.5, SCALED_MW, 1., 8, 3, 12, 2000);
	return tmp;
}

UpdaterCfg* UpdaterCfg::getExtraFastMC3() {
	UpdaterCfg* tmp = new UpdaterCfg(0.8, 0.4, 10, 8.e-1, 6, 4, 8, 1000);
	return tmp;
}

const unsigned int UpdaterCfg::scaleMeanWindow(const unsigned int N, const unsigned int MEAN_WINDOW, const unsigned int MAX) {
	uint nProposal = Parallel::mcmcMgr().getNProposal();
	return std::min((int)MAX, (int)(MEAN_WINDOW*pow(nProposal, ((double)N-1.)/(2.*(double)MAX))));
}

} /* namespace Config */
} /* namespace ParameterBlock */
