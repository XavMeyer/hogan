//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConvCheckerCfg.h
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef CONVCHECKERCFG_H_
#define CONVCHECKERCFG_H_

#include <stddef.h>

#include "Utils/Code/SerializationSupport.h"

namespace ParameterBlock {
namespace Config {

class ConvCheckerCfg {
public:

	ConvCheckerCfg();
	ConvCheckerCfg(const double aMEAN_TH, const double aRSD_TH,
				   const double aMRSD_TH, const unsigned int aTOL_RELAX_TH);
	~ConvCheckerCfg();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

	static ConvCheckerCfg* getDefault();
	static ConvCheckerCfg* getFast();
	static ConvCheckerCfg* getExtraFast();

public:
	const double MEAN_TH;
	const double RSD_TH;
	const double MRSD_TH;
	const unsigned TOL_RELAX_TH;

private:

	static ConvCheckerCfg *instDef, *instFast;

	friend class boost::serialization::access;

};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* CONVCHECKERCFG_H_ */
