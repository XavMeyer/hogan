//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConvCheckerCfg.cpp
 *
 * @date May 3, 2015
 * @author meyerx
 * @brief
 */
#include "ConvCheckerCfg.h"

namespace ParameterBlock {
namespace Config {

ConvCheckerCfg * ConvCheckerCfg::instDef = NULL;
ConvCheckerCfg * ConvCheckerCfg::instFast = NULL;

ConvCheckerCfg::ConvCheckerCfg() :
		MEAN_TH(1.e-2), RSD_TH(5.e-2), MRSD_TH(5.e-2), TOL_RELAX_TH(150) {
}

ConvCheckerCfg::ConvCheckerCfg(const double aMEAN_TH, const double aRSD_TH,
							   const double aMRSD_TH, const unsigned int aTOL_RELAX_TH) :
	MEAN_TH(aMEAN_TH), RSD_TH(aRSD_TH), MRSD_TH(aMRSD_TH), TOL_RELAX_TH(aTOL_RELAX_TH) {
}

ConvCheckerCfg::~ConvCheckerCfg() {
}

ConvCheckerCfg* ConvCheckerCfg::getDefault() {
	if(instDef == NULL) {
		instDef = new ConvCheckerCfg(1.e-2, 5.e-2, 5.e-2, 150);
	}
	return instDef;
}

ConvCheckerCfg* ConvCheckerCfg::getFast() {
	if(instFast == NULL) {
		instFast = new ConvCheckerCfg(2.e-2, 5.e-2, 5.e-2, 100);
	}
	return instFast;
}

ConvCheckerCfg* ConvCheckerCfg::getExtraFast() {
	if(instFast == NULL) {
		instFast = new ConvCheckerCfg(5.e-2, 1.e-1, 1.e-1, 75);
	}
	return instFast;
}

} /* namespace Config */
} /* namespace ParameterBlock */
