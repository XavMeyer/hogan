//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConfigFactory.cpp
 *
 * @date May 7, 2015
 * @author meyerx
 * @brief
 */

#include "ConfigFactory.h"
#include "FastFactory.h"
#include "ExtraFastFactory.h"
#include "DefaultFactory.h"
#include "SlowFactory.h"

namespace ParameterBlock {
namespace Config {

ConfigFactory::ConfigFactory() {

}

ConfigFactory::~ConfigFactory() {

}


ConfigFactory::sharedPtr_t ConfigFactory::createDefaultFactory() {
	return sharedPtr_t(new DefaultFactory());
}

ConfigFactory::sharedPtr_t ConfigFactory::createFastFactory() {
	return sharedPtr_t(new FastFactory());
}

ConfigFactory::sharedPtr_t ConfigFactory::createExtraFastFactory() {
	return sharedPtr_t(new ExtraFastFactory());
}

ConfigFactory::sharedPtr_t ConfigFactory::createSlowFactory() {
	return sharedPtr_t(new SlowFactory());
}


} /* namespace Likelihood */
} /* namespace StatisticalModel */
