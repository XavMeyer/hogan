//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightRhoUpdater.h
 *
 * @date Jan 14, 2016
 * @author meyerx
 * @brief
 */
#ifndef LIGHTRHOUPDATER_H_
#define LIGHTRHOUPDATER_H_

#include "../../RMC/DeltaRMC.h"
#include "Parallel/Parallel.h"

#include <string>
#include <sstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "ParameterBlock/BlockStats/Config/Container/ConvCheckerCfg.h"

using namespace boost::accumulators;
using namespace std;

namespace ParameterBlock {

using Config::ConvCheckerCfg;
using Config::UpdaterCfg;

class LightRhoUpdater {
public:
	LightRhoUpdater(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg);
	virtual ~LightRhoUpdater();

	virtual void reset(const bool keepRho=false);
	void setTargetAlpha(const double targetAlpha);
	virtual void addVal(const double meanAlpha);
	void updateSd(const double factor);

	double getRho() const;
	double getTraceAlpha() const;
	unsigned int getT() const;

	void resetAccumulator();

	virtual string toString() const;

protected:

	const UpdaterCfg *updCfg;

	static const uint ACC_WINDOW_SIZE;
	static const double MIN_RHO, MAX_RHO;

	unsigned int N;
	size_t t;
	double alpha, rho, traceAlpha;
	//double a, b, c, d, e;

	accumulator_set<double, stats<tag::rolling_mean > > accAlpha;

	DeltaRMC drmc;
};

} // namespace ParameterBlock


#endif /* LIGHTRHOUPDATER_H_ */
