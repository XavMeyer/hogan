//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightRhoUpdater.cpp
 *
* @date Jan 14, 2016
 * @author meyerx
 * @brief
 */
#include "LightRhoUpdater.h"

namespace ParameterBlock {

const uint LightRhoUpdater::ACC_WINDOW_SIZE = 10;
const double LightRhoUpdater::MIN_RHO = -10.;
const double LightRhoUpdater::MAX_RHO = 2.5;

LightRhoUpdater::LightRhoUpdater(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg) :
	updCfg(aUpdCfg), N(aN), alpha(aAlpha), accAlpha(tag::rolling_mean::window_size = ACC_WINDOW_SIZE), drmc(aUpdCfg)
{
	reset();
}

LightRhoUpdater::~LightRhoUpdater() {
}

void LightRhoUpdater::reset(const bool keepRho) {
	t = 1;
	if(!keepRho) {
		rho = log(-log(0.9));
	}
	resetAccumulator();
	traceAlpha = 0;
}

void LightRhoUpdater:: setTargetAlpha(const double targetAlpha){
	alpha = targetAlpha;
}

void LightRhoUpdater::addVal(const double meanAlpha){
	if(isnan(meanAlpha)) return;
	double delta = (meanAlpha-alpha);
	double gamma = drmc.nextGamma(delta);

	// Make sure rho stay into his bounds
	rho = rho + gamma*delta;
	rho = rho > MAX_RHO ? MAX_RHO : rho;
	rho = rho < MIN_RHO ? MIN_RHO : rho;

	double dblT=t;
	traceAlpha = dblT*traceAlpha/(dblT+1.) + meanAlpha/(dblT+1.);
	accAlpha(meanAlpha);

	++t;
}

void LightRhoUpdater::updateSd(const double factor){
	rho *= factor;
}

double LightRhoUpdater::getRho() const {
	return rho;
}

double LightRhoUpdater::getTraceAlpha() const {
	return traceAlpha;
}

unsigned int LightRhoUpdater::getT() const {
	return t;
}

void LightRhoUpdater::resetAccumulator() {
	for(uint i=0; i<ACC_WINDOW_SIZE; ++i) {
		accAlpha(0);
	}
}

string LightRhoUpdater::toString() const {
	stringstream ss;
	ss << "t=" << t << " : Rho=" << rho << " : gamma=" <<  drmc.getGamma() << " : traceAlpha=" << traceAlpha << " : targetAlpha=" << alpha;
	return ss.str();
}

} // namespace ParameterBlock

