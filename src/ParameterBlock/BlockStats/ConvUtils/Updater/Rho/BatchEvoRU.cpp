//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoRU.cpp
 *
 * @date Jan 14, 2016
 * @author meyerx
 * @brief
 */
#include "BatchEvoRU.h"

namespace ParameterBlock {

BatchEvoRU::BatchEvoRU(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
		LightRhoUpdater(aAlpha, aN, aUpdCfg), mcc(aUpdCfg, aConvCfg)
{
	batchAlpha = 0.;
	tmpT=0;
	reset();
}

BatchEvoRU::~BatchEvoRU() {
}

void BatchEvoRU::addVal(const double meanAlpha) {
	if(isnan(meanAlpha)) return;

	batchAlpha += meanAlpha/(double)updCfg->MEAN_WINDOW;
	++tmpT;

	if(tmpT % updCfg->MEAN_WINDOW == 0) {
		LightRhoUpdater::addVal(meanAlpha);
		mcc.addValue(rho);

		tmpT = 0;
		batchAlpha = 0;
	}
}

void BatchEvoRU::setAverageSd(){
	rho = mcc.getAverage();
}


bool BatchEvoRU::hasConverged() const {
	bool isClose = fabs(alpha-boost::accumulators::rolling_mean(accAlpha)) < 0.02;
	bool isAtBound = (rho == MAX_RHO) || (rho == MIN_RHO);
	/*std::cout << "Conv check : " << boost::accumulators::rolling_mean(accAlpha) << " :: " << fabs(alpha-boost::accumulators::rolling_mean(accAlpha));
	if(isClose && mcc.hasConverged(rho)){
		std::cout << " :: Has conv" << std::endl;
	} else {
		std::cout << " :: Has not conv" << std::endl;
	}*/
	return ((!isAtBound && isClose && mcc.hasConverged(rho) && t > 10) || t >= updCfg->MAX_T);
}

uint BatchEvoRU::getTmpT() const {
	return tmpT;
}

void BatchEvoRU::reset(const bool keepLambda) {
	LightRhoUpdater::reset(keepLambda);
	tmpT = 0;
	mcc.reset();
	drmc.reset();
}

void BatchEvoRU::resetMVCC() {
	mcc.reset();
}

string BatchEvoRU::toString() const {
	stringstream ss;
	ss << LightRhoUpdater::toString();
	ss << " : tmpT=" << tmpT << " : batchA=" << batchAlpha <<  " : conv=";
	if(hasConverged()) {
		ss << "t";
	} else {
		ss << "f";
	}
	ss << endl << mcc.toString();
	return ss.str();
}


} // namespaceParameterBlock

