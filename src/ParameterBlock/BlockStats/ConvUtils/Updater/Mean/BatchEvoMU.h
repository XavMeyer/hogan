//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoMU.h
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#ifndef BATCHEVOMU_H_
#define BATCHEVOMU_H_

#include "../../RMC/DeltaRMC.h"
#include "MeanUpdater.h"
#include "RecursiveMU.h"

namespace ParameterBlock {

class BatchEvoMU: public MeanUpdater {
public:
	BatchEvoMU(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~BatchEvoMU();

	void addVal(const double x);

	void reset(const bool keepMu = false);

	string toString() const;

	size_t getTmpT() const;

	const RobinMonroConv* getRMC() const;

private:
	double tmpMean;
	unsigned int tmpT;
	DeltaRMC drmc;
};

} // namespace ParameterBlock


#endif /* BATCHEVOMU_H_ */
