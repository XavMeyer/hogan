//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeanUpdater.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef MEANUPDATER_H_
#define MEANUPDATER_H_

#include "../../RMC/RobinMonroConv.h"
#include "../../CC/MeanVarCC.h"
#include "../../CC/DynMultiLvlCC.h"

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "ParameterBlock/BlockStats/Config/Container/ConvCheckerCfg.h"

#include <string>
#include <sstream>

using namespace std;

namespace ParameterBlock {

using Config::ConvCheckerCfg;
using Config::UpdaterCfg;

class MeanUpdater {
public:

	MeanUpdater(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~MeanUpdater();

	virtual void addVal(const double x) = 0;

	virtual void reset(const bool keepMu = false);
	void resetMVCC();

	void setInitialMean(const double initMean);

	size_t getT() const;
	double getMean() const;
	bool hasConverged() const;

	virtual string toString() const;

	void relaxTolerance(const double aFactorTol){
		mcc.relaxTolerance(aFactorTol);
	}

	virtual const RobinMonroConv* getRMC() const;

protected:

	const UpdaterCfg *updCfg;
	const ConvCheckerCfg *convCfg;

	size_t t;
	double mean;
	DynMultiLvlCC mcc;
};

} // namespace ParameterBlock


#endif /* MEANUPDATER_H_ */
