//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoMU.cpp
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#include "BatchEvoMU.h"

namespace ParameterBlock {

BatchEvoMU::BatchEvoMU(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg):
		MeanUpdater(aUpdCfg, aConvCfg), drmc(aUpdCfg)
{
	tmpT = 0;
	tmpMean = 0.;
}

BatchEvoMU::~BatchEvoMU() {
}

void BatchEvoMU::addVal(const double x) {

	double dblT = static_cast<double>(tmpT);
	tmpMean = tmpMean * dblT/(dblT+1.) + x/(dblT+1.);
	++tmpT;

	if(tmpT % updCfg->MEAN_WINDOW == 0) {
		double delta = tmpMean - mean;
		double gamma = drmc.nextGamma(delta);
		mean += gamma*delta;
		mcc.addValue(mean);

		tmpT = 0;
		tmpMean = mean;

		++t;
	}
}

void BatchEvoMU::reset(const bool keepMu){
	MeanUpdater::reset(keepMu);
	tmpT = 0;
	tmpMean = mean;
}

string BatchEvoMU::toString() const {
	stringstream ss;
	string parentString = MeanUpdater::toString();
	ss << parentString << " : tmpT=" << tmpT << " : tmpMean=" << tmpMean << " : gamma=" << drmc.getGamma();
	ss << endl << mcc.toString();
	return ss.str();
}

size_t BatchEvoMU::getTmpT() const {
	return tmpT;
}

const RobinMonroConv* BatchEvoMU::getRMC() const {
	return &drmc;
}

} // namespace ParameterBlock

