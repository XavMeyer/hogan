//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvolutiveMU.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "EvolutiveMU.h"

namespace ParameterBlock {

EvolutiveMU::EvolutiveMU(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg)
	: MeanUpdater(aUpdCfg, aConvCfg), drmc(aUpdCfg)
{
}

EvolutiveMU::~EvolutiveMU() {
}

void EvolutiveMU::addVal(const double x) {
	double delta = x - mean;
	double gamma = drmc.nextGamma(delta);
	mean += gamma*delta;
	mcc.addValue(mean);
	++t;
}

const RobinMonroConv* EvolutiveMU::getRMC() const {
	return &drmc;
}

} // namespace ParameterBlock

