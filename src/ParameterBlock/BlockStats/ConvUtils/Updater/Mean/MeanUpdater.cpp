//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeanUpdater.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "MeanUpdater.h"

namespace ParameterBlock {

MeanUpdater::MeanUpdater(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	updCfg(aUpdCfg), convCfg(aConvCfg), mcc(aUpdCfg, aConvCfg)
{
	reset();
}

MeanUpdater::~MeanUpdater() {
}

size_t MeanUpdater::getT() const {
	return t;
}

double MeanUpdater::getMean() const {
	return mean;
}

void MeanUpdater::reset(const bool keepMu) {
	t = 1;
	if(!keepMu) mean = 0.;
	mcc.reset();
}

void MeanUpdater::resetMVCC() {
	mcc.reset();
}

void MeanUpdater::setInitialMean(const double initMean){
	mean = initMean;
}

bool MeanUpdater::hasConverged() const {
	return (mcc.hasConverged(mean) || t >= updCfg->MAX_T) && t > 5;
}

string MeanUpdater::toString() const {
	stringstream ss;
	ss << "t=" << t << " : mean=" << mean << " : conv=";
	if(hasConverged()) {
		ss << "t";
	} else {
		ss << "f";
	}
	return ss.str();
}

const RobinMonroConv* MeanUpdater::getRMC() const {
	return NULL;
}

} // namespace ParameterBlock

