//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoSU.cpp
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#include "BatchEvoSU.h"

namespace ParameterBlock {

const unsigned int BatchEvoSU::INIT_TMP_T = 0;

BatchEvoSU::BatchEvoSU(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg)
	: CovMatrixUpdater(aN, aUpdCfg, aConvCfg), tmpSigma(sigma), drmc(aUpdCfg)
{
	tmpT=INIT_TMP_T;
}

BatchEvoSU::~BatchEvoSU() {
}

void BatchEvoSU::addVals(vector<double> X, const vector<MeanUpdater*> &means) {
	Eigen::Map<Eigen::VectorXd> mappedX(X.data(), X.size());
	Eigen::Map<Eigen::MatrixXd> mapTmpSigma(tmpSigma.data(), N, N);

	for(unsigned int i=0; i<N; ++i){
		X[i] -= means[i]->getMean();
	}

	// Add (X-mu)*(X-mu)^T to tmp
	Eigen::VectorXd copiedX(mappedX);
	Eigen::MatrixXd sigmaEIG(mapTmpSigma);
	sigmaEIG = sigmaEIG - copiedX * copiedX.adjoint();


	// Add tmp to tmp SIGMA
	double gamma1 = drmc.getGamma();
	mapTmpSigma = mapTmpSigma - gamma1*sigmaEIG;

	++tmpT;

	if((tmpT-INIT_TMP_T) % updCfg->MEAN_WINDOW == 0) {
		double gamma2 = drmc.nextGamma();
		// Sigma += gamma*delta
		for(unsigned int i=0; i<N*N; ++i){
			sigma[i] += gamma2*(tmpSigma[i]-sigma[i]);
		}

		hasChanged = true;
		updateMCC();

		// "Reset"
		tmpT = INIT_TMP_T;
		tmpSigma = sigma;

		++t;
	}
}

void BatchEvoSU::reset(const bool keepSigma) {
	CovMatrixUpdater::reset(keepSigma);
	tmpT=INIT_TMP_T;
	tmpSigma = sigma;
	//drmc.reset();
}
uint BatchEvoSU::getTmpT() const {
	return tmpT;
}


string BatchEvoSU::toString() const {
	stringstream ss;
	string parentString = CovMatrixUpdater::toString();
	ss << parentString << " : tmpT=" << tmpT << " : tmpSigma=" << tmpSigma[0] << " : gamma=" << drmc.getGamma() << endl;
	for(uint i=0; i<N; ++i) ss << sigma[i*N+i] << "\t";
	ss << endl << mcc.toString() << endl << mcc2.toString();
	return ss.str();
}

const RobinMonroConv* BatchEvoSU::getRMC() const {
	return &drmc;
}

} // namespace ParameterBlock

