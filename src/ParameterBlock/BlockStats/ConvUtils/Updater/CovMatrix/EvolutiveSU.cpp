//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvolutiveSU.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "EvolutiveSU.h"

namespace ParameterBlock {

EvolutiveSU::EvolutiveSU(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	CovMatrixUpdater(aN, aUpdCfg, aConvCfg), drmc(aUpdCfg)
{

}

EvolutiveSU::~EvolutiveSU() {
}

void EvolutiveSU::addVals(vector<double> X, const vector<MeanUpdater*> &means) {
	// Delta is a copy of Sigma (instead of -Sigma)
	vector<double> delta(sigma);
	double gamma = drmc.nextGamma();

	Eigen::Map<Eigen::VectorXd> mappedX(X.data(), X.size());
	Eigen::Map<Eigen::MatrixXd> mapSigma(sigma.data(), N, N);

	for(unsigned int i=0; i<N; ++i){
		X[i] -= means[i]->getMean();
	}

	// Add (X-mu)*(X-mu)^T to tmp
	Eigen::VectorXd copiedX(mappedX);
	Eigen::MatrixXd sigmaEIG(mapSigma);
	sigmaEIG = sigmaEIG - copiedX * copiedX.adjoint();

	// Since we have Sigma instead of -Sigma we substract ((X-mu)*(X-mu)^T)
	// delta = Sigma + (-1)*((X-mu)*(X-mu)^T)
	mapSigma = mapSigma - gamma*sigmaEIG;

	hasChanged = true;
	updateMCC();

	++t;
}

const RobinMonroConv* EvolutiveSU::getRMC() const {
	return &drmc;
}

} // namespace ParameterBlock

