//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoSU.h
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#ifndef BATCHEVOSU_H_
#define BATCHEVOSU_H_

#include "../../RMC/DefaultRMC.h"
#include "CovMatrixUpdater.h"
#include <Eigen/Core>

namespace ParameterBlock {

class BatchEvoSU: public CovMatrixUpdater {
public:
	BatchEvoSU(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~BatchEvoSU();

	void reset(const bool keepSigma=true);
	void addVals(vector<double> X, const vector<MeanUpdater*> &means);

	uint getTmpT() const;

	string toString() const;

	const RobinMonroConv* getRMC() const;

private:

	static const unsigned int INIT_TMP_T;

	unsigned int tmpT;
	vector<double> tmpSigma;
	DefaultRMC drmc;
};

} // namespace ParameterBlock


#endif /* BATCHEVOSU_H_ */
