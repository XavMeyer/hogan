//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CovMatrixUpdater.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef COVMATRIXUPDATER_H_
#define COVMATRIXUPDATER_H_

#include "Parallel/Parallel.h"

#include "../Mean/MeanUpdater.h"
#include "../../CC/MeanVarCC.h"
#include "../../CC/DynMultiLvlCC.h"

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "ParameterBlock/BlockStats/Config/Container/ConvCheckerCfg.h"

#include <vector>

using namespace std;

namespace ParameterBlock {

using Config::ConvCheckerCfg;
using Config::UpdaterCfg;

class CovMatrixUpdater {
public:
	CovMatrixUpdater(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~CovMatrixUpdater();

	virtual void addVals(vector<double> X, const vector<MeanUpdater*> &means) = 0;

	virtual void reset(const bool keepSigma=true);
	void resetMVCC();
	void setInitialSigma(const double initSigma);
	void setInitialSigma(const unsigned int idx, const double initSigma);
	void setInitialSigma(const vector<double> &initSigma);
	void cleanCorrelation(const double threshold=0.1);

	bool hasConverged() const;
	size_t getT() const;
	const double* getSigma() const;
	double getSigma(const unsigned int idx) const;
	double getMeanDiag() const;
	double getMeanCorrelation() const;

	bool updated() const;
	void resetUpdated();

	virtual string toString() const;

	void relaxTolerance(const double aFactorTol){
		mcc.relaxTolerance(aFactorTol);
		mcc2.relaxTolerance(2.5*aFactorTol);
	}

	void relaxTolerance(const double aFactTolDiag, const double aFactTolCorrel){
		mcc.relaxTolerance(aFactTolDiag);
		mcc2.relaxTolerance(aFactTolCorrel);
	}

	virtual const RobinMonroConv* getRMC() const;

protected:

	const UpdaterCfg *updCfg;
	const ConvCheckerCfg *convCfg;

	size_t t;
	bool hasChanged;
	const size_t N;
	vector<double> sigma;
	DynMultiLvlCC mcc;
	DynMultiLvlCC mcc2;

	void updateMCC();
};

} // namespace ParameterBlock


#endif /* COVMATRIXUPDATER_H_ */
