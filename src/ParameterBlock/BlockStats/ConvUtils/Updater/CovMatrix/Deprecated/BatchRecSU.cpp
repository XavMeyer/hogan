//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchRecSU.cpp
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#include "BatchRecSU.h"

/*extern "C" {
	void dger_(	int *m, int *n, double *alpha,
				double *x, int *incx, double *y, int *incy,
				double *a, int *lda);

	void dgeev_( char* jobvl, char* jobvr, int* n, double* a,
	            int* lda, double* wr, double* wi, double* vl, int* ldvl,
	            double* vr, int* ldvr, double* work, int* lwork, int* info );
}*/

namespace ParameterBlock {

const unsigned int BatchRecSU::INIT_TMP_T = 1;

BatchRecSU::BatchRecSU(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg)
	: CovMatrixUpdater(aN, aUpdCfg, aConvCfg), tmpSigma(sigma), mu(N,0.), matMu(N*N, 0.), drmc(aUpdCfg)
{
	tmpT=INIT_TMP_T;
}

BatchRecSU::~BatchRecSU() {
}

void BatchRecSU::addVals(vector<double> X, const vector<MeanUpdater*> &means) {
	double dblT = tmpT;
	// tmp = matMu
	vector<double> tmp(matMu);
	Eigen::Map<Eigen::MatrixXd> mappedTmp(tmp.data(), N, N);
	Eigen::Map<Eigen::MatrixXd> mappedMatMu(matMu.data(), N, N);
	Eigen::Map<Eigen::MatrixXd> mappedTmpSigma(tmpSigma.data(), N, N);
	Eigen::Map<Eigen::VectorXd> mappedX(X.data(), N);
	Eigen::Map<Eigen::VectorXd> mappedMu(mu.data(), N);

	Eigen::VectorXd copiedX(mappedX);
	Eigen::VectorXd copiedMu(mappedMu);


	for(unsigned int i=0; i<N; ++i){
		mu[i] = means[i]->getMean();
	}

	// tmp = tmp*dblT
	mappedTmp = mappedTmp*dblT;

	// Add X*X^T to tmp
	mappedTmp = mappedTmp + copiedX * copiedX.adjoint();

	// Process matMu t
	mappedMatMu = copiedMu * copiedMu.adjoint();

	double c1 = dblT+1.;
	double c2 = (dblT-1.)/dblT;
	mappedTmp = mappedTmp - c1*mappedMatMu;
	mappedTmpSigma *= c2;
	mappedTmpSigma += mappedTmp/dblT;
	++tmpT;

	if((tmpT-INIT_TMP_T) % updCfg->MEAN_WINDOW == 0) {
		double gamma = drmc.nextGamma();
		// Sigma += gamma*delta
		for(unsigned int i=0; i<N*N; ++i){
			sigma[i] += gamma*(tmpSigma[i]-sigma[i]);
		}

		hasChanged = true;
		updateMCC();

		// "Reset"
		tmpT = INIT_TMP_T;
		for(unsigned int i=0; i<N; ++i){
			mu[i] = means[i]->getMean();
		}

		Eigen::VectorXd copiedMu2(mappedMu);
		mappedMatMu = copiedMu2 * copiedMu2.adjoint();

		tmpSigma = sigma;

		++t;
	}
}

void BatchRecSU::reset(const bool keepSigma) {
	CovMatrixUpdater::reset(keepSigma);
	tmpT=INIT_TMP_T;
	if(!keepSigma){
		for(unsigned int i=0; i<N*N; ++i){
			matMu[i] = 0.;
		}
	}
	//drmc.reset();
}

void BatchRecSU::setInitialMeans(const vector<MeanUpdater*> &means) {

	for(unsigned int i=0; i<N; ++i){
		mu[i] = means[i]->getMean();
	}

	Eigen::Map<Eigen::MatrixXd> mappedMatMu(matMu.data(), N, N);
	Eigen::Map<Eigen::VectorXd> mappedMu(mu.data(), N);

	Eigen::VectorXd copiedMu(mappedMu);

	mappedMatMu = copiedMu*copiedMu.adjoint();
}

uint BatchRecSU::getTmpT() const {
	return tmpT;
}

string BatchRecSU::toString() const {
	stringstream ss;
	string parentString = CovMatrixUpdater::toString();
	ss << parentString << " : tmpT=" << tmpT << " : tmpSigma=" << tmpSigma[0] << " : gamma=" << drmc.getGamma() << endl;
	for(uint i=0; i<N; ++i) ss << sigma[i*N+i] << "\t";
	//ss << endl << mcc.toString();
	return ss.str();
}

} // namespace ParameterBlock

