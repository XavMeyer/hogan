//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CovMatrixUpdater.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "CovMatrixUpdater.h"

namespace ParameterBlock {

CovMatrixUpdater::CovMatrixUpdater(const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	updCfg(aUpdCfg), convCfg(aConvCfg), N(aN), mcc(aUpdCfg, aConvCfg),  mcc2(aUpdCfg, aConvCfg)
{
	sigma.resize(N*N, 0.);
	reset();
}

CovMatrixUpdater::~CovMatrixUpdater() {
}

size_t CovMatrixUpdater::getT() const {
	return t;
}

const double* CovMatrixUpdater::getSigma() const {
	return &sigma[0];
}

double CovMatrixUpdater::getSigma(const unsigned int idx) const {
	return sigma[idx*N+idx];
}

bool CovMatrixUpdater::hasConverged() const {
	bool conv = mcc.hasConverged(getMeanDiag());
	if(N>1) {
		conv = conv && mcc2.hasConverged(getMeanCorrelation());
	}

	return (conv  || t >= updCfg->MAX_T) && t > 5;
}

void CovMatrixUpdater::setInitialSigma(const double initSigma) {
	for(unsigned int i=0; i<N; ++i){
		sigma[i*N+i] = initSigma;
	}
	hasChanged = true;
}

void CovMatrixUpdater::setInitialSigma(const unsigned int idx, const double initSigma) {
		sigma[idx*N+idx] = initSigma;
		hasChanged = true;
}

void CovMatrixUpdater::setInitialSigma(const vector<double> &initSigma) {
	for(unsigned int i=0; i<N*N; ++i){
		sigma[i] = initSigma[i];
	}
	hasChanged = true;
}

void CovMatrixUpdater::cleanCorrelation(const double threshold) {
	for(unsigned int i=0; i<N; ++i){
		for(unsigned int j=0; j<N; ++j){
			double correl = sigma[i*N+j] / (sqrt(sigma[i*N+i])*sqrt(sigma[j*N+j]));
			if(fabs(correl) <= threshold) {
				sigma[i*N+j] = 0.;
			}
		}
	}
}

void CovMatrixUpdater::reset(const bool keepSigma) {
	hasChanged = false;
	t=1;
	if(!keepSigma){
		for(unsigned int i=0; i<N*N; ++i){
			sigma[i] = 0.;
		}
	}
	mcc.reset();
	mcc2.reset();
}

void CovMatrixUpdater::resetMVCC() {
	mcc.reset();
	mcc2.reset();
}

void CovMatrixUpdater::updateMCC() {
	mcc.addValue(getMeanDiag());
	if(N>1) {
		mcc2.addValue(getMeanCorrelation());
	}
}

double CovMatrixUpdater::getMeanDiag() const {
	double meanDiag = 0.;
	double dblN = static_cast<double>(N);

	for(unsigned int i=0; i<N; ++i){
		meanDiag += sigma[i*N+i]/dblN;
	}
	return meanDiag;
}

double CovMatrixUpdater::getMeanCorrelation() const {
	double meanCorrel = 0.;
	double denom = static_cast<double>(N*(N-1))/2.;

	for(unsigned int i=0; i<N; ++i){
		for(unsigned int j=i+1; j<N; ++j){
			meanCorrel += (sigma[i*N+j] / (sqrt(sigma[i*N+i])*sqrt(sigma[j*N+j])))/denom;
		}
	}
	return meanCorrel;
}

bool CovMatrixUpdater::updated() const {
	return hasChanged;
}

void CovMatrixUpdater::resetUpdated() {
	hasChanged = false;
}

string CovMatrixUpdater::toString() const {
	stringstream ss;
	ss << "t=" << getT() << " : sigma=" << sigma[0]<< " : conv=";
	if(hasConverged()) {
		ss << "t";
	} else {
		ss << "f";
	}
	return ss.str();
}

const RobinMonroConv* CovMatrixUpdater::getRMC() const {
	return NULL;
}

} // namespace ParameterBlock

