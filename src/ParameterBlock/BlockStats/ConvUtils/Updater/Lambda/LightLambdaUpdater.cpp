//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightLambdaUpdater.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "LightLambdaUpdater.h"

namespace ParameterBlock {

const uint LightLambdaUpdater::ACC_WINDOW_SIZE = 6;

LightLambdaUpdater::LightLambdaUpdater(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg) :
	updCfg(aUpdCfg), N(aN), alpha(aAlpha), accAlpha(tag::rolling_mean::window_size = ACC_WINDOW_SIZE), drmc(aUpdCfg)
{
	reset();
}

LightLambdaUpdater::~LightLambdaUpdater() {
}

void LightLambdaUpdater::reset(const bool keepLambda) {
	t = 1;
	if(!keepLambda) {
		Sd = (2.4*2.4)/(double)N;
	}
	resetAccumulator();
	traceAlpha = 0;
}

void LightLambdaUpdater:: setTargetAlpha(const double targetAlpha){
	alpha = targetAlpha;
}

void LightLambdaUpdater::addVal(const double meanAlpha){
	if(isnan(meanAlpha)) return;
	double delta = (meanAlpha-alpha);
	double gamma = drmc.nextGamma(delta);
	Sd = exp(log(Sd) + gamma*delta);

	double dblT=t;
	traceAlpha = dblT*traceAlpha/(dblT+1.) + meanAlpha/(dblT+1.);
	accAlpha(meanAlpha);

	++t;
}

void LightLambdaUpdater::updateSd(const double factor){
	Sd *= factor;
}

double LightLambdaUpdater::getSd() const {
	return Sd;
}

double LightLambdaUpdater::getTraceAlpha() const {
	return traceAlpha;
}

unsigned int LightLambdaUpdater::getT() const {
	return t;
}

void LightLambdaUpdater::resetAccumulator() {
	for(uint i=0; i<ACC_WINDOW_SIZE; ++i) {
		accAlpha(0);
	}
}

string LightLambdaUpdater::toString() const {
	stringstream ss;
	ss << "t=" << t << " : SD=" << Sd << " : gamma=" <<  drmc.getGamma() << " : traceAlpha=" << traceAlpha << " : targetAlpha=" << alpha;
	return ss.str();
}

const RobinMonroConv* LightLambdaUpdater::getRMC() const {
	return &drmc;
}

} // namespace ParameterBlock

