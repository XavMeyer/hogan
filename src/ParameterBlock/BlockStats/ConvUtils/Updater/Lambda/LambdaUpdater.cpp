//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LambdaUpdater.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "LambdaUpdater.h"

namespace ParameterBlock {

LambdaUpdater::LambdaUpdater(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	LightLambdaUpdater(aAlpha, aN, aUpdCfg), mcc(aUpdCfg, aConvCfg)
{
	reset();
}

LambdaUpdater::~LambdaUpdater() {
}

void LambdaUpdater::addVal(const double meanAlpha){
	LightLambdaUpdater::addVal(meanAlpha);
	mcc.addValue(Sd);
}

void LambdaUpdater::setAverageSd(){
	Sd = mcc.getAverage();
}


bool LambdaUpdater::hasConverged() const {
	return mcc.hasConverged(Sd) || t >= updCfg->MAX_T;
}

void LambdaUpdater::reset(const bool keepLambda) {
	LightLambdaUpdater::reset(keepLambda);
	mcc.reset();
}

} // namespace ParameterBlock

