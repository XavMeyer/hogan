//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchEvoLU.cpp
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#include "BatchEvoLU.h"

namespace ParameterBlock {

BatchEvoLU::BatchEvoLU(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	LightLambdaUpdater(aAlpha, aN, aUpdCfg), mcc(aUpdCfg, aConvCfg)
{
	batchAlpha = 0.;
	tmpT=0;
	reset();
}

BatchEvoLU::~BatchEvoLU() {
}

void BatchEvoLU::addVal(const double meanAlpha) {
	if(isnan(meanAlpha)) return;

	batchAlpha += meanAlpha/(double)updCfg->MEAN_WINDOW;
	++tmpT;

	if(tmpT % updCfg->MEAN_WINDOW == 0) {
		LightLambdaUpdater::addVal(meanAlpha);
		mcc.addValue(Sd);

		tmpT = 0;
		batchAlpha = 0;
	}
}

void BatchEvoLU::setAverageSd(){
	Sd = mcc.getAverage();
}


bool BatchEvoLU::hasConverged() const {
	bool isClose = fabs(alpha-boost::accumulators::rolling_mean(accAlpha)) < 0.01;
	return ((isClose && mcc.hasConverged(Sd)) || t >= updCfg->MAX_T);
}

uint BatchEvoLU::getTmpT() const {
	return tmpT;
}

void BatchEvoLU::reset(const bool keepLambda) {
	LightLambdaUpdater::reset(keepLambda);
	tmpT = 0;
	batchAlpha = 0.;
	mcc.reset();
	drmc.reset();
}

void BatchEvoLU::resetMVCC() {
	mcc.reset();
}

string BatchEvoLU::toString() const {
	stringstream ss;
	ss << LightLambdaUpdater::toString();
	ss << " : tmpT=" << tmpT << " : batchA=" << batchAlpha <<  " : conv=";
	if(hasConverged()) {
		ss << "t";
	} else {
		ss << "f";
	}
	ss << endl << mcc.toString();
	return ss.str();
}

} // namespaceParameterBlock

