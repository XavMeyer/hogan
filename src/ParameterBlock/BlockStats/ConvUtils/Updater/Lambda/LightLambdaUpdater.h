//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LightLambdaUpdater.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef LIGHTLAMBDAUPDATER_H_
#define LIGHTLAMBDAUPDATER_H_

#include "../../RMC/DeltaRMC.h"
#include "Parallel/Parallel.h"

#include <string>
#include <sstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "ParameterBlock/BlockStats/Config/Container/ConvCheckerCfg.h"

using namespace boost::accumulators;
using namespace std;

namespace ParameterBlock {

using Config::ConvCheckerCfg;
using Config::UpdaterCfg;

class LightLambdaUpdater {
public:
	LightLambdaUpdater(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg);
	virtual ~LightLambdaUpdater();

	virtual void reset(const bool keepLambda=false);
	void setTargetAlpha(const double targetAlpha);
	virtual void addVal(const double meanAlpha);
	void updateSd(const double factor);

	double getSd() const;
	double getTraceAlpha() const;
	unsigned int getT() const;

	void resetAccumulator();

	virtual string toString() const;

	const RobinMonroConv* getRMC() const;

protected:

	const UpdaterCfg *updCfg;

	static const uint ACC_WINDOW_SIZE;

	unsigned int N;
	size_t t;
	double alpha, Sd, traceAlpha;
	//double a, b, c, d, e;

	accumulator_set<double, stats<tag::rolling_mean > > accAlpha;

	DeltaRMC drmc;
};

} // namespace ParameterBlock


#endif /* LIGHTLAMBDAUPDATER_H_ */
