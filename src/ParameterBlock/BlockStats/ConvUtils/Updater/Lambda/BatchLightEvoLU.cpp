//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchLightEvoLU.cpp
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchLightEvoLU.h"

namespace ParameterBlock {

BatchLightEvoLU::BatchLightEvoLU(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg) :
	LightLambdaUpdater(aAlpha, aN, aUpdCfg)
{
	tmpT=0;
	batchAlpha = 0.;
}

BatchLightEvoLU::~BatchLightEvoLU() {
}

void BatchLightEvoLU::addVal(const double meanAlpha, const bool print) {
	if(isnan(meanAlpha)) return;

	batchAlpha += meanAlpha/(double)updCfg->MEAN_WINDOW;
	++tmpT;

	if(tmpT % updCfg->MEAN_WINDOW == 0) {
		LightLambdaUpdater::addVal(batchAlpha);

		tmpT = 0;
		batchAlpha = 0;
	}
}

uint BatchLightEvoLU::getTmpT() const {
	return tmpT;
}

void BatchLightEvoLU::reset(const bool keepLambda) {
	LightLambdaUpdater::reset(keepLambda);
	tmpT = 0;
	batchAlpha = 0.;
}

string BatchLightEvoLU::toString() const {
	stringstream ss;
	ss << "t=" << t << " : SD=" << Sd << " : gamma=" <<  drmc.getGamma() << " : traceAlpha=" << traceAlpha;
	ss << " : bAlpha=" << batchAlpha << " : alpha=" << alpha;
	return ss.str();
}

} // namespace ParameterBlock

