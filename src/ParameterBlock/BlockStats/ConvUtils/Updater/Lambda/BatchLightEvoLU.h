//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BatchLightEvoLU.h
 *
 * @date Jun 12, 2014
 * @author meyerx
 * @brief
 */
#ifndef BATCHLIGHTEVOLU_H_
#define BATCHLIGHTEVOLU_H_

#include "LightLambdaUpdater.h"

namespace ParameterBlock {

class BatchLightEvoLU: public LightLambdaUpdater {
public:
	BatchLightEvoLU(const double aAlpha, const unsigned int aN, const UpdaterCfg *aUpdCfg);
	~BatchLightEvoLU();

	void reset(const bool keepLambda=false);
	void addVal(const double meanAlpha, const bool print=false);

	uint getTmpT() const;

	string toString() const;

private:
	unsigned int tmpT;
	double batchAlpha;

};

} // namespace ParameterBlock

#endif /* BATCHLIGHTEVOLU_H_ */
