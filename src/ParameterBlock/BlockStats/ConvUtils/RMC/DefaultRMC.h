//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DefaultRMC.h
 *
 * @date Jun 10, 2014
 * @author meyerx
 * @brief
 */
#ifndef DEFAULTRMC_H_
#define DEFAULTRMC_H_

#include "RobinMonroConv.h"

namespace ParameterBlock {

class DefaultRMC: public RobinMonroConv {
public:
	DefaultRMC(const UpdaterCfg *aUpdCfg);
	virtual ~DefaultRMC();

	double nextGamma();
	double getGamma() const;

private:

};

} // namespace ParameterBlock

#endif /* DEFAULTRMC_H_ */
