//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConvergenceChecker.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef CONVERGENCECHECKER_H_
#define CONVERGENCECHECKER_H_

#include <cmath>
#include <vector>
#include <list>
using namespace std;

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "ParameterBlock/BlockStats/Config/Container/ConvCheckerCfg.h"

using namespace boost::accumulators;

namespace ParameterBlock {

using Config::ConvCheckerCfg;
using Config::UpdaterCfg;

class ConvergenceChecker {
public:
	ConvergenceChecker(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~ConvergenceChecker();

	virtual void addValue(const double val) = 0;
	virtual bool hasConverged(const double val) const = 0;

	double getAverage() const {return sum/(double)updCfg->L_WINDOW;}

protected:

	const UpdaterCfg *updCfg;
	const ConvCheckerCfg *convCfg;

	double sum;

};

} // namespace ParameterBlock


#endif /* CONVERGENCECHECKER_H_ */
