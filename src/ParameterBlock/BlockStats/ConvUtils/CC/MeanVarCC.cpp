//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MeanVarCC.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "MeanVarCC.h"

namespace ParameterBlock {

MeanVarCC::MeanVarCC(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	ConvergenceChecker(aUpdCfg, aConvCfg), vals(updCfg->L_WINDOW, 0.)
{
	sum = sumSq = 0.;
}

MeanVarCC::~MeanVarCC() {
}

void MeanVarCC::addValue(const double val) {
	sum -= vals.front();
	sum += val;
	sumSq -= vals.front()*vals.front();
	sumSq += val*val;

	// Update list
	vals.pop_front();
	vals.push_back(val);
}

bool MeanVarCC::hasConverged(const double val) const {
	double mean = sum / (double)updCfg->L_WINDOW;
	double std = sqrt(sumSq / (double)updCfg->L_WINDOW - mean*mean);
	double relSTD = std/mean;
	return relSTD < convCfg->MEAN_TH && fabs(1.-val/mean) < updCfg->EPSILON;
}

void MeanVarCC::reset() {
	sum = sumSq = 0.;
	vals.assign(vals.size(), 0.);
}


string MeanVarCC::toString() const {
	double mean = sum / (double)updCfg->L_WINDOW;
	double std = sqrt(sumSq / (double)updCfg->L_WINDOW - mean*mean);
	double relSTD = std/mean;
	stringstream ss;
	ss << "Size=" << updCfg->L_WINDOW << " : Mean=" << mean << " : relSTD=" << relSTD << endl;
	return ss.str();
}

} // namespace ParameterBlock

