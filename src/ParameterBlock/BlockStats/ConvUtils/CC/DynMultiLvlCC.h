//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DynMultiLvlCC.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef DYNMULTILVLCC_H_
#define DYNMULTILVLCC_H_

#include <list>
#include <string>
#include <sstream>

#include "ConvergenceChecker.h"
#include "Parallel/Parallel.h"

using namespace std;

namespace ParameterBlock {

class DynMultiLvlCC: public ConvergenceChecker {
public:
	DynMultiLvlCC(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg);
	virtual ~DynMultiLvlCC();

	void addValue(const double val);
	bool hasConverged(const double val) const;
	void reset();

	void relaxTolerance(const double factor);

	string toString() const;
	string toStringDBG(const double val) const;

protected:
	double sumSq, factorTol;
	list<double> vals, bMeans, bRSDs;

	accumulator_set<double, stats<tag::variance> > acc;

private:

	size_t t;

	void updateWindowed(const double val);
	void updateBatch(const double val);
};

} // namespace ParameterBlock

#endif /* DYNMULTILVLCC_H_ */
