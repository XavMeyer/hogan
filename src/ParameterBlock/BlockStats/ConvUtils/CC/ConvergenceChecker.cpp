//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ConvergenceChecker.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "ConvergenceChecker.h"

namespace ParameterBlock {

ConvergenceChecker::ConvergenceChecker(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
		updCfg(aUpdCfg), convCfg(aConvCfg)
{
	sum=0.;
}

ConvergenceChecker::~ConvergenceChecker() {
}

} // namespace ParameterBlock

