//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DynMultiLvlCC.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "DynMultiLvlCC.h"

namespace ParameterBlock {

DynMultiLvlCC::DynMultiLvlCC(const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
	ConvergenceChecker(aUpdCfg, aConvCfg), vals(updCfg->L_WINDOW, 0.), bMeans(0, 0.), bRSDs(0, 0.)
{
	t=0;
	factorTol = 1.0;
	sum = sumSq = 0.;
}

DynMultiLvlCC::~DynMultiLvlCC() {
}

void DynMultiLvlCC::updateWindowed(const double val) {
	// Update sum and sum square
	sum -= vals.front();
	sum += val;
	sumSq -= vals.front()*vals.front();
	sumSq += val*val;

	// Update list
	vals.pop_front();
	vals.push_back(val);
	t++;
}

void DynMultiLvlCC::updateBatch(const double val) {
	acc(val);
	// Process mean and variance for batch of bSize values
	if(boost::accumulators::count(acc) >= updCfg->L_BATCH) {
		// Keep track of last updCfg->NB_BATCH batch RelSD
		bMeans.push_back(boost::accumulators::mean(acc));
		bRSDs.push_back(sqrt(boost::accumulators::variance(acc))/boost::accumulators::mean(acc));

		if(bMeans.size() >= updCfg->NB_BATCH){
			bMeans.pop_front();
			bRSDs.pop_front();
		}

		acc = accumulator_set<double, stats<tag::variance> >();
	}
}

void DynMultiLvlCC::addValue(const double val) {
	updateBatch(val);
	updateWindowed(val);

	/*if(pMCMC::mpiMgr().isMainProcessor()){
		cout << toStringDBG(val);
	}*/
}

bool DynMultiLvlCC::hasConverged(const double val) const {

	// Local values (windowed)
	double mean = sum / (double)updCfg->L_WINDOW;
	double std = sqrt(sumSq / (double)updCfg->L_WINDOW - mean*mean);
	double relSD = std/mean;
	// windowed RSD
	double distWR = std::max(relSD - convCfg->RSD_TH, 0.)/convCfg->RSD_TH;

	// Instant values (punctual)
	// Val / windowed mean ratio
	double distVW = std::max(fabs(1.-(val/mean)) - convCfg->MEAN_TH, 0.)/convCfg->MEAN_TH;

	// Global values (batch)
	double distBRSDM = 0., distBRSDR = 0., distWB = 0.;
	if(bMeans.size() > 2) {
		accumulator_set<double, stats<tag::variance> > tmpAccM;
		tmpAccM = std::for_each( bMeans.begin(), bMeans.end(), tmpAccM );
		// RSD of batch mean
		distBRSDM = sqrt(boost::accumulators::variance(tmpAccM))/boost::accumulators::mean(tmpAccM);
		distBRSDM = std::max(distBRSDM - convCfg->MRSD_TH, 0.)/convCfg->MRSD_TH;

		accumulator_set<double, stats<tag::variance> > tmpAccR;
		tmpAccR = std::for_each(bRSDs .begin(), bRSDs.end(), tmpAccR );
		// SD of batch RSD
		distBRSDR = sqrt(boost::accumulators::variance(tmpAccR));
		distBRSDR = std::max(distBRSDR - convCfg->RSD_TH, 0.)/convCfg->RSD_TH;

		// windowed / average batch mean ratio
		distWB = std::max(fabs(1.-(mean/boost::accumulators::mean(tmpAccM))) - 2.*convCfg->MEAN_TH, 0.)/(2.*convCfg->MEAN_TH);
	}

	// Weight to scale each expected value to ~0.1
	double totalDist = distVW + distWB + distWR + distBRSDR + distBRSDM;

	double tolerance = updCfg->EPSILON;
	if(t > convCfg->TOL_RELAX_TH){
		tolerance *= pow((double)t/(double)convCfg->TOL_RELAX_TH, 1.5);
	}

	return totalDist < factorTol*tolerance && t > 1.5*updCfg->L_BATCH;
}

void DynMultiLvlCC::reset() {
	factorTol = 1.0;
	sum = sumSq = 0.;
	vals.assign(vals.size(), 0.);
	bMeans.assign(0, 0.);
	bRSDs.assign(0, 0.);
}

void DynMultiLvlCC::relaxTolerance(const double factor){
	factorTol = factor;
}


string DynMultiLvlCC::toString() const {
	double mean = sum / (double)updCfg->L_WINDOW;
	double std = sqrt(sumSq / (double)updCfg->L_WINDOW - mean*mean);
	double relSTD = std/mean;
	stringstream ss; ss.precision(4);
	double tolerance = updCfg->EPSILON;
	if(t > 3.*updCfg->NB_BATCH*updCfg->L_BATCH){
		tolerance *= pow((double)t/(3.*(double)updCfg->NB_BATCH*(double)updCfg->L_BATCH), 2);
	}
	ss << "Size=" << updCfg->L_WINDOW << " : Mean=" << mean << " : relSTD=" << relSTD << " : tolerance =" << tolerance << " : factTol = " << factorTol << endl;
	return ss.str();
}

string DynMultiLvlCC::toStringDBG(const double val) const {
	// Local values (windowed)
	double mean = sum / (double)updCfg->L_WINDOW;
	double std = sqrt(sumSq / (double)updCfg->L_WINDOW - mean*mean);
	double relSD = std/mean;
	// windowed RSD
	double distWR = std::max(relSD - convCfg->RSD_TH, 0.)/convCfg->RSD_TH;

	// Instant values (punctual)
	// Val / windowed mean ratio
	double distVW = std::max(fabs(1.-(val/mean)) - convCfg->MEAN_TH, 0.)/convCfg->MEAN_TH;

	// Global values (batch)
	double distBRSDM = 0., distBRSDR = 0., distWB = 0.;
	accumulator_set<double, stats<tag::variance> > tmpAccM;
	accumulator_set<double, stats<tag::variance> > tmpAccR;
	if(bMeans.size() > 2) {

		tmpAccM = std::for_each( bMeans.begin(), bMeans.end(), tmpAccM );
		// RSD of batch mean
		distBRSDM = sqrt(boost::accumulators::variance(tmpAccM))/boost::accumulators::mean(tmpAccM);
		distBRSDM = std::max(distBRSDM - convCfg->MRSD_TH, 0.)/convCfg->MRSD_TH;

		tmpAccR = std::for_each(bRSDs .begin(), bRSDs.end(), tmpAccR );
		// SD of batch RSD
		distBRSDR = sqrt(boost::accumulators::variance(tmpAccR));
		distBRSDR = std::max(distBRSDR - convCfg->RSD_TH, 0.)/convCfg->RSD_TH;

		// windowed / average batch mean ratio
		distWB = std::max(fabs(1.-(mean/boost::accumulators::mean(tmpAccM))) - 2.*convCfg->MEAN_TH, 0.)/(2.*convCfg->MEAN_TH);
	}

	// Weight to scale each expected value to ~0.1
	double totalDist = distVW + distWB + distWR + distBRSDR + distBRSDM;

	double tolerance = updCfg->EPSILON;
	if(t > convCfg->TOL_RELAX_TH){
		tolerance *= pow((double)t/(double)convCfg->TOL_RELAX_TH, 1.5);
	}

	// vector MLB IDX
	// 	1		2		3		4		5		6		7		8		9		10		11		12		13		14		15		16		17		18
	//	X		MU		SD		RSD		dVW		dWB		dWRSD	BM		BRSD	BMM		BMSD	BMRSD	BMDIST	BRM		BRSD	BRRSD	BRDist	totalDist

	stringstream ss; ss.precision(4);
	ss << "DBG >>\t";
	// local [mean SD RSD distVW dist WB]
	ss << val << "\t" << mean << "\t" << std << "\t" << relSD << "\t" << distVW << "\t" << distWB << "\t" << distWR << "\t";
	// batch [mean RSD]
	ss << bMeans.back() << "\t" << bRSDs.back() << "\t";
	// batch Mean [mean SD RSD dist]
	ss << boost::accumulators::mean(tmpAccM) << "\t" << sqrt(boost::accumulators::variance(tmpAccM)) << "\t" << sqrt(boost::accumulators::variance(tmpAccM))/boost::accumulators::mean(tmpAccM) << "\t" << distBRSDM << "\t";
	// batch Var [mean SD RSD dist]
	ss << boost::accumulators::mean(tmpAccR) << "\t" << sqrt(boost::accumulators::variance(tmpAccR)) << "\t" << sqrt(boost::accumulators::variance(tmpAccR))/boost::accumulators::mean(tmpAccR) << "\t" << distBRSDR << "\t";
	// Final dist
	ss << totalDist << endl << "****************************************" << endl;
	return ss.str();
}

} // namespace ParameterBlock

