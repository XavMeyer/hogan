//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoPCABS.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef EVOPCABS_H_
#define EVOPCABS_H_

#include "BlockStatsInterface.h"
#include "../BlockModifier/BlockModifierInterface.h"

#include "ConvUtils/Updater/Lambda/BatchEvoLU.h"
#include "ConvUtils/Updater/Lambda/BatchLightEvoLU.h"
#include "ConvUtils/Updater/Mean/BatchEvoMU.h"
#include "ConvUtils/Updater/CovMatrix/BatchEvoSU.h"

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

namespace ParameterBlock {

class EvoPCABS: public BlockStats {
public:
	typedef boost::shared_ptr<EvoPCABS> EvoPCABSPtr_t;

public:
	EvoPCABS(double aAlpha, const Block::sharedPtr_t aBlock, const BlockStatCfg::sharedPtr_t aCfg);
	EvoPCABS(const EvoPCABS &toCopy);
	~EvoPCABS();

	void initSample(const Sample &sample);
	void update(const Sample &sample, const unsigned int nTimes=1);
	void updateSD(const double aAlpha);
	void updateLocSD(const vector<double> &aAlphas);

	bool isReady();
	bool isReadyForLocUpdate();

	bool isDisabled(const unsigned int idx) const;
	double getMu(size_t idx) const;
	double getSd() const;
	double getLocSd(unsigned int idx) const;
	double getSigmaIdx(unsigned int idx) const;
	const double* getSigma() const;

	void reduceLocSD(uint idx, const double factor);

	void reset();
	void resetMVCC();
	void resetForCorrel();
	void resetForIndep();

	bool hasCorrelation() const;
	bool hasDisabledFactors() const;

	void relaxToleranceSigma(const double tolFactor,  const double tolFactorCorrel);
	void relaxToleranceLambda(const double tolFactor);

	const vector<double>& getValuesEIG() const;
	const vector<double>& getVectorsEIG() const;
	int computeSigmaEIG();

	std::string getSummaryString() const;

public:
	enum eig_status {SUCCESS = 0, ERROR = 1};

private:

	CovMatrixUpdater *evoSu;
	vector<MeanUpdater*> evoMu;
	BatchEvoLU *evoLu;
	vector<BatchLightEvoLU*> evoLocalLu;
	DynMultiLvlCC mvcc;

	uint locSdT;
	uint nLocSDDisabled;
	double targAlpha;
	vector<bool> locSDEnabled;
	vector<uint> divergeCnt;
	vector<double> factors;
	vector<double> eigVal, eigVec;

	void initSigma();
	void initLambda(const double myAlpha);

	bool hasUpdateConverged() const;

};

} // namespace ParameterBlock


#endif /* EVOPCABS_H_ */
