//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoTheoBS.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "EvoTheoBS.h"

namespace ParameterBlock {

EvoTheoBS::EvoTheoBS(double aAlpha, const Block *aBlock, const BlockStatCfg::sharedPtr_t aCfg) :
	BlockStats(aBlock, aCfg), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER)
{
	targAlpha = aAlpha;
	locSdT = 0;

	// Init Sigma updater
	evoSu = new BatchEvoSU(aBlock->size(), cfg->SIGMA, cfg->CONV_CHECKER);
	initSigma();

	// Init Mu updaters
	for(uint iM=0; iM<n; ++iM){
		evoMu.push_back(new BatchEvoMU(cfg->MU, cfg->CONV_CHECKER));
	}

	// Init Lu updaters
	evoLu = new BatchEvoLU(aAlpha, aBlock->size(), cfg->LAMBDA, cfg->CONV_CHECKER);
	for(uint iM=0; iM<n; ++iM){
		evoLocalLu.push_back(new BatchLightEvoLU(pow(aAlpha, 1.5/n), aBlock->size(), cfg->LOC_LAMBDA));
	}

	factors.assign(n, 1.0);
	optiLs.assign(n, 0.0);
	cntNotOpti = 0;

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);
	divergeCnt.assign(n, 0);

}

EvoTheoBS::EvoTheoBS(const EvoTheoBS &toCopy) :
	BlockStats(toCopy), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER)
{
	targAlpha = toCopy.targAlpha;
	locSdT = toCopy.locSdT;
	evoSu = new BatchEvoSU(*dynamic_cast<BatchEvoSU*>(toCopy.evoSu));
	for(uint iM=0; iM<toCopy.evoMu.size(); ++iM){
		evoMu.push_back(new BatchEvoMU(*dynamic_cast<BatchEvoMU*>(toCopy.evoMu[iM])));
	}

	evoLu = new BatchEvoLU(*dynamic_cast<BatchEvoLU*>(toCopy.evoLu));
	for(uint iL=0; iL<toCopy.evoLocalLu.size(); ++iL){
		evoLocalLu.push_back(new BatchLightEvoLU(*dynamic_cast<BatchLightEvoLU*>(toCopy.evoLocalLu[iL])));
	}

	factors = toCopy.factors;
	optiLs = toCopy.optiLs;
	cntNotOpti = toCopy.cntNotOpti;

	nLocSDDisabled = toCopy.nLocSDDisabled;
	locSDEnabled = toCopy.locSDEnabled;
	divergeCnt = toCopy.divergeCnt;

	if(n==1) locLambdaConverged = true;
}

EvoTheoBS::~EvoTheoBS() {
	delete evoSu;

	for(uint iM=0; iM<n; ++iM){
		delete evoMu[iM];
	}

	delete evoLu;
	for(uint iM=0; iM<n; ++iM){
		delete evoLocalLu[iM];
	}
}

void EvoTheoBS::initSigma() {

	if(block->getBlockModifier() != 0){
		vector<double> sigma = block->getBlockModifier()->getWindowSize();
		if(sigma.size() == n*n) {
			evoSu->setInitialSigma(sigma);
		} else {
			double sigma = block->getBlockModifier()->getWindowSize()[0];
			evoSu->setInitialSigma(sigma);
		}
	} else if(block->getBlockModifier(0) != 0){
		for(unsigned int i=0; i<n; ++i){
			double sigma = block->getBlockModifier(i)->getWindowSize()[0];
			evoSu->setInitialSigma(i, sigma);
		}
	} else {
		for(unsigned int i=0; i<n; ++i){
			evoSu->setInitialSigma(1.0);
		}
	}

}


void EvoTheoBS::initSample(const Sample &sample){
	vector<double> X;
	getXValues(sample, X);

	for(uint iM=0; iM < n; ++iM){
		evoMu[iM]->setInitialMean(X[iM]);
	}
}

void EvoTheoBS::update(const Sample &sample, const uint nTimes){
	if(sigmaConverged) return;
	if(!sample.hasChanged() && (((evoSu->getT()-1)*cfg->SIGMA->MEAN_WINDOW +
			dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) < cfg->VAR_STEP_BOOST)) {
		return;
	}

	vector<double> X;
	getXValues(sample, X);

	for(uint iT=0; iT<nTimes; ++iT){

		// Update mu
		for(uint iM=0; iM<n; ++iM){
			evoMu[iM]->addVal(X[iM]);
		}
		// Update Sigma
		double oldMeanDiag = evoSu->getMeanDiag();
		evoSu->addVals(X, evoMu);
		double newMeanDiag = evoSu->getMeanDiag();
		evoLu->updateSd(oldMeanDiag/newMeanDiag);

	}

	if(hasUpdateConverged()) {
		sigmaConverged = true;
		uint nP = Parallel::mcmcMgr().getNProposal();
		for(uint iL=0; iL<n; ++iL){
			double var = evoSu->getSigma(iL);
			double tL = getOptiL(nP, n, var)/sqrt(n);
			optiLs[iL] = tL;
		}
	}
}

void EvoTheoBS::updateSD(const double aAlpha){
	if(lambdaConverged) return;
	evoLu->addVal(aAlpha);

	if(evoLu->hasConverged() && sigmaConverged) {
		if((n > 1 && locLambdaConverged) || n == 1) {

			bool optiGood = true;
			for(unsigned int i=0; i<n; ++i){
				if(cntNotOpti < 6*cfg->LAMBDA->MEAN_WINDOW){
					double l = sqrt(evoLu->getSd()*evoSu->getSigma(i)*factors[i]);
					if(fabs(((optiLs[i]-l)/optiLs[i])) > 0.7){
						optiGood = false;
						lambdaConverged = false;
						++cntNotOpti;
						if(cntNotOpti == 3*cfg->LAMBDA->MEAN_WINDOW) {
							if(n!=1) locLambdaConverged = false;
							sigmaConverged = false;
							evoSu->reset(true);
						}
					}
				}
			}
			if(!optiGood) return;

			evoLu->setAverageSd();

			lambdaConverged = true;
			locLambdaConverged = true;
		}
	}
}

void EvoTheoBS::updateLocSD(const vector<double> &aAlphas) {
	if(lambdaConverged || locLambdaConverged) return;

	double sum = 0.0;
	double facSumSq = 0.0;
	for(uint iL=0; iL<n; ++iL){
		if(aAlphas[iL] < 0.){
			evoLocalLu[iL]->addVal(-aAlphas[iL]);
			evoLocalLu[iL]->updateSd(0.5);
		} else {
			evoLocalLu[iL]->addVal(aAlphas[iL]);
		}

		if(locSDEnabled[iL]){
			sum += evoLocalLu[iL]->getSd();
		}
	}

	double tmpN = (n-nLocSDDisabled);
	for(uint iL=0; iL<n; ++iL){
		if(locSDEnabled[iL]){
			factors[iL] = evoLocalLu[iL]->getSd() / sum;
			facSumSq += pow((factors[iL]-(1./tmpN)),2);
		}
	}

	double facVar = 0.;
	if(tmpN > 1){
		facVar = facSumSq/(tmpN-1.);
	}

	if((tmpN*sqrt(facVar) > 2.) && evoLu->getSd() > 800.0) {
		// Find biggest factor
		uint idxBF = 0;
		for(uint iL=1; iL<n; ++iL){
			if(locSDEnabled[iL] && factors[iL] > factors[idxBF]){
				idxBF = iL;
			}
		}

		divergeCnt[idxBF]++;
		if(divergeCnt[idxBF] > 8) {
			// Set idxBF to inactive
			locSDEnabled[idxBF] = false;
			++nLocSDDisabled;

			// Update factors
			double tmpSum = 0.0;
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					tmpSum += factors[iL];
				}
			}
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					factors[iL] /= tmpSum;
				}
			}
			factors[idxBF] = -1;
			evoLocalLu[idxBF]->reset(false);

			// Change Alpha and SD
			evoLu->updateSd(tmpSum);
			//evoLu->setTargetAlpha(0.);

		}

	}

	if(evoLocalLu[0]->getTmpT() == 0) {
		mvcc.addValue(sqrt(facVar));
	}

	// Check for local convergence
	if((n-nLocSDDisabled) == 1 || (mvcc.hasConverged(sqrt(facVar)) && sigmaConverged) || evoLocalLu[0]->getT() >= cfg->LOC_LAMBDA->MAX_T){
		locLambdaConverged = true;
	}

}

bool EvoTheoBS::isReady() {
	bool initC = true;
	bool endC = !(sigmaConverged && lambdaConverged && locLambdaConverged);

	return initC && endC;
}


bool EvoTheoBS::isReadyForLocUpdate(){
	if(lambdaConverged || n == 1) return false;
	bool initOk = ((evoSu->getT()-1)*cfg->LAMBDA->MEAN_WINDOW + dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) > cfg->VAR_STEP_BOOST;
	bool waitSD = evoLu->getT() > (cfg->LAMBDA->NB_BATCH*cfg->LAMBDA->L_BATCH)*0.1;
	bool periodicUpdate = (locSdT % cfg->LOC_LAMBDA_STEP_UPDATE == 0);
	++locSdT;

	return initOk && periodicUpdate && waitSD;
}

double EvoTheoBS::getSd() const {
	return evoLu->getSd();
}
double EvoTheoBS::getLocSd(unsigned int idx) const {
	if(locSDEnabled[idx]){
		return factors[idx];
	} else {
		return evoLocalLu[idx]->getSd()/evoLu->getSd();
	}
}

double EvoTheoBS::getSigmaIdx(unsigned int idx) const {
	return evoSu->getSigma(idx);
}

const double* EvoTheoBS::getSigma() const {
	return evoSu->getSigma();
}

void EvoTheoBS::reduceLocSD(uint idx, const double factor) {
	// Update the local SD
	evoLocalLu[idx]->updateSd(factor);
	if(locSDEnabled[idx]){ // if this loc SD is enabled, updated factors
		double sum = 0.;
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				sum += evoLocalLu[iL]->getSd();
			}
		}
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				factors[iL] = evoLocalLu[iL]->getSd() / sum;
			}
		}
	}
}

bool EvoTheoBS::hasUpdateConverged() const {
	for(uint iM=0; iM<n; ++iM){
		if(!evoMu[iM]->hasConverged()){
			return false;
		}
	}
	return evoSu->hasConverged();
}

void EvoTheoBS::reset(){

	// SU
	evoSu->reset(true);
	BatchEvoSU* BRSUptr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BRSUptr != 0) BRSUptr->reset(true);

	// MU
	for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->reset(true);
		BatchEvoMU* BEMUPtr = dynamic_cast<BatchEvoMU*>(evoMu[iM]);
		if(BEMUPtr != 0) BEMUPtr->reset(true);
	}

	evoLu->reset(true);
	BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) BLELUPtr->reset(true);

	mvcc.reset();
	for(uint iM=0; iM<n; ++iM){
		evoLocalLu[iM]->reset(true);
		BatchLightEvoLU* BLELUPtr = dynamic_cast<BatchLightEvoLU*>(evoLu);
		if(BLELUPtr != 0) BLELUPtr->reset(true);
	}

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);

	sigmaConverged = lambdaConverged = locLambdaConverged = false;
}

void EvoTheoBS::resetMVCC(){

	// SU
	/*BatchEvoSU* BLESUPtr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BLESUPtr != 0) BLESUPtr->resetMVCC();*/

	// MU
	/*for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->resetMVCC();
	}*/

	/*BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) BLELUPtr->resetMVCC();*/

	//mvcc.reset();
	this->relaxToleranceLambda(1.0);
	sigmaConverged = false;
	lambdaConverged = locLambdaConverged = false;
}

bool EvoTheoBS::hasCorrelation() const {
	const double *ptrS = evoSu->getSigma();

	for(uint i=0; i<n; ++i){
		double diagI = 1./sqrt(evoSu->getSigma(i));
		for(uint j=0; j<n; ++j){
			double diagJ = 1./sqrt(evoSu->getSigma(j));
			double corrIJ = diagI*diagJ*fabs(ptrS[i*n+j]);
			if(i!=j && corrIJ > cfg->THRESHOLD_CORRELATION) return true;
		}
	}
	return false;
}

bool EvoTheoBS::hasDisabledFactors() const {
	return nLocSDDisabled > 0;
}

void EvoTheoBS::relaxToleranceSigma(const double tolFactor) {
	double relaxTol = tolFactor;
	for (uint iM=0; iM<evoMu.size(); ++iM){
		evoMu[iM]->relaxTolerance(relaxTol);
	}
	evoSu->relaxTolerance(relaxTol);
}

void EvoTheoBS::relaxToleranceLambda(const double tolFactor) {
	double relaxTol = tolFactor;
	evoLu->relaxTolerance(relaxTol);
	mvcc.relaxTolerance(relaxTol);
}

} // namespace ParameterBlock

