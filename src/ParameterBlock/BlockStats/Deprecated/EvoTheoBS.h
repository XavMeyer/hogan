//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoTheoBS.h
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#ifndef EVOTHEOBS_H_
#define EVOTHEOBS_H_

#include "ParameterBlock/BlockStats/BlockStatsInterface.h"

#include "ConvUtils/Updater/Lambda/BatchEvoLU.h"
#include "ConvUtils/Updater/Lambda/BatchLightEvoLU.h"
#include "ConvUtils/Updater/Mean/BatchEvoMU.h"
#include "ConvUtils/Updater/CovMatrix/BatchEvoSU.h"


namespace ParameterBlock {

class EvoTheoBS: public BlockStats {
public:
	EvoTheoBS(double aAlpha, const Block *aBlock, const BlockStatCfg::sharedPtr_t aCfg);
	EvoTheoBS(const EvoTheoBS &toCopy);
	~EvoTheoBS();

	void initSample(const Sample &sample);
	void update(const Sample &sample, const unsigned int nTimes=1);
	void updateSD(const double aAlpha);
	void updateLocSD(const vector<double> &aAlphas);

	bool isReady();
	bool isReadyForLocUpdate();

	double getSd() const;
	double getLocSd(unsigned int idx) const;
	double getSigmaIdx(unsigned int idx) const;
	const double* getSigma() const;

	void reduceLocSD(uint idx, const double factor);

	void reset();
	void resetMVCC();

	bool hasCorrelation() const;
	bool hasDisabledFactors() const;

	void relaxToleranceSigma(const double tolFactor);
	void relaxToleranceLambda(const double tolFactor);

private:

	CovMatrixUpdater *evoSu;
	vector<MeanUpdater*> evoMu;
	BatchEvoLU *evoLu;
	vector<BatchLightEvoLU*> evoLocalLu;
	DynMultiLvlCC mvcc;

	uint locSdT;
	uint nLocSDDisabled;
	uint cntNotOpti;
	double targAlpha;
	vector<bool> locSDEnabled;
	vector<uint> divergeCnt;
	vector<double> factors, optiLs;

	void initSigma();

	bool hasUpdateConverged() const;

};

} // namespace ParameterBlock

#endif /* EVOTHEOBS_H_ */
