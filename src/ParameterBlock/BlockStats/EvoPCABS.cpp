//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoPCABS.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "EvoPCABS.h"

//#define DEBUG 4
//#define DEBUG_EIG 0

namespace ParameterBlock {

EvoPCABS::EvoPCABS(double aAlpha, const Block::sharedPtr_t aBlock, const BlockStatCfg::sharedPtr_t aCfg) :
	BlockStats(aBlock, aCfg), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER),
	eigVal(n, 0.), eigVec(n*n, 0.)
{
	targAlpha = aAlpha;
	locSdT = 0;

	// Init Sigma updater
	evoSu = new BatchEvoSU(aBlock->size(), cfg->SIGMA, cfg->CONV_CHECKER);
	evoSu->relaxTolerance(1.0, 2.5);
	initSigma();

	// Init Mu updaters
	for(uint iM=0; iM<n; ++iM){
		evoMu.push_back(new BatchEvoMU(cfg->MU, cfg->CONV_CHECKER));
	}

	// Init Lu updaters
	evoLu = new BatchEvoLU(targAlpha, aBlock->size(), cfg->LAMBDA, cfg->CONV_CHECKER);
	initLambda(targAlpha);

	for(uint iM=0; iM<n; ++iM){
		evoLocalLu.push_back(new BatchLightEvoLU(pow(targAlpha, 1.5/n), aBlock->size(), cfg->LOC_LAMBDA));
	}

	factors.assign(n, 1.0/n);

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);
	divergeCnt.assign(n, 0);
}

EvoPCABS::EvoPCABS(const EvoPCABS &toCopy) :
	BlockStats(toCopy), mvcc(cfg->LOC_LAMBDA, cfg->CONV_CHECKER)
{
	targAlpha = toCopy.targAlpha;
	locSdT = toCopy.locSdT;
	evoSu = new BatchEvoSU(*dynamic_cast<BatchEvoSU*>(toCopy.evoSu));
	for(uint iM=0; iM<toCopy.evoMu.size(); ++iM){
		evoMu.push_back(new BatchEvoMU(*dynamic_cast<BatchEvoMU*>(toCopy.evoMu[iM])));
	}

	evoLu = new BatchEvoLU(*dynamic_cast<BatchEvoLU*>(toCopy.evoLu));
	for(uint iL=0; iL<toCopy.evoLocalLu.size(); ++iL){
		evoLocalLu.push_back(new BatchLightEvoLU(*dynamic_cast<BatchLightEvoLU*>(toCopy.evoLocalLu[iL])));
	}

	factors = toCopy.factors;

	nLocSDDisabled = toCopy.nLocSDDisabled;
	locSDEnabled = toCopy.locSDEnabled;

	eigVal = toCopy.eigVal;
	eigVec = toCopy.eigVec;
}

EvoPCABS::~EvoPCABS() {
	delete evoSu;

	for(uint iM=0; iM<n; ++iM){
		delete evoMu[iM];
	}

	delete evoLu;
	for(uint iM=0; iM<n; ++iM){
		delete evoLocalLu[iM];
	}
}

void EvoPCABS::initSigma() {

	if(block->getBlockModifier() != 0){
		vector<double> sigma = block->getBlockModifier()->getWindowSize();
		if(sigma.size() == n*n) {
			evoSu->setInitialSigma(sigma);
		} else {
			double sigma = block->getBlockModifier()->getWindowSize()[0];
			evoSu->setInitialSigma(sigma);
		}
	} else if(block->getBlockModifier(0) != 0){
		for(unsigned int i=0; i<n; ++i){
			double sigma = block->getBlockModifier(i)->getWindowSize()[0];
			evoSu->setInitialSigma(i, sigma);
		}
	} else {
		for(unsigned int i=0; i<n; ++i){
			evoSu->setInitialSigma(1.0);
		}
	}

}

void EvoPCABS::initLambda(const double myAlpha) {
	uint nP = Parallel::mcmcMgr().getNProposal();
	double var = evoSu->getSigma(0) > 0 ? evoSu->getSigma(0) : 1.e-3;
	double lOp = getOptiL(nP, n, var);
	double newLambda = lOp*lOp/(var);
	newLambda /= evoLu->getSd();
	evoLu->updateSd(newLambda);
}


void EvoPCABS::initSample(const Sample &sample){
	vector<double> X;
	getXValues(sample, X);

	for(uint iM=0; iM < n; ++iM){
		evoMu[iM]->setInitialMean(X[iM]);
	}
}

void EvoPCABS::update(const Sample &sample, const uint nTimes){
	if(sigmaConverged) return;
	if(!sample.hasChanged() && (((evoSu->getT()-1)*cfg->SIGMA->MEAN_WINDOW + dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) < cfg->VAR_STEP_BOOST)) return;

	vector<double> X;
	getXValues(sample, X);

	for(uint iT=0; iT<nTimes; ++iT){

		// Update mu
		for(uint iM=0; iM<n; ++iM){
			evoMu[iM]->addVal(X[iM]);
		}
		// Update Sigma
		evoSu->addVals(X, evoMu);
	}

	if(hasUpdateConverged()) {
		sigmaConverged = true;
	}
}

void EvoPCABS::updateSD(const double aAlpha){
	if(lambdaConverged) return;

	if(aAlpha >= 0.){
		evoLu->addVal(aAlpha);
	} else {
		// If does not diverge too much, just scale lambda
		uint nP = Parallel::mcmcMgr().getNProposal();
		if(divergeCnt[0] == 0) {
			double sigma = evoSu->getSigma(0);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = lOp*lOp/(evoSu->getSigma(0));

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
		} else if(divergeCnt[0] == 1){ // Else reduce sigma and lambda and hard reset
			double fac = 1.;
			double sigma = evoSu->getSigma(0)/(fac*10.);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = (lOp*lOp/(evoSu->getSigma(0)))/(fac*10.);

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
			evoLu->updateSd(newLambda);
			evoLu->setTargetAlpha(evoLu->getTraceAlpha()+0.03);

		} else {
			double sigma = evoSu->getSigma(0);
			double lOp = getOptiL(nP, 1, sigma);
			double newLambda = (lOp*lOp/(evoSu->getSigma(0)))/10.;

			evoLu->addVal(-aAlpha);
			newLambda /= evoLu->getSd();
			evoLu->updateSd(newLambda);

			lambdaConverged = true;
			locLambdaConverged = true;
		}
		divergeCnt[0]++;
	}


#ifdef DEBUG
	const uint L_SD_NPROP = cfg->LAMBDA->MEAN_WINDOW;
	if((block->getId() == DEBUG) && evoLu->getT() % 5 == 0 && evoLu->getTmpT() == (L_SD_NPROP-1)) { // (Parallel::mpiMgr().getRank() == 0) &&
		stringstream ss;
		ss << "Block " << block->getName() << endl;
		ss << "Block " << block->toString() << endl;
		/*cout.precision(4);
		for(uint i=0; i<block->size(); ++i){
			for(uint j=0; j<block->size(); ++j){
				ss << scientific << evoSu->getSigma()[i+j*block->size()] << "\t";
			}
			ss << endl;
		}*/
		ss << dynamic_cast<BatchEvoSU*>(evoSu)->toString() << endl;
		ss << dynamic_cast<BatchEvoMU*>(evoMu.front())->toString() << endl;
		ss << dynamic_cast<BatchEvoLU*>(evoLu)->toString() << endl;
		ss << "F>>\t";
		for(uint iL=0; iL<n; ++iL){
			ss << factors[iL]  << "\t";
		}
		ss << endl;
		ss << "locSD>>\t";
		for(uint iL=0; iL<n; ++iL){
			ss << evoLocalLu[iL]->toString()  << endl;
		}
		ss << endl;
		ss << mvcc.toString();
		ss << endl << "**********************************************";
		Parallel::mpiMgr().print(ss.str());
	}
#endif

	if(evoLu->hasConverged() && sigmaConverged) {
		if((n > 1 && locLambdaConverged) || n == 1) {

			evoLu->setAverageSd();

			lambdaConverged = true;
			locLambdaConverged = true;

#ifdef DEBUG
			if((block->getId() == DEBUG)) {//&& (Parallel::mpiMgr().getRank() == 0)) {
				stringstream ss;
				//sleep(pMCMC::mpiMgr().getRank()*2.);
				ss << "CONV : Block " << block->getName() << endl;
				ss << block->toString() << endl;
				//cout << dynamic_cast<BatchEvoSU*>(evoSu)->toString() << endl;
				ss << dynamic_cast<BatchEvoLU*>(evoLu)->toString() << fixed << endl;
				ss.precision(4);
				uint myN = block->size();
				ss << "[";
				for(unsigned int i=0; i<myN; ++i){
					for(unsigned int j=0; j<myN; ++j){
						ss << scientific <<  evoSu->getSigma()[i*myN+j] << "\t";
					}
					ss << ";" << endl;
				}

				ss << "F>>\t";
				for(uint iL=0; iL<n; ++iL){
					ss << factors[iL]  << "\t";
				}
				ss << endl;
				ss << "locSD>>\t";
				for(uint iL=0; iL<n; ++iL){
					ss << evoLocalLu[iL]->toString()  << endl;
				}
				ss << endl;
				ss << mvcc.toString() << fixed << endl;
				ss << "**********************************************" << endl;
				Parallel::mpiMgr().print(ss.str());
			}
#endif
		}
	}

}

void EvoPCABS::updateLocSD(const vector<double> &aAlphas) {
	if(lambdaConverged || locLambdaConverged) return;

	double sum = 0.0;
	double facSumSq = 0.0;
	double alphaMean = 0.0;
	for(uint iL=0; iL<n; ++iL){
		if(aAlphas[iL] < 0.){
			evoLocalLu[iL]->addVal(-aAlphas[iL]);
			evoLocalLu[iL]->updateSd(0.5);
		} else {
			evoLocalLu[iL]->addVal(aAlphas[iL]);
		}

		if(locSDEnabled[iL]){
			sum += evoLocalLu[iL]->getSd();
		}
	}

	double tmpN = (n-nLocSDDisabled);
	for(uint iL=0; iL<n; ++iL){
		if(locSDEnabled[iL]){
			factors[iL] = evoLocalLu[iL]->getSd() / sum;
			facSumSq += pow((factors[iL]-(1./tmpN)),2);
			alphaMean += evoLocalLu[iL]->getTraceAlpha()/tmpN;
		}
	}

	double facVar = 0.;
	if(tmpN > 1){
		facVar = facSumSq/(tmpN-1.);
	}

	//cout << "Check for divergence : " << tmpN*sqrt(facVar)  << endl;
	if((tmpN*sqrt(facVar) > 1.5) && evoLu->getSd() > 300.0) {
		// Find biggest factor
		uint idxBF = 0;
		for(uint iL=1; iL<n; ++iL){
			if(locSDEnabled[iL] && factors[iL] > factors[idxBF]){
				idxBF = iL;
			}
		}

		divergeCnt[idxBF]++;
		if(divergeCnt[idxBF] > 8) {
			// Set idxBF to inactive
			locSDEnabled[idxBF] = false;
			++nLocSDDisabled;

			// Update factors
			double tmpSum = 0.0;
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					tmpSum += factors[iL];
				}
			}
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					factors[iL] /= tmpSum;
				}
			}
			factors[idxBF] = -1;
			evoLocalLu[idxBF]->setTargetAlpha(evoLocalLu[idxBF]->getTraceAlpha()-0.05);
			evoLocalLu[idxBF]->reset(false);
			// Change Alpha and SD
			evoLu->updateSd(tmpSum);
		}
	}

	if(evoLocalLu[0]->getTmpT() == 0) {
		mvcc.addValue(sqrt(facVar));
		if(evoLocalLu[0]->getT() > 40){
			for(uint iL=0; iL<n; ++iL){
				if(locSDEnabled[iL]){
					evoLocalLu[iL]->setTargetAlpha(alphaMean);
				}
			}
		}
	}

	// Check for local convergence
	if((n-nLocSDDisabled) == 1 || (mvcc.hasConverged(sqrt(facVar)) && sigmaConverged) || evoLocalLu[0]->getT() >= cfg->LOC_LAMBDA->MAX_T){
		//std::cout << block->getId() << (sigmaConverged ? " :: t" : " :: f") << std::endl;
		locLambdaConverged = true;
	}
}

bool EvoPCABS::isReady() {
	bool initC = true;
	bool endC = !(sigmaConverged && lambdaConverged && locLambdaConverged);

	return initC && endC;
}


bool EvoPCABS::isReadyForLocUpdate(){
	if(lambdaConverged || n == 1) return false;
	bool initOk = ((evoSu->getT()-1)*cfg->LAMBDA->MEAN_WINDOW + dynamic_cast<BatchEvoSU*>(evoSu)->getTmpT()) > cfg->VAR_STEP_BOOST;
	bool waitSD = evoLu->getT() > (cfg->LAMBDA->NB_BATCH*cfg->LAMBDA->L_BATCH)*0.1;
	bool periodicUpdate = (locSdT % cfg->LOC_LAMBDA_STEP_UPDATE == 0);
	++locSdT;

	return initOk && periodicUpdate && waitSD;
}

bool EvoPCABS::isDisabled(const unsigned int idx) const {
	return !locSDEnabled[idx];
}

double EvoPCABS::getMu(size_t idx) const {
	return evoMu[idx]->getMean();
}

double EvoPCABS::getSd() const {
	return evoLu->getSd();
}
double EvoPCABS::getLocSd(unsigned int idx) const {
	if(locSDEnabled[idx]){
		return factors[idx];
	} else {
		return evoLocalLu[idx]->getSd()/evoLu->getSd();
	}
}

double EvoPCABS::getSigmaIdx(unsigned int idx) const {
	return evoSu->getSigma(idx);
}

const double* EvoPCABS::getSigma() const {
	return evoSu->getSigma();
}

void EvoPCABS::reduceLocSD(uint idx, const double factor) {
	// Update the local SD
	evoLocalLu[idx]->updateSd(factor);
	if(locSDEnabled[idx]){ // if this loc SD is enabled, updated factors
		double sum = 0.;
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				sum += evoLocalLu[iL]->getSd();
			}
		}
		for(uint iL=0; iL<n; ++iL){
			if(locSDEnabled[iL]){
				factors[iL] = evoLocalLu[iL]->getSd() / sum;
			}
		}
	}
}

bool EvoPCABS::hasUpdateConverged() const {
	for(uint iM=0; iM<n; ++iM){
		if(!evoMu[iM]->hasConverged()){
			return false;
		}
	}
	return evoSu->hasConverged();
}

void EvoPCABS::reset(){

	// SU
	evoSu->reset(true);
	BatchEvoSU* BRSUptr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BRSUptr != 0) BRSUptr->reset(true);

	// MU
	for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->reset(true);
		BatchEvoMU* BEMUPtr = dynamic_cast<BatchEvoMU*>(evoMu[iM]);
		if(BEMUPtr != 0) BEMUPtr->reset(true);
	}

	evoLu->reset(true);
	BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) BLELUPtr->reset(true);

	mvcc.reset();
	for(uint iM=0; iM<n; ++iM){
		evoLocalLu[iM]->reset(true);
		BatchLightEvoLU* BLELUPtr = dynamic_cast<BatchLightEvoLU*>(evoLu);
		if(BLELUPtr != 0) BLELUPtr->reset(true);
	}

	nLocSDDisabled = 0;
	locSDEnabled.assign(n, true);

	sigmaConverged = lambdaConverged = locLambdaConverged = false;
}

void EvoPCABS::resetMVCC(){

	// SU
	/*BatchEvoSU* BLESUPtr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BLESUPtr != 0) BLESUPtr->resetMVCC();*/

	// MU
	/*for(uint iM=0; iM<n; ++iM){
		evoMu[iM]->resetMVCC();
	}*/

	//BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	//if(BLELUPtr != 0) {
		//BLELUPtr->reset(true);
		//BLELUPtr->resetMVCC();
	//}

	//evoLu->resetAccumulator();

	//mvcc.reset();
	this->relaxToleranceLambda(1.0);
	sigmaConverged = false;
	lambdaConverged = locLambdaConverged = false;
}

void EvoPCABS::resetForCorrel(){

	// Reset tolerance
	this->relaxToleranceSigma(1.0, 1.0);
	this->relaxToleranceLambda(1.0);

	// Reset MVCC and localLu
	mvcc.reset();
	factors.assign(n, 1.0/n);
	for(uint iM=0; iM<n; ++iM){
		evoLocalLu[iM]->reset(false);
	}

	BatchEvoSU* BLESUPtr = dynamic_cast<BatchEvoSU*>(evoSu);
	if(BLESUPtr != 0) BLESUPtr->reset(true);

	BatchEvoLU* BLELUPtr = dynamic_cast<BatchEvoLU*>(evoLu);
	if(BLELUPtr != 0) BLELUPtr->reset(true);

	// Reset converged
	sigmaConverged = false;
	lambdaConverged = locLambdaConverged = false;
}

void EvoPCABS::resetForIndep(){

	// Reset tolerance
	this->relaxToleranceSigma(1.0, 2.5);
	this->relaxToleranceLambda(1.0);

	// Reset MVCC and localLu
	sigmaConverged = lambdaConverged = locLambdaConverged = false;
}


bool EvoPCABS::hasCorrelation() const {
	const double *ptrS = evoSu->getSigma();

	for(uint i=0; i<n; ++i){
		double diagI = 1./sqrt(evoSu->getSigma(i));
		for(uint j=0; j<n; ++j){
			double diagJ = 1./sqrt(evoSu->getSigma(j));
			double corrIJ = diagI*diagJ*fabs(ptrS[i*n+j]);
			if(i!=j && corrIJ > cfg->THRESHOLD_CORRELATION) {
				return true;
			}
		}
	}
	return false;
}

bool EvoPCABS::hasDisabledFactors() const {
	return nLocSDDisabled > 0;
}

void EvoPCABS::relaxToleranceSigma(const double tolFactor, const double tolFactorCorrel) {
	double relaxTol = tolFactor;
	for (uint iM=0; iM<evoMu.size(); ++iM){
		evoMu[iM]->relaxTolerance(relaxTol);
	}
	evoSu->relaxTolerance(relaxTol, tolFactorCorrel);
}

void EvoPCABS::relaxToleranceLambda(const double tolFactor) {
	double relaxTol = tolFactor;
	evoLu->relaxTolerance(relaxTol);
	mvcc.relaxTolerance(relaxTol);
}

const vector<double>& EvoPCABS::getValuesEIG() const {
	return eigVal;
}

const vector<double>& EvoPCABS::getVectorsEIG() const {
	return eigVec;
}

int EvoPCABS::computeSigmaEIG() {
	if(!evoSu->updated()) return SUCCESS;

	//evoSu->cleanCorrelation(0.20);

	Eigen::Map<const Eigen::MatrixXd> mappedSigma(evoSu->getSigma(), n, n);
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es(mappedSigma);

	if(es.info() == Eigen::Success) {
		evoSu->resetUpdated();

		Eigen::Map<Eigen::VectorXd> mappedVal(eigVal.data(), n);
		Eigen::Map<Eigen::MatrixXd> mappedVec(eigVec.data(), n, n);

		mappedVal = es.eigenvalues().real();
		mappedVec = es.eigenvectors().real();
		//eigVal = tmpEval;
		//eigVec = tmpEVec;
		return SUCCESS;
	}  else {
		return ERROR;
	}
}

std::string EvoPCABS::getSummaryString() const {
	std::stringstream ss;
	ss << "Acceptance rate :\t" << evoLu->getTraceAlpha() << std::endl;
	ss << "Lambda (val, t, rmc t) :\t" << evoLu->getSd() << "\t" << evoLu->getT() << "\t" << evoLu->getRMC()->getT() << std::endl;
	ss << "Sigma (t, rmc t)       :\t" << evoSu->getT() << "\t" << evoSu->getRMC()->getT() << std::endl;

	/*for(size_t i=0; i<block->getPIndices().size(); ++i) {
		for(size_t j=0; j<block->getPIndices().size(); ++j) {
			ss << std::scientific << std::setprecision(2) << std::setw(2) << evoSu->getSigma()[i*block->size()+j] << " ";
		}
		ss << std::endl;
	}*/


	ss << setw(10) << "pid\t";
	for(size_t iP=0; iP<block->size(); ++iP) {
		ss << std::setw(8) << block->getPIndice(iP) << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "mu \t";
	for(size_t iP=0; iP<evoMu.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoMu[iP]->getMean() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "t(mu)\t";
	for(size_t iP=0; iP<evoMu.size(); ++iP) {
		ss << std::setw(8) << evoMu[iP]->getRMC()->getT() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "Sigma\t";
	for(size_t iP=0; iP<block->size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoSu->getSigma(iP) << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "Factors\t";
	for(size_t iP=0; iP<factors.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << factors[iP] << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "LocSd\t";
	for(size_t iP=0; iP<evoLocalLu.size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoLocalLu[iP]->getSd() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "LocSdT\t";
	for(size_t iP=0; iP<evoLocalLu.size(); ++iP) {
		ss << std::setw(8) << evoLocalLu[iP]->getRMC()->getT() << "\t";
	}
	ss << std::endl;

	ss << setw(10) << "ScaledSD\t";
	for(size_t iP=0; iP<block->size(); ++iP) {
		ss << std::scientific << std::setprecision(2) << std::setw(2) << evoSu->getSigma(iP)* factors[iP] * evoLu->getSd() << "\t";
	}
	ss << std::endl;

	return ss.str();
}


} // namespace ParameterBlock

