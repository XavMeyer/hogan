//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockStatsInterface.h
 *
 * @date mai 7, 2014
 * @author meyerx
 * @brief
 */
#ifndef BLOCKSTATS_H_
#define BLOCKSTATS_H_

#include <cmath>

#include "../Block.h"
#include "Sampler/Samples/Sample.h"

#include <iomanip>
#include <vector>
#include <cmath>
#include <boost/math/distributions.hpp>
#include <boost/shared_ptr.hpp>

#include "Config/Container/BlockStatCfg.h"

namespace ParameterBlock {

using namespace std;
using Sampler::Sample;
using Config::BlockStatCfg;

class BlockStats {
public:
typedef boost::shared_ptr<BlockStats> blockStatPtr_t;

public:
	BlockStats(const Block::sharedPtr_t aBlock, const BlockStatCfg::sharedPtr_t aCfg);
	BlockStats(const BlockStats &toCopy);
	virtual ~BlockStats();

	virtual void initSample(const Sample &sample) = 0;
	virtual void update(const Sample &sample, const unsigned int nTimes=1) = 0;
	virtual void updateSD(const double aAlpha) = 0;
	virtual void updateLocSD(const vector<double> &aAlphas) = 0;

	virtual bool isReady() = 0;
	virtual bool isReadyForLocUpdate() = 0;

	virtual double getMu(size_t idx) const = 0;
	virtual double getSd() const = 0;
	virtual double getLocSd(unsigned int idx) const = 0;
	virtual double getSigmaIdx(unsigned int idx) const = 0;
	virtual const double* getSigma() const = 0;

	virtual bool hasSigmaConverged() const;
	virtual bool hasLambdaConverged() const;
	virtual bool hasLocLambdaConverged() const;

	virtual std::string getSummaryString() const = 0;

protected:
	bool sigmaConverged, lambdaConverged, locLambdaConverged;

	double getAlphaFromL(const double l, const double var) const;
	double getOptiL(const unsigned int nP, const unsigned int nPPB, const double var) const;

	const Block::sharedPtr_t block;
	const BlockStatCfg::sharedPtr_t cfg;
	unsigned int n;

	void getXValues(const Sample &sample, vector<double> &X);

};

} // namespace ParameterBlock

#endif /* BLOCKSTATS_H_ */
