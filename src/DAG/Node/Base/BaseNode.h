//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseNode.h
 *
 * @date Nov 24, 2014
 * @author meyerx
 * @brief
 *
 * The life of a Node during the DAG traversal is the following :
 * PENDING when waiting on children nodes to be READY
 * READY when all children node are READY or if leaf
 * SCHEDULED when DAG scheduler noticed its READYness (important in shared memory)
 * PROCESSING when currently processed
 * DONE when the processing is done
 *
 * UPDATE happen if an internal node is changed, it is used to partially reset the DAG.
 *
 */
#ifndef BASENODE_H_
#define BASENODE_H_

#include <set>
#include <map>
#include <vector>
#include <iostream>
#include <sstream>
#include <assert.h>
#include <stdlib.h>
#include <typeinfo>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "mpi.h"
#include "Eigen/Core"

#include "Utils/Code/CustomProfiling.h"

namespace DAG {

class BaseNode {

public: // CONST
	enum state_t {PENDING, READY, SCHEDULED, PROCESSING, DONE, UPDATED};
	enum {CHILDREN_SIZE_THRESHOLD=500};

public: // Functions

	// Ctor Dtor
	BaseNode();
	virtual ~BaseNode();

	// DAG init
	void addChild(BaseNode* aChild);
	bool removeChild(BaseNode* aChild);

	// DAG traversal
	void process(bool doFakeComputation=false);
	void signal(BaseNode* aChild);
	void signalParents();

	void resetState();
	void childUpdated(BaseNode *aChild);
	void childUpdated(const size_t childIdx);

	void hasBeenSerialized();

	virtual std::string toString() const;

	// Utils functions
	size_t getId() const {
		return id;
	}

	bool checkState(state_t aState) const {
		return state == aState;
	}

	state_t getState() const {
		return state;
	}

	size_t getNChildren() const {
		return children.size();
	}

	size_t getNParent() const {
		return parents.size();
	}

	BaseNode* getChild(const size_t idx) const {
		return children[idx];
	}

	BaseNode* getParent(const size_t idx) const {
		return parents[idx];
	}

	double getAverageTime() const {
		return boost::accumulators::mean(accTime);
	}

	size_t getNEval() const {
		return boost::accumulators::count(accTime);
	}


	bool isReady() const {
		return state == READY;
	}

	bool isLeaf() const {
		return children.empty();
	}

	void updated() {
		state = UPDATED;
	}

	bool isUpdated() const {
		return state == UPDATED;
	}

	void ready() {
		state = READY;
	}

	bool isScheduled() {
		return state == SCHEDULED;
	}

	void scheduled() {
		state = SCHEDULED;
	}

	size_t getLevel() const {
		return level;
	}

	void setLevel(size_t aLevel) {
		level = aLevel;
	}

	bool isDone() const {
		return state == DONE;
	}

	void setDone() {
		state = DONE;
	}

	bool isPending() const {
		return state == PENDING;
	}

	void deleteChildren();

	std::string subtreeToString() const;
	std::string subtreeToString(std::set<size_t> &visited) const;

protected:

	state_t state;

	// Pure virtual processing of the node and signaling from children
	virtual void doProcessing() = 0;
	virtual bool processSignal(BaseNode* aChild) = 0;

	// Utils get the children ID
	const size_t getChildrenRow(BaseNode* aChild) const;

	Eigen::IOFormat eigenFmt;

private:

	typedef std::vector<bool> vecBool_t;
	typedef std::vector<BaseNode*> vecBNode_t;
	typedef std::map<size_t, size_t> mapCR_t;

	static size_t idSeq;
	size_t id, nSignal, level;
	boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accTime;

	vecBNode_t parents;
	vecBNode_t children;
	vecBool_t childrenSignaled;
	mapCR_t childrenRow;

	void addParent(BaseNode* aParent);

	// Overloadable functions for parent + childen addition
	virtual void doAddParent(BaseNode* aParent) {}
	virtual void doAddChild(const size_t rowId, BaseNode* aChild) {}

	virtual void doRemoveParent(BaseNode* aParent) {}
	virtual void doRemoveChild(const size_t rowId, BaseNode* aChild) {}


	bool unregisterChild(BaseNode *childrenPtr);
	bool unregisterParent(BaseNode *childrenPtr);

};


struct CompareBaseNodePtrByID {
	const size_t id;
	CompareBaseNodePtrByID(const BaseNode* bnPtr) : id(bnPtr->getId()) {}
    bool operator () (const BaseNode* bnPtr) const
    {
        return bnPtr->getId() == id;
    }
};

} /* namespace DAG */

#endif /* BASENODE_H_ */
