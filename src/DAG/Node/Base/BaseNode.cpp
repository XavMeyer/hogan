//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Node.cpp
 *
 * @date Nov 24, 2014
 * @author meyerx
 * @brief
 */
#include "BaseNode.h"

namespace DAG {

size_t BaseNode::idSeq = 0;

BaseNode::BaseNode() : eigenFmt(4, 0, ", ", ", ", "", "") {
	id = idSeq++;
	nSignal = 0;
	level = 1;
	state = READY;
}

BaseNode::~BaseNode() {
	for(size_t i=0; i<children.size(); ++i){
		children[i]->unregisterParent(this);
	}

	for(size_t i=0; i<parents.size(); ++i){
		parents[i]->unregisterChild(this);
	}
}


void BaseNode::addParent(BaseNode* aParent) {
	parents.push_back(aParent);
	doAddParent(aParent);

}

void BaseNode::addChild(BaseNode* aChild) {
	// Add children and init its signal
	children.push_back(aChild);
	if(aChild->isDone()) {
		childrenSignaled.push_back(true);
		nSignal += 1;
	} else {
		childrenSignaled.push_back(false);
	}
	state = PENDING;

	// Call overloadable function
	doAddChild(children.size()-1, aChild);

	// Init the map for children rows if nb of children is greater than CHILDREN_SIZE_THRESHOLD
	if(children.size() == CHILDREN_SIZE_THRESHOLD || childrenRow.size() > 0){
		for(uint i=0; i<children.size(); ++i){
			const size_t idx = i;
			childrenRow.insert(std::pair<size_t, const size_t>(children[i]->getId(), idx));
		}
	} else if (children.size() > CHILDREN_SIZE_THRESHOLD) {
		childrenRow.insert(std::pair<size_t, const size_t>(aChild->getId(), children.size()-1));
	}

	aChild->addParent(this);

}

bool BaseNode::removeChild(BaseNode* aChild) {
	// Try to unregister a child
	bool found = unregisterChild(aChild);
	if(found) { // If it exist
		state = PENDING;
		aChild->unregisterParent(this); // Cleanup trace in child
	}

	return found;
}



void BaseNode::process(bool doFakeComputation) {

	// Check that it can be processed
	if(state != SCHEDULED) {
		std::cerr << "Error : void BaseNode::process();" << std::endl;
		std::cerr << "Trying to process unscheduled node [" << this->getId() << "]." << std::endl;
		abort();
	}
	state = PROCESSING;

	//CustomProfiling cp;
	//cp.startTime();

	if(!doFakeComputation) {
		volatile double sTime = MPI_Wtime();
		doProcessing();
		volatile double eTime = MPI_Wtime();
		//cp.endTime();
		//accTime(cp.duration());
		accTime(eTime-sTime);
	}

	// Processing is done, go to done state
	state = DONE;
}

void BaseNode::signalParents() {
	for(size_t i=0; i<parents.size(); ++i){
		parents[i]->signal(this);
	}
}

void BaseNode::signal(BaseNode* aChild) {

	if(state != PENDING) return;

	if(!processSignal(aChild)) {
		std::cerr << "Error : void BaseNode::signal(const BaseNode& aChild);" << std::endl;
		std::cerr << "Parent node [" << this->getId() << "] can't manage signal from child node [" << aChild->getId() << "]." << std::endl;
		abort();
	}

	// search in function of size
	const size_t cRow = getChildrenRow(aChild);
	if(!childrenSignaled[cRow]){
		childrenSignaled[cRow] = true;
		++nSignal;

		// All children signaled, we are ready
		if(nSignal == childrenSignaled.size()){
			//cout << "Node [" << getId() << "] is ready." << endl;
			state = READY;
		}
	}
}

void BaseNode::resetState() {
	if(isLeaf()) {
		state = READY;
	} else {
		state = PENDING;

		nSignal = 0;
		for(size_t i=0; i<childrenSignaled.size(); ++i){
			childrenSignaled[i] = false;
		}
	}
}

void BaseNode::childUpdated(BaseNode *aChild) {
	childUpdated(getChildrenRow(aChild));
}

void BaseNode::childUpdated(const size_t childIdx) {
	state = PENDING;

	if(childrenSignaled[childIdx]){
		nSignal -= 1;
		childrenSignaled[childIdx] = false;
	}
}

void BaseNode::hasBeenSerialized() {
	for(size_t i=0; i<parents.size(); ++i){
		parents[i]->updated();
	}
}


const size_t BaseNode::getChildrenRow(BaseNode* aChild) const {
	// If map is avaiable use it
	if(childrenRow.size() > 0) {
		// Search into map
		mapCR_t::const_iterator row = childrenRow.find(aChild->getId());
		assert(row != childrenRow.end());
		return row->second;
	} else { // otherwise use vector
		for(size_t i=0; i<children.size(); ++i) {
			if(children[i]->getId() == aChild->getId()){
				return i;
			}
		}
	}

	std::cerr << "Children not found in DAG::Node::getChildrenRow" << std::endl;
	abort();

	return 0;
}

std::string BaseNode::toString() const {
	std::stringstream ss;
	ss << "BaseNode id=" << id << " with children = ( ";
	for(size_t i=0; i<children.size(); ++i) {
		ss << children[i]->getId() << "[";
		if(childrenSignaled[i]) {
			ss << "S";
		} else {
			ss << "!S";
		}
		ss << "] ,";
	}
	ss << " ) and parents = ( ";

	for(size_t i=0; i<parents.size(); ++i) {
		ss << parents[i]->getId() << " ,";
	}
	ss << " ) in state : ";

	if(state == PENDING) {
		ss << "PENDING";
	} else if(state == READY) {
		ss << "READY";
	} else if(state == SCHEDULED) {
		ss << "SCHEDULED";
	} else if(state == PROCESSING) {
		ss << "PROCESSING";
	} else if(state == DONE) {
		ss << "DONE";
	} else if(state == UPDATED) {
		ss << "UPDATED";
	}

	//ss << " || nEval = " << getNEval();
	//ss << " || meanTime = " << getAverageTime();


	return ss.str();
}

bool BaseNode::unregisterChild(BaseNode *childrenPtr) {
	for(size_t i=0; i<children.size(); ++i) {
		if(children[i] == childrenPtr){ // If children is found
			if(childrenRow.size() > 0) { // If we use childrenRow
				// We have to remove the current children
				childrenRow.erase(childrenPtr->getId());

				// and update the swapped one
				childrenRow[children.back()->getId()] = i;
			}
			std::swap(children[i], children.back());
			children.pop_back();

			if(childrenSignaled[i]) nSignal -= 1;
			childrenSignaled[i] = childrenSignaled.back();
			childrenSignaled.pop_back();

			doRemoveChild(i, childrenPtr);
			return true;
		}
	}

	return false;
}

bool BaseNode::unregisterParent(BaseNode *parentPtr) {
	for(size_t i=0; i<parents.size(); ++i) {
		if(parents[i] == parentPtr){
			std::swap(parents[i], parents.back());
			parents.pop_back();
			doRemoveParent(parentPtr);
			return true;
		}
	}
	return false;
}

void BaseNode::deleteChildren() {
	while(!children.empty()) {
		BaseNode *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string BaseNode::subtreeToString() const {
	std::set<size_t> visited;
	return subtreeToString(visited);
}

std::string BaseNode::subtreeToString(std::set<size_t> &visited) const {
	stringstream ss;
	ss << toString() << endl;
	visited.insert(this->getId());
	for(size_t i=0; i<children.size(); ++i) {
		if(visited.count(children[i]->getId()) == 0) {
			ss << children[i]->subtreeToString(visited);
		}
	}
	return ss.str();
}

} /* namespace DAG */
