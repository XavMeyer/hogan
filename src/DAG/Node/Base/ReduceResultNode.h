//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReduceResultNode.h
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef REDUCERESULTNODE_H_
#define REDUCERESULTNODE_H_

#include <DAG/Node/Base/BaseNode.h>
#include <DAG/Node/Base/Operator/Base.h>
#include <DAG/Node/Base/ResultNode.h>

#include <vector>

namespace DAG {

template <typename T, typename OpType >
class ReduceResultNode: public ResultNode<T> {
public:
	ReduceResultNode();
	virtual ~ReduceResultNode();

protected:

	std::vector<ResultNode<T>*> vecResAttribut;
	Operator::Base<T> *op;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, BaseNode* aChild);

};

} /* namespace DAG */

#endif /* REDUCERESULTNODE_H_ */
