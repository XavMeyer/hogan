//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReduceResultNode.cpp
 *
 * @date Mar 14, 2015
 * @author meyerx
 * @brief
 */
#include <DAG/Node/Base/ReduceResultNode.h>

namespace DAG {

template <typename T, typename OpType>
ReduceResultNode<T, OpType>::ReduceResultNode() : ResultNode<T>() {
	// Create the operator
	op = new OpType();
}

template <typename T, typename OpType>
ReduceResultNode<T, OpType>::~ReduceResultNode() {
}

template <typename T, typename OpType>
void ReduceResultNode<T, OpType>::doProcessing() {

	if(vecResAttribut.size() < 2) {
		std::cerr << "void ReduceResultNode<T, OpType>::doProcessing();" << std::endl;
		std::cerr << "ERROR : Can't reduce less than 2 elements ( Node id=" << this->getId() << ").";
		std::cerr << std::endl;
		abort();
	}

	this->result = op->reduce(vecResAttribut[0]->getResult(), vecResAttribut[1]->getResult());
	for(size_t i=2; i<vecResAttribut.size(); ++i) {
		this->result = op->reduce(this->result, vecResAttribut[i]->getResult());
	}
}

template <typename T, typename OpType>
bool ReduceResultNode<T, OpType>::processSignal(BaseNode* aChild){
	return true;
}

template <typename T, typename OpType>
void ReduceResultNode<T, OpType>::doAddChild(const size_t rowId, BaseNode* aChild){
	ResultNode<T>* ptrAR = dynamic_cast<ResultNode<T>*>(aChild);
	if(ptrAR != NULL){
		vecResAttribut.push_back(ptrAR);
		//std::cout << "[" << this->getId() << "] Add node : " << aChild->getId() << std::endl;
	}
}

} /* namespace DAG */
