//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseAlgorithm.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef BASEALGORITHM_H_
#define BASEALGORITHM_H_

#include "PriorityNode.h"
#include "NodeAssignment.h"

#include <boost/shared_ptr.hpp>

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

typedef std::vector< std::vector< BaseNode*> > vecBaseNode2D_t;

class BaseAlgorithm {
public:
	typedef boost::shared_ptr< BaseAlgorithm > sharedPtr_t;
public:
	BaseAlgorithm(const size_t aNThread, const std::list<BaseNode*> &aTopoList);
	virtual ~BaseAlgorithm();

	prioMap_t &getPriorities();
	double getSchedulingLength(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);

	virtual void init(bool isFullReinit) = 0;
	virtual void assignTaskToProc(vecBaseNode2D_t &assignVec,
			  	  	  	  	   	  assignmentMap_t &assignMap) = 0;

protected:

	static const double EPSILON_MEMORY_BARRIER, DEFAULT_NODE_TIME;

	const size_t nThread;
	const BaseNode* root;
	const std::list<BaseNode*> &topoList;

	prioMap_t priorities;

	void initPriorties();
	void initBLevel();
	void initTLevel();
	void computePriorityCP_MISF();

	void assignByPriority(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);

	std::vector<size_t> defineParentsCountPerProc(const size_t nProc, BaseNode *node, assignmentMap_t &assignMap);
	size_t findNextProc(double earlierStartTime, BaseNode *node, std::vector<double> procTime, std::vector<size_t> &nPPP);
	double findEarlierNodeStartTime(BaseNode *node, assignmentMap_t &assignMap);
	void swapRootNodeToManager(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);

};

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* BASEALGORITHM_H_ */
