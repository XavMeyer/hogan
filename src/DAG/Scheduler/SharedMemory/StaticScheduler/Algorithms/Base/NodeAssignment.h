//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NodeAssignment.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef NODEASSIGNMENT_H_
#define NODEASSIGNMENT_H_

#include "DAG/Node/Base/BaseNode.h"

#include <map>

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

class NodeAssignment {
public:
	NodeAssignment();
	NodeAssignment(const size_t aIProc, BaseNode *aNode);
	~NodeAssignment();

public:
	size_t idProc;
	BaseNode *node;
	double startTime, endTime;
};

typedef std::map<size_t, NodeAssignment> assignmentMap_t;

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* NODEASSIGNMENT_H_ */
