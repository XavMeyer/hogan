//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseAlgorithm.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "BaseAlgorithm.h"

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

const double BaseAlgorithm::EPSILON_MEMORY_BARRIER = 0.e-6; // lets say 10 microseconds
const double BaseAlgorithm::DEFAULT_NODE_TIME = 100.e-6; // defining default node time at 100 microseconds


BaseAlgorithm::BaseAlgorithm(const size_t aNThread, const std::list<BaseNode*> &aTopoList) :
		nThread(aNThread), root(aTopoList.back()), topoList(aTopoList) {

}

BaseAlgorithm::~BaseAlgorithm() {
}

prioMap_t& BaseAlgorithm::getPriorities() {
	return priorities;
}

void BaseAlgorithm::initPriorties() {
	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode *node = (*it);
		PriorityNode pNode(node);
		assert(pNode.outDegree < 1.e5);
		pNode.priority = (double)(root->getLevel() - node->getLevel()) + (pNode.outDegree / 1.e5);
		priorities.insert(std::pair<size_t, PriorityNode>(node->getId(), pNode));
	}
}


void BaseAlgorithm::initBLevel() {
	double edgeCost = 0.;

	typedef std::list<BaseNode*>::const_reverse_iterator itList_t;
	for(itList_t it=topoList.rbegin(); it != topoList.rend(); ++it) {
		BaseNode* node = (*it);
		double max = 0;
		for(size_t iP=0; iP<node->getNParent(); ++iP) {
			BaseNode* parent = node->getParent(iP);
			double cost = edgeCost + priorities[parent->getId()].bLevel;
			max = std::max(cost, max);
		}
		assert(node->getAverageTime() >= 0.);
		priorities[node->getId()].bLevel = node->getAverageTime() + max;
	}
}

void BaseAlgorithm::initTLevel() {
	double edgeCost = 0.;

	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode* node = (*it);
		double max = 0;
		for(size_t iC=0; iC<node->getNChildren(); ++iC) {
			BaseNode* child = node->getChild(iC);
			double cost = edgeCost + node->getAverageTime() + priorities[child->getId()].tLevel;
			max = std::max(cost, max);
		}
		priorities[node->getId()].tLevel = max;
	}
}

void BaseAlgorithm::computePriorityCP_MISF() {
	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode* node = (*it);
		PriorityNode &pNode = priorities[node->getId()];
		pNode.priority = pNode.bLevel;
		//std::cout << "Priority for node " << node->getId() << " = " << pNode.priority << std::endl;
	}
}


std::vector<size_t> BaseAlgorithm::defineParentsCountPerProc(const size_t nProc, BaseNode *node, assignmentMap_t &assignMap) {
	typedef assignmentMap_t::iterator itAMap_t;
	std::vector<size_t> parentsCount(nProc, 0);

	// For each children
	for(size_t iC=0; iC<node->getNChildren(); ++iC) {
		// Check if already assigned
		itAMap_t it = assignMap.find(node->getChild(iC)->getId());
		if(it != assignMap.end()) { // If so add 1 to the processor
			parentsCount[it->second.idProc] += 1;
		}
	}

	return parentsCount;
}

size_t BaseAlgorithm::findNextProc(double earlierStartTime, BaseNode *node, std::vector<double> procTime, std::vector<size_t> &nPPP) {

	size_t nPrecurs = node->getNChildren();

	for(size_t iP=0; iP<procTime.size(); ++iP) {
		procTime[iP] = std::max(procTime[iP], earlierStartTime);
		procTime[iP] += (double)(nPrecurs-nPPP[iP])*EPSILON_MEMORY_BARRIER;
	}

	// Find first proc
	size_t argSmallest = 0;
	double smallestTime = procTime[0];
	for(size_t iP=1; iP<procTime.size(); ++iP) {
		if(procTime[iP] < smallestTime) {
			argSmallest = iP;
			smallestTime = procTime[iP];
		} else if(fabs(procTime[iP] - smallestTime) < 1.e-5) {
			if(nPPP[iP] > nPPP[argSmallest]) {
				argSmallest = iP;
				smallestTime = procTime[iP];
			}
		}
	}
	//std::cout << std::endl;

	return argSmallest;

}

void BaseAlgorithm::swapRootNodeToManager(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {

	//std::cout << " Root ID : " << root->getId() << std::endl;  //FIXME

	// Check who as the root
	int idProcRoot = -1;
	for(size_t iP=0; iP<assignVec.size(); ++iP) {
		// std::cout << " Assign back : " << assignVec[iP].back()->getId() << std::endl;  //FIXME
		if(assignVec[iP].back()->getId() == root->getId()) {
			idProcRoot = iP;
			break;
		}
	}
	assert(idProcRoot >= 0);

	// Its not the manager
	if(idProcRoot != 0) {
		// Swap assignement vectors
		std::swap(assignVec[0], assignVec[idProcRoot]);

		// Update idProc in assignMap for the nodes that moved on proc 0
		for(size_t iN=0; iN<assignVec[0].size(); ++iN) {
			assignMap[assignVec[0][iN]->getId()].idProc = 0;
		}

		// Update idProc in assignMap for the nodes that moved on proc that had the rootNode
		for(size_t iN=0; iN<assignVec[idProcRoot].size(); ++iN) {
			assignMap[assignVec[idProcRoot][iN]->getId()].idProc = idProcRoot;
		}
	}
}

double BaseAlgorithm::findEarlierNodeStartTime(BaseNode *node, assignmentMap_t &assignMap) {
	double earlierTime = 0.;

	for(size_t iC=0; iC < node->getNChildren(); ++iC) {
		earlierTime = std::max(earlierTime, assignMap[node->getChild(iC)->getId()].endTime);
	}

	return earlierTime;
}

void BaseAlgorithm::assignByPriority(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {

	// Clear assignements
	assignVec.resize(nThread);
	for(size_t iP=0; iP<assignVec.size(); ++iP) {
		assignVec[iP].clear();
		assignMap.clear();
	}


	// Defined the ordered queue
	std::list<PriorityNode> prioQ;
	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode* node = (*it);
		prioQ.push_back(priorities[node->getId()]);
	}

	prioQ.sort(BiggestPriorityFirst());

	// Processor time counter
	std::vector<double> procTime(nThread, 0.);

	// For each node ordered by priority
	while(!prioQ.empty()) { // For each node, ordered by priority
		// popNode
		BaseNode *node = prioQ.front().node;
		prioQ.pop_front();

		// Count the number of parent of this node on each proc
		std::vector<size_t> nPPP(defineParentsCountPerProc(nThread, node, assignMap));

		// Find the earlier possible start time for this node
		double earlierStartTime = findEarlierNodeStartTime(node, assignMap);

		// Find next proc
		size_t nextProcRdy = findNextProc(earlierStartTime, node, procTime, nPPP);

		// Define procTime, node start time and end time
		NodeAssignment nAssign(nextProcRdy, node);
		double nodeProcTime = node->getNEval() > 0 ? node->getAverageTime() : DEFAULT_NODE_TIME;
		nAssign.startTime = std::max(earlierStartTime, procTime[nextProcRdy]);

		size_t nPrecurs = node->getNChildren();
		nAssign.startTime += (double)(nPrecurs-nPPP[nextProcRdy])*EPSILON_MEMORY_BARRIER;
		nAssign.endTime = nAssign.startTime + nodeProcTime;
		procTime[nextProcRdy] = nAssign.endTime;

		// Assign node to nextProc (MAP + vector of vector of node)
		assignVec[nextProcRdy].push_back(node);
		assignMap[node->getId()] = nAssign;
	}

	swapRootNodeToManager(assignVec, assignMap);

	/*for(size_t iP=0; iP<assignVec.size(); ++iP) {
		std::cout << "Processor " << iP << " must process : ( " << assignVec[iP].size() << " )" << std::endl;
		for(size_t iN=0; iN<assignVec[iP].size(); ++iN) {
			std::cout << assignVec[iP][iN]->toString() << std::endl;
		}
		std::cout << "-------------------------------------------------" << std::endl;
	}*/

}

double BaseAlgorithm::getSchedulingLength(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {
	return assignMap[assignVec.front().back()->getId()].endTime;
}



} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
