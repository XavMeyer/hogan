//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * PriorityNode.h
 *
 *  Created on: Jun 9, 2015
 *      Author: meyerx
 */

#ifndef DAG_SCHEDULER_STATICSCHEDULING_PRIORITYNODE_H_
#define DAG_SCHEDULER_STATICSCHEDULING_PRIORITYNODE_H_

#include "DAG/Node/Base/BaseNode.h"

#include <map>
#include <queue>
#include <vector>
#include <list>

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

struct SmallestPriorityFirst;
struct BiggestPriorityFirst;

class PriorityNode {
public:
	size_t outDegree;
	double priority, tLevel, bLevel;
	BaseNode* node;

public:
	PriorityNode();
	PriorityNode(BaseNode *aNode);
	~PriorityNode();
};

struct SmallestPriorityFirst {
	bool operator() (const PriorityNode& lhs, const PriorityNode& rhs) const {
		if(lhs.priority == rhs.priority) {
			return lhs.outDegree < rhs.outDegree;
		} else {
			return lhs.priority < rhs.priority;
		}
	}
};

struct BiggestPriorityFirst {
	bool operator() (const PriorityNode& lhs, const PriorityNode& rhs) const {
		if(lhs.priority == rhs.priority) {
			return lhs.outDegree > rhs.outDegree;
		} else {
			return lhs.priority > rhs.priority;
		}
	}
};

typedef std::priority_queue<PriorityNode, std::vector<PriorityNode>, BiggestPriorityFirst > prioQ_t;
typedef std::map<size_t, PriorityNode> prioMap_t;

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* DAG_SCHEDULER_STATICSCHEDULING_PRIORITYNODE_H_ */
