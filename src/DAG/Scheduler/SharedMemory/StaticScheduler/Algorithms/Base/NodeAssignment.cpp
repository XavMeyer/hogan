//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NodeAssignment.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "NodeAssignment.h"

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

NodeAssignment::NodeAssignment() :
	idProc(0), node(NULL) {
	startTime = endTime = 0.0;
}

NodeAssignment::NodeAssignment(const size_t aIProc, BaseNode *aNode) :
	idProc(aIProc), node(aNode) {
	startTime = endTime = 0.0;
}

NodeAssignment::~NodeAssignment() {
}

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
