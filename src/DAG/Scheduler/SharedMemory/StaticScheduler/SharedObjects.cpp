//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedObjects.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "SharedObjects.h"

namespace DAG {
namespace Scheduler {
namespace Static {

SharedObjects::SharedObjects(size_t aNThread, DAG::BaseNode const *aRootNode) :
	nThread(aNThread), rootNode(aRootNode), assignVec(nThread),
	signalBuffers(nThread), signalBufferMutex(nThread), waitingForSignal(nThread) {

	fakeComputation = false;
	nextTask = NONE;

	//pthread_mutex_init(&nextTaskMutex, NULL);
	//pthread_cond_init(&waitingForNextTask, NULL);
	pthread_barrier_init(&syncBarrier, NULL, nThread);

	for(size_t iT=0; iT<nThread; ++iT) {
		pthread_mutex_init(&signalBufferMutex[iT], NULL);
		pthread_cond_init(&waitingForSignal[iT], NULL);
	}

}

SharedObjects::~SharedObjects() {

	//pthread_mutex_destroy(&nextTaskMutex);
	//pthread_cond_destroy(&waitingForNextTask);

	for(size_t iT=0; iT<nThread; ++iT) {
		pthread_mutex_destroy(&signalBufferMutex[iT]);
		pthread_cond_destroy(&waitingForSignal[iT]);
	}

	pthread_barrier_destroy(&syncBarrier);
}

void SharedObjects::waitForNextTask() {

	/*pthread_mutex_lock(&nextTaskMutex);
	pthread_cond_wait(&waitingForNextTask, &nextTaskMutex);
	pthread_mutex_unlock(&nextTaskMutex);*/
	pthread_barrier_wait(&syncBarrier);
}

void SharedObjects::sendNextTask(const task_t aNextTask) {

	nextTask = aNextTask;
	pthread_barrier_wait(&syncBarrier);
	/*pthread_mutex_lock(&nextTaskMutex);
	nextTask = aNextTask;
	pthread_cond_broadcast(&waitingForNextTask);
	pthread_mutex_unlock(&nextTaskMutex);*/
}

void SharedObjects::waitEndTask() {
	pthread_barrier_wait(&syncBarrier);
}

void SharedObjects::signalEndTask() {
	pthread_barrier_wait(&syncBarrier);
}


void SharedObjects::signalParentNode(int destProc, size_t completedNodeId) {
	pthread_mutex_lock(&signalBufferMutex[destProc]);
	signalBuffers[destProc].push_back(completedNodeId);
	pthread_cond_signal(&waitingForSignal[destProc]);
	pthread_mutex_unlock(&signalBufferMutex[destProc]);
}

void SharedObjects::waitForSignal(int myProc, std::vector<size_t> &completedNodesId){
	pthread_mutex_lock(&signalBufferMutex[myProc]);
	if(signalBuffers[myProc].empty()) {
		pthread_cond_wait(&waitingForSignal[myProc], &signalBufferMutex[myProc]);
	}
	while(!signalBuffers[myProc].empty()) {
		completedNodesId.push_back(signalBuffers[myProc].back());
		signalBuffers[myProc].pop_back();
	}
	pthread_mutex_unlock(&signalBufferMutex[myProc]);
}

} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
