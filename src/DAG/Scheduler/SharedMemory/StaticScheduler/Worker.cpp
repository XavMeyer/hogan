//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Worker.cpp
 *
 * @date Aug 14, 2015
 * @author meyerx
 * @brief
 */
#include "Worker.h"

namespace DAG {
namespace Scheduler {
namespace Static {

Worker::Worker(const int aId, SharedObjects &aSharedObj) :
	myId(aId), sharedObj(aSharedObj) {

	//std::stringstream ss;
	//ss << "Thread_" << myId << ".txt";
	//oFile.open(ss.str().c_str());

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	//pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);

	/*int fifo_max_prio, fifo_min_prio;
	struct sched_param fifo_param;
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	fifo_max_prio = sched_get_priority_max(SCHED_FIFO);
	fifo_min_prio = sched_get_priority_min(SCHED_FIFO);
	int fifo_mid_prio = (fifo_min_prio + fifo_max_prio)/2;
	fifo_param.sched_priority = fifo_max_prio;
	pthread_attr_setschedparam(&attr, &fifo_param);*/

	/*cpu_set_t cpus;
    CPU_ZERO(&cpus);
    CPU_SET(id, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);*/

	if(myId > 0) {
		pthread_create(&myPthread, &attr, &processHelper, this);
	}

}

Worker::~Worker() {

}


void Worker::init() {
	// Make local copy of assignments
	myNodes = sharedObj.assignVec[myId];
	assignMap = sharedObj.assignMap;

	//oFile << "-------------------------------------------------" << std::endl;
	//for(size_t iN=0; iN<myNodes.size(); ++iN) {
		//oFile << myNodes[iN]->toString() << std::endl;
	//}
	//oFile << "-------------------------------------------------" << std::endl;

}

void Worker::process() {

	for(size_t iN=0; iN<myNodes.size(); ++iN) {
		BaseNode *node = myNodes[iN];
		//oFile << "[" << myId << " ]	" << "Current node : " << node->toString() << std::endl;

		// Node already up to date
		if(node->isDone()) continue;

		// Get the next node (blocking until next node is ready)
		waitUntilReady(node);

		// Process the node
		//std::cout << "[" << myId << " ]	" << "Current node : " << node->toString() << std::endl;
		node->process(sharedObj.fakeComputation);

		// signal the parent of this node that the node has been processed
		signalParentsNode(node);
	}
}

void Worker::run() {
	// Init the shared object
	init();

	do {
		sharedObj.waitForNextTask();
		switch (sharedObj.nextTask) {
			case SharedObjects::INIT_AND_PROCESS:
				init();
				process();
				break;
			case SharedObjects::PROCESS:
				process();
				break;
			case SharedObjects::STOP:
				return; // EXIT HERE
			default:
				break;
		}
		//sharedObj.signalEndTask();
	} while (true);
}

void Worker::join() {
	pthread_join(myPthread, NULL);
}


void Worker::getNextNode(size_t iCur, std::vector<BaseNode*> &tmpNodes) {

	if(tmpNodes[iCur]->isReady() || tmpNodes[iCur]->isScheduled()){
		return;
	}

	for(size_t iN=iCur+1; iN<tmpNodes.size(); ++iN) {
		// check if there is one ready
		if(tmpNodes[iN]->isReady() || tmpNodes[iN]->isScheduled()) {
			std::swap(tmpNodes[iCur], tmpNodes[iN]);
			return;
		}
	}
}

void Worker::waitUntilReady(BaseNode* node) {

	// While not is not ready
	while(!(node->isReady() || node->isScheduled())) {
		// Check for MPI update
		//oFile << "[" << myId << " ]	" << "Waiting for a signal. " << std::endl;
		waitForSignal();
		//oFile << "[" << myId << " ]	state : " << node->toString() << std::endl;
	}

	// Node is considered scheduled and returned
	node->scheduled();
}


void Worker::waitForSignal() {
	std::vector<size_t> completedNodesId;
	sharedObj.waitForSignal(myId, completedNodesId);

	for(size_t iN=0; iN<completedNodesId.size(); ++iN) {
		BaseNode *node = assignMap[completedNodesId[iN]].node;

		for(size_t iP=0; iP<node->getNParent(); ++iP){
			// Get a parent
			BaseNode *parent = node->getParent(iP);

			// Check assignement
			int idOwner = assignMap[parent->getId()].idProc;
			bool isRemote = idOwner != myId;

			if(!isRemote) {
				//oFile << "[" << myId << " ]	" << "[REMOTE] node : " << node->getId() << " signal remote " <<  parent->getId() << " on proc " << idOwner << std::endl;
				parent->signal(node);
				//oFile << "[" << myId << " ]	state parent : " << parent->toString() << std::endl;
			}
		}
	}
}

void Worker::signalParentsNode(BaseNode *node) {
	std::set<int> remoteProc;

	for(size_t i=0; i<node->getNParent(); ++i){
		// Get a parent
		BaseNode *parent = node->getParent(i);

		// Check assignement
		int idOwner = assignMap[parent->getId()].idProc;
		bool isRemote = idOwner != myId;

		 // If it is a remote node
		if(isRemote) {
			// Keep its ownerId
			//oFile << "[" << myId << " ]	" << "Node : " << node->getId() << " signal remote " <<  parent->getId() << " on proc " << idOwner << std::endl;
			remoteProc.insert(idOwner);
		} else { // Local update of parent node
			//oFile << "[" << myId << " ]	" << "Node : " << node->getId() << " signal local " <<  parent->getId() << std::endl;
			parent->signal(node);
		}
	}

	// For each owner, we send a message
	typedef std::set<int>::iterator itSet_t;
	for(itSet_t it=remoteProc.begin(); it != remoteProc.end(); ++it) {
		// Send a non blocking message to owner
		//oFile << "[" << myId << " ]	" << "Send signal for node " << node->getId() << " to proc " << *it << std::endl;
		sharedObj.signalParentNode(*it, node->getId());
	}
}

/*
void Worker::processV2() {

	std::list<BaseNode*> rdyNodes;
	size_t nRemaining = 0;

	for(size_t iN=0; iN<myNodes.size(); ++iN) {
		if(myNodes[iN]->isScheduled()) {
			rdyNodes.push_back(myNodes[iN]);
			nRemaining++;
		} else if(myNodes[iN]->isPending()) {
			nRemaining++;
		}
	}

	while(nRemaining > 0) {
		nRemaining --;

		if(rdyNodes.empty()) {
			waitUntilReadyV2(rdyNodes);
		}

		BaseNode *node = rdyNodes.front();
		rdyNodes.pop_front();

		//std::cout << "[" << myId << " ]	nrem = " << nRemaining << "\t Current node : " << node->toString() << std::endl;

		// Process the node
		node->process();

		// signal the parent of this node that the node has been processed
		signalParentsNodeV2(node, rdyNodes);
	}
}


void Worker::waitUntilReadyV2(std::list<BaseNode*> &rdyNodes) {

	while(rdyNodes.empty()) {
		std::vector<size_t> completedNodesId;
		sharedObj.waitForSignal(myId, completedNodesId);

		for(size_t iN=0; iN<completedNodesId.size(); ++iN) {
			BaseNode *node = assignMap[completedNodesId[iN]].node;

			for(size_t iP=0; iP<node->getNParent(); ++iP){
				// Get a parent
				BaseNode *parent = node->getParent(iP);

				// Check assignement
				int idOwner = assignMap[parent->getId()].idProc;
				bool isRemote = idOwner != myId;

				if(!isRemote) {
					//oFile << "[" << myId << " ]	" << "[REMOTE] node : " << node->getId() << " signal remote " <<  parent->getId() << " on proc " << idOwner << std::endl;
					parent->signal(node);
					if(parent->isReady()) {
						parent->scheduled();
						rdyNodes.push_back(parent);
					}
					//oFile << "[" << myId << " ]	state parent : " << parent->toString() << std::endl;
				}
			}
		}
	}
}

void Worker::signalParentsNodeV2(BaseNode *node, std::list<BaseNode*> &rdyNodes) {
	std::set<int> remoteProc;

	for(size_t i=0; i<node->getNParent(); ++i){
		// Get a parent
		BaseNode *parent = node->getParent(i);

		// Check assignement
		int idOwner = assignMap[parent->getId()].idProc;
		bool isRemote = idOwner != myId;

		 // If it is a remote node
		if(isRemote) {
			// Keep its ownerId
			//oFile << "[" << myId << " ]	" << "Node : " << node->getId() << " signal remote " <<  parent->getId() << " on proc " << idOwner << std::endl;
			remoteProc.insert(idOwner);
		} else { // Local update of parent node
			//oFile << "[" << myId << " ]	" << "Node : " << node->getId() << " signal local " <<  parent->getId() << std::endl;
			parent->signal(node);
			if(parent->isReady()) {
				parent->scheduled();
				rdyNodes.push_back(parent);
			}
		}
	}

	// For each owner, we send a message
	typedef std::set<int>::iterator itSet_t;
	for(itSet_t it=remoteProc.begin(); it != remoteProc.end(); ++it) {
		// Send a non blocking message to owner
		//oFile << "[" << myId << " ]	" << "Send signal for node " << node->getId() << " to proc " << *it << std::endl;
		sharedObj.signalParentNode(*it, node->getId());
	}
}*/


void* Worker::processHelper(void* arg) {
	Worker *ptr = static_cast<Worker*>(arg);
	ptr->run();
	return 0;
}

} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
