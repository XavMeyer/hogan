//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedObjects.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef SHAREDOBJECTS_SCHEDULER_DAG_MPI_SHAREDMEM_SIMPLEPRIO_H_
#define SHAREDOBJECTS_SCHEDULER_DAG_MPI_SHAREDMEM_SIMPLEPRIO_H_

#include "DAG/Node/Base/BaseNode.h"

#include "Parallel/Parallel.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/Algorithms/DefaultAlgorithm.h"

namespace DAG {
namespace Scheduler {
namespace Static {

class SharedObjects {
public:
	enum task_t {INIT_AND_PROCESS=0, PROCESS=1, STOP=2, NONE=100};

public:
	SharedObjects(size_t aNThread, DAG::BaseNode const *aRootNode);
	~SharedObjects();

	void waitForNextTask();
	void sendNextTask(const task_t aNextTask);

	void signalParentNode(int destProc, size_t completedNodeId);
	void waitForSignal(int myProc, std::vector<size_t> &completedNodesId);

	void waitEndTask();
	void signalEndTask();

public:
	const size_t nThread;
	bool fakeComputation;
	DAG::BaseNode const *rootNode;

	Algorithms::vecBaseNode2D_t assignVec;
	Algorithms::assignmentMap_t assignMap;

	task_t nextTask;
	//pthread_mutex_t nextTaskMutex;
	//pthread_cond_t waitingForNextTask;
	pthread_barrier_t syncBarrier;					// Barrier

	typedef std::vector< std::vector< size_t > > signalBuffers_t;
	signalBuffers_t signalBuffers;
	std::vector<pthread_mutex_t> signalBufferMutex;	// Mutex on signalBuffer
	std::vector<pthread_cond_t> waitingForSignal;	// Condition on signalBuffer

};

} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* SHAREDOBJECTS_SCHEDULER_DAG_MPI_SHAREDMEM_SIMPLEPRIO_H_ */
