//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Worker.h
 *
 * @date Aug 14, 2015
 * @author meyerx
 * @brief
 */
#ifndef WORKER_H_
#define WORKER_H_

#include "Parallel/Parallel.h"
#include "SharedObjects.h"
#include <fstream>

namespace DAG {
namespace Scheduler {
namespace Static {

class Worker {
public:
	Worker(const int aId, SharedObjects &aSharedObj);
	~Worker();

	void init();
	void process();

	void run();
	void join();

private:
	const int myId;
	pthread_t myPthread;
	SharedObjects &sharedObj;

	std::vector<BaseNode*> myNodes;
	Algorithms::assignmentMap_t assignMap;

	//std::ofstream oFile;

	void getNextNode(size_t iCur, std::vector<BaseNode*> &tmpNodes);
	void waitUntilReady(BaseNode* node);
	void waitForSignal();
	void signalParentsNode(BaseNode *node);

	/*
	void processV2();
	void waitUntilReadyV2(std::list<BaseNode*> &rdyNodes);
	void signalParentsNodeV2(BaseNode *node, std::list<BaseNode*> &rdyNodes);
	*/

	static void* processHelper(void* arg);

};

} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* WORKER_H_ */
