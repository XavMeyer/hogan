//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedMemoryThread.cpp
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#include "SharedMemoryThread.h"

namespace DAG {
namespace Scheduler {
namespace PriorityList {

SharedMemoryThread::SharedMemoryThread(const size_t aId, SharedObjects &aSharedObj) :
	id(aId), myPthread(0), sharedObj(aSharedObj) {

}

SharedMemoryThread::~SharedMemoryThread() {
}

void SharedMemoryThread::addReadyNode(BaseNode *aReadyNode) {
	readyNodes.push_back(aReadyNode);
}

void SharedMemoryThread::run() {
	/*std::ptrdiff_t l1, l2;
	Eigen::internal::manage_caching_sizes(Eigen::GetAction, &l1, &l2);
	std::cout << "L1 : " << l1 << std::endl;
	std::cout << "L2 : " << l2 << std::endl;*/
	// TODO FIX THAT IN CASE OF MORE THAN 1 MPI PROCESS PER NODE
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	/*cpu_set_t cpus;
	pthread_attr_init(&attr);
    CPU_ZERO(&cpus);
    CPU_SET(id, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);*/

	pthread_create(&myPthread, &attr, &processHelper, this);

	/*Eigen::internal::manage_caching_sizes(Eigen::GetAction, &l1, &l2);
	std::cout << "L1 : " << l1 << std::endl;
	std::cout << "L2 : " << l2 << std::endl;*/

	/*struct sched_param param;
	param.__sched_priority = 0;
	pthread_setschedparam(myPthread, SCHED_BATCH, &param);*/
}

void SharedMemoryThread::join() {
	pthread_join(myPthread, NULL);
}

void* SharedMemoryThread::processHelper(void* arg) {
	SharedMemoryThread *ptr = static_cast<SharedMemoryThread*>(arg);
	ptr->process();
	return 0;
}

void SharedMemoryThread::process() {
	//CustomProfiling cp;

	BaseNode *node;
	// Get the next node pointer
	while((node = getNextNode()) != NULL) { // If not NULL do :
		//cp.startTime();
		//cp.startTime(5);
		node->process(sharedObj.fakeComputation); // Process the content of the node
		//cp.endTime(5);
		//std::cout << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;
//		oFile << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;

		//cp.startTime();
		if(node != sharedObj.rootNode) { // not the root node
			updateDAG(node); // Update the DAG
		} else { // its the root node, we are done
			pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
			sharedObj.traversed = true;
			pthread_cond_broadcast(&sharedObj.globalRdyNotEmtpy);
			pthread_mutex_unlock(&sharedObj.globalRdyMutex);
		}
		//cp.endTime();
		//std::cout << "[" << id << "] Process node " << node->getId() << " during " << cp.duration() << " seconds ( " << cp.duration(5) << " ). " << std::endl;
	}
//	oFile.close();

}

BaseNode* SharedMemoryThread::getNextNode() {
	//CustomProfiling cp;
	//cp.startTime();

	BaseNode *node = NULL;
	// If we have a ready node locally
	if(!readyNodes.empty()) {
		node = readyNodes.front();
		readyNodes.pop_front();
	} else { // otherwise we go global
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
		// We are waiting for an available node or for the DAG to be traversed
		while(sharedObj.prioQ.empty() && !sharedObj.traversed){
			pthread_cond_wait(&sharedObj.globalRdyNotEmtpy, &sharedObj.globalRdyMutex);
		}
		// We try to get a Node else it means that the DAG is traversed
		if(!sharedObj.prioQ.empty()) {
			node = sharedObj.prioQ.top().node;
			sharedObj.prioQ.pop();
		}
		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}
	//cp.endTime();
	//std::cout << "[" << id << "] Get next node took " << cp.duration() << " seconds." << std::endl;

	return node;
}

void SharedMemoryThread::updateDAG(BaseNode *node) {

	std::vector<BaseNode *> tmpReady;

	// Signal all parents and check their readyness
	for(size_t i=0; i<node->getNParent(); ++i){
		// Get a parent
		BaseNode *parent = node->getParent(i);
		// Get its attributed mutex and lock
		pthread_spinlock_t& lock = sharedObj.getNodeSpinlock(parent);
		pthread_spin_lock(&lock);

		// Signal the parent that node is done
		parent->signal(node);
		if(parent->isReady()) { // If the parent is ready
			addReadyParentNode(parent, tmpReady);
		}

		// Release the mutex
		pthread_spin_unlock(&lock);
	}

	// If we have pending ready nodes to be added to the global pool
	if(!tmpReady.empty()) {
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Securize the access to global list
		for(size_t i=0; i<tmpReady.size(); ++i) {
			sharedObj.prioQ.push(sharedObj.priorities[tmpReady[i]->getId()]); // Add new ready node
			if(i>0) {
				pthread_cond_signal(&sharedObj.globalRdyNotEmtpy); // Signal another thread
			}
		}

		readyNodes.push_back(sharedObj.prioQ.top().node);
		sharedObj.prioQ.pop();

		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}

}

void SharedMemoryThread::addReadyParentNode(BaseNode *readyNode, std::vector<BaseNode *> &tmpReady) {
	tmpReady.push_back(readyNode);
	readyNode->scheduled(); // ReadyNode can now be considered as scheduled
}

} /* namespace PriorityList */
} /* namespace Scheduler */
} /* namespace DAG */
