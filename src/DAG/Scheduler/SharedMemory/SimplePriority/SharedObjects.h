//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedObjects.h
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#ifndef SHAREDOBJECTSPRIO_H_
#define SHAREDOBJECTSPRIO_H_

#include <pthread.h>
#include <vector>
#include <list>
#include <queue>
#include <vector>
#include <map>
#include <functional>


#include "Utils/Uncopyable.h"
#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/Algorithms/DefaultAlgorithm.h"

namespace DAG {
namespace Scheduler {
namespace PriorityList {

class SharedObjects : public Uncopyable {
public:
	SharedObjects(BaseNode* const aRootNode, Static::Algorithms::prioQ_t &aPrioQ,
			Static::Algorithms::prioMap_t &aPriorities);
	~SharedObjects();

	pthread_spinlock_t& getNodeSpinlock(const BaseNode *node);

public:

	static const size_t NB_NODE_MUTEX;				// NB of Node mutex
	bool traversed, fakeComputation;				// Has the DAG been fully traversed ?
	pthread_mutex_t globalRdyMutex;					// Mutex on ReadyNodes
	pthread_cond_t globalRdyNotEmtpy;				// Condition on ReadyNodes
	pthread_spinlock_t* nodeSpinlock;				// Spinlock for nodes

	BaseNode const *rootNode;
	Static::Algorithms::prioQ_t &prioQ;
	Static::Algorithms::prioMap_t &priorities;

};

} /* namespace PriorityList */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* SHAREDOBJECTSPRIO_H_ */
