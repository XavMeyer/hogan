//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ThreadSafeScheduler.h
 *
 * @date Feb 26, 2015
 * @author meyerx
 * @brief
 */
#ifndef THREADSAFESCHEDULERPRIO_H_
#define THREADSAFESCHEDULERPRIO_H_

#include "SharedObjects.h"
#include "SharedMemoryThread.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/Algorithms/DefaultAlgorithm.h"

#include "DAG/Scheduler/BaseScheduler.h"
#include "Utils/Code/CustomProfiling.h"

#include <pthread.h>

namespace DAG {
namespace Scheduler {
namespace PriorityList {

class ThreadSafeScheduler: public DAG::Scheduler::BaseScheduler {
public:
	ThreadSafeScheduler(const size_t aNThread, BaseNode* const aRoot);
	~ThreadSafeScheduler();

	void reinitialize(bool isHardInit);

protected:

	void doProcessing(bool doFakeComputation);

private:

	size_t cntCall;
	const size_t nThread;							// NB of threads
	std::vector<SharedMemoryThread*> threadVec;		// Threads

	Static::Algorithms::BaseAlgorithm::sharedPtr_t prioAlgo;
	Static::Algorithms::prioQ_t prioQ;
	Static::Algorithms::prioMap_t &priorities;

	SharedObjects sharedObj;

private:

};


} /* namespace PriorityList */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* THREADSAFESCHEDULERPRIO_H_ */
