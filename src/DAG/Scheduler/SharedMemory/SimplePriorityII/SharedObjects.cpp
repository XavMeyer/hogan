//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedObjects.cpp
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#include "SharedObjects.h"

namespace DAG {
namespace Scheduler {
namespace PriorityListII {

const size_t SharedObjects::NB_NODE_MUTEX = 20;

SharedObjects::SharedObjects(size_t aNThread, BaseNode* const aRootNode, Static::Algorithms::prioQ_t &aPrioQ,
							 Static::Algorithms::prioMap_t &aPriorities) :
		nThread(aNThread), rootNode(aRootNode), prioQ(aPrioQ), priorities(aPriorities) {
	// Init
	traversed = false;
	terminate = false;
	fakeComputation = false;
	pthread_mutex_init(&globalRdyMutex, NULL);
	pthread_cond_init(&globalRdyNotEmtpy, NULL);

	pthread_mutex_init(&threadsMutex, NULL);
	pthread_cond_init(&threadsMutexWait, NULL);

	pthread_mutex_init(&mainMutex, NULL);
	pthread_cond_init(&mainMutexWait, NULL);

	pthread_barrier_init(&syncBarrier, NULL, nThread+1);


	nodeSpinlock = new pthread_spinlock_t[NB_NODE_MUTEX];
	for(size_t i=0; i<NB_NODE_MUTEX; ++i) {
		pthread_spin_init(&nodeSpinlock[i], PTHREAD_PROCESS_SHARED);
	}
}

SharedObjects::~SharedObjects() {
	// Deinit
	pthread_mutex_destroy(&globalRdyMutex);
	pthread_cond_destroy(&globalRdyNotEmtpy);
	pthread_mutex_destroy(&threadsMutex);
	pthread_cond_destroy(&threadsMutexWait);
	pthread_mutex_destroy(&mainMutex);
	pthread_cond_destroy(&mainMutexWait);

	pthread_barrier_destroy(&syncBarrier);

	for(size_t i=0; i<NB_NODE_MUTEX; ++i) {
		pthread_spin_destroy(&nodeSpinlock[i]);
	}
}


pthread_spinlock_t& SharedObjects::getNodeSpinlock(const BaseNode *node) {
	size_t mutexId = node->getId() % NB_NODE_MUTEX;
	return nodeSpinlock[mutexId];
}

} /* namespace PriorityListII */
} /* namespace Scheduler */
} /* namespace DAG */
