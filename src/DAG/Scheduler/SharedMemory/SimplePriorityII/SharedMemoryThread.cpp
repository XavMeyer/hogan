//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedMemoryThread.cpp
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#include "SharedMemoryThread.h"

namespace DAG {
namespace Scheduler {
namespace PriorityListII {

SharedMemoryThread::SharedMemoryThread(const size_t aId, SharedObjects &aSharedObj) :
	id(aId), myPthread(0), sharedObj(aSharedObj) {

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	//pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);

	/*int fifo_max_prio, fifo_min_prio;
	struct sched_param fifo_param;
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	fifo_max_prio = sched_get_priority_max(SCHED_FIFO);
	fifo_min_prio = sched_get_priority_min(SCHED_FIFO);
	int fifo_mid_prio = (fifo_min_prio + fifo_max_prio)/2;
	fifo_param.sched_priority = fifo_max_prio;
	pthread_attr_setschedparam(&attr, &fifo_param);*/

	/*cpu_set_t cpus;
    CPU_ZERO(&cpus);
    CPU_SET(id, &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);*/


	pthread_create(&myPthread, &attr, &processHelper, this);
}

SharedMemoryThread::~SharedMemoryThread() {
}

void SharedMemoryThread::addReadyNode(BaseNode *aReadyNode) {
	readyNodes.push_back(aReadyNode);
}

void SharedMemoryThread::run() {

	// wait for main
	while(true) {
		//std::cout << "Threads wait on main." << std::endl;
		pthread_barrier_wait(&sharedObj.syncBarrier);
		//std::cout << "Threads works." << std::endl;

		if(sharedObj.terminate) {
			return;
		} else {
			process();
		}
		pthread_barrier_wait(&sharedObj.syncBarrier);
	}
}

void SharedMemoryThread::join() {
	pthread_join(myPthread, NULL);
}

void* SharedMemoryThread::processHelper(void* arg) {
	SharedMemoryThread *ptr = static_cast<SharedMemoryThread*>(arg);
	ptr->run();
	return 0;
}

void SharedMemoryThread::process() {
	//CustomProfiling cp;

	BaseNode *node;
	// Get the next node pointer
	while((node = getNextNode()) != NULL) { // If not NULL do :
		//cp.startTime();
		//cp.startTime(5);
		node->process(sharedObj.fakeComputation); // Process the content of the node
		//cp.endTime(5);
		//std::cout << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;
//		oFile << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;

		//cp.startTime();
		if(node != sharedObj.rootNode) { // not the root node
			updateDAG(node); // Update the DAG
		} else { // its the root node, we are done
			pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
			sharedObj.traversed = true;
			// Wake up other thread
			pthread_cond_broadcast(&sharedObj.globalRdyNotEmtpy);
			pthread_mutex_unlock(&sharedObj.globalRdyMutex);
		}
		//cp.endTime();
		//std::cout << "[" << id << "] Process node " << node->getId() << " during " << cp.duration() << " seconds ( " << cp.duration(5) << " ). " << std::endl;
	}
	//std::cout << "[" << id << "] Finished process" << std::endl;
//	oFile.close();

}

BaseNode* SharedMemoryThread::getNextNode() {
	//CustomProfiling cp;
	//cp.startTime();

	BaseNode *node = NULL;
	// If we have a ready node locally
	if(!readyNodes.empty()) {
		node = readyNodes.front();
		readyNodes.pop_front();
	} else { // otherwise we go global
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
		// We are waiting for an available node or for the DAG to be traversed
		while(sharedObj.prioQ.empty() && !sharedObj.traversed){
			//std::cout << "[" << id << "] Waiting : " << sharedObj.prioQ.size();
			//if(sharedObj.traversed) std::cout << " and traversed";
			//std::cout << std::endl;
			pthread_cond_wait(&sharedObj.globalRdyNotEmtpy, &sharedObj.globalRdyMutex);
		}
		// We try to get a Node else it means that the DAG is traversed
		if(!sharedObj.prioQ.empty()) {
			node = sharedObj.prioQ.top().node;
			sharedObj.prioQ.pop();
		}
		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}
	//cp.endTime();
	//std::cout << "[" << id << "] Get next node took " << cp.duration() << " seconds." << std::endl;

	return node;
}

void SharedMemoryThread::updateDAG(BaseNode *node) {

	std::vector<BaseNode *> tmpReady;

	// Signal all parents and check their readyness
	for(size_t i=0; i<node->getNParent(); ++i){
		// Get a parent
		BaseNode *parent = node->getParent(i);
		// Get its attributed mutex and lock
		pthread_spinlock_t& lock = sharedObj.getNodeSpinlock(parent);
		pthread_spin_lock(&lock);

		// Signal the parent that node is done
		parent->signal(node);
		if(parent->isReady()) { // If the parent is ready
			addReadyParentNode(parent, tmpReady);
		}

		// Release the mutex
		pthread_spin_unlock(&lock);
	}

	// If we have pending ready nodes to be added to the global pool
	if(!tmpReady.empty()) {
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Securize the access to global list
		for(size_t i=0; i<tmpReady.size(); ++i) {
			sharedObj.prioQ.push(sharedObj.priorities[tmpReady[i]->getId()]); // Add new ready node
			if(i>0) {
				pthread_cond_signal(&sharedObj.globalRdyNotEmtpy); // Signal another thread
			}
		}

		readyNodes.push_back(sharedObj.prioQ.top().node);
		sharedObj.prioQ.pop();

		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}

}

void SharedMemoryThread::addReadyParentNode(BaseNode *readyNode, std::vector<BaseNode *> &tmpReady) {
	tmpReady.push_back(readyNode);
	readyNode->scheduled(); // ReadyNode can now be considered as scheduled
}

} /* namespace PriorityListII */
} /* namespace Scheduler */
} /* namespace DAG */
