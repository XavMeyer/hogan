//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedMemoryThread.cpp
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#include "SharedMemoryThread.h"

namespace DAG {
namespace Scheduler {
namespace Dynamic {

SharedMemoryThread::SharedMemoryThread(const size_t aId, SharedObjects &aSharedObj) :
	id(aId), myPthread(0), sharedObj(aSharedObj) {

}

SharedMemoryThread::~SharedMemoryThread() {
}

void SharedMemoryThread::addReadyNode(BaseNode *aReadyNode) {
	readyNodes.push_back(aReadyNode);
}

void SharedMemoryThread::run() {
	pthread_create(&myPthread, NULL, &processHelper, this);
}

void SharedMemoryThread::join() {
	pthread_join(myPthread, NULL);
}

void* SharedMemoryThread::processHelper(void* arg) {
	SharedMemoryThread *ptr = static_cast<SharedMemoryThread*>(arg);
	ptr->process();
	return 0;
}

void SharedMemoryThread::process() {

//	std::stringstream ss;
//	ss << "myFile_" << id << ".txt";
//	std::ofstream oFile(ss.str().c_str());
	//CustomProfiling cp;

	BaseNode *node;
	// Get the next node pointer
	while((node = getNextNode()) != NULL) { // If not NULL do :
		//cp.startTime();
		//cp.startTime(5);
		node->process(sharedObj.fakeComputation); // Process the content of the node
		//cp.endTime(5);
		//std::cout << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;
//		oFile << "[" << id << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;

		//cp.startTime();
		if(node != sharedObj.rootNode) { // not the root node
			updateDAG(node); // Update the DAG
		} else { // its the root node, we are done
			pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
			sharedObj.traversed = true;
			pthread_cond_broadcast(&sharedObj.globalRdyNotEmtpy);
			pthread_mutex_unlock(&sharedObj.globalRdyMutex);
		}
		//cp.endTime();
		//std::cout << "[" << id << "] Process node " << node->getId() << " during " << cp.duration() << " seconds ( " << cp.duration(5) << " ). " << std::endl;
	}
//	oFile.close();

}

BaseNode* SharedMemoryThread::getNextNode() {
	//CustomProfiling cp;
	//cp.startTime();

	BaseNode *node = NULL;
	// If we have a ready node locally
	if(!readyNodes.empty()) {
		node = readyNodes.front();
		readyNodes.pop_front();
		//std::cout << "[" << id << "] using local node : " << node->toString() << std::endl;
	} else { // otherwise we go global
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Secures the access to global list
		// We are waiting for an available node or for the DAG to be traversed
		while(sharedObj.readyNodes.empty() && !sharedObj.traversed){
			//std::cout << "[" << id << "] waiting for a node" << std::endl;
			pthread_cond_wait(&sharedObj.globalRdyNotEmtpy, &sharedObj.globalRdyMutex);
			//std::cout << "[" << id << "] no more waiting" << std::endl;
		}
		// We try to get a Node else it means that the DAG is traversed
		if(!sharedObj.readyNodes.empty()) {
			node = sharedObj.readyNodes.front();
			sharedObj.readyNodes.pop_front();
			//std::cout << "[" << id << "] using global node : " << node->toString() << std::endl;
		}
		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}
	//cp.endTime();
	//std::cout << "[" << id << "] Get next node took " << cp.duration() << " seconds." << std::endl;

	return node;
}

void SharedMemoryThread::updateDAG(BaseNode *node) {

	std::vector<BaseNode *> tmpReady;

	// Signal all parents and check their readyness
	for(size_t i=0; i<node->getNParent(); ++i){
		// Get a parent
		BaseNode *parent = node->getParent(i);
		// Get its attributed mutex and lock
		//pthread_mutex_t& mutex = sharedObj.getNodeMutex(parent);
		//pthread_mutex_lock(&mutex);
		pthread_spinlock_t& lock = sharedObj.getNodeSpinlock(parent);
		pthread_spin_lock(&lock);

		// Signal the parent that node is done
		parent->signal(node);
		if(parent->isReady()) { // If the parent is ready
			addReadyParentNode(parent, tmpReady);
		}

		// Release the mutex
		pthread_spin_unlock(&lock);
		//pthread_mutex_unlock(&mutex);
	}

	// If we have pending ready nodes to be added to the global pool
	if(!tmpReady.empty()) {
		pthread_mutex_lock(&sharedObj.globalRdyMutex); // Securize the access to global list
		for(size_t i=0; i<tmpReady.size(); ++i) {
			sharedObj.readyNodes.push_back(tmpReady[i]); // Add new ready node
			pthread_cond_signal(&sharedObj.globalRdyNotEmtpy); // Signal another thread
		}
		pthread_mutex_unlock(&sharedObj.globalRdyMutex);
	}

}

void SharedMemoryThread::addReadyParentNode(BaseNode *readyNode, std::vector<BaseNode *> &tmpReady) {
	if(readyNodes.empty()) { // If we have nothing to do, we keep the job
		readyNodes.push_back(readyNode);
		//std::cout << "[" << id << "] adding node : " << readyNode->toString() << " to local."<< std::endl;
	} else { // else we prepare to share it with other threads
		tmpReady.push_back(readyNode);
		//std::cout << "[" << id << "] adding node : " << readyNode->toString() << " to global."<< std::endl;
	}
	readyNode->scheduled(); // ReadyNode can now be considered as scheduled
}

} /* namespace Dynamic */
} /* namespace Scheduler */
} /* namespace DAG */
