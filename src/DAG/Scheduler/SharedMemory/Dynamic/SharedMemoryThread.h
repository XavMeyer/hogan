//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedMemoryThread.h
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#ifndef SHAREDMEMORYTHREADMUTEX_H_
#define SHAREDMEMORYTHREADMUTEX_H_

#include "Utils/Uncopyable.h"
#include "DAG/Node/Base/BaseNode.h"
#include "SharedObjects.h"

#include <list>
#include <pthread.h>
#include <sstream>
#include <fstream>

namespace DAG {
namespace Scheduler {
namespace Dynamic {

class SharedMemoryThread : public Uncopyable {
public:
	SharedMemoryThread(const size_t aId, SharedObjects &aSharedObj);
	~SharedMemoryThread();

	void addReadyNode(BaseNode *aReadyNode);

	void run();
	void join();

private:
	const size_t id;
	pthread_t myPthread;
	SharedObjects &sharedObj;
	std::list<BaseNode*> readyNodes;

	static void* processHelper(void* arg);
	void process();

	BaseNode* getNextNode();
	void updateDAG(BaseNode *node);
	void addReadyParentNode(BaseNode *readyNode, std::vector<BaseNode *> &tmpReady);

};

} /* namespace Dynamic */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* SHAREDMEMORYTHREADMUTEX_H_ */
