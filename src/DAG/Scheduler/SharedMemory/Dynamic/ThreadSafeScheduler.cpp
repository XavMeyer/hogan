//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ThreadSafeScheduler.cpp
 *
 * @date Jan 20, 2015
 * @author meyerx
 * @brief
 */
#include "ThreadSafeScheduler.h"

namespace DAG {
namespace Scheduler {
namespace Dynamic {

ThreadSafeScheduler::ThreadSafeScheduler(const size_t aNThread, BaseNode* const aRoot) :
		BaseScheduler(aRoot), nThread(aNThread), threadVec(nThread), sharedObj(aRoot, readyNodes) {

	for(size_t i=0; i<nThread; ++i) {
		threadVec[i] = new SharedMemoryThread(i, sharedObj);
	}

}

ThreadSafeScheduler::~ThreadSafeScheduler() {

	for(size_t i=0; i<nThread; ++i) {
		delete threadVec[i];
	}

}


void ThreadSafeScheduler::doProcessing(bool doFakeComputation) {

	// Not yet traversed
	sharedObj.traversed = false;
	sharedObj.fakeComputation = doFakeComputation;

	// Distribute the first nodes (avoid concurrent access on global readyNodes)
	size_t i=0;
	while(!readyNodes.empty()){
		BaseNode *node = readyNodes.front();
		readyNodes.pop_front();
		threadVec[i]->addReadyNode(node);
		i = (i + 1) % nThread;
		//std::cout << "Add node " << node->getId() << " to priority queue." << std::endl;
	}

	// Run each thread
	for(size_t i=0; i<nThread; ++i) {
		threadVec[i]->run();
	}

	// Wait for each thread completion
	for(size_t i=0; i<nThread; ++i) {
		threadVec[i]->join();
	}
}

void ThreadSafeScheduler::reinitialize(bool isHardInit) {
	BaseScheduler::defaultReinitialize(isHardInit);
}

} /* namespace Dynamic */
} /* namespace Scheduler */
} /* namespace DAG */
