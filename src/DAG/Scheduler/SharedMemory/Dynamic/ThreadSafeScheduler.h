//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ThreadSafeScheduler.h
 *
 * @date Feb 26, 2015
 * @author meyerx
 * @brief
 */
#ifndef THREADSAFESCHEDULERMUTEX_H_
#define THREADSAFESCHEDULERMUTEX_H_

#include "DAG/Scheduler/BaseScheduler.h"
#include "SharedObjects.h"
#include "SharedMemoryThread.h"
#include "Utils/Code/CustomProfiling.h"

#include <pthread.h>

namespace DAG {
namespace Scheduler {
namespace Dynamic {

class ThreadSafeScheduler: public DAG::Scheduler::BaseScheduler {
public:
	ThreadSafeScheduler(const size_t aNThread, BaseNode* const aRoot);
	~ThreadSafeScheduler();

	void reinitialize(bool isHardInit);

protected:

	void doProcessing(bool doFakeComputation);

private:

	const size_t nThread;							// NB of threads
	std::vector<SharedMemoryThread*> threadVec;		// Threads
	SharedObjects sharedObj;
};


} /* namespace Dynamic */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* THREADSAFESCHEDULERMUTEX_H_ */
