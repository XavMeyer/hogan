//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SequentialScheduler.cpp
 *
 * @date Jan 20, 2015
 * @author meyerx
 * @brief
 */
#include "SequentialScheduler.h"

namespace DAG {
namespace Scheduler {
namespace Sequential {

SequentialScheduler::SequentialScheduler(BaseNode* const aRoot) : BaseScheduler(aRoot){
}

SequentialScheduler::~SequentialScheduler() {
}

void SequentialScheduler::doProcessing(bool doFakeComputation) {
	if(readyNodes.empty()) {
		//cerr << "Warning : SequentialScheduler::process()" << endl;
		//cerr << "All node are unchanged." << endl;
		return;
	}

	//CustomProfiling cp;

	// While there are node to compute
	BaseNode *node = NULL;
	while(!readyNodes.empty()) {
		// Pop the front of the list
		node = readyNodes.front();
		readyNodes.pop_front();

		// Launch node processing
		//std::cout << "Processing : " << node->toString() << std::endl;

		//cp.startTime();
		node->process(doFakeComputation); // Process the content of the node
		node->signalParents(); // Signal the parent that the processing is done
		//cp.endTime();
		//std::cout << "[" << 0 << "] Processed : " << node->getId() << " in " << cp.duration() << " seconds." << std::endl;

		// Check if parents are now ready
		for(size_t i=0; i<node->getNParent(); ++i) {
			//std::cout<< "Parent [" << node->getParent(i)->getId() << "] is " << (node->getParent(i)->isReady() ? "ready" : "not ready") << std::endl;
			if(node->getParent(i)->isReady()) {
				readyNodes.push_back(node->getParent(i));
				node->getParent(i)->scheduled();
			}
		}

	}

	// Insure that the last node is the root node.
	if(root != node) {
		cerr << "Error : SequentialScheduler::process()" << endl;
		cerr << "The final node [" << node->getId() << "] is not the root node." << endl;
		abort();
	}
}

void SequentialScheduler::reinitialize(bool isHardInit) {
	BaseScheduler::defaultReinitialize(isHardInit, false);
}

} /* namespace Sequential */
} /* namespace Scheduler */
} /* namespace DAG */
