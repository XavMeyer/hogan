//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SequentialScheduler.h
 *
 * @date Jan 20, 2015
 * @author meyerx
 * @brief
 */
#ifndef SEQUENTIALSCHEDULER_H_
#define SEQUENTIALSCHEDULER_H_

#include "../BaseScheduler.h"
#include "Utils/Code/CustomProfiling.h"

#include <Eigen/Core>

namespace DAG {
namespace Scheduler {
namespace Sequential {


class SequentialScheduler: public DAG::Scheduler::BaseScheduler {
public:
	SequentialScheduler(BaseNode* const aRoot);
	~SequentialScheduler();

	void reinitialize(bool isHardInit);

protected:
	void doProcessing(bool doFakeComputation);
};

} /* namespace Sequential */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* SEQUENTIALSCHEDULER_H_ */
