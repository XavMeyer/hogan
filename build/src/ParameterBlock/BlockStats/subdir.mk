################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ParameterBlock/BlockStats/BlockStatsInterface.cpp \
../src/ParameterBlock/BlockStats/EvoBS.cpp \
../src/ParameterBlock/BlockStats/EvoPCABS.cpp 

OBJS += \
./src/ParameterBlock/BlockStats/BlockStatsInterface.o \
./src/ParameterBlock/BlockStats/EvoBS.o \
./src/ParameterBlock/BlockStats/EvoPCABS.o 

CPP_DEPS += \
./src/ParameterBlock/BlockStats/BlockStatsInterface.d \
./src/ParameterBlock/BlockStats/EvoBS.d \
./src/ParameterBlock/BlockStats/EvoPCABS.d 


# Each subdirectory must supply rules for building sources it contributes
src/ParameterBlock/BlockStats/%.o: ../src/ParameterBlock/BlockStats/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


