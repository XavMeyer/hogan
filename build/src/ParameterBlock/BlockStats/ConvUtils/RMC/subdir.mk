################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ParameterBlock/BlockStats/ConvUtils/RMC/DefaultRMC.cpp \
../src/ParameterBlock/BlockStats/ConvUtils/RMC/DeltaRMC.cpp \
../src/ParameterBlock/BlockStats/ConvUtils/RMC/RobinMonroConv.cpp 

OBJS += \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/DefaultRMC.o \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/DeltaRMC.o \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/RobinMonroConv.o 

CPP_DEPS += \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/DefaultRMC.d \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/DeltaRMC.d \
./src/ParameterBlock/BlockStats/ConvUtils/RMC/RobinMonroConv.d 


# Each subdirectory must supply rules for building sources it contributes
src/ParameterBlock/BlockStats/ConvUtils/RMC/%.o: ../src/ParameterBlock/BlockStats/ConvUtils/RMC/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


