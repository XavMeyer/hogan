################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchEvoLU.cpp \
../src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchLightEvoLU.cpp \
../src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LambdaUpdater.cpp \
../src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LightLambdaUpdater.cpp 

OBJS += \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchEvoLU.o \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchLightEvoLU.o \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LambdaUpdater.o \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LightLambdaUpdater.o 

CPP_DEPS += \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchEvoLU.d \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/BatchLightEvoLU.d \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LambdaUpdater.d \
./src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/LightLambdaUpdater.d 


# Each subdirectory must supply rules for building sources it contributes
src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/%.o: ../src/ParameterBlock/BlockStats/ConvUtils/Updater/Lambda/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


