################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ParameterBlock/BlockModifier/TreeModifier/BranchHelper.cpp \
../src/ParameterBlock/BlockModifier/TreeModifier/ESPRPM.cpp \
../src/ParameterBlock/BlockModifier/TreeModifier/STNNIPM.cpp 

OBJS += \
./src/ParameterBlock/BlockModifier/TreeModifier/BranchHelper.o \
./src/ParameterBlock/BlockModifier/TreeModifier/ESPRPM.o \
./src/ParameterBlock/BlockModifier/TreeModifier/STNNIPM.o 

CPP_DEPS += \
./src/ParameterBlock/BlockModifier/TreeModifier/BranchHelper.d \
./src/ParameterBlock/BlockModifier/TreeModifier/ESPRPM.d \
./src/ParameterBlock/BlockModifier/TreeModifier/STNNIPM.d 


# Each subdirectory must supply rules for building sources it contributes
src/ParameterBlock/BlockModifier/TreeModifier/%.o: ../src/ParameterBlock/BlockModifier/TreeModifier/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


