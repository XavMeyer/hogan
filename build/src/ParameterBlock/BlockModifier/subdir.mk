################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ParameterBlock/BlockModifier/BlockModifierInterface.cpp \
../src/ParameterBlock/BlockModifier/EllipsoidReflection.cpp \
../src/ParameterBlock/BlockModifier/GaussianWindowPM.cpp \
../src/ParameterBlock/BlockModifier/MVGaussianWindowPM.cpp \
../src/ParameterBlock/BlockModifier/MultiplierWindowPM.cpp \
../src/ParameterBlock/BlockModifier/PCAWindowPM.cpp \
../src/ParameterBlock/BlockModifier/UniformWindowPM.cpp 

OBJS += \
./src/ParameterBlock/BlockModifier/BlockModifierInterface.o \
./src/ParameterBlock/BlockModifier/EllipsoidReflection.o \
./src/ParameterBlock/BlockModifier/GaussianWindowPM.o \
./src/ParameterBlock/BlockModifier/MVGaussianWindowPM.o \
./src/ParameterBlock/BlockModifier/MultiplierWindowPM.o \
./src/ParameterBlock/BlockModifier/PCAWindowPM.o \
./src/ParameterBlock/BlockModifier/UniformWindowPM.o 

CPP_DEPS += \
./src/ParameterBlock/BlockModifier/BlockModifierInterface.d \
./src/ParameterBlock/BlockModifier/EllipsoidReflection.d \
./src/ParameterBlock/BlockModifier/GaussianWindowPM.d \
./src/ParameterBlock/BlockModifier/MVGaussianWindowPM.d \
./src/ParameterBlock/BlockModifier/MultiplierWindowPM.d \
./src/ParameterBlock/BlockModifier/PCAWindowPM.d \
./src/ParameterBlock/BlockModifier/UniformWindowPM.d 


# Each subdirectory must supply rules for building sources it contributes
src/ParameterBlock/BlockModifier/%.o: ../src/ParameterBlock/BlockModifier/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


