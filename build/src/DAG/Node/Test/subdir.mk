################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DAG/Node/Test/AddNode.cpp \
../src/DAG/Node/Test/LoadNode.cpp \
../src/DAG/Node/Test/MultNode.cpp 

OBJS += \
./src/DAG/Node/Test/AddNode.o \
./src/DAG/Node/Test/LoadNode.o \
./src/DAG/Node/Test/MultNode.o 

CPP_DEPS += \
./src/DAG/Node/Test/AddNode.d \
./src/DAG/Node/Test/LoadNode.d \
./src/DAG/Node/Test/MultNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/DAG/Node/Test/%.o: ../src/DAG/Node/Test/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


