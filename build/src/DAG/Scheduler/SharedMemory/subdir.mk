################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DAG/Scheduler/SharedMemory/SharedMemoryThread.cpp \
../src/DAG/Scheduler/SharedMemory/SharedObjects.cpp \
../src/DAG/Scheduler/SharedMemory/ThreadSafeScheduler.cpp 

OBJS += \
./src/DAG/Scheduler/SharedMemory/SharedMemoryThread.o \
./src/DAG/Scheduler/SharedMemory/SharedObjects.o \
./src/DAG/Scheduler/SharedMemory/ThreadSafeScheduler.o 

CPP_DEPS += \
./src/DAG/Scheduler/SharedMemory/SharedMemoryThread.d \
./src/DAG/Scheduler/SharedMemory/SharedObjects.d \
./src/DAG/Scheduler/SharedMemory/ThreadSafeScheduler.d 


# Each subdirectory must supply rules for building sources it contributes
src/DAG/Scheduler/SharedMemory/%.o: ../src/DAG/Scheduler/SharedMemory/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


