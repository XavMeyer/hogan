################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DAG/Scheduler/SharedMemory/StaticScheduler/SharedObjects.cpp \
../src/DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.cpp \
../src/DAG/Scheduler/SharedMemory/StaticScheduler/Worker.cpp 

OBJS += \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/SharedObjects.o \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.o \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/Worker.o 

CPP_DEPS += \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/SharedObjects.d \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.d \
./src/DAG/Scheduler/SharedMemory/StaticScheduler/Worker.d 


# Each subdirectory must supply rules for building sources it contributes
src/DAG/Scheduler/SharedMemory/StaticScheduler/%.o: ../src/DAG/Scheduler/SharedMemory/StaticScheduler/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


