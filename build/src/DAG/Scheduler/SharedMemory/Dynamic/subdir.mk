################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DAG/Scheduler/SharedMemory/Dynamic/SharedMemoryThread.cpp \
../src/DAG/Scheduler/SharedMemory/Dynamic/SharedObjects.cpp \
../src/DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.cpp 

OBJS += \
./src/DAG/Scheduler/SharedMemory/Dynamic/SharedMemoryThread.o \
./src/DAG/Scheduler/SharedMemory/Dynamic/SharedObjects.o \
./src/DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.o 

CPP_DEPS += \
./src/DAG/Scheduler/SharedMemory/Dynamic/SharedMemoryThread.d \
./src/DAG/Scheduler/SharedMemory/Dynamic/SharedObjects.d \
./src/DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.d 


# Each subdirectory must supply rules for building sources it contributes
src/DAG/Scheduler/SharedMemory/Dynamic/%.o: ../src/DAG/Scheduler/SharedMemory/Dynamic/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


