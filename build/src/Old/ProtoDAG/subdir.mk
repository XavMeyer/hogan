################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Old/ProtoDAG/FillMatrix.cpp \
../src/Old/ProtoDAG/PrototypeDAG.cpp \
../src/Old/ProtoDAG/ResComparator.cpp \
../src/Old/ProtoDAG/TestProcessing.cpp 

OBJS += \
./src/Old/ProtoDAG/FillMatrix.o \
./src/Old/ProtoDAG/PrototypeDAG.o \
./src/Old/ProtoDAG/ResComparator.o \
./src/Old/ProtoDAG/TestProcessing.o 

CPP_DEPS += \
./src/Old/ProtoDAG/FillMatrix.d \
./src/Old/ProtoDAG/PrototypeDAG.d \
./src/Old/ProtoDAG/ResComparator.d \
./src/Old/ProtoDAG/TestProcessing.d 


# Each subdirectory must supply rules for building sources it contributes
src/Old/ProtoDAG/%.o: ../src/Old/ProtoDAG/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/ParallelMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


