################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Old/ProtoMCMC/Bench.cpp \
../src/Old/ProtoMCMC/Bench2.cpp \
../src/Old/ProtoMCMC/LogNrmPara.cpp \
../src/Old/ProtoMCMC/NestedSampling.cpp \
../src/Old/ProtoMCMC/coinTossExamplePara.cpp \
../src/Old/ProtoMCMC/test.cpp \
../src/Old/ProtoMCMC/testBDRAlpha.cpp \
../src/Old/ProtoMCMC/testBDRate.cpp 

OBJS += \
./src/Old/ProtoMCMC/Bench.o \
./src/Old/ProtoMCMC/Bench2.o \
./src/Old/ProtoMCMC/LogNrmPara.o \
./src/Old/ProtoMCMC/NestedSampling.o \
./src/Old/ProtoMCMC/coinTossExamplePara.o \
./src/Old/ProtoMCMC/test.o \
./src/Old/ProtoMCMC/testBDRAlpha.o \
./src/Old/ProtoMCMC/testBDRate.o 

CPP_DEPS += \
./src/Old/ProtoMCMC/Bench.d \
./src/Old/ProtoMCMC/Bench2.d \
./src/Old/ProtoMCMC/LogNrmPara.d \
./src/Old/ProtoMCMC/NestedSampling.d \
./src/Old/ProtoMCMC/coinTossExamplePara.d \
./src/Old/ProtoMCMC/test.d \
./src/Old/ProtoMCMC/testBDRAlpha.d \
./src/Old/ProtoMCMC/testBDRate.d 


# Each subdirectory must supply rules for building sources it contributes
src/Old/ProtoMCMC/%.o: ../src/Old/ProtoMCMC/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/ParallelMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


