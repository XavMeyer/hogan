################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Parallel/RNG/RNG.cpp \
../src/Parallel/RNG/Sitmo11RNG.cpp 

OBJS += \
./src/Parallel/RNG/RNG.o \
./src/Parallel/RNG/Sitmo11RNG.o 

CPP_DEPS += \
./src/Parallel/RNG/RNG.d \
./src/Parallel/RNG/Sitmo11RNG.d 


# Each subdirectory must supply rules for building sources it contributes
src/Parallel/RNG/%.o: ../src/Parallel/RNG/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


