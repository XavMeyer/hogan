################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Parallel/Manager/BaseManager.cpp \
../src/Parallel/Manager/MCMCManager.cpp \
../src/Parallel/Manager/MLManager.cpp \
../src/Parallel/Manager/ManagerUtils.cpp \
../src/Parallel/Manager/MpiManager.cpp \
../src/Parallel/Manager/ParallelLikManager.cpp 

OBJS += \
./src/Parallel/Manager/BaseManager.o \
./src/Parallel/Manager/MCMCManager.o \
./src/Parallel/Manager/MLManager.o \
./src/Parallel/Manager/ManagerUtils.o \
./src/Parallel/Manager/MpiManager.o \
./src/Parallel/Manager/ParallelLikManager.o 

CPP_DEPS += \
./src/Parallel/Manager/BaseManager.d \
./src/Parallel/Manager/MCMCManager.d \
./src/Parallel/Manager/MLManager.d \
./src/Parallel/Manager/ManagerUtils.d \
./src/Parallel/Manager/MpiManager.d \
./src/Parallel/Manager/ParallelLikManager.d 


# Each subdirectory must supply rules for building sources it contributes
src/Parallel/Manager/%.o: ../src/Parallel/Manager/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


