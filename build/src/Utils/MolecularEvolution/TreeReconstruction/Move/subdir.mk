################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/TreeReconstruction/Move/ESPR.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Move/Move.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Move/MoveManager.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Move/STNNI.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.cpp 

OBJS += \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/ESPR.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/Move.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/MoveManager.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/STNNI.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/ESPR.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/Move.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/MoveManager.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/STNNI.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Move/TreeProposal.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/TreeReconstruction/Move/%.o: ../src/Utils/MolecularEvolution/TreeReconstruction/Move/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


