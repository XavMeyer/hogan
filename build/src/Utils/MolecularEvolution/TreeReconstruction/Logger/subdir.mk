################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/TreeReconstruction/Logger/DecisionTree.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Logger/FinalNode.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Logger/PartitionStatistics.cpp \
../src/Utils/MolecularEvolution/TreeReconstruction/Logger/SplitLogger.cpp 

OBJS += \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/DecisionTree.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/FinalNode.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/PartitionStatistics.o \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/SplitLogger.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/DecisionTree.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/FinalNode.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/PartitionStatistics.d \
./src/Utils/MolecularEvolution/TreeReconstruction/Logger/SplitLogger.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/TreeReconstruction/Logger/%.o: ../src/Utils/MolecularEvolution/TreeReconstruction/Logger/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


