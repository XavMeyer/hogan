################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/DataLoader/Alignment/Alignment.cpp \
../src/Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.cpp \
../src/Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.cpp \
../src/Utils/MolecularEvolution/DataLoader/Alignment/MSA.cpp 

OBJS += \
./src/Utils/MolecularEvolution/DataLoader/Alignment/Alignment.o \
./src/Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.o \
./src/Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.o \
./src/Utils/MolecularEvolution/DataLoader/Alignment/MSA.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/DataLoader/Alignment/Alignment.d \
./src/Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.d \
./src/Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.d \
./src/Utils/MolecularEvolution/DataLoader/Alignment/MSA.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/DataLoader/Alignment/%.o: ../src/Utils/MolecularEvolution/DataLoader/Alignment/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


