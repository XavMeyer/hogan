################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/Definitions/AminoAcid.cpp \
../src/Utils/MolecularEvolution/Definitions/AminoAcids.cpp \
../src/Utils/MolecularEvolution/Definitions/Codons.cpp \
../src/Utils/MolecularEvolution/Definitions/MolecularDefHelper.cpp \
../src/Utils/MolecularEvolution/Definitions/Nucleotides.cpp 

OBJS += \
./src/Utils/MolecularEvolution/Definitions/AminoAcid.o \
./src/Utils/MolecularEvolution/Definitions/AminoAcids.o \
./src/Utils/MolecularEvolution/Definitions/Codons.o \
./src/Utils/MolecularEvolution/Definitions/MolecularDefHelper.o \
./src/Utils/MolecularEvolution/Definitions/Nucleotides.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/Definitions/AminoAcid.d \
./src/Utils/MolecularEvolution/Definitions/AminoAcids.d \
./src/Utils/MolecularEvolution/Definitions/Codons.d \
./src/Utils/MolecularEvolution/Definitions/MolecularDefHelper.d \
./src/Utils/MolecularEvolution/Definitions/Nucleotides.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/Definitions/%.o: ../src/Utils/MolecularEvolution/Definitions/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


