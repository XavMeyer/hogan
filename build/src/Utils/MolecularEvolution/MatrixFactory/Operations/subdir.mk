################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/MatrixFactory/Operations/Base.cpp \
../src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDot.cpp \
../src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.cpp \
../src/Utils/MolecularEvolution/MatrixFactory/Operations/Diagonal.cpp 

OBJS += \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/Base.o \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDot.o \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.o \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/Diagonal.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/Base.d \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDot.d \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.d \
./src/Utils/MolecularEvolution/MatrixFactory/Operations/Diagonal.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/MatrixFactory/Operations/%.o: ../src/Utils/MolecularEvolution/MatrixFactory/Operations/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


