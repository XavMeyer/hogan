################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/MolecularEvolution/MatrixFactory/CodonModels/CodeMLMF.cpp \
../src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.cpp \
../src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleSubCodonMF.cpp 

OBJS += \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/CodeMLMF.o \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.o \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleSubCodonMF.o 

CPP_DEPS += \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/CodeMLMF.d \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.d \
./src/Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleSubCodonMF.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/MolecularEvolution/MatrixFactory/CodonModels/%.o: ../src/Utils/MolecularEvolution/MatrixFactory/CodonModels/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


