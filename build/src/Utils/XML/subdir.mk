################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/XML/HelpersXML.cpp \
../src/Utils/XML/ReaderXML.cpp \
../src/Utils/XML/Tag.cpp \
../src/Utils/XML/WriterXML.cpp 

OBJS += \
./src/Utils/XML/HelpersXML.o \
./src/Utils/XML/ReaderXML.o \
./src/Utils/XML/Tag.o \
./src/Utils/XML/WriterXML.o 

CPP_DEPS += \
./src/Utils/XML/HelpersXML.d \
./src/Utils/XML/ReaderXML.d \
./src/Utils/XML/Tag.d \
./src/Utils/XML/WriterXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/XML/%.o: ../src/Utils/XML/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


