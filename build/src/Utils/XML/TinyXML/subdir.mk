################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/XML/TinyXML/tinystr.cpp \
../src/Utils/XML/TinyXML/tinyxml.cpp \
../src/Utils/XML/TinyXML/tinyxmlerror.cpp \
../src/Utils/XML/TinyXML/tinyxmlparser.cpp 

OBJS += \
./src/Utils/XML/TinyXML/tinystr.o \
./src/Utils/XML/TinyXML/tinyxml.o \
./src/Utils/XML/TinyXML/tinyxmlerror.o \
./src/Utils/XML/TinyXML/tinyxmlparser.o 

CPP_DEPS += \
./src/Utils/XML/TinyXML/tinystr.d \
./src/Utils/XML/TinyXML/tinyxml.d \
./src/Utils/XML/TinyXML/tinyxmlerror.d \
./src/Utils/XML/TinyXML/tinyxmlparser.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/XML/TinyXML/%.o: ../src/Utils/XML/TinyXML/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


