################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/XML/Sampler/ReaderSamplerXML.cpp \
../src/Utils/XML/Sampler/WriterSamplerXML.cpp 

OBJS += \
./src/Utils/XML/Sampler/ReaderSamplerXML.o \
./src/Utils/XML/Sampler/WriterSamplerXML.o 

CPP_DEPS += \
./src/Utils/XML/Sampler/ReaderSamplerXML.d \
./src/Utils/XML/Sampler/WriterSamplerXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/XML/Sampler/%.o: ../src/Utils/XML/Sampler/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


