################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/XML/Likelihood/ReaderLikelihoodXML.cpp \
../src/Utils/XML/Likelihood/WriterLikelihoodXML.cpp 

OBJS += \
./src/Utils/XML/Likelihood/ReaderLikelihoodXML.o \
./src/Utils/XML/Likelihood/WriterLikelihoodXML.o 

CPP_DEPS += \
./src/Utils/XML/Likelihood/ReaderLikelihoodXML.d \
./src/Utils/XML/Likelihood/WriterLikelihoodXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/XML/Likelihood/%.o: ../src/Utils/XML/Likelihood/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


