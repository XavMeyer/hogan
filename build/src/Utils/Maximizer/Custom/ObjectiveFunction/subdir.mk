################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/Maximizer/Custom/ObjectiveFunction/LikelihoodFunction.cpp \
../src/Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.cpp \
../src/Utils/Maximizer/Custom/ObjectiveFunction/TestFunctions.cpp 

OBJS += \
./src/Utils/Maximizer/Custom/ObjectiveFunction/LikelihoodFunction.o \
./src/Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.o \
./src/Utils/Maximizer/Custom/ObjectiveFunction/TestFunctions.o 

CPP_DEPS += \
./src/Utils/Maximizer/Custom/ObjectiveFunction/LikelihoodFunction.d \
./src/Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.d \
./src/Utils/Maximizer/Custom/ObjectiveFunction/TestFunctions.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/Maximizer/Custom/ObjectiveFunction/%.o: ../src/Utils/Maximizer/Custom/ObjectiveFunction/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


