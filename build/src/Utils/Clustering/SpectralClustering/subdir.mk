################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/Clustering/SpectralClustering/ClusterRotate.cpp \
../src/Utils/Clustering/SpectralClustering/Evrot.cpp \
../src/Utils/Clustering/SpectralClustering/Kmeans.cpp \
../src/Utils/Clustering/SpectralClustering/SpectralBlockFactory.cpp \
../src/Utils/Clustering/SpectralClustering/SpectralClustering.cpp 

OBJS += \
./src/Utils/Clustering/SpectralClustering/ClusterRotate.o \
./src/Utils/Clustering/SpectralClustering/Evrot.o \
./src/Utils/Clustering/SpectralClustering/Kmeans.o \
./src/Utils/Clustering/SpectralClustering/SpectralBlockFactory.o \
./src/Utils/Clustering/SpectralClustering/SpectralClustering.o 

CPP_DEPS += \
./src/Utils/Clustering/SpectralClustering/ClusterRotate.d \
./src/Utils/Clustering/SpectralClustering/Evrot.d \
./src/Utils/Clustering/SpectralClustering/Kmeans.d \
./src/Utils/Clustering/SpectralClustering/SpectralBlockFactory.d \
./src/Utils/Clustering/SpectralClustering/SpectralClustering.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/Clustering/SpectralClustering/%.o: ../src/Utils/Clustering/SpectralClustering/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


