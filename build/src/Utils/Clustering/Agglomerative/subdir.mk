################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/Clustering/Agglomerative/AgglomerativeBlockFactory.cpp \
../src/Utils/Clustering/Agglomerative/AgglomerativeClustering.cpp \
../src/Utils/Clustering/Agglomerative/FastAgglomerativeClustering.cpp \
../src/Utils/Clustering/Agglomerative/Node.cpp 

OBJS += \
./src/Utils/Clustering/Agglomerative/AgglomerativeBlockFactory.o \
./src/Utils/Clustering/Agglomerative/AgglomerativeClustering.o \
./src/Utils/Clustering/Agglomerative/FastAgglomerativeClustering.o \
./src/Utils/Clustering/Agglomerative/Node.o 

CPP_DEPS += \
./src/Utils/Clustering/Agglomerative/AgglomerativeBlockFactory.d \
./src/Utils/Clustering/Agglomerative/AgglomerativeClustering.d \
./src/Utils/Clustering/Agglomerative/FastAgglomerativeClustering.d \
./src/Utils/Clustering/Agglomerative/Node.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/Clustering/Agglomerative/%.o: ../src/Utils/Clustering/Agglomerative/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


