################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/Clustering/UPGMA/DistanceUPGMA.cpp 

OBJS += \
./src/Utils/Clustering/UPGMA/DistanceUPGMA.o 

CPP_DEPS += \
./src/Utils/Clustering/UPGMA/DistanceUPGMA.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/Clustering/UPGMA/%.o: ../src/Utils/Clustering/UPGMA/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


