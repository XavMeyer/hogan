################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Utils/Code/CheckpointFile.cpp \
../src/Utils/Code/CustomProfiling.cpp \
../src/Utils/Code/Logger.cpp \
../src/Utils/Code/UniqueProfiler.cpp 

OBJS += \
./src/Utils/Code/CheckpointFile.o \
./src/Utils/Code/CustomProfiling.o \
./src/Utils/Code/Logger.o \
./src/Utils/Code/UniqueProfiler.o 

CPP_DEPS += \
./src/Utils/Code/CheckpointFile.d \
./src/Utils/Code/CustomProfiling.d \
./src/Utils/Code/Logger.d \
./src/Utils/Code/UniqueProfiler.d 


# Each subdirectory must supply rules for building sources it contributes
src/Utils/Code/%.o: ../src/Utils/Code/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


