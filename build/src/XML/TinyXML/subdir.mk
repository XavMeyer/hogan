################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/XML/TinyXML/tinystr.cpp \
../src/XML/TinyXML/tinyxml.cpp \
../src/XML/TinyXML/tinyxmlerror.cpp \
../src/XML/TinyXML/tinyxmlparser.cpp 

OBJS += \
./src/XML/TinyXML/tinystr.o \
./src/XML/TinyXML/tinyxml.o \
./src/XML/TinyXML/tinyxmlerror.o \
./src/XML/TinyXML/tinyxmlparser.o 

CPP_DEPS += \
./src/XML/TinyXML/tinystr.d \
./src/XML/TinyXML/tinyxml.d \
./src/XML/TinyXML/tinyxmlerror.d \
./src/XML/TinyXML/tinyxmlparser.d 


# Each subdirectory must supply rules for building sources it contributes
src/XML/TinyXML/%.o: ../src/XML/TinyXML/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/protoMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


