################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/XML/Likelihood/ReaderLikelihoodXML.cpp \
../src/XML/Likelihood/WriterLikelihoodXML.cpp 

OBJS += \
./src/XML/Likelihood/ReaderLikelihoodXML.o \
./src/XML/Likelihood/WriterLikelihoodXML.o 

CPP_DEPS += \
./src/XML/Likelihood/ReaderLikelihoodXML.d \
./src/XML/Likelihood/WriterLikelihoodXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/XML/Likelihood/%.o: ../src/XML/Likelihood/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/protoMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


