################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/XML/HelpersXML.cpp \
../src/XML/ReaderXML.cpp \
../src/XML/Tag.cpp \
../src/XML/WriterXML.cpp 

OBJS += \
./src/XML/HelpersXML.o \
./src/XML/ReaderXML.o \
./src/XML/Tag.o \
./src/XML/WriterXML.o 

CPP_DEPS += \
./src/XML/HelpersXML.d \
./src/XML/ReaderXML.d \
./src/XML/Tag.d \
./src/XML/WriterXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/XML/%.o: ../src/XML/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/protoMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


