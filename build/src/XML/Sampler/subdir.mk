################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/XML/Sampler/ReaderSamplerXML.cpp \
../src/XML/Sampler/WriterSamplerXML.cpp 

OBJS += \
./src/XML/Sampler/ReaderSamplerXML.o \
./src/XML/Sampler/WriterSamplerXML.o 

CPP_DEPS += \
./src/XML/Sampler/ReaderSamplerXML.d \
./src/XML/Sampler/WriterSamplerXML.d 


# Each subdirectory must supply rules for building sources it contributes
src/XML/Sampler/%.o: ../src/XML/Sampler/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/protoMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


