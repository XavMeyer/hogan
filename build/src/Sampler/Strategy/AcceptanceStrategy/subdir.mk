################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.cpp \
../src/Sampler/Strategy/AcceptanceStrategy/BaseMHAS.cpp \
../src/Sampler/Strategy/AcceptanceStrategy/GPMH_MHAS.cpp \
../src/Sampler/Strategy/AcceptanceStrategy/MC3_MHAS.cpp \
../src/Sampler/Strategy/AcceptanceStrategy/TI_MHAS.cpp 

OBJS += \
./src/Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.o \
./src/Sampler/Strategy/AcceptanceStrategy/BaseMHAS.o \
./src/Sampler/Strategy/AcceptanceStrategy/GPMH_MHAS.o \
./src/Sampler/Strategy/AcceptanceStrategy/MC3_MHAS.o \
./src/Sampler/Strategy/AcceptanceStrategy/TI_MHAS.o 

CPP_DEPS += \
./src/Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.d \
./src/Sampler/Strategy/AcceptanceStrategy/BaseMHAS.d \
./src/Sampler/Strategy/AcceptanceStrategy/GPMH_MHAS.d \
./src/Sampler/Strategy/AcceptanceStrategy/MC3_MHAS.d \
./src/Sampler/Strategy/AcceptanceStrategy/TI_MHAS.d 


# Each subdirectory must supply rules for building sources it contributes
src/Sampler/Strategy/AcceptanceStrategy/%.o: ../src/Sampler/Strategy/AcceptanceStrategy/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


