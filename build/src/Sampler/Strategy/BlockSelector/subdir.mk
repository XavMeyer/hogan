################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Sampler/Strategy/BlockSelector/BlockSelector.cpp \
../src/Sampler/Strategy/BlockSelector/OptimisedBS.cpp \
../src/Sampler/Strategy/BlockSelector/OptimisedTIBS.cpp \
../src/Sampler/Strategy/BlockSelector/RandomBS.cpp \
../src/Sampler/Strategy/BlockSelector/RandomTreeInferenceBS.cpp \
../src/Sampler/Strategy/BlockSelector/SingleBS.cpp \
../src/Sampler/Strategy/BlockSelector/SingleCyclicBS.cpp \
../src/Sampler/Strategy/BlockSelector/SingleParamBS.cpp \
../src/Sampler/Strategy/BlockSelector/SingleRevCyclicBS.cpp 

OBJS += \
./src/Sampler/Strategy/BlockSelector/BlockSelector.o \
./src/Sampler/Strategy/BlockSelector/OptimisedBS.o \
./src/Sampler/Strategy/BlockSelector/OptimisedTIBS.o \
./src/Sampler/Strategy/BlockSelector/RandomBS.o \
./src/Sampler/Strategy/BlockSelector/RandomTreeInferenceBS.o \
./src/Sampler/Strategy/BlockSelector/SingleBS.o \
./src/Sampler/Strategy/BlockSelector/SingleCyclicBS.o \
./src/Sampler/Strategy/BlockSelector/SingleParamBS.o \
./src/Sampler/Strategy/BlockSelector/SingleRevCyclicBS.o 

CPP_DEPS += \
./src/Sampler/Strategy/BlockSelector/BlockSelector.d \
./src/Sampler/Strategy/BlockSelector/OptimisedBS.d \
./src/Sampler/Strategy/BlockSelector/OptimisedTIBS.d \
./src/Sampler/Strategy/BlockSelector/RandomBS.d \
./src/Sampler/Strategy/BlockSelector/RandomTreeInferenceBS.d \
./src/Sampler/Strategy/BlockSelector/SingleBS.d \
./src/Sampler/Strategy/BlockSelector/SingleCyclicBS.d \
./src/Sampler/Strategy/BlockSelector/SingleParamBS.d \
./src/Sampler/Strategy/BlockSelector/SingleRevCyclicBS.d 


# Each subdirectory must supply rules for building sources it contributes
src/Sampler/Strategy/BlockSelector/%.o: ../src/Sampler/Strategy/BlockSelector/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


