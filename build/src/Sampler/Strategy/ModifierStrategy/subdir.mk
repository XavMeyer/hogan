################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Sampler/Strategy/ModifierStrategy/DefaultMS.cpp \
../src/Sampler/Strategy/ModifierStrategy/ModifierStrategy.cpp \
../src/Sampler/Strategy/ModifierStrategy/SameSamplesMS.cpp 

OBJS += \
./src/Sampler/Strategy/ModifierStrategy/DefaultMS.o \
./src/Sampler/Strategy/ModifierStrategy/ModifierStrategy.o \
./src/Sampler/Strategy/ModifierStrategy/SameSamplesMS.o 

CPP_DEPS += \
./src/Sampler/Strategy/ModifierStrategy/DefaultMS.d \
./src/Sampler/Strategy/ModifierStrategy/ModifierStrategy.d \
./src/Sampler/Strategy/ModifierStrategy/SameSamplesMS.d 


# Each subdirectory must supply rules for building sources it contributes
src/Sampler/Strategy/ModifierStrategy/%.o: ../src/Sampler/Strategy/ModifierStrategy/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


