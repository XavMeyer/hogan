################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Sampler/Adaptive/AdaptiveBlock.cpp \
../src/Sampler/Adaptive/DefaultAdaptiveBlock.cpp \
../src/Sampler/Adaptive/FixedProposalBlock.cpp \
../src/Sampler/Adaptive/MixedAdaptiveBlock.cpp \
../src/Sampler/Adaptive/PCAAdaptiveBlock.cpp 

OBJS += \
./src/Sampler/Adaptive/AdaptiveBlock.o \
./src/Sampler/Adaptive/DefaultAdaptiveBlock.o \
./src/Sampler/Adaptive/FixedProposalBlock.o \
./src/Sampler/Adaptive/MixedAdaptiveBlock.o \
./src/Sampler/Adaptive/PCAAdaptiveBlock.o 

CPP_DEPS += \
./src/Sampler/Adaptive/AdaptiveBlock.d \
./src/Sampler/Adaptive/DefaultAdaptiveBlock.d \
./src/Sampler/Adaptive/FixedProposalBlock.d \
./src/Sampler/Adaptive/MixedAdaptiveBlock.d \
./src/Sampler/Adaptive/PCAAdaptiveBlock.d 


# Each subdirectory must supply rules for building sources it contributes
src/Sampler/Adaptive/%.o: ../src/Sampler/Adaptive/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


