################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Simulator/MolecularEvolution/Nodes/BranchMatrixNode.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/GatherResults.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/MatrixNode.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/MatrixScalingNode.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/ParentNode.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/RootNode.cpp \
../src/Model/Simulator/MolecularEvolution/Nodes/SucccessorNode.cpp 

OBJS += \
./src/Model/Simulator/MolecularEvolution/Nodes/BranchMatrixNode.o \
./src/Model/Simulator/MolecularEvolution/Nodes/GatherResults.o \
./src/Model/Simulator/MolecularEvolution/Nodes/MatrixNode.o \
./src/Model/Simulator/MolecularEvolution/Nodes/MatrixScalingNode.o \
./src/Model/Simulator/MolecularEvolution/Nodes/ParentNode.o \
./src/Model/Simulator/MolecularEvolution/Nodes/RootNode.o \
./src/Model/Simulator/MolecularEvolution/Nodes/SucccessorNode.o 

CPP_DEPS += \
./src/Model/Simulator/MolecularEvolution/Nodes/BranchMatrixNode.d \
./src/Model/Simulator/MolecularEvolution/Nodes/GatherResults.d \
./src/Model/Simulator/MolecularEvolution/Nodes/MatrixNode.d \
./src/Model/Simulator/MolecularEvolution/Nodes/MatrixScalingNode.d \
./src/Model/Simulator/MolecularEvolution/Nodes/ParentNode.d \
./src/Model/Simulator/MolecularEvolution/Nodes/RootNode.d \
./src/Model/Simulator/MolecularEvolution/Nodes/SucccessorNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Simulator/MolecularEvolution/Nodes/%.o: ../src/Model/Simulator/MolecularEvolution/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


