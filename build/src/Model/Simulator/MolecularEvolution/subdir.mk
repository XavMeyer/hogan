################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Simulator/MolecularEvolution/BranchModel.cpp \
../src/Model/Simulator/MolecularEvolution/NucleotideModel.cpp \
../src/Model/Simulator/MolecularEvolution/OmegaSimulator.cpp \
../src/Model/Simulator/MolecularEvolution/SimNode.cpp \
../src/Model/Simulator/MolecularEvolution/TreeModel.cpp 

OBJS += \
./src/Model/Simulator/MolecularEvolution/BranchModel.o \
./src/Model/Simulator/MolecularEvolution/NucleotideModel.o \
./src/Model/Simulator/MolecularEvolution/OmegaSimulator.o \
./src/Model/Simulator/MolecularEvolution/SimNode.o \
./src/Model/Simulator/MolecularEvolution/TreeModel.o 

CPP_DEPS += \
./src/Model/Simulator/MolecularEvolution/BranchModel.d \
./src/Model/Simulator/MolecularEvolution/NucleotideModel.d \
./src/Model/Simulator/MolecularEvolution/OmegaSimulator.d \
./src/Model/Simulator/MolecularEvolution/SimNode.d \
./src/Model/Simulator/MolecularEvolution/TreeModel.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Simulator/MolecularEvolution/%.o: ../src/Model/Simulator/MolecularEvolution/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


