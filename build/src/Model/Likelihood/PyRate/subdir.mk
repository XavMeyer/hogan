################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/PyRate/BDRAlphaBigUpd.cpp \
../src/Model/Likelihood/PyRate/BDRate.cpp \
../src/Model/Likelihood/PyRate/BDRateBigUpd.cpp \
../src/Model/Likelihood/PyRate/BDRateUpd.cpp 

OBJS += \
./src/Model/Likelihood/PyRate/BDRAlphaBigUpd.o \
./src/Model/Likelihood/PyRate/BDRate.o \
./src/Model/Likelihood/PyRate/BDRateBigUpd.o \
./src/Model/Likelihood/PyRate/BDRateUpd.o 

CPP_DEPS += \
./src/Model/Likelihood/PyRate/BDRAlphaBigUpd.d \
./src/Model/Likelihood/PyRate/BDRate.d \
./src/Model/Likelihood/PyRate/BDRateBigUpd.d \
./src/Model/Likelihood/PyRate/BDRateUpd.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/PyRate/%.o: ../src/Model/Likelihood/PyRate/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/ParallelMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


