################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/TreeInference/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/CPVNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/LeafNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/ParentNode.cpp \
../src/Model/Likelihood/TreeInference/Nodes/Proportions.cpp \
../src/Model/Likelihood/TreeInference/Nodes/RootNode.cpp 

OBJS += \
./src/Model/Likelihood/TreeInference/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/TreeInference/Nodes/CPVNode.o \
./src/Model/Likelihood/TreeInference/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/TreeInference/Nodes/EdgeNode.o \
./src/Model/Likelihood/TreeInference/Nodes/FinalResultNode.o \
./src/Model/Likelihood/TreeInference/Nodes/LeafNode.o \
./src/Model/Likelihood/TreeInference/Nodes/MatrixNode.o \
./src/Model/Likelihood/TreeInference/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/TreeInference/Nodes/ParentNode.o \
./src/Model/Likelihood/TreeInference/Nodes/Proportions.o \
./src/Model/Likelihood/TreeInference/Nodes/RootNode.o 

CPP_DEPS += \
./src/Model/Likelihood/TreeInference/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/TreeInference/Nodes/CPVNode.d \
./src/Model/Likelihood/TreeInference/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/TreeInference/Nodes/EdgeNode.d \
./src/Model/Likelihood/TreeInference/Nodes/FinalResultNode.d \
./src/Model/Likelihood/TreeInference/Nodes/LeafNode.d \
./src/Model/Likelihood/TreeInference/Nodes/MatrixNode.d \
./src/Model/Likelihood/TreeInference/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/TreeInference/Nodes/ParentNode.d \
./src/Model/Likelihood/TreeInference/Nodes/Proportions.d \
./src/Model/Likelihood/TreeInference/Nodes/RootNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/TreeInference/Nodes/%.o: ../src/Model/Likelihood/TreeInference/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


