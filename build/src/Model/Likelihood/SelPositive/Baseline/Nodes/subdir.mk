################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineClassNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/InternalNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/LeafNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/Proportions.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/RootNode.cpp \
../src/Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.cpp 

OBJS += \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineClassNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/InternalNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/LeafNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/Proportions.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/RootNode.o \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.o 

CPP_DEPS += \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineClassNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/InternalNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/LeafNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/Proportions.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/RootNode.d \
./src/Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/SelPositive/Baseline/Nodes/%.o: ../src/Model/Likelihood/SelPositive/Baseline/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


