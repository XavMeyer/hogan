################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/SelPositive/Light/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/CPV.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/CPVNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/LeafNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/ParentNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/Proportions.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/RootNode.cpp \
../src/Model/Likelihood/SelPositive/Light/Nodes/TypesHelper.cpp 

OBJS += \
./src/Model/Likelihood/SelPositive/Light/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/CPV.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/CPVNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/FinalResultNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/LeafNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/ParentNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/Proportions.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/RootNode.o \
./src/Model/Likelihood/SelPositive/Light/Nodes/TypesHelper.o 

CPP_DEPS += \
./src/Model/Likelihood/SelPositive/Light/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/CPV.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/CPVNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/FinalResultNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/LeafNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/ParentNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/Proportions.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/RootNode.d \
./src/Model/Likelihood/SelPositive/Light/Nodes/TypesHelper.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/SelPositive/Light/Nodes/%.o: ../src/Model/Likelihood/SelPositive/Light/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


