################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/Helper/BDRateHlp.cpp \
../src/Model/Likelihood/Helper/CodonModelsHlp.cpp \
../src/Model/Likelihood/Helper/CorrelNormalHlp.cpp \
../src/Model/Likelihood/Helper/HelperInterface.cpp \
../src/Model/Likelihood/Helper/IndepLogNormalHlp.cpp \
../src/Model/Likelihood/Helper/TreeInferenceHlp.cpp \
../src/Model/Likelihood/Helper/UnivarDistribHlp.cpp 

OBJS += \
./src/Model/Likelihood/Helper/BDRateHlp.o \
./src/Model/Likelihood/Helper/CodonModelsHlp.o \
./src/Model/Likelihood/Helper/CorrelNormalHlp.o \
./src/Model/Likelihood/Helper/HelperInterface.o \
./src/Model/Likelihood/Helper/IndepLogNormalHlp.o \
./src/Model/Likelihood/Helper/TreeInferenceHlp.o \
./src/Model/Likelihood/Helper/UnivarDistribHlp.o 

CPP_DEPS += \
./src/Model/Likelihood/Helper/BDRateHlp.d \
./src/Model/Likelihood/Helper/CodonModelsHlp.d \
./src/Model/Likelihood/Helper/CorrelNormalHlp.d \
./src/Model/Likelihood/Helper/HelperInterface.d \
./src/Model/Likelihood/Helper/IndepLogNormalHlp.d \
./src/Model/Likelihood/Helper/TreeInferenceHlp.d \
./src/Model/Likelihood/Helper/UnivarDistribHlp.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/Helper/%.o: ../src/Model/Likelihood/Helper/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


