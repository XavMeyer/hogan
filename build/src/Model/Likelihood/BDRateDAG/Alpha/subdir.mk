################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BDRateDAG/Alpha/AlphaLik.cpp \
../src/Model/Likelihood/BDRateDAG/Alpha/AlphaV2Lik.cpp \
../src/Model/Likelihood/BDRateDAG/Alpha/SampleWrapper.cpp 

OBJS += \
./src/Model/Likelihood/BDRateDAG/Alpha/AlphaLik.o \
./src/Model/Likelihood/BDRateDAG/Alpha/AlphaV2Lik.o \
./src/Model/Likelihood/BDRateDAG/Alpha/SampleWrapper.o 

CPP_DEPS += \
./src/Model/Likelihood/BDRateDAG/Alpha/AlphaLik.d \
./src/Model/Likelihood/BDRateDAG/Alpha/AlphaV2Lik.d \
./src/Model/Likelihood/BDRateDAG/Alpha/SampleWrapper.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BDRateDAG/Alpha/%.o: ../src/Model/Likelihood/BDRateDAG/Alpha/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


