################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/StochasticBS/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/CPV.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/CPVNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/LeafNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/MatrixSNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/ParentNode.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/Proportions.cpp \
../src/Model/Likelihood/StochasticBS/Nodes/RootNode.cpp 

OBJS += \
./src/Model/Likelihood/StochasticBS/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/CPV.o \
./src/Model/Likelihood/StochasticBS/Nodes/CPVNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/EdgeNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/FinalResultNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/LeafNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixSNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/ParentNode.o \
./src/Model/Likelihood/StochasticBS/Nodes/Proportions.o \
./src/Model/Likelihood/StochasticBS/Nodes/RootNode.o 

CPP_DEPS += \
./src/Model/Likelihood/StochasticBS/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/CPV.d \
./src/Model/Likelihood/StochasticBS/Nodes/CPVNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/EdgeNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/FinalResultNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/LeafNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixSNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/ParentNode.d \
./src/Model/Likelihood/StochasticBS/Nodes/Proportions.d \
./src/Model/Likelihood/StochasticBS/Nodes/RootNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/StochasticBS/Nodes/%.o: ../src/Model/Likelihood/StochasticBS/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


