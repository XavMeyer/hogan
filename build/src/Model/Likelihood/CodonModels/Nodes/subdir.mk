################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/CodonModels/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/CPV.cpp \
../src/Model/Likelihood/CodonModels/Nodes/CPVNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/LeafNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/ParentNode.cpp \
../src/Model/Likelihood/CodonModels/Nodes/Proportions.cpp \
../src/Model/Likelihood/CodonModels/Nodes/RootNode.cpp 

OBJS += \
./src/Model/Likelihood/CodonModels/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/CodonModels/Nodes/CPV.o \
./src/Model/Likelihood/CodonModels/Nodes/CPVNode.o \
./src/Model/Likelihood/CodonModels/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/CodonModels/Nodes/EdgeNode.o \
./src/Model/Likelihood/CodonModels/Nodes/FinalResultNode.o \
./src/Model/Likelihood/CodonModels/Nodes/LeafNode.o \
./src/Model/Likelihood/CodonModels/Nodes/MatrixNode.o \
./src/Model/Likelihood/CodonModels/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/CodonModels/Nodes/ParentNode.o \
./src/Model/Likelihood/CodonModels/Nodes/Proportions.o \
./src/Model/Likelihood/CodonModels/Nodes/RootNode.o 

CPP_DEPS += \
./src/Model/Likelihood/CodonModels/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/CodonModels/Nodes/CPV.d \
./src/Model/Likelihood/CodonModels/Nodes/CPVNode.d \
./src/Model/Likelihood/CodonModels/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/CodonModels/Nodes/EdgeNode.d \
./src/Model/Likelihood/CodonModels/Nodes/FinalResultNode.d \
./src/Model/Likelihood/CodonModels/Nodes/LeafNode.d \
./src/Model/Likelihood/CodonModels/Nodes/MatrixNode.d \
./src/Model/Likelihood/CodonModels/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/CodonModels/Nodes/ParentNode.d \
./src/Model/Likelihood/CodonModels/Nodes/Proportions.d \
./src/Model/Likelihood/CodonModels/Nodes/RootNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/CodonModels/Nodes/%.o: ../src/Model/Likelihood/CodonModels/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


