################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BranchSiteRELV2/BranchSiteRELik.cpp \
../src/Model/Likelihood/BranchSiteRELV2/SampleWrapper.cpp \
../src/Model/Likelihood/BranchSiteRELV2/TreeNodeREL.cpp 

OBJS += \
./src/Model/Likelihood/BranchSiteRELV2/BranchSiteRELik.o \
./src/Model/Likelihood/BranchSiteRELV2/SampleWrapper.o \
./src/Model/Likelihood/BranchSiteRELV2/TreeNodeREL.o 

CPP_DEPS += \
./src/Model/Likelihood/BranchSiteRELV2/BranchSiteRELik.d \
./src/Model/Likelihood/BranchSiteRELV2/SampleWrapper.d \
./src/Model/Likelihood/BranchSiteRELV2/TreeNodeREL.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BranchSiteRELV2/%.o: ../src/Model/Likelihood/BranchSiteRELV2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


