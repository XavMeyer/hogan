################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/CPV.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/CPVNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/CombinedBMNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/LeafNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/ParentNode.cpp \
../src/Model/Likelihood/BranchSiteRELV2/Nodes/RootNode.cpp 

OBJS += \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CPV.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CPVNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CombinedBMNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/EdgeNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/FinalResultNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/LeafNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/MatrixNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/ParentNode.o \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/RootNode.o 

CPP_DEPS += \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CPV.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CPVNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/CombinedBMNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/EdgeNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/FinalResultNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/LeafNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/MatrixNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/ParentNode.d \
./src/Model/Likelihood/BranchSiteRELV2/Nodes/RootNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BranchSiteRELV2/Nodes/%.o: ../src/Model/Likelihood/BranchSiteRELV2/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


