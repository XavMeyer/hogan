################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/Test/YangModel/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/Test/YangModel/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/Test/YangModel/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/Test/YangModel/Nodes/ProcNode.cpp 

OBJS += \
./src/Model/Likelihood/Test/YangModel/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/Test/YangModel/Nodes/MatrixNode.o \
./src/Model/Likelihood/Test/YangModel/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/Test/YangModel/Nodes/ProcNode.o 

CPP_DEPS += \
./src/Model/Likelihood/Test/YangModel/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/Test/YangModel/Nodes/MatrixNode.d \
./src/Model/Likelihood/Test/YangModel/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/Test/YangModel/Nodes/ProcNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/Test/YangModel/Nodes/%.o: ../src/Model/Likelihood/Test/YangModel/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


