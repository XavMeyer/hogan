################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/Test/CorrelatedNormals.cpp \
../src/Model/Likelihood/Test/LogNormalLH.cpp \
../src/Model/Likelihood/Test/LogNormalNDLH.cpp \
../src/Model/Likelihood/Test/NormalLH.cpp 

OBJS += \
./src/Model/Likelihood/Test/CorrelatedNormals.o \
./src/Model/Likelihood/Test/LogNormalLH.o \
./src/Model/Likelihood/Test/LogNormalNDLH.o \
./src/Model/Likelihood/Test/NormalLH.o 

CPP_DEPS += \
./src/Model/Likelihood/Test/CorrelatedNormals.d \
./src/Model/Likelihood/Test/LogNormalLH.d \
./src/Model/Likelihood/Test/LogNormalNDLH.d \
./src/Model/Likelihood/Test/NormalLH.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/Test/%.o: ../src/Model/Likelihood/Test/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


