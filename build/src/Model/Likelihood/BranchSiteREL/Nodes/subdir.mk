################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BranchSiteREL/Nodes/BranchMatrixNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/CPV.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/CPVNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/CombineSiteNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/CombinedBMNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/EdgeNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/FinalResultNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/LeafNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/MatrixNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/ParentNode.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/Proportions.cpp \
../src/Model/Likelihood/BranchSiteREL/Nodes/RootNode.cpp 

OBJS += \
./src/Model/Likelihood/BranchSiteREL/Nodes/BranchMatrixNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/CPV.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/CPVNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/CombineSiteNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/CombinedBMNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/EdgeNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/FinalResultNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/LeafNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/MatrixNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/ParentNode.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/Proportions.o \
./src/Model/Likelihood/BranchSiteREL/Nodes/RootNode.o 

CPP_DEPS += \
./src/Model/Likelihood/BranchSiteREL/Nodes/BranchMatrixNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/CPV.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/CPVNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/CombineSiteNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/CombinedBMNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/EdgeNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/FinalResultNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/LeafNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/MatrixNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/ParentNode.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/Proportions.d \
./src/Model/Likelihood/BranchSiteREL/Nodes/RootNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BranchSiteREL/Nodes/%.o: ../src/Model/Likelihood/BranchSiteREL/Nodes/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


