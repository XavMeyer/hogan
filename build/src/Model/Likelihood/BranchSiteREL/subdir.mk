################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BranchSiteREL/BranchSiteRELik.cpp \
../src/Model/Likelihood/BranchSiteREL/SampleWrapper.cpp \
../src/Model/Likelihood/BranchSiteREL/TreeNodeREL.cpp 

OBJS += \
./src/Model/Likelihood/BranchSiteREL/BranchSiteRELik.o \
./src/Model/Likelihood/BranchSiteREL/SampleWrapper.o \
./src/Model/Likelihood/BranchSiteREL/TreeNodeREL.o 

CPP_DEPS += \
./src/Model/Likelihood/BranchSiteREL/BranchSiteRELik.d \
./src/Model/Likelihood/BranchSiteREL/SampleWrapper.d \
./src/Model/Likelihood/BranchSiteREL/TreeNodeREL.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BranchSiteREL/%.o: ../src/Model/Likelihood/BranchSiteREL/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


