################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BDRate/BDRAlphaBigUpd.cpp \
../src/Model/Likelihood/BDRate/BDRateBigUpd.cpp \
../src/Model/Likelihood/BDRate/BDRateStd.cpp \
../src/Model/Likelihood/BDRate/BDRateUpd.cpp 

OBJS += \
./src/Model/Likelihood/BDRate/BDRAlphaBigUpd.o \
./src/Model/Likelihood/BDRate/BDRateBigUpd.o \
./src/Model/Likelihood/BDRate/BDRateStd.o \
./src/Model/Likelihood/BDRate/BDRateUpd.o 

CPP_DEPS += \
./src/Model/Likelihood/BDRate/BDRAlphaBigUpd.d \
./src/Model/Likelihood/BDRate/BDRateBigUpd.d \
./src/Model/Likelihood/BDRate/BDRateStd.d \
./src/Model/Likelihood/BDRate/BDRateUpd.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BDRate/%.o: ../src/Model/Likelihood/BDRate/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


