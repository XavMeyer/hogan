################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/BDRate/Alpha/AlphaLik.cpp \
../src/Model/Likelihood/BDRate/Alpha/AlphaV2Lik.cpp \
../src/Model/Likelihood/BDRate/Alpha/SampleWrapper.cpp 

OBJS += \
./src/Model/Likelihood/BDRate/Alpha/AlphaLik.o \
./src/Model/Likelihood/BDRate/Alpha/AlphaV2Lik.o \
./src/Model/Likelihood/BDRate/Alpha/SampleWrapper.o 

CPP_DEPS += \
./src/Model/Likelihood/BDRate/Alpha/AlphaLik.d \
./src/Model/Likelihood/BDRate/Alpha/AlphaV2Lik.d \
./src/Model/Likelihood/BDRate/Alpha/SampleWrapper.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/BDRate/Alpha/%.o: ../src/Model/Likelihood/BDRate/Alpha/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/ParallelMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


