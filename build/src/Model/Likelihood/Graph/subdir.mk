################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Likelihood/Graph/GraphLik.cpp \
../src/Model/Likelihood/Graph/GraphNode.cpp 

OBJS += \
./src/Model/Likelihood/Graph/GraphLik.o \
./src/Model/Likelihood/Graph/GraphNode.o 

CPP_DEPS += \
./src/Model/Likelihood/Graph/GraphLik.d \
./src/Model/Likelihood/Graph/GraphNode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Likelihood/Graph/%.o: ../src/Model/Likelihood/Graph/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


