################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Prior/Beta_Boost.cpp \
../src/Model/Prior/DirichletPDF.cpp \
../src/Model/Prior/ExponentialBoost.cpp \
../src/Model/Prior/Gamma_Boost.cpp \
../src/Model/Prior/Gaussian_Boost.cpp \
../src/Model/Prior/NoPrior.cpp \
../src/Model/Prior/PriorInterface.cpp \
../src/Model/Prior/Uniform_Boost.cpp 

OBJS += \
./src/Model/Prior/Beta_Boost.o \
./src/Model/Prior/DirichletPDF.o \
./src/Model/Prior/ExponentialBoost.o \
./src/Model/Prior/Gamma_Boost.o \
./src/Model/Prior/Gaussian_Boost.o \
./src/Model/Prior/NoPrior.o \
./src/Model/Prior/PriorInterface.o \
./src/Model/Prior/Uniform_Boost.o 

CPP_DEPS += \
./src/Model/Prior/Beta_Boost.d \
./src/Model/Prior/DirichletPDF.d \
./src/Model/Prior/ExponentialBoost.d \
./src/Model/Prior/Gamma_Boost.d \
./src/Model/Prior/Gaussian_Boost.d \
./src/Model/Prior/NoPrior.d \
./src/Model/Prior/PriorInterface.d \
./src/Model/Prior/Uniform_Boost.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/Prior/%.o: ../src/Model/Prior/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -DBOOST_MATH_PROMOTE_DOUBLE_POLICY=false -DEIGEN_DONT_PARALLELIZE -I../src -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -m64 -march=native -funroll-loops -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


