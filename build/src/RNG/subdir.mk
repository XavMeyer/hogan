################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/RNG/RNG.cpp \
../src/RNG/Sitmo11RNG.cpp 

OBJS += \
./src/RNG/RNG.o \
./src/RNG/Sitmo11RNG.o 

CPP_DEPS += \
./src/RNG/RNG.d \
./src/RNG/Sitmo11RNG.d 


# Each subdirectory must supply rules for building sources it contributes
src/RNG/%.o: ../src/RNG/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	mpicxx -DTIXML_USE_STL -I"/home/meyerx/workspace/protoMCMC/src" -I$(MPI_INC) -I$(EIGEN_INC) -I$(BOOST_INC) -I$(CXX_INC)  -O3 -Wall -c -fmessage-length=0 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


