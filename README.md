# README #

Source code for HOGAN, the parallel M-H algorithm with multivariate adaptive proposals.

HOGAN is coded in C++ with:

 * Posix threads
 * MPI
 * boost
 * EIGEN

-----------------------------------------------------------------------

### Repository content ###

Content of each directory:

 * **build/** >> Contains the makefile and sub-makefiles.

 * **examples/** >> Contains basic files to test the implementation.

 * **examples/config/** >> Contains the configuration XML files.

 * **examples/data/** >> Contains some example input files.

 * **src/** >> Contains the source code.

-----------------------------------------------------------------------

### How to compile ###

a) To compile HOGAN you must first install:
> 1. [Boost C++](http://www.boost.org/) with at least version 1.56
> 2. [EIGEN C++](http://eigen.tuxfamily.org) with at least version 3
> 3. [MPI](https://www.open-mpi.org/)

b) Go in the **build/** folder.
> 1. Configure the path for Boost, Eigen, MPI and C++ if needed
> 2. make all or make -j *p* all
> > use *p* processor for compiling HOGAN faster

-----------------------------------------------------------------------

### How to use ###

Go back in the *root* directory.

 * To test HOGAN using a independent normal model do
>      mpirun -np 4 build/HOGAN examples/config/IndepNormals.xml
 * Each model can be similarly tested using the configurations files in the **examples/config/** directory:
    + *CorrelNormals.xml* for correlated normals
    + *BDRateDAG.xml* for simulated PyRate file
    + *BDRAlphaDAG.xml* for the empirical plant dataset
    + *CodonModels.xml* for the *M2a* codon model
    + *TreeInference.xml* for tree inference

-----------------------------------------------------------------------

### XML config files ###
Some important informations about some of the fields in the configurations files:

###### Processor configuration ######

 * **nProposal** >> Defines the number of processors used for the parallel MCMC algorithm (MPI)
 * **nChain** >> Defines the number of processors used for parallel MCMCMC (MPI)
 * **nThread** >> Defines the number of processors for the parallel likelihood (Posix)

The number of processors used is thus nProposal x nChain x nThread and the number of processors to specify to MPI is nProposal x nChain.

###### Adaptive proposals ######

 * **blockSize** >> Defines the number of parameters per blocks.
 * **adaptiveType** >> Defines the type of adaptive proposals and can be
    * *NotAdaptive*
    * *DefaultAdaptive*
    * *MixedAdaptive*
    * *PCA_Adaptive*
 * **blockStatCfg** >> Defines the convergence detection rate (extrafast, fast, default, slow)


###### Misc ######

 * **seed** >> Defines the starting random seed
 * **filename** >> Defines the path and prefix of the output log file that can be read with tracer if **writeBinary** is set to false
 * **nIteration** >> Defines the number of iterations

### How to cite
Please cite HOGAN using the following reference:
Xavier Meyer, Bastien Chopard, Nicolas Salamin; Accelerating Bayesian inference for evolutionary biology models. Bioinformatics 2017; 33 (5): 669-676. doi: 10.1093/bioinformatics/btw712


### Funding
The development of HOGAN was funded by the grant SNF CR32I3_143768 grant awarded by the Swiss National Science Foundation to Nicolas Salamin and Bastien Chopard.